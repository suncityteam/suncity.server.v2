﻿using System;
using System.ComponentModel.DataAnnotations;
using SunCity.Domain.Interfaces.Types;

namespace SunCity.Game.Types.Property.Entity.Property
{
    public readonly struct PropertyName
        : IName<PropertyName>
    {

        [MaxLength(MaxLength)]
        public string Name { get; }
        public const int MaxLength = 32;

        public PropertyName(string InPropertyName) => Name = InPropertyName;

        public static bool operator ==(PropertyName InLhs, PropertyName InRhs) => InLhs.Equals(InRhs);
        public static bool operator !=(PropertyName InLhs, PropertyName InRhs) => !InLhs.Equals(InRhs);


        #region Interfaces
        public override bool Equals(object? obj) => obj is PropertyName other && Equals(other);
        public bool Equals(PropertyName InPropertyName) => Name == InPropertyName.Name;
        public int CompareTo(PropertyName InPropertyName) => Name.CompareTo(InPropertyName);
        public override int GetHashCode() => (Name != null ? Name.GetHashCode() : 0);
        public string ToString(string? InFormat, IFormatProvider? InFormatProvider) => Name;
        public int CompareTo(object? InPropertyName) => Name.CompareTo(InPropertyName);
        #endregion
    }
}