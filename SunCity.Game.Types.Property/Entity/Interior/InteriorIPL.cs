﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;
using SunCity.Domain.Interfaces.Types;

namespace SunCity.Game.Types.Property.Entity.Interior
{
    /// <summary>
    /// IPL
    /// </summary>
    public readonly struct InteriorItemPlacement : IName<InteriorItemPlacement>
    {
        [Required]
        [MaxLength(128)]
        public string Name { get; }

        public InteriorItemPlacement(string InInteriorItemPlacement) => Name = InInteriorItemPlacement;


        #region Interfaces
        public bool Equals(InteriorItemPlacement InInteriorItemPlacement) => Name == InInteriorItemPlacement.Name;
        public override bool Equals(object? InObj) => InObj is InteriorItemPlacement other && Equals(other);

        public int CompareTo(InteriorItemPlacement InInteriorItemPlacement) => Name.CompareTo(InInteriorItemPlacement);
        public override int GetHashCode() => (Name != null ? Name.GetHashCode() : 0);
        public int CompareTo(object? InInteriorItemPlacement) => Name.CompareTo(InInteriorItemPlacement);
        #endregion

        public static bool operator ==(InteriorItemPlacement InLhs, InteriorItemPlacement InRhs) => InLhs.Equals(InRhs);
        public static bool operator !=(InteriorItemPlacement InLhs, InteriorItemPlacement InRhs) => !InLhs.Equals(InRhs);
    }
}
