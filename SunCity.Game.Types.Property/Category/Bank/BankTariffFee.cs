﻿using Microsoft.EntityFrameworkCore;

namespace SunCity.Game.Types.Property.Category.Bank
{
    /// <summary>
    /// Банковская комисия
    /// </summary>
    [Owned]
    public class BankTariffFee
    {
        /// <summary>
        /// Комиссия при действии в своём банке
        /// </summary>
        public double Own { get; set; }

        /// <summary>
        /// Комиссия при действии в другом банке
        /// </summary>
        public double Another { get; set; }

        public BankTariffFee() { }

        public BankTariffFee(double InOwn, double InAnother)
        {
            Own = InOwn;
            Another = InAnother;
        }

    }
}