﻿using System;
using System.Threading.Tasks;
using SunCity.RageMp.ClientScripts.Process;

namespace SunCity.RageMp.ClientScripts
{
    class Program
    {
        static async Task Main(string[] args)
        {          
            Console.WriteLine("Hello World!");
            var process = new RageMpClientScriptsFixProcess();
            await process.OnProcess();
        }
    }
}