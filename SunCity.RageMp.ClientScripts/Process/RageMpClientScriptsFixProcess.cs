﻿using System;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace SunCity.RageMp.ClientScripts.Process
{
    public class RageMpClientScriptsFixProcess
    {
        public async Task OnProcess()
        {
            //var server_root = Directory.GetCurrentDirectory();
            
            var server_root =@"D:\work\SunCity\SunCity.Server";
            var client_packages = Path.Combine(server_root, "client_packages");
            
            var filePaths = Directory.GetFiles(client_packages, "*.js", SearchOption.AllDirectories);
            var files = filePaths.Select(file => new RageMpClientScripts(file)).ToArray();

            foreach (var file in files)
            {
                await file.Read();
                file.FixPaths();
                await file.Save();
            }
        }
        
    }
}