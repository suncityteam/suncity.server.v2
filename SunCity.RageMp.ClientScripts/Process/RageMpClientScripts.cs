﻿using System.IO;
using System.Threading.Tasks;

namespace SunCity.RageMp.ClientScripts.Process
{
    public class RageMpClientScripts
    {
        public string Path { get; }
        public string RawContent { get; private set; }
        public string FixContent { get; private set; }

        public RageMpClientScripts(string path)
        {
            Path = path;
        }

        public async Task Read()
        {
            RawContent = await File.ReadAllTextAsync(Path);
        }

        public void FixPaths()
        {
            FixContent = RawContent.Replace("../", string.Empty);
        }

        public Task Save()
        {
            return File.WriteAllTextAsync(Path, FixContent);
        }
    }
}