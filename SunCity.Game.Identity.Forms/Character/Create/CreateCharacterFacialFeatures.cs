﻿using System.ComponentModel.DataAnnotations;

namespace SunCity.Game.Identity.Forms.Character.Create
{
    public class CreateCharacterFacialFeatures
    {
        [Required]
        public float NoseWidth { get; set; }
        
        [Required]
        public float NoseHeight { get; set; }
        
        [Required]
        public float NoseLength { get; set; }
        
        
        [Required]
        public float NoseBridge { get; set; }
        
        [Required]
        public float NoseTip { get; set; }
        
        [Required]
        public float NoseShift { get; set; }

        [Required]
        public float BrowHeight { get; set; }
        public float BrowWidth { get; set; }

        public float CheekboneHeight { get; set; }
        public float CheekboneWidth { get; set; }
        public float CheeksWidth { get; set; }

        public float Eyes { get; set; }
        public float Lips { get; set; }

        public float JawWidth { get; set; }
        public float JawHeight { get; set; }

        public float ChinLength { get; set; }
        public float ChinPosition { get; set; }
        public float ChinWidth { get; set; }
        public float ChinShape { get; set; }

        public float NeckWidth { get; set; }
    }
}