﻿using SunCity.Game.Enums.Character;

namespace SunCity.Game.Identity.Forms.Character.Create
{
    public class CreateCharacterGeneralInformation
    {
        public byte FatherId { get; set; }
        public byte MotherId { get; set; }
        public GameCharacterSex Sex { get; set; }

        public float Similarity { get; set; }
        public float Skin { get; set; }
        public float Ageing { get; set; }
        public short Sundamage { get; set; }
        public short Freckles { get; set; }


    }
}