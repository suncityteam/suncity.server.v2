﻿namespace SunCity.Game.Identity.Forms.Character.Create
{
    public class CreateCharacterHairAndColor
    {
        public byte Hair { get; set; }
        public byte HairColor { get; set; }

        public short Eyebrows { get; set; }
        public short EyebrowsColor { get; set; }

        public byte EyeColor { get; set; }

        public short Lipstick { get; set; }
        public short LipstickColor { get; set; }

        public short Beard { get; set; }
        public short BeardColor { get; set; }
    }
}