﻿using SunCity.Game.Enums.Character;

namespace SunCity.Game.Identity.Forms.Character.Create
{
    public class CreateCharacterPlayerName
    {
        //public GameCharacterStarterFraction Fraction { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }

        public override string ToString()
        {
            return $"{FirstName}_{LastName}";
        }

    }
}