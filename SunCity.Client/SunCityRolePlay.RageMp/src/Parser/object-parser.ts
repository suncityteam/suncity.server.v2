import {WithParser} from '../Parser/with-parser';

export class ObjectParser<T extends WithParser> {
    constructor(private classRef: typeof WithParser) {
    }

    parse<TContent>(json: any): TContent {
        return this.classRef.fromJson(json) as TContent;
    }
}
