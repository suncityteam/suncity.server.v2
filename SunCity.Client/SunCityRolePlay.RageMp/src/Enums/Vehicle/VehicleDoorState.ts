export enum VehicleDoorState {
    DoorClosed,
    DoorOpen,
    DoorBroken,
}

export enum VehicleDoorId {
    DoorFrontLeft,
    DoorFrontRight,
    DoorRearLeft,
    DoorRearRight,
    DoorHood,
    DoorTrunk
}
