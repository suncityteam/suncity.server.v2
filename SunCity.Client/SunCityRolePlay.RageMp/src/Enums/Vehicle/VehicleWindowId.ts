export enum VehicleWindowId {
    WindowFrontRight,
    WindowFrontLeft,
    WindowRearRight,
    WindowRearLeft
}

