//  https://wiki.rage.mp/index.php?title=Raycasting::testPointToPoint
export enum TracePointToPointFlags {
    None,
    Map = 1,
    Vehicles = 2,
    Peds = 4,
    PedsToo = 8,
    Objects = 16,
    Unk1 = 32,
    Unk2 = 64,
    Unk3 = 128,
    Flores = 256
}
