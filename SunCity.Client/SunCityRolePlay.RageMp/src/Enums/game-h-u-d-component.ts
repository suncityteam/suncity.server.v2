//  https://wiki.rage.mp/index.php?title=HUD_Components
export enum GameHUDComponent {
    HUD = 0,
    HUD_WANTED_STARS = 1,
    HUD_WEAPON_ICON = 2,
    HUD_CASH = 3,
    HUD_MP_CASH = 4,
    HUD_CASH_CHANGE = 13,
    HUD_RADIO_STATIONS = 16,
    HUD_WEAPON_WHEEL = 19,
    HUD_WEAPON_WHEEL_STATS = 20,
    MAX_HUD_WEAPONS = 22
}
