export enum WorldPropertyCategory {
    None,

    // ========================

    /// <summary>
    /// Частный дом. Может быть с гаражем
    /// </summary>
    House,

    /// <summary>
    /// Гараж
    /// </summary>
    Garage,

    /// <summary>
    /// Комната в отеле
    /// </summary>
    HotelRoom,

    /// <summary>
    /// Квартира в доме
    /// </summary>
    Apartment,


    // ========================

    /// <summary>
    /// Магазин
    /// </summary>
    Store,

    /// <summary>
    /// Офис
    /// </summary>
    Office,

    /// <summary>
    /// Склад
    /// </summary>
    WareHouse,

    /// <summary>
    /// Клуб
    /// </summary>
    Club,

    /// <summary>
    /// Ресторан
    /// </summary>
    Restaurant,

    /// <summary>
    /// Бар
    /// </summary>
    Bar,

    /// <summary>
    /// Казино
    /// </summary>
    Casino,

    /// <summary>
    /// Банк
    /// </summary>
    Bank,

    // ------------------------
    /// <summary>
    /// Ферма
    /// </summary>
    Farm,

    /// <summary>
    /// Шахта
    /// </summary>
    Miner,

    /// <summary>
    /// Лесопилка
    /// </summary>
    Sawmill,

    // ------------------------
    /// <summary>
    /// Автосервис
    /// </summary>
    CarService,

    /// <summary>
    /// Автосалон
    /// </summary>
    CarShowroom,

    /// <summary>
    /// Заправка
    /// </summary>
    GasStation,


    // ------------------------

    /// <summary>
    /// Телефонная компания
    /// </summary>
    TelephoneCompany,

    /// <summary>
    /// Электрическая компания
    /// </summary>
    EnergyCompany,

    /// <summary>
    /// Страховая компания
    /// </summary>
    InsuranceCompany,

    /// <summary>
    /// Агентство недвижимости
    /// </summary>
    RealEstateAgency,


    /// <summary>
    /// Адвокатское бюро
    /// </summary>
    LawCompany,

    /// <summary>
    /// Аренда авто
    /// </summary>
    CarSharing,

    /// <summary>
    /// Авторынок
    /// </summary>
    AutoMarket,

    /// <summary>
    /// Нефтеперерабатывающий завод
    /// </summary>
    OilRefinery,

    /// <summary>
    /// Нефтяная вышка
    /// </summary>
    OilDerrick
}

export function toFriendlyString(category: WorldPropertyCategory) {
    switch (category) {
        case WorldPropertyCategory.House:
            return 'Дом';
        case WorldPropertyCategory.Garage:
            return 'Гараж';
        case WorldPropertyCategory.HotelRoom:
            return 'Комната в отеле';
        case WorldPropertyCategory.Apartment:
            return 'Квартира';
        case WorldPropertyCategory.Store:
            return 'Магазин';
        case WorldPropertyCategory.Office:
            return 'Офис';
        case WorldPropertyCategory.WareHouse:
            return 'Склад';
        case WorldPropertyCategory.Club:
            return 'Клуб';
        case WorldPropertyCategory.Restaurant:
            return 'Ресторан';
        case WorldPropertyCategory.Bar:
            return 'Бар';
        case WorldPropertyCategory.Casino:
            return 'Казино';
        case WorldPropertyCategory.Bank:
            return 'Банк';
        case WorldPropertyCategory.Farm:
            return 'Ферма';
        case WorldPropertyCategory.Miner:
            return 'Шахта';
        case WorldPropertyCategory.Sawmill:
            return 'Лесопилка';
        case WorldPropertyCategory.CarService:
            return 'СТО';
        case WorldPropertyCategory.CarShowroom:
            return 'Автосалон';
        case WorldPropertyCategory.GasStation:
            break;
        case WorldPropertyCategory.TelephoneCompany:
            break;
        case WorldPropertyCategory.EnergyCompany:
            break;
        case WorldPropertyCategory.InsuranceCompany:
            break;
        case WorldPropertyCategory.RealEstateAgency:
            break;
        case WorldPropertyCategory.LawCompany:
            break;
        case WorldPropertyCategory.CarSharing:
            break;
        case WorldPropertyCategory.AutoMarket:
            break;
        case WorldPropertyCategory.OilRefinery:
            break;
        case WorldPropertyCategory.OilDerrick:
            break;

    }
    return 'Недвижимость';
}
