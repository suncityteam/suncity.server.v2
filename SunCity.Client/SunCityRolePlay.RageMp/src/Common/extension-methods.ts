import {TracePointToPointFlags} from '../Enums/trace-point-to-point-flags';

export function isNullOrUndefined(obj: any): boolean {
    if (obj === null) {
        return true;
    }

    if (obj === undefined) {
        return true;
    }

    return false;
}

export function getLookingAtEntity(traceFlags: TracePointToPointFlags, distance: number = 10): EntityMp | undefined {
    const res = mp.game.graphics.getScreenActiveResolution(1, 1);
    const secondPoint = mp.game.graphics.screen2dToWorld3d(new mp.Vector3(res.x / 2, res.y / 2, 0));
    if (!secondPoint) {
        return null;
    }

    const startPosition = mp.players.local.getBoneCoords(12844, 0.5, 0, 0);
    startPosition.z -= 0.3;

    const result = mp.raycasting.testPointToPoint(startPosition, secondPoint, mp.players.local.handle, traceFlags);
    if (result && result.entity && result.entity.type) {
        const entPos = result.entity.position;
        const lPos = mp.players.local.position;
        if (mp.game.gameplay.getDistanceBetweenCoords(entPos.x, entPos.y, entPos.z, lPos.x, lPos.y, lPos.z, true) > distance) {
            return null;
        }
        return result.entity;
    }
    return null;
}

export function getControlActionName(id: number): string | null {
    if (id > -1 && id < 357) {
        //  https://wiki.rage.mp/index.php?title=Controls::disableControlAction
        return mp.game.controls.getControlActionName(2, id, true);
    } else {
        mp.gui.chat.push('!{orange}[WARNING] !{white}Invalid controlID, make sure its between (0, 356).');
        return null;
    }
}

export function hexToRGB(hex: string) {
    const bigint = parseInt(hex.replace(/[^0-9A-F]/gi, ''), 16);
    // tslint:disable-next-line:no-bitwise
    return [(bigint >> 16) & 255, (bigint >> 8) & 255, bigint & 255];
}

