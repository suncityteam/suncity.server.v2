export interface IAbstractModule {
    onBegin(): void;
    onComplete(): void;
    onInit(): void;
}

export function CreateModuleInstance<T extends IAbstractModule>(type /*: { new(): T ;}*/ ): T  {
    const instance = Object.create(type) as T;
    instance.onInit();

    return instance;
}
