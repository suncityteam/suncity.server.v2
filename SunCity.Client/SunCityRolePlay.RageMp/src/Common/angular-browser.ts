import {ControllerBrowserResponse, ControllerResponse} from '../Models/ragemp';

export class AngularBrowser {
    public static readonly host = 'http://localhost:4200/';
    public static Instance: AngularBrowser;
    public readonly browser = mp.browsers.new(`${AngularBrowser.host}/authorization`);

    public static Create(): AngularBrowser {
        AngularBrowser.Instance = new AngularBrowser();
        AngularBrowser.Instance.browser.execute(`onInitRageClientId('${mp.players.local.id}');`);

        mp.events.add('Handler:BrowserEvent', (json) => {
            AngularBrowser.Instance.browser.execute(`onCallCustomRageEvent('${json}');`);
        });
        return AngularBrowser.Instance;
    }

    public callAngularEvent(eventName: string, object: any): void {
        const response: ControllerResponse<any> = {
            content: object,
            paramName: 'ragemp',
            status: 'Successfully'
        };

        const browserResponse: ControllerBrowserResponse = {
            eventName,
            response,
            requestId: 'ragemp'
        };

        // this.browser.execute(`alert('${eventName}');`);

        const json = JSON.stringify(browserResponse);
        this.browser.execute(`onCallCustomRageEvent('${json}');`);
    }
}
