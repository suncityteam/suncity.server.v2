import {OperationResponse} from '../Models/operation-response';
import {WithParser} from '../Parser/with-parser';
import {ObjectParser} from '../Parser/object-parser';


export class EventManager {
    public static readonly instance = new EventManager();

    constructor() {
        mp.events.add('CallServerEvent', (args) => EventManager.callServerEvent(args));
    }

    public static removeEvent(handler: EventMp | undefined): void {
        if (handler) {
            handler.destroy();
        }
    }

    public static bindCEFEventWithType<TContent extends WithParser>(type: any, eventName: string, callback: (args: TContent) => void): EventMp {
        const handler = new mp.Event(
            `CEF:${eventName}`,
            (jsonStr) => {
                const json = JSON.parse(jsonStr) as TContent;
                const parser = new ObjectParser<TContent>(type);
                const object = parser.parse(json) as TContent;
                callback(object);
            }
        );

        return handler;
    }

    public static bindCEFEvent(eventName: string, callback: (args: any) => void): EventMp {
        const handler = new mp.Event(
            `CEF:${eventName}`,
            (json) => {
                callback(JSON.parse(json));
            }
        );

        return handler;
    }

    public static bindEvent(eventName: string, callback: (args?: OperationResponse<any>) => void): EventMp {
        const handler = new mp.Event(
            `Event:${eventName}`,
            (json) => {
                callback(JSON.parse(json));
            }
        );

        return handler;
    }

    public static bindEventWithType<TContent extends WithParser>(type: any, eventName: string, callback: (args: OperationResponse<TContent>) => void): EventMp {
        const handler = new mp.Event(
            `Event:${eventName}`,
            (jsonStr) => {
                const json = JSON.parse(jsonStr) as OperationResponse<TContent>;
                const parser = new ObjectParser<TContent>(type);

                const object = parser.parse(json.content);
                const result: OperationResponse<TContent> = {
                    error: json.error,
                    isCorrect: json.isCorrect,
                    isNotCorrect: json.isNotCorrect,
                    content: object as TContent
                };

                callback(result);
            }
        );

        return handler;
    }

    public static bindEventHandler(eventName: string, callback: (args: OperationResponse<any>) => void): EventMp {
        const handler = new mp.Event(
            `Handler:${eventName}`,
            (json) => {
                callback(JSON.parse(json));
            }
        );

        return handler;
    }

    public static callServerEvent(...args): void {
        mp.events.callRemote(args[0], ...args.slice(1));
    }

    public static callClientEvent(...args): void {
        mp.events.call(`Event:${args[0]}`, ...args.slice(1));
    }
}
