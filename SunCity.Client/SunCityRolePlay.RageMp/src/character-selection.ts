import {configureWorldPlayerCharacter} from './character-shared';

export class CharacterSelection {
    public static Create(): CharacterSelection {
        const instance = new CharacterSelection();
        instance.onInit();

        return instance;
    }

    private onInit() {
        mp.events.add('CEF:Player.Character.Select.Complete', () => this.onComplete());
    }

    private onComplete(): void {
        configureWorldPlayerCharacter();
    }
}

