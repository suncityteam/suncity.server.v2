import {Vector3} from '../../Models/vector3';
import {WithParser} from '../../Parser/with-parser';


export class VehicleShowRoomRageModel extends WithParser {
    readonly entityId: string;
    readonly standPosition: Vector3;
    readonly standCamera: Vector3;
    readonly standRotation: Vector3;

    constructor(id: string, stand: Vector3, rotation: Vector3, camera: Vector3) {
        super();
        this.entityId = id;
        this.standPosition = stand;
        this.standRotation = rotation;
        this.standCamera = camera;
    }

    static fromJson(value: VehicleShowRoomRageModel): VehicleShowRoomRageModel {
        return new VehicleShowRoomRageModel
        (
            value.entityId,
            Vector3.fromJson(value.standPosition),
            Vector3.fromJson(value.standRotation),
            Vector3.fromJson(value.standCamera)
        );
    }
}
