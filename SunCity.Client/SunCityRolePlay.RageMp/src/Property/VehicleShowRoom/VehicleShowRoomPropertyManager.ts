import {OperationResponse} from '../../Models/operation-response';
import {VehicleShowRoomRageModel} from '../../Property/VehicleShowRoom/VehicleShowRoomRageModel';

import {toggleGameHud} from '../../character-shared';
import {UiManager} from '../../HUD/ui-manager';
import {EventManager} from '../../Common/event-manager';
import {RotationOrder, Zero} from '../../Models/Vector/Vector3/Vector3Mp';
import {RgbColor} from '../../Models/Color/RgbColor';

interface VehicleShowRoomPropertySessionEventHandlers {
    changeFirstVehicleColor: EventMp;
    changeSecondVehicleColor: EventMp;
    changeVehicleModel: EventMp;
}

class VehicleShowRoomPropertySession {
    private readonly eventHandlers: VehicleShowRoomPropertySessionEventHandlers;
    private readonly sceneryCamera: CameraMp;

    private readonly standPosition: Vector3Mp;
    private readonly standRotation: Vector3Mp;

    private readonly standCamera: Vector3Mp;
    private readonly standCameraFov = 50;

    private vehicle: VehicleMp;

    constructor(showRoom: VehicleShowRoomRageModel) {
        this.standPosition = new mp.Vector3(showRoom.standPosition.x, showRoom.standPosition.y, showRoom.standPosition.z);
        this.standCamera = new mp.Vector3(showRoom.standCamera.x, showRoom.standCamera.y, showRoom.standCamera.z);
        this.standRotation = new mp.Vector3(showRoom.standRotation.x, showRoom.standRotation.y, showRoom.standRotation.z);

        this.sceneryCamera = mp.cameras.new('default', this.standCamera, Zero, this.standCameraFov);
        this.sceneryCamera.pointAtCoord(this.standPosition.x, this.standPosition.y, this.standPosition.z);

        this.eventHandlers = {
            changeFirstVehicleColor: EventManager.bindCEFEventWithType<RgbColor>(RgbColor, 'Property:Business:VehicleShowRoom:ChangeFirstVehicleColor', colors => this.onVehicleFirstColorChanged(colors)),
            changeSecondVehicleColor: EventManager.bindCEFEventWithType<RgbColor>(RgbColor, 'Property:Business:VehicleShowRoom:ChangeSecondVehicleColor', colors => this.onVehicleSecondColorChanged(colors)),
            changeVehicleModel: EventManager.bindCEFEvent('Property:Business:VehicleShowRoom:ChangeVehicleModel', vehicle => this.onVehicleModelChanged(vehicle))
        };

        this.onBegin();
    }

    public onVehicleFirstColorChanged(color: RgbColor): void {
        this.vehicle.setCustomPrimaryColour(color.r, color.g, color.b);
    }

    public onVehicleSecondColorChanged(color: RgbColor): void {
        this.vehicle.setCustomSecondaryColour(color.r, color.g, color.b);
    }

    public onVehicleModelChanged(model: string): void {
        const vehicleModelHash = mp.game.joaat(model);

        if (this.vehicle) {
            this.vehicle.model = vehicleModelHash;
        } else {
            this.vehicle = mp.vehicles.new(vehicleModelHash, this.standPosition, {dimension: mp.players.local.dimension});
        }

        this.vehicle.position = this.standPosition;
        this.vehicle.setRotation(this.standRotation.x, this.standRotation.y, this.standRotation.z, RotationOrder, true);
    }

    public onBegin() {
        this.sceneryCamera.setActive(true);
        mp.game.cam.renderScriptCams(true, false, 0, true, false);
        toggleGameHud(false);
    }

    public onComplete() {
        if (this.sceneryCamera.doesExist()) {
            this.sceneryCamera.destroy(true);
        }

        if (this.vehicle) {
            this.vehicle.destroy();
            this.vehicle = undefined;
        }

        mp.game.cam.renderScriptCams(false, false, 0, true, false);
        EventManager.removeEvent(this.eventHandlers.changeVehicleModel);
        EventManager.removeEvent(this.eventHandlers.changeFirstVehicleColor);
        EventManager.removeEvent(this.eventHandlers.changeSecondVehicleColor);

        toggleGameHud(true);
    }
}

export class VehicleShowRoomPropertyManager {

    private session: VehicleShowRoomPropertySession;

    public static Create(): VehicleShowRoomPropertyManager {
        const instance = new VehicleShowRoomPropertyManager();
        instance.onInit();

        return instance;
    }

    private onInit() {
        EventManager.bindEventWithType<VehicleShowRoomRageModel>(VehicleShowRoomRageModel, 'Property:Business:VehicleShowRoom:Client:Menu.Open', (result => this.onShowClientMenu(result)));
        EventManager.bindCEFEvent('Property:Business:VehicleShowRoom:Client:Menu.Close', () => this.onCloseClientMenu());
        EventManager.bindEvent('Property:Business:VehicleShowRoom:Client:Menu.Close', () => this.onCloseClientMenu());
    }

    private onCloseClientMenu() {
        if (this.session) {
            this.session.onComplete();
            this.session = undefined;
        }
    }

    private onShowClientMenu(result: OperationResponse<VehicleShowRoomRageModel>) {
        this.onCloseClientMenu();
        this.session = new VehicleShowRoomPropertySession(result.content);
        UiManager.Instance.openWindow(`World/Property/VehicleShowRoom/Client/${result.content.entityId}`);
    }
}
