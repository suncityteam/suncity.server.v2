import {WorldPropertyLiteModel} from '../Models/world-property-lite-model';
import {OperationResponse} from '../Models/operation-response';
import {toFriendlyString} from '../Enums/world-property-category';
import {KeyboardKeyCodes} from '../Enums/keyboard-key-codes';
import {WorldPlayer} from '../world-player';
import {EventManager} from '../Common/event-manager';
import {AngularBrowser} from '../Common/angular-browser';
import {UiManager} from '../HUD/ui-manager';
import {IPropertyId} from '../Types/property-types';

export class PropertyManager {
    private properties: WorldPropertyLiteModel[];
    private lastRequestedIPL: string;

    public static Create(): PropertyManager {
        const instance = new PropertyManager();
        instance.onInit();

        return instance;
    }

    private onInit() {

        EventManager.bindEvent('Property:Business:Bank:Client:Menu', (result => this.onShowClientBankMenu(result)));

        //EventManager.bindEvent('Property:GetPropertyList', (result => this.onGetPropertyListResponse(result)));
        EventManager.bindEvent('Property:Buy', (result => this.onBeginPropertyBuy(result)));

        mp.events.add('playerEnterColshape', (shape: ColshapeMp) => this.onEnterColShape(shape));

        EventManager.bindEvent('RequestIpl', (ipl) => this.onRequestIPL(ipl));
        EventManager.bindEvent('RemoveIpl', (ipl) => this.onRemoveIPL(ipl));

        mp.keys.bind(KeyboardKeyCodes.Alt, true, () => this.onClickedEnterOrExit());

        this.startLoadPropertyList();
    }

    private onRequestIPL(IPL: OperationResponse<string>) {
        if (this.lastRequestedIPL) {
            mp.game.streaming.removeIpl(this.lastRequestedIPL);
        }

        mp.game.streaming.requestIpl(IPL.content);
        this.lastRequestedIPL = IPL.content;
    }

    private onRemoveIPL(IPL: OperationResponse<string>) {
        mp.game.streaming.removeIpl(IPL.content);
        this.lastRequestedIPL = null;
    }

    private onClickedEnterOrExit(): void {
        if (WorldPlayer.isDead()) {
            return;
        }

        if (WorldPlayer.isInAnyVehicle()) {
            return;
        }

        if (WorldPlayer.isAuthorized()) {
            mp.events.callRemote('Property:EnterOrExit');
        }
    }

    private getPropertyById(entityId: string): WorldPropertyLiteModel | null {
        for (const property of this.properties) {
            if (property.EntityId === entityId) {
                return property;
            }
        }
        return null;
    }

    private getPropertyByShape(shape: ColshapeMp): WorldPropertyLiteModel | null {
        for (const property of this.properties) {
            if (property.colShape.id === shape.id) {
                return property;
            }
        }
        return null;
    }

    private onEnterColShape(shape: ColshapeMp): void {
        const property = this.getPropertyByShape(shape);
        if (property) {
            if (property.HasOwner) {
                if (property.HasInterior) {
                    mp.game.graphics.notify(`Для входа в ${toFriendlyString(property.Category)} используйте ALT`);
                }
            } else {
                mp.game.graphics.notify(`Продается ${toFriendlyString(property.Category)}, для покупки используйте ALT`);
            }
        }
    }

    private startLoadPropertyList(): void {
        mp.gui.chat.push('startLoadPropertyList');
        setTimeout(() => this.tryLoadPropertyList(), 2500);
    }

    private tryLoadPropertyList(): void {
        if (this.properties) {
            return;
        }

        this.startLoadPropertyList();
        EventManager.callServerEvent('Property:GetPropertyList');
    }

    private onBeginPropertyBuy(properties: OperationResponse<IPropertyId>) {
        UiManager.Instance.openWindow(`World/Property/Buying/${properties.content.id}`);
    }

    private onGetPropertyListResponse(properties: OperationResponse<WorldPropertyLiteModel[]>) {
        if (properties && properties.content && properties.content.length) {
            for (const prop of properties.content) {
                // prop.colShape = mp.colshapes.newCircle(prop.Position.X, prop.Position.Y, 2);
            }
            this.properties = properties.content;
        }
    }

    private onShowClientBankMenu(properties: OperationResponse<IPropertyId>) {
        UiManager.Instance.openWindow(`World/Property/Bank/Card/Client/Authorization/${properties.content.id}`);
    }
}
