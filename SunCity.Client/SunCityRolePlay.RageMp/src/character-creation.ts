import {
    CharacterCreationFacialFeaturesForm,
    CharacterCreationForm,
    CharacterCreationGeneralInformationForm,
    CharacterCreationHairAndColorForm,
    CharacterCreationView,
} from './Forms/character-creation-form';
import {IAbstractModule} from './Common/abstract-module';
import {toggleGameHud} from './character-shared';

export class CharacterCreation implements IAbstractModule {
    private form: CharacterCreationForm;
    private sceneryCamera: CameraMp;

    private readonly headOverlays: { [id: number]: { index: number, opacity: number, firstColor: number, secondColor: number } } = {};

    private readonly headOverlayColors: { [id: number]: { colorType: number, colorID: number, secondColorID: number } } = {};

    private readonly faceFeatures: { [id: number]: { scale: number } } = {};

    private headBlendData: {
        shapeFirstID: number, shapeSecondID: number, shapeThirdID: number, skinFirstID: number,
        skinSecondID: number, skinThirdID: number, shapeMix: number, skinMix: number, thirdMix: number, isParent: boolean
    };

    private eyeColor = -1;

    private hairColor: { colorID: number, highlightColorID: number } = {colorID: -1, highlightColorID: -1};

    public static Create(): CharacterCreation {
        const instance = new CharacterCreation();
        instance.onInit();
        return instance;
    }

    public onBegin() {
        this.form = null;
        this.sceneryCamera = mp.cameras.new('default', new mp.Vector3(152.6008, -1001.25, -98.45), new mp.Vector3(-20.0, 0.0, 0.0), 90);
        this.sceneryCamera.setActive(true);
        mp.game.cam.renderScriptCams(true, false, 0, true, false);
        toggleGameHud(false);
    }

    public onComplete() {
        if (this.sceneryCamera != null) {
            this.sceneryCamera.destroy();
        }
        mp.game.cam.renderScriptCams(false, false, 0, true, false);
    }

    public onSwitchView(view: CharacterCreationView) {
        if (view === CharacterCreationView.Character) {
            // Make the camera point to the body
            this.sceneryCamera.setCoord(152.3708, -1001.75, -98.45);
        } else if (view === CharacterCreationView.Face) {
            // Make the camera point to the face
            this.sceneryCamera.setCoord(152.6008, -1001.25, -98.45);
        }
    }

    public onInit() {
        mp.events.add('CEF:Player.Character.Create.Begin', () => this.onBegin());
        mp.events.add('CEF:Player.Character.Create.Process', (form) => this.onFormChanged(JSON.parse(form)));
        mp.events.add('CEF:Player.Character.Create.SwitchView', (view) => this.onSwitchView(JSON.parse(view)));
        mp.events.add('CEF:Player.Character.Create.Complete', () => this.onComplete());

    }

    public onFormChanged(form: CharacterCreationForm) {
        if (form.character) {
            this.setModel(form.character);
        }

        if (form.facialFeatures) {
            this.setFaceFeatures(form.facialFeatures);
        }

        if (form.hairs) {
            this.setHeadOverlays(form.hairs);
        }

        this.form = form;
    }

    private setModel(form: CharacterCreationGeneralInformationForm) {
        const gender = form.sex;
        if (!this.form || !this.form.character || this.form.character.sex !== gender) {
            if (gender === 0) {
                //  Мужчина
                mp.players.local.model = mp.game.joaat('mp_m_freemode_01');
            } else /*if (gender == 1)*/ {
                //  Женщина
                mp.players.local.model = mp.game.joaat('mp_f_freemode_01');
            }
        }

        this.setHeadBlendData
        (
            form.motherId,
            form.fatherId,
            0,
            form.motherId,
            form.fatherId,
            0,
            form.similarity,
            form.skin,
            0.0,
            true
        );

        this.setHeadOverlay(3, form.ageing, 100, 0, 0);
        this.setHeadOverlay(7, form.sundamage, 100, 0, 0);
        this.setHeadOverlay(9, form.freckles, 100, 0, 0);
    }

    private setHeadOverlays(form: CharacterCreationHairAndColorForm) {
        this.setHeadOverlay(8, form.lipstick, 100, 0, 0);
        this.setHeadOverlayColor(8, 1, form.lipstickColor, 100);

        this.setHeadOverlay(8, form.eyebrows, 100, 0, 0);
        this.setHeadOverlayColor(2, 1, form.eyebrowsColor, 100);

        // this.setHeadOverlay(10, form.chest, 100, 0, 0);
        // this.setHeadOverlayColor(10, 1, form.chestColor, 100);

        mp.players.local.setComponentVariation(2, form.hair, 1, 0);
        this.setHairColor(form.hairColor, 0);
        this.setEyeColor(form.eyeColor);
    }

    private setFaceFeatures(form: CharacterCreationFacialFeaturesForm) {
        this.setFaceFeature(1, form.noseHeight);
        this.setFaceFeature(2, form.noseLength);
        this.setFaceFeature(3, form.noseBridge);
        this.setFaceFeature(4, form.noseTip);
        this.setFaceFeature(5, form.noseShift);

        this.setFaceFeature(6, form.browHeight);
        this.setFaceFeature(7, form.browWidth);

        this.setFaceFeature(8, form.cheekboneHeight);
        this.setFaceFeature(9, form.cheekboneWidth);
        this.setFaceFeature(10, form.cheeksWidth);

        this.setFaceFeature(11, form.eyes);
        this.setFaceFeature(12, form.lips);

        this.setFaceFeature(13, form.jawWidth);
        this.setFaceFeature(14, form.jawHeight);

        this.setFaceFeature(15, form.chinLength);
        this.setFaceFeature(16, form.chinPosition);
        this.setFaceFeature(17, form.chinWidth);
        this.setFaceFeature(18, form.chinShape);

        this.setFaceFeature(19, form.neckWidth);

    }

    private setHeadOverlay(overlayID: number, index: number, opacity: number, firstColor: number, secondColor: number): void {
        if (!this.headOverlays[overlayID] || this.headOverlays[overlayID].index !== index) {
            this.headOverlays[overlayID] = {index, opacity, firstColor, secondColor};
            mp.players.local.setHeadOverlay(overlayID, index, opacity, firstColor, secondColor);
        }
    }

    private setHeadOverlayColor(overlayID: number, colorType: number, colorID: number, secondColorID: number): void {
        if (
            !this.headOverlayColors[overlayID]
            || this.headOverlayColors[overlayID].colorType !== colorType
            || this.headOverlayColors[overlayID].colorID !== colorID
            || this.headOverlayColors[overlayID].secondColorID !== secondColorID
        ) {
            this.headOverlayColors[overlayID] = {colorType, colorID, secondColorID};
            mp.players.local.setHeadOverlayColor(overlayID, colorType, colorID, secondColorID);
        }
    }

    private setFaceFeature(index: number, scale: number): void {
        if (!this.faceFeatures[index] || this.faceFeatures[index].scale !== scale) {
            this.faceFeatures[index] = {scale};
            mp.players.local.setFaceFeature(index, scale);
        }

    }

    private setHeadBlendData(shapeFirstID: number, shapeSecondID: number, shapeThirdID: number, skinFirstID: number,
                             skinSecondID: number, skinThirdID: number, shapeMix: number, skinMix: number, thirdMix: number, isParent: boolean): void {
        if
        (
            !this.headBlendData

            || this.headBlendData.shapeFirstID !== shapeFirstID
            || this.headBlendData.shapeSecondID !== shapeSecondID
            || this.headBlendData.shapeThirdID !== shapeThirdID

            || this.headBlendData.skinFirstID !== skinFirstID
            || this.headBlendData.skinSecondID !== skinSecondID
            || this.headBlendData.skinThirdID !== skinThirdID

            || this.headBlendData.shapeMix !== shapeMix
            || this.headBlendData.skinMix !== skinMix
            || this.headBlendData.thirdMix !== thirdMix

            || this.headBlendData.isParent !== isParent
        ) {
            this.headBlendData = {shapeFirstID, shapeSecondID, shapeThirdID, skinFirstID, skinSecondID, skinThirdID, shapeMix, skinMix, thirdMix, isParent};
            mp.players.local.setHeadBlendData(shapeFirstID, shapeSecondID, shapeThirdID, skinFirstID, skinSecondID, skinThirdID, shapeMix, skinMix, thirdMix, isParent);
        }
    }

    private setEyeColor(index: number): void {
        if (this.eyeColor !== index) {
            this.eyeColor = index;
            mp.players.local.setEyeColor(index);
        }
    }

    private setHairColor(colorID: number, highlightColorID: number): void {
        if (this.hairColor.colorID !== colorID || this.hairColor.highlightColorID !== highlightColorID) {
            this.hairColor = {colorID, highlightColorID};
            mp.players.local.setHairColor(colorID, highlightColorID);
        }
    }

}
