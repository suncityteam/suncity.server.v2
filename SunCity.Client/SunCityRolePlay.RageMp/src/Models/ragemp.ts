
export interface ControllerBrowserResponse {
    response: ControllerResponse<any>;
    eventName: string;
    requestId: string;
}

export interface ControllerResponseBasic {
    status: string;
    paramName: string;
}


export interface ControllerResponse<T> extends ControllerResponseBasic {
    content: T;
}

