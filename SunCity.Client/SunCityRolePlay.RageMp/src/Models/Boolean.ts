import {WithParser} from '../Parser/with-parser';

export class Bool extends WithParser {

    constructor(value: boolean) {
        super();
        this.value = value;
    }

    public static TRUE: Bool = new Bool(true);
    public static FALSE: Bool = new Bool(false);

    private static readonly TRUE_VALUES = [
        'true',
        'yes',
        '1'
    ];

    readonly value: boolean;

    static fromJson(value: Bool): Bool {
        //  Hack: For not override boolean to Bool wrapper on backend
        const str = value.toString().toLocaleLowerCase();
        if (Bool.TRUE_VALUES.some(s => str === s)) {
            return Bool.TRUE;
        }

        return Bool.FALSE;
    }
}
