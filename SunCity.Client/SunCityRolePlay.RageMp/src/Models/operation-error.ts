export interface OperationError {
    readonly code: number;
    readonly message: string;
}
