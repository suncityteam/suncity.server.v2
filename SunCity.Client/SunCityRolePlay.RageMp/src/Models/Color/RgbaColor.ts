import {RgbColor} from './RgbColor';

export class RgbaColor extends RgbColor {
    readonly A: number;

    constructor(red: number, green: number, blue: number, alpha: number) {
        super(red, green, blue);
        this.A = alpha;
    }

    static fromJson(value: RgbaColor): RgbaColor {
        return new RgbaColor
        (
            parseInt(value.r.toString(), 10),
            parseInt(value.g.toString(), 10),
            parseInt(value.b.toString(), 10),
            parseInt(value.A.toString(), 10)
        );
    }
}
