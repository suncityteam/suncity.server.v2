import {WithParser} from '../../Parser/with-parser';
import {isNullOrUndefined} from '../../Common/extension-methods';

export class RgbColor extends WithParser {
    public static readonly Black: RgbColor = new RgbColor(0, 0, 0);
    public static readonly White: RgbColor = new RgbColor(255, 255, 255);

    readonly r: number;
    readonly g: number;
    readonly b: number;

    constructor(red: number, green: number, blue: number) {
        super();
        this.r = red;
        this.g = green;
        this.b = blue;
    }

    static isValid(value: RgbColor): boolean {
        if (!value) {
            return false;
        }

        if (isNullOrUndefined(value.r)) {
            return false;
        }

        if (isNullOrUndefined(value.g)) {
            return false;
        }

        if (isNullOrUndefined(value.b)) {
            return false;
        }

        return true;
    }

    static fromJson(value: RgbColor): RgbColor {
        return new RgbColor
        (
            parseInt(value.r.toString(), 10),
            parseInt(value.g.toString(), 10),
            parseInt(value.b.toString(), 10)
        );
    }
}

