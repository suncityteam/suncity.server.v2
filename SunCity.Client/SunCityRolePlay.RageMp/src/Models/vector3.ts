import {WithParser} from '../Parser/with-parser';

export class Vector3 extends WithParser {
    readonly x: number;
    readonly y: number;
    readonly z: number;

    constructor(x: number, y: number, z: number) {
        super();
        this.x = x;
        this.y = y;
        this.z = z;
    }

    static fromJson(value: Vector3): Vector3 {
        return new Vector3
        (
            parseFloat(value.x.toString()),
            parseFloat(value.y.toString()),
            parseFloat(value.z.toString())
        );
    }
}
