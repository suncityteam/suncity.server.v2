import {OperationError} from './operation-error';

export class OperationResponse<TContent> {
    readonly content: TContent;
    readonly error?: OperationError;
    readonly isCorrect: boolean;
    readonly isNotCorrect: boolean;

    static parse<TContent>(value: OperationResponse<TContent>): OperationResponse<TContent> {
        return value;
    }
}

