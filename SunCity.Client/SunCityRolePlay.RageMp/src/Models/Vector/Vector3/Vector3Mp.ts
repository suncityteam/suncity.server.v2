export const Zero: Vector3Mp = new mp.Vector3(0, 0, 0);
export const One: Vector3Mp = new mp.Vector3(1, 1, 1);

//  https://wiki.rage.mp/index.php?title=Entity::setRotation
//  value ranges from 0 to 5
//  For the most part R* uses 1 or 2 as the order.
export const  RotationOrder = 2;
