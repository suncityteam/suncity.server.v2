import {WorldPropertyCategory} from '../Enums/world-property-category';
import {Vector3} from './vector3';

export interface WorldPropertyLiteModel {
    readonly EntityId: string;
    readonly Position: Vector3;
    readonly Category: WorldPropertyCategory;
    readonly HasInterior: boolean;
    readonly HasOwner: boolean;
    colShape: ColshapeMp;
}
