import {AngularBrowser} from '../Common/angular-browser';
import {toggleGameHud} from '../character-shared';
import {GameControls} from '../Enums/game-controls';
import {TracePointToPointFlags} from '../Enums/trace-point-to-point-flags';
import {getLookingAtEntity} from '../Common/extension-methods';
import {GameHUDComponent} from '../Enums/game-h-u-d-component';
//  https://github.com/DerFlamer12332/Rage-Server/blob/5638f0cb1c788a4f118620ceb2a5275bdff8667d/client_packages/scripts/changeDetect.js
//  https://github.com/LucasRitter/ragemp-kotlin-client/tree/master/src/mp

export class CharacterHud {
    public static Create(): CharacterHud {
        const instance = new CharacterHud();
        instance.onInit();
        return instance;
    }

    static getSharedObject(name: string, defaultValue?: any): any {
        const data = mp.players.local.getVariable(name);
        if (data) {
            return data;
        }
        return defaultValue;
    }

    onInit(): void {
        this.init();

        this.subscribePropertyChanges('Hunger');
        this.subscribePropertyChanges('Thirst');

        this.subscribePropertyChanges('Health');
        this.subscribePropertyChanges('Armour');

        this.subscribePropertyChanges('Money');
        this.subscribePropertyChanges('DonatePoints');

        this.subscribePropertyChanges('Name');
        this.subscribePropertyChanges('Fraction');

        setInterval(() => this.onUpdateLocation, 2000);

        mp.events.add('render', () => this.onDisplayMicrophoneState());
        mp.events.add('render', () => this.onDisableControlAction());
        mp.events.add('render', () => this.onDisplayLoocking());
    }

    private onUpdateLocation() {
        const player = mp.players.local;
        const streetHash = mp.game.pathfind.getStreetNameAtCoord(player.position.x, player.position.y, player.position.z, 0, 0);
        const streetName = encodeURIComponent(mp.game.ui.getStreetNameFromHashKey(streetHash.streetName));
        this.onPropertyChanges(mp.players.local, 'StreetName', streetName, '');
    }

    private subscribePropertyChanges(propertyName: string): void {
        mp.events.addDataHandler(propertyName, (entity: EntityMp, value: any, oldValue: any) => this.onPropertyChanges(entity, propertyName, value, oldValue));
    }

    private onPropertyChanges(entity: EntityMp, propertyName: string, value: any, oldValue: any): void {
        mp.gui.chat.push(`[${entity.type}]: ${propertyName}: ${value}`);
        if (entity.type === 'player') {
            AngularBrowser.Instance.callAngularEvent('Player.HUD.Update', {
                propertyName,
                value: value ? value.toString() : '',
                oldValue: oldValue ? oldValue.toString() : ''
            });
        }
    }

    private init(): void {
        toggleGameHud(false);

        mp.gui.cursor.show(true, true);
        mp.game.ui.setPauseMenuActive(false);
    }

    private onDisplayMicrophoneState(): void {
        mp.players.forEachInStreamRange(player => {
            if (player !== mp.players.local && player.isVoiceActive) {
                const pos = player.getBoneCoords(12844, 0.5, 0, 0);
                mp.game.graphics.drawText('MIC', [pos.x, pos.y, pos.z + 0.30], {
                    font: 4,
                    color: [255, 0, 0, 255],
                    scale: [0.25, 0.25],
                    outline: true,
                    centre: true
                });
            }
        });
    }

    private onDisplayLoocking(): void {
        if (mp.players.local.isDead()) {
            return;
        }

        if (mp.players.local.isOnVehicle() || mp.players.local.isInFlyingVehicle() || mp.players.local.isSittingInAnyVehicle()) {
            return;
        }

        const lookingAt = getLookingAtEntity(TracePointToPointFlags.Vehicles);
        if (lookingAt) {
            if (lookingAt.type === 'vehicle') {
                mp.game.graphics.drawText('Для посадки в транспорт\nИспользуйте \'F\' или \'G\'', [lookingAt.position.x, lookingAt.position.y, lookingAt.position.z], {
                    font: 0,
                    color: [255, 255, 255, 100],
                    scale: [0.22, 0.22],
                    outline: true,
                    centre: true
                });
            }
        }
    }

    private onDisableControlAction(): void {
        mp.game.controls.disableControlAction(0, GameControls.INPUT_THROW_GRENADE, true);
        mp.game.controls.disableControlAction(0, GameControls.INPUT_FRONTEND_PAUSE, true);

        mp.game.controls.disableControlAction(2, GameControls.INPUT_VEH_CIN_CAM, true);
        mp.game.controls.disableControlAction(2, GameControls.INPUT_RELOAD, true);

        mp.game.controls.disableControlAction(2, GameControls.INPUT_WEAPON_WHEEL_UD, true);
        mp.game.controls.disableControlAction(2, GameControls.INPUT_WEAPON_WHEEL_LR, true);
        mp.game.controls.disableControlAction(2, GameControls.INPUT_WEAPON_WHEEL_PREV, true);

        mp.game.controls.disableControlAction(2, GameControls.INPUT_SELECT_NEXT_WEAPON, true);
        mp.game.controls.disableControlAction(2, GameControls.INPUT_SELECT_PREV_WEAPON, true);

        mp.game.controls.disableControlAction(2, GameControls.INPUT_SELECT_WEAPON, true);

        mp.game.controls.disableControlAction(2, GameControls.INPUT_VEH_SELECT_NEXT_WEAPON, true);
        mp.game.controls.disableControlAction(2, GameControls.INPUT_VEH_SELECT_PREV_WEAPON, true);


        mp.game.controls.disableControlAction(2, GameControls.INPUT_SELECT_WEAPON_UNARMED, true);
        mp.game.controls.disableControlAction(2, GameControls.INPUT_SELECT_WEAPON_MELEE, true);
        mp.game.controls.disableControlAction(2, GameControls.INPUT_SELECT_WEAPON_HANDGUN, true);
        mp.game.controls.disableControlAction(2, GameControls.INPUT_SELECT_WEAPON_SHOTGUN, true);
        mp.game.controls.disableControlAction(2, GameControls.INPUT_SELECT_WEAPON_SMG, true);
        mp.game.controls.disableControlAction(2, GameControls.INPUT_SELECT_WEAPON_AUTO_RIFLE, true);
        mp.game.controls.disableControlAction(2, GameControls.INPUT_SELECT_WEAPON_SNIPER, true);
        mp.game.controls.disableControlAction(2, GameControls.INPUT_SELECT_WEAPON_HEAVY, true);
        mp.game.controls.disableControlAction(2, GameControls.INPUT_SELECT_WEAPON_SPECIAL, true);

        mp.game.controls.disableControlAction(2, GameControls.INPUT_PREV_WEAPON, true);
        mp.game.controls.disableControlAction(2, GameControls.INPUT_NEXT_WEAPON, true);

        mp.game.ui.hideHudComponentThisFrame(GameHUDComponent.HUD_WANTED_STARS);
        mp.game.ui.hideHudComponentThisFrame(GameHUDComponent.HUD_WEAPON_ICON);
        mp.game.ui.hideHudComponentThisFrame(GameHUDComponent.HUD_CASH);
        mp.game.ui.hideHudComponentThisFrame(GameHUDComponent.HUD_MP_CASH);
        mp.game.ui.hideHudComponentThisFrame(GameHUDComponent.HUD_CASH_CHANGE);
        mp.game.ui.hideHudComponentThisFrame(GameHUDComponent.HUD_RADIO_STATIONS);
        mp.game.ui.hideHudComponentThisFrame(GameHUDComponent.HUD_WEAPON_WHEEL);
        mp.game.ui.hideHudComponentThisFrame(GameHUDComponent.HUD_WEAPON_WHEEL_STATS);
        mp.game.ui.hideHudComponentThisFrame(GameHUDComponent.MAX_HUD_WEAPONS);

    }

}
