import {BasicScaleform, ScaleformInstructionalButtonStyle} from './Scaleform';
import {getControlActionName, hexToRGB} from '../Common/extension-methods';

type HudManagerBgColor = Array3d | string | RGBA | RGB;

interface HudManagerButton {
    title: string;
    control: string;
}

export class HudManager {
    private readonly hud: BasicScaleform = new BasicScaleform('instructional_buttons');
    private readonly buttons: HudManagerButton[] = [];

    constructor(
        private style: ScaleformInstructionalButtonStyle,
        // bgColor accepts HEX and RGBA
        private bgColor: HudManagerBgColor
    ) {
        mp.events.add('render', () => {
            this.hud.renderFullscreen();
        });

        if (style) {
            this.changeStyle(style);
        }

        if (bgColor) {
            this.setBackgroundColor(bgColor);
        }

        this.resetBar();
    }

    changeStyle(style: ScaleformInstructionalButtonStyle): void {
        this.style = style;
        this.hud.drawInstructionalButtons(this.style);
    }

    setBackgroundColor(bgColor: HudManagerBgColor) {
        if (bgColor) {
            if (Array.isArray(bgColor)) {
                this.hud.callFunction('SET_BACKGROUND_COLOUR', bgColor[0], bgColor[1], bgColor[2], bgColor[3]);
            } else if (bgColor.match(/#[0-9A-Fa-f]{6}/)) {
                const color = hexToRGB(bgColor.replace('#', ''));
                this.hud.callFunction('SET_BACKGROUND_COLOUR', color[0], color[1], color[2], 180);
            } else {
                mp.gui.chat.push('!{orange}[WARNING] !{white}Invalid color given. Make sure it suits as specified in resource\'s description');
            }
        }
        this.hud.drawInstructionalButtons(this.style);
    }

    addButton(title: string, controlID: number): void {
        const control = getControlActionName(controlID);
        if (control) {
            this.hud.callFunction('SET_DATA_SLOT', this.buttons.length, control, title);
            this.buttons.push({
                control,
                title
            });

            this.hud.drawInstructionalButtons(this.style);
        }
    }

    toggleHud(state: boolean): void {
        this.hud.toggle(state);
        if (state) {
            this.hud.drawInstructionalButtons(this.style);
        }
    }

    resetBar() {
        this.hud.clearAll();
        this.hud.toggleMouseButtons(true);
        this.hud.createContainer();
        this.hud.callFunction('SET_CLEAR_SPACE', 100);
    }
}
