import {AngularBrowser} from '../Common/angular-browser';
import {KeyboardKeyCodes} from '../Enums/keyboard-key-codes';
import {WorldPlayer} from '../world-player';
import {EventManager} from '../Common/event-manager';
import {Bool} from '../Models/Boolean';
import {OperationResponse} from '../Models/operation-response';

export class UiManager {
    public static readonly Instance = UiManager.Create();

    public canShowWindow = true;
    private freezeControls = true;

    private static Create(): UiManager {
        const instance = new UiManager();
        instance.onInit();
        return instance;
    }

    public openWindow(url: string): boolean {
        if (this.canShowWindow) {
            this.canShowWindow = false;
            AngularBrowser.Instance.callAngularEvent('Browser.NavigateTo', url);

            mp.gui.cursor.show(true, true);
            // mp.gui.chat.show(false);
            // mp.gui.chat.activate(false);
            mp.gui.cursor.show(true, true);
            mp.gui.cursor.visible = true;

            setTimeout(() => {
                mp.gui.cursor.show(true, true);
                mp.gui.cursor.visible = true;
            }, 1000);


            return true;
        }

        return false;
    }

    private onInit(): void {
        mp.events.add('CEF:WindowClosed', () => this.onWindowClosed());
        EventManager.bindEventWithType<Bool>(Bool, 'Player:HUD:Toggle', (toggle) => this.onTogglePlayerHUD(toggle));

        mp.keys.bind(KeyboardKeyCodes.I, true, () => this.onClickedShowInventory());
        mp.keys.bind(KeyboardKeyCodes.VK_OEM_3, true, () => this.onToggleMouseCursor());
    }

    private onToggleMouseCursor(): void {
        mp.gui.cursor.visible = !mp.gui.cursor.visible;
    }

    private onWindowClosed() {
        this.canShowWindow = true;

        mp.gui.cursor.show(false, false);
        mp.gui.chat.show(true);
        mp.gui.chat.activate(true);
    }

    private onClickedShowInventory(): void {
        if (WorldPlayer.isAuthorized() && WorldPlayer.isLive()) {
            // this.openWindow('Inventory/Player');
        }
    }

    private onTogglePlayerHUD(toggle: OperationResponse<Bool>) {
        if (toggle.isCorrect) {
            AngularBrowser.Instance.callAngularEvent('Player.HUD.Toggle', toggle.content.value);
        }
    }
}
