import {AngularBrowser} from '../Common/angular-browser';
import {KeyboardKeyCodes} from '../Enums/keyboard-key-codes';
import {HudManager} from './hudManager';
import {isNullOrUndefined} from '../Common/extension-methods';
import {EventManager} from '../Common/event-manager';
import {Bool} from '../Models/Boolean';
import {OperationResponse} from '../Models/operation-response';

export class VehicleEntity {
    constructor(public readonly vehicle: VehicleMp) {
    }

    public toggleEngine(): void {
    }
}

export class VehicleHud {
    private readonly vehicleHelpKeys = new HudManager(-1, null);
    private readonly angular = AngularBrowser.Instance;
    private lastSpeedValue: number = null;
    private lastFuelValue: number = null;

    private lightsEnabled = false;

    public static Create(): VehicleHud {
        const instance = new VehicleHud();
        instance.onInit();
        return instance;
    }

    private getPlayer(): PlayerMp | null {
        return mp.players.local;
    }

    private getVehicle(): VehicleMp | null {
        return mp.players.local && mp.players.local.vehicle;
    }

    private isInVehicle(): boolean {
        return mp.players.local && mp.players.local.vehicle != null;
    }

    private onInit(): void {
        EventManager.bindEventWithType<Bool>(Bool, 'Vehicle:ToggleEngine', (toggle) => this.onToggleEngineResponse(toggle));
        EventManager.bindEventWithType<Bool>(Bool, 'Vehicle:ToggleSeatBelt', (toggle) => this.onToggleSeatBeltResponse(toggle));

        mp.events.add('render', () => this.onRender());
        mp.events.add('playerEnteredVehicle', (vehicleId, seat) => this.onPlayerEnterVehicle(vehicleId, seat));
        mp.events.add('playerLeftVehicle', () => this.onPlayerLeaveVehicle());

        mp.events.add('playerDeath', () => this.onPlayerLeaveVehicle());
        mp.events.add('playerSpawn', () => this.onPlayerLeaveVehicle());

        mp.keys.bind(KeyboardKeyCodes.L, true, () => this.onToggleLight());
        mp.keys.bind(KeyboardKeyCodes.E, true, () => this.onClickedToggleEngine());
        mp.keys.bind(KeyboardKeyCodes.NumTop2, true, () => this.onClickedToggleEngine());
        mp.keys.bind(KeyboardKeyCodes.NumTop3, true, () => this.onClickedToggleBelt());
    }

    private toggleSpeedometer(enabled: boolean): void {
        this.angular.callAngularEvent('Vehicle.HUD.Speedometer.View.Toggle', enabled);
    }

    private onToggleSeatBeltResponse(toggle: OperationResponse<Bool>): void {
        if (this.isInVehicle()) {
            if (toggle.isCorrect) {
                this.toggleSeatBelt(toggle.content.value);
            }
        }
    }

    private onToggleEngineResponse(toggle: OperationResponse<Bool>): void {
        if (toggle.isCorrect) {
            this.getVehicle().setEngineOn(toggle.content.value, false, false);
            this.angular.callAngularEvent('Vehicle.HUD.Speedometer.Engine.Update', toggle.content.value);
        }
    }

    private onClickedToggleBelt(): void {
        if (this.isInVehicle()) {
            mp.events.callRemote('Vehicle:ToggleSeatBelt');
        }
    }

    private onClickedToggleEngine() {
        if (this.isInVehicle()) {
            mp.events.callRemote('Vehicle:ToggleEngine');
        }
    }

    private onToggleLight() {
        if (this.isInVehicle()) {
            this.angular.callAngularEvent('Vehicle.HUD.Speedometer.Light.Update', this.lightsEnabled);
        }
    }

    private onPlayerEnterVehicle(vehicleId: number, seatId: number): void {
        // ====================
        const vehicle = mp.vehicles.at(vehicleId);
        mp.players.local.setConfigFlag(429, true);
        vehicle.setUndriveable(true);

        // ====================
        const isVehicleDriverSeat = seatId <= 0;
        this.toggleSpeedometer(isVehicleDriverSeat);
        this.toggleSeatBelt(false);

        // ====================
        const engineState = vehicle.getVariable('EngineState');
        if (engineState) {
            vehicle.setEngineOn(true, true, false);
            vehicle.setUndriveable(false);
        } else {
            vehicle.setEngineOn(false, true, false);
            vehicle.setUndriveable(true);

            const numKey2 = 158;
            const numKey3 = 159;
            this.vehicleHelpKeys.addButton('Для того что бы завести / заглушить двигатель нажмите ', numKey2);
            this.vehicleHelpKeys.addButton('Для того что бы пристегнуться нажмите ', numKey3);
            this.vehicleHelpKeys.toggleHud(true);
            setTimeout(() => this.vehicleHelpKeys.toggleHud(false), 2000);
        }
    }

    private onPlayerLeaveVehicle(): void {
        this.toggleSeatBelt(false);
        this.toggleSpeedometer(false);
    }

    private toggleSeatBelt(toggle: boolean): void {
        mp.players.local.setConfigFlag(32, toggle);
    }

    private onRender(): void {
        if (this.isInVehicle()) {
            const currentSpeed = this.getVehicle().getSpeed();
            if (currentSpeed !== this.lastSpeedValue) {
                this.lastSpeedValue = currentSpeed;
                this.angular.callAngularEvent('Vehicle.HUD.Speedometer.Speed.Update', currentSpeed);
            }

            const currentFuel = this.getVehicle().getVariable('Fuel');
            if (currentFuel !== this.lastFuelValue) {
                this.lastFuelValue = currentFuel;
                this.angular.callAngularEvent('Vehicle.HUD.Speedometer.Fuel.Update', currentFuel);
            }
        }
    }
}
