export type ScaleformInstructionalButtonStyle = 1 | -1;

export class BasicScaleform {
    private readonly handle: number;
    private enabled: boolean;

    public constructor(scaleformName) {
        this.handle = mp.game.graphics.requestScaleformMovie(scaleformName);
        while (!mp.game.graphics.hasScaleformMovieLoaded(this.handle)) {
            mp.game.wait(5);
        }
    }

    public clearAll(): void {
        this.callFunction('CLEAR_ALL');
    }


    public createContainer(): void {
        this.callFunction('CREATE_CONTAINER');
    }

    public toggleMouseButtons(toggle: boolean): void {
        this.callFunction('TOGGLE_MOUSE_BUTTONS', toggle);
    }

    public drawInstructionalButtons(style: ScaleformInstructionalButtonStyle): void {
        this.callFunction('DRAW_INSTRUCTIONAL_BUTTONS', style);
    }

    public callFunction(functionName, ...args) {
        mp.game.graphics.pushScaleformMovieFunction(this.handle, functionName);
        args.forEach(arg => {
            switch (typeof arg) {
                case 'string': {
                    mp.game.graphics.pushScaleformMovieFunctionParameterString(arg);
                    break;
                }

                case 'boolean': {
                    mp.game.graphics.pushScaleformMovieFunctionParameterBool(arg);
                    break;
                }

                case 'number': {
                    if (Number(arg) === arg && arg % 1 !== 0) {
                        mp.game.graphics.pushScaleformMovieFunctionParameterFloat(arg);
                    } else {
                        mp.game.graphics.pushScaleformMovieFunctionParameterInt(arg);
                    }
                }
            }
        });

        mp.game.graphics.popScaleformMovieFunctionVoid();
    }

    public drawScaleform(x: number, y: number, width: number, height: number, red: number, green: number, blue: number, alpha: number, p9: number): void {
        mp.game.graphics.drawScaleformMovie(this.handle, x, y, width, height, red, green, blue, alpha, p9);
    }

    public toggle(toggle: boolean): void {
        this.enabled = toggle;
    }

    public renderFullscreen(): void {
        if (this.enabled) {
            mp.game.graphics.drawScaleformMovieFullscreen(this.handle, 255, 255, 255, 255, false);
        }
    }

    public dispose() {
        mp.game.graphics.setScaleformMovieAsNoLongerNeeded(this.handle);
    }
}
