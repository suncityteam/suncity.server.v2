export interface CharacterCreationPlayerNameForm {
  firstName: string;
  latName: string;
}

export interface CharacterCreationGeneralInformationForm {
  fatherId: number;

  motherId: number;

  sex: number;

  similarity: number;

  skin: number;

  ageing: number;

  sundamage: number;

  freckles: number;
}

export interface CharacterCreationFacialFeaturesForm {

  noseWidth: number;
  noseHeight: number;
  noseLength: number;
  noseBridge: number;
  noseTip: number;
  noseShift: number;

  browHeight: number;
  browWidth: number;

  cheekboneHeight: number;
  cheekboneWidth: number;
  cheeksWidth: number;

  eyes: number;
  lips: number;

  jawWidth: number;
  jawHeight: number;

  chinLength: number;
  chinPosition: number;
  chinWidth: number;
  chinShape: number;

  neckWidth: number;
}

export interface CharacterCreationHairAndColorForm {
  hair: number;
  hairColor: number;

  eyebrows: number;
  eyebrowsColor: number;

  eyeColor: number;

  lipstick: number;
  lipstickColor: number;

  beard: number;
  beardColor: number;
}

export interface CharacterCreationForm {
  playerName: CharacterCreationPlayerNameForm;
  character: CharacterCreationGeneralInformationForm;
  facialFeatures: CharacterCreationFacialFeaturesForm;
  hairs: CharacterCreationHairAndColorForm;
}

export const enum CharacterCreationView {
    /** Общий план*/
    Character,

    /** Лицо в близи*/
    Face,
}

