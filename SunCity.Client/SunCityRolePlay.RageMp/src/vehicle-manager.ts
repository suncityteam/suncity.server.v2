import {VehicleWindowStates} from './Enums/Vehicle/VehicleWindowStates';
import {VehicleWindowId} from './Enums/Vehicle/VehicleWindowId';
import {VehicleDoorId, VehicleDoorState} from './Enums/Vehicle/VehicleDoorState';
import {isNullOrUndefined} from './Common/extension-methods';
import {VehicleIndicatorLights} from './Enums/Vehicle/VehicleIndicatorLights';

export class VehicleManager {

    public static Create(): VehicleManager {
        const instance = new VehicleManager();
        // instance.onInit();
        return instance;
    }

    constructor() {
        mp.events.add('entityStreamIn', (entity) => this.onVehicleStreamIn(entity));

        mp.events.addDataHandler('EngineState', (entity: VehicleMp, state: boolean) => this.onEngineStateChanged(entity, state));
        mp.events.addDataHandler('LightsState', (entity: VehicleMp, state: boolean) => this.onLightsStateChanged(entity, state));
        // mp.events.addDataHandler('LockedDoors', (entity: VehicleMp, state: boolean) => this.onDoorsStateChanged(entity, state));
        mp.events.addDataHandler('SirenSound', (entity: VehicleMp, state: boolean) => this.onSirenSoundStateChanged(entity, state));
        mp.events.addDataHandler('Nitro', (entity: VehicleMp, state: boolean) => this.onNitroStateChanged(entity, state));

        mp.events.addDataHandler('LeftLightIndicator', (entity: VehicleMp, state: boolean) => this.onLightsIndicatorsChanged(entity, state, VehicleIndicatorLights.Left));
        mp.events.addDataHandler('RightLightIndicator', (entity: VehicleMp, state: boolean) => this.onLightsIndicatorsChanged(entity, state, VehicleIndicatorLights.Right));

        mp.events.addDataHandler('DirtyLevel', (entity: VehicleMp, value: number) => this.onDirtyLevelChanged(entity, value));
        mp.events.addDataHandler('BodyHealth', (entity: VehicleMp, value: number) => this.onBodyHealthChanged(entity, value));
        mp.events.addDataHandler('EngineHealth', (entity: VehicleMp, value: number) => this.onEngineHealthChanged(entity, value));

        mp.events.addDataHandler('WindowFrontRight', (entity: VehicleMp, state: VehicleWindowStates) => this.onWindowStateChanged(entity, state, VehicleWindowId.WindowFrontRight));
        mp.events.addDataHandler('WindowFrontLeft', (entity: VehicleMp, state: VehicleWindowStates) => this.onWindowStateChanged(entity, state, VehicleWindowId.WindowFrontLeft));
        mp.events.addDataHandler('WindowRearRight', (entity: VehicleMp, state: VehicleWindowStates) => this.onWindowStateChanged(entity, state, VehicleWindowId.WindowRearRight));
        mp.events.addDataHandler('WindowRearLeft', (entity: VehicleMp, state: VehicleWindowStates) => this.onWindowStateChanged(entity, state, VehicleWindowId.WindowRearLeft));

        mp.events.addDataHandler('DoorFrontLeft', (entity: VehicleMp, state: VehicleDoorState) => this.onDoorsStateChanged(entity, state, VehicleDoorId.DoorFrontLeft));
        mp.events.addDataHandler('DoorFrontRight', (entity: VehicleMp, state: VehicleDoorState) => this.onDoorsStateChanged(entity, state, VehicleDoorId.DoorFrontRight));

        mp.events.addDataHandler('DoorRearLeft', (entity: VehicleMp, state: VehicleDoorState) => this.onDoorsStateChanged(entity, state, VehicleDoorId.DoorRearLeft));
        mp.events.addDataHandler('DoorRearRight', (entity: VehicleMp, state: VehicleDoorState) => this.onDoorsStateChanged(entity, state, VehicleDoorId.DoorRearRight));

        mp.events.addDataHandler('DoorHood', (entity: VehicleMp, state: VehicleDoorState) => this.onDoorsStateChanged(entity, state, VehicleDoorId.DoorHood));
        mp.events.addDataHandler('DoorTrunk', (entity: VehicleMp, state: VehicleDoorState) => this.onDoorsStateChanged(entity, state, VehicleDoorId.DoorTrunk));
    }

    private onVehicleStreamIn(entity: EntityMp) {
        if (entity && entity.type === 'vehicle' && mp.vehicles.exists(entity.id)) {
            const vehicle = entity as VehicleMp;
            mp.game.streaming.requestCollisionAtCoord(vehicle.position.x, vehicle.position.y, vehicle.position.z);
            vehicle.setLoadCollisionFlag(true);
            vehicle.trackVisibility();
            vehicle.setInvincible(false);

            this.onEngineStateChanged(vehicle, vehicle.getVariable('EngineState'));
            this.onDirtyLevelChanged(vehicle, vehicle.getVariable('DirtyLevel'));

            this.onBodyHealthChanged(vehicle, vehicle.getVariable('BodyHealth'));
            this.onEngineHealthChanged(vehicle, vehicle.getVariable('EngineHealth'));

            this.onDoorsStateChanged(vehicle, vehicle.getVariable('DoorFrontLeft'), VehicleDoorId.DoorFrontLeft);
            this.onDoorsStateChanged(vehicle, vehicle.getVariable('DoorFrontRight'), VehicleDoorId.DoorFrontRight);

            this.onDoorsStateChanged(vehicle, vehicle.getVariable('DoorRearLeft'), VehicleDoorId.DoorRearLeft);
            this.onDoorsStateChanged(vehicle, vehicle.getVariable('DoorRearRight'), VehicleDoorId.DoorRearRight);

            this.onDoorsStateChanged(vehicle, vehicle.getVariable('DoorHood'), VehicleDoorId.DoorHood);
            this.onDoorsStateChanged(vehicle, vehicle.getVariable('DoorTrunk'), VehicleDoorId.DoorTrunk);


            this.onWindowStateChanged(vehicle, vehicle.getVariable('WindowFrontRight'), VehicleWindowId.WindowFrontRight);
            this.onWindowStateChanged(vehicle, vehicle.getVariable('WindowFrontLeft'), VehicleWindowId.WindowFrontLeft);

            this.onWindowStateChanged(vehicle, vehicle.getVariable('WindowRearLeft'), VehicleWindowId.WindowRearLeft);
            this.onWindowStateChanged(vehicle, vehicle.getVariable('WindowRearRight'), VehicleWindowId.WindowRearRight);
        }
    }

    private onEngineStateChanged(entity: VehicleMp, state: boolean) {
        const engineEnabled = !!state;
        entity.setEngineOn(engineEnabled, true, true);
        entity.setUndriveable(!engineEnabled);
    }

    private onLightsStateChanged(entity: VehicleMp, state: boolean) {
        // entity.setLights(state);
    }

    private onDirtyLevelChanged(entity: VehicleMp, value: number) {
        entity.setDirtLevel(value);
    }

    private onBodyHealthChanged(entity: VehicleMp, value: number) {
        entity.setBodyHealth(value);
    }

    private onEngineHealthChanged(entity: VehicleMp, value: number) {
        entity.setEngineHealth(value);
    }

    private onWindowStateChanged(entity: VehicleMp, state: VehicleWindowStates, window: VehicleWindowId) {
        if (state === VehicleWindowStates.WindowBroken) {
            entity.removeWindow(window);
        } else if (state === VehicleWindowStates.WindowFixed) {
            entity.fixWindow(window);
        } else if (state === VehicleWindowStates.WindowDown) {
            entity.rollDownWindow(window);
        } else {
            entity.rollUpWindow(window);
        }
    }

    private onDoorsStateChanged(entity: VehicleMp, state: VehicleDoorState, door: VehicleDoorId) {
        if (state === VehicleDoorState.DoorBroken) {
            entity.setDoorBroken(door, true);
        } else if (state === VehicleDoorState.DoorClosed) {
            entity.setDoorShut(door, true);
        } else {
            entity.setDoorOpen(door, false, false);
        }
    }

    private onSirenSoundStateChanged(entity: VehicleMp, state: boolean) {
        entity.setSiren(state);
    }

    private onNitroStateChanged(entity: VehicleMp, state: boolean) {
        //  TODO: https://pawno-info.ru/threads/316892-Synced-Nitro-1-0-0
    }

    private onLightsIndicatorsChanged(entity: VehicleMp, state: boolean, indicator: VehicleIndicatorLights) {
        entity.setIndicatorLights(indicator, state);
    }
}
