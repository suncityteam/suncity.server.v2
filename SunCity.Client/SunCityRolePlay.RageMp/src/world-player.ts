import {WorldEntity} from './world-entity';
import {isNullOrUndefined} from './Common/extension-methods';

export class WorldPlayer extends WorldEntity {
    public static readonly instance = new WorldPlayer();

    public static getSharedObject<TValue>(Name: string, Default?: TValue | undefined): TValue | undefined {
        const data = mp.players.local.getVariable(Name);
        if (isNullOrUndefined(data)) {
            return Default;
        }
        return data;
    }

    constructor() {
        super();
    }

    public static isDead(): boolean {
        return mp.players.local.isDead();
    }

    public static isLive(): boolean {
        return !mp.players.local.isDead();
    }

    public static isInAnyVehicle(): boolean {
        return mp.players.local.isSittingInAnyVehicle();
    }

    public static isAuthorized(): boolean {
        const value = WorldPlayer.getSharedObject('IsAuthorized', false);
        return !!value;
    }
}
