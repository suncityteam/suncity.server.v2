export function toggleGameHud(enabled: boolean): void {
    //  cash always hidden
    mp.game.ui.displayCash(false);
    mp.game.ui.displayAreaName(enabled);

    mp.game.ui.displayRadar(enabled);
    mp.game.ui.displayHud(enabled);

    mp.gui.chat.show(enabled);
    mp.gui.chat.activate(enabled);
}

export function configureWorldPlayerCharacter(): void {
    mp.game.player.setHealthRechargeMultiplier(0);
    mp.game.player.disableVehicleRewards();
    // mp.game.player.setTargetingMode(1);

    mp.players.local.clearLastDamage();
    mp.players.local.resetVisibleDamage();
    // mp.players.local.resetStamina();
    // mp.players.local.removeHelmet(true);

    mp.players.local.removeAllWeapons();
    // mp.players.local.setLockon(false);
    // mp.players.local.setLockonRangeOverride(0.0);
    mp.players.local.setCanBeDamaged(true);
    mp.players.local.setOnlyDamagedByPlayer(false);
    mp.players.local.setProofs(true, false, false, false, false, true, true, false);

    mp.game.ui.displayRadar(true);
    mp.game.ui.displayHud(true);
    mp.gui.chat.show(true);
    mp.gui.chat.activate(true);
    mp.gui.cursor.show(false, false);


    //
    mp.game.gameplay.setFadeInAfterDeathArrest(false);
    mp.game.gameplay.setFadeOutAfterDeath(false);
    mp.game.gameplay.setFadeInAfterLoad(false);
    mp.game.vehicle.defaultEngineBehaviour = true;
    mp.nametags.enabled = false;
}
