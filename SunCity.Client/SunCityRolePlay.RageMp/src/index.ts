import {AngularBrowser} from './Common/angular-browser';
import {CharacterCreation} from './character-creation';
import {CharacterSelection} from './character-selection';
import {CharacterHud} from './HUD/character-hud';
import {VehicleHud} from './HUD/vehicle-hud';
import {PropertyManager} from './Property/property-manager';
import {VehicleManager} from './vehicle-manager';
import {VehicleShowRoomPropertyManager} from './Property/VehicleShowRoom/VehicleShowRoomPropertyManager';

AngularBrowser.Create();

CharacterSelection.Create();
CharacterCreation.Create();

CharacterHud.Create();
VehicleHud.Create();
PropertyManager.Create();
VehicleManager.Create();
VehicleShowRoomPropertyManager.Create();
