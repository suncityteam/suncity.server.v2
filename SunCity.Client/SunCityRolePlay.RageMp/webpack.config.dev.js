const path = require("path");

module.exports = {
    devtool: 'source-map',
    entry: {
        "rageclient.min": "./src/index.ts",
    },
    output: {
        path: path.join(__dirname, "./out"),
        filename: "index.js",
        sourceMapFilename: 'index.map'
    },
    resolve: {
        extensions: ['.ts', '.tsx', '.js']
    },
    plugins: [ ],
    module: {
        rules: [
            { test: /\.tsx?$/, loader: 'ts-loader' }
        ]
    }
};
