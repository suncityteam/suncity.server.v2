import {IOperationError} from './GameHttpClient';
import {Action} from '@ngrx/store';
import {TypedAction} from '@ngrx/store/src/models';

export interface IOperationResponse {
  isCorrect: boolean;
  isNotCorrect: boolean;
  error: IOperationError;
  content?: any;
}

export function mapOperationResponse(operation: IOperationResponse, successfully: any, failed: any) {
  if (operation) {
    if (operation.isCorrect) {
      return successfully(operation.content);
    }
    return failed(operation.error);
  }
  return failed();
}
