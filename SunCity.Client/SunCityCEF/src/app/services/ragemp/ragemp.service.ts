import {EventEmitter, Injectable} from '@angular/core';
import {ToastrService} from 'ngx-toastr';
import {Observable, Subject} from 'rxjs';

declare function callServerEvent(...args);

declare function callClientEvent(...args);

export type IRAGEServiceCallback = (response: string | number | object) => void;

class RageMPServiceEvent {
  callback: Subject<ControllerResponse<any>> = new Subject<ControllerResponse<any>>();
  callbackObs = this.callback.asObservable();

  constructor(public readonly  name: string) {
  }


}

export function RageEventResponse(eventName: string): (target: any, propertyName: string) => void {
  return (target: any, propertyName: string): void => {
    // значение свойства
    let val = target[propertyName];

    // Удаляем то, что уже находится в поле
    if (delete target[propertyName]) {
      // Создаем новое поле с геттером и сеттером
      Object.defineProperty(target, propertyName, {
        get: () => val,
        set: (newVal) => {
          const subj = new Subject<ControllerResponse<any>>();
          RageMPService.getService().subscribe(eventName, subj);
          val = subj.asObservable();
        },
        enumerable: true,
        configurable: true
      });
    }
  };
}

@Injectable()
export class RageMPService {

  constructor(private readonly toastr: ToastrService) {
    RageMPService.service = this;
  }

  private static service: RageMPService | undefined = undefined;
  private readonly events: RageMPServiceEvent[] = [];

  public static getService(): RageMPService {
    if (!RageMPService.service) {
      throw new Error('RageMPService not initialized');
    }
    return RageMPService.service;
  }

  public onCallCustomEvents(rageEvent: ControllerBrowserResponse): void {
    const event = this.events.find(q => q.name === rageEvent.eventName);
    if (event) {
      event.callback.next(rageEvent.response);
    }
  }

  public subscribe<T>(eventName: string, callback: Subject<ControllerResponse<T>>): void {
    // this.toastr.info(`subscribe: ${eventName}`);
    let event: RageMPServiceEvent = this.events.find(q => q.name === eventName);
    if (event == null) {
      event = new RageMPServiceEvent(eventName);
      this.events.push(event);
    }

    event.callbackObs.subscribe(callback);
  }

  public callClientEvent(eventName: string, content?: any) {
    const json = JSON.stringify(content);

    this.toastr.info(json, `callClientEvent: ${eventName}`, {
      timeOut: 7000
    });

    callClientEvent(eventName, json);
  }

  public callServerEvent(eventName: string, content?: any, callback?: IRAGEServiceCallback): void {

    const json = JSON.stringify(content);

    this.toastr.info(json, `callServerEvent: ${eventName}`, {
      timeOut: 7000
    });

    callServerEvent(eventName, json);
  }
}

export interface ControllerBrowserResponse {
  response: ControllerResponse<any>;
  eventName: string;
  requestId: string;
}

export interface ControllerResponseBasic {
  status: string;
  paramName: string;
}


export interface ControllerResponse<T> extends ControllerResponseBasic {
  content: T;
}
