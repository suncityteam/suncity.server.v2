import {Component, HostListener} from '@angular/core';
import {ControllerBrowserResponse, ControllerResponse, RageEventResponse, RageMPService} from './ragemp.service';
import {ToastrService} from 'ngx-toastr';
import {SessionStorage} from 'ngx-store-9';
import {Observable} from 'rxjs';
import {Router} from '@angular/router';

@Component({
  selector: 'app-ragemp-proxy-component',
  template: ''
})
export class RageMPServiceComponent {
  @SessionStorage()
  private rageClientId: number;

  @RageEventResponse('Browser.NavigateTo')
  private readonly onNavigateToEvent = new Observable<ControllerResponse<string>>();

  constructor(
    private readonly service: RageMPService,
    private readonly toastr: ToastrService,
    private readonly router: Router
  ) {
    this.onNavigateToEvent.subscribe(res => this.onNavigateTo(res));
  }

  @HostListener('window:callCustomRageEvent', ['$event'])
  protected onCallCustomEvents(event: CustomEvent): void {
    const json = atob(event.detail);
    try {
      const rageEvent = JSON.parse(json) as ControllerBrowserResponse;
      this.service.onCallCustomEvents(rageEvent);
    } catch (e) {
      this.toastr.success(`onCallCustomEvents: ${e.toString()}`);
    }
  }

  @HostListener('window:initRageClientId', ['$event'])
  protected initRageClientId(event: CustomEvent): void {
    this.toastr.success(`initRageClientId: ${event.detail}`);
    this.rageClientId = event.detail;
  }

  private onNavigateTo(res: ControllerResponse<string>) {
    this.toastr.success(`onNavigateTo: ${res.content}`);
    this.router.navigate([res.content]);
  }
}
