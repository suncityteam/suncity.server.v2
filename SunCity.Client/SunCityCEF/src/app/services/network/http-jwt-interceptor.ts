import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {HttpEvent, HttpHandler, HttpInterceptor, HttpRequest} from '@angular/common/http';
import {LocalStorage, SessionStorage} from 'ngx-store-9';

@Injectable()
export class HttpJwtInterceptor implements HttpInterceptor {
  @SessionStorage()
  private rageClientId: number;

  @LocalStorage()
  private readonly jwt: string;

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    request = request.clone({
      setHeaders: {
        Authorization: `Bearer ${this.jwt}`,
        RageMp: this.rageClientId ? this.rageClientId.toString() : '-1',
        // UseUserMock: 'true'
      }
    });
    return next.handle(request);
  }
}
