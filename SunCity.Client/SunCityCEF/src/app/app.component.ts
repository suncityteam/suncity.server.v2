import {Component, OnInit, ViewEncapsulation} from '@angular/core';
import {NgxUiLoaderService} from 'ngx-ui-loader';
import {PlayerHudService} from './modules/hud/services/player/player-hud.service';
import {VehicleHUDService} from './modules/hud/services/vehicle/vehicle-hud.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class AppComponent implements OnInit {
  title = 'SunCityRolePlay';

  constructor(
    private ngxService: NgxUiLoaderService,
    playerHudService: PlayerHudService,
    vehicleHUDService: VehicleHUDService
  ) {
    playerHudService.init();
    vehicleHUDService.init();
  }

  ngOnInit() {
    this.ngxService.start(); // start foreground spinner of the master loader with 'default' taskId
    // Stop the foreground loading after 5s
    setTimeout(() => {
      this.ngxService.stop(); // stop foreground spinner of the master loader with 'default' taskId
    }, 5000);

  }

}
