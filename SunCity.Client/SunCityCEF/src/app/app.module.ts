import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppComponent} from './app.component';
import {RouterModule, Routes} from '@angular/router';
import {AuthorizationModule} from './modules/authorization/authorization.module';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {ToastrModule} from 'ngx-toastr';
import {SharedModule} from './modules/shared/shared.module';
import {InventoryModule} from './modules/inventory/inventory.module';
import {HUDModule} from './modules/hud/hud.module';
import {WorldPropertySharedModule} from './modules/world/property/shared/world-property-shared.module';
import {HTTP_INTERCEPTORS, HttpClientModule} from '@angular/common/http';
import {WebStorageModule} from 'ngx-store-9';
import {HttpJwtInterceptor} from './services/network/http-jwt-interceptor';
import {GameCostPipe} from './modules/shared/pipes/game-cost/game-cost.pipe';
import {WorldPropertyBusinessBankModule} from './modules/world/property/business/bank/world-property-business-bank.module';
import {WorldPropertyBusinessVehicleShowRoomModule} from './modules/world/property/business/vehicle-showroom/world-property-business-vehicle-showroom.module';
import { StoreRootModule} from '@ngrx/store';
import {StoreDevtoolsModule} from '@ngrx/store-devtools';
import {StoreRouterConnectingModule} from '@ngrx/router-store';
import {ApplicationStoreModule} from './store/application-store-module';
import {API_BASE_URL} from './http/GameHttpClient';

export const AppRoutes: Routes = [
  {
    path: 'ng/auth',
    loadChildren: './modules/authorization/authorization.module#AuthorizationModule'
  },
];

// @ts-ignore
@NgModule({
  declarations: [
    AppComponent,
  ],
  imports: [
    BrowserModule,

    ApplicationStoreModule,

    StoreDevtoolsModule.instrument(),
    StoreRootModule,
    StoreRouterConnectingModule.forRoot({stateKey: 'router'}),

    HttpClientModule,
    WebStorageModule,
    AuthorizationModule.forRoot(),
    InventoryModule,
    RouterModule.forRoot(AppRoutes),
    BrowserAnimationsModule,
    ToastrModule.forRoot(),
    SharedModule.forRoot(),
    WorldPropertySharedModule,
    WorldPropertyBusinessBankModule,
    WorldPropertyBusinessVehicleShowRoomModule,
    HUDModule,
  ],
  exports: [
    SharedModule,
    HttpClientModule,
    WebStorageModule,
  ],
  providers: [
    GameCostPipe,
    {provide: HTTP_INTERCEPTORS, useClass: HttpJwtInterceptor, multi: true},
    {provide: API_BASE_URL, useValue: 'http://localhost:5000'},

  ], bootstrap: [AppComponent]
})

export class AppModule {
}
