import {Injectable} from '@angular/core';
import {ControllerResponse, RageEventResponse, RageMPService} from '../../../../services/ragemp/ragemp.service';
import {Observable} from 'rxjs';
import {Store} from '@ngrx/store';
import {GameHudStoreState} from '../../store/game-hud-store-state';
import {GameHudStoreActions} from '../../store/game-hud-store-actions';

@Injectable({
  providedIn: 'root'
})
export class VehicleHUDService {
  @RageEventResponse('Vehicle.HUD.Speedometer.Speed.Update')
  public readonly onSpeedometerUpdateSpeed = new Observable<ControllerResponse<number>>();

  @RageEventResponse('Vehicle.HUD.Speedometer.Fuel.Update')
  public readonly onSpeedometerUpdateFuel = new Observable<ControllerResponse<number>>();


  @RageEventResponse('Vehicle.HUD.Speedometer.Light.Update')
  public readonly onSpeedometerUpdateLight = new Observable<ControllerResponse<boolean>>();

  @RageEventResponse('Vehicle.HUD.Speedometer.Engine.Update')
  public readonly onSpeedometerUpdateEngine = new Observable<ControllerResponse<boolean>>();

  @RageEventResponse('Vehicle.HUD.Speedometer.View.Toggle')
  public readonly onSpeedometerToggleView = new Observable<ControllerResponse<boolean>>();

  public init() {
    // Its normal, required for force init service
  }

  constructor(
    rage: RageMPService,
    store$: Store<GameHudStoreState>,
  ) {
    this.onSpeedometerToggleView.subscribe(response => store$.dispatch(GameHudStoreActions.TOGGLE_SPEEDOMETER({toggle: response.content})));

  }
}
