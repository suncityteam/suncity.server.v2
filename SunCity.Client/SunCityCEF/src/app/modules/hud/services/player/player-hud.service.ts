import {Injectable} from '@angular/core';
import {ControllerResponse, RageEventResponse, RageMPService} from '../../../../services/ragemp/ragemp.service';
import {Observable} from 'rxjs';
import {PlayerDataObject} from '../../models/Player/Progress/PlayerProgressModel';
import {Store} from '@ngrx/store';
import {GameHudStoreState} from '../../store/game-hud-store-state';
import {GameHudStoreActions} from '../../store/game-hud-store-actions';
import {ToastrService} from 'ngx-toastr';

@Injectable({
  providedIn: 'root'
})
export class PlayerHudService {
  @RageEventResponse('Player.HUD.Update')
  public readonly onUpdatePlayerHUD = new Observable<ControllerResponse<PlayerDataObject<any>>>();

  @RageEventResponse('Player.HUD.Toggle')
  public readonly onTogglePlayerHUD = new Observable<ControllerResponse<boolean>>();

  public init() {
    // Its normal, required for force init service
  }

  constructor(
    rage: RageMPService,
    store$: Store<GameHudStoreState>,
  ) {
    this.onTogglePlayerHUD.subscribe(response => {
      store$.dispatch(GameHudStoreActions.TOGGLE_HUD({toggle: response.content}));
    });

  }
}
