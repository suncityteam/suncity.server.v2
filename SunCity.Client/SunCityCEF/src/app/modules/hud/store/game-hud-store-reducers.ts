import {Action, createReducer, on} from '@ngrx/store';
import {GameHudStoreState} from './game-hud-store-state';
import {GameHudStoreActions} from './game-hud-store-actions';

export const initialState: GameHudStoreState = {
  toggleChat: false,
  toggleHud: false,
  toggleSpeedometer: false
};

export function getInitialState(): GameHudStoreState {
  return initialState;
}

const Reducer = createReducer(getInitialState(),
  on(GameHudStoreActions.TOGGLE_CHAT, (state, {toggle}) => ({...state, toggleChat: toggle})),
  on(GameHudStoreActions.TOGGLE_HUD, (state, {toggle}) => ({...state, toggleHud: toggle})),
  on(GameHudStoreActions.TOGGLE_SPEEDOMETER, (state, {toggle}) => ({...state, toggleSpeedometer: toggle})),
);

export default function GameHudStoreReducers(state: GameHudStoreState | undefined, action: Action) {
  return Reducer(state, action);
}
