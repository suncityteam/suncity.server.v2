import {createFeatureSelector, createSelector} from '@ngrx/store';
import {GameHudStoreModuleName} from './game-hud-store-actions';
import {GameHudStoreState} from './game-hud-store-state';

export class GameHudStoreSelectors {
  static readonly store = createFeatureSelector<GameHudStoreState>(GameHudStoreModuleName);

  static readonly toggleChat = createSelector(GameHudStoreSelectors.store, (state: GameHudStoreState): boolean => state.toggleChat);
  static readonly toggleHud = createSelector(GameHudStoreSelectors.store, (state: GameHudStoreState): boolean => state.toggleHud);
  static readonly toggleSpeedometer = createSelector(GameHudStoreSelectors.store, (state: GameHudStoreState): boolean => state.toggleSpeedometer);
}
