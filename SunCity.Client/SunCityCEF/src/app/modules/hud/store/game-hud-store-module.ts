import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {StoreModule} from '@ngrx/store';
import {EffectsModule} from '@ngrx/effects';

import {GameHudStoreModuleName} from './game-hud-store-actions';
import {GameHudStoreEffects} from './game-hud-store-effects';
import GameHudStoreReducers from './game-hud-store-reducers';

@NgModule({
  imports: [
    CommonModule,
    StoreModule.forFeature(GameHudStoreModuleName, GameHudStoreReducers),
    // EffectsModule.forFeature([GameHudStoreEffects]),
  ],
  // providers: [GameHudStoreEffects]
})
export class GameHudStoreModule { }
