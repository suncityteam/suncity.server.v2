import {createAction, props} from '@ngrx/store';

export const GameHudStoreModuleName = 'GameHud';

export class GameHudStoreActions {
  public static readonly TOGGLE_CHAT = createAction('TOGGLE_CHAT', props<{ toggle: boolean }>());
  public static readonly TOGGLE_SPEEDOMETER = createAction('TOGGLE_SPEEDOMETER', props<{ toggle: boolean }>());
  public static readonly TOGGLE_HUD = createAction('TOGGLE_HUD', props<{ toggle: boolean }>());

}
