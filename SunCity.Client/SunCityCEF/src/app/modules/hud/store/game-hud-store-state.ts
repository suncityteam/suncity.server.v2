export interface GameHudStoreState {
  toggleChat: boolean;
  toggleHud: boolean;
  toggleSpeedometer: boolean;
}
