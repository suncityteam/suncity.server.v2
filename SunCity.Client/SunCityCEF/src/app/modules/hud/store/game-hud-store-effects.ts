import {Injectable} from '@angular/core';
import {Actions, Effect, ofType} from '@ngrx/effects';
import {select, Store} from '@ngrx/store';
import {GameHudStoreState} from './game-hud-store-state';

@Injectable()
export class GameHudStoreEffects {
  constructor(
    private readonly actions$: Actions,
    private readonly store$: Store<GameHudStoreState>,
  ) {
  }

  /*@Effect()
  initialize$ = this.actions$.pipe(
    ofType(VehicleShowRoomActions.INITIALIZE),
    mergeMap(() => of(VehicleShowRoomActions.INITIALIZE_SUCCESSFULLY()))
  );*/

}
