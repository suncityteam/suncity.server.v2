import {Component, OnInit} from '@angular/core';
import {GameHudStoreSelectors} from '../../../store/game-hud-store-selectors';
import {Store} from '@ngrx/store';
import {GameHudStoreState} from '../../../store/game-hud-store-state';
import {PlayerHudService} from '../../../services/player/player-hud.service';

@Component({
  selector: 'app-player-game-hud',
  templateUrl: './player-game-hud.component.html',
  styleUrls: ['./player-game-hud.component.scss']
})
export class PlayerGameHudComponent implements OnInit {

  public readonly toggleHud$ = this.store$.select(GameHudStoreSelectors.toggleHud);

  constructor(
    private readonly store$: Store<GameHudStoreState>) {
  }

  ngOnInit() {
  }

}
