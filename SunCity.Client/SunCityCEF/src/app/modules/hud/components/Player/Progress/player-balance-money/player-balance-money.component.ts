import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-player-balance-money',
  templateUrl: './player-balance-money.component.html',
  styleUrls: ['./player-balance-money.component.scss']
})
export class PlayerBalanceMoneyComponent implements OnInit {

  @Input()
  public amount: number;

  constructor() {
  }

  ngOnInit() {
  }

}
