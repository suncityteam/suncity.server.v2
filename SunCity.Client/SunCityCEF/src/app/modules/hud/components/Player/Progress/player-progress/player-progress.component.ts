import {ChangeDetectorRef, Component, OnInit} from '@angular/core';
import {PlayerDataObject, PlayerProgressModel} from '../../../../models/Player/Progress/PlayerProgressModel';
import {ControllerResponse} from '../../../../../../services/ragemp/ragemp.service';
import {PlayerHudService} from '../../../../services/player/player-hud.service';
import {ToastrService} from 'ngx-toastr';

@Component({
  selector: 'app-player-progress',
  templateUrl: './player-progress.component.html',
  styleUrls: ['./player-progress.component.scss']
})
export class PlayerProgressComponent implements OnInit {
  public model: PlayerProgressModel;

  constructor(
    private readonly service: PlayerHudService,
    private readonly toastr: ToastrService,
    private readonly ref: ChangeDetectorRef) {
    this.model = {
      thirst: 10,
      satiety: 0,
      armour: 0,
      location: null,
      name: null,
      fraction: null,
      health: 0,
      money: 0,
      donate: 0
    };

    this.service.onUpdatePlayerHUD.subscribe(res => this.onUpdatePlayerHUD(res));
  }

  ngOnInit() {
  }

  private onUpdatePlayerHUD(res: ControllerResponse<PlayerDataObject<any>>) {
    this.toastr.success(JSON.stringify(res));

    if (res.content.propertyName === 'Money') {
      this.model.money = res.content.value;
    }

    if (res.content.propertyName === 'DonatePoints') {
      this.model.donate = res.content.value;
    }

    if (res.content.propertyName === 'Health') {
      this.model.health = res.content.value;
    }

    if (res.content.propertyName === 'Armour') {
      this.model.armour = res.content.value;
    }

    if (res.content.propertyName === 'Thirst') {
      this.model.thirst = res.content.value;
    }

    if (res.content.propertyName === 'Hunger') {
      this.model.satiety = res.content.value;
    }

    if (res.content.propertyName === 'StreetName') {
      this.model.location = decodeURIComponent(res.content.value);
    }

    if (res.content.propertyName === 'Name') {
      this.model.name = decodeURIComponent(res.content.value);
    }

    /*if (!this.model.fraction && (res.content.fraction || res.content.fraction.length < 1)) {
      this.model.fraction = 'Гражданин';
    }*/
  }
}
