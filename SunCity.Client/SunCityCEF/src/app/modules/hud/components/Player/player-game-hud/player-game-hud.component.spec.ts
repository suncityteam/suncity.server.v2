import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PlayerGameHudComponent } from './player-game-hud.component';

describe('PlayerGameHudComponent', () => {
  let component: PlayerGameHudComponent;
  let fixture: ComponentFixture<PlayerGameHudComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PlayerGameHudComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PlayerGameHudComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
