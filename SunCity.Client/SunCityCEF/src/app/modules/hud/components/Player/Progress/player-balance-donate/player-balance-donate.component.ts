import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-player-balance-donate',
  templateUrl: './player-balance-donate.component.html',
  styleUrls: ['./player-balance-donate.component.scss']
})
export class PlayerBalanceDonateComponent implements OnInit {
  @Input()
  public amount: number;

  constructor() {
  }

  ngOnInit() {
  }

}
