import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PlayerBalanceMoneyComponent } from './player-balance-money.component';

describe('PlayerBalanceMoneyComponent', () => {
  let component: PlayerBalanceMoneyComponent;
  let fixture: ComponentFixture<PlayerBalanceMoneyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PlayerBalanceMoneyComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PlayerBalanceMoneyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
