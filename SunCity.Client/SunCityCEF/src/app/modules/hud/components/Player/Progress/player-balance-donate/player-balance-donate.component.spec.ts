import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PlayerBalanceDonateComponent } from './player-balance-donate.component';

describe('PlayerBalanceDonateComponent', () => {
  let component: PlayerBalanceDonateComponent;
  let fixture: ComponentFixture<PlayerBalanceDonateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PlayerBalanceDonateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PlayerBalanceDonateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
