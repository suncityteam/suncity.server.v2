import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VehicleGameHudComponent } from './vehicle-game-hud.component';

describe('VehicleGameHudComponent', () => {
  let component: VehicleGameHudComponent;
  let fixture: ComponentFixture<VehicleGameHudComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VehicleGameHudComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VehicleGameHudComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
