import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CarSpeedometerComponent } from './car-speedometer.component';

describe('CarSpeedometerComponent', () => {
  let component: CarSpeedometerComponent;
  let fixture: ComponentFixture<CarSpeedometerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CarSpeedometerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CarSpeedometerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
