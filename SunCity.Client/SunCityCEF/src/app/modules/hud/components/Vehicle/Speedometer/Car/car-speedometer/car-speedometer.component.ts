import {Component, OnInit} from '@angular/core';
import {ToastrService} from 'ngx-toastr';
import {VehicleHUDService} from '../../../../../services/vehicle/vehicle-hud.service';
import {ControllerResponse} from '../../../../../../../services/ragemp/ragemp.service';
import {IStatelessIconic} from '../../../../../../shared/components/icons/stateless-iconic/stateless-iconic.component';


@Component({
  selector: 'app-car-speedometer',
  templateUrl: './car-speedometer.component.html',
  styleUrls: ['./car-speedometer.component.scss']
})
export class CarSpeedometerComponent implements OnInit {
  public readonly iconEngine: IStatelessIconic = {
    0: 'mdi mdi-engine-off',
    1: 'mdi mdi-engine'
  };

  public readonly iconLights: IStatelessIconic = {
    0: 'mdi mdi-car-light-dimmed',
    1: 'mdi mdi-car-light-dimmed mdi-car-light-enabled'
  };

  public readonly iconDoors: IStatelessIconic = {
    0: 'mdi mdi-car-door',
    1: 'mdi mdi-car-door-lock'
  };

  public engineEnabled: boolean;
  public lightsEnabled: boolean;
  public doorsLocked: boolean;

  public speed: number;
  public fuel: number;

  constructor(private readonly service: VehicleHUDService) {
    this.speed = 0;
    this.service.onSpeedometerUpdateSpeed.subscribe(response => this.onUpdateSpeed(response));
    this.service.onSpeedometerUpdateFuel.subscribe(response => this.onUpdateFuel(response));

    this.service.onSpeedometerUpdateEngine.subscribe(response => this.onToggleEngine(response));
    this.service.onSpeedometerUpdateLight.subscribe(response => this.onToggleLights(response));
  }

  ngOnInit() {
  }

  private onToggleEngine(response: ControllerResponse<boolean>) {
    this.engineEnabled = response.content;
  }

  private onToggleLights(response: ControllerResponse<boolean>) {
    this.lightsEnabled = response.content;
  }

  public onUpdateSpeed(response: ControllerResponse<number>) {
    this.speed = response.content * 3.6;
  }

  private onUpdateFuel(response: ControllerResponse<number>) {
    this.fuel = response.content;
  }
}
