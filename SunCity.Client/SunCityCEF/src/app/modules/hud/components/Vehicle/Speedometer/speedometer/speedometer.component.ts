import {Component, HostListener, OnInit} from '@angular/core';
import {Store} from '@ngrx/store';
import {GameHudStoreState} from '../../../../store/game-hud-store-state';
import {GameHudStoreSelectors} from '../../../../store/game-hud-store-selectors';

@Component({
  selector: 'app-speedometer',
  templateUrl: './speedometer.component.html',
  styleUrls: ['./speedometer.component.scss']
})
export class SpeedometerComponent implements OnInit {
  public readonly toggleSpeedometer$ = this.store$.select(GameHudStoreSelectors.toggleSpeedometer);

  constructor(
    private readonly store$: Store<GameHudStoreState>) {
  }

  ngOnInit() {
  }
}
