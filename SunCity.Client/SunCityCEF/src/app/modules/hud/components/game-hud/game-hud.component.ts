import {Component, OnInit} from '@angular/core';

@Component({
  selector: 'app-game-hud',
  templateUrl: './game-hud.component.html',
  styleUrls: ['./game-hud.component.scss']
})
export class GameHUDComponent implements OnInit {

  public get isAllowDisplayVehicleHUD(): boolean {
    return true;
  }

  public get isAllowDisplayPlayerHUD(): boolean {
    return true;
  }

  constructor() {
  }

  ngOnInit() {
  }

}
