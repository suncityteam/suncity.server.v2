export interface PlayerDataObject<T> {
  readonly propertyName: string;
  readonly oldValue?: T;
  readonly value: T;
}

export interface PlayerProgressModel {

  name: string;
  fraction: string;
  location: string;

  health: number;
  armour: number;

  thirst: number;
  satiety: number;

  money: number;
  donate: number;
}
