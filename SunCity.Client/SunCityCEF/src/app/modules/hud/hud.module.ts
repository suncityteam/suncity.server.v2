import {NgModule} from '@angular/core';
import {CommonModule, DecimalPipe} from '@angular/common';
import {CarSpeedometerComponent} from './components/Vehicle/Speedometer/Car/car-speedometer/car-speedometer.component';
import { PlayerProgressComponent } from './components/Player/Progress/player-progress/player-progress.component';
import {NgbProgressbarModule} from '@ng-bootstrap/ng-bootstrap';
import { GameHUDComponent } from './components/game-hud/game-hud.component';
import {SharedModule} from '../shared/shared.module';
import { PlayerBalanceMoneyComponent } from './components/Player/Progress/player-balance-money/player-balance-money.component';
import { PlayerBalanceDonateComponent } from './components/Player/Progress/player-balance-donate/player-balance-donate.component';
import { PlayerGameHudComponent } from './components/Player/player-game-hud/player-game-hud.component';
import { VehicleGameHudComponent } from './components/Vehicle/vehicle-game-hud/vehicle-game-hud.component';
import { SpeedometerComponent } from './components/Vehicle/Speedometer/speedometer/speedometer.component';
import {GameHudStoreModule} from './store/game-hud-store-module';

@NgModule({
  declarations: [
    CarSpeedometerComponent,
    PlayerProgressComponent,
    GameHUDComponent,
    PlayerBalanceMoneyComponent,
    PlayerBalanceDonateComponent,
    PlayerGameHudComponent,
    VehicleGameHudComponent,
    SpeedometerComponent,
  ],
  exports: [
    CarSpeedometerComponent,
    PlayerProgressComponent,
    GameHUDComponent,
    DecimalPipe,
  ],
  imports: [
    CommonModule,
    NgbProgressbarModule,
    SharedModule,
    GameHudStoreModule,
  ]
})
export class HUDModule {
}
