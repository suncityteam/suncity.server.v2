import {ModuleWithProviders, NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {AuthorizationPageComponent} from './components/authorization/authorization-page/authorization-page.component';
import {AuthorizationLoginComponent} from './components/authorization/authorization-login/authorization-login.component';
import {AuthorizationRegisterComponent} from './components/authorization/authorization-register/authorization-register.component';
import {AuthorizationForgotPasswordComponent} from './components/authorization/authorization-forgot-password/authorization-forgot-password.component';
import {RouterModule, Routes} from '@angular/router';
import {ReactiveFormsModule} from '@angular/forms';
import {RageMPService} from '../../services/ragemp/ragemp.service';
import {RageMPServiceComponent} from '../../services/ragemp/rage-m-p-service.component';
import {CharacterSelectionPageComponent} from './components/character-selection/character-selection-page/character-selection-page.component';
import {CharacterSelectionSpawnComponent} from './components/character-selection-spawn/character-selection-spawn.component';
import {CharacterSelectionItemComponent} from './components/character-selection/character-selection-item/character-selection-item.component';
import {CharacterSelectionCreateFreeComponent} from './components/character-selection/character-selection-create-free/character-selection-create-free.component';
import {CharacterSelectionCreatePaymentComponent} from './components/character-selection/character-selection-create-payment/character-selection-create-payment.component';
import {CharacterSelectionCreateFirstComponent} from './components/character-selection/character-selection-create-first/character-selection-create-first.component';
import {SharedModule} from '../shared/shared.module';
import {NgxUiLoaderModule} from 'ngx-ui-loader';
import { CharacterCreationPageComponent } from './components/character-creation/character-creation-page/character-creation-page.component';
import { CharacterCreationGeneralComponent } from './components/character-creation/character-creation-general/character-creation-general.component';
import { CharacterCreationAppearanceComponent } from './components/character-creation/character-creation-appearance/character-creation-appearance.component';
import { CharacterCreationHairAndColorComponent } from './components/character-creation/character-creation-hair-and-color/character-creation-hair-and-color.component';
import { CharacterCreationFacialFeaturesComponent } from './components/character-creation/character-creation-facial-features/character-creation-facial-features.component';
import { CharacterCreationClothesComponent } from './components/character-creation/character-creation-clothes/character-creation-clothes.component';
import { AuthorizationLatestNewsComponent } from './components/authorization/authorization-latest-news/authorization-latest-news.component';
import { AuthorizationWindowComponent } from './components/authorization/authorization-window/authorization-window.component';
import { AuthorizationStatisticsComponent } from './components/authorization/authorization-statistics/authorization-statistics.component';

export const AuthRoutes: Routes = [
  {
    path: 'authorization',
    component: AuthorizationPageComponent
  },
  {
    path: 'Authorization/Character/Create',
    component: CharacterCreationPageComponent
  },
  {
    path: 'Authorization/Character/Select',
    component: CharacterSelectionPageComponent
  },
  {
    path: 'Authorization/Character/SelectSpawn',
    component: CharacterSelectionSpawnComponent
  },
];


@NgModule({
  declarations:
    [
      RageMPServiceComponent,
      AuthorizationPageComponent,
      AuthorizationLoginComponent,
      AuthorizationRegisterComponent,
      AuthorizationForgotPasswordComponent,
      CharacterSelectionPageComponent,
      CharacterSelectionSpawnComponent,
      CharacterSelectionItemComponent,
      CharacterSelectionCreateFreeComponent,
      CharacterSelectionCreatePaymentComponent,
      CharacterSelectionCreateFirstComponent,
      CharacterCreationPageComponent,
      CharacterCreationGeneralComponent,
      CharacterCreationAppearanceComponent,
      CharacterCreationHairAndColorComponent,
      CharacterCreationFacialFeaturesComponent,
      CharacterCreationClothesComponent,
      AuthorizationLatestNewsComponent,
      AuthorizationWindowComponent,
      AuthorizationStatisticsComponent,
    ],
  imports: [
    CommonModule,
    RouterModule.forChild(AuthRoutes),
    ReactiveFormsModule,

    SharedModule
  ],
  exports: [
    RageMPServiceComponent
  ],
  providers: [RageMPService]
})
export class AuthorizationModule {
  static forRoot(): ModuleWithProviders {
    return {
      ngModule: AuthorizationModule,
      providers: []
    };
  }

}
