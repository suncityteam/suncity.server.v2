import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CharacterSelectionSpawnComponent } from './character-selection-spawn.component';

describe('CharacterSelectionSpawnComponent', () => {
  let component: CharacterSelectionSpawnComponent;
  let fixture: ComponentFixture<CharacterSelectionSpawnComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CharacterSelectionSpawnComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CharacterSelectionSpawnComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
