import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CharacterCreationClothesComponent } from './character-creation-clothes.component';

describe('CharacterCreationClothesComponent', () => {
  let component: CharacterCreationClothesComponent;
  let fixture: ComponentFixture<CharacterCreationClothesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CharacterCreationClothesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CharacterCreationClothesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
