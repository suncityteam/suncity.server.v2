import {Component, OnInit} from '@angular/core';
import {CharacterCreationPlayerNameFormAdapter} from './character-creation-player-name-form.adapter';
import {NgbTabChangeEvent} from '@ng-bootstrap/ng-bootstrap';
import {Router} from '@angular/router';
import {ToastrService} from 'ngx-toastr';
import {CharacterCreationHairAndColorFormAdapter} from '../character-creation-hair-and-color/character-creation-hair-and-color-form.adapter';
import {CharacterCreationGeneralFormAdapter} from '../character-creation-general/character-creation-general-form.adapter';
import {CharacterCreationFacialFeaturesAdapter} from '../character-creation-facial-features/character-creation-facial-features.adapter';
import {CharacterService} from '../../../services/character.service';
import {CharacterHttpClient, CreateCharacterRequest, PlayerCharacterIdOperationResponse} from '../../../../../http/GameHttpClient';
import {CharacterCreationView} from '../../../../../../../ragemp/Forms/character-creation-form';

@Component({
  selector: 'app-character-creation-page',
  templateUrl: './character-creation-page.component.html',
  styleUrls: ['./character-creation-page.component.scss']
})
export class CharacterCreationPageComponent implements OnInit {
  public readonly HairAndColor = new CharacterCreationHairAndColorFormAdapter();
  public readonly General = new CharacterCreationGeneralFormAdapter();
  public readonly Facial = new CharacterCreationFacialFeaturesAdapter();
  public readonly Name = new CharacterCreationPlayerNameFormAdapter();

  public isValidForm(): boolean {
    return this.HairAndColor.isValid && this.General.isValid && this.Facial.isValid && this.Name.isValid;
  }

  constructor(
    private readonly characterService: CharacterService,
    private readonly characterHttpClient: CharacterHttpClient,
    private readonly router: Router,
    private readonly toasts: ToastrService
  ) {
  }

  public onTabChange($event: NgbTabChangeEvent): void {
    if ($event.nextId === 'tab-character-creation-facial-features') {
      this.characterService.onClientCreateCharacterSwitchView(CharacterCreationView.Face);
    } else {
       this.characterService.onClientCreateCharacterSwitchView(CharacterCreationView.Character);
    }
  }

  public onSubmitForm(): void {
    if (this.isValidForm) {
      const form = new CreateCharacterRequest({
        character: this.General.mapToForm(),
        facialFeatures: this.Facial.mapToForm(),
        hairs: this.HairAndColor.mapToForm(),
        name: this.Name.mapToForm()
      });
      this.characterHttpClient.create(form).subscribe(res => this.onCreateCharacterResponse(res));
    } else {
      this.toasts.error('Некорректно заполнена информация о персонаже');
    }
  }

  public onReturnToCharacterSelection() {
    this.characterService.onClientCreateCharacterComplete();
    this.router.navigate(['/Authorization/Character/Select']);
  }

  ngOnInit() {
    this.characterService.onClientCreateCharacterBegin();
  }

  private onCreateCharacterResponse(res: PlayerCharacterIdOperationResponse) {
    if (res.isCorrect) {
      this.toasts.error('Поздравляем, вы создали нового персонажа.', 'Создание персонажа');
      this.router.navigate(['/']);
    } else {
      this.toasts.error(res.error.message, 'Не удалось создать персонажа');
    }
  }
}
