import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {CharacterCreationHairAndColorFormAdapter} from './character-creation-hair-and-color-form.adapter';
import {BaseComponent} from '../../../../shared/components/form-groups/form-group-adapter/form-group-adapter';

@Component({
  selector: 'app-character-creation-hair-and-color',
  templateUrl: './character-creation-hair-and-color.component.html',
  styleUrls: ['./character-creation-hair-and-color.component.scss']
})
export class CharacterCreationHairAndColorComponent extends BaseComponent<CharacterCreationHairAndColorFormAdapter> implements OnInit {

  constructor() {
    super();
  }

  ngOnInit() {
  }

}
