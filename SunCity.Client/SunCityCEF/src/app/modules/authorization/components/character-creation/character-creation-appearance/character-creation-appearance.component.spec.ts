import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CharacterCreationAppearanceComponent } from './character-creation-appearance.component';

describe('CharacterCreationAppearanceComponent', () => {
  let component: CharacterCreationAppearanceComponent;
  let fixture: ComponentFixture<CharacterCreationAppearanceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CharacterCreationAppearanceComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CharacterCreationAppearanceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
