import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CharacterCreationHairAndColorComponent } from './character-creation-hair-and-color.component';

describe('CharacterCreationHairAndColorComponent', () => {
  let component: CharacterCreationHairAndColorComponent;
  let fixture: ComponentFixture<CharacterCreationHairAndColorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CharacterCreationHairAndColorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CharacterCreationHairAndColorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
