import {Component, OnInit, Output} from '@angular/core';
import {CharacterCreationFacialFeaturesAdapter} from './character-creation-facial-features.adapter';
import {BaseComponent} from '../../../../shared/components/form-groups/form-group-adapter/form-group-adapter';
import {CharacterCreationHairAndColorFormAdapter} from '../character-creation-hair-and-color/character-creation-hair-and-color-form.adapter';

@Component({
  selector: 'app-character-creation-facial-features',
  templateUrl: './character-creation-facial-features.component.html',
  styleUrls: ['./character-creation-facial-features.component.scss']
})
export class CharacterCreationFacialFeaturesComponent extends BaseComponent<CharacterCreationFacialFeaturesAdapter> implements OnInit {

  constructor() {
    super();
  }

  ngOnInit() {
  }

}
