import {FormGroupAdapter} from '../../../../shared/components/form-groups/form-group-adapter/form-group-adapter';
import {Control} from '../../../../shared/components/form-groups/form-group-adapter/form-group-control';
import {FormControl} from '@angular/forms';
import {CreateCharacterFacialFeatures} from '../../../../../http/GameHttpClient';

/** Facial features option */
export class CharacterCreationFacialFeaturesAdapter extends FormGroupAdapter{

  @Control({value: 0.0})
  public readonly noseWidth: FormControl;

  @Control({value: 0.0})
  public readonly noseHeight: FormControl;

  @Control({value: 0.0})
  public readonly noseLength: FormControl;

  @Control({value: 0.0})
  public readonly noseBridge: FormControl;

  @Control({value: 0.0})
  public readonly noseTip: FormControl;

  @Control({value: 0.0})
  public readonly noseShift: FormControl;


  @Control({value: 0.0})
  public readonly browHeight: FormControl;

  @Control({value: 0.0})
  public readonly browWidth: FormControl;

  @Control({value: 0.0})
  public readonly cheekboneHeight: FormControl;

  @Control({value: 0.0})
  public readonly cheekboneWidth: FormControl;

  @Control({value: 0.0})
  public readonly cheeksWidth: FormControl;

  @Control({value: 0.0})
  public readonly eyes: FormControl;

  @Control({value: 0.0})
  public readonly lips: FormControl;

  @Control({value: 0.0})
  public readonly jawWidth: FormControl;

  @Control({value: 0.0})
  public readonly jawHeight: FormControl;

  @Control({value: 0.0})
  public readonly chinLength: FormControl;

  @Control({value: 0.0})
  public readonly chinPosition: FormControl;

  @Control({value: 0.0})
  public readonly chinWidth: FormControl;

  @Control({value: 0.0})
  public readonly chinShape: FormControl;

  @Control({value: 0.0})
  public readonly neckWidth: FormControl;

  public constructor() {
    super();
  }

  mapFromModel(model: any): void {
  }

  mapToForm(): CreateCharacterFacialFeatures {
    return new CreateCharacterFacialFeatures({
      noseWidth: this.noseWidth.value,
      noseHeight: this.noseHeight.value,
      noseLength: this.noseLength.value,
      noseBridge: this.noseBridge.value,
      noseTip: this.noseTip.value,
      noseShift: this.noseShift.value,

      browHeight: this.browHeight.value,
      browWidth: this.browWidth.value,

      cheekboneHeight: this.cheekboneHeight.value,
      cheekboneWidth: this.cheekboneWidth.value,
      cheeksWidth: this.cheeksWidth.value,

      eyes: this.eyes.value,
      lips: this.lips.value,

      jawWidth: this.jawWidth.value,
      jawHeight: this.jawHeight.value,

      chinLength: this.chinLength.value,
      chinPosition: this.chinPosition.value,
      chinWidth: this.chinWidth.value,
      chinShape: this.chinShape.value,

      neckWidth: this.neckWidth.value,
    });
  }

}
