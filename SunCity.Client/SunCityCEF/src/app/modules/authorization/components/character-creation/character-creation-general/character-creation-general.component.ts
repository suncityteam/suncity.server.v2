import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {CharacterParents} from '../../../../shared/data/character-parrnts';
import {CharacterCreationGeneralFormAdapter} from './character-creation-general-form.adapter';
import {BaseComponent} from '../../../../shared/components/form-groups/form-group-adapter/form-group-adapter';

@Component({
  selector: 'app-character-creation-general',
  templateUrl: './character-creation-general.component.html',
  styleUrls: ['./character-creation-general.component.scss']
})
export class CharacterCreationGeneralComponent extends BaseComponent<CharacterCreationGeneralFormAdapter> implements OnInit {
  constructor(
    public readonly parents: CharacterParents
  ) {
    super();
  }

  ngOnInit() {
  }

}
