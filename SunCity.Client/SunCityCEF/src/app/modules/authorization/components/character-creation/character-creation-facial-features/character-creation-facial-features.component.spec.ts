import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CharacterCreationFacialFeaturesComponent } from './character-creation-facial-features.component';

describe('CharacterCreationFacialFeaturesComponent', () => {
  let component: CharacterCreationFacialFeaturesComponent;
  let fixture: ComponentFixture<CharacterCreationFacialFeaturesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CharacterCreationFacialFeaturesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CharacterCreationFacialFeaturesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
