import {FormGroupAdapter} from '../../../../shared/components/form-groups/form-group-adapter/form-group-adapter';
import {FormControl, Validators} from '@angular/forms';
import {Control} from '../../../../shared/components/form-groups/form-group-adapter/form-group-control';
import {CreateCharacterPlayerName} from '../../../../../http/GameHttpClient';

export class CharacterCreationPlayerNameFormAdapter extends FormGroupAdapter {

  @Control({validators: [Validators.required, Validators.minLength(3), Validators.maxLength(18)]})
  public readonly firstName: FormControl;

  @Control({validators: [Validators.required, Validators.minLength(3), Validators.maxLength(18)]})
  public readonly lastName: FormControl;

  public constructor() {
    super();
  }

  mapToForm(): CreateCharacterPlayerName {
    return new CreateCharacterPlayerName({
      firstName: this.firstName.value,
      lastName: this.lastName.value,
    });
  }

}
