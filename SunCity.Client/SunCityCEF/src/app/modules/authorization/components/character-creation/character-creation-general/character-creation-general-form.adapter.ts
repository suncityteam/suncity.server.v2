import {FormGroupAdapter} from '../../../../shared/components/form-groups/form-group-adapter/form-group-adapter';
import {Control} from '../../../../shared/components/form-groups/form-group-adapter/form-group-control';
import {FormControl} from '@angular/forms';
import {CreateCharacterGeneralInformation} from '../../../../../http/GameHttpClient';

export class CharacterCreationGeneralFormAdapter extends FormGroupAdapter {
  public constructor() {
    super();
  }

  @Control({value: 0})
  public readonly fatherId: FormControl;

  @Control({value: 0})
  public readonly motherId: FormControl;

  @Control({value: 0})
  public readonly sex: FormControl;

  @Control({value: 0.0})
  public readonly similarity: FormControl;

  @Control({value: 0.0})
  public readonly skin: FormControl;

  @Control({value: 0.0})
  public readonly ageing: FormControl;

  @Control({value: 0.0})
  public readonly sundamage: FormControl;

  @Control({value: 0.0})
  public readonly freckles: FormControl;

  public mapToForm(): CreateCharacterGeneralInformation {
    return new CreateCharacterGeneralInformation({
      fatherId: this.fatherId.value,
      motherId: this.motherId.value,

      sex: this.sex.value,

      similarity: this.similarity.value,
      skin: this.skin.value,
      ageing: this.ageing.value,
      sundamage: this.sundamage.value,
      freckles: this.freckles.value,
    });
  }
}
