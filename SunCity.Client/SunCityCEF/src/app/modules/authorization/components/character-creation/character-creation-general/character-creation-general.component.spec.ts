import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CharacterCreationGeneralComponent } from './character-creation-general.component';

describe('CharacterCreationGeneralComponent', () => {
  let component: CharacterCreationGeneralComponent;
  let fixture: ComponentFixture<CharacterCreationGeneralComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CharacterCreationGeneralComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CharacterCreationGeneralComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
