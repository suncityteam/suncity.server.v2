import {FormGroupAdapter} from '../../../../shared/components/form-groups/form-group-adapter/form-group-adapter';
import {Control} from '../../../../shared/components/form-groups/form-group-adapter/form-group-control';
import {FormControl} from '@angular/forms';
import {CreateCharacterHairAndColor} from '../../../../../http/GameHttpClient';

export class CharacterCreationHairAndColorFormAdapter extends FormGroupAdapter {

  @Control({value: 0.0})
  public hair: FormControl;
  @Control({value: 0})
  public hairColor: FormControl;

  @Control({value: 0.0})
  public eyebrows: FormControl;
  @Control({value: 0})
  public eyebrowsColor: FormControl;

  @Control({value: 0})
  public eyeColor: FormControl;

  @Control({value: 0})
  public lipstick: FormControl;
  @Control({value: 0})
  public lipstickColor: FormControl;

  @Control({value: 0.0})
  public beard: FormControl;
  @Control({value: 0})
  public beardColor: FormControl;

  public constructor() {
    super();
  }

  mapFromModel(model: any): void {
  }

  mapToForm(): CreateCharacterHairAndColor {
    return new CreateCharacterHairAndColor({
      hair: this.hair.value,
      hairColor: this.hairColor.value,

      eyebrows: this.eyebrows.value,
      eyebrowsColor: this.eyebrowsColor.value,

      eyeColor: this.eyeColor.value,

      lipstick: this.lipstick.value,
      lipstickColor: this.lipstickColor.value,

      beard: this.eyebrows.value,
      beardColor: this.eyebrowsColor.value,

    });
  }
}
