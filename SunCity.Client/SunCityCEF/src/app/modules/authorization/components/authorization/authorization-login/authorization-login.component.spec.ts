import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AuthorizationLoginComponent } from './authorization-login.component';

describe('AuthorizationLoginComponent', () => {
  let component: AuthorizationLoginComponent;
  let fixture: ComponentFixture<AuthorizationLoginComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AuthorizationLoginComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AuthorizationLoginComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
