import {Component, HostListener, OnInit} from '@angular/core';
import {ToastrService} from 'ngx-toastr';
import {AuthorizationRegisterFormAdapter} from '../authorization-register/authorization-register-form.adapter';
import {AuthorizationLoginFormGroup} from '../authorization-login/authorization-login-form-group';

@Component({
  selector: 'app-authorization-page',
  templateUrl: './authorization-page.component.html',
  styleUrls: ['./authorization-page.component.css']
})
export class AuthorizationPageComponent implements OnInit {

  public readonly Login = new AuthorizationLoginFormGroup();
  public readonly Register = new AuthorizationRegisterFormAdapter();
  public readonly ForgotPassword = new AuthorizationRegisterFormAdapter();


//  https://codepen.io/jcoulterdesign/pen/azepmX
  constructor(private readonly toastr: ToastrService) {

  }

  ngOnInit() {
  }

}
