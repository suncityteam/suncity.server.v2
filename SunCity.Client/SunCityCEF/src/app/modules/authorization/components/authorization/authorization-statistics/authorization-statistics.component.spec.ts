import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AuthorizationStatisticsComponent } from './authorization-statistics.component';

describe('AuthorizationStatisticsComponent', () => {
  let component: AuthorizationStatisticsComponent;
  let fixture: ComponentFixture<AuthorizationStatisticsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AuthorizationStatisticsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AuthorizationStatisticsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
