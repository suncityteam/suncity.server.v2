import {Component, OnInit} from '@angular/core';
import {AuthorizationRegisterFormAdapter} from '../authorization-register/authorization-register-form.adapter';

@Component({
  selector: 'app-authorization-forgot-password',
  templateUrl: './authorization-forgot-password.component.html',
  styleUrls: ['./authorization-forgot-password.component.css']
})
export class AuthorizationForgotPasswordComponent implements OnInit {
  public readonly form = new AuthorizationRegisterFormAdapter();

  ngOnInit() {
  }

}
