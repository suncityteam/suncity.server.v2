import {FormGroupAdapter} from '../../../../shared/components/form-groups/form-group-adapter/form-group-adapter';
import {Control} from '../../../../shared/components/form-groups/form-group-adapter/form-group-control';
import {FormControl} from '@angular/forms';

export class AuthorizationLoginFormGroup extends FormGroupAdapter {
  @Control({value: 'sentike'})
  public readonly Login: FormControl;

  @Control({value: 'sentike'})
  public readonly Password: FormControl;

}
