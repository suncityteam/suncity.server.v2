import {Component, OnInit} from '@angular/core';
import {AuthorizationRegisterFormAdapter} from './authorization-register-form.adapter';
import {ToastrService} from 'ngx-toastr';
import {Router} from '@angular/router';
import {
  AuthorizationHttpClient,
  AuthorizationLoginResponseOperationResponse,
  AuthorizationRegisterRequest
} from '../../../../../http/GameHttpClient';

@Component({
  selector: 'app-authorization-register',
  templateUrl: './authorization-register.component.html',
  styleUrls: ['./authorization-register.component.css']
})
export class AuthorizationRegisterComponent implements OnInit {
  public readonly form = new AuthorizationRegisterFormAdapter();

  constructor(
    private readonly toastr: ToastrService,
    private readonly authorizationHttpClient: AuthorizationHttpClient,
    private readonly router: Router
  ) {
  }

  ngOnInit() {
  }

  public onSubmitForm() {
    if (this.form.isInvalid) {
      this.toastr.error('Некорректно заполнена форма авторизации');
      return;
    }

    const form = new AuthorizationRegisterRequest({
      login: this.form.Login.value,
      email: this.form.Email.value,
      password: this.form.Password.value,
      refererOrPromoCode: this.form.Referer.value,
    });

    this.authorizationHttpClient.register(form).subscribe(res => this.onRegisterComplete(res));
  }

  private onRegisterComplete(res: AuthorizationLoginResponseOperationResponse) {
    this.router.navigate(['/Authorization/Character/Create']);
  }
}
