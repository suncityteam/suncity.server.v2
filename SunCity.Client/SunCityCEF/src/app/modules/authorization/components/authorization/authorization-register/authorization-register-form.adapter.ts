import {FormGroupAdapter} from '../../../../shared/components/form-groups/form-group-adapter/form-group-adapter';
import {FormControl} from '@angular/forms';
import {Control} from '../../../../shared/components/form-groups/form-group-adapter/form-group-control';

export class AuthorizationRegisterFormAdapter extends FormGroupAdapter {

  @Control()
  public readonly Login: FormControl;

  @Control()
  public readonly Email: FormControl;

  @Control()
  public readonly Password: FormControl;

  @Control()
  public readonly Referer: FormControl;

  public constructor() {
    super();
  }
}
