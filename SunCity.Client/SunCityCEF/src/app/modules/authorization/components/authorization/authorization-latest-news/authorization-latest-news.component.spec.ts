import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AuthorizationLatestNewsComponent } from './authorization-latest-news.component';

describe('AuthorizationLatestNewsComponent', () => {
  let component: AuthorizationLatestNewsComponent;
  let fixture: ComponentFixture<AuthorizationLatestNewsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AuthorizationLatestNewsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AuthorizationLatestNewsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
