import {Component, OnInit} from '@angular/core';
import {ToastrService} from 'ngx-toastr';
import {Router} from '@angular/router';
import {AuthorizationLoginFormGroup} from './authorization-login-form-group';
import {
  AuthorizationHttpClient,
  AuthorizationLoginRequest,
  AuthorizationLoginResponseOperationResponse, PlayerAccountLogin
} from '../../../../../http/GameHttpClient';

@Component({
  selector: 'app-authorization-login',
  templateUrl: './authorization-login.component.html',
  styleUrls: ['./authorization-login.component.css']
})
export class AuthorizationLoginComponent implements OnInit {
  public readonly form = new AuthorizationLoginFormGroup();

  constructor(
    private readonly toastr: ToastrService,
    private readonly authorizationHttpClient: AuthorizationHttpClient,
    private readonly router: Router
  ) {
  }

  public onSubmitForm() {
    if (this.form.isInvalid) {
      this.toastr.error('Некорректно заполнена форма авторизации');
      return;
    }

    const form = new AuthorizationLoginRequest({
      login: new PlayerAccountLogin({name: this.form.Login.value}),
      password: this.form.Password.value,
    });

    this.authorizationHttpClient.login(form).subscribe(res => this.onLoginComplete(res));
  }

  private onLoginComplete(response: AuthorizationLoginResponseOperationResponse): void {

    if (response.isCorrect) {
      this.toastr.success('Добро пожаловать! Вы успешно авторизовались');
      this.router.navigate(['/Authorization/Character/Select']);
    } else {
      this.toastr.error(`onLoginComplete: ${JSON.stringify(response)}`);
    }
  }

  ngOnInit() {
  }

}
