import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AuthorizationForgotPasswordComponent } from './authorization-forgot-password.component';

describe('AuthorizationForgotPasswordComponent', () => {
  let component: AuthorizationForgotPasswordComponent;
  let fixture: ComponentFixture<AuthorizationForgotPasswordComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AuthorizationForgotPasswordComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AuthorizationForgotPasswordComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
