import {Component, Input, OnInit, Output} from '@angular/core';
import {UseCharacterStatus} from '../../../models/UseCharacterStatus';
import {Subject} from 'rxjs';

@Component({
  selector: 'app-character-selection-create-payment',
  templateUrl: './character-selection-create-payment.component.html',
  styleUrls: ['./character-selection-create-payment.component.scss']
})
export class CharacterSelectionCreatePaymentComponent implements OnInit {
  private readonly clickedSubj = new Subject<UseCharacterStatus>();

  @Input()
  public status: UseCharacterStatus;

  @Output()
  public clicked = this.clickedSubj.asObservable();

  public get Allowed() {
    return !this.status || this.status === UseCharacterStatus.Allowed ;
  }

  public get NotAllowed() {
    return this.status === UseCharacterStatus.NotAllowed;
  }

  public get RequiredPayment() {
    return this.status === UseCharacterStatus.RequiredPayment;
  }

  public onClicked() {
    this.clickedSubj.next(this.status);
  }

  constructor() {
  }

  ngOnInit() {
  }

}
