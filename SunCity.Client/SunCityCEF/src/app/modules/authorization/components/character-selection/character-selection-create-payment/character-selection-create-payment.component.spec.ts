import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CharacterSelectionCreatePaymentComponent } from './character-selection-create-payment.component';

describe('CharacterSelectionCreatePaymentComponent', () => {
  let component: CharacterSelectionCreatePaymentComponent;
  let fixture: ComponentFixture<CharacterSelectionCreatePaymentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CharacterSelectionCreatePaymentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CharacterSelectionCreatePaymentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
