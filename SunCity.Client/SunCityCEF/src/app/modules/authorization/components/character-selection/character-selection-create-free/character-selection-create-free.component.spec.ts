import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CharacterSelectionCreateFreeComponent } from './character-selection-create-free.component';

describe('CharacterSelectionCreateFreeComponent', () => {
  let component: CharacterSelectionCreateFreeComponent;
  let fixture: ComponentFixture<CharacterSelectionCreateFreeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CharacterSelectionCreateFreeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CharacterSelectionCreateFreeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
