import {Component, Input, OnInit, Output} from '@angular/core';
import {Subject} from 'rxjs';
import {UseCharacterStatus} from '../../../models/UseCharacterStatus';

@Component({
  selector: 'app-character-selection-create-first',
  templateUrl: './character-selection-create-first.component.html',
  styleUrls: ['./character-selection-create-first.component.scss']
})
export class CharacterSelectionCreateFirstComponent implements OnInit {

  private readonly clickedSubj = new Subject();

  @Output()
  public clicked = this.clickedSubj.asObservable();


  public onClicked() {
    this.clickedSubj.next();
  }

  constructor() {
  }

  ngOnInit() {
  }

}
