import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Toast, ToastrService} from 'ngx-toastr';
import {GameCharacterModel, IPlayerCharacterId} from '../../../../../http/GameHttpClient';

@Component({
  selector: 'app-character-selection-item',
  templateUrl: './character-selection-item.component.html',
  styleUrls: ['./character-selection-item.component.scss']
})
export class CharacterSelectionItemComponent implements OnInit {
  @Output()
  public clicked = new EventEmitter<IPlayerCharacterId>();

  @Input()
  public model: GameCharacterModel;

  constructor(private toasts: ToastrService) {
  }

  ngOnInit() {
  }


  public onClicked() {
    this.toasts.info(JSON.stringify(this.model));
    this.clicked.next(this.model.entityId);
  }


}
