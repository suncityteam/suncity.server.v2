import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CharacterSelectionItemComponent } from './character-selection-item.component';

describe('CharacterSelectionItemComponent', () => {
  let component: CharacterSelectionItemComponent;
  let fixture: ComponentFixture<CharacterSelectionItemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CharacterSelectionItemComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CharacterSelectionItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
