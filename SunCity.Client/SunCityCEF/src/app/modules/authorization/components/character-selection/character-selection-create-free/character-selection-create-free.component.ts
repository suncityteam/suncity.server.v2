import {Component, Input, OnInit, Output} from '@angular/core';
import {UseCharacterStatus} from '../../../models/UseCharacterStatus';
import {Observable, Subject} from 'rxjs';

@Component({
  selector: 'app-character-selection-create-free',
  templateUrl: './character-selection-create-free.component.html',
  styleUrls: ['./character-selection-create-free.component.scss']
})
export class CharacterSelectionCreateFreeComponent implements OnInit {
  private readonly clickedSubj = new Subject();

  @Input()
  public status: UseCharacterStatus;

  @Output()
  public clicked = this.clickedSubj.asObservable();

  public get Allowed() {
    return !this.status || this.status === UseCharacterStatus.Allowed ;
  }

  public get NotAllowed() {
    return this.status === UseCharacterStatus.NotAllowed;
  }

  public onClicked() {
    this.clickedSubj.next();
  }

  constructor() {
  }

  ngOnInit() {
  }

}
