import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CharacterSelectionCreateFirstComponent } from './character-selection-create-first.component';

describe('CharacterSelectionCreateFirstComponent', () => {
  let component: CharacterSelectionCreateFirstComponent;
  let fixture: ComponentFixture<CharacterSelectionCreateFirstComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CharacterSelectionCreateFirstComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CharacterSelectionCreateFirstComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
