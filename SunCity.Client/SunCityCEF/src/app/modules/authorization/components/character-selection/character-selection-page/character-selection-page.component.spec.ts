import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CharacterSelectionPageComponent } from './character-selection-page.component';

describe('CharacterSelectionPageComponent', () => {
  let component: CharacterSelectionPageComponent;
  let fixture: ComponentFixture<CharacterSelectionPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CharacterSelectionPageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CharacterSelectionPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
