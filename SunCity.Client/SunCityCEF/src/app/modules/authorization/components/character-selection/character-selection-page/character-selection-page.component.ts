import {Component, OnInit, ViewEncapsulation} from '@angular/core';
import {ToastrService} from 'ngx-toastr';
import {Router} from '@angular/router';
import {
  AuthorizationWhatToDoOperationResponse,
  AuthorizationWhatToDoOperationResponseContent,
  CharacterHttpClient,
  GetAccountInformationResponse,
  GetAccountInformationResponseOperationResponse,
  OperationResponse,
  PlayerCharacterId,
  SelectCharacterForm
} from '../../../../../http/GameHttpClient';
import {CharacterService} from '../../../services/character.service';

@Component({
  selector: 'app-character-selection-page',
  templateUrl: './character-selection-page.component.html',
  styleUrls: ['./character-selection-page.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class CharacterSelectionPageComponent implements OnInit {
  public model: GetAccountInformationResponse;

  constructor(
    private readonly toasts: ToastrService,
    private readonly router: Router,
    private readonly service: CharacterService,
    private readonly characterHttpClient: CharacterHttpClient
  ) {
  }

  ngOnInit() {
    this.characterHttpClient.whatToDo().subscribe(res => this.onWhatToDo(res));
  }

  public onSelectCharacterClicked(characterId: string) {
    const form = new SelectCharacterForm({
      playerCharacterId: new PlayerCharacterId({id: characterId}),
      useAsDefault: true
    });
    this.characterHttpClient.select(form).subscribe(res => this.onSelectCharacterResponse(res));
  }

  public onCreateCharacterClicked() {
    this.router.navigate(['/Authorization/Character/Create']);
  }

  private onGetCharacterList(response: GetAccountInformationResponseOperationResponse) {
    this.toasts.error(`onGetCharacterList: ${JSON.stringify(response)}`, 'onGetCharacterList');
    this.model = response.content;
  }

  private onSelectCharacterResponse(res: OperationResponse) {
    if (res.isCorrect) {
      this.service.onClientSelectCharacterComplete();
      this.toasts.success('Приятной игры!');
      this.router.navigate(['/']);
    } else {
      this.toasts.error(res.error.message, 'Не удалось войти в игру');
    }
  }

  private onWhatToDo(res: AuthorizationWhatToDoOperationResponse) {
    this.toasts.info(`onWhatToDo: ${JSON.stringify(res)}`);
    if (res.isCorrect) {
      if (res.content === AuthorizationWhatToDoOperationResponseContent.SelectCharacter) {
        this.characterHttpClient.getList().subscribe(r => this.onGetCharacterList(r));
      } else if (res.content === AuthorizationWhatToDoOperationResponseContent.CreateCharacter) {
        this.onCreateCharacterClicked();
      } else {
        this.service.onClientSelectCharacterComplete();
        this.toasts.success('Приятной игры!');
        this.router.navigate(['/']);
      }
    } else {
      this.toasts.error(res.error.message, 'Не удалось войти в игру');
      this.characterHttpClient.getList().subscribe(r => this.onGetCharacterList(r));
    }


  }
}
