export enum UseCharacterStatus {
  NotAllowed,
  Allowed,
  RequiredPayment
}
