import {Injectable} from '@angular/core';
import {
  RageMPService
} from '../../../services/ragemp/ragemp.service';
import {ToastrService} from 'ngx-toastr';

@Injectable({
  providedIn: 'root'
})
export class AuthorizationService {

  constructor(
    private readonly rageService: RageMPService,
    private readonly toastr: ToastrService
  ) {
  }

  public selectCharacterSpawn(spawn: number): void {
    this.rageService.callServerEvent('Player.Character.SelectSpawn', spawn);
  }

  public whatToDo(): void {
    this.rageService.callServerEvent('Player.Character.WhatToDo');
  }
}
