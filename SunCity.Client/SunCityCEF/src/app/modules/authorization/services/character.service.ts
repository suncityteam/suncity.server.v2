import {Injectable} from '@angular/core';
import {RageMPService} from '../../../services/ragemp/ragemp.service';
import {CharacterCreationForm, CharacterCreationView} from '../../../../../ragemp/Forms/character-creation-form';

@Injectable({
  providedIn: 'root'
})
export class CharacterService {

  constructor(
    private readonly rageService: RageMPService
  ) {
  }

  public onClientCreateCharacterProcess(form: CharacterCreationForm) {
    this.rageService.callClientEvent('CEF:Player.Character.Create.Process', form);
  }

  public onClientCreateCharacterBegin() {
    this.rageService.callClientEvent('CEF:Player.Character.Create.Begin');
  }

  public onClientCreateCharacterComplete() {
    this.rageService.callClientEvent('CEF:Player.Character.Create.Complete');
  }

  public onClientSelectCharacterComplete() {
    this.rageService.callClientEvent('CEF:Player.Character.Select.Complete');
  }

  public onClientCreateCharacterSwitchView(view: CharacterCreationView) {
    this.rageService.callClientEvent('CEF:Player.Character.Create.SwitchView', view);
  }
}
