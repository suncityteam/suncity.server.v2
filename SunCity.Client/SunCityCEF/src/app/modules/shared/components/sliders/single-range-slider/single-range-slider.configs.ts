import {Options} from 'ng5-slider';

export class SingleRangeSliderConfigs {
  public readonly rangeSliderConfigNormilezed: Options = {
    floor: 0.0,
    ceil: 1.0,
    step: 0.1
  };
}

