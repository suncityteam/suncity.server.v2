import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DropDownSliderComponent } from './drop-down-slider.component';

describe('DropDownSliderComponent', () => {
  let component: DropDownSliderComponent;
  let fixture: ComponentFixture<DropDownSliderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DropDownSliderComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DropDownSliderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
