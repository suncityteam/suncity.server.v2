import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Observable} from 'rxjs';
import {AbstractControl, FormControl} from '@angular/forms';
import {KeyValueControl} from '../../key-value-control';

@Component({
  selector: 'app-drop-down-slider',
  templateUrl: './drop-down-slider.component.html',
  styleUrls: ['./drop-down-slider.component.scss']
})
export class DropDownSliderComponent extends KeyValueControl implements OnInit {
  constructor() {
    super();
  }

  ngOnInit() {
  }

  public onClickedLeft(): void {
    this.index--;
    if (this.index < 0) {
      this.index = 0;
    }
    this.onSelectionChanged();
  }

  public onClickedRight(): void {
    this.index++;
    if (this.index === this.items.length) {
      this.index = 0;
    }
    this.onSelectionChanged();
  }
}
