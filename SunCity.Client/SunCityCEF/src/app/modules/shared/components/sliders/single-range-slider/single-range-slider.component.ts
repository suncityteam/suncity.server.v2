import {Component, Input, OnInit} from '@angular/core';
import {Options} from 'ng5-slider';
import {FormControl} from '@angular/forms';

@Component({
  selector: 'app-single-range-slider',
  templateUrl: './single-range-slider.component.html',
  styleUrls: ['./single-range-slider.component.scss']
})
export class SingleRangeSliderComponent implements OnInit {

  @Input()
  public contol: FormControl;
  public value: number;

  @Input()
  public min ? = 0;

  @Input()
  public max ? = 100;

  @Input()
  public step ? = 1;

  @Input()
  public options?: Options;

  public onValueChanged(value: number) {
    if (isNaN(value)) {
      return;
    }
    this.contol.setValue(value);
  }

  ngOnInit() {
    if (this.contol) {
      if (isNaN(this.contol.value)) {
        this.value = this.min;
      } else {
        this.value = this.contol.value;
      }
      this.contol.valueChanges.subscribe(v => this.value = v);
    } else {
      throw new Error('Missed control bind');
    }

    if (!this.options) {
      this.options = {};
    }

    if (this.min !== undefined) {
      this.options.floor = this.min;
    }

    if (this.max !== undefined) {
      this.options.ceil = this.max;
    }

    if (this.step !== undefined) {
      this.options.step = this.step;
    }
  }

}
