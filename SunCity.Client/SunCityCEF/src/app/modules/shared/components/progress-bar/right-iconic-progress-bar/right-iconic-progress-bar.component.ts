import {Component, OnInit} from '@angular/core';
import {IconicProgressBar} from '../iconic-progress-bar';

@Component({
  selector: 'app-right-iconic-progress-bar',
  templateUrl: './right-iconic-progress-bar.component.html',
  styleUrls: ['./right-iconic-progress-bar.component.scss']
})
export class RightIconicProgressBarComponent extends IconicProgressBar implements OnInit {

  constructor() {
    super();
  }

  ngOnInit() {
  }

}
