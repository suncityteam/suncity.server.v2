import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { KtProgressBarComponent } from './kt-progress-bar.component';

describe('KtProgressBarComponent', () => {
  let component: KtProgressBarComponent;
  let fixture: ComponentFixture<KtProgressBarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ KtProgressBarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(KtProgressBarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
