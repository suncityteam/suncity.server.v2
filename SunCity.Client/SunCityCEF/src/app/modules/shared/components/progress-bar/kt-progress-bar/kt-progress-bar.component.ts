import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-kt-progress-bar',
  templateUrl: './kt-progress-bar.component.html',
  styleUrls: ['./kt-progress-bar.component.scss']
})
export class KtProgressBarComponent implements OnInit {

  @Input()
  public readonly color: string;

  @Input()
  public readonly label: string;

  @Input()
  public readonly unit: string;

  @Input()
  public readonly maximum: number;

  @Input()
  public readonly minimum: number = 0;

  @Input()
  public readonly current: number;

  public get percent(): number {
    return this.current / this.maximum * 100.0;
  }

  constructor() {
  }

  ngOnInit() {
  }

}
