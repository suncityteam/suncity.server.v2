import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RightIconicProgressBarComponent } from './right-iconic-progress-bar.component';

describe('RightIconicProgressBarComponent', () => {
  let component: RightIconicProgressBarComponent;
  let fixture: ComponentFixture<RightIconicProgressBarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RightIconicProgressBarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RightIconicProgressBarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
