import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LeftIconicProgressBarComponent } from './left-iconic-progress-bar.component';

describe('LeftIconicProgressBarComponent', () => {
  let component: LeftIconicProgressBarComponent;
  let fixture: ComponentFixture<LeftIconicProgressBarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LeftIconicProgressBarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LeftIconicProgressBarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
