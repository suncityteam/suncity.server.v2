import {Component, OnInit} from '@angular/core';
import {IconicProgressBar} from '../iconic-progress-bar';

@Component({
  selector: 'app-left-iconic-progress-bar',
  templateUrl: './left-iconic-progress-bar.component.html',
  styleUrls: ['./left-iconic-progress-bar.component.scss']
})
export class LeftIconicProgressBarComponent extends IconicProgressBar implements OnInit {

  constructor() {
    super();
  }

  ngOnInit() {
  }

}
