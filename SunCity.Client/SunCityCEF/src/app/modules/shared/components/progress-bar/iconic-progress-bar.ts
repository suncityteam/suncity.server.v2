import {Input} from '@angular/core';

export class IconicProgressBar {
  @Input()
  value: number;

  @Input()
  icon: string;

  @Input()
  styleName: string;
}
