import {Component, Input, OnInit} from '@angular/core';
import {IOperationError} from '../../../../../http/GameHttpClient';

@Component({
  selector: 'app-operation-error',
  templateUrl: './operation-error.component.html',
  styleUrls: ['./operation-error.component.scss']
})
export class OperationErrorComponent implements OnInit {
  @Input() operationError: IOperationError;

  constructor() { }

  ngOnInit() {
  }

}
