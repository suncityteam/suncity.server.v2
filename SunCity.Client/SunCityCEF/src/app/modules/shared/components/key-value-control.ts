import {EventEmitter, Input, Output} from '@angular/core';
import {AbstractControl} from '@angular/forms';

export abstract class KeyValueControl {
  @Input()
  public items: any[];

  @Input()
  public isObject = true;

  @Input()
  public keyValue = 'Name';

  @Input()
  public keyId = 'EntityId';

  @Input()
  public control: AbstractControl;

  @Output()
  public selected = new EventEmitter<any>();

  public index = 0;

  public get currentItem(): any {
    return this.items[this.index];
  }

  public get displayValue(): any {
    if (this.isObject) {
      return this.currentItem[this.keyValue];
    }
    return this.currentItem;
  }

  public getDisplayTextBy(index: number) {
    return this.getDisplayText(this.items[index]);
  }

  public getDisplayText(item: any) {
    if (this.isObject) {
      return item[this.keyValue];
    }
    return item;
  }

  public getDisplayId(item: any) {
    if (this.isObject) {
      return item[this.keyId];
    }
    return this.items.indexOf(item);
  }

  protected onSelectionChanged() {
    this.selected.emit(this.currentItem);
    if (this.control) {
      if (this.isObject) {
        this.control.setValue(this.currentItem[this.keyId]);
      } else {
        this.control.setValue(this.index);
      }
    }
  }
}
