import {Component, Input, OnInit} from '@angular/core';
import {FormControl} from '@angular/forms';

@Component({
  selector: 'app-key-value-params',
  templateUrl: './key-value-params.component.html',
  styleUrls: ['./key-value-params.component.scss']
})
export class KeyValueParamsComponent implements OnInit {

  @Input()
  public readonly key: string;

  @Input()
  public readonly value: string;

  @Input()
  public readonly control: FormControl;

  public get displayValue(): string {
    if (this.control) {
      return this.control.value;
    }
    return this.value;
  }


  constructor() {
  }

  ngOnInit() {
  }

}
