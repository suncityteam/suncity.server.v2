import {ValidatorFn} from '@angular/forms';
import {IFormControlOptions, IFormGroupAdapter} from './form-group-adapter-interface';


export function Control(options?: IFormControlOptions) {
  return (target: object, propertyName: string) => {
    if (delete target[propertyName]) {
      Object.defineProperty(target, propertyName, {
        get() {
          const adapter = this as IFormGroupAdapter;
          if (!adapter || !adapter.formGroup || !adapter.formControlsValidators) {
            throw new Error('this not implemented FormGroupAdapter');
          }
          adapter.addControlIfNotExist(propertyName, options);
          const control = adapter.getControl(propertyName);
          return control;
        },
        enumerable: true,
        configurable: true
      });
    }
  };
}
