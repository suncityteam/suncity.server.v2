import {IFormGroupAdapter} from './form-group-adapter-interface';
import {FormArray} from '@angular/forms';

export function ArrayControl() {
  return (target: any, propertyName: string) => {
    if (delete target[propertyName]) {
      Object.defineProperty(target, propertyName, {
        get() {
          const adapter = this as IFormGroupAdapter;
          if (!adapter || !adapter.formGroup || !adapter.formControlsValidators) {
            throw new Error('this not implemented FormGroupAdapter');
          }
          adapter.addArrayControlIfNotExist(propertyName);
          const control = adapter.getControl<FormArray>(propertyName);
          return control;
        },
        enumerable: true,
        configurable: true
      });
    }
  };
}
