export enum FormGroupValidationState {
  /** Форма не валидна */
  Invalid,
  /** Форма валидна */
  Valid,
}

