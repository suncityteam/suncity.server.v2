import {AbstractControl, AbstractControlOptions, AsyncValidatorFn, FormArray, FormControl, FormGroup, ValidatorFn} from '@angular/forms';
import {IFormControlOptions, IFormGroupAdapter} from './form-group-adapter-interface';
import {EventEmitter, Input} from '@angular/core';
import {FormGroupValidationState} from './form-group-validation-state';

export abstract class BaseComponent<TFormGroup extends FormGroupAdapter> {
  @Input()
  public readonly FormGroup: TFormGroup;
}

export abstract class FormGroupAdapter implements IFormGroupAdapter {
  public readonly formGroup: FormGroup;
  public readonly formGroupValidators: ValidatorFn[];
  public readonly formControlsValidators: { [id: string]: ValidatorFn[] } = {};
  public debugLogValueChanges: boolean;

  public get isValid(): boolean {
    return this.formGroup.valid;
  }

  public get isInvalid(): boolean {
    return this.formGroup.invalid;
  }

  public constructor(
    validatorOrOpts?: ValidatorFn | ValidatorFn[] | AbstractControlOptions | null,
    asyncValidator?: AsyncValidatorFn | AsyncValidatorFn[] | null) {
    this.formGroup = new FormGroup({}, validatorOrOpts, asyncValidator);
  }

  public validate(): FormGroupValidationState {
    this.formGroup.updateValueAndValidity();
    if (this.formGroup.valid) {
      return FormGroupValidationState.Valid;
    }
    return FormGroupValidationState.Invalid;
  }

  public setControlArray(control: FormArray, val: any[], options: { onlySelf?: boolean; emitEvent?: boolean; } = {}): void {
    control.controls = val.map(elem => new FormControl(elem));
    control.updateValueAndValidity(options);
  }

  public addControlIfNotExist(name: string, options?: IFormControlOptions): void {
    const existControl = this.formGroup.controls[name];
    if (!existControl) {
      const value = options && options.value;
      const disabled = options && options.disabled;
      const validators = options && options.validators;
      const control = new FormControl({value, disabled}, validators);
      (control as any).ControlName = name;

      this.formGroup.addControl(name, control);
      this.formControlsValidators[name] = validators;
    }
  }

  public addArrayControlIfNotExist(name: string): void {
    if (!this.formGroup.contains(name)) {
      const control = new FormArray([]);
      this.formGroup.addControl(name, control);
    }
  }

  public getControl<T extends AbstractControl>(name: string): T {
    return this.formGroup.controls[name] as T;
  }
}
