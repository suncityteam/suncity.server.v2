import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { KeyValueParamsComponent } from './key-value-params.component';

describe('KeyValueParamsComponent', () => {
  let component: KeyValueParamsComponent;
  let fixture: ComponentFixture<KeyValueParamsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ KeyValueParamsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(KeyValueParamsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
