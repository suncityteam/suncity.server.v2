import {FormGroupValidationState} from './form-group-validation-state';
import {AbstractControl, FormGroup, ValidatorFn} from '@angular/forms';
import {EventEmitter} from '@angular/core';

export interface IFormControlOptions {
  validators?: ValidatorFn[];
  disabled?: boolean;
  value?: any;
}

export interface IFormGroupAdapter {
  formGroup: FormGroup;
  formGroupValidators: ValidatorFn[];
  formControlsValidators: { [id: string]: ValidatorFn[] };

  validate(): FormGroupValidationState;

  addControlIfNotExist(name: string, options?: IFormControlOptions): void;

  addArrayControlIfNotExist(name: string): void;

  getControl<T extends AbstractControl>(name: string): T;
}
