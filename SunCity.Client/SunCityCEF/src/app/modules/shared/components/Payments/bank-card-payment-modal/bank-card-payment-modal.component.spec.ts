import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BankCardPaymentModalComponent } from './bank-card-payment-modal.component';

describe('BankCardPaymentModalComponent', () => {
  let component: BankCardPaymentModalComponent;
  let fixture: ComponentFixture<BankCardPaymentModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BankCardPaymentModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BankCardPaymentModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
