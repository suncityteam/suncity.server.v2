import {Observable} from 'rxjs';
import {IBankAuthorizationForm} from '../../../../../http/GameHttpClient';

export interface BankCardPaymentModalModel {
  readonly description: string;
  readonly cost: number;
  readonly paymentMethod: (form: IBankAuthorizationForm) => Observable<any>;
}
