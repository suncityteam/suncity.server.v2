import {FormGroupAdapter} from '../../form-groups/form-group-adapter/form-group-adapter';
import {Control} from '../../form-groups/form-group-adapter/form-group-control';
import {FormControl} from '@angular/forms';

export class BankCardPaymentModalFormGroup extends FormGroupAdapter {
  @Control()
  public bankCardId: FormControl;

  @Control({disabled: true})
  public bankCardName: FormControl;

  @Control()
  public pinCode: FormControl;

  @Control({disabled: true})
  public description: FormControl;

  @Control({disabled: true})
  public cost: FormControl;
}
