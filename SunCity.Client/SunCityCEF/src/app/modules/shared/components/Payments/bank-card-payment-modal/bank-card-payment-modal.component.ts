import {Component, Input, OnInit} from '@angular/core';
import {BankCardPaymentModalFormGroup} from './bank-card-payment-modal-form.group';
import {ModalWindow} from '../../../../world/property/shared/services/modal/modal.service';
import {BankCardPaymentModalModel} from './bank-card-payment-modal.model';
import {NgbModalRef} from '@ng-bootstrap/ng-bootstrap';
import {
  BankCardAccountHttpClient,
  BankCardPreviewModel,
  BankCardPreviewModelArrayOperationResponse, IBankAuthorizationForm,
  IOperationError, PurchaseWorldPropertyResponseOperationResponse
} from '../../../../../http/GameHttpClient';

@Component({
  selector: 'app-bank-card-payment-modal',
  templateUrl: './bank-card-payment-modal.component.html',
  styleUrls: ['./bank-card-payment-modal.component.scss']
})
export class BankCardPaymentModalComponent implements OnInit, ModalWindow<BankCardPaymentModalModel> {
  @Input() public readonly model: BankCardPaymentModalModel;
  @Input() public readonly modalRef: NgbModalRef;

  public operationError: IOperationError | undefined;
  public bankCards: BankCardPreviewModel[];
  public readonly form = new BankCardPaymentModalFormGroup();

  constructor(
    private readonly bankCardService: BankCardAccountHttpClient,
  ) {

  }

  ngOnInit() {
    this.form.cost.setValue(this.model.cost);
    this.form.description.setValue(this.model.description);

    this.bankCardService.getPlayerBankCardList(undefined).subscribe(res => this.onGetBankCardList(res));
  }

  private onGetBankCardList(res: BankCardPreviewModelArrayOperationResponse) {
    this.bankCards = res.content;
    const selectedBankCard = this.bankCards.find(q => q.entityId === this.form.bankCardId.value);
    this.onBankCardSelected(selectedBankCard);
  }

  public onClickedCancelPayment(closed: boolean): void {
    if (closed) {
      this.modalRef.close(undefined);
    } else {
      this.modalRef.close(false);
    }
  }

  public onClickedConfirmPayment(): void {
    this.operationError = null;

    const form: IBankAuthorizationForm = {
      payerBankCardId: this.form.bankCardId.value,
      pinCode: this.form.pinCode.value,
    };

    this.model.paymentMethod(form).subscribe(res => this.onAuthorizationResponse(res));
  }

  private onAuthorizationResponse(res: PurchaseWorldPropertyResponseOperationResponse): void {
    this.operationError = res.error;
    if (res.isCorrect) {
      this.modalRef.close(true);
    }
  }

  public onBankCardSelected(bankCard: BankCardPreviewModel): void {
    if (bankCard) {
      this.form.bankCardId.setValue(bankCard.entityId.id);
      this.form.bankCardName.setValue(bankCard.number);
    } else {
      this.form.bankCardId.setValue(null);
      this.form.bankCardName.setValue(null);
    }
  }
}
