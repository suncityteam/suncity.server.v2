import {Component, EventEmitter, Input, OnInit, Output, ViewEncapsulation} from '@angular/core';
import {RageMPService} from '../../../../../services/ragemp/ragemp.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-window-close-button',
  templateUrl: './window-close-button.component.html',
  styleUrls: ['./window-close-button.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class WindowCloseButtonComponent implements OnInit {

  @Output()
  public readonly windowClosed = new EventEmitter();

  constructor(
    private readonly rage: RageMPService,
    private readonly router: Router
  ) {
  }

  ngOnInit() {
  }

  onClickedCloseWindow() {
    this.windowClosed.emit();
    this.router.navigate(['/']).then(() => {
      this.rage.callClientEvent('CEF:WindowClosed');
    });
  }
}
