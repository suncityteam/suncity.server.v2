import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PurchaseDropdownButtonComponent } from './purchase-dropdown-button.component';

describe('PurchaseDropdownButtonComponent', () => {
  let component: PurchaseDropdownButtonComponent;
  let fixture: ComponentFixture<PurchaseDropdownButtonComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PurchaseDropdownButtonComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PurchaseDropdownButtonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
