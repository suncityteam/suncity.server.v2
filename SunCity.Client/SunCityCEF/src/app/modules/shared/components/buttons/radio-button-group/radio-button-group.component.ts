import {Component, OnInit} from '@angular/core';
import {KeyValueControl} from '../../key-value-control';

@Component({
  selector: 'app-radio-button-group',
  templateUrl: './radio-button-group.component.html',
  styleUrls: ['./radio-button-group.component.scss']
})
export class RadioButtonGroupComponent extends KeyValueControl implements OnInit {

  ngOnInit() {
  }

  public onSelectItem(index: number): void {
    this.index = index;
    this.onSelectionChanged();
  }

}
