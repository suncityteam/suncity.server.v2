import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';

@Component({
  selector: 'app-iconic-button',
  templateUrl: './iconic-button.component.html',
  styleUrls: ['./iconic-button.component.scss']
})
export class IconicButtonComponent implements OnInit {

  @Input()
  public readonly text: string;

  @Input()
  public readonly icon: string;

  @Output()
  public readonly clicked = new EventEmitter();

  constructor() {
  }

  ngOnInit() {
  }

  public onClicked(): void {
    this.clicked.emit();
  }

}
