import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {PurchaseMethod} from '../../../../../enums/purchase/purchase-method';


@Component({
  selector: 'app-purchase-dropdown-button',
  templateUrl: './purchase-dropdown-button.component.html',
  styleUrls: ['./purchase-dropdown-button.component.scss']
})
export class PurchaseDropdownButtonComponent implements OnInit {
  public readonly purchaseMethods = PurchaseMethod;

  @Input()
  public readonly allowCashPay = true;

  @Input()
  public readonly allowBankCardPay = true;

  @Input()
  public readonly allowDonatePay = false;

  @Output()
  public readonly purchaseMethodSelected = new EventEmitter<PurchaseMethod>();

  constructor() {
  }

  ngOnInit() {
  }

  private onPurchaseMethodSelected(purchaseMethod: PurchaseMethod): void {
    this.purchaseMethodSelected.emit(purchaseMethod);
  }

}
