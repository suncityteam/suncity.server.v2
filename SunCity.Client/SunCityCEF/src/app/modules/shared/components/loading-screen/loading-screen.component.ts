import {Component, Input, OnInit} from '@angular/core';
import {interval, Observable} from 'rxjs';
import {take} from 'rxjs/operators';
import {NgxUiLoaderService} from 'ngx-ui-loader';

export enum LoadingScreenState {
  None,
  Process,
  Successfully,
  Failed,
}

@Component({
  selector: 'app-loading-screen',
  templateUrl: './loading-screen.component.html',
  styleUrls: ['./loading-screen.component.scss']
})
export class LoadingScreenComponent implements OnInit {

  @Input()
  public processMessage = 'Выполнение операции';

  @Input()
  public successfullyMessage = 'Операция выполнена успешно';

  @Input()
  public failedMessage = 'Неудалось выполнить операцию';

  @Input()
  public operationStatusSource: Observable<LoadingScreenState>;

  @Input()
  public operationTimeOutInSec = 15;

  @Input()
  public operationHiddenTimeInSec = 5;

  private operationStatus: LoadingScreenState = LoadingScreenState.None;

  constructor(private ngxService: NgxUiLoaderService) {
  }

  ngOnInit() {
    this.ngxService.startLoader('loader-01');


    if (this.operationStatusSource) {
      this.operationStatusSource.subscribe(status => this.onOperationStatusSource(status));
    } else {
      // throw new Error('missed operationStatusSource');
    }
  }

  public get actualMessage(): string {
    if (this.operationStatus === LoadingScreenState.Successfully) {
      return this.successfullyMessage;
    } else if (this.operationStatus === LoadingScreenState.Failed) {
      return this.failedMessage;
    } else if (this.operationStatus === LoadingScreenState.Process) {
      return this.processMessage;
    }
    return null;
  }

  private onOperationStatusSource(state: LoadingScreenState): void {
    this.operationStatus = state;
    if (state === LoadingScreenState.Process) {
      interval(this.operationTimeOutInSec * 1000)
        .pipe(take(1)).subscribe(q => this.onOperationStatusSource(LoadingScreenState.Failed));
    } else if (state !== LoadingScreenState.None) {
      interval(this.operationHiddenTimeInSec)
        .pipe(take(1)).subscribe(q => this.onOperationStatusSource(LoadingScreenState.None));
    }
  }

}
