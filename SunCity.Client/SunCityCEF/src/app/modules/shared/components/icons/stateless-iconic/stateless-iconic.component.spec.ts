import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StatelessIconicComponent } from './stateless-iconic.component';

describe('StatelessIconicComponent', () => {
  let component: StatelessIconicComponent;
  let fixture: ComponentFixture<StatelessIconicComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StatelessIconicComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StatelessIconicComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
