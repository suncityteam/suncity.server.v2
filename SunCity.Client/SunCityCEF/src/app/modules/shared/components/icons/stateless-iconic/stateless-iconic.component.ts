import {Component, Input, OnInit} from '@angular/core';

export interface IStatelessIconic {
  [index: number]: string;
}

@Component({
  selector: 'app-stateless-iconic',
  templateUrl: './stateless-iconic.component.html',
  styleUrls: ['./stateless-iconic.component.scss']
})
export class StatelessIconicComponent implements OnInit {

  @Input()
  public readonly icons: IStatelessIconic;

  @Input()
  public state: number | boolean;

  public get icon(): string {
    const value = Number(this.state);
    return this.icons[value];
  }

  constructor() {
  }

  ngOnInit() {
    if (this.icons === null || this.icons === undefined) {
      throw new Error('Missed bind icons');
    }
  }

}
