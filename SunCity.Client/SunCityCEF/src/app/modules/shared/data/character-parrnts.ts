import {Injectable} from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class CharacterParents {
  public readonly fathers = [
    'Benjamin', 'Daniel', 'Joshua', 'Noah', 'Andrew', 'Juan', 'Alex', 'Isaac',
    'Evan', 'Ethan', 'Vincent', 'Angel', 'Diego', 'Adrian', 'Gabriel', 'Michael',
    'Santiago', 'Kevin', 'Louis', 'Samuel', 'Anthony', 'Claude', 'Niko', 'John'
  ];

  public readonly mothers = [
    'Hannah', 'Aubrey', 'Jasmine', 'Gisele', 'Amelia', 'Isabella', 'Zoe', 'Ava',
    'Camila', 'Violet', 'Sophia', 'Evelyn', 'Nicole', 'Ashley', 'Gracie', 'Brianna',
    'Natalie', 'Olivia', 'Elizabeth', 'Charlotte', 'Emma', 'Misty'
  ];

  public readonly genders = [
    'Мужской', 'Женский'
  ];
}
