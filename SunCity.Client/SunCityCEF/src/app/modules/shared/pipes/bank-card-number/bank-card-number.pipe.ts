import {Pipe, PipeTransform} from '@angular/core';

@Pipe({
  name: 'bankCardNumber'
})
export class BankCardNumberPipe implements PipeTransform {

  transform(value: number, args?: any): any {
    if (value) {
      const cardNumber = value.toString();
      return cardNumber.replace(/(\d{4})/g, '$1 ').replace(/(^\s+|\s+$)/, '');
    }
    return null;
  }

}
