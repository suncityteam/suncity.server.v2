import {Pipe, PipeTransform} from '@angular/core';
import {GameCostCurrency, IGameCost} from '../../../../http/GameHttpClient';

@Pipe({
  name: 'gameCost'
})
export class GameCostPipe implements PipeTransform {

  transform(value: IGameCost, colored?: boolean): any {
    if (colored) {
      if (value.currency === GameCostCurrency.Donate) {
        return `${value.amount} DP`;
      } else if (value.currency === GameCostCurrency.Money) {
        return `${value.amount} $`;
      } else {
        return `${value.amount} ?`;
      }
    } else {
      if (value.currency === GameCostCurrency.Donate) {
        return `${value.amount} DP`;
      } else if (value.currency === GameCostCurrency.Money) {
        return `${value.amount} $`;
      } else {
        return `${value.amount} ?`;
      }
    }
    return null;
  }

}
