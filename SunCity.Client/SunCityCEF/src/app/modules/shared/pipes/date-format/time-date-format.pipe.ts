import {Pipe, PipeTransform} from '@angular/core';
import * as moment from 'moment';

@Pipe({
  name: 'dateTimeFormat'
})
export class DateTimeFormatPipe implements PipeTransform {
  transform(value: any, args?: any): string | null {
    if (value) {
      return moment(value).format('DD.MM.YYYY HH:mm');
    }
    return null;
  }
}
