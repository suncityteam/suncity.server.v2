import { Pipe, PipeTransform } from '@angular/core';
import * as moment from 'moment';

@Pipe({
  name: 'dateWithMonthName'
})
export class DateWithMonthNamePipe implements PipeTransform {

  transform(value: any, args?: any): any {
    return value && moment(value).format('DD MMMM YYYY');
  }

}
