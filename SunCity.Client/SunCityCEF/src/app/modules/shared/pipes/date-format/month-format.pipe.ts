import { Pipe, PipeTransform } from '@angular/core';
import * as moment from 'moment';

@Pipe({
  name: 'monthFormat'
})
export class MonthFormatPipe implements PipeTransform {

  transform(value: any, args?: any): any {
    return moment(value).format('MMMM YYYY');
  }

}


