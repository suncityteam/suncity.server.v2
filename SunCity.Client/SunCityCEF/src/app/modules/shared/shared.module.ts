import {ModuleWithProviders, NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {LoadingScreenComponent} from './components/loading-screen/loading-screen.component';
import {NgxUiLoaderModule} from 'ngx-ui-loader';
import {DropDownSliderComponent} from './components/sliders/drop-down-slider/drop-down-slider.component';
import {RadioButtonGroupComponent} from './components/buttons/radio-button-group/radio-button-group.component';
import {
  NgbButtonLabel,
  NgbButtonsModule,
  NgbCheckBox, NgbDropdown, NgbDropdownItem, NgbDropdownMenu,
  NgbDropdownModule, NgbDropdownToggle, NgbModal, NgbModalModule,
  NgbModule, NgbPopoverModule,
  NgbRadio,
  NgbRadioGroup
} from '@ng-bootstrap/ng-bootstrap';
import {SingleRangeSliderComponent} from './components/sliders/single-range-slider/single-range-slider.component';
import {Ng5SliderModule} from 'ng5-slider';
import {LeftIconicProgressBarComponent} from './components/progress-bar/left-iconic-progress-bar/left-iconic-progress-bar.component';
import {RightIconicProgressBarComponent} from './components/progress-bar/right-iconic-progress-bar/right-iconic-progress-bar.component';
import {StatelessIconicComponent} from './components/icons/stateless-iconic/stateless-iconic.component';
import {KeyValueParamsComponent} from './components/form-groups/key-value-params/key-value-params.component';
import {PurchaseDropdownButtonComponent} from './components/buttons/purchase-dropdown-button/purchase-dropdown-button.component';
import {IconicButtonComponent} from './components/buttons/iconic-button/iconic-button.component';
import {DateFormatPipe} from './pipes/date-format/date-format.pipe';
import {DateTimeFormatPipe} from './pipes/date-format/time-date-format.pipe';
import {MonthFormatPipe} from './pipes/date-format/month-format.pipe';
import {TimeFormatPipe} from './pipes/date-format/time-format.pipe';
import {OperationErrorComponent} from './components/error/operation-error/operation-error.component';
import {WindowCloseButtonComponent} from './components/buttons/window-close-button/window-close-button.component';
import {WindowHeaderComponent} from './components/window-header/window-header.component';
import {GameCostPipe} from './pipes/game-cost/game-cost.pipe';
import {BankCardPaymentModalComponent} from './components/Payments/bank-card-payment-modal/bank-card-payment-modal.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {BankCardNumberPipe} from './pipes/bank-card-number/bank-card-number.pipe';
import {GameItemsModule} from '../world/game-items/game-items.module';
import {NgDragDropModule} from 'ng-drag-drop';
import {KtProgressBarComponent} from './components/progress-bar/kt-progress-bar/kt-progress-bar.component';
import {ColorSketchModule} from 'ngx-color/sketch';
import {ColorGithubModule} from 'ngx-color/github';
import {ColorCircleModule} from 'ngx-color/circle';
import {RouterModule} from '@angular/router';

@NgModule({
  declarations:
    [
      LoadingScreenComponent,
      DropDownSliderComponent,
      RadioButtonGroupComponent,
      SingleRangeSliderComponent,
      LeftIconicProgressBarComponent,
      RightIconicProgressBarComponent,
      StatelessIconicComponent,
      KeyValueParamsComponent,
      PurchaseDropdownButtonComponent,
      IconicButtonComponent,
      DateFormatPipe,
      DateTimeFormatPipe,
      MonthFormatPipe,
      TimeFormatPipe,
      OperationErrorComponent,
      WindowCloseButtonComponent,
      WindowHeaderComponent,
      GameCostPipe,
      BankCardPaymentModalComponent,
      BankCardNumberPipe,
      KtProgressBarComponent,
    ],
  exports:
    [
      BankCardNumberPipe,
      BankCardPaymentModalComponent,

      LoadingScreenComponent,
      NgxUiLoaderModule,
      DropDownSliderComponent,
      RadioButtonGroupComponent,
      WindowCloseButtonComponent,
      WindowHeaderComponent,

      NgbButtonLabel,
      NgbButtonsModule,
      NgbRadioGroup,
      NgbRadio,
      NgbCheckBox,
      NgbModule,
      SingleRangeSliderComponent,
      Ng5SliderModule,
      RightIconicProgressBarComponent,
      LeftIconicProgressBarComponent,
      StatelessIconicComponent,
      KeyValueParamsComponent,
      PurchaseDropdownButtonComponent,
      NgbDropdownModule,
      NgbDropdown,
      NgbDropdownMenu,
      NgbDropdownItem,
      NgbDropdownToggle,
      IconicButtonComponent,
      DateFormatPipe,
      DateTimeFormatPipe,
      MonthFormatPipe,
      TimeFormatPipe,
      OperationErrorComponent,
      GameCostPipe,
      NgbModalModule,
      GameItemsModule,
      NgbPopoverModule,
      NgDragDropModule,
      KtProgressBarComponent,

      ColorSketchModule,
      ColorCircleModule,
      FormsModule,
      RouterModule
    ],
  imports: [
    FormsModule,
    RouterModule,

    ColorSketchModule,
    ColorCircleModule,

    CommonModule,
    NgxUiLoaderModule,
    NgbButtonsModule,
    NgbModule,
    Ng5SliderModule,
    NgbDropdownModule,
    NgbModalModule,
    ReactiveFormsModule,
    GameItemsModule,
    NgbPopoverModule,
    NgDragDropModule.forRoot()
  ],
  providers: [
    DateFormatPipe,
    DateTimeFormatPipe,
    MonthFormatPipe,
    TimeFormatPipe,
    GameCostPipe,
    BankCardNumberPipe,
  ], entryComponents: [
    PurchaseDropdownButtonComponent,
    BankCardPaymentModalComponent
  ]
})


export class SharedModule {
  static forRoot(): ModuleWithProviders {
    return {
      ngModule: SharedModule,
      providers: []
    };
  }

}
