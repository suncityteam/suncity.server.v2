import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Params, Router} from '@angular/router';
import {ToastrService} from 'ngx-toastr';
import {PropertyBuyingFormGroupAdapter} from '../../property-buying-form-group.adapter';
import {GameCostPipe} from '../../../../../shared/pipes/game-cost/game-cost.pipe';
import {PurchaseMethod} from '../../../../../../enums/purchase/purchase-method';
import {BankCardPaymentModalComponent} from '../../../../../shared/components/Payments/bank-card-payment-modal/bank-card-payment-modal.component';
import {ModalService} from '../../services/modal/modal.service';
import {BankCardPaymentModalModel} from '../../../../../shared/components/Payments/bank-card-payment-modal/bank-card-payment-modal.model';
import {
  BankAuthorizationForm,
  GetWorldPropertySellingInformationResponse,
  GetWorldPropertySellingInformationResponseOperationResponse, IBankAuthorizationForm,
  IOperationError,
  PropertyHttpClient, PurchaseWorldPropertyRequest,
  PurchaseWorldPropertyResponseOperationResponse,
  WorldPropertyCategoryEnumDescriptionValue
} from '../../../../../../http/GameHttpClient';
import {Observable} from 'rxjs';

@Component({
  selector: 'app-property-buying-page',
  templateUrl: './property-buying-page.component.html',
  styleUrls: ['./property-buying-page.component.scss']
})
export class PropertyBuyingPageComponent implements OnInit {
  public model: GetWorldPropertySellingInformationResponse;
  public operationError: IOperationError;

  public readonly WorldPropertyCategory = WorldPropertyCategoryEnumDescriptionValue;
  public readonly form: PropertyBuyingFormGroupAdapter;

  public constructor(
    private readonly router: Router,
    private readonly activatedRoute: ActivatedRoute,
    private readonly httpClient: PropertyHttpClient,

    private readonly toastr: ToastrService,
    private readonly modalService: ModalService,
    gameCostPipe: GameCostPipe
  ) {
    this.form = new PropertyBuyingFormGroupAdapter(gameCostPipe);
  }

  public ngOnInit(): void {
    this.activatedRoute.params.subscribe(params => this.onChangeRouteParams(params));
  }

  private onChangeRouteParams(params: Params): void {
    const propertyId = params.propertyId;
    if (propertyId) {
      this.httpClient.getSellingInformation(propertyId).subscribe(res => this.onGetSellingInformationResponse(res));
    }
  }

  private onGetSellingInformationResponse(res: GetWorldPropertySellingInformationResponseOperationResponse) {
    this.operationError = res.error;
    if (res.isCorrect) {
      this.model = res.content;
      this.form.mapFromModel(this.model);
    }
  }

  public onClickedClose(): void {
    this.router.navigate(['/']);
  }

  public onPurchaseMethodSelected(purchaseMethod: PurchaseMethod): void {
    if (purchaseMethod === PurchaseMethod.Cash) {
      this.toastr.error('Покупка дома с помощью наличных не доступна!');
    } else if (purchaseMethod === PurchaseMethod.BankCard) {

      const model: BankCardPaymentModalModel = {
        description: this.model.category.description,
        cost: this.model.sellerCost.amount,
        paymentMethod: (form) => this.onPropertyPurchaseConfirmed(form),
      };

      this.modalService.show<BankCardPaymentModalModel>(BankCardPaymentModalComponent, model);
    }
  }

  private onPropertyPurchaseConfirmed(form: IBankAuthorizationForm): Observable<PurchaseWorldPropertyResponseOperationResponse> {
    return this.httpClient.purchase(new PurchaseWorldPropertyRequest({
      propertyId: this.model.entityId,
      payment: new BankAuthorizationForm(form)
    }));
  }
}

