import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PropertyBuyingRequiredLicensesComponent } from './property-buying-required-licenses.component';

describe('PropertyBuyingRequiredLicensesComponent', () => {
  let component: PropertyBuyingRequiredLicensesComponent;
  let fixture: ComponentFixture<PropertyBuyingRequiredLicensesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PropertyBuyingRequiredLicensesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PropertyBuyingRequiredLicensesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
