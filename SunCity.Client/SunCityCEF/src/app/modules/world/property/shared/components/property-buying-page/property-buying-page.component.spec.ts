import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PropertyBuyingPageComponent } from './property-buying-page.component';

describe('PropertyBuyingPageComponent', () => {
  let component: PropertyBuyingPageComponent;
  let fixture: ComponentFixture<PropertyBuyingPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PropertyBuyingPageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PropertyBuyingPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
