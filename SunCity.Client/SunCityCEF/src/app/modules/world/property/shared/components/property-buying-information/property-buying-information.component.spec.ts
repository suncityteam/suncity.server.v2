import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PropertyBuyingInformationComponent } from './property-buying-information.component';

describe('PropertyBuyingInformationComponent', () => {
  let component: PropertyBuyingInformationComponent;
  let fixture: ComponentFixture<PropertyBuyingInformationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PropertyBuyingInformationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PropertyBuyingInformationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
