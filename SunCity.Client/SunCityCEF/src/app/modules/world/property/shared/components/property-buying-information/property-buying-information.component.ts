import {Component, Input, OnInit} from '@angular/core';
import {GetWorldPropertySellingInformationResponse} from '../../../../../../http/GameHttpClient';

@Component({
  selector: 'app-property-buying-information',
  templateUrl: './property-buying-information.component.html',
  styleUrls: ['./property-buying-information.component.scss']
})
export class PropertyBuyingInformationComponent implements OnInit {
  @Input() public readonly model: GetWorldPropertySellingInformationResponse;

  constructor() {
  }

  ngOnInit() {
  }

}
