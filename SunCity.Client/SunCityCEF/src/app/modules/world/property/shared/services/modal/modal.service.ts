import {Injectable} from '@angular/core';
import {NgbModal, NgbModalRef} from '@ng-bootstrap/ng-bootstrap';

export interface ModalWindow<TModel> {
  model: TModel;
  modalRef: NgbModalRef;
}

@Injectable({
  providedIn: 'root'
})
export class ModalService {

  constructor(private readonly modalService: NgbModal) {
  }

  public show<TModel>(content: any, model?: TModel) {
    const modalRef = this.modalService.open(content, {
      backdrop: 'static',
      keyboard: false,
    });

    const window = modalRef.componentInstance as ModalWindow<TModel>;
    window.model = model;
    window.modalRef = modalRef;
  }
}
