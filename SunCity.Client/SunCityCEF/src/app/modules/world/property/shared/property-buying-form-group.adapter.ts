import {FormGroupAdapter} from '../../../shared/components/form-groups/form-group-adapter/form-group-adapter';
import {Control} from '../../../shared/components/form-groups/form-group-adapter/form-group-control';
import {FormControl} from '@angular/forms';
import {GameCostPipe} from '../../../shared/pipes/game-cost/game-cost.pipe';
import {GetWorldPropertySellingInformationResponse} from '../../../../http/GameHttpClient';


export class PropertyBuyingFormGroupAdapter extends FormGroupAdapter {

  @Control()
  public readonly id: FormControl;

  @Control()
  public readonly title: FormControl;

  @Control({value: 'Квартира'})
  public readonly category: FormControl;

  @Control({value: 'Комфорт +'})
  public readonly class: FormControl;

  @Control({value: 'D. Donskoy'})
  public readonly seller: FormControl;

  @Control({value: '150'})
  public readonly square: FormControl;

  @Control({value: '950$ / день'})
  public readonly vatRate: FormControl;

  @Control({value: '150$ / м2'})
  public readonly squareCost: FormControl;

  @Control({value: '1'})
  public readonly numberOfRooms: FormControl;

  @Control({value: '159089$'})
  public readonly stateCost: FormControl;

  @Control({value: '91959089$'})
  public readonly sellerCost: FormControl;

  @Control({value: '12%'})
  public readonly agencyCommission: FormControl;

  @Control({value: '555-995-999'})
  public readonly agencyPhoneNumber: FormControl;

  @Control({value: 'Paradise через А'})
  public readonly agency: FormControl;

  constructor(private readonly gameCostPipe: GameCostPipe) {
    super();
  }

  public mapFromModel(model: GetWorldPropertySellingInformationResponse): void {
    this.id.setValue(model.entityId);

    //  TODO: add on back end
    // this.title.setValue(model.name);

    this.category.setValue(model.category.description);
    this.class.setValue(model.class.description);

    //  TODO: add on back end
    // this.seller.setValue(model.owner);

    this.square.setValue(model.square);
    // this.squareCost.setValue(model.squareCost);

    this.stateCost.setValue(this.gameCostPipe.transform(model.stateCost, true));
    this.sellerCost.setValue(this.gameCostPipe.transform(model.sellerCost, true));

    // this.agencyCommission.setValue(model.agencyCommission);
    // this.agency.setValue(model.agency);
  }
}

