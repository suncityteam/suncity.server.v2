import {Component, Input, OnInit} from '@angular/core';
import {GetWorldPropertySellingInformationResponse} from '../../../../../../http/GameHttpClient';

@Component({
  selector: 'app-property-buying-required-licenses',
  templateUrl: './property-buying-required-licenses.component.html',
  styleUrls: ['./property-buying-required-licenses.component.scss']
})
export class PropertyBuyingRequiredLicensesComponent implements OnInit {
  @Input() public readonly model: GetWorldPropertySellingInformationResponse;

  constructor() { }

  ngOnInit() {
  }

}
