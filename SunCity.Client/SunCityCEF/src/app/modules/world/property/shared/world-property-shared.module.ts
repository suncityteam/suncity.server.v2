import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {RouterModule, Routes} from '@angular/router';
import {SharedModule} from '../../../shared/shared.module';
import {PropertyBuyingPageComponent} from './components/property-buying-page/property-buying-page.component';
import {ReactiveFormsModule} from '@angular/forms';
import { PropertyBuyingInformationComponent } from './components/property-buying-information/property-buying-information.component';
import { PropertyBuyingRequiredLicensesComponent } from './components/property-buying-required-licenses/property-buying-required-licenses.component';

export const GameWorldPropertySharedRoutes: Routes = [
  {
    path: 'World/Property/Buying/:propertyId',
    component: PropertyBuyingPageComponent
  }
];

@NgModule({
  declarations:
    [
      PropertyBuyingPageComponent,
      PropertyBuyingInformationComponent,
      PropertyBuyingRequiredLicensesComponent,
    ],
  imports: [
    CommonModule,
    SharedModule.forRoot(),
    RouterModule.forChild(GameWorldPropertySharedRoutes),
    ReactiveFormsModule,
  ], exports: [
    SharedModule,
  ]
})
export class WorldPropertySharedModule {
}
