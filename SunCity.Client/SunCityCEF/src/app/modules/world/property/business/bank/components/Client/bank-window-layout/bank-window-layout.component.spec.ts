import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BankWindowLayoutComponent } from './bank-window-layout.component';

describe('BankWindowLayoutComponent', () => {
  let component: BankWindowLayoutComponent;
  let fixture: ComponentFixture<BankWindowLayoutComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BankWindowLayoutComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BankWindowLayoutComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
