import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {CurrentBankCardStateService} from '../../../../../../states/current-bank-card-state.service';
import {ToastrService} from 'ngx-toastr';
import {
  BankAuthorizationForm,
  BankCardAccountHttpClient, BankGetCardInformationResponse,
  IOperationError
} from '../../../../../../../../../../../http/GameHttpClient';

@Component({
  selector: 'app-bank-card-information-page',
  templateUrl: './bank-card-information-page.component.html',
  styleUrls: ['./bank-card-information-page.component.scss']
})
export class BankCardInformationPageComponent implements OnInit {
  public operationError: IOperationError | undefined;
  public model: BankGetCardInformationResponse;

  constructor(
    private readonly toastr: ToastrService,
    private readonly router: Router,
    private readonly bankCardService: BankCardAccountHttpClient,
    private readonly currentBankCard: CurrentBankCardStateService
  ) {
  }

  ngOnInit() {
    /*if (this.currentBankCard.model) {
      this.bankCardService.getBankCardInformation(new BankAuthorizationForm({
        payerBankCardId: this.currentBankCard.entityId,
        pinCode: this.currentBankCard.pinCode
      })).subscribe(res => this.onGetBankCardResponse(res));
    } else {
      this.toastr.error('Неудалось получить информацию о банковской карте');
      this.router.navigate(['/']);
    }*/
  }

  /*private onGetBankCardResponse(res: BankGetCardInformationResponseOperationResponse): void {
    this.model = res.content;
    this.currentBankCard.model = res.content;
  }*/

}
