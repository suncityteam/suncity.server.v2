import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {RouterModule, Routes} from '@angular/router';
import {ReactiveFormsModule} from '@angular/forms';
import {SharedModule} from '../../../../shared/shared.module';
import {VehicleShowroomPageComponent} from './components/clients/vehicle-showroom-page/vehicle-showroom-page.component';
import {VehicleShowroomCategoriesComponent} from './components/clients/vehicle-showroom-categories/vehicle-showroom-categories.component';
import {VehicleShowroomListComponent} from './components/clients/vehicle-showroom-list/vehicle-showroom-list.component';
import {VehicleShowroomInformationComponent} from './components/clients/vehicle-showroom-information/vehicle-showroom-information.component';
import {VehicleShowroomHeaderComponent} from './components/clients/vehicle-showroom-header/vehicle-showroom-header.component';
import {VehicleShowroomOrderComponent} from './components/clients/vehicle-showroom-order/vehicle-showroom-order.component';
import {VehicleShowRoomStoreModule} from './store/vehicle-show-room-module';
import { VehicleShowroomColorComponent } from './components/clients/vehicle-showroom-color/vehicle-showroom-color.component';

const WorldPropertyBusinessVehicleShowRoomRoutes: Routes = [
  {
    component: VehicleShowroomPageComponent,
    path: 'World/Property/VehicleShowRoom/Client/:propertyId'
  }
];

@NgModule({
  declarations:
    [
      VehicleShowroomPageComponent,
      VehicleShowroomPageComponent,
      VehicleShowroomInformationComponent,
      VehicleShowroomListComponent,
      VehicleShowroomCategoriesComponent,
      VehicleShowroomHeaderComponent,
      VehicleShowroomOrderComponent,
      VehicleShowroomColorComponent,
    ],
  imports: [
    CommonModule,
    SharedModule.forRoot(),
    RouterModule.forChild(WorldPropertyBusinessVehicleShowRoomRoutes),

    VehicleShowRoomStoreModule,

    ReactiveFormsModule,
  ], exports: [
    SharedModule,
  ],
  providers: []
})
export class WorldPropertyBusinessVehicleShowRoomModule {
}
