import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VehicleShowroomCategoriesComponent } from './vehicle-showroom-categories.component';

describe('VehicleShowroomCategoriesComponent', () => {
  let component: VehicleShowroomCategoriesComponent;
  let fixture: ComponentFixture<VehicleShowroomCategoriesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VehicleShowroomCategoriesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VehicleShowroomCategoriesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
