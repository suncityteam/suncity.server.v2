import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Store} from '@ngrx/store';
import {VehicleShowRoomState} from '../../../store/vehicle-show-room-state';
import {VehicleShowRoomActions} from '../../../store/vehicle-show-room-actions';
import {VehicleModelClassEnumDescription} from '../../../../../../../../http/GameHttpClient';
import VehicleShowRoomSelectors from '../../../store/vehicle-show-room-selectors';

@Component({
  selector: 'app-vehicle-showroom-categories',
  templateUrl: './vehicle-showroom-categories.component.html',
  styleUrls: ['./vehicle-showroom-categories.component.scss']
})
export class VehicleShowroomCategoriesComponent implements OnInit {
  public readonly category$ = this.store$.select(VehicleShowRoomSelectors.selectedCategory);
  public selectedCategory: VehicleModelClassEnumDescription;

  public readonly categories$ = this.store$.select(VehicleShowRoomSelectors.categories);

  constructor(private readonly store$: Store<VehicleShowRoomState>) {
  }


  ngOnInit() {
    this.category$.subscribe(category => this.selectedCategory = category);
  }

  public onCategorySelected(category: VehicleModelClassEnumDescription): void {
    this.store$.dispatch(VehicleShowRoomActions.SELECT_CATEGORY({category}));
  }

  public isActive(category: VehicleModelClassEnumDescription): boolean {
    return this.selectedCategory && category.value === this.selectedCategory.value;
  }

}
