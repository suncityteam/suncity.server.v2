import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {StoreModule} from '@ngrx/store';
import {EffectsModule} from '@ngrx/effects';

import {VehicleShowRoomReducer} from './vehicle-show-room-reducer';
import {VehicleShowRoomEffects} from './vehicle-show-room-effects';
import {VehicleShowRoomModuleName} from './vehicle-show-room-actions';

@NgModule({
  imports: [
    CommonModule,
    StoreModule.forFeature(VehicleShowRoomModuleName, VehicleShowRoomReducer),
    EffectsModule.forFeature([VehicleShowRoomEffects]),
  ],
  providers: [VehicleShowRoomEffects]
})
export class VehicleShowRoomStoreModule { }
