import {Component, OnInit} from '@angular/core';
import {CurrentBankPropertyStateService} from '../../../../../states/current-bank-property-state.service';
import {
  BankCardTariffHttpClient,
  BankCardTariffModel,
  BankCardTariffModelArrayOperationResponse
} from '../../../../../../../../../../http/GameHttpClient';

@Component({
  selector: 'app-bank-card-tariff-page',
  templateUrl: './bank-card-tariff-page.component.html',
  styleUrls: ['./bank-card-tariff-page.component.scss']
})
export class BankCardTariffPageComponent implements OnInit {

  public tariffList: BankCardTariffModel[];

  constructor(
    private readonly bankCardTariffHttpClient: BankCardTariffHttpClient,
    private readonly currentBankProperty: CurrentBankPropertyStateService,
  ) {
  }

  ngOnInit(): void {
    this.bankCardTariffHttpClient.getCardTariffList(this.currentBankProperty.property.propertyId.id).subscribe(res => this.onGetCardTariffList(res));
  }

  private onGetCardTariffList(res: BankCardTariffModelArrayOperationResponse) {
    this.tariffList = res.content;
  }


}
