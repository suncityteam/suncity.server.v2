import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {StoreModule} from '@ngrx/store';
import {EffectsModule} from '@ngrx/effects';
import {BankClientStoreModuleName} from './bank-client-store-state';
import {BankClientStoreReducers} from './bank-client-store-reducers';
import {BankClientStoreEffects} from './bank-client-store-effects';

@NgModule({
  imports: [
    CommonModule,
    StoreModule.forFeature(BankClientStoreModuleName, BankClientStoreReducers),
    EffectsModule.forFeature([ BankClientStoreEffects]),
  ],
  providers: [BankClientStoreEffects]
})
export class BankClientStoreModule { }
