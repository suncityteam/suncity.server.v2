import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {BankGetCardInformationResponse} from '../../../../../../../../../../../http/GameHttpClient';

@Component({
  selector: 'app-bank-card-information',
  templateUrl: './bank-card-information.component.html',
  styleUrls: ['./bank-card-information.component.scss']
})
export class BankCardInformationComponent {
  @Input() public readonly model: BankGetCardInformationResponse;
}
