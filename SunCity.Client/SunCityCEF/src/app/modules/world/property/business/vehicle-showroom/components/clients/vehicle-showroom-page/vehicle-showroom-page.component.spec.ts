import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VehicleShowroomPageComponent } from './vehicle-showroom-page.component';

describe('VehicleShowroomPageComponent', () => {
  let component: VehicleShowroomPageComponent;
  let fixture: ComponentFixture<VehicleShowroomPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VehicleShowroomPageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VehicleShowroomPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
