import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BankCardInformationComponent } from './bank-card-information.component';

describe('BankCardInformationComponent', () => {
  let component: BankCardInformationComponent;
  let fixture: ComponentFixture<BankCardInformationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BankCardInformationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BankCardInformationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
