import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OpenCardPageComponent } from './open-card-page.component';

describe('OpenCardPageComponent', () => {
  let component: OpenCardPageComponent;
  let fixture: ComponentFixture<OpenCardPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OpenCardPageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OpenCardPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
