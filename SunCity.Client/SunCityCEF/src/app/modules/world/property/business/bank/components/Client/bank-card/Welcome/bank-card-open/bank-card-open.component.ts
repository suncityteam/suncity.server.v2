import {Component, EventEmitter, OnInit, Output} from '@angular/core';

@Component({
  selector: 'app-bank-card-open',
  templateUrl: './bank-card-open.component.html',
  styleUrls: ['./bank-card-open.component.scss']
})
export class BankCardOpenComponent implements OnInit {

  @Output()
  public readonly clicked = new EventEmitter<any>();

  constructor() {
  }

  ngOnInit() {
  }

  public onClicked(): void {
    this.clicked.emit();
  }

}
