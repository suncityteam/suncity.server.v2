import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BankCardOpenComponent } from './bank-card-open.component';

describe('BankCardOpenComponent', () => {
  let component: BankCardOpenComponent;
  let fixture: ComponentFixture<BankCardOpenComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BankCardOpenComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BankCardOpenComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
