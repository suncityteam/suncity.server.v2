export enum BankCardTransactionCategory {
  Deposit,

  Withdraw,

  TransferFrom,

  TransferFor,

  PropertyPurchaseFrom,
  PropertyPurchaseFor,

  PaydayFrom,
  PaydayFor
}
