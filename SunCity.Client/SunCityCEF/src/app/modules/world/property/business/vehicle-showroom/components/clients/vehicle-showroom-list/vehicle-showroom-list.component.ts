import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Store} from '@ngrx/store';
import {VehicleShowRoomState} from '../../../store/vehicle-show-room-state';
import {VehicleShowRoomActions} from '../../../store/vehicle-show-room-actions';
import {GetVehiclesShowRoomCatalogItem} from '../../../../../../../../http/GameHttpClient';
import VehicleShowRoomSelectors from '../../../store/vehicle-show-room-selectors';

@Component({
  selector: 'app-vehicle-showroom-list',
  templateUrl: './vehicle-showroom-list.component.html',
  styleUrls: ['./vehicle-showroom-list.component.scss']
})
export class VehicleShowroomListComponent {

  public readonly vehicles$ = this.store$.select(VehicleShowRoomSelectors.displayVehicles);
  public selectedVehicle: GetVehiclesShowRoomCatalogItem;

  constructor(
    private readonly store$: Store<VehicleShowRoomState>,
  ) {
  }

  public onVehicleSelected(item: GetVehiclesShowRoomCatalogItem) {
    this.selectedVehicle = item;
    this.store$.dispatch(VehicleShowRoomActions.SELECT_VEHICLE({vehicle: item}));
  }

  public isSelected(item: GetVehiclesShowRoomCatalogItem): boolean {
    return this.selectedVehicle && this.selectedVehicle.entityId === item.entityId;
  }
}
