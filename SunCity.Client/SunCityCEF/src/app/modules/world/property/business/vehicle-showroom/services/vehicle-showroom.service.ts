import {Injectable} from '@angular/core';
import {RageMPService} from '../../../../../../services/ragemp/ragemp.service';
import {Color} from 'ngx-color';
import {VehicleHashEnumDescriptionValue} from '../../../../../../http/GameHttpClient';

@Injectable({
  providedIn: 'root'
})
export class VehicleShowroomService {
  constructor(
              private readonly rageService: RageMPService
  ) {
  }

  public onClientMenuClose() {
    this.rageService.callClientEvent('CEF:Property:Business:VehicleShowRoom:Client:Menu.Close');
  }

  public onClientVehicleModelChange(model: VehicleHashEnumDescriptionValue) {
    this.rageService.callClientEvent('CEF:Property:Business:VehicleShowRoom:ChangeVehicleModel', model);
  }

  public onClientVehicleFirstColorChange(color: Color) {
    this.rageService.callClientEvent('CEF:Property:Business:VehicleShowRoom:ChangeFirstVehicleColor', color.rgb);
  }

  public onClientVehicleSecondColorChange(color: Color) {
    this.rageService.callClientEvent('CEF:Property:Business:VehicleShowRoom:ChangeSecondVehicleColor', color.rgb);
  }
}
