import {createFeatureSelector, createSelector} from '@ngrx/store';
import {BankClientStoreModuleName, BankClientStoreState} from './bank-client-store-state';
import {
  BankAuthorizationForm,
  BankCardPreviewModel,
  GetWorldPropertyInformationResponse, IOperationError,
  PropertyId
} from '../../../../../../../http/GameHttpClient';

export class BankClientStoreSelectors {
  static readonly store = createFeatureSelector<BankClientStoreState>(BankClientStoreModuleName);

  static readonly error = createSelector(BankClientStoreSelectors.store, (state: BankClientStoreState): IOperationError => state.error);

  static readonly property = createSelector(BankClientStoreSelectors.store, (state: BankClientStoreState): GetWorldPropertyInformationResponse => state.property);
  static readonly propertyId = createSelector(BankClientStoreSelectors.property, (property: GetWorldPropertyInformationResponse): PropertyId => property ? property.propertyId : undefined);


  static readonly authorization = createSelector(BankClientStoreSelectors.store, (state: BankClientStoreState): BankAuthorizationForm => state.authorization);
  static readonly bankCards = createSelector(BankClientStoreSelectors.store, (state: BankClientStoreState): BankCardPreviewModel[] => state.bankCards);

}
