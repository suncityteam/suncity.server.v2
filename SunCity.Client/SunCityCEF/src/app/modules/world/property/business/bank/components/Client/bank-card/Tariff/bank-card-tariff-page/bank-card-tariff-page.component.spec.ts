import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BankCardTariffPageComponent } from './bank-card-tariff-page.component';

describe('BankCardTariffPageComponent', () => {
  let component: BankCardTariffPageComponent;
  let fixture: ComponentFixture<BankCardTariffPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BankCardTariffPageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BankCardTariffPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
