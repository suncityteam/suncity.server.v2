import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VehicleShowroomOrderComponent } from './vehicle-showroom-order.component';

describe('VehicleShowroomOrderComponent', () => {
  let component: VehicleShowroomOrderComponent;
  let fixture: ComponentFixture<VehicleShowroomOrderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VehicleShowroomOrderComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VehicleShowroomOrderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
