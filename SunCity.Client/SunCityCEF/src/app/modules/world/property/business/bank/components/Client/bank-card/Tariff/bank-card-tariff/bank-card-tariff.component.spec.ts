import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BankCardTariffComponent } from './bank-card-tariff.component';

describe('BankCardTariffComponent', () => {
  let component: BankCardTariffComponent;
  let fixture: ComponentFixture<BankCardTariffComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BankCardTariffComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BankCardTariffComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
