import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VehicleShowroomColorComponent } from './vehicle-showroom-color.component';

describe('VehicleShowroomColorComponent', () => {
  let component: VehicleShowroomColorComponent;
  let fixture: ComponentFixture<VehicleShowroomColorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VehicleShowroomColorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VehicleShowroomColorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
