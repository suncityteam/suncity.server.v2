import {Control} from '../../../../../../../../shared/components/form-groups/form-group-adapter/form-group-control';
import {FormControl} from '@angular/forms';
import {FormGroupAdapter} from '../../../../../../../../shared/components/form-groups/form-group-adapter/form-group-adapter';

export class OpenCardFormGroup extends FormGroupAdapter {

  @Control()
  public name: FormControl;

  @Control()
  public tariffId: FormControl;

  @Control()
  public tariffName: FormControl;

  @Control()
  public pinCode: FormControl;

  @Control()
  public secureWord: FormControl;
}
