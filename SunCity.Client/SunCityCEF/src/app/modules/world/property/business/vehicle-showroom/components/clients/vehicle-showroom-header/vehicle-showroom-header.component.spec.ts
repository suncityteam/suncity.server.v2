import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VehicleShowroomHeaderComponent } from './vehicle-showroom-header.component';

describe('VehicleShowroomHeaderComponent', () => {
  let component: VehicleShowroomHeaderComponent;
  let fixture: ComponentFixture<VehicleShowroomHeaderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VehicleShowroomHeaderComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VehicleShowroomHeaderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
