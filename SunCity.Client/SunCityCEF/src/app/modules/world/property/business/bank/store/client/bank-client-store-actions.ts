import {createAction, props} from '@ngrx/store';
import {
  BankAuthorizationForm, BankGetCardInformationResponse, BankCardPreviewModel,
  GetWorldPropertyInformationResponse, IOperationError,
  PropertyId, IPropertyId
} from '../../../../../../../http/GameHttpClient';


export class BankClientStoreActions {
  public static readonly INITIALIZE = createAction('BANK_CLIENT: INITIALIZE', props<{ propertyId: IPropertyId }>());
  public static readonly INITIALIZE_SUCCESSFULLY = createAction('BANK_CLIENT: INITIALIZE_SUCCESSFULLY', props<{ property: GetWorldPropertyInformationResponse }>());
  public static readonly INITIALIZE_FAILED = createAction('BANK_CLIENT: INITIALIZE_FAILED', props<{ error: IOperationError }>());

  public static readonly AUTHORIZATION = createAction('BANK_CLIENT: AUTHORIZATION', props<{ form: BankAuthorizationForm }>());
  public static readonly AUTHORIZATION_SUCCESSFULLY = createAction('BANK_CLIENT: AUTHORIZATION_SUCCESSFULLY', props<{ form: BankAuthorizationForm }>());
  public static readonly AUTHORIZATION_FAILED = createAction('BANK_CLIENT: AUTHORIZATION_FAILED', props<{ error: IOperationError }>());

  public static readonly FETCH_BANK_CARD_INFORMATION = createAction('BANK_CLIENT: FETCH_BANK_CARD_INFORMATION');
  public static readonly FETCH_BANK_CARD_INFORMATION_SUCCESSFULLY = createAction('BANK_CLIENT: FETCH_BANK_CARD_INFORMATION_SUCCESSFULLY', props<{ bankCard: BankGetCardInformationResponse }>());
  public static readonly FETCH_BANK_CARD_INFORMATION_FAILED = createAction('BANK_CLIENT: FETCH_BANK_CARD_INFORMATION_FAILED', props<{ error: IOperationError }>());

  public static readonly FETCH_BANK_CARD_LIST = createAction('BANK_CLIENT: FETCH_BANK_CARD_LIST');
  public static readonly FETCH_BANK_CARD_LIST_SUCCESSFULLY = createAction('BANK_CLIENT: FETCH_BANK_CARD_LIST_SUCCESSFULLY', props<{ bankCards: BankCardPreviewModel[] }>());
  public static readonly FETCH_BANK_CARD_LIST_FAILED = createAction('BANK_CLIENT: FETCH_BANK_CARD_LIST_FAILED', props<{ error: IOperationError }>());
}
