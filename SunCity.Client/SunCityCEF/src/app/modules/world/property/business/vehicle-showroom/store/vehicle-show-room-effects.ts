import {Injectable} from '@angular/core';
import {ActionType, select, Store} from '@ngrx/store';
import 'rxjs/add/operator/withLatestFrom';


import {map, switchMap, catchError, withLatestFrom, mergeMap, tap} from 'rxjs/operators';
import {VehicleShowRoomActions} from './vehicle-show-room-actions';
import {VehicleShowroomService} from '../services/vehicle-showroom.service';

import {Actions, Effect, ofType} from '@ngrx/effects';
import {EMPTY, of} from 'rxjs';
import {VehicleShowRoomState} from './vehicle-show-room-state';

import {
  PropertyId, VehicleHashEnumDescription,
  VehicleShowRoomHttpClient
} from '../../../../../../http/GameHttpClient';
import {Color} from 'ngx-color';
import VehicleShowRoomSelectors from './vehicle-show-room-selectors';

@Injectable()
export class VehicleShowRoomEffects {
  constructor(
    private readonly actions$: Actions,
    private readonly store$: Store<VehicleShowRoomState>,
    private readonly service: VehicleShowroomService,
    private readonly httpClient: VehicleShowRoomHttpClient
  ) {
  }

  @Effect()
  initialize$ = this.actions$.pipe(
    ofType(VehicleShowRoomActions.INITIALIZE),
    mergeMap(() => of(VehicleShowRoomActions.INITIALIZE_SUCCESSFULLY()))
  );


  @Effect()
  initializeOk$ = this.actions$.pipe(
    ofType(VehicleShowRoomActions.INITIALIZE_SUCCESSFULLY),
    mergeMap(() => of(VehicleShowRoomActions.FETCH_CATALOG()))
  );

  @Effect({dispatch: false})
  onChangeVehicleModel = this.actions$.pipe(
    ofType(VehicleShowRoomActions.SELECT_VEHICLE),
    withLatestFrom(this.store$.pipe(select(VehicleShowRoomSelectors.vehicleModel))),
    tap(([action, vehicleModel]: [any, VehicleHashEnumDescription]) => {
        this.service.onClientVehicleModelChange(vehicleModel.value);
      }
    )
  );

  @Effect({dispatch: false})
  onChangeVehicleFirstColor = this.actions$.pipe(
    ofType(VehicleShowRoomActions.CHANGE_FIRST_COLOR),
    withLatestFrom(this.store$.pipe(select(VehicleShowRoomSelectors.vehicleFirstColor))),
    tap(([action, color]: [any, Color]) =>
      this.service.onClientVehicleFirstColorChange(color)
    )
  );

  @Effect({dispatch: false})
  onChangeVehicleSecondColor = this.actions$.pipe(
    ofType(VehicleShowRoomActions.CHANGE_SECOND_COLOR),
    withLatestFrom(this.store$.pipe(select(VehicleShowRoomSelectors.vehicleSecondColor))),
    tap(([action, color]: [any, Color]) =>
      this.service.onClientVehicleSecondColorChange(color)
    )
  );


  @Effect({dispatch: false})
  onCloseModal = this.actions$.pipe(
    ofType(VehicleShowRoomActions.AFTER_CLOSE_WINDOW),
    tap(() =>
      this.service.onClientMenuClose()
    )
  );

  @Effect()
  fetchCatalog = this.actions$.pipe(
    ofType(VehicleShowRoomActions.FETCH_CATALOG),
    withLatestFrom(this.store$.pipe(select(VehicleShowRoomSelectors.propertyId))),
    switchMap(([action, propertyId]: [any, PropertyId]) =>
      this.httpClient.getCatalog(propertyId.id).pipe(
        map(result => VehicleShowRoomActions.FETCH_CATALOG_SUCCESSFULLY({catalog: result.content}),
          catchError(error => EMPTY)
        )))
  );

}
