import {FormGroupAdapter} from '../../../../../../../../shared/components/form-groups/form-group-adapter/form-group-adapter';
import {Control} from '../../../../../../../../shared/components/form-groups/form-group-adapter/form-group-control';
import {FormControl} from '@angular/forms';

export class BankCardAuthorizationFormGroup extends FormGroupAdapter {
  @Control()
  public bankCardId: FormControl;

  @Control()
  public bankCardName: FormControl;

  @Control()
  public pinCode: FormControl;
}
