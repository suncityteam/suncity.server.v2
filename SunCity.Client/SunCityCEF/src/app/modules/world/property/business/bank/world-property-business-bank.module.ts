import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {RouterModule, Routes} from '@angular/router';
import {BankClientWelcomePageComponent} from './components/Client/bank-card/Welcome/bank-client-welcome-page/bank-client-welcome-page.component';
import {BankCardTariffPageComponent} from './components/Client/bank-card/Tariff/bank-card-tariff-page/bank-card-tariff-page.component';
import {OpenCardPageComponent} from './components/Client/bank-card/OpenCard/open-card-page/open-card-page.component';
import {BankCardAuthorizationPageComponent} from './components/Client/bank-card/Authorization/bank-card-authorization-page/bank-card-authorization-page.component';
import {BankCardTariffComponent} from './components/Client/bank-card/Tariff/bank-card-tariff/bank-card-tariff.component';
import {BankCardOpenComponent} from './components/Client/bank-card/Welcome/bank-card-open/bank-card-open.component';
import {BankCardPreviewComponent} from './components/Client/bank-card/Welcome/bank-card-preview/bank-card-preview.component';
import {ReactiveFormsModule} from '@angular/forms';
import {SharedModule} from '../../../../shared/shared.module';
import {BankWindowLayoutComponent} from './components/Client/bank-window-layout/bank-window-layout.component';
import { BankCardInformationPageComponent } from './components/Client/bank-card/Menu/Information/bank-card-information-page/bank-card-information-page.component';
import { BankCardDepositPageComponent } from './components/Client/bank-card/Menu/Deposit/bank-card-deposit-page/bank-card-deposit-page.component';
import { BankCardTransferPageComponent } from './components/Client/bank-card/Menu/Transfer/bank-card-transfer-page/bank-card-transfer-page.component';
import { BankCardInformationComponent } from './components/Client/bank-card/Menu/Information/bank-card-information/bank-card-information.component';
import { BankCardTransactionsComponent } from './components/Client/bank-card/Menu/Information/bank-card-transactions/bank-card-transactions.component';
import { BankCardStatusPipe } from './pipes/bank-card-status/bank-card-status.pipe';
import { BankCardTransactionComponent } from './components/Client/bank-card/Menu/Information/bank-card-transaction/bank-card-transaction.component';
import { BankCardWithdrawPageComponent } from './components/Client/bank-card/Menu/Withdraw/bank-card-withdraw-page/bank-card-withdraw-page.component';
import {CurrentBankCardStateService} from './states/current-bank-card-state.service';
import {CurrentBankPropertyStateService} from './states/current-bank-property-state.service';
import { BankCardOperationCompletePageComponent } from './components/Client/bank-card/Menu/OperationComplete/bank-card-operation-complete-page/bank-card-operation-complete-page.component';
import { BankCardTransactionsPageComponent } from './components/Client/bank-card/Menu/Transactions/bank-card-transactions-page/bank-card-transactions-page.component';
import { BankCardServicesPageComponent } from './components/Client/bank-card/Menu/Services/bank-card-services-page/bank-card-services-page.component';
import {BankClientStoreModule} from './store/client/bank-client-store-module';

const WorldPropertyBusinessBankRoutes: Routes = [
  {
    component: BankWindowLayoutComponent,
    path: 'World/Property/Bank/Card/Client',
    children: [
      {
        path: 'Menu',
        component: BankClientWelcomePageComponent
      },
      {
        path: 'Tariff',
        component: BankCardTariffPageComponent
      },
      {
        path: 'OpenCard',
        component: OpenCardPageComponent
      },
      {
        path: 'Authorization',
        component: BankCardAuthorizationPageComponent
      },
      {
        path: 'Authorization/:propertyId',
        component: BankCardAuthorizationPageComponent
      },
      {
        path: 'Information',
        component: BankCardInformationPageComponent
      },
      {
        path: 'Deposit',
        component: BankCardDepositPageComponent
      },
      {
        path: 'Withdraw',
        component: BankCardWithdrawPageComponent
      },
      {
        path: 'Transfer',
        component: BankCardTransferPageComponent
      },
      {
        path: 'Transactions',
        component: BankCardTransactionsPageComponent
      },
      {
        path: 'Services',
        component: BankCardServicesPageComponent
      },
      {
        path: 'OperationComplete/:transactionId',
        component: BankCardOperationCompletePageComponent
      },
    ]
  }


];

@NgModule({
  declarations:
    [
      BankClientWelcomePageComponent,
      BankCardPreviewComponent,
      BankCardOpenComponent,
      BankCardTariffPageComponent,
      BankCardTariffComponent,
      OpenCardPageComponent,
      BankCardAuthorizationPageComponent,
      BankWindowLayoutComponent,
      BankCardInformationPageComponent,
      BankCardDepositPageComponent,
      BankCardTransferPageComponent,
      BankCardInformationComponent,
      BankCardTransactionsComponent,
      BankCardStatusPipe,
      BankCardTransactionComponent,
      BankCardWithdrawPageComponent,
      BankCardOperationCompletePageComponent,
      BankCardTransactionsPageComponent,
      BankCardServicesPageComponent
    ],
  imports: [
    CommonModule,
    SharedModule.forRoot(),
    RouterModule.forChild(WorldPropertyBusinessBankRoutes),
    ReactiveFormsModule,
    BankClientStoreModule,
  ], exports: [
    SharedModule,
  ],
  providers: [
    CurrentBankCardStateService,
    CurrentBankPropertyStateService,
  ]
})
export class WorldPropertyBusinessBankModule {
}
