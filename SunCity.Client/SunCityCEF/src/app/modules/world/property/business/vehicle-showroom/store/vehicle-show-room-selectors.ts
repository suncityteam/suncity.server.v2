import {createFeatureSelector, createSelector} from '@ngrx/store';
import {VehicleShowRoomState} from './vehicle-show-room-state';
import {VehicleShowRoomModuleName} from './vehicle-show-room-actions';
import {VehicleShowroomListOrderBy} from '../enums/vehicle-showroom-list-order.by';
import {OrderByMode} from '../../../../../../enums/order-by-mode';
import {removeDuplicates} from '../../../../../../extensions/array-extensions';
import {
  GetVehiclesShowRoomCatalogItem,
  GetVehiclesShowRoomCatalogResponse, PropertyId, PropertyName, VehicleHashEnumDescription,
  VehicleModelClassEnumDescription
} from '../../../../../../http/GameHttpClient';
import {Color} from 'ngx-color';

export class VehicleShowRoomSelectors {
  static readonly store = createFeatureSelector<VehicleShowRoomState>(VehicleShowRoomModuleName);
  static readonly propertyId = createSelector(VehicleShowRoomSelectors.store, (state: VehicleShowRoomState): PropertyId => state.propertyId);
  static readonly catalog = createSelector(VehicleShowRoomSelectors.store, (state: VehicleShowRoomState): GetVehiclesShowRoomCatalogResponse => state.catalog);

  static readonly propertyName = createSelector(VehicleShowRoomSelectors.catalog, (catalog: GetVehiclesShowRoomCatalogResponse): string => {
    if (catalog) {
      return catalog.name ? catalog.name.name : 'Самые лучшие тачки в LS';
    }
    return 'Автосалон';
  });

  static readonly selectedVehicle = createSelector(VehicleShowRoomSelectors.store, (state: VehicleShowRoomState): GetVehiclesShowRoomCatalogItem | undefined => state.selectedVehicle);

  static readonly vehicleModel = createSelector(VehicleShowRoomSelectors.selectedVehicle, (vehicle: GetVehiclesShowRoomCatalogItem): VehicleHashEnumDescription | undefined =>
    vehicle ? vehicle.information.model : undefined
  );

  static readonly vehicleFirstColor = createSelector(VehicleShowRoomSelectors.store, (state: VehicleShowRoomState): Color =>
    state.firstColor
  );

  static readonly vehicleSecondColor = createSelector(VehicleShowRoomSelectors.store, (state: VehicleShowRoomState): Color => state.secondColor);

  static readonly orderByField = createSelector(VehicleShowRoomSelectors.store, (state: VehicleShowRoomState): VehicleShowroomListOrderBy => state.orderByField);
  static readonly orderByMode = createSelector(VehicleShowRoomSelectors.store, (state: VehicleShowRoomState): OrderByMode => state.orderByMode);

  static readonly selectedCategory = createSelector(VehicleShowRoomSelectors.store, (state: VehicleShowRoomState): VehicleModelClassEnumDescription => state.selectedCategory);

  static readonly itemsInCatalog = createSelector(VehicleShowRoomSelectors.catalog, (catalog: GetVehiclesShowRoomCatalogResponse): GetVehiclesShowRoomCatalogResponse[] =>
    catalog ? catalog.items : []
  );

  static readonly itemsInCatalogCategory = createSelector(VehicleShowRoomSelectors.itemsInCatalog, VehicleShowRoomSelectors.selectedCategory,
    (catalogVehicles: GetVehiclesShowRoomCatalogItem[], category: VehicleModelClassEnumDescription) => {
      if (category) {
        return catalogVehicles.filter(item => item.information.category.value === category.value);
      }
      return catalogVehicles;
    });

  static readonly categories = createSelector(VehicleShowRoomSelectors.itemsInCatalog, (catalogVehicles: GetVehiclesShowRoomCatalogItem[]): VehicleModelClassEnumDescription[] => {
    const categories = catalogVehicles.map(item => item.information.category);
    return removeDuplicates(categories, 'Value');
  });

  static readonly displayVehicles =
    createSelector(VehicleShowRoomSelectors.itemsInCatalogCategory, VehicleShowRoomSelectors.orderByField, VehicleShowRoomSelectors.orderByMode,
      (catalogVehicles: GetVehiclesShowRoomCatalogItem[], orderBy: VehicleShowroomListOrderBy, orderMode: OrderByMode) => {
        if (orderMode === OrderByMode.Ascending) {
          return getVehicleShowRoomDisplayVehiclesOrderedByAscending(catalogVehicles, orderBy);
        }
        return getVehicleShowRoomDisplayVehiclesOrderedByDescending(catalogVehicles, orderBy);
      });
}

const onSortByModelName = (a: GetVehiclesShowRoomCatalogItem, b: GetVehiclesShowRoomCatalogItem): number => {
  const a$ = a.information.model.description.toLowerCase();
  const b$ = b.information.model.description.toLowerCase();

  return a$ && b$ ? a$.localeCompare(b$, undefined, {sensitivity: 'base'}) : 0;
};

const getVehicleShowRoomDisplayVehiclesOrderedByAscending = (catalogVehicles: GetVehiclesShowRoomCatalogItem[], orderBy: VehicleShowroomListOrderBy) => {
  const vehicles: GetVehiclesShowRoomCatalogItem[] = [...catalogVehicles];
  if (orderBy === VehicleShowroomListOrderBy.Cost) {
    return vehicles.sort((a, b) => a.cost.amount - b.cost.amount);
  } else if (orderBy === VehicleShowroomListOrderBy.Name) {
    return vehicles.sort((a, b) => onSortByModelName(a, b));
  } else if (orderBy === VehicleShowroomListOrderBy.Popular) {
    // this.catalog.Items.sort((a, b) => a.cost.amount - b.cost.amount);
  } else if (orderBy === VehicleShowroomListOrderBy.Speed) {
    return vehicles.sort((a, b) => a.information.maxSpeed - b.information.maxSpeed);
  } else if (orderBy === VehicleShowroomListOrderBy.Handleability) {
    return vehicles.sort((a, b) => a.information.handleability - b.information.handleability);
  } else if (orderBy === VehicleShowroomListOrderBy.Capacity) {
    return vehicles.sort((a, b) => a.information.bagCapacity - b.information.bagCapacity);
  }
  return vehicles;
};

const getVehicleShowRoomDisplayVehiclesOrderedByDescending = (catalogVehicles: GetVehiclesShowRoomCatalogItem[], orderBy: VehicleShowroomListOrderBy) => {
  const vehicles: GetVehiclesShowRoomCatalogItem[] = [...catalogVehicles];
  if (orderBy === VehicleShowroomListOrderBy.Cost) {
    return vehicles.sort((a, b) => b.cost.amount - a.cost.amount);
  } else if (orderBy === VehicleShowroomListOrderBy.Name) {
    return vehicles.sort((a, b) => onSortByModelName(b, a));
  } else if (orderBy === VehicleShowroomListOrderBy.Popular) {
    // this.catalog.Items.sort((a, b) => b.cost.amount - a.cost.amount);
  } else if (orderBy === VehicleShowroomListOrderBy.Speed) {
    return vehicles.sort((a, b) => b.information.maxSpeed - a.information.maxSpeed);
  } else if (orderBy === VehicleShowroomListOrderBy.Handleability) {
    return vehicles.sort((a, b) => b.information.handleability - a.information.handleability);
  } else if (orderBy === VehicleShowroomListOrderBy.Capacity) {
    return vehicles.sort((a, b) => b.information.bagCapacity - a.information.bagCapacity);
  }
  return vehicles;
};

export default VehicleShowRoomSelectors;
