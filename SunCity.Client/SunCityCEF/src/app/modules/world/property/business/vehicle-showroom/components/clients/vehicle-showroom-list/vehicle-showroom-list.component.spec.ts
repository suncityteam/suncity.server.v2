import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VehicleShowroomListComponent } from './vehicle-showroom-list.component';

describe('VehicleShowroomListComponent', () => {
  let component: VehicleShowroomListComponent;
  let fixture: ComponentFixture<VehicleShowroomListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VehicleShowroomListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VehicleShowroomListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
