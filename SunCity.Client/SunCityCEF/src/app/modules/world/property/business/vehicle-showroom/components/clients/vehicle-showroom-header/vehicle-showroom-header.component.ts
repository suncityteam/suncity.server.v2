import {Component, Input, OnInit} from '@angular/core';
import {Store} from '@ngrx/store';
import {VehicleShowRoomState} from '../../../store/vehicle-show-room-state';
import {VehicleModelClassEnumDescription} from '../../../../../../../../http/GameHttpClient';
import VehicleShowRoomSelectors from '../../../store/vehicle-show-room-selectors';
import {VehicleShowRoomActions} from '../../../store/vehicle-show-room-actions';

@Component({
  selector: 'app-vehicle-showroom-header',
  templateUrl: './vehicle-showroom-header.component.html',
  styleUrls: ['./vehicle-showroom-header.component.scss']
})
export class VehicleShowroomHeaderComponent implements OnInit {
  public readonly category$ = this.store$.select(VehicleShowRoomSelectors.selectedCategory);
  public selectedCategory: VehicleModelClassEnumDescription;

  constructor(private readonly store$: Store<VehicleShowRoomState>) {
  }

  ngOnInit() {
    this.category$.subscribe(category => this.selectedCategory = category);
  }

  onVehicleShowroomWindowClosed() {
    this.store$.dispatch(VehicleShowRoomActions.AFTER_CLOSE_WINDOW());
  }
}
