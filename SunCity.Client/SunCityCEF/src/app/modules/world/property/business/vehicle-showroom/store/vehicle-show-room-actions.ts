import {createAction, props} from '@ngrx/store';
import {VehicleShowroomListOrderBy} from '../enums/vehicle-showroom-list-order.by';
import {OrderByMode} from '../../../../../../enums/order-by-mode';
import {Color} from 'ngx-color';
import {
  GetVehiclesShowRoomCatalogItem,
  GetVehiclesShowRoomCatalogResponse, IOperationError, PropertyId,
  VehicleModelClassEnumDescription
} from '../../../../../../http/GameHttpClient';

export const VehicleShowRoomModuleName = 'VehicleShowRoom';

export class VehicleShowRoomActions {
  public static readonly INITIALIZE = createAction('INITIALIZE', props<{ propertyId: PropertyId }>());
  public static readonly INITIALIZE_SUCCESSFULLY = createAction('INITIALIZE_SUCCESSFULLY');

  public static readonly FETCH_CATALOG = createAction('FETCH_CATALOG');
  public static readonly FETCH_CATALOG_SUCCESSFULLY = createAction('FETCH_CATALOG_SUCCESSFULLY', props<{ catalog: GetVehiclesShowRoomCatalogResponse }>());
  public static readonly FETCH_CATALOG_FAILED = createAction('FETCH_CATALOG_FAILED', props<{ error: IOperationError }>());

  public static readonly SWITCH_ORDER_BY_FIELD = createAction('SWITCH_ORDER_BY_FIELD', props<{ orderBy: VehicleShowroomListOrderBy }>());
  public static readonly SWITCH_ORDER_BY_MODE = createAction('SWITCH_ORDER_BY_MODE', props<{ orderMode: OrderByMode }>());

  public static readonly SELECT_VEHICLE = createAction('SELECT_VEHICLE', props<{ vehicle: GetVehiclesShowRoomCatalogItem }>());
  public static readonly SELECT_CATEGORY = createAction('SELECT_CATEGORY', props<{ category: VehicleModelClassEnumDescription }>());

  public static readonly CHANGE_FIRST_COLOR = createAction('CHANGE_FIRST_COLOR', props<{ color: Color }>());
  public static readonly CHANGE_SECOND_COLOR = createAction('CHANGE_SECOND_COLOR', props<{ color: Color }>());

  public static readonly AFTER_CLOSE_WINDOW = createAction('AFTER_CLOSE_WINDOW');


}

