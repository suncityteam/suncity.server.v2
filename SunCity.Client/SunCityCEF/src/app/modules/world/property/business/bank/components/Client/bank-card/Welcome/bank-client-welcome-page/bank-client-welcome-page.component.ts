import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Params, Router} from '@angular/router';
import {LocalStorage} from 'ngx-store-9';
import {
  BankCardAccountHttpClient, BankCardId,
  BankCardPreviewModel,
  BankCardPreviewModelArrayOperationResponse, PropertyId
} from '../../../../../../../../../../http/GameHttpClient';

@Component({
  selector: 'app-bank-client-welcome-page',
  templateUrl: './bank-client-welcome-page.component.html',
  styleUrls: ['./bank-client-welcome-page.component.scss']
})
export class BankClientWelcomePageComponent implements OnInit {
  public bankPropertyId: PropertyId;
  public bankCards: BankCardPreviewModel[];

  constructor(
    private readonly router: Router,
    private readonly activatedRoute: ActivatedRoute,
    private readonly bankCardHttpClient: BankCardAccountHttpClient,
  ) {
  }

  ngOnInit() {
    this.activatedRoute.params.subscribe(params => this.onChangeRouteParams(params));
  }

  private onChangeRouteParams(params: Params): void {
    this.bankPropertyId = new PropertyId({id: params.propertyId});
    if (this.bankPropertyId) {
      this.bankCardHttpClient.getPlayerBankCardList(this.bankPropertyId.id).subscribe(res => this.onGetBankCardList(res));
    }
  }

  private onGetBankCardList(res: BankCardPreviewModelArrayOperationResponse) {
    this.bankCards = res.content;
  }

  public onBankCardCreate(): void {
    this.router.navigate([`World/Property/Bank/Card/Client/OpenCard/${this.bankPropertyId.id}`]);
  }

  public onBankCardSelected(bankCardId: BankCardId): void {
    this.router.navigate([`World/Property/Bank/Card/Client/Authorization/${this.bankPropertyId.id}`]);
  }
}
