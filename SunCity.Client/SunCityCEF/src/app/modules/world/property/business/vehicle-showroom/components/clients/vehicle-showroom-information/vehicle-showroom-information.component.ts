import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {PurchaseMethod} from '../../../../../../../../enums/purchase/purchase-method';
import {Store} from '@ngrx/store';
import {VehicleShowRoomState} from '../../../store/vehicle-show-room-state';
import {BankCardPaymentModalModel} from '../../../../../../../shared/components/Payments/bank-card-payment-modal/bank-card-payment-modal.model';
import {BankCardPaymentModalComponent} from '../../../../../../../shared/components/Payments/bank-card-payment-modal/bank-card-payment-modal.component';
import {VehicleShowroomService} from '../../../services/vehicle-showroom.service';
import {ModalService} from '../../../../../shared/services/modal/modal.service';
import {VehicleShowRoomActions} from '../../../store/vehicle-show-room-actions';
import {Color} from 'ngx-color';
import {ToastrService} from 'ngx-toastr';
import {
  BankAuthorizationForm,
  GetVehiclesShowRoomCatalogItem, IBankAuthorizationForm, PropertyId,
  PurchaseVehicleInVehicleShowRoomRequest,
  VehicleShowRoomHttpClient
} from '../../../../../../../../http/GameHttpClient';
import VehicleShowRoomSelectors from '../../../store/vehicle-show-room-selectors';

@Component({
  selector: 'app-vehicle-showroom-information',
  templateUrl: './vehicle-showroom-information.component.html',
  styleUrls: ['./vehicle-showroom-information.component.scss']
})
export class VehicleShowroomInformationComponent implements OnInit {
  public readonly vehicle$ = this.store$.select(VehicleShowRoomSelectors.selectedVehicle);

  public vehicle: GetVehiclesShowRoomCatalogItem;

  @Output()
  public readonly purchaseMethodSelected = new EventEmitter<PurchaseMethod>();

  constructor(
    private readonly store$: Store<VehicleShowRoomState>,
    private readonly service: VehicleShowroomService,
    private readonly httpClient: VehicleShowRoomHttpClient,
    private readonly modalService: ModalService,
    private readonly toastr: ToastrService
  ) {
  }

  public onPurchaseMethodSelected(purchaseMethod: PurchaseMethod): void {
    if (purchaseMethod === PurchaseMethod.Cash) {
      // this.toastr.error('Покупка дома с помощью наличных не доступна!');
    } else if (purchaseMethod === PurchaseMethod.BankCard) {

      const model: BankCardPaymentModalModel = {
        description: `Покупка авто ${this.vehicle.information.model.description}`,
        cost: this.vehicle.cost.amount,
        paymentMethod: (form) => this.onPaymentConfirm(form),
      };

      this.modalService.show<BankCardPaymentModalModel>(BankCardPaymentModalComponent, model);
    }
  }

  private onPaymentConfirm(form: IBankAuthorizationForm) {
    return this.httpClient.purchase(new PurchaseVehicleInVehicleShowRoomRequest({
      firstColor: 1133,
      secondColor: 8894,
      payment: new BankAuthorizationForm(form),
      propertyId: new PropertyId({id: 'a33a8807-e440-4746-83f2-3b4b9012f7a7'})
    }));
  }


  ngOnInit(): void {
    this.vehicle$.subscribe(vehicle => this.vehicle = vehicle);
  }

  onFirstColorSelected($event: Color) {
    this.store$.dispatch(VehicleShowRoomActions.CHANGE_FIRST_COLOR({color: $event}));
  }

  onSecondColorSelected($event: Color) {
    this.store$.dispatch(VehicleShowRoomActions.CHANGE_SECOND_COLOR({color: $event}));
  }
}
