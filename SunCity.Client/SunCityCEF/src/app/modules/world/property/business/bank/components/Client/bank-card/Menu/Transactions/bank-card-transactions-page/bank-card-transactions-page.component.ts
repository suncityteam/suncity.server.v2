import {Component, OnInit} from '@angular/core';
import {ToastrService} from 'ngx-toastr';
import {Router} from '@angular/router';
import {CurrentBankCardStateService} from '../../../../../../states/current-bank-card-state.service';
import {
  BankAuthorizationForm,
  BankCardAccountHttpClient,
  BankCardTransactionPreviewModel, BankCardTransactionPreviewModelArrayOperationResponse, BankGetCardTransactionsRequest,
  IOperationError
} from '../../../../../../../../../../../http/GameHttpClient';

@Component({
  selector: 'app-bank-card-transactions-page',
  templateUrl: './bank-card-transactions-page.component.html',
  styleUrls: ['./bank-card-transactions-page.component.scss']
})
export class BankCardTransactionsPageComponent implements OnInit {
  public operationError: IOperationError | undefined;
  public model: BankCardTransactionPreviewModel[];

  constructor(
    private readonly toastr: ToastrService,
    private readonly router: Router,
    private readonly bankCardService: BankCardAccountHttpClient,
    private readonly currentBankCard: CurrentBankCardStateService
  ) {
  }

  ngOnInit() {
    if (this.currentBankCard.model) {
      this.bankCardService.getBankCardTransactions(new BankGetCardTransactionsRequest({
        credentials: new BankAuthorizationForm({
          payerBankCardId: this.currentBankCard.entityId,
          pinCode: this.currentBankCard.pinCode
        })
      })).subscribe(res => this.onGetBankCardResponse(res));
    } else {
      this.toastr.error('Неудалось получить информацию о банковской карте');
      this.router.navigate(['/']);
    }
  }

  private onGetBankCardResponse(res: BankCardTransactionPreviewModelArrayOperationResponse): void {
    this.operationError = res.error;
    this.model = res.content;
  }

}
