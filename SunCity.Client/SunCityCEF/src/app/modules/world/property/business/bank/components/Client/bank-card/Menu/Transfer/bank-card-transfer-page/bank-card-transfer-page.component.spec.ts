import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BankCardTransferPageComponent } from './bank-card-transfer-page.component';

describe('BankCardTransferPageComponent', () => {
  let component: BankCardTransferPageComponent;
  let fixture: ComponentFixture<BankCardTransferPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BankCardTransferPageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BankCardTransferPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
