import { Component, OnInit } from '@angular/core';
import {CurrentBankCardStateService} from '../../../states/current-bank-card-state.service';

@Component({
  selector: 'app-bank-window-layout',
  templateUrl: './bank-window-layout.component.html',
  styleUrls: ['./bank-window-layout.component.scss']
})
export class BankWindowLayoutComponent implements OnInit {

  constructor(
    public readonly currentBankCard: CurrentBankCardStateService
  ) { }

  ngOnInit() {
  }

}
