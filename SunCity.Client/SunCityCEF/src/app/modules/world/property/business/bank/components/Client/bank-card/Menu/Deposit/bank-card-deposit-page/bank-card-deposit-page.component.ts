import {Component, OnInit} from '@angular/core';
import {ToastrService} from 'ngx-toastr';
import {Router} from '@angular/router';
import {CurrentBankCardStateService} from '../../../../../../states/current-bank-card-state.service';
import {BankCardDepositFormGroup} from '../bank-card-deposit-form-group';
import {
  BankAuthorizationForm,
  BankCardAccountHttpClient,
  BankCardId, BankCardTransactionIdOperationResponse, BankDepositCardRequest,
  IOperationError
} from '../../../../../../../../../../../http/GameHttpClient';

@Component({
  selector: 'app-bank-card-deposit-page',
  templateUrl: './bank-card-deposit-page.component.html',
  styleUrls: ['./bank-card-deposit-page.component.scss']
})
export class BankCardDepositPageComponent implements OnInit {
  public operationError: IOperationError | undefined;
  public readonly FormGroup = new BankCardDepositFormGroup();

  constructor(
    private readonly toastr: ToastrService,
    private readonly router: Router,
    private readonly bankCardService: BankCardAccountHttpClient,
    private readonly currentBankCard: CurrentBankCardStateService,
  ) {

  }

  ngOnInit() {
    this.FormGroup.bankCardId.setValue(this.currentBankCard.model.entityId);
    this.FormGroup.bankCardNumber.setValue(this.currentBankCard.cardNumber);
  }

  onClickedDeposit(): void {
    const form = new BankDepositCardRequest({credentials: new BankAuthorizationForm({
        payerBankCardId: new BankCardId(this.currentBankCard.entityId),
        pinCode: this.currentBankCard.pinCode
      }),
      amount: parseInt(this.FormGroup.depositAmount.value, 10)
    });

    this.bankCardService.depositToBankCard(form).subscribe(res => this.onDepositResponse(res));
  }

  private onDepositResponse(res: BankCardTransactionIdOperationResponse): void {
    this.operationError = res.error;
    if (res.isCorrect) {
      this.toastr.success(`Вы успешно пополнили карту ${this.currentBankCard.cardNumber} на ${this.FormGroup.depositAmount.value}$`, 'Пополнение карты');
      this.router.navigate([`/World/Property/Bank/Card/Client/OperationComplete/123`]);
    }
  }
}
