import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BankCardServicesPageComponent } from './bank-card-services-page.component';

describe('BankCardServicesPageComponent', () => {
  let component: BankCardServicesPageComponent;
  let fixture: ComponentFixture<BankCardServicesPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BankCardServicesPageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BankCardServicesPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
