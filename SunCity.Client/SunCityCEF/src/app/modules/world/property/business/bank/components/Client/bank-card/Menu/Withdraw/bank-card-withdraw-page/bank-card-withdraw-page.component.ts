import {Component, OnInit} from '@angular/core';
import {ToastrService} from 'ngx-toastr';
import {Router} from '@angular/router';
import {CurrentBankCardStateService} from '../../../../../../states/current-bank-card-state.service';
import {BankCardWithdrawFormGroup} from '../bank-card-withdraw-form-group';
import {
  BankAuthorizationForm,
  BankCardAccountHttpClient, BankCardBalanceModelOperationResponse,
  BankCardTransactionIdOperationResponse, BankGetCardBalanceRequest, BankWithdrawCardRequest,
  IOperationError
} from '../../../../../../../../../../../http/GameHttpClient';

@Component({
  selector: 'app-bank-card-withdraw-page',
  templateUrl: './bank-card-withdraw-page.component.html',
  styleUrls: ['./bank-card-withdraw-page.component.scss']
})
export class BankCardWithdrawPageComponent implements OnInit {

  public operationError: IOperationError | undefined;
  public readonly FormGroup = new BankCardWithdrawFormGroup();

  constructor(
    private readonly toastr: ToastrService,
    private readonly router: Router,
    private readonly bankCardService: BankCardAccountHttpClient,
    private readonly currentBankCard: CurrentBankCardStateService,
  ) {

  }

  public ngOnInit() {
    this.FormGroup.bankCardId.setValue(this.currentBankCard.model.entityId);
    this.FormGroup.bankCardNumber.setValue(this.currentBankCard.cardNumber);
    this.bankCardService.getBankCardBalance(new BankGetCardBalanceRequest({
      credentials: new BankAuthorizationForm({
        payerBankCardId: this.currentBankCard.model.entityId,
        pinCode: this.currentBankCard.pinCode
      })
    })).subscribe(res => this.onGetBalanceResponse(res));
  }

  public onClickedWithdraw(): void {
    const form = new BankWithdrawCardRequest({
      credentials: new BankAuthorizationForm({
        payerBankCardId: this.currentBankCard.entityId,
        pinCode: this.currentBankCard.pinCode
      }),
      amount: parseInt(this.FormGroup.withdrawAmount.value, 10)
    });

    this.bankCardService.withdrawFromBankCard(form).subscribe(res => this.onWithdrawResponse(res));
  }

  private onWithdrawResponse(res: BankCardTransactionIdOperationResponse): void {
    this.operationError = res.error;
    if (res.isCorrect) {
      this.toastr.success(`Вы успешно сняли ${this.FormGroup.withdrawAmount.value}$ с карты ${this.currentBankCard.cardNumber}`, 'Снятие наличных');
      this.router.navigate([`/World/Property/Bank/Card/Client/OperationComplete/123`]);
    }
  }

  private onGetBalanceResponse(res: BankCardBalanceModelOperationResponse) {
    this.operationError = res.error;
    if (res.isCorrect) {
      this.FormGroup.bankCardBalance.setValue(res.content.balance);
    }
  }
}
