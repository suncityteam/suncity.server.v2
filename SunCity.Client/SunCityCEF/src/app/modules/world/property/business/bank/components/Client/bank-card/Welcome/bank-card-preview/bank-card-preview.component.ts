import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {BankCardId, BankCardPreviewModel} from '../../../../../../../../../../http/GameHttpClient';

@Component({
  selector: 'app-bank-card-preview',
  templateUrl: './bank-card-preview.component.html',
  styleUrls: ['./bank-card-preview.component.scss']
})
export class BankCardPreviewComponent implements OnInit {

  @Input() public readonly model: BankCardPreviewModel;
  @Output() public readonly selected = new EventEmitter<BankCardId>();

  constructor() {
  }

  ngOnInit() {
  }

  public onClicked(): void {
    this.selected.emit(this.model.entityId);
  }

}
