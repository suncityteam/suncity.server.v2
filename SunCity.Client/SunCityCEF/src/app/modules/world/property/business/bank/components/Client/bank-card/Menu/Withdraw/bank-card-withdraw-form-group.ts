import {FormGroupAdapter} from '../../../../../../../../../shared/components/form-groups/form-group-adapter/form-group-adapter';
import {FormControl} from '@angular/forms';
import {Control} from '../../../../../../../../../shared/components/form-groups/form-group-adapter/form-group-control';

export class BankCardWithdrawFormGroup extends FormGroupAdapter {
  @Control() public readonly bankCardId: FormControl;
  @Control() public readonly bankCardNumber: FormControl;
  @Control({disabled: true}) public readonly bankCardBalance: FormControl;

  @Control() public readonly withdrawAmount: FormControl;
}
