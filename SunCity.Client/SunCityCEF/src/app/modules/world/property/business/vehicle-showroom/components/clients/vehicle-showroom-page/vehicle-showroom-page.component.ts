import {Component, OnInit} from '@angular/core';
import {VehicleShowroomService} from '../../../services/vehicle-showroom.service';
import {ActivatedRoute, Params} from '@angular/router';

import {Store} from '@ngrx/store';
import {VehicleShowRoomActions} from '../../../store/vehicle-show-room-actions';
import {VehicleShowRoomState} from '../../../store/vehicle-show-room-state';
import {PropertyId} from '../../../../../../../../http/GameHttpClient';
import VehicleShowRoomSelectors from '../../../store/vehicle-show-room-selectors';
import {ToastrService} from 'ngx-toastr';

@Component({
  selector: 'app-vehicle-showroom-page',
  templateUrl: './vehicle-showroom-page.component.html',
  styleUrls: ['./vehicle-showroom-page.component.scss']
})
export class VehicleShowroomPageComponent implements OnInit {
  public readonly propertyName$ = this.store$.select(VehicleShowRoomSelectors.propertyName);

  constructor(
    private readonly store$: Store<VehicleShowRoomState>,
    private readonly service: VehicleShowroomService,
    private readonly activatedRoute: ActivatedRoute,
    private readonly toastr: ToastrService
  ) {

  }

  ngOnInit() {
    this.activatedRoute.params.subscribe(params => this.onChangeRouteParams(params));
  }

  private onChangeRouteParams(params: Params): void {
    this.toastr.info(`VehicleShowroomPageComponent: ${JSON.stringify(params)}`);
    const propertyId = new PropertyId({id: params.propertyId});
    if (propertyId) {
      this.store$.dispatch(VehicleShowRoomActions.INITIALIZE({propertyId}));
    }
  }

}



