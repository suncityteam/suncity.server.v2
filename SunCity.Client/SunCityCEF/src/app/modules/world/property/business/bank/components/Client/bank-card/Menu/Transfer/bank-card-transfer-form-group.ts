import {FormGroupAdapter} from '../../../../../../../../../shared/components/form-groups/form-group-adapter/form-group-adapter';
import {FormControl} from '@angular/forms';
import {Control} from '../../../../../../../../../shared/components/form-groups/form-group-adapter/form-group-control';

export class BankCardTransferFormGroup extends FormGroupAdapter {
  @Control({disabled: true}) public readonly bankCardId: FormControl;
  @Control({disabled: true}) public readonly bankCardNumber: FormControl;
  @Control({disabled: true}) public readonly bankCardBalance: FormControl;

  @Control({disabled: false}) public readonly targetBankCardNumber: FormControl;
  @Control({disabled: true}) public readonly targetBankCardOwner: FormControl;

  @Control({disabled: false}) public readonly transferAmount: FormControl;

  public getTargetBankCardNumber(): number {
    if (this.targetBankCardNumber.value) {
      const normalized = this.targetBankCardNumber.value.replace(' ', '').replace(' ', '').replace(' ', '');
      if (normalized && normalized.length) {
        return normalized;
      }
    }
    return null;
  }
}
