import {Component, OnInit} from '@angular/core';
import {BankCardAuthorizationFormGroup} from '../bank-card-authorization-form-group';
import {ActivatedRoute, Router} from '@angular/router';
import {ToastrService} from 'ngx-toastr';
import {CurrentBankCardStateService} from '../../../../../states/current-bank-card-state.service';
import {CurrentBankPropertyStateService} from '../../../../../states/current-bank-property-state.service';

import {
  BankAuthorizationForm,
  BankCardAccountHttpClient, BankCardId,
  BankCardPreviewModel,
  IOperationError, PropertyId
} from '../../../../../../../../../../http/GameHttpClient';
import {Store} from '@ngrx/store';
import {BankClientStoreState} from '../../../../../store/client/bank-client-store-state';
import {BankClientStoreActions} from '../../../../../store/client/bank-client-store-actions';
import {BankClientStoreSelectors} from '../../../../../store/client/bank-client-store-selectors';
import {Actions, ofType} from '@ngrx/effects';

@Component({
  selector: 'app-bank-card-authorization-page',
  templateUrl: './bank-card-authorization-page.component.html',
  styleUrls: ['./bank-card-authorization-page.component.scss']
})
export class BankCardAuthorizationPageComponent implements OnInit {
  public readonly bankCards$ = this.store$.select(BankClientStoreSelectors.bankCards);
  public readonly property$ = this.store$.select(BankClientStoreSelectors.property);
  public readonly operationError$ = this.store$.select(BankClientStoreSelectors.error);

  public readonly form = new BankCardAuthorizationFormGroup();

  constructor(
    private readonly router: Router,
    private readonly bankCardService: BankCardAccountHttpClient,
    private readonly toastr: ToastrService,
    private readonly currentBankCard: CurrentBankCardStateService,
    private readonly currentBankProperty: CurrentBankPropertyStateService,
    private readonly store$: Store<BankClientStoreState>,
    private readonly actions$: Actions,
    private readonly route: ActivatedRoute
  ) {

  }

  ngOnInit() {
    this.actions$
      .pipe(ofType(BankClientStoreActions.INITIALIZE_SUCCESSFULLY))
      .subscribe(() => this.store$.dispatch(BankClientStoreActions.FETCH_BANK_CARD_LIST())
      );

    this.actions$
      .pipe(ofType(BankClientStoreActions.AUTHORIZATION_SUCCESSFULLY))
      .subscribe(() => this.router.navigate(['/World/Property/Bank/Card/Client/Information'])
      );

    const propertyIdArg: string = this.route.snapshot.params.propertyId;
    if (propertyIdArg) {
      const propertyId = new PropertyId({id: propertyIdArg});
      this.store$.dispatch(BankClientStoreActions.INITIALIZE({propertyId}));
    }
  }

  public onClickedAuthorization(): void {
    const form = new BankAuthorizationForm({
      payerBankCardId: new BankCardId({id: this.form.bankCardId.value}),
      pinCode: this.form.pinCode.value
    });

    this.store$.dispatch(BankClientStoreActions.AUTHORIZATION({form}));
  }


  public onBankCardSelected(bankCard: BankCardPreviewModel): void {
    if (bankCard) {
      this.form.bankCardId.setValue(bankCard.entityId.id);
      this.form.bankCardName.setValue(bankCard.number);
    } else {
      this.form.bankCardId.setValue(null);
      this.form.bankCardName.setValue(null);
    }
  }

  public onClickedRequestOpenBankCard(): void {
    this.router.navigate(['/World/Property/Bank/Card/Client/OpenCard']);
  }
}
