import {Action, createReducer, on} from '@ngrx/store';
import {BankClientStoreState} from './bank-client-store-state';
import {BankClientStoreActions} from './bank-client-store-actions';

const initialState: BankClientStoreState = {
  initialized: false,
  inProcess: false,

  authorization: undefined,
  bankCard: undefined,
  error: undefined,
  property: undefined,
  bankCards: undefined,
};

function getInitialState(): BankClientStoreState {
  return initialState;
}

const Reducer = createReducer(getInitialState(),
  on(BankClientStoreActions.INITIALIZE, (state) => ({
    ...state,
    property: undefined,
    bankCard: undefined,
    authorization: undefined,
    error: undefined,
    inProcess: false,
    initialized: false
  })),

  on(BankClientStoreActions.INITIALIZE_SUCCESSFULLY, (state, {property}) => ({...state, property, initialized: true})
  ),
on(BankClientStoreActions.INITIALIZE_FAILED, (state, {error}) => ({...state, error, initialized: false})),

  on(BankClientStoreActions.AUTHORIZATION, (state, {form}) => ({...state, authorization: form, inProcess: true, error: undefined})),
  on(BankClientStoreActions.AUTHORIZATION_SUCCESSFULLY, (state, {form}) => ({...state, authorization: form, inProcess: false})),
  on(BankClientStoreActions.AUTHORIZATION_FAILED, (state, {error}) => ({...state, error, inProcess: false})),

  on(BankClientStoreActions.FETCH_BANK_CARD_INFORMATION, (state) => ({...state, inProcess: true, error: undefined})),
  on(BankClientStoreActions.FETCH_BANK_CARD_INFORMATION_SUCCESSFULLY, (state, {bankCard}) => ({
    ...state,
    authorization: bankCard,
    inProcess: false
  })),
  on(BankClientStoreActions.FETCH_BANK_CARD_INFORMATION_FAILED, (state, {error}) => ({...state, error, inProcess: false})),

  on(BankClientStoreActions.FETCH_BANK_CARD_LIST, (state) => ({...state, inProcess: true, error: undefined})),
  on(BankClientStoreActions.FETCH_BANK_CARD_LIST_SUCCESSFULLY, (state, {bankCards}) => ({...state, bankCards, inProcess: false})),
  on(BankClientStoreActions.FETCH_BANK_CARD_LIST_FAILED, (state, {error}) => ({...state, error, inProcess: false})),
)
;

export function BankClientStoreReducers(state: BankClientStoreState | undefined, action: Action) {
  return Reducer(state, action);
}
