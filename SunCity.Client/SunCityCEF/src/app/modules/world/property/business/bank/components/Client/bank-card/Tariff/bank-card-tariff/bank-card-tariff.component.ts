import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {BankCardTariffId, BankCardTariffModel} from '../../../../../../../../../../http/GameHttpClient';

@Component({
  selector: 'app-bank-card-tariff',
  templateUrl: './bank-card-tariff.component.html',
  styleUrls: ['./bank-card-tariff.component.scss']
})
export class BankCardTariffComponent implements OnInit {
  @Input() public readonly model: BankCardTariffModel;
  @Output() public readonly selected = new EventEmitter<BankCardTariffId>();

  constructor() {
  }

  ngOnInit() {
  }

  public onSelected(): void {
    this.selected.emit(this.model.entityId);
  }

}
