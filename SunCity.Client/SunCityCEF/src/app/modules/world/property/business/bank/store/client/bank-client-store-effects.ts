import {Injectable} from '@angular/core';
import {Actions, Effect, ofType} from '@ngrx/effects';
import {Action, select, Store} from '@ngrx/store';
import {
  BankAuthorizationForm, BankAuthorizationRequest,
  BankCardAccountHttpClient,
  BankGetCardInformationRequest, GetWorldPropertyInformationResponse,
  PropertyHttpClient,
  PropertyId
} from '../../../../../../../http/GameHttpClient';
import {catchError, map, mergeMap, switchMap, tap, withLatestFrom} from 'rxjs/operators';
import {BankClientStoreState} from './bank-client-store-state';
import {BankClientStoreSelectors} from './bank-client-store-selectors';
import {BankClientStoreActions} from './bank-client-store-actions';


@Injectable()
export class BankClientStoreEffects {
  constructor(
    private readonly actions$: Actions,
    private readonly store$: Store<BankClientStoreState>,
    private readonly bankHttpClient: BankCardAccountHttpClient,
    private readonly propertyHttpClient: PropertyHttpClient
  ) {

  }

  @Effect()
  initialize$ = this.actions$.pipe(
    ofType(BankClientStoreActions.INITIALIZE),
    switchMap((action) =>
      this.propertyHttpClient.getInformation(action.propertyId.id).pipe(
        map(result => result.isCorrect ?
          BankClientStoreActions.INITIALIZE_SUCCESSFULLY({property: result.content}) :
          BankClientStoreActions.INITIALIZE_FAILED({error: result.error})
        )
      )
    ));

  @Effect()
  fetchBankCardList$ = this.actions$.pipe(
    ofType(BankClientStoreActions.FETCH_BANK_CARD_LIST),
    withLatestFrom(this.store$.pipe(select(BankClientStoreSelectors.property))),
    switchMap(([action, property]: [any, GetWorldPropertyInformationResponse]) =>
      this.bankHttpClient.getPlayerBankCardList(property.propertyId.id).pipe(
        map(result => result.isCorrect ?
          BankClientStoreActions.FETCH_BANK_CARD_LIST_SUCCESSFULLY({bankCards: result.content}) :
          BankClientStoreActions.FETCH_BANK_CARD_LIST_FAILED({error: result.error})
        ))
    ));

  @Effect()
  authorization$ = this.actions$.pipe(
    ofType(BankClientStoreActions.AUTHORIZATION),
    switchMap((action) => this.bankHttpClient.authorization(new BankAuthorizationRequest({credentials: action.form})).pipe(
      map(result => result.isCorrect ?
        BankClientStoreActions.AUTHORIZATION_SUCCESSFULLY({form: action.form}) :
        BankClientStoreActions.AUTHORIZATION_FAILED({error: result.error})
      ))
    ));

  @Effect()
  fetchBankCardInformation$ = this.actions$.pipe(
    ofType(BankClientStoreActions.FETCH_BANK_CARD_INFORMATION),
    withLatestFrom(this.store$.pipe(select(BankClientStoreSelectors.authorization))),
    switchMap(([action, authorization]: [any, BankAuthorizationForm]) => this.bankHttpClient.getBankCardInformation(new BankGetCardInformationRequest({credentials: authorization})).pipe(
      map(result => result.isCorrect ?
        BankClientStoreActions.FETCH_BANK_CARD_INFORMATION_SUCCESSFULLY({bankCard: result.content}) :
        BankClientStoreActions.FETCH_BANK_CARD_INFORMATION_FAILED({error: result.error})
      ))
    ));
}
