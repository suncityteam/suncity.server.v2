import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BankCardWithdrawPageComponent } from './bank-card-withdraw-page.component';

describe('BankCardWithdrawPageComponent', () => {
  let component: BankCardWithdrawPageComponent;
  let fixture: ComponentFixture<BankCardWithdrawPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BankCardWithdrawPageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BankCardWithdrawPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
