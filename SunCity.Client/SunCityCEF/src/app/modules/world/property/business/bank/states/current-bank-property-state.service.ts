import {Injectable} from '@angular/core';
import {
  GetWorldPropertyInformationResponse,
  PropertyId,
  WorldPropertyCategoryEnumDescription,
  WorldPropertyCategoryEnumDescriptionValue,
  WorldPropertyClassEnumDescription,
  WorldPropertyClassEnumDescriptionValue
} from '../../../../../../http/GameHttpClient';

@Injectable()
export class CurrentBankPropertyStateService {
  public property: GetWorldPropertyInformationResponse;

  constructor() {
    this.property = new GetWorldPropertyInformationResponse({
      propertyId: new PropertyId({id: '77c6972b-7edd-4515-a6d3-c3e179a87327'}),
      category: new WorldPropertyCategoryEnumDescription({
        value: WorldPropertyCategoryEnumDescriptionValue.Bank,
        description: 'Банк'
      }),
      class: new WorldPropertyClassEnumDescription({description: '', value: WorldPropertyClassEnumDescriptionValue.None}),
      name: 'Банк LS'
    });
  }
}
