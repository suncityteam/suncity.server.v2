import {Pipe, PipeTransform} from '@angular/core';
import {BankCardStatus} from '../../enums/client/card/BankCardStatus';

@Pipe({
  name: 'bankCardStatus'
})
export class BankCardStatusPipe implements PipeTransform {

  transform(value: BankCardStatus, args?: any): string {
    if (value === BankCardStatus.Active) {
      return 'Активна';
    }

    if (value === BankCardStatus.Expired) {
      return 'Истекла';
    }

    if (value === BankCardStatus.Closed) {
      return 'Закрыта';
    }

    return 'Неизвестно';
  }

}
