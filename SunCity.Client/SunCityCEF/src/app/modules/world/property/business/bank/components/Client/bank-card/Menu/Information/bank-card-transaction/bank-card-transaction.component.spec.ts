import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BankCardTransactionComponent } from './bank-card-transaction.component';

describe('BankCardTransactionComponent', () => {
  let component: BankCardTransactionComponent;
  let fixture: ComponentFixture<BankCardTransactionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BankCardTransactionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BankCardTransactionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
