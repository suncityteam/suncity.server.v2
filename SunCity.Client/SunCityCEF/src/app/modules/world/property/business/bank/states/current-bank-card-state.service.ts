import {Injectable} from '@angular/core';
import {SessionStorage} from 'ngx-store-9';
import {BankCardNumberPipe} from '../../../../../shared/pipes/bank-card-number/bank-card-number.pipe';
import {BankCardId, BankGetCardInformationResponse} from '../../../../../../http/GameHttpClient';

@Injectable()
export class CurrentBankCardStateService {
  constructor(private readonly bankCardNumberPipe: BankCardNumberPipe) {

  }

  @SessionStorage() public model: BankGetCardInformationResponse;
  @SessionStorage() private pinCodeRaw: string;

  public get pinCode(): number {
    return parseInt(this.pinCodeRaw, 10) as number;
  }

  public set pinCode(value: number) {
    if (value) {
      this.pinCodeRaw = value.toString();
    } else {
      this.pinCodeRaw = null;
    }
  }

  public get entityId(): BankCardId {
    return this.model.entityId;
  }

  public get cardNumber(): string {
    return this.bankCardNumberPipe.transform(this.model.number);
  }

  public reset(): void {
    this.pinCode = null;
    this.model = null;
  }

  public apply(model: BankGetCardInformationResponse, pinCode: number): void {
    this.model = model;
    this.pinCode = pinCode;
  }
}

