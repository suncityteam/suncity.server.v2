import {Action, createReducer, on} from '@ngrx/store';

import {VehicleShowRoomActions} from './vehicle-show-room-actions';
import {VehicleShowroomListOrderBy} from '../enums/vehicle-showroom-list-order.by';
import {VehicleShowRoomState} from './vehicle-show-room-state';
import {OrderByMode} from '../../../../../../enums/order-by-mode';

export const initialState: VehicleShowRoomState = {
  propertyId: null,
  catalog: null,
  selectedCategory: null,
  selectedVehicle: null,
  orderByField: VehicleShowroomListOrderBy.Name,
  orderByMode: OrderByMode.Ascending,
  firstColor: undefined,
  secondColor: undefined,

};

export function getInitialState(): VehicleShowRoomState {
  return initialState;
}


const Reducer = createReducer(getInitialState(),
  on(VehicleShowRoomActions.INITIALIZE, (state, {propertyId}) => ({...state, propertyId})),

  on(VehicleShowRoomActions.FETCH_CATALOG, state => state),
  on(VehicleShowRoomActions.FETCH_CATALOG_SUCCESSFULLY, (state, {catalog}) => ({...state, catalog})),


  on(VehicleShowRoomActions.SWITCH_ORDER_BY_FIELD, (state, {orderBy}) => ({...state, orderByField: orderBy})),
  on(VehicleShowRoomActions.SWITCH_ORDER_BY_MODE, (state, {orderMode}) => ({...state, orderByMode: orderMode})),

  on(VehicleShowRoomActions.SELECT_CATEGORY, (state, {category}) => ({...state, selectedCategory: category})),
  on(VehicleShowRoomActions.SELECT_VEHICLE, (state, {vehicle}) => ({...state, selectedVehicle: vehicle})),

  on(VehicleShowRoomActions.CHANGE_FIRST_COLOR, (state, {color}) => ({...state, firstColor: color})),
  on(VehicleShowRoomActions.CHANGE_SECOND_COLOR, (state, {color}) => ({...state, secondColor: color})),
);

export function VehicleShowRoomReducer(state: VehicleShowRoomState | undefined, action: Action) {
  return Reducer(state, action);
}
