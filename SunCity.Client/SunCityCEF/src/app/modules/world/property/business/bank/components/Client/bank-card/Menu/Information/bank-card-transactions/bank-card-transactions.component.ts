import {Component, Input, OnInit} from '@angular/core';
import {BankCardTransactionPreviewModel} from '../../../../../../../../../../../http/GameHttpClient';

@Component({
  selector: 'app-bank-card-transactions',
  templateUrl: './bank-card-transactions.component.html',
  styleUrls: ['./bank-card-transactions.component.scss']
})
export class BankCardTransactionsComponent implements OnInit {
  @Input() public readonly model: BankCardTransactionPreviewModel[];
  constructor() { }

  ngOnInit() {
  }

}
