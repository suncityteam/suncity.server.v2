

export enum VehicleShowroomListOrderBy {

  /// <summary>
  /// <para>Стоимость</para>
  /// </summary>
  Cost,

  /// <summary>
  /// <para>Название</para>
  /// </summary>
  Name,

  /// <summary>
  /// <para>Вместимость багажника</para>
  /// </summary>
  Capacity,

  /// <summary>
  /// <para>Максимальная скорость</para>
  /// </summary>
  Speed,

  /// <summary>
  /// <para>Управляемость</para>
  /// </summary>
  Handleability,

  /// <summary>
  /// <para>Популярность</para>
  /// </summary>
  Popular
}
