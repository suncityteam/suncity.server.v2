import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BankCardPreviewComponent } from './bank-card-preview.component';

describe('BankCardPreviewComponent', () => {
  let component: BankCardPreviewComponent;
  let fixture: ComponentFixture<BankCardPreviewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BankCardPreviewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BankCardPreviewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
