import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Color, ColorEvent} from 'ngx-color';

@Component({
  selector: 'app-vehicle-showroom-color',
  templateUrl: './vehicle-showroom-color.component.html',
  styleUrls: ['./vehicle-showroom-color.component.scss']
})
export class VehicleShowroomColorComponent implements OnInit {
  @Input()
  public readonly title: string;

  @Output()
  public readonly colorSelected = new EventEmitter<Color>();

  constructor() {
  }

  ngOnInit() {
  }

  onChangeComplete($event: ColorEvent): void {
    this.colorSelected.emit($event.color);
  }
}
