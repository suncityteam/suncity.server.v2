import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BankCardDepositPageComponent } from './bank-card-deposit-page.component';

describe('BankCardDepositPageComponent', () => {
  let component: BankCardDepositPageComponent;
  let fixture: ComponentFixture<BankCardDepositPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BankCardDepositPageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BankCardDepositPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
