import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BankCardOperationCompletePageComponent } from './bank-card-operation-complete-page.component';

describe('BankCardOperationCompletePageComponent', () => {
  let component: BankCardOperationCompletePageComponent;
  let fixture: ComponentFixture<BankCardOperationCompletePageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BankCardOperationCompletePageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BankCardOperationCompletePageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
