import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BankCardTransactionsComponent } from './bank-card-transactions.component';

describe('BankCardTransactionsComponent', () => {
  let component: BankCardTransactionsComponent;
  let fixture: ComponentFixture<BankCardTransactionsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BankCardTransactionsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BankCardTransactionsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
