import {
  BankAuthorizationForm,
  BankGetCardInformationResponse, BankCardPreviewModel, BankCardPreviewModelArrayOperationResponse,
  GetWorldPropertyInformationResponse,
  IOperationError
} from '../../../../../../../http/GameHttpClient';

export const BankClientStoreModuleName = 'BankClientStore';

export interface BankClientStoreState {
  initialized: boolean;
  inProcess: boolean;
  property: GetWorldPropertyInformationResponse;
  authorization: BankAuthorizationForm;
  bankCard: BankGetCardInformationResponse;
  bankCards: BankCardPreviewModel[];
  error?: IOperationError;
}
