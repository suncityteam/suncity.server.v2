export enum BankCardStatus {
  Active,
  Closed,
  Expired
}
