import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VehicleShowroomInformationComponent } from './vehicle-showroom-information.component';

describe('VehicleShowroomInformationComponent', () => {
  let component: VehicleShowroomInformationComponent;
  let fixture: ComponentFixture<VehicleShowroomInformationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VehicleShowroomInformationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VehicleShowroomInformationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
