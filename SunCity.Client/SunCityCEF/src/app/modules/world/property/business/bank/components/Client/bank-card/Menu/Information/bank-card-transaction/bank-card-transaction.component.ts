import {Component, Input, OnInit} from '@angular/core';
import {
  BankCardTransactionPreviewModel,
  BankCardTransactionPreviewModelCategory
} from '../../../../../../../../../../../http/GameHttpClient';

@Component({
  selector: 'app-bank-card-transaction',
  templateUrl: './bank-card-transaction.component.html',
  styleUrls: ['./bank-card-transaction.component.scss']
})
export class BankCardTransactionComponent implements OnInit {
  public readonly BankCardTransactionCategory = BankCardTransactionPreviewModelCategory;
  @Input() public readonly model: BankCardTransactionPreviewModel;

  constructor() {
  }

  ngOnInit() {
  }

}
