import {Component, Input, OnInit} from '@angular/core';
import {IOperationError} from '../../../../../../../../../../../http/GameHttpClient';
@Component({
  selector: 'app-bank-card-operation-complete-page',
  templateUrl: './bank-card-operation-complete-page.component.html',
  styleUrls: ['./bank-card-operation-complete-page.component.scss']
})
export class BankCardOperationCompletePageComponent implements OnInit {
  public operationError: IOperationError | undefined;

  constructor() { }

  ngOnInit() {
  }

}
