import {VehicleShowroomListOrderBy} from '../enums/vehicle-showroom-list-order.by';
import {OrderByMode} from '../../../../../../enums/order-by-mode';
import {Color} from 'ngx-color';
import {
  GetVehiclesShowRoomCatalogItem,
  GetVehiclesShowRoomCatalogResponse, PropertyId,
  VehicleModelClassEnumDescription
} from '../../../../../../http/GameHttpClient';

export interface VehicleShowRoomState {
  propertyId: PropertyId;

  catalog: GetVehiclesShowRoomCatalogResponse;

  selectedCategory: VehicleModelClassEnumDescription;
  selectedVehicle: GetVehiclesShowRoomCatalogItem;

  orderByField: VehicleShowroomListOrderBy;
  orderByMode: OrderByMode;

  firstColor: Color | undefined;
  secondColor: Color | undefined;
}
