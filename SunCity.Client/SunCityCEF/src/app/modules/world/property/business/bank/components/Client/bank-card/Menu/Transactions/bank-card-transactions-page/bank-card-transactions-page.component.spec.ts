import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BankCardTransactionsPageComponent } from './bank-card-transactions-page.component';

describe('BankCardTransactionsPageComponent', () => {
  let component: BankCardTransactionsPageComponent;
  let fixture: ComponentFixture<BankCardTransactionsPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BankCardTransactionsPageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BankCardTransactionsPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
