import {Component, OnInit} from '@angular/core';
import {OpenCardFormGroup} from '../open-card-form-group';
import {CurrentBankPropertyStateService} from '../../../../../states/current-bank-property-state.service';
import {Router} from '@angular/router';
import {CurrentBankCardStateService} from '../../../../../states/current-bank-card-state.service';
import {
  BankAuthorizationForm,
  BankCardAccountHttpClient,
  BankGetCardInformationResponseOperationResponse, BankCardTariffHttpClient, BankCardTariffModel,
  BankCardTariffModelArrayOperationResponse,
  IOperationError, BankOpenCardRequest, BankOpenCardResponseOperationResponse, BankGetCardInformationRequest
} from '../../../../../../../../../../http/GameHttpClient';

@Component({
  selector: 'app-open-card-page',
  templateUrl: './open-card-page.component.html',
  styleUrls: ['./open-card-page.component.scss']
})
export class OpenCardPageComponent implements OnInit {
  public operationError: IOperationError | undefined;
  public readonly form = new OpenCardFormGroup();
  public tariffList: BankCardTariffModel[];

  constructor(
    private readonly router: Router,
    private readonly bankCardService: BankCardAccountHttpClient,
    private readonly bankTariffService: BankCardTariffHttpClient,
    private readonly currentBankProperty: CurrentBankPropertyStateService,
    private readonly currentBankCard: CurrentBankCardStateService,
  ) {
  }

  ngOnInit(): void {
    this.currentBankCard.reset();
    this.bankTariffService.getCardTariffList(/*this.currentBankProperty.property.entityId*/ undefined).subscribe(res => this.onGetCardTariffList(res));
  }

  private onGetCardTariffList(res: BankCardTariffModelArrayOperationResponse) {
    this.operationError = res.error;
    this.tariffList = res.content;
  }

  public onTariffSelected(tariff: BankCardTariffModel): void {
    this.form.tariffId.setValue(tariff.entityId);
    this.form.tariffName.setValue(tariff.name);
  }

  public onClickedOpenBankCard(): void {
    const form = new BankOpenCardRequest({
      tariffEntityId: this.form.tariffId.value,
      pinCode: this.form.pinCode.value,
      secureWord: this.form.secureWord.value
    });
    this.bankCardService.openCard(form).subscribe(res => this.onOpenBankCardResponse(res));
  }

  private onOpenBankCardResponse(res: BankOpenCardResponseOperationResponse) {
    this.operationError = res.error;
    if (res.isCorrect) {
      const credentials = new BankAuthorizationForm({
        payerBankCardId: res.content.entityId,
        pinCode: res.content.pinCode
      });

      const form = new BankGetCardInformationRequest({
        credentials
      });

      this.bankCardService.getBankCardInformation(form).subscribe(rez => this.onGetBankCardResponse(rez));
    }
  }

  private onGetBankCardResponse(res: BankGetCardInformationResponseOperationResponse): void {
    this.operationError = res.error;
    if (res.isCorrect) {
      this.currentBankCard.apply(res.content, this.form.pinCode.value);
      this.router.navigate(['/World/Property/Bank/Card/Client/Information']);
    }
  }

}
