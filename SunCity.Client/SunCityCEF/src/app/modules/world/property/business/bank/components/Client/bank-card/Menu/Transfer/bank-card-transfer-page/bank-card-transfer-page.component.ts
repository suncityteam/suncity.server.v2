import {Component, OnInit} from '@angular/core';
import {ToastrService} from 'ngx-toastr';
import {Router} from '@angular/router';
import {CurrentBankCardStateService} from '../../../../../../states/current-bank-card-state.service';
import {BankCardTransferFormGroup} from '../bank-card-transfer-form-group';
import {
  BankAuthorizationForm,
  BankCardAccountHttpClient,
  BankCardBalanceModelOperationResponse,
  BankCardOwnerModelOperationResponse,
  BankCardTransactionIdOperationResponse, BankGetCardBalanceRequest, BankGetCardOwnerRequest, BankTransferToCardRequest,
  IOperationError
} from '../../../../../../../../../../../http/GameHttpClient';

@Component({
  selector: 'app-bank-card-transfer-page',
  templateUrl: './bank-card-transfer-page.component.html',
  styleUrls: ['./bank-card-transfer-page.component.scss']
})
export class BankCardTransferPageComponent implements OnInit {
  public operationError: IOperationError | undefined;
  public readonly FormGroup = new BankCardTransferFormGroup();

  constructor(
    private readonly toastr: ToastrService,
    private readonly router: Router,
    private readonly bankCardService: BankCardAccountHttpClient,
    private readonly currentBankCard: CurrentBankCardStateService,
  ) {

  }

  public ngOnInit() {
    this.FormGroup.bankCardId.setValue(this.currentBankCard.model.entityId);
    this.FormGroup.bankCardNumber.setValue(this.currentBankCard.cardNumber);
    this.bankCardService.getBankCardBalance(new BankGetCardBalanceRequest({
      credentials: new BankAuthorizationForm({
        payerBankCardId: this.currentBankCard.model.entityId,
        pinCode: this.currentBankCard.pinCode
      })
    })).subscribe(res => this.onGetBalanceResponse(res));

    this.FormGroup.targetBankCardNumber.valueChanges.subscribe(() => this.onEnterBankCardNumber());

  }

  public onClickedTransfer(): void {
    const form = new BankTransferToCardRequest({
      credentials: new BankAuthorizationForm({
        payerBankCardId: this.currentBankCard.entityId,
        pinCode: this.currentBankCard.pinCode
      }),
      amount: parseInt(this.FormGroup.transferAmount.value, 10),
      payeeBankCardNumber: this.FormGroup.getTargetBankCardNumber()
    });

    this.bankCardService.transferToBankCardNumber(form).subscribe(res => this.onTransferResponse(res));
  }

  private onTransferResponse(res: BankCardTransactionIdOperationResponse): void {
    this.operationError = res.error;
    if (res.isCorrect) {
      // this.toastr.success(`Вы успешно сняли ${this.FormGroup.withdrawAmount.value}$ с карты ${this.currentBankCard.cardNumber}`, 'Снятие наличных');
      this.router.navigate([`/World/Property/Bank/Card/Client/OperationComplete/123`]);
    }
  }

  private onGetBalanceResponse(res: BankCardBalanceModelOperationResponse) {
    this.operationError = res.error;
    if (res.isCorrect) {
      this.FormGroup.bankCardBalance.setValue(res.content.balance);
    }
  }

  private onEnterBankCardNumber(): void {
    const bankCardNumber = this.FormGroup.getTargetBankCardNumber();
    this.bankCardService.getBankCardOwner(new BankGetCardOwnerRequest({bankCardNumber})).subscribe(res => this.onGetBankCardOwnerResponse(res));
  }

  private onGetBankCardOwnerResponse(res: BankCardOwnerModelOperationResponse) {
    this.operationError = res.error;
    if (res.isCorrect) {
      this.FormGroup.targetBankCardOwner.setValue(res.content.ownerName);
    } else {
      this.FormGroup.targetBankCardOwner.setValue(null);
    }
  }
}
