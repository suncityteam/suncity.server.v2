import {Component, OnInit} from '@angular/core';
import {Store} from '@ngrx/store';
import {VehicleShowRoomState} from '../../../store/vehicle-show-room-state';
import {VehicleShowRoomActions} from '../../../store/vehicle-show-room-actions';
import {OrderByMode} from '../../../../../../../../enums/order-by-mode';
import {VehicleShowroomListOrderBy} from '../../../enums/vehicle-showroom-list-order.by';
import VehicleShowRoomSelectors from '../../../store/vehicle-show-room-selectors';

@Component({
  selector: 'app-vehicle-showroom-order',
  templateUrl: './vehicle-showroom-order.component.html',
  styleUrls: ['./vehicle-showroom-order.component.scss']
})
export class VehicleShowroomOrderComponent implements OnInit {
  public readonly VehicleShowroomListOrderBy = VehicleShowroomListOrderBy;
  public readonly orderByField$ = this.store$.select(VehicleShowRoomSelectors.orderByField);
  public readonly orderByMode$ = this.store$.select(VehicleShowRoomSelectors.orderByMode);

  public orderByField: VehicleShowroomListOrderBy;
  public orderByMode: OrderByMode;

  constructor(private readonly store$: Store<VehicleShowRoomState>) {

  }

  public isActive(order: VehicleShowroomListOrderBy): boolean {
    return order === this.orderByField;
  }

  public onChangeOrderBy(orderByField: VehicleShowroomListOrderBy): void {
    if (this.orderByField === orderByField) {
      if (this.orderByMode === OrderByMode.Ascending) {
        this.store$.dispatch(VehicleShowRoomActions.SWITCH_ORDER_BY_MODE({orderMode: OrderByMode.Descending}));
      } else {
        this.store$.dispatch(VehicleShowRoomActions.SWITCH_ORDER_BY_MODE({orderMode: OrderByMode.Ascending}));
      }
    }
    this.store$.dispatch(VehicleShowRoomActions.SWITCH_ORDER_BY_FIELD({orderBy: orderByField}));
  }


  ngOnInit() {
    this.orderByField$.subscribe(orderBy => this.orderByField = orderBy);
    this.orderByMode$.subscribe(mode => this.orderByMode = mode);
  }

}
