import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BankCardInformationPageComponent } from './bank-card-information-page.component';

describe('BankCardInformationPageComponent', () => {
  let component: BankCardInformationPageComponent;
  let fixture: ComponentFixture<BankCardInformationPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BankCardInformationPageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BankCardInformationPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
