import {Injectable} from '@angular/core';
import {GameItem, GameItemId, GameItemsHttpClient} from '../../../../http/GameHttpClient';

@Injectable({
  providedIn: 'root'
})
export class GameItemsStorage {
  public items: GameItem[];

  constructor(private readonly gameItemsService: GameItemsHttpClient) {
    this.gameItemsService.getItemModels().subscribe(res => this.onGetGameItemModelsResponse(res));
  }

  public getByModelId(modelId: GameItemId): GameItem | null {
    if (modelId) {
      const item = this.items.find(i => i.entityId === modelId);
      if (item) {
        return item;
      }
    }
    return null;
  }

  private onGetGameItemModelsResponse(res: GameItem[]) {
    this.items = res;
  }
}
