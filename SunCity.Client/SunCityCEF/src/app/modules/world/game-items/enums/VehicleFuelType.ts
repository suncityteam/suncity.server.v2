/* tslint:disable:no-bitwise */

export enum VehicleFuelType {
  None = 0,

  /// <summary>
  /// Бензин
  /// </summary>
  Petrol = 1,

  /// <summary>
  /// Дизель
  /// </summary>
  Diesel = 1 << 1,

  /// <summary>
  /// Электричество
  /// </summary>
  Electricity = 1 << 2,

  /// <summary>
  /// Газ
  /// </summary>
  Gas = 1 << 3,

  Premium = 1 << 4
}
