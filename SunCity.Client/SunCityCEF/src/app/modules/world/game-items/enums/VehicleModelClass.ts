export enum VehicleModelClass {
  None,

  Boats,
  Compacts,
  Coupes,
  Cycles,
  Emergency,
  Industrial,
  Service,
  Military,

  Motorcycles,

  Muscle,
  Sports,
  SportsClassic,
  SuperCars,

  OffRoad,
  SUVs,
  Sedans,

  Helicopters,
  Planes,

  Trailer,
  Trains,
  Utility,

  Vans
}

