export enum ItemCategory {
  //=======================
  None,

  //=======================
  /// <summary>
  /// Еда
  /// </summary>
  Food,

  /// <summary>
  /// Напиток
  /// </summary>
  Drink,

  /// <summary>
  /// Наркотики
  /// </summary>
  Drug,

  /// <summary>
  /// Лекарства
  /// </summary>
  Medicine,

  //=======================
  Resource,

  //=======================
  /// <summary>
  /// Оружие
  /// </summary>
  Weapon,

  /// <summary>
  /// Одежда
  /// </summary>
  Clothing,

  //=======================
  VehicleKey,
  PropertyKey,
}

