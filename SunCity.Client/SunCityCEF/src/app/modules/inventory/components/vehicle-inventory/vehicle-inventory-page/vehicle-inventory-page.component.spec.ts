import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VehicleInventoryPageComponent } from './vehicle-inventory-page.component';

describe('VehicleInventoryPageComponent', () => {
  let component: VehicleInventoryPageComponent;
  let fixture: ComponentFixture<VehicleInventoryPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VehicleInventoryPageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VehicleInventoryPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
