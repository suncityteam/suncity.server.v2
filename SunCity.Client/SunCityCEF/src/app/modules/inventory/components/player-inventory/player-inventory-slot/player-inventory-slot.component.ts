import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Observable} from 'rxjs';
import {DropEvent} from 'ng-drag-drop';
import {IPlayerInventoryItemDragData} from '../player-inventory-bag/player-inventory-bag.component';
import {IPlayerInventoryDragData} from '../player-inventory-bag/i-player-inventory-drag.data';
import {PlayerInventorySlotModel, PlayerInventorySlotModelInventorySlot} from '../../../../../http/GameHttpClient';

@Component({
  selector: 'app-player-inventory-slot',
  templateUrl: './player-inventory-slot.component.html',
  styleUrls: ['./player-inventory-slot.component.scss']
})
export class PlayerInventorySlotComponent implements OnInit {
  @Output() public readonly dragItem = new EventEmitter<IPlayerInventoryDragData>();

  @Input()
  public readonly defaultIcon: string;

  @Input()
  public readonly dropScope: PlayerInventorySlotModelInventorySlot;

  @Input()
  public readonly item: Observable<PlayerInventorySlotModel>;
  public slotItem: PlayerInventorySlotModel;

  constructor() {
  }

  ngOnInit() {
    if (this.item) {
      this.item.subscribe(item => this.slotItem = item);
    }
  }

  onItemDrop($event: DropEvent) {
    const data = $event.dragData as IPlayerInventoryItemDragData;
    this.dragItem.emit({
      from: data.slot,
      to: null,
      item: data.item
    });
  }
}
