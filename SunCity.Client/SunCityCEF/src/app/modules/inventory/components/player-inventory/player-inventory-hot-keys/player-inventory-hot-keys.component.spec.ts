import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PlayerInventoryHotKeysComponent } from './player-inventory-hot-keys.component';

describe('PlayerInventoryHotKeysComponent', () => {
  let component: PlayerInventoryHotKeysComponent;
  let fixture: ComponentFixture<PlayerInventoryHotKeysComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PlayerInventoryHotKeysComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PlayerInventoryHotKeysComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
