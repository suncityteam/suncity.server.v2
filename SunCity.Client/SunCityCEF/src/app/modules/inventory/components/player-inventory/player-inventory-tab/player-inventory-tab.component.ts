import {Component, OnInit} from '@angular/core';
import {PlayerInventoryWeapons} from '../../../models/player-inventory-weapons';
import {PlayerInventoryEquipments} from '../../../models/player-inventory-equipments';
import {PlayerInventoryGridConfiguration} from './player-inventory-grid.configuration';
import {IPlayerInventoryDragData} from '../player-inventory-bag/i-player-inventory-drag.data';
import {
  InventroryItemEquipForm, InventroryItemMoveForm,
  IOperationError, IOperationResponse,
  PlayerInventoryHttpClient,
  PlayerInventoryModelOperationResponse,
  PlayerInventorySlotModelInventorySlot
} from '../../../../../http/GameHttpClient';

@Component({
  selector: 'app-player-inventory-tab',
  templateUrl: './player-inventory-tab.component.html',
  styleUrls: ['./player-inventory-tab.component.scss']
})
export class PlayerInventoryTabComponent implements OnInit {
  public readonly weapons = new PlayerInventoryWeapons();
  public readonly equipments = new PlayerInventoryEquipments();
  public readonly configuration = new PlayerInventoryGridConfiguration();
  public readonly PlayerInventorySlot = PlayerInventorySlotModelInventorySlot;
  public operationError: IOperationError | null;

  constructor(
    private readonly inventory: PlayerInventoryHttpClient
  ) {
  }

  ngOnInit() {
    this.inventory.getInventory().subscribe(res => this.onInventoryLoadResponse(res));
  }

  private onInventoryLoadResponse(res: PlayerInventoryModelOperationResponse) {
    this.operationError = res.error;
    if (res.isCorrect) {
      for (const slot of res.content.slots) {
        if (slot.inventorySlot === PlayerInventorySlotModelInventorySlot.Outerwear) {
          this.equipments.shirt.next(slot);
        } else if (slot.inventorySlot === PlayerInventorySlotModelInventorySlot.Pants) {
          this.equipments.pants.next(slot);
        } else if (slot.inventorySlot === PlayerInventorySlotModelInventorySlot.Bag) {
          this.equipments.bag.next(slot);
        }
      }
    }
  }

  onItemDragAndDrop($event: IPlayerInventoryDragData): void {
    this.operationError = null;
    if ($event.from.entityId !== $event.to.entityId) {
      this.inventory.moveItemTo(new InventroryItemMoveForm({
        fromId: $event.from.item.entityId,
        targetId: $event.to.item.entityId,
        itemId: $event.item.entityId
      })).subscribe(res => this.onMoveItemResponse(res));
    }
  }

  onEquipItem($event: IPlayerInventoryDragData): void {
    this.operationError = null;
    this.inventory.equipItem(new InventroryItemEquipForm({
      fromId: $event.from.item.entityId,
      itemId: $event.item.entityId
    })).subscribe(res => this.onMoveItemResponse(res));
  }


  private onMoveItemResponse(res: IOperationResponse) {
    this.operationError = res.error;
    if (res.isCorrect) {
      this.inventory.getInventory().subscribe(resz => this.onInventoryLoadResponse(resz));
    }
  }
}
