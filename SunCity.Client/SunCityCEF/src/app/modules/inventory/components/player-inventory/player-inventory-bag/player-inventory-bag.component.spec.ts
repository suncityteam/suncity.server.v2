import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PlayerInventoryBagComponent } from './player-inventory-bag.component';

describe('PlayerInventoryBagComponent', () => {
  let component: PlayerInventoryBagComponent;
  let fixture: ComponentFixture<PlayerInventoryBagComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PlayerInventoryBagComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PlayerInventoryBagComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
