export class PlayerInventoryGridConfiguration {
  public readonly weapons = 'col-100';
  public readonly equipmentsFirst = 'col-32';
  public readonly equipmentsNext = 'col-32 offset-2';
  public readonly equipmentsSingle = 'col-32 offset-34';

  public readonly bagSingle = 'col-15 offset-1 order-2';
  public readonly bagDouble = 'col-31 offset-1 order-1';

}
