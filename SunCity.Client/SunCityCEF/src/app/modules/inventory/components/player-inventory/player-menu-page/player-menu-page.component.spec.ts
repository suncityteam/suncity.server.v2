import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PlayerMenuPageComponent } from './player-menu-page.component';

describe('PlayerMenuPageComponent', () => {
  let component: PlayerMenuPageComponent;
  let fixture: ComponentFixture<PlayerMenuPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PlayerMenuPageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PlayerMenuPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
