import {Component, HostListener, OnInit} from '@angular/core';

@Component({
  selector: 'app-player-menu-page',
  templateUrl: './player-menu-page.component.html',
  styleUrls: ['./player-menu-page.component.scss']
})
export class PlayerMenuPageComponent implements OnInit {
  constructor() {
  }

  ngOnInit() {
  }

  @HostListener('document:keydown.escape', ['$event'])
  private onClickedCloseWindow(event: KeyboardEvent): void {
    console.log(event);
  }

}
