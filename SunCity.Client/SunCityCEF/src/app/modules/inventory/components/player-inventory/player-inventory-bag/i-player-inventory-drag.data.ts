import {PlayerInventoryItemModel, PlayerInventorySlotModel} from '../../../../../http/GameHttpClient';

export interface IPlayerInventoryDragData {
  readonly from: PlayerInventorySlotModel;
  readonly to: PlayerInventorySlotModel;
  readonly item: PlayerInventoryItemModel;
}

