import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {GameItemsStorage} from '../../../../world/game-items/services/game-items-storage';
import {Observable} from 'rxjs';
import {DropEvent} from 'ng-drag-drop';
import {IPlayerInventoryDragData} from './i-player-inventory-drag.data';
import {GameItem, GameItemId, PlayerInventoryItemModel, PlayerInventorySlotModel} from '../../../../../http/GameHttpClient';

export interface IPlayerInventoryItemDragData {
  slot: PlayerInventorySlotModel;
  item: PlayerInventoryItemModel;
}

export interface IPlayerInventorySlot {
  disabled: boolean;
  item: PlayerInventoryItemModel | null;
}

@Component({
  selector: 'app-player-inventory-bag',
  templateUrl: './player-inventory-bag.component.html',
  styleUrls: ['./player-inventory-bag.component.scss']
})
export class PlayerInventoryBagComponent implements OnInit {

  public get realCapacity(): number {
    if (this.itemModel) {
      return this.itemModel.capacity;
    }
    return 0;
  }

  public get displayCapacity(): number {
    if (this.itemModel) {
      return Math.max(this.itemModel.capacity, this.slotCapacity);
    }
    return this.slotCapacity;
  }

  public get slotItem(): PlayerInventoryItemModel | null {
    if (this.slotStorage) {
      return this.slotStorage.item;
    }
    return null;
  }

  public get itemModel(): GameItem | null {
    if (this.slotItem) {
      return this.getItemModel(this.slotItem.modelId);
    }
    return null;
  }

  constructor(private readonly gameItemsStorage: GameItemsStorage) {
  }

  public slots: IPlayerInventorySlot[] = [];

  @Output() public readonly dragItem = new EventEmitter<IPlayerInventoryDragData>();
  @Input() public readonly storage: Observable<PlayerInventorySlotModel>;
  public slotStorage: PlayerInventorySlotModel;

  @Input()
  public readonly slotName: string;

  @Input()
  public readonly slotCapacity: number;

  @Input()
  public readonly allowScroll: boolean;

  public readonly configuration = {
    bagSingle: 'col-15 offset-1 order-2',
    bagDouble: 'col-31 offset-1 order-1',
  };

  public getItemModel(modelId: GameItemId): GameItem | null {
    return this.gameItemsStorage.getByModelId(modelId);
  }

  private getChildItem(index: number): PlayerInventoryItemModel | null {
    if (this.slotItem) {
      if (this.slotItem.child.length > index) {
        return this.slotItem.child[index];
      }
    }
    return null;
  }

  ngOnInit() {
    this.onSlotChanged(null);
    this.storage.subscribe(slot => this.onSlotChanged(slot));
  }

  public itemDragData(item: PlayerInventoryItemModel): IPlayerInventoryItemDragData {
    return {
      item,
      slot: this.slotStorage
    };
  }

  private onSlotChanged(slot: PlayerInventorySlotModel): void {
    this.slotStorage = slot;

    this.slots = [];
    for (let i = 0; i < this.displayCapacity; i++) {
      this.slots[i] = {
        disabled: i >= this.realCapacity,
        item: this.getChildItem(i),
      };
    }
  }

  onItemDrop($event: DropEvent) {
    const data = $event.dragData as IPlayerInventoryItemDragData;
    this.dragItem.emit({
      from: data.slot,
      to: this.slotStorage,
      item: data.item
    });
  }

}
