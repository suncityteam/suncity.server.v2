import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PlayerInventoryTabComponent } from './player-inventory-tab.component';

describe('PlayerInventoryTabComponent', () => {
  let component: PlayerInventoryTabComponent;
  let fixture: ComponentFixture<PlayerInventoryTabComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PlayerInventoryTabComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PlayerInventoryTabComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
