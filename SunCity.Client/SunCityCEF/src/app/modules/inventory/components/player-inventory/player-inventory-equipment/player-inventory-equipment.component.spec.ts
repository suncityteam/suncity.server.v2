import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PlayerInventoryEquipmentComponent } from './player-inventory-equipment.component';

describe('PlayerInventoryEquipmentComponent', () => {
  let component: PlayerInventoryEquipmentComponent;
  let fixture: ComponentFixture<PlayerInventoryEquipmentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PlayerInventoryEquipmentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PlayerInventoryEquipmentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
