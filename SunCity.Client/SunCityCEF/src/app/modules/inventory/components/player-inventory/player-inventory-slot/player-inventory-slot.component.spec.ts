import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PlayerInventorySlotComponent } from './player-inventory-slot.component';

describe('PlayerInventorySlotComponent', () => {
  let component: PlayerInventorySlotComponent;
  let fixture: ComponentFixture<PlayerInventorySlotComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PlayerInventorySlotComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PlayerInventorySlotComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
