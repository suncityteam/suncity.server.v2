import {Component, Input, OnInit} from '@angular/core';
import {GameItemsStorage} from '../../../../world/game-items/services/game-items-storage';
import {GameItem, PlayerInventoryItemModel} from '../../../../../http/GameHttpClient';

@Component({
  selector: 'app-inventory-item',
  templateUrl: './inventory-item.component.html',
  styleUrls: ['./inventory-item.component.scss']
})
export class InventoryItemComponent implements OnInit {

  @Input()
  public defaultIcon: string;

  @Input()
  public item: PlayerInventoryItemModel;

  @Input()
  public disabled: boolean;

  @Input()
  public num: string;

  public get imageClass(): string {
    if (this.itemModel) {
      if (this.itemModel.imageClass) {
        return this.itemModel.imageClass;
      }
      return this.itemModel.entityId.id;
    }
    return this.defaultIcon;
  }

  public get itemModel(): GameItem | null {
    if (this.item) {
      return this.storage.getByModelId(this.item.modelId);
    }
    return null;
  }

  constructor(private readonly storage: GameItemsStorage) {
  }

  ngOnInit() {
  }
}
