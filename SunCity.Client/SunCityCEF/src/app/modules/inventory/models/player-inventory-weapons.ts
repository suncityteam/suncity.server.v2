import {Subject} from 'rxjs';
import {PlayerInventorySlotModel} from '../../../http/GameHttpClient';

export class PlayerInventoryWeapons {
  public readonly primary = new Subject<PlayerInventorySlotModel>();
  public readonly secondary = new Subject<PlayerInventorySlotModel>();

  public readonly $primary = this.primary.asObservable();
  public readonly $secondary = this.secondary.asObservable();
}
