import {Observable, Subject} from 'rxjs';
import {PlayerInventorySlotModel} from '../../../http/GameHttpClient';

export class PlayerInventoryEquipments {

  public readonly glasses = new Subject<PlayerInventorySlotModel>();
  public readonly hat = new Subject<PlayerInventorySlotModel>();
  public readonly earrings = new Subject<PlayerInventorySlotModel>();
  public readonly beads = new Subject<PlayerInventorySlotModel>();
  public readonly shirt = new Subject<PlayerInventorySlotModel>();
  public readonly wristWatch = new Subject<PlayerInventorySlotModel>();
  public readonly bodyArmor = new Subject<PlayerInventorySlotModel>();
  public readonly pants = new Subject<PlayerInventorySlotModel>();
  public readonly bag = new Subject<PlayerInventorySlotModel>();
  public readonly footwear = new Subject<PlayerInventorySlotModel>();

  public readonly $glasses = this.glasses.asObservable();
  public readonly $hat = this.hat.asObservable();
  public readonly $earrings = this.earrings.asObservable();
  public readonly $beads = this.beads.asObservable();
  public readonly $shirt = this.shirt.asObservable();
  public readonly $wristWatch = this.wristWatch.asObservable();
  public readonly $bodyArmor = this.bodyArmor.asObservable();
  public readonly $pants = this.pants.asObservable();
  public readonly $bag = this.bag.asObservable();
  public readonly $footwear = this.footwear.asObservable();
}
