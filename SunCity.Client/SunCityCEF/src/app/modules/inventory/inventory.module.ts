import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {InventoryItemComponent} from './components/shared/inventory-item/inventory-item.component';
import {SharedModule} from '../shared/shared.module';
import {RouterModule, Routes} from '@angular/router';
import {VehicleInventoryPageComponent} from './components/vehicle-inventory/vehicle-inventory-page/vehicle-inventory-page.component';
import { PlayerMenuPageComponent } from './components/player-inventory/player-menu-page/player-menu-page.component';
import { PlayerInventoryTabComponent } from './components/player-inventory/player-inventory-tab/player-inventory-tab.component';
import { PlayerInventoryBagComponent } from './components/player-inventory/player-inventory-bag/player-inventory-bag.component';
import { PlayerInventoryEquipmentComponent } from './components/player-inventory/player-inventory-equipment/player-inventory-equipment.component';
import { PlayerInventoryHotKeysComponent } from './components/player-inventory/player-inventory-hot-keys/player-inventory-hot-keys.component';
import { PlayerInventorySlotComponent } from './components/player-inventory/player-inventory-slot/player-inventory-slot.component';

export const InventoryRoutes: Routes = [
  {
    path: 'Player/Inventory',
    component: PlayerMenuPageComponent
  },
  {
    path: 'Inventory/Vehicle',
    component: VehicleInventoryPageComponent
  },
];

@NgModule({
  declarations: [
    InventoryItemComponent,
    VehicleInventoryPageComponent,
    PlayerMenuPageComponent,
    PlayerInventoryTabComponent,
    PlayerInventoryBagComponent,
    PlayerInventoryEquipmentComponent,
    PlayerInventoryHotKeysComponent,
    PlayerInventorySlotComponent
  ],
  imports: [
    CommonModule,
    SharedModule.forRoot(),
    RouterModule.forChild(InventoryRoutes),
  ],
  exports: [
    VehicleInventoryPageComponent,
    InventoryItemComponent,
    SharedModule,
  ]
})

export class InventoryModule {
}

