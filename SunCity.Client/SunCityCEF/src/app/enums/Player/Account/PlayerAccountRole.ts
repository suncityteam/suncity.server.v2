export enum PlayerAccountRole {
  None,
  Helper,
  Moderator,
  Administrator,
  SuperAdministrator
}
