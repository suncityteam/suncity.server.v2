export enum OrderByMode {
  /// <summary>
  /// <para>По возрастанию</para>
  /// <para>- от А до Я</para>
  /// <para>- от 0 до 100</para>
  /// </summary>
  Ascending,

  /// <summary>
  /// <para>По убыванию</para>
  /// <para>- от Я до А</para>
  /// <para>- от 100 до 0</para>
  /// </summary>
  Descending
}
