import {StoreModule} from '@ngrx/store';
import {EffectsModule} from '@ngrx/effects';
import {CommonModule} from '@angular/common';
import {NgModule} from '@angular/core';
import {ApplicationStoreReducers} from './application-store-reducers';

//  https://github.com/katesky/ngrx-multy-store-part-1.5/blob/master/src/app/feature1/store1/store1.reducer.ts

@NgModule({
  imports: [
    CommonModule,
    StoreModule.forRoot(ApplicationStoreReducers),
    EffectsModule.forRoot([]),
  ],
  providers: []
})
export class ApplicationStoreModule { }
