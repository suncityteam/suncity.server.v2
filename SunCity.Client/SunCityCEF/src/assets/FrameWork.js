function callServerEvent(...args)
{
  try
  {
    mp.trigger("CallServerEvent", ...args);
    mp.trigger(args[0], args);
  } catch (e)
  {
    alert(e);
  }
}

function callClientEvent(...args)
{
  try
  {
    //alert('callClientEvent error' + args[0]);
    mp.trigger(args[0], args[1]);
  } catch (e)
  {
    //alert('callClientEvent error' + e.toString());
  }
}


function onCallCustomAngularEvent(eventName, json)
{
  //var encodedData = utf8_to_b64(json);
  var event = new CustomEvent(eventname, {detail: `${json}`});
  window.dispatchEvent(event);
}

function onInitRageClientId(InRageClientId)
{
  const event = new CustomEvent('initRageClientId', {detail: `${InRageClientId}`});
  window.dispatchEvent(event);
}

function onCallCustomRageEvent(json)
{
  const encodedData = utf8_to_b64(json);
  const event = new CustomEvent('callCustomRageEvent', {detail: `${encodedData}`});
  window.dispatchEvent(event);
}

function utf8_to_b64(str)
{
  return window.btoa(unescape(encodeURIComponent(str)));
}

function b64_to_utf8(str)
{
  return decodeURIComponent(escape(window.atob(str)));
}

//setInterval(onCallCustomRageEvent, 5000);

