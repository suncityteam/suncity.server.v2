﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Extensions.DependencyInjection;
using SunCity.Game.World.DataBase;

namespace SunCity.Game.World.Common.Process
{
    public abstract class GameWorldProcess
    {
        private IServiceProvider ServiceProvider { get; }
        protected IServiceScope CreateScope() => ServiceProvider.CreateScope();

        protected GameWorldProcess(IServiceProvider InServiceProvider)
        {
            ServiceProvider = InServiceProvider;
            Task.Factory.StartNew(OnProcess, TaskCreationOptions.LongRunning);
        }

        protected abstract Task OnProcess();
    }
}
