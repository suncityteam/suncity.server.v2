﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SunCity.Game.World.Common.Components
{
    public abstract class BaseEntitySharedData<TOwner, TValue> : BaseEntityComponent<TOwner>
    {
        public string Name { get; }
        public virtual TValue Value { get; set; }
        public Action<TOwner, TValue> OnValueChanged { get; set; }

        protected BaseEntitySharedData(TOwner InOwner, string InName) : base(InOwner)
        {
            Name = InName;
        }
    }

    public abstract class BaseEntityComponent<TOwner>
    {
        public TOwner Owner { get; }

        protected BaseEntityComponent(TOwner InOwner)
        {
            Owner = InOwner;
        }

    }
}
