﻿using System;
using System.Diagnostics.CodeAnalysis;

namespace SunCity.Game.World.Common.Chat
{
    [SuppressMessage("ReSharper", "InconsistentNaming")]
    public static class ChatColors
    {
        public const int CHAT_RANGES = 5;

        // Chat colors
        public static string COLOR_CHAT_CLOSE { get; } = "!{#E6E6E6}";
        public static string COLOR_CHAT_NEAR { get; } = "!{#C8C8C8}";
        public static string COLOR_CHAT_MEDIUM { get; } = "!{#AAAAAA}";
        public static string COLOR_CHAT_FAR { get; } = "!{#8C8C8C}";
        public static string COLOR_CHAT_LIMIT { get; } = "!{#6E6E6E}";
        public static string COLOR_CHAT_ME { get; } = "!{#C2A2DA}";
        public static string COLOR_CHAT_DO { get; } = "!{#0F9622}";
        public static string COLOR_CHAT_FACTION { get; } = "!{#27F7C8}";
        public static string COLOR_CHAT_PHONE { get; } = "!{#27F7C8}";
        public static string COLOR_OOC_CLOSE { get; } = "!{#4C9E9E}";
        public static string COLOR_OOC_NEAR { get; } = "!{#438C8C}";
        public static string COLOR_OOC_MEDIUM { get; } = "!{#2E8787}";
        public static string COLOR_OOC_FAR { get; } = "!{#187373}";
        public static string COLOR_OOC_LIMIT { get; } = "!{#0A5555}";
        public static string COLOR_ADMIN_INFO { get; } = "!{#00FCFF}";
        public static string COLOR_ADMIN_NEWS { get; } = "!{#F93131}";
        public static string COLOR_ADMIN_MP { get; } = "!{#F93131}";
        public static string COLOR_SUCCESS { get; } = "!{#33B517}";
        public static string COLOR_ERROR { get; } = "!{#A80707}";
        public static string COLOR_INFO { get; } = "!{#FDFE8B}";
        public static string COLOR_HELP { get; } = "!{#FFFFFF}";
        public static string COLOR_SU_POSITIVE { get; } = "!{#E3E47D}";
        public static string COLOR_RADIO { get; } = "!{#1598C4}";
        public static string COLOR_RADIO_POLICE { get; } = "!{#4169E1}";
        public static string COLOR_RADIO_EMERGENCY { get; } = "!{#FF9F0F}";
        public static string COLOR_NEWS { get; } = "!{#805CC9}";

        //   https://wiki.rage.mp/index.php?title=Fonts_and_Colors
        public static string COLOR_GREEN { get; } = "!{#71ca00}";
        public static string COLOR_Yellow { get; } = "!{#eec650}";
        public static string COLOR_Blue { get; } = "!{#5cb4e3}";
    }
}
