﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;

namespace SunCity.EntityFrameWork
{
    public static class ModelBuilderExtensions
    {
        public static async Task<T> AddAndSaveAsync<T>(this DbContext InDbContext, T InEntity) where T : class
        {
            InDbContext.Add(InEntity);
            await InDbContext.SaveChangesAsync();
            return InEntity;
        }

        public static ModelBuilder UseValueConverterForType<T>(this ModelBuilder InModelBuilder, ValueConverter InConverter)
        {
            return InModelBuilder.UseValueConverterForType(typeof(T), InConverter);
        }

        public static ModelBuilder UseValueConverterForType(this ModelBuilder InModelBuilder, Type InType, ValueConverter InConverter)
        {
            foreach (var entityType in InModelBuilder.Model.GetEntityTypes())
            {
                var properties = entityType.ClrType.GetProperties().Where(p => p.PropertyType == InType);
                foreach (var property in properties)
                {
                    InModelBuilder.Entity(entityType.Name).Property(property.Name)
                                  .HasConversion(InConverter);
                }
            }

            return InModelBuilder;
        }
    }
}
