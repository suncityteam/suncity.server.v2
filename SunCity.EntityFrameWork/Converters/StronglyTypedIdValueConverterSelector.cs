﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;
using SunCity.Common.Reflection;
using SunCity.Domain.Interfaces.Types;
using SunCity.EntityFrameWork.Converters.Internal;

namespace SunCity.EntityFrameWork.Converters
{
    internal class StronglyTypedIdValueConverterSelector : ValueConverterSelector
    {
        public StronglyTypedIdValueConverterSelector(ValueConverterSelectorDependencies InDependencies) : base(InDependencies)
        {

        }

        public override IEnumerable<ValueConverterInfo> Select(Type InModelClrType, Type InProviderClrType = null)
        {
            var baseConverters = base.Select(InModelClrType, InProviderClrType);
            foreach (var converter in baseConverters)
            {
                yield return converter;
            }

            var underlyingModelType = InModelClrType.UnwrapNullableType();
            var isImplementedGuid = underlyingModelType.IsImplementedGenericInterface(typeof(IGuidId<>));

            if (isImplementedGuid)
            {
                var converterType = typeof(GuidValueConverter<>).MakeGenericType(underlyingModelType);
                yield return new ValueConverterInfo(InModelClrType, typeof(Guid), InInfo => (ValueConverter)Activator.CreateInstance(converterType));
            }

            var isImplementedName = underlyingModelType.IsImplementedGenericInterface(typeof(IName<>));
            if (isImplementedName)
            {
                var converterType = typeof(NameValueConverter<>).MakeGenericType(underlyingModelType);
                yield return new ValueConverterInfo(InModelClrType, typeof(string), InInfo => (ValueConverter)Activator.CreateInstance(converterType));
            }

            var isImplementedAmount = underlyingModelType.IsImplementedGenericInterface(typeof(IAmount<>));
            if (isImplementedAmount)
            {
                var argumentType = underlyingModelType.GetGenericArguments(typeof(IAmount<>));
                var converterType = typeof(AmountValueConverter<,>).MakeGenericType(underlyingModelType, argumentType[0]);
                yield return new ValueConverterInfo(InModelClrType, argumentType[0], InInfo => (ValueConverter)Activator.CreateInstance(converterType));
            }
        }
    }
}
