﻿using System.Threading.Tasks;
using AutoMapper;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;

namespace SunCity.EntityFrameWork.Converters
{
    public static class StronglyTypedIdValueConverterExtensions
    {
        public static DbContextOptionsBuilder UseStronglyTypedIdValueConverter(this DbContextOptionsBuilder InBuilder)
        {
            InBuilder.ReplaceService<IValueConverterSelector, StronglyTypedIdValueConverterSelector>();
            return InBuilder;
        }
    }
}