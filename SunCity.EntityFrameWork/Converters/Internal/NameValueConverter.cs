﻿using System;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;
using SunCity.Domain.Interfaces.Types;

namespace SunCity.EntityFrameWork.Converters.Internal
{
    internal class NameValueConverter<TModel> : ValueConverter<TModel, string>
        where TModel : IName<TModel>
    {
        private static Type ModelType { get; } = typeof(TModel);
        public NameValueConverter() : base(InModel => InModel.Name, InGuid => (TModel)Activator.CreateInstance(ModelType, InGuid)) { }
    }
}