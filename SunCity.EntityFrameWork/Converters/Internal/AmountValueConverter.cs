﻿using System;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;
using SunCity.Domain.Interfaces.Types;

namespace SunCity.EntityFrameWork.Converters.Internal
{
    internal class AmountValueConverter<TModel, TAmount> : ValueConverter<TModel, TAmount>
        where TModel : IAmount<TAmount>
        where TAmount : IConvertible
    {
        private static Type ModelType { get; } = typeof(TModel);
        public AmountValueConverter() : base(InModel => InModel.Amount, InGuid => (TModel)Activator.CreateInstance(ModelType, InGuid)) { }
    }
}