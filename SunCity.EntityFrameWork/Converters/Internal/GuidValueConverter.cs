﻿using System;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;
using SunCity.Domain.Interfaces.Types;

namespace SunCity.EntityFrameWork.Converters.Internal
{
    internal class GuidValueConverter<TModel> : ValueConverter<TModel, Guid>
        where TModel : IGuidId<TModel>
    {
        private static Type ModelType { get; } = typeof(TModel);
        public GuidValueConverter() : base(InModel => InModel.Id, InGuid => (TModel)Activator.CreateInstance(ModelType, InGuid)) { }
    }
}