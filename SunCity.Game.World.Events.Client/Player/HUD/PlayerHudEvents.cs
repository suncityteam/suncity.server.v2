﻿using System.Threading.Tasks;
using SunCity.Common.Operation;
using SunCity.Common.RageMp.Pools.Players;
using SunCity.Game.World.Extensions.Player;

namespace SunCity.Game.World.Events.Client.Player.HUD
{
    public static class PlayerHudEvents
    {
        public static void TogglePlayerHud(this IRagePlayer InPlayer, OperationResponse<bool> InTogglePlayerHud)
        {
            InPlayer.TriggerEvent("Player:HUD:Toggle", InTogglePlayerHud);
        }
    }
}
