﻿using System.Threading.Tasks;
using SunCity.Common.Operation;
using SunCity.Common.RageMp.Pools.Players;
using SunCity.Game.World.Extensions.Player;

namespace SunCity.Game.World.Events.Client.Property
{
    public static class WorldPropertyEvents
    {
        public static void ShowPropertyBuyWindow(this IRagePlayer InPlayer, OperationResponse<PropertyId> InPropertyId)
        {
            InPlayer.TriggerEvent("Property:Buy", InPropertyId);
        }
    }
}