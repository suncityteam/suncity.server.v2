﻿using System.Threading.Tasks;
using SunCity.Common.Operation;
using SunCity.Common.RageMp.Pools.Players;

namespace SunCity.Game.World.Events.Client.Property.Business.Bank
{
    public static class WorldPropertyBankEvents
    {
        public static void ShowBankClientMenu(this IRagePlayer InPlayer, OperationResponse<PropertyId> InPropertyId)
        {
            InPlayer.TriggerEvent("Property:Business:Bank:Client:Menu", InPropertyId);
        }
    }
}
