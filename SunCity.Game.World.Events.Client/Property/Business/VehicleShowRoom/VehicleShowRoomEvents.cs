﻿using System.Threading.Tasks;
using SunCity.Common.Operation;
using SunCity.Common.RageMp.Pools.Players;
using SunCity.Game.World.Extensions.Player;
using SunCity.Game.World.Models.Property.Business.VehicleShowRoom;

namespace SunCity.Game.World.Events.Client.Property.Business.VehicleShowRoom
{
    public static class VehicleShowRoomEvents
    {
        public static void ShowVehicleShowRoomClientMenu(this IRagePlayer InPlayer, OperationResponse<VehicleShowRoomRageModel> InPropertyId)
        {
            InPlayer.TriggerEvent("Property:Business:VehicleShowRoom:Client:Menu.Open", InPropertyId);
        }
        
        public static void HideVehicleShowRoomClientMenu(this IRagePlayer InPlayer)
        {
            InPlayer.TriggerEvent("Property:Business:VehicleShowRoom:Client:Menu.Close");
        }
    }
}