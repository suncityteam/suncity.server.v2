﻿using System.Threading.Tasks;
using SunCity.Common.Operation;
using SunCity.Common.RageMp.Pools.Players;
using SunCity.Game.World.Extensions.Player;

namespace SunCity.Game.World.Events.Client.Property.Business.GasStation
{
    public static class WorldPropertyGasStationEvents
    {
        public static void ShowGasStationClientMenu(this IRagePlayer InPlayer, OperationResponse<PropertyId> InPropertyId)
        {
            InPlayer.TriggerEvent("Property:Business:GasStation:Client:Menu", InPropertyId);
        }
    }
}
