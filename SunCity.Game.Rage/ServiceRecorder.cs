﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using SunCity.Common;
using SunCity.Common.RageMp;
using SunCity.Game.Rage.Connections;

namespace SunCity.Game.Rage
{
    public static class ServiceRecorder
    {
        public static IServiceCollection AddRageConnectionManager(this IServiceCollection InServiceCollection, IHostEnvironment environment, IConfiguration InConfiguration)
        {
            InServiceCollection.AddScoped<IRageConnectionStorage, RageConnectionStorage>();
            InServiceCollection.AddScoped(provider => provider.GetRequiredService<IRageConnectionStorage>().RagePlayer);
            InServiceCollection.AddScoped(provider => provider.GetRequiredService<IRageConnectionStorage>().WorldPlayer);

            if (environment.IsEnvironment(EnvironmentNames.USE_MOCK_USERS) /*|| environment.IsEnvironment(EnvironmentNames.INTEGRATION_TESTS)*/)
            {
                InServiceCollection.AddScoped<IRageConnectionStorage, MockRageConnectionStorage>();
            }
            else
            {
                InServiceCollection.AddScoped<IRageConnectionStorage, RageConnectionStorage>();
            }
            
            return InServiceCollection;
        }
        
        public static IApplicationBuilder UseRageMpServices(this IApplicationBuilder applicationBuilder)
        {
            return applicationBuilder;
        }
    }
}
