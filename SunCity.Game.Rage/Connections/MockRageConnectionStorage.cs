﻿using System;
using GTANetworkAPI;
using Microsoft.AspNetCore.Http;
using SunCity.Common.Operation;
using SunCity.Common.RageMp.Pools.Players;
using SunCity.Game.Enums.World.Player.Inventory;
using SunCity.Game.World.Forms.Character;
using SunCity.Game.World.Forms.Character.Inventory;
using SunCity.Game.World.Models.Item.World;
using SunCity.Game.World.Player;

namespace SunCity.Game.Rage.Connections
{
    internal class MockRageConnectionStorage : IRageConnectionStorage
    {
        private IServiceProvider ServiceProvider { get; }
        private static object Sync = new object();
        
        public MockRageConnectionStorage(IServiceProvider serviceProvider)
        {
            ServiceProvider = serviceProvider;
        }

        public IRagePlayer RagePlayer { get;private set; }
        public WorldPlayer WorldPlayer { get; private set; }

        public OperationResponse Ensure(IHeaderDictionary InHeaders)
        {
            WorldPlayer = GetOrFactory();
            return OperationResponseCache.Successfully;
        }

        public OperationResponse Ensure(IRagePlayer player)
        {
            throw new NotImplementedException();
        }

        private WorldPlayer GetOrFactory()
        {
            if (WP != null)
                return WP;

            lock (Sync)
            {
                if (WP != null)
                    return WP;
                
                WP = MockFactory();
                return WP;
            }
        }
        
        private WorldPlayer MockFactory()
        {
            var form = new WorldJoinCharacterForm()
            {
                Inventory = new LoadCharacterInventory()
                {
                    Slots = new LoadCharacterInventoryItemForm[]
                    {
                        new LoadCharacterInventoryItemForm()
                        {
                            EntityId      = new CharacterInventoryItemId(Guid.Parse("71aa4e32-8f64-4570-8628-000000000001")),
                            InventorySlot = PlayerInventorySlot.Pants,
                            Item = new WorldItemModel()
                            {
                                EntityId = new WorldItemId(Guid.Parse("71aa4e32-8f64-4570-8628-300000000001")),
                                Child = new WorldItemModel[]
                                {
                                    new WorldItemModel
                                    {
                                        EntityId = new WorldItemId(Guid.Parse("71aa4e32-8f64-4570-8628-100000000001")),
                                        ModelId  = new GameItemId(Guid.Parse("71aa4e32-8f64-4570-8628-12e2bd754180"))
                                    },
                                },
                                ModelId = new GameItemId(Guid.Parse("a93d4a74-680a-4f1a-8985-9b48f30c4dda"))
                            }
                        },
                        new LoadCharacterInventoryItemForm()
                        {
                            EntityId      = new CharacterInventoryItemId(Guid.Parse("71aa4e32-8f64-4570-8628-000000000002")),
                            InventorySlot = PlayerInventorySlot.Outerwear,
                            Item = new WorldItemModel()
                            {
                                EntityId = new WorldItemId(Guid.Parse("71aa4e32-8f64-4570-8628-300000000002")),
                                Child = new WorldItemModel[]
                                {
                                    new WorldItemModel
                                    {
                                        EntityId = new WorldItemId(Guid.Parse("71aa4e32-8f64-4570-8628-200000000001")),
                                        ModelId  = new GameItemId(Guid.Parse("c812eac4-3c35-4841-a625-dc38aabb01c7"))
                                    },
     
                                    new WorldItemModel
                                    {                                     
                                        EntityId = new WorldItemId(Guid.Parse("71aa4e32-8f64-4570-8628-200000000003")),
                                        ModelId =  new GameItemId(Guid.Parse("a93d4a74-680a-4f1a-8985-9b48f30c4dda")),
                                        Child = new WorldItemModel[]
                                        {
                           
                                            new WorldItemModel
                                            {
                                                EntityId = new WorldItemId(Guid.Parse("71aa4e32-8f64-4570-8628-500000000001")),
                                                ModelId  = new GameItemId(Guid.Parse("71aa4e32-8f64-4570-8628-12e2bd754180"))
                                            },
                                            new WorldItemModel
                                            {
                                                EntityId = new WorldItemId(Guid.Parse("71aa4e32-8f64-4570-8628-500000000002")),
                                                ModelId  = new GameItemId(Guid.Parse("ab6eba08-151c-403b-b453-6bf75a844ea1"))
                                            },
                                            new WorldItemModel
                                            {
                                                EntityId = new WorldItemId(Guid.Parse("71aa4e32-8f64-4570-8628-500000000003")),
                                                ModelId  = new GameItemId(Guid.Parse("71aa4e32-8f64-4570-8628-12e2bd754180"))
                                            },
                                            new WorldItemModel
                                            {
                                                EntityId = new WorldItemId(Guid.Parse("71aa4e32-8f64-4570-8628-500000000004")),
                                                ModelId  = new GameItemId(Guid.Parse("ab6eba08-151c-403b-b453-6bf75a844ea1"))
                                            },
                                        }
                                    } 
                                },
                                ModelId = new GameItemId(Guid.Parse("9135b5ac-457c-430a-8b30-64fb928299d6"))
                            }
                        },
                    }
                }
            };

            //var player = new WorldPlayer(form);
            //player.Initialize(new PlayerInitializeContext<WorldJoinCharacterForm>(ServiceProvider, form))
            return null;
        }
        
        public static WorldPlayer WP { get; private set; }
    }
}