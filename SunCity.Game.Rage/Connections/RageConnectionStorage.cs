﻿using System.Collections.Generic;
using System.Text;
using GTANetworkAPI;
using Microsoft.AspNetCore.Http;
using SunCity.Common.Operation;
using SunCity.Common.RageMp.Pools.Players;
using SunCity.Game.World.Player;

namespace SunCity.Game.Rage.Connections
{
    public interface IRageConnectionStorage
    {
        IRagePlayer RagePlayer { get; }
        WorldPlayer WorldPlayer { get; }
        OperationResponse Ensure(IHeaderDictionary InHeaders);
        OperationResponse Ensure(IRagePlayer player);
    }

    internal class RageConnectionStorage : IRageConnectionStorage
    {
        public const string HeaderName = "RageMp";
        
        public IRagePlayer RagePlayer { get; private set; }
        public WorldPlayer WorldPlayer { get; private set; }

        private IPlayerPool PlayerPool { get; }
        private IWorldPlayerStorage WorldPlayerStorage { get; }
        
        public RageConnectionStorage(IPlayerPool playerPool, IWorldPlayerStorage worldPlayerStorage)
        {
            PlayerPool = playerPool;
            WorldPlayerStorage = worldPlayerStorage;
        }

        public OperationResponse Ensure(IHeaderDictionary InHeaders)
        {
            if (InHeaders.ContainsKey(HeaderName))
            {
                var connectionIdHeader = InHeaders[HeaderName];
                var connectionIdString = connectionIdHeader[0];

                if (ushort.TryParse(connectionIdString, out var connectionId))
                {
                    var player = PlayerPool.GetAt(connectionId);
                    if (player != null && player.Exists)
                    {
                        RagePlayer = player;

                        //  WorldPlayer allowed null if user not authorized
                        WorldPlayer = WorldPlayerStorage[RagePlayer];

                        return OperationResponseCache.Successfully;
                    }
                }
            }

            return OperationResponseCache.ConnectionNotFound;
        }

        public OperationResponse Ensure(IRagePlayer player)
        {
            RagePlayer = player;
            if (WorldPlayerStorage.TryGetPlayer(RagePlayer, out var worldPlayer))
            {
                WorldPlayer = worldPlayer;
            }

            return OperationResponseCache.Successfully;
        }
    }
}