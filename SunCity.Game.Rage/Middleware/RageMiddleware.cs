﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Authorization;
using Microsoft.AspNetCore.Mvc.Filters;
using SunCity.Game.Rage.Connections;

namespace SunCity.Game.Rage.Middleware
{
    public sealed class RageMiddleware : IAuthorizationFilter
    {
        private IRageConnectionStorage ConnectionStorage { get; }
        private static ContentResult Unauthorized { get; } = new ContentResult()
        {
            //  TODO: Use json result
            Content = "Resource unavailable - header not set."
        };

        public RageMiddleware(IRageConnectionStorage InConnectionStorage)
        {
            ConnectionStorage = InConnectionStorage;
        }

        public void OnAuthorization(AuthorizationFilterContext InContext)
        { 
            // Allow Anonymous skips all authorization
            //if (InContext.Filters.Any(IsAllowAnonymous))
            //    return;

            var ensureResult = ConnectionStorage.Ensure(InContext.HttpContext.Request.Headers);
            if (ensureResult.IsNotCorrect)
            {
                InContext.HttpContext.Response.StatusCode = (int)HttpStatusCode.Unauthorized;
                InContext.Result = Unauthorized;
            }
        }

        private static bool IsAllowAnonymous(IFilterMetadata InFilterMetadata)
        {
            return InFilterMetadata is IAllowAnonymousFilter;
        }
    }
}
