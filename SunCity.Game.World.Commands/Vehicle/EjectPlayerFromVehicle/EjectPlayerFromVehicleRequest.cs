﻿using SunCity.Common.Commands.Interfaces;

namespace SunCity.Game.World.Commands.Vehicle.EjectPlayerFromVehicle
{
    public class EjectPlayerFromVehicleRequest : IRageMpCommandRequest
    {
        public ushort TargetPlayerId { get; set; }
    }
}
