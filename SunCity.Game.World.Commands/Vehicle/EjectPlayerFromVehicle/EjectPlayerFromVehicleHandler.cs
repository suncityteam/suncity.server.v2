﻿using System.Threading;
using System.Threading.Tasks;
using SunCity.Common.Commands.Interfaces;
using SunCity.Common.Operation;

namespace SunCity.Game.World.Commands.Vehicle.EjectPlayerFromVehicle
{
    public class EjectPlayerFromVehicleHandler : RageMpCommandHandler<EjectPlayerFromVehicleRequest>
    {
        public EjectPlayerFromVehicleHandler(RageMpCommandConstructor<EjectPlayerFromVehicleRequest> constructor) : base(constructor)
        {
        }

        public override Task<OperationResponse> Handle(EjectPlayerFromVehicleRequest request, CancellationToken cancellationToken)
        {
            var targetPlayer = GetPlayerById(request.TargetPlayerId);

            RagePlayer.SendChatMessage($"Вы выкинули игрока({targetPlayer.Name}) из машины");
            RagePlayer.SendChatMessage($"Игрок ({targetPlayer.Name}) выкинул Вас из машины");

            return TaskOperationResponseCache.Successfully;
        }
    }
}