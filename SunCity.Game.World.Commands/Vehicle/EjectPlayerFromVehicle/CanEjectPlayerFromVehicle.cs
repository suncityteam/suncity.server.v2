﻿using System.Threading;
using System.Threading.Tasks;
using MediatR;
using SunCity.Common.Commands.Interfaces;
using SunCity.Common.Operation;

namespace SunCity.Game.World.Commands.Vehicle.EjectPlayerFromVehicle
{
    public class CanEjectPlayerFromVehicle : RageMpCommandPipelineBehavior<EjectPlayerFromVehicleRequest>
    {
        public CanEjectPlayerFromVehicle(RageMpCommandConstructor<EjectPlayerFromVehicleRequest> constructor) : base(constructor)
        {

        }

        public override Task<OperationResponse> Handle(EjectPlayerFromVehicleRequest request, CancellationToken cancellationToken, RequestHandlerDelegate<OperationResponse> next)
        {
            var targetPlayer = GetPlayerById(request.TargetPlayerId);
            if (targetPlayer == null || !targetPlayer.Exists)
            {
                RagePlayer.SendChatMessage($"Игрок с ид {request.TargetPlayerId} не найден");
                return TaskOperationResponseCache.Successfully;
            }

            if (!targetPlayer.IsInVehicle)
            {
                RagePlayer.SendChatMessage($"Игрок не находится в машине");
                return TaskOperationResponseCache.Successfully;
            }

            if (!RagePlayer.IsInVehicle)
            {
                RagePlayer.SendChatMessage($"Вы находитесь не в машине");
                return TaskOperationResponseCache.Successfully;
            }

            if (targetPlayer.Vehicle.Id != RagePlayer.Vehicle.Id)
            {
                RagePlayer.SendChatMessage($"Игрок находится в другой машине");
                return TaskOperationResponseCache.Successfully;
            }

            return next();
        }


    }
}