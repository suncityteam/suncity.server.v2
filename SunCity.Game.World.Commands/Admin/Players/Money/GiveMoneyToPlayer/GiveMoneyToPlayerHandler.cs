﻿using System.Threading;
using System.Threading.Tasks;
using SunCity.Common.Commands.Interfaces;
using SunCity.Common.Operation;
using SunCity.Game.World.Player;

namespace SunCity.Game.World.Commands.Admin.Players.Money.GiveMoneyToPlayer
{
    public class GiveMoneyToPlayerHandler : RageMpCommandHandler<GiveMoneyToPlayerRequest>
    {
        private IWorldPlayerStorage PlayerStorage { get; }
        public GiveMoneyToPlayerHandler(RageMpCommandConstructor<GiveMoneyToPlayerRequest> constructor, IWorldPlayerStorage playerStorage) : base(constructor)
        {
            PlayerStorage = playerStorage;
        }

        public override Task<OperationResponse> Handle(GiveMoneyToPlayerRequest request, CancellationToken cancellationToken)
        {
            var targetPlayer = RageMpPool.Players.GetAt(request.TargetPlayerId);
            PlayerStorage.TryGetPlayer(targetPlayer, out var targetWorldPlayer);

            targetWorldPlayer.Character.Money.Value += request.Amount;
            targetWorldPlayer.SendClientMessage($"Администратор выдал вам {request.Amount}$");
            return TaskOperationResponseCache.Successfully;
        }
    }
}