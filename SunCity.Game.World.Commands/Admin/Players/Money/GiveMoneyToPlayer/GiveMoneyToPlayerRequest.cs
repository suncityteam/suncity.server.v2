﻿using SunCity.Common.Commands.Interfaces;
using SunCity.Game.World.Commands.Common;

namespace SunCity.Game.World.Commands.Admin.Players.Money.GiveMoneyToPlayer
{
    public class GiveMoneyToPlayerRequest 
        : IRageMpCommandRequest
            , ICommandRequestWithTargetPlayer
    {
        public ushort TargetPlayerId { get; set; }
        public long Amount { get; set; }
    }
}
