﻿using SunCity.Common.Commands.Interfaces;
using SunCity.Game.World.Commands.Common;

namespace SunCity.Game.World.Commands.Admin.Players.Money.CheckPlayerMoney
{
    public class CheckPlayerMoneyRequest 
        : IRageMpCommandRequest
            , ICommandRequestWithTargetPlayer
    {
        public ushort TargetPlayerId { get; set; }
    }
}
