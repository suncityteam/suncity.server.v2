﻿using System.Threading;
using System.Threading.Tasks;
using SunCity.Common.Commands.Interfaces;
using SunCity.Common.Operation;
using SunCity.Game.World.Player;

namespace SunCity.Game.World.Commands.Admin.Players.Money.CheckPlayerMoney
{
    public class CheckPlayerMoneyHandler : RageMpCommandHandler<CheckPlayerMoneyRequest>
    {
        private IWorldPlayerStorage PlayerStorage { get; }
        public CheckPlayerMoneyHandler(RageMpCommandConstructor<CheckPlayerMoneyRequest> constructor, IWorldPlayerStorage playerStorage) : base(constructor)
        {
            PlayerStorage = playerStorage;
        }

        public override Task<OperationResponse> Handle(CheckPlayerMoneyRequest request, CancellationToken cancellationToken)
        {
            var targetPlayer = RageMpPool.Players.GetAt(request.TargetPlayerId);
            PlayerStorage.TryGetPlayer(targetPlayer, out var targetWorldPlayer);

            RagePlayer.SendChatMessage($"У игрока {targetWorldPlayer.Name} на руках {targetWorldPlayer.Character.Money.Value}$ / UI: {targetPlayer.Model}");

            return TaskOperationResponseCache.Successfully;
        }
    }
}