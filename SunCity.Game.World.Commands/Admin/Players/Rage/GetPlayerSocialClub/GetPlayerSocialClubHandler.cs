﻿using System.Threading;
using System.Threading.Tasks;
using SunCity.Common.Commands.Interfaces;
using SunCity.Common.Operation;

namespace SunCity.Game.World.Commands.Admin.Players.Rage.GetPlayerSocialClub
{
    public class GetPlayerSocialClubHandler : RageMpCommandHandler<GetPlayerSocialClubRequest>
    {
        public GetPlayerSocialClubHandler(RageMpCommandConstructor<GetPlayerSocialClubRequest> constructor) : base(constructor)
        {
        }

        public override Task<OperationResponse> Handle(GetPlayerSocialClubRequest request, CancellationToken cancellationToken)
        {
            var targetPlayer = GetPlayerById(request.TargetPlayerId);
            RagePlayer.SendChatMessage($"У игрока {targetPlayer.Name}, SocialClub: {targetPlayer.SocialClubName}");
            return TaskOperationResponseCache.Successfully;
        }
    }
}