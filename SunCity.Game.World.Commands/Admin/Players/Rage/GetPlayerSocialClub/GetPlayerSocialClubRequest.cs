﻿using SunCity.Common.Commands.Interfaces;
using SunCity.Game.World.Commands.Common;

namespace SunCity.Game.World.Commands.Admin.Players.Rage.GetPlayerSocialClub
{
    public class GetPlayerSocialClubRequest 
        : IRageMpCommandRequest
            , ICommandRequestWithTargetPlayer
    {
        public ushort TargetPlayerId { get; set; }
    }
}
