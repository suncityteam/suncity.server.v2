﻿using System.Threading;
using System.Threading.Tasks;
using SunCity.Common.Commands.Interfaces;
using SunCity.Common.Operation;

namespace SunCity.Game.World.Commands.Admin.Players.Rage.GetPlayerHWID
{
    public class GetPlayerHWIDHandler : RageMpCommandHandler<GetPlayerHWIDRequest>
    {
        public GetPlayerHWIDHandler(RageMpCommandConstructor<GetPlayerHWIDRequest> constructor) : base(constructor)
        {
        }

        public override Task<OperationResponse> Handle(GetPlayerHWIDRequest request, CancellationToken cancellationToken)
        {
            var targetPlayer = GetPlayerById(request.TargetPlayerId);
            RagePlayer.SendChatMessage($"У игрока {targetPlayer.Name}, HWID: {targetPlayer.Serial}");
            return TaskOperationResponseCache.Successfully;
        }
    }
}