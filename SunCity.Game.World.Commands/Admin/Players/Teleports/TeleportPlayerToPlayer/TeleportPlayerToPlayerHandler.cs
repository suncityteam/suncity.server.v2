﻿using System.Threading;
using System.Threading.Tasks;
using GTANetworkAPI;
using SunCity.Common.Commands.Interfaces;
using SunCity.Common.Operation;
using SunCity.Game.World.Commands.Common;

namespace SunCity.Game.World.Commands.Admin.Players.Teleports.TeleportPlayerToPlayer
{
    public class TeleportPlayerToPlayerHandler : RageMpCommandHandler<TeleportPlayerToPlayerRequest>
    {
        public TeleportPlayerToPlayerHandler(RageMpCommandConstructor<TeleportPlayerToPlayerRequest> constructor) : base(constructor)
        {
        }

        public override async Task<OperationResponse> Handle(TeleportPlayerToPlayerRequest request, CancellationToken cancellationToken)
        {
            var whoPlayer = RageMpPool.Players.GetAt(request.WhoPlayerId);
            var wherePlayer = RageMpPool.Players.GetAt(request.WherePlayerId);

            if( whoPlayer == null || whoPlayer.Exists == false)
            {
                RagePlayer.SendChatMessage($"Игрок с ид {request.WhoPlayerId} не найден");
            }
            else if (wherePlayer == null || wherePlayer.Exists == false)
            {
                RagePlayer.SendChatMessage($"Игрок с ид {request.WherePlayerId} не найден");
            }
            else
            {
                whoPlayer.Position = wherePlayer.Position;
                RagePlayer.SendChatMessage($"Вы телепортировали {whoPlayer.Name} к {wherePlayer.Name}");
                whoPlayer.SendChatMessage($"Администратор телепортировал вас к {wherePlayer.Name}");
                wherePlayer.SendChatMessage($"Администратор телепортировал к вам {whoPlayer.Name}");
            }

            return OperationResponseCache.Successfully;
        }

        public sealed class Command : Script
        {
            [Command("tpt", "/TPT [Ид игрока] [Ид игрока]", Alias = "T2T")]
            public static void CommandHandler(GTANetworkAPI.Player sender, ushort whoPlayerId, ushort wherePlayerId)
            {
                RageMpCommandExecutor.CommandExecutor.Execute(sender, new TeleportPlayerToPlayerRequest
                {
                    WhoPlayerId = whoPlayerId,
                    WherePlayerId = wherePlayerId
                });
            }
        }
    }
}