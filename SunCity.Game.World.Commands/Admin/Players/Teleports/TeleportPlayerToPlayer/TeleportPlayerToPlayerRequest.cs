﻿using SunCity.Common.Commands.Interfaces;

namespace SunCity.Game.World.Commands.Admin.Players.Teleports.TeleportPlayerToPlayer
{
    public class TeleportPlayerToPlayerRequest : IRageMpCommandRequest
    {
        public ushort WhoPlayerId { get; set; }
        public ushort WherePlayerId { get; set; }
    }
}