﻿using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using GTANetworkAPI;
using SunCity.Common.Commands.Interfaces;
using SunCity.Common.Operation;
using SunCity.Common.RageMp.Pools;
using SunCity.Game.Enums.World.Property;
using SunCity.Game.World.Commands.Common;
using SunCity.Game.World.Property;

namespace SunCity.Game.World.Commands.Admin.Players.Teleports.TeleportToProperty.TeleportToVehicleShowRoom
{
    public sealed class TeleportToVehicleShowRoomHandler : RageMpCommandHandler<TeleportToVehicleShowRoomRequest>
    {
        private IWorldPropertyStorage PropertyStorage { get; }

        public TeleportToVehicleShowRoomHandler
        (
            RageMpCommandConstructor<TeleportToVehicleShowRoomRequest> constructor,
            IWorldPropertyStorage propertyStorage
        ) : base(constructor)
        {
            PropertyStorage = propertyStorage;
        }

        public override async Task<OperationResponse> Handle(TeleportToVehicleShowRoomRequest request, CancellationToken cancellationToken)
        {
            var property = PropertyStorage.Values.First(q => q.PropertyCategory == WorldPropertyCategory.CarShowroom);

            RagePlayer.SendChatMessage($"Вы успешно телепортировались к {property.PropertyCategoryName} (Name: {property.Name}), ид: {property.EntityId}");
            RagePlayer.Dimension = RageMpConsts.GlobalDimension;
            RagePlayer.Position = property.Position;

            return OperationResponseCache.Successfully;
        }

        public sealed class Command : Script
        {
            [Command("gotocardealer", "/GoToCarDealer ")]
            public static void CommandHandler(GTANetworkAPI.Player sender, string propertyId)
            {
                RageMpCommandExecutor.CommandExecutor.Execute(sender, new TeleportToVehicleShowRoomRequest()
                {
                });
            }
        }
    }
}