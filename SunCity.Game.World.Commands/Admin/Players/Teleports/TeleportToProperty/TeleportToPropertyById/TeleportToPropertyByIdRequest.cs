﻿using SunCity.Common.Commands.Interfaces;

namespace SunCity.Game.World.Commands.Admin.Players.Teleports.TeleportToProperty.TeleportToPropertyById
{
    public sealed class TeleportToPropertyByIdRequest:IRageMpCommandRequest
    {
        public PropertyId PropertyId { get; set; }
    }
}
