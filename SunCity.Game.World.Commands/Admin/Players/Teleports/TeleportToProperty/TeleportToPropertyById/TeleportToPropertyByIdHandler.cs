﻿using System.Threading;
using System.Threading.Tasks;
using GTANetworkAPI;
using SunCity.Common.Commands.Interfaces;
using SunCity.Common.Operation;
using SunCity.Common.RageMp.Pools;
using SunCity.Game.World.Commands.Common;
using SunCity.Game.World.Property;

namespace SunCity.Game.World.Commands.Admin.Players.Teleports.TeleportToProperty.TeleportToPropertyById
{
    public sealed class TeleportToPropertyByIdHandler : RageMpCommandHandler<TeleportToPropertyByIdRequest>
    {
        private IWorldPropertyStorage PropertyStorage { get; }

        public TeleportToPropertyByIdHandler
        (
            RageMpCommandConstructor<TeleportToPropertyByIdRequest> constructor,
            IWorldPropertyStorage propertyStorage
        ) : base(constructor)
        {
            PropertyStorage = propertyStorage;
        }

        public override async Task<OperationResponse> Handle(TeleportToPropertyByIdRequest request, CancellationToken cancellationToken)
        {
            var property = PropertyStorage[request.PropertyId];
            if (property == null)
            {
                RagePlayer.SendChatMessage($"Недвижимость с ид {request.PropertyId} не найдена");
            }
            else
            {
                RagePlayer.SendChatMessage($"Вы успешно телепортировались к {property.PropertyCategoryName} (Name: {property.Name}), ид: {request.PropertyId}");
                RagePlayer.Dimension = RageMpConsts.GlobalDimension;
                RagePlayer.Position = property.Position;
            }

            return OperationResponseCache.Successfully;
        }

        public sealed class Command : Script
        {
            [Command("gotoproperty", "/GoToProperty [ид недвижимости]")]
            public static void CommandHandler(GTANetworkAPI.Player sender, string propertyId)
            {
                RageMpCommandExecutor.CommandExecutor.Execute(sender, new TeleportToPropertyByIdRequest()
                {
                    PropertyId = PropertyId.Parse(propertyId)
                });
            }
        }
    }
}