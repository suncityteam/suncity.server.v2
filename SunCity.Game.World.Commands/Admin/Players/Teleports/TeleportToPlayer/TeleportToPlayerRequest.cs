﻿using SunCity.Common.Commands.Interfaces;
using SunCity.Game.World.Commands.Common;

namespace SunCity.Game.World.Commands.Admin.Players.Teleports.TeleportToPlayer
{
    public class TeleportToPlayerRequest 
        : IRageMpCommandRequest
            , ICommandRequestWithTargetPlayer
    {
        public ushort TargetPlayerId { get; set; }
    }
}