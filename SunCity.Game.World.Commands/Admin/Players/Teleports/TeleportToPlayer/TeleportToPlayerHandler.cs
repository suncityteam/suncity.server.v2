﻿using System.Threading;
using System.Threading.Tasks;
using GTANetworkAPI;
using SunCity.Common.Commands.Interfaces;
using SunCity.Common.Operation;
using SunCity.Game.World.Commands.Common;

namespace SunCity.Game.World.Commands.Admin.Players.Teleports.TeleportToPlayer
{
    public class TeleportToPlayerHandler : RageMpCommandHandler<TeleportToPlayerRequest>
    {
        public TeleportToPlayerHandler(RageMpCommandConstructor<TeleportToPlayerRequest> constructor) : base(constructor)
        {
        }

        public override async Task<OperationResponse> Handle(TeleportToPlayerRequest request, CancellationToken cancellationToken)
        {
            var targetPlayer = RageMpPool.Players.GetAt(request.TargetPlayerId);
            if (targetPlayer == null || targetPlayer.Exists == false)
            {
                RagePlayer.SendChatMessage($"Игрок с ид {request.TargetPlayerId} не наден");
            }
            else
            {
                RagePlayer.Position = targetPlayer.Position;
                RagePlayer.SendChatMessage($"Вы телепортировались к {targetPlayer.Name}");
                targetPlayer.SendChatMessage($"Администратор телепортировался к вам");
            }

            return OperationResponseCache.Successfully;
        }

        public sealed class Command : Script
        {
            [Command("goto", "/goto [Ид игрока]")]
            public static void CommandHandler(GTANetworkAPI.Player sender, ushort targetPlayerId)
            {
                RageMpCommandExecutor.CommandExecutor.Execute(sender, new TeleportToPlayerRequest
                {
                    TargetPlayerId = targetPlayerId
                });
            }
        }
    }
}