﻿using SunCity.Common.Commands.Interfaces;
using SunCity.Game.World.Commands.Common;

namespace SunCity.Game.World.Commands.Admin.Players.Healths.GiveHealth
{
    public sealed class GiveHealthRequest
        : IRageMpCommandRequest
        , ICommandRequestWithTargetPlayer
    {
        public ushort TargetPlayerId { get; set; }
        public int Health { get; set; }
    }
}