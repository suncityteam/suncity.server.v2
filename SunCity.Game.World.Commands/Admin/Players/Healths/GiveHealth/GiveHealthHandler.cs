﻿using System.Threading;
using System.Threading.Tasks;
using GTANetworkAPI;
using SunCity.Common.Commands.Interfaces;
using SunCity.Common.Operation;
using SunCity.Game.World.Commands.Common;

namespace SunCity.Game.World.Commands.Admin.Players.Healths.GiveHealth
{
    public sealed class GiveHealthHandler : RageMpCommandHandler<GiveHealthRequest>
    {
        public GiveHealthHandler(RageMpCommandConstructor<GiveHealthRequest> constructor) 
            : base(constructor)
        {
        }

        public override async Task<OperationResponse> Handle(GiveHealthRequest request, CancellationToken cancellationToken)
        {
            var targetPlayer = RageMpPool.Players.GetAt(request.TargetPlayerId);
            if (targetPlayer == null || targetPlayer.Exists == false)
            {
                RagePlayer.SendChatMessage($"Игрок с ид {request.TargetPlayerId} не найден");
            }
            else
            {
                targetPlayer.Health = request.Health;
                targetPlayer.SendChatMessage($"Администратор {RagePlayer.Name} выдал {request.Health} здоровья");
                RagePlayer.SendChatMessage($"Вы выдали {request.Health} здоровья для {targetPlayer.Name}");

                if (targetPlayer.Health > 0)
                {
                    targetPlayer.StopAnimation();
                }
            }

            return OperationResponseCache.Successfully;
        }

        public sealed class Command : Script
        {
            [Command("givehp", "/GiveHp [Ид игрока] [Здоровье]")]
            public static void CommandHandler(GTANetworkAPI.Player sender, ushort targetPlayerId, int health)
            {
                RageMpCommandExecutor.CommandExecutor.Execute(sender, new GiveHealthRequest
                {
                    Health = health,
                    TargetPlayerId = targetPlayerId
                });
            }
        }
    }
}