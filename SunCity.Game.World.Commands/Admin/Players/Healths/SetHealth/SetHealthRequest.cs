﻿using SunCity.Common.Commands.Interfaces;
using SunCity.Game.World.Commands.Common;

namespace SunCity.Game.World.Commands.Admin.Players.Healths.SetHealth
{
    public class SetHealthRequest
        : IRageMpCommandRequest
            , ICommandRequestWithTargetPlayer
    {
        public ushort TargetPlayerId { get; set; }
        public int Health { get; set; }
    }
}