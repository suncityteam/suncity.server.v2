﻿using System.Threading;
using System.Threading.Tasks;
using GTANetworkAPI;
using SunCity.Common.Commands.Interfaces;
using SunCity.Common.Operation;
using SunCity.Game.World.Commands.Common;

namespace SunCity.Game.World.Commands.Admin.Players.Healths.SetHealth
{
    public class SetHealthHandler : RageMpCommandHandler<SetHealthRequest>
    {
        public SetHealthHandler(RageMpCommandConstructor<SetHealthRequest> constructor)
            : base(constructor)
        {
        }

        public override Task<OperationResponse> Handle(SetHealthRequest request, CancellationToken cancellationToken)
        {
            var targetPlayer = RageMpPool.Players.GetAt(request.TargetPlayerId);
            if (targetPlayer == null || targetPlayer.Exists == false)
            {
                RagePlayer.SendChatMessage($"Игрок с ид {request.TargetPlayerId} не найден");
            }
            else
            {
                targetPlayer.Health = request.Health;
                targetPlayer.SendChatMessage($"Администратор {RagePlayer.Name} изменил уровень вашего здоровья до {request.Health}");
                RagePlayer.SendChatMessage($"Вы изменили уровень здоровья {targetPlayer.Name} до {request.Health} ");

                if (targetPlayer.Health > 0)
                {
                    targetPlayer.StopAnimation();
                }
            }

            return TaskOperationResponseCache.Successfully;
        }

        public class Command : Script
        {
            [Command("sethp", "/SetHp [Ид игрока] [Здоровье]")]
            public static void CommandHandler(GTANetworkAPI.Player sender, ushort targetPlayerId, int health)
            {
                RageMpCommandExecutor.CommandExecutor.Execute(sender, new SetHealthRequest
                {
                    Health = health,
                    TargetPlayerId = targetPlayerId
                });
            }
        }
    }
}