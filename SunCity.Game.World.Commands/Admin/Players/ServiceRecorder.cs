﻿using Microsoft.Extensions.DependencyInjection;
using SunCity.Common.Commands;
using SunCity.Game.World.Commands.Admin.Players.Healths.GiveHealth;
using SunCity.Game.World.Commands.Admin.Players.Healths.SetHealth;
using SunCity.Game.World.Commands.Admin.Players.Teleports.TeleportPlayerToPlayer;
using SunCity.Game.World.Commands.Admin.Players.Teleports.TeleportToProperty.TeleportToPropertyById;
using SunCity.Game.World.Commands.Admin.Players.Teleports.TeleportToProperty.TeleportToVehicleShowRoom;

namespace SunCity.Game.World.Commands.Admin.Players
{
    internal static class ServiceRecorder
    {
        internal static void AddAdminPlayerCommands(this IServiceCollection InServiceCollection)
        {
            InServiceCollection.AddHealths();
            InServiceCollection.AddTeleports();
        } 
        
        private static void AddHealths(this IServiceCollection InServiceCollection)
        {
            InServiceCollection.AddRageMpCommandHandler<GiveHealthRequest, GiveHealthHandler>();
            InServiceCollection.AddRageMpCommandHandler<SetHealthRequest, SetHealthHandler>();
        }        

        private static void AddTeleports(this IServiceCollection InServiceCollection)
        {
            InServiceCollection.AddRageMpCommandHandler<TeleportPlayerToPlayerRequest, TeleportPlayerToPlayerHandler>();
            InServiceCollection.AddRageMpCommandHandler<TeleportPlayerToPlayerRequest, TeleportPlayerToPlayerHandler>();

            InServiceCollection.AddRageMpCommandHandler<TeleportToPropertyByIdRequest, TeleportToPropertyByIdHandler>();
            InServiceCollection.AddRageMpCommandHandler<TeleportToVehicleShowRoomRequest, TeleportToVehicleShowRoomHandler>();
        }
    }
}