﻿using SunCity.Common.Commands.Interfaces;
using SunCity.Game.World.Commands.Common;
using SunCity.Common.Commands.Interfaces;
using SunCity.Game.World.Commands.Common;

namespace SunCity.Game.World.Commands.Admin.Players.Positions.GetPlayerDimension
{
    public class GetPlayerDimensionRequest
        : IRageMpCommandRequest
        , ICommandRequestWithTargetPlayer
    {
        public ushort TargetPlayerId { get; set; }
    }
}
