﻿using System.Threading;
using System.Threading.Tasks;
using GTANetworkAPI;
using SunCity.Common.Commands.Interfaces;
using SunCity.Common.Operation;
using SunCity.Game.World.Commands.Common;
using SunCity.Game.World.Player;

namespace SunCity.Game.World.Commands.Admin.Players.Positions.GetPlayerDimension
{
    public class GetPlayerDimensionHandler : RageMpCommandHandler<GetPlayerDimensionRequest>
    {
        private IWorldPlayerStorage PlayerStorage { get; }

        public GetPlayerDimensionHandler
        (
            RageMpCommandConstructor<GetPlayerDimensionRequest> constructor,
            IWorldPlayerStorage playerStorage
        )
            : base(constructor)
        {
            PlayerStorage = playerStorage;
        }

        public override Task<OperationResponse> Handle(GetPlayerDimensionRequest request, CancellationToken cancellationToken)
        {
            var targetPlayer = RageMpPool.Players.GetAt(request.TargetPlayerId);
            PlayerStorage.TryGetPlayer(targetPlayer, out var targetWorldPlayer);
            
            RagePlayer.SendChatMessage($"Игрок находится в {targetWorldPlayer.Dimension}(реально: {targetPlayer.Dimension}) виртуальном мире. Недвижимость: {targetWorldPlayer.EnteredProperty?.PropertyCategory}");
            
            return TaskOperationResponseCache.Successfully;
        }

        public sealed class Command : Script
        {
            [Command("getdimension", "/GetDimension [ид игрока]")]
            public static void CommandHandler(GTANetworkAPI.Player sender, ushort targetPlayerId)
            {
                RageMpCommandExecutor.CommandExecutor.Execute(sender, new GetPlayerDimensionRequest
                {
                    TargetPlayerId = targetPlayerId,
                });
            }
        }
    }
}