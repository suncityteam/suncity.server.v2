﻿using SunCity.Common.Commands.Interfaces;

namespace SunCity.Game.World.Commands.Admin.Players.Positions.SavePosition
{
    public class SavePositionRequest : IRageMpCommandRequest
    {
        public string Comment { get; set; }
    }
}
