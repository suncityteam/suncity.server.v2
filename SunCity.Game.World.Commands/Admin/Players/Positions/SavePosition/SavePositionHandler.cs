﻿using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using SunCity.Common.Commands.Interfaces;
using SunCity.Common.Operation;

namespace SunCity.Game.World.Commands.Admin.Players.Positions.SavePosition
{
    public class SavePositionHandler : RageMpCommandHandler<SavePositionRequest>
    {
        public SavePositionHandler(RageMpCommandConstructor<SavePositionRequest> constructor) : base(constructor)
        {
        }

        public override Task<OperationResponse> Handle(SavePositionRequest request, CancellationToken cancellationToken)
        {
            Logger.LogWarning($"Saved position: {RagePlayer.Position} | Comment: {request.Comment}");
            return TaskOperationResponseCache.Successfully;
        }
    }
}