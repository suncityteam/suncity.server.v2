﻿using SunCity.Common.Commands.Interfaces;
using SunCity.Game.World.Commands.Common;
using System.Threading;
using System.Threading.Tasks;
using GTANetworkAPI;
using SunCity.Common.Operation;
using SunCity.Game.Types.Property.Entity.Interior;
using SunCity.Game.World.Extensions.Player;

namespace SunCity.Game.World.Commands.Admin.Players.IPL.LoadIPLForPlayer
{
    public class LoadIPLForPlayerHandler : RageMpCommandHandler<LoadIPLForPlayerRequest>
    {
        public LoadIPLForPlayerHandler(RageMpCommandConstructor<LoadIPLForPlayerRequest> constructor)
            : base(constructor)
        {
        }

        public override async Task<OperationResponse> Handle(LoadIPLForPlayerRequest request, CancellationToken cancellationToken)
        {
            var targetPlayer = RageMpPool.Players.GetAt(request.TargetPlayerId);
            targetPlayer.RequestIpl(request.Ipl);
            return OperationResponseCache.Successfully;
        }

        public sealed class Command : Script
        {
            [Command("loadipl", "/loadipl [ид игрока] [IPL]")]
            public static void CommandHandler(GTANetworkAPI.Player sender, ushort targetPlayerId, string ipl)
            {
                RageMpCommandExecutor.CommandExecutor.Execute(sender, new LoadIPLForPlayerRequest
                {
                    TargetPlayerId = targetPlayerId,
                    Ipl = new InteriorItemPlacement(ipl)
                });
            }
        }
    }
}