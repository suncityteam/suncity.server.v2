﻿using SunCity.Common.Commands.Interfaces;
using SunCity.Game.Types.Property.Entity.Interior;
using SunCity.Game.World.Commands.Common;

namespace SunCity.Game.World.Commands.Admin.Players.IPL.LoadIPLForPlayer
{
    public class LoadIPLForPlayerRequest
        : IRageMpCommandRequest
        , ICommandRequestWithTargetPlayer
    {
        public ushort TargetPlayerId { get; set; }
        public bool CleanClientIpl { get; set; }
        public InteriorItemPlacement Ipl { get; set; }
    }
}
