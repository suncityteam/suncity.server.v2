﻿using System.Threading;
using System.Threading.Tasks;
using GTANetworkAPI;
using SunCity.Common.Commands.Interfaces;
using SunCity.Common.Operation;
using SunCity.Game.World.Commands.Common;

namespace SunCity.Game.World.Commands.Admin.Players.Effects.StartEffectForPlayer
{
    public class StartEffectForPlayerHandler: RageMpCommandHandler<StartEffectForPlayerRequest>
    {
        public StartEffectForPlayerHandler(RageMpCommandConstructor<StartEffectForPlayerRequest> constructor) 
            : base(constructor)
        {
        }

        public override async Task<OperationResponse> Handle(StartEffectForPlayerRequest request, CancellationToken cancellationToken)
        {
            var targetPlayer = RageMpPool.Players.GetAt(request.TargetPlayerId);
            
            RagePlayer.SendChatMessage($"Вы включили Effect: {request.EffectName}");
            return OperationResponseCache.Successfully;
        }

        public sealed class Command : Script
        {
            [Command("starteffect", "/StartEffect [Эффект] [Длительность] [Цикличность]")]
            public static void CommandHandler(GTANetworkAPI.Player sender, ushort targetPlayerId, string effect, int duration, bool loop)
            {
                RageMpCommandExecutor.CommandExecutor.Execute(sender, new StartEffectForPlayerRequest
                {
                    TargetPlayerId = targetPlayerId,
                    EffectName = effect,
                    Duration = duration,
                    Loop = loop
                });
            }
        }
    }
}