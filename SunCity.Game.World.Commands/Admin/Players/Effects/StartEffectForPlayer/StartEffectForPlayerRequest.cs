﻿using SunCity.Common.Commands.Interfaces;
using SunCity.Game.World.Commands.Common;

namespace SunCity.Game.World.Commands.Admin.Players.Effects.StartEffectForPlayer
{
    public class StartEffectForPlayerRequest 
        : IRageMpCommandRequest
        , ICommandRequestWithTargetPlayer
    {
        public ushort TargetPlayerId { get; set; }
        public string EffectName { get; set; }
        public bool Loop { get; set; }
        public int Duration { get; set; }
    }
}
