﻿using SunCity.Common.Commands.Interfaces;
using SunCity.Game.World.Commands.Common;

namespace SunCity.Game.World.Commands.Admin.Players.Effects.StopEffectForPlayer
{
    public class StopEffectForPlayerRequest       
        : IRageMpCommandRequest
        , ICommandRequestWithTargetPlayer
    {
        public ushort TargetPlayerId { get; set; }
        public string EffectName { get; set; }
    }
}
