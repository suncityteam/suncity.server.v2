﻿using System.Threading;
using System.Threading.Tasks;
using GTANetworkAPI;
using SunCity.Common.Commands.Interfaces;
using SunCity.Common.Operation;
using SunCity.Game.World.Commands.Common;

namespace SunCity.Game.World.Commands.Admin.Players.Effects.StopEffectForPlayer
{
    public class StopEffectForPlayerHandler: RageMpCommandHandler<StopEffectForPlayerRequest>
    {
        public StopEffectForPlayerHandler(RageMpCommandConstructor<StopEffectForPlayerRequest> constructor) 
            : base(constructor)
        {
        }

        public override async Task<OperationResponse> Handle(StopEffectForPlayerRequest request, CancellationToken cancellationToken)
        {
            var targetPlayer = RageMpPool.Players.GetAt(request.TargetPlayerId);
            RagePlayer.SendChatMessage($"Вы выключили Effect: {request.EffectName}");

            return OperationResponseCache.Successfully;
        }

        public sealed class Command : Script
        {
            [Command("stopeffect", "/StopEffect [Эффект] [Длительность] [Цикличность]")]
            public static void CommandHandler(GTANetworkAPI.Player sender, ushort targetPlayerId, string effect, int duration, bool loop)
            {
                RageMpCommandExecutor.CommandExecutor.Execute(sender, new StopEffectForPlayerRequest
                {
                    TargetPlayerId = targetPlayerId,
                    EffectName = effect,
                });
            }
        }
    }
}