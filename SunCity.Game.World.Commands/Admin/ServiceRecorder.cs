﻿using Microsoft.Extensions.DependencyInjection;
using SunCity.Game.World.Commands.Admin.Players;
using SunCity.Game.World.Commands.Admin.Properties;
using SunCity.Game.World.Commands.Admin.Vehicles;

namespace SunCity.Game.World.Commands.Admin
{
    internal static class ServiceRecorder
    {
        internal static void AddAdminCommands(this IServiceCollection InServiceCollection)
        {
            InServiceCollection.AddAdminPlayerCommands();
            InServiceCollection.AddAdminPropertiesCommands();
            InServiceCollection.AddAdminVehiclesCommands();
        }
    }
}