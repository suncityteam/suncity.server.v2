﻿using SunCity.Common.Commands.Interfaces;
using SunCity.Game.World.Commands.Common;

namespace SunCity.Game.World.Commands.Admin.Vehicles.SetVehicleHealth
{
    public class SetVehicleHealthRequest        
        : IRageMpCommandRequest
        , ICommandRequestWithTargetVehicleOptional
    {
        public ushort? TargetVehicleId { get; set; }
    }
}
