﻿using Microsoft.Extensions.DependencyInjection;
using SunCity.Common.Commands;
using SunCity.Game.World.Commands.Admin.Vehicles.CreateVeh;

namespace SunCity.Game.World.Commands.Admin.Vehicles
{
    internal static class ServiceRecorder
    {
        internal static void AddAdminVehiclesCommands(this IServiceCollection InServiceCollection)
        {
            InServiceCollection.AddCreateVeh();
        }    
        
        private static void AddCreateVeh(this IServiceCollection InServiceCollection)
        {
            InServiceCollection.AddRageMpCommandHandler<CreateVehRequest, CreateVehHandler>();
        }
    }
}