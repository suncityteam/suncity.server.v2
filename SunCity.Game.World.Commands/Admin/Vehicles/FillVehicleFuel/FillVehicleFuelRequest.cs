﻿using SunCity.Common.Commands.Interfaces;
using SunCity.Game.World.Commands.Common;

namespace SunCity.Game.World.Commands.Admin.Vehicles.FillVehicleFuel
{
    public class FillVehicleFuelRequest      
        : IRageMpCommandRequest
        , ICommandRequestWithTargetVehicleOptional
    {
        public ushort? TargetVehicleId { get; set; }
    }
}