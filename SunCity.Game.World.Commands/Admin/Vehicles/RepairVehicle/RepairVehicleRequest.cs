﻿using SunCity.Common.Commands.Interfaces;
using SunCity.Game.World.Commands.Common;

namespace SunCity.Game.World.Commands.Admin.Vehicles.RepairVehicle
{
    public class RepairVehicleRequest        
        : IRageMpCommandRequest
        , ICommandRequestWithTargetVehicleOptional
    {
        public ushort? TargetVehicleId { get; set; }
    }
}
