﻿using System;
using System.Threading.Tasks;
using GTANetworkAPI;
using Microsoft.Extensions.DependencyInjection;
using SunCity.Common.RageMp.Pools.Vehicles;
using SunCity.Game.World.Vehicle;

namespace SunCity.Game.World.Commands.Admin.Vehicles.RepairVehicle
{
  // // [CommandName("Repair")]
  // // [CommandName("FixVeh")]
  //  //[CommandDescription("Отремонтировать ТС в котором игрок")]
    public class RepairVehicleHandler// : ICommand
    {
        private IWorldVehicleStorage VehicleStorage { get; }

        public RepairVehicleHandler(IServiceProvider InServiceProvider)
        {
            VehicleStorage = InServiceProvider.GetService<IWorldVehicleStorage>();
        }

        public async Task Execute(GTANetworkAPI.Player InPlayer)
        {
            var vehicle = InPlayer.Vehicle;

            if (vehicle == null || vehicle.Exists == false)
            {
                InPlayer.SendChatMessage($"Вы не в машине что бы ее отремонтировать");
                return;
            }

            var worldVehicle = VehicleStorage[(IRageVehicle) vehicle];
            worldVehicle.Health.BodyHealth.Value = 1000;
            worldVehicle.Health.EngineHealth.Value = 1000;
            vehicle.Repair();

            InPlayer.SendChatMessage($"Вы отремонтировали {(VehicleHash)vehicle.Model}");
        }
    }
}
