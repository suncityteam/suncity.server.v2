﻿using System.Threading;
using System.Threading.Tasks;
using GTANetworkAPI;
using SunCity.Common.Commands.Interfaces;
using SunCity.Common.Operation;
using SunCity.Game.World.Commands.Common;
using SunCity.Game.World.Services.Vehicle.Services;
using SunCity.Game.World.Vehicle;

namespace SunCity.Game.World.Commands.Admin.Vehicles.FlipVehicle
{
    public sealed class FlipVehicleHandler : RageMpCommandHandler<FlipVehicleRequest>
    {
        private IWorldVehicleStorage VehicleStorage { get; }
        private IWorldVehicleServices WorldVehicleServices { get; }

        public FlipVehicleHandler
        (
            RageMpCommandConstructor<FlipVehicleRequest> constructor,
            IWorldVehicleStorage vehicleStorage,
            IWorldVehicleServices worldVehicleServices
        ) : base(constructor)
        {
            VehicleStorage = vehicleStorage;
            WorldVehicleServices = worldVehicleServices;
        }

        public override async Task<OperationResponse> Handle(FlipVehicleRequest request, CancellationToken cancellationToken)
        {
            return OperationResponseCache.Successfully;
        }

        public sealed class Command : Script
        {
            [Command("flipveh", "/flipveh (Ид машины)")]
            public static void CommandHandler(GTANetworkAPI.Player sender, ushort? vehicleId)
            {
                RageMpCommandExecutor.CommandExecutor.Execute(sender, new FlipVehicleRequest()
                {
                    TargetVehicleId = vehicleId
                });
            }
        }
    }
}