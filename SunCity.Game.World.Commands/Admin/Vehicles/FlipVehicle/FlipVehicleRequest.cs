﻿using SunCity.Common.Commands.Interfaces;
using SunCity.Game.World.Commands.Common;

namespace SunCity.Game.World.Commands.Admin.Vehicles.FlipVehicle
{
    public class FlipVehicleRequest
        : IRageMpCommandRequest
        , ICommandRequestWithTargetVehicleOptional
    {
        public ushort? TargetVehicleId { get; set; }
    }
}