﻿using System.Threading;
using System.Threading.Tasks;
using GTANetworkAPI;
using SunCity.Common.Commands.Interfaces;
using SunCity.Common.Operation;
using SunCity.Game.World.Commands.Common;
using SunCity.Game.World.Services.Vehicle.Models;
using SunCity.Game.World.Services.Vehicle.Services;
using SunCity.Game.World.Vehicle;

namespace SunCity.Game.World.Commands.Admin.Vehicles.CreateVeh
{
    public sealed class CreateVehHandler : RageMpCommandHandler<CreateVehRequest>
    {
        private IWorldVehicleStorage VehicleStorage { get; }
        private IWorldVehicleServices WorldVehicleServices { get; }

        public CreateVehHandler(RageMpCommandConstructor<CreateVehRequest> constructor, IWorldVehicleStorage vehicleStorage, IWorldVehicleServices worldVehicleServices) : base(constructor)
        {
            VehicleStorage = vehicleStorage;
            WorldVehicleServices = worldVehicleServices;
        }

        public override async Task<OperationResponse> Handle(CreateVehRequest request, CancellationToken cancellationToken)
        {
            var entity = await WorldVehicleServices.CreateVehicle(new CreateWorldVehicleModel
            {
                Model = request.Model,
                VehicleId = VehicleId.GenerateId(),

                Position = RagePlayer.Position,
                Rotation = RagePlayer.Rotation,

                FirstColor = request.FirstColor,
                SecondColor = request.SecondColor,
            });

            var vehicle = VehicleStorage.AddVehicle(entity);
            RagePlayer.SendChatMessage($"Создан {request.Model}, ид: {vehicle.Vehicle.Id} / {vehicle.EntityId}");
            RagePlayer.SetIntoVehicle(vehicle.Vehicle, -1);

            return OperationResponseCache.Successfully;
        }

        public sealed class Command : Script
        {
            [Command("veh", "/Veh [Модель] [Основной цвет] [Дополнительный цвет]")]
            public static void CommandHandler(GTANetworkAPI.Player sender, VehicleHash vehicleHash, int primaryColor, int secondaryColor)
            {
                RageMpCommandExecutor.CommandExecutor.Execute(sender, new CreateVehRequest()
                {
                    Model = vehicleHash,
                    FirstColor = primaryColor,
                    SecondColor = secondaryColor
                });
            }
        }
    }
}