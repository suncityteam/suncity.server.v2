﻿using GTANetworkAPI;
using SunCity.Common.Commands.Interfaces;

namespace SunCity.Game.World.Commands.Admin.Vehicles.CreateVeh
{
    public sealed class CreateVehRequest : IRageMpCommandRequest
    {
        public VehicleHash Model { get; set; }

        public int FirstColor { get; set; }

        public int SecondColor { get; set; }

        public override string ToString()
        {
            return $"Model: {Model}, FirstColor: {FirstColor}, SecondColor: {SecondColor}";
        }
    }
}
