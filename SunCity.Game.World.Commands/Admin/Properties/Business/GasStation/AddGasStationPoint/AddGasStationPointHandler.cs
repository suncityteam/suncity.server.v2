﻿using System.Threading;
using System.Threading.Tasks;
using GTANetworkAPI;
using SunCity.Common.Commands.Interfaces;
using SunCity.Common.Operation;
using SunCity.Common.Types.Extensions;
using SunCity.Game.Enums.World.Property;
using SunCity.Game.World.Commands.Common;
using SunCity.Game.World.DataBase;
using SunCity.Game.World.DataBase.Entitites.Property.Business.GasStation;
using SunCity.Game.World.Property;
using SunCity.Game.World.Property.Category.Business.GasStation;

namespace SunCity.Game.World.Commands.Admin.Properties.Business.GasStation.AddGasStationPoint
{
    public class AddGasStationPointHandler : RageMpCommandHandler<AddGasStationPointRequest>
    {
        private GameWorldDbContext DbContext { get; }
        private IWorldPropertyStorage PropertyStorage { get; }

        public AddGasStationPointHandler(RageMpCommandConstructor<AddGasStationPointRequest> constructor, GameWorldDbContext dbContext, IWorldPropertyStorage propertyStorage) : base(constructor)
        {
            DbContext = dbContext;
            PropertyStorage = propertyStorage;
        }

        public override async Task<OperationResponse> Handle(AddGasStationPointRequest request, CancellationToken cancellationToken)
        {
            var gasStation = await PropertyStorage.FindNearProperty<GasStationWorldProperty>(RagePlayer.Position, WorldPropertyCategory.GasStation, 50.0f);
            var point = new GasStationPointEntity
            {
                EntityId = GasStationPointId.GenerateId(),
                GasStationPropertyId = gasStation.EntityId,
                Position = RagePlayer.Position.AddZ(-1f),
            };

            await DbContext.GasStationPointEntity.AddAsync(point, cancellationToken);
            await DbContext.SaveChangesAsync(cancellationToken);
            gasStation.AddPoint(point, RageMpPool);

            RagePlayer.SendChatMessage($"Точка АЗС была успешно создана");

            return OperationResponseCache.Successfully;
        }

        public sealed class Command : Script
        {
            [Command("addgasstationpoint", "/AddGasStationPoint")]
            public static void CommandHandler(GTANetworkAPI.Player sender)
            {
                RageMpCommandExecutor.CommandExecutor.Execute(sender, new AddGasStationPointRequest()
                {

                });
            }
        }
    }
}