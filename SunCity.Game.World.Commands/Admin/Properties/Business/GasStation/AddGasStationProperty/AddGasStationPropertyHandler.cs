﻿using System;
using System.Threading;
using System.Threading.Tasks;
using GTANetworkAPI;
using SunCity.Common.Commands.Interfaces;
using SunCity.Common.Operation;
using SunCity.Common.Types.Time;
using SunCity.Game.Enums;
using SunCity.Game.Enums.World.Property;
using SunCity.Game.Types.Cost;
using SunCity.Game.Types.Property.Entity.Property;
using SunCity.Game.World.Commands.Common;
using SunCity.Game.World.DataBase;
using SunCity.Game.World.DataBase.Entitites.Property;
using SunCity.Game.World.DataBase.Entitites.Property.Business;
using SunCity.Game.World.DataBase.Entitites.Property.Business.GasStation;
using SunCity.Game.World.Property;

namespace SunCity.Game.World.Commands.Admin.Properties.Business.GasStation.AddGasStationProperty
{
    public sealed class AddGasStationPropertyHandler : RageMpCommandHandler<AddGasStationPropertyRequest>
    {
        private GameWorldDbContext DbContext { get; }
        private IWorldPropertyStorage PropertyStorage { get; }
        private WorldPropertyFactory Factory { get; }

        public AddGasStationPropertyHandler(RageMpCommandConstructor<AddGasStationPropertyRequest> constructor, GameWorldDbContext dbContext, IWorldPropertyStorage propertyStorage, WorldPropertyFactory factory) : base(constructor)
        {
            DbContext = dbContext;
            PropertyStorage = propertyStorage;
            Factory = factory;
        }

        public override async Task<OperationResponse> Handle(AddGasStationPropertyRequest request, CancellationToken cancellationToken)
        {
            var propertyEntry = await DbContext.PropertyEntity.AddAsync(new PropertyEntity()
            {
                EntityId = PropertyId.GenerateId(),
                PropertyClass = WorldPropertyClass.None,
                PropertyCategory = WorldPropertyCategory.GasStation,

                IconPosition = RagePlayer.Position,
                Position = RagePlayer.Position,
                Rotation = RagePlayer.Rotation.Z,

                Name = new PropertyName("АЗС"),

                SellingCost = new GameCost(request.Price, request.Currency),
                StateCost = new GameCost(request.Price, request.Currency),

                BusinessPropertyEntity = new BusinessPropertyEntity()
                {
                    GasStationPropertyEntity = new GasStationPropertyEntity(),
                    WorkingTime = new TimeRange
                    {
                        Begin = TimeSpan.FromHours(7.5),
                        End = TimeSpan.FromHours(19.0)
                    }
                }
            }, cancellationToken);

            await DbContext.SaveChangesAsync(cancellationToken);

            var property = propertyEntry.Entity;
            RagePlayer.SendChatMessage($"АЗС была добавлен в БД со стоимостью {property.StateCost}");

            var worldProperty = Factory.CreateProperty(property);
            if (worldProperty.IsCorrect)
            {
                await PropertyStorage.AddProperty(worldProperty.Content);
            }
            else
            {
                RagePlayer.SendChatMessage($"Неудалось добавить АЗС в игровой мир. {worldProperty.Error}");
            }

            return OperationResponseCache.Successfully;
        }

        public sealed class Command : Script
        {
            [Command("addgasstation", "/AddGasStation [стоимость] [валюта]")]
            public static void CommandHandler(GTANetworkAPI.Player sender, long price, GameCurrency currency)
            {
                RageMpCommandExecutor.CommandExecutor.Execute(sender, new AddGasStationPropertyRequest()
                {
                    Price = price,
                    Currency = currency
                });
            }
        }
    }
}