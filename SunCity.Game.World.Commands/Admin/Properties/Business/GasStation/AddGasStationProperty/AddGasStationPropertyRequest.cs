﻿using SunCity.Common.Commands.Interfaces;
using SunCity.Game.Enums;

namespace SunCity.Game.World.Commands.Admin.Properties.Business.GasStation.AddGasStationProperty
{
    public sealed class AddGasStationPropertyRequest : IRageMpCommandRequest
    {
        public long Price { get; set; }
        public GameCurrency Currency { get; set; }
    }
}
