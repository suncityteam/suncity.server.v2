﻿using Microsoft.Extensions.DependencyInjection;
using SunCity.Common.Commands;
using SunCity.Common.MediatR.Extensions;
using SunCity.Game.World.Commands.Admin.Properties.Business.GasStation.AddGasStationPoint;
using SunCity.Game.World.Commands.Admin.Properties.Business.GasStation.AddGasStationProperty;
using SunCity.Game.World.Commands.Common.Validators.Property;
using SunCity.Game.World.Commands.Common.Validators.Vehicle;

namespace SunCity.Game.World.Commands.Admin.Properties.Business.GasStation
{
    internal static class ServiceRecorder
    {
        internal static void AddAdminGasStationBusinessPropertiesCommands(this IServiceCollection InServiceCollection)
        {
            InServiceCollection.AddPipelineBehavior<AddGasStationPropertyRequest, WhenPlayerNotInVehicle<AddGasStationPropertyRequest>>();
            InServiceCollection.AddPipelineBehavior<AddGasStationPropertyRequest, WhenPlayerNotInProperty<AddGasStationPropertyRequest>>();
            InServiceCollection.AddRageMpCommandHandler<AddGasStationPropertyRequest, AddGasStationPropertyHandler>();

            InServiceCollection.AddPipelineBehavior<AddGasStationPointRequest, WhenPlayerInVehicle<AddGasStationPointRequest>>();
            InServiceCollection.AddPipelineBehavior<AddGasStationPointRequest, WhenPlayerNotInProperty<AddGasStationPointRequest>>();
            InServiceCollection.AddRageMpCommandHandler<AddGasStationPointRequest, AddGasStationPointHandler>();
        }
    }
}