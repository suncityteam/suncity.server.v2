﻿using System.Threading;
using System.Threading.Tasks;
using GTANetworkAPI;
using SunCity.Common.Commands.Interfaces;
using SunCity.Common.Operation;
using SunCity.Common.Types.Extensions;
using SunCity.Game.Enums.World.Property;
using SunCity.Game.World.Commands.Common;
using SunCity.Game.World.DataBase;
using SunCity.Game.World.DataBase.Entitites.Property.Business.CarShowroom;
using SunCity.Game.World.Property;
using SunCity.Game.World.Property.Category.Business.CarShowroom;

namespace SunCity.Game.World.Commands.Admin.Properties.Business.VehicleShowRoom.AddVehicleShowRoomSpawnPoint
{
    public sealed class AddVehicleShowRoomSpawnPointHandler : RageMpCommandHandler<AddVehicleShowRoomSpawnPointRequest>
    {
        private GameWorldDbContext DbContext { get; }
        private IWorldPropertyStorage PropertyStorage { get; }
        
        public AddVehicleShowRoomSpawnPointHandler(RageMpCommandConstructor<AddVehicleShowRoomSpawnPointRequest> constructor, GameWorldDbContext dbContext, IWorldPropertyStorage propertyStorage) : base(constructor)
        {
            DbContext = dbContext;
            PropertyStorage = propertyStorage;
        }

        public override async Task<OperationResponse> Handle(AddVehicleShowRoomSpawnPointRequest request, CancellationToken cancellationToken)
        {
            var vehicleShowRoom = await PropertyStorage
                .FindNearProperty<CarShowroomWorldProperty>(RagePlayer.Position, WorldPropertyCategory.CarShowroom, 50.0f);

            var point = new CarShowroomVehicleSpawnPointEntity()
            {
                EntityId = CarShowroomVehicleSpawnId.GenerateId(),
                CarShowroomId = vehicleShowRoom.EntityId,
                Position = RagePlayer.Position.AddZ(-1f),
                Rotation = RagePlayer.Rotation,
            };

            await DbContext.CarShowroomVehicleSpawnPointEntity.AddAsync(point, cancellationToken);
            await DbContext.SaveChangesAsync(cancellationToken);

            vehicleShowRoom.AddSpawnPoint(point);
            RagePlayer.SendChatMessage($"Точка спавна была успешно создана");

            return OperationResponseCache.Successfully;
        }

        public sealed class Command : Script
        {
            [Command("addvehicleshowroomspawnpoint", "/AddVehicleShowRoomSpawnPoint ")]
            public static void CommandHandler(GTANetworkAPI.Player sender)
            {
                RageMpCommandExecutor.CommandExecutor.Execute(sender, new AddVehicleShowRoomSpawnPointRequest()
                {

                });
            }
        }
    }
}