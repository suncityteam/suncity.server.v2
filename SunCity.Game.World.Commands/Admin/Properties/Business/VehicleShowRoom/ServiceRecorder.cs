﻿using Microsoft.Extensions.DependencyInjection;
using SunCity.Common.Commands;
using SunCity.Common.MediatR.Extensions;
using SunCity.Game.World.Commands.Admin.Properties.Business.VehicleShowRoom.AddVehicleShowRoomSpawnPoint;
using SunCity.Game.World.Commands.Common.Validators.Property;
using SunCity.Game.World.Commands.Common.Validators.Vehicle;

namespace SunCity.Game.World.Commands.Admin.Properties.Business.VehicleShowRoom
{
    internal static class ServiceRecorder
    {
        internal static void AddAdminVehicleShowRoomBusinessPropertiesCommands(this IServiceCollection InServiceCollection)
        {
            InServiceCollection.AddPipelineBehavior<AddVehicleShowRoomSpawnPointRequest, WhenPlayerNotInVehicle<AddVehicleShowRoomSpawnPointRequest>>();
            InServiceCollection.AddPipelineBehavior<AddVehicleShowRoomSpawnPointRequest, WhenPlayerNotInProperty<AddVehicleShowRoomSpawnPointRequest>>();
            InServiceCollection.AddRageMpCommandHandler<AddVehicleShowRoomSpawnPointRequest, AddVehicleShowRoomSpawnPointHandler>();
        }
    }
}