﻿using System.Threading;
using System.Threading.Tasks;
using SunCity.Common.Commands.Interfaces;
using SunCity.Common.Operation;

namespace SunCity.Game.World.Commands.Admin.Properties.Business.Bank.AddPlayerBankCard
{
    public class AddPlayerBankCardHandler : RageMpCommandHandler<AddPlayerBankCardRequest>
    {
        public AddPlayerBankCardHandler(RageMpCommandConstructor<AddPlayerBankCardRequest> constructor) : base(constructor)
        {
        }

        public override async Task<OperationResponse> Handle(AddPlayerBankCardRequest request, CancellationToken cancellationToken)
        {
            throw new System.NotImplementedException();
        }
    }
}