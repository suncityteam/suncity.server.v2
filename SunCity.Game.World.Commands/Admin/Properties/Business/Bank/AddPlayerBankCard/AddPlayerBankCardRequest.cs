﻿using SunCity.Common.Commands.Interfaces;
using SunCity.Game.World.Commands.Common;

namespace SunCity.Game.World.Commands.Admin.Properties.Business.Bank.AddPlayerBankCard
{
    public class AddPlayerBankCardRequest
        : IRageMpCommandRequest
        , ICommandRequestWithTargetPlayer
    {
        public ushort TargetPlayerId { get; set; }
        public PropertyId BankPropertyId { get; set; }
        public long Balance { get; set; }
    }
}