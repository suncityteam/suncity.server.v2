﻿using Microsoft.Extensions.DependencyInjection;
using SunCity.Common.Commands;
using SunCity.Common.MediatR.Extensions;
using SunCity.Game.World.Commands.Admin.Properties.Business.Bank.AddBankProperty;
using SunCity.Game.World.Commands.Common.Validators.Property;
using SunCity.Game.World.Commands.Common.Validators.Vehicle;

namespace SunCity.Game.World.Commands.Admin.Properties.Business.Bank
{
    internal static class ServiceRecorder
    {
        internal static void AddAdminBankBusinessPropertiesCommands(this IServiceCollection InServiceCollection)
        {
            InServiceCollection.AddPipelineBehavior<AddBankPropertyRequest, WhenPlayerNotInVehicle<AddBankPropertyRequest>>();
            InServiceCollection.AddPipelineBehavior<AddBankPropertyRequest, WhenPlayerNotInProperty<AddBankPropertyRequest>>();
            InServiceCollection.AddRageMpCommandHandler<AddBankPropertyRequest, AddBankPropertyHandler>();
        }
    }
}