﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using GTANetworkAPI;
using SunCity.Common.Types.Time;
using SunCity.Game.Enums;
using SunCity.Game.Enums.World.Property;
using SunCity.Game.Types.Cost;
using SunCity.Game.Types.Property.Category.Bank;
using SunCity.Game.Types.Property.Entity.Property;
using SunCity.Game.World.DataBase;
using SunCity.Game.World.DataBase.Entitites.Property;
using SunCity.Game.World.DataBase.Entitites.Property.Business;
using SunCity.Game.World.DataBase.Entitites.Property.Business.Bank;
using SunCity.Game.World.DataBase.Entitites.Property.Business.Bank.Tariff;
using SunCity.Game.World.Property;
using SunCity.Common.Commands.Interfaces;
using SunCity.Common.Operation;
using SunCity.Game.World.Commands.Common;

namespace SunCity.Game.World.Commands.Admin.Properties.Business.Bank.AddBankProperty
{
    public class AddBankPropertyHandler : RageMpCommandHandler<AddBankPropertyRequest>
    {
        private GameWorldDbContext DbContext { get; }
        private IWorldPropertyStorage PropertyStorage { get; }
        private WorldPropertyFactory Factory { get; }

        public AddBankPropertyHandler
        (
            RageMpCommandConstructor<AddBankPropertyRequest> constructor,
            GameWorldDbContext dbContext,
            IWorldPropertyStorage propertyStorage,
            WorldPropertyFactory factory
        ) : base(constructor)
        {
            DbContext = dbContext;
            PropertyStorage = propertyStorage;
            Factory = factory;
        }

        public override async Task<OperationResponse> Handle(AddBankPropertyRequest request, CancellationToken cancellationToken)
        {
            var property = DbContext.PropertyEntity.Add(new PropertyEntity()
            {
                EntityId = PropertyId.GenerateId(),
                PropertyClass = WorldPropertyClass.None,
                PropertyCategory = WorldPropertyCategory.Bank,

                IconPosition = RagePlayer.Position,
                Position = RagePlayer.Position,
                Rotation = RagePlayer.Rotation.Z,

                InteriorId = new InteriorId(Guid.Parse("23a1f959-f689-490a-9d78-8f4e66b4710b")),
                Name = new PropertyName("Банк"),

                SellingCost = new GameCost(10000, GameCurrency.Donate),
                StateCost = new GameCost(10000, GameCurrency.Donate),

                BusinessPropertyEntity = new BusinessPropertyEntity()
                {
                    BankPropertyEntity = new BankPropertyEntity()
                    {
                        CardTariffEntities = new List<BankCardTariffEntity>()
                        {
                            new BankCardTariffEntity()
                            {
                                EntityId = BankCardTariffId.GenerateId(),
                                Name = "Эконом",
                                Deposit = new BankTariffFee(),
                                MaintenanceCost = new GameCost(),
                                TariffCost = new GameCost(),
                                Transfer = new BankTariffFee(),
                                WithDraw = new BankTariffFee(),
                            }
                        }
                    },
                    WorkingTime = new TimeRange
                    {
                        Begin = TimeSpan.FromHours(7.5),
                        End = TimeSpan.FromHours(19.0)
                    }
                }
            }).Entity;

            await DbContext.SaveChangesAsync(cancellationToken);
            RagePlayer.SendChatMessage($"Банк был добавлен в БД со стоимостью {property.StateCost}");

            var worldProperty = Factory.CreateProperty(property);
            if (worldProperty.IsCorrect)
            {
                await PropertyStorage.AddProperty(worldProperty.Content);
            }
            else
            {
                RagePlayer.SendChatMessage($"Неудалось добавить Банк в игровой мир. {worldProperty.Error}");
            }

            return OperationResponseCache.Successfully;
        }

        public sealed class Command : Script
        {
            [Command("addbank", "/AddBankProperty")]
            public static void CommandHandler(GTANetworkAPI.Player sender)
            {
                RageMpCommandExecutor.CommandExecutor.Execute(sender, new AddBankPropertyRequest()
                {

                });
            }
        }
    }
}