﻿using Microsoft.Extensions.DependencyInjection;
using SunCity.Common.Commands;
using SunCity.Common.MediatR.Extensions;
using SunCity.Game.World.Commands.Admin.Properties.Business.Bank;
using SunCity.Game.World.Commands.Admin.Properties.Business.Bank.AddBankProperty;
using SunCity.Game.World.Commands.Admin.Properties.Business.GasStation;
using SunCity.Game.World.Commands.Admin.Properties.Business.GasStation.AddGasStationPoint;
using SunCity.Game.World.Commands.Admin.Properties.Business.GasStation.AddGasStationProperty;
using SunCity.Game.World.Commands.Admin.Properties.Business.VehicleShowRoom;
using SunCity.Game.World.Commands.Admin.Properties.Business.VehicleShowRoom.AddVehicleShowRoomSpawnPoint;
using SunCity.Game.World.Commands.Common.Validators.Property;
using SunCity.Game.World.Commands.Common.Validators.Vehicle;

namespace SunCity.Game.World.Commands.Admin.Properties.Business
{
    internal static class ServiceRecorder
    {
        internal static void AddAdminBusinessPropertiesCommands(this IServiceCollection InServiceCollection)
        {
            InServiceCollection.AddAdminBankBusinessPropertiesCommands();
            InServiceCollection.AddAdminGasStationBusinessPropertiesCommands();
            InServiceCollection.AddAdminVehicleShowRoomBusinessPropertiesCommands();
        }
    }
}