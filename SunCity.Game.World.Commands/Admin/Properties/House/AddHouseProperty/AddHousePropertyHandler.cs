﻿using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using GTANetworkAPI;
using JetBrains.Annotations;
using SunCity.Game.World.DataBase;
using SunCity.Common.Commands.Interfaces;
using SunCity.Common.Operation;
using SunCity.Game.Enums;
using SunCity.Game.Enums.World.Property;
using SunCity.Game.Types.Cost;
using SunCity.Game.Types.Property.Entity.Property;
using SunCity.Game.World.Commands.Common;
using SunCity.Game.World.DataBase.Entitites.Property;
using SunCity.Game.World.DataBase.Entitites.Property.Residence;
using SunCity.Game.World.Interior;
using SunCity.Game.World.Property;

namespace SunCity.Game.World.Commands.Admin.Properties.House.AddHouseProperty
{
    public class AddHousePropertyHandler : RageMpCommandHandler<AddHousePropertyRequest>
    {
        private GameWorldDbContext DbContext { get; }
        private IWorldPropertyStorage PropertyStorage { get; }
        private WorldPropertyFactory Factory { get; }
        private IWorldInteriorStorage InteriorStorage { get; }

        public AddHousePropertyHandler
        (
            RageMpCommandConstructor<AddHousePropertyRequest> constructor,
            GameWorldDbContext dbContext,
            IWorldPropertyStorage propertyStorage,
            WorldPropertyFactory factory,
            IWorldInteriorStorage interiorStorage
        ) : base(constructor)
        {
            DbContext = dbContext;
            PropertyStorage = propertyStorage;
            Factory = factory;
            InteriorStorage = interiorStorage;
        }

        public override async Task<OperationResponse> Handle(AddHousePropertyRequest request, CancellationToken cancellationToken)
        {
            var interior = InteriorStorage.Values.First();
            var property = DbContext.PropertyEntity.Add(new PropertyEntity()
            {
                EntityId = PropertyId.GenerateId(),
                PropertyClass = request.PropertyClass,
                PropertyCategory = WorldPropertyCategory.House,
                IconPosition = RagePlayer.Position,
                Position = RagePlayer.Position,
                Rotation = RagePlayer.Rotation.Z,
                Name = new PropertyName(string.Empty),
                SellingCost = request.Cost.CloneObject(),
                StateCost = request.Cost.CloneObject(),
                InteriorId = interior.EntityId,
                ResidencePropertyEntity = new ResidencePropertyEntity()
            }).Entity;

            await DbContext.SaveChangesAsync(cancellationToken);
            RagePlayer.SendChatMessage($"Дом {request.PropertyClass} класса был добавлен в БД со стоимостью {property.StateCost}");

            var worldProperty = Factory.CreateProperty(property);
            if (worldProperty.IsCorrect)
            {
                await PropertyStorage.AddProperty(worldProperty.Content);
            }
            else
            {
                RagePlayer.SendChatMessage($"Неудалось добавить дом в игровой мир. {worldProperty.Error}");
            }

            return OperationResponseCache.Successfully;
        }
        
        public sealed class Command : Script
        {
            [Command("addhouse", "/AddHouse [класс] [цена] [валюта]")]
            public static void CommandHandler([CanBeNull] GTANetworkAPI.Player sender, WorldPropertyClass propertyClass, long cost, GameCurrency currency)
            {
                RageMpCommandExecutor.CommandExecutor.Execute(sender, new AddHousePropertyRequest()
                {
                    PropertyClass = propertyClass,
                    Cost = new GameCost(cost, currency)
                });
            }
        }
    }
}