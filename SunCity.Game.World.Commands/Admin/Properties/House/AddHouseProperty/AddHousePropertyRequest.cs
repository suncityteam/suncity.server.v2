﻿using SunCity.Common.Commands.Interfaces;
using SunCity.Game.Enums;
using SunCity.Game.Enums.World.Property;
using SunCity.Game.Types.Cost;

namespace SunCity.Game.World.Commands.Admin.Properties.House.AddHouseProperty
{
    public class AddHousePropertyRequest : IRageMpCommandRequest
    {
        public WorldPropertyClass PropertyClass { get; set; }
        
        public GameCost Cost { get; set; }
    }
}