﻿using Microsoft.Extensions.DependencyInjection;
using SunCity.Game.World.Commands.Admin.Properties.Business;

namespace SunCity.Game.World.Commands.Admin.Properties
{
    internal static class ServiceRecorder
    {
        internal static void AddAdminPropertiesCommands(this IServiceCollection InServiceCollection)
        {
            InServiceCollection.AddAdminBusinessPropertiesCommands();
        } 
        

    }
}