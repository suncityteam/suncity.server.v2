﻿using System.Threading;
using System.Threading.Tasks;
using SunCity.Common.Commands.Interfaces;
using SunCity.Common.Operation;
using SunCity.Game.World.Events.Client.Property;
using SunCity.Game.World.Extensions.Player;
using SunCity.Game.World.Player;
using SunCity.Game.World.Property;

namespace SunCity.Game.World.Commands.Property.Controls.PropertyBuy
{
    public class PropertyBuyHandler : RageMpCommandHandler<PropertyBuyRequest>
    {
        private WorldPlayer WorldPlayer { get; }
        private IWorldPropertyStorage PropertyStorage { get; }

        public PropertyBuyHandler(RageMpCommandConstructor<PropertyBuyRequest> constructor, IWorldPropertyStorage propertyStorage, WorldPlayer worldPlayer) : base(constructor)
        {
            PropertyStorage = propertyStorage;
            WorldPlayer = worldPlayer;
        }

        public override async Task<OperationResponse> Handle(PropertyBuyRequest request, CancellationToken cancellationToken)
        {
            if (WorldPlayer.EnteredProperty != null)
            {
                StartPropertyBuy(WorldPlayer.EnteredProperty);
            }
            else
            {
                var nearProperty = await PropertyStorage.FindNearProperty(RagePlayer);
                if (nearProperty == null)
                {
                    WorldPlayer.SendClientMessage("Поблизости нет недвижимости которую можно купить");
                    return OperationResponseCache.Successfully;
                }

                StartPropertyBuy(nearProperty);
            }

            return OperationResponseCache.Successfully;
        }


        private void StartPropertyBuy(WorldProperty InWorldProperty)
        {
            if (InWorldProperty.IsNotSelling)
            {
                WorldPlayer.SendClientMessage("Недвижимость не продается");
            }
            else
            {
                RagePlayer.ShowPropertyBuyWindow(InWorldProperty.EntityId);
            }
        }
    }
}