﻿using Microsoft.Extensions.DependencyInjection;
using SunCity.Common.Commands;
using SunCity.Common.MediatR.Extensions;
using SunCity.Game.World.Commands.Common.Validators.Health;
using SunCity.Game.World.Commands.Property.Controls.PropertyBuy;

namespace SunCity.Game.World.Commands.Property.Controls
{
    internal static class ServiceRecorder
    {
        internal static void AddPropertyControlsCommands(this IServiceCollection InServiceCollection)
        {
            InServiceCollection.AddPipelineBehavior<PropertyBuyRequest, WhenPlayerAlive<PropertyBuyRequest>>();
            InServiceCollection.AddRageMpCommandHandler<PropertyBuyRequest, PropertyBuyHandler>();
        }
    }
}