﻿using System.Threading;
using System.Threading.Tasks;
using SunCity.Common.Commands.Interfaces;
using SunCity.Common.Operation;
using SunCity.Common.RageMp.Pools.Players;
using SunCity.Game.Enums.World.Property;
using SunCity.Game.World.Events.Client.Property.Business.Bank;
using SunCity.Game.World.Player;
using SunCity.Game.World.Property;

namespace SunCity.Game.World.Commands.Property.Category.Business.Bank.Client.ShowBankClientMenu
{
    public class ShowBankClientMenuHandler : RageMpCommandHandler<ShowBankClientMenuRequest>
    {
        private IWorldPropertyStorage PropertyStorage { get; }
        private WorldPlayer WorldPlayer { get; }

        public ShowBankClientMenuHandler
        (
            RageMpCommandConstructor<ShowBankClientMenuRequest> constructor,
            IWorldPropertyStorage propertyStorage, WorldPlayer worldPlayer) : base(constructor
        )
        {
            PropertyStorage = propertyStorage;
            WorldPlayer = worldPlayer;
        }

        public override Task<OperationResponse> Handle(ShowBankClientMenuRequest request, CancellationToken cancellationToken)
        {
            RagePlayer.ShowBankClientMenu(WorldPlayer.EnteredProperty.EntityId);
            return TaskOperationResponseCache.Successfully;
        }

        public async Task Execute(IRagePlayer InPlayer)
        {
            var nearProperty = await PropertyStorage.FindNearProperty(InPlayer);
            if (nearProperty == null || nearProperty.PropertyCategory != WorldPropertyCategory.Bank)
            {
                InPlayer.SendChatMessage("Вы не находитесь в банке");
                return;
            }

            //if (WorldPlayer.IsDead)
            //{
            //    await InPlayer.SendChatMessage("Вы при смерти");
            //    return;
            //}
            //
            //await InPlayer.ShowBankClientMenu(nearProperty.EntityId);
        }
    }
}