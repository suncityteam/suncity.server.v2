﻿using Microsoft.Extensions.DependencyInjection;
using SunCity.Common.Commands;
using SunCity.Common.MediatR.Extensions;
using SunCity.Game.World.Commands.Common.Validators.Health;
using SunCity.Game.World.Commands.Common.Validators.Property;
using SunCity.Game.World.Commands.Common.Validators.Property.Category.Business;
using SunCity.Game.World.Commands.Property.Category.Business.Bank.Client.ShowBankClientMenu;

namespace SunCity.Game.World.Commands.Property.Category.Business.Bank
{
    internal static class ServiceRecorder
    {
        internal static void AddBankBusinessCommands(this IServiceCollection InServiceCollection)
        {
            InServiceCollection.AddPipelineBehavior<ShowBankClientMenuRequest, WhenPlayerAlive<ShowBankClientMenuRequest>>();
            InServiceCollection.AddPipelineBehavior<ShowBankClientMenuRequest, WhenPlayerInProperty<ShowBankClientMenuRequest, BankCategory>>();
            InServiceCollection.AddRageMpCommandHandler<ShowBankClientMenuRequest, ShowBankClientMenuHandler>();
        }
    }
}