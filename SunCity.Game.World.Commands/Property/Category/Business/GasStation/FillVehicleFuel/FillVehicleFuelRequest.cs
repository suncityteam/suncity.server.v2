﻿using SunCity.Common.Commands.Interfaces;
using SunCity.Game.Enums.Vehicle;
using SunCity.Game.World.Commands.Common;

namespace SunCity.Game.World.Commands.Property.Category.Business.GasStation.FillVehicleFuel
{
    public sealed class FillVehicleFuelRequest 
        : IRageMpCommandRequest
    {
        public VehicleFuelType FuelType { get; set; }
        public int Amount { get; set; }
    }
}
