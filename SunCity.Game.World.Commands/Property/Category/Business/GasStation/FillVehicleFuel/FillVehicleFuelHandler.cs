﻿using System.Threading;
using System.Threading.Tasks;
using SunCity.Game.World.Property;
using SunCity.Common.Commands.Interfaces;
using SunCity.Common.Operation;
using SunCity.Game.World.Events.Client.Property.Business.GasStation;
using SunCity.Game.World.Player;

namespace SunCity.Game.World.Commands.Property.Category.Business.GasStation.FillVehicleFuel
{
    public class FillVehicleFuelHandler : RageMpCommandHandler<FillVehicleFuelRequest>
    {
        private WorldPlayer WorldPlayer { get; }
        private IWorldPropertyStorage PropertyStorage { get; }

        public FillVehicleFuelHandler(RageMpCommandConstructor<FillVehicleFuelRequest> constructor, IWorldPropertyStorage propertyStorage, WorldPlayer worldPlayer) : base(constructor)
        {
            PropertyStorage = propertyStorage;
            WorldPlayer = worldPlayer;
        }

        public override async Task<OperationResponse> Handle(FillVehicleFuelRequest request, CancellationToken cancellationToken)
        {
            var nearGasStationPoint = await PropertyStorage.FindNearGasStationPoint(RagePlayer);
            if (nearGasStationPoint == null)
            {
                RagePlayer.SendChatMessage("Вы не находитесь на заправке");
                return OperationResponseCache.Successfully;
            }

            RagePlayer.ShowGasStationClientMenu(nearGasStationPoint.Property.EntityId);
            return OperationResponseCache.Successfully;
        }
    }
}
