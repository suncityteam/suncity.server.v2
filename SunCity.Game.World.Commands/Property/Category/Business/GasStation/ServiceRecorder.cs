﻿using Microsoft.Extensions.DependencyInjection;
using SunCity.Common.Commands;
using SunCity.Common.MediatR.Extensions;
using SunCity.Game.World.Commands.Common.Validators.Health;
using SunCity.Game.World.Commands.Common.Validators.Vehicle;
using SunCity.Game.World.Commands.Property.Category.Business.GasStation.FillVehicleFuel;

namespace SunCity.Game.World.Commands.Property.Category.Business.GasStation
{
    internal static class ServiceRecorder
    {
        internal static void AddGasStationCommands(this IServiceCollection InServiceCollection)
        {
            InServiceCollection.AddPipelineBehavior<FillVehicleFuelRequest, WhenPlayerAlive<FillVehicleFuelRequest>>();
            InServiceCollection.AddPipelineBehavior<FillVehicleFuelRequest, WhenPlayerIsVehicleDriver<FillVehicleFuelRequest>>();
            InServiceCollection.AddRageMpCommandHandler<FillVehicleFuelRequest, FillVehicleFuelHandler>();
        }
    }
}