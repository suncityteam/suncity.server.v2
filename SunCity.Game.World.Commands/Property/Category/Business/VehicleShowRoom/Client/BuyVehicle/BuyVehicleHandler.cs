﻿using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using SunCity.Common.Commands.Interfaces;
using SunCity.Common.Operation;
using SunCity.Game.Enums.World.Interior;
using SunCity.Game.World.Events.Client.Property.Business.VehicleShowRoom;
using SunCity.Game.World.Models.Property.Business.VehicleShowRoom;
using SunCity.Game.World.Player;
using SunCity.Game.World.Property;
using SunCity.Game.World.Property.Category.Business.CarShowroom;

namespace SunCity.Game.World.Commands.Property.Category.Business.VehicleShowRoom.Client.BuyVehicle
{
    public class BuyVehicleHandler : RageMpCommandHandler<BuyVehicleRequest>
    {
        private IWorldPropertyStorage PropertyStorage { get; }
        private WorldPlayer WorldPlayer { get; }

        public BuyVehicleHandler(RageMpCommandConstructor<BuyVehicleRequest> constructor, IWorldPropertyStorage propertyStorage, WorldPlayer worldPlayer) : base(constructor)
        {
            PropertyStorage = propertyStorage;
            WorldPlayer = worldPlayer;
        }

        public override async Task<OperationResponse> Handle(BuyVehicleRequest request, CancellationToken cancellationToken)
        {
            var showRoom = (CarShowroomWorldProperty)WorldPlayer.EnteredProperty;
            if (showRoom == null)
            {
                RagePlayer.SendChatMessage("Вы не в автосалоне");
                return OperationResponseCache.Successfully;
            }

            RagePlayer.ShowVehicleShowRoomClientMenu(new VehicleShowRoomRageModel
            {
                EntityId = showRoom.EntityId.Id,
                StandPosition = showRoom.Interior.Interior.Actions.First(q => q.Action == InteriorActionType.StandPoint).Position,
                StandCamera = showRoom.Interior.Interior.Actions.First(q => q.Action == InteriorActionType.StandCamera).Position,
                StandRotation = showRoom.Interior.Interior.Actions.First(q => q.Action == InteriorActionType.StandRotation).Position,
            });

            return OperationResponseCache.Successfully;
        }

    }
}