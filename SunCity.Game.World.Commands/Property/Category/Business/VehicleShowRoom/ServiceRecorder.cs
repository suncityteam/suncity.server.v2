﻿using Microsoft.Extensions.DependencyInjection;
using SunCity.Common.Commands;
using SunCity.Game.World.Commands.Property.Category.Business.VehicleShowRoom.Client.BuyVehicle;

namespace SunCity.Game.World.Commands.Property.Category.Business.VehicleShowRoom
{
    internal static class ServiceRecorder
    {
        internal static void AddVehicleShowRoomCommands(this IServiceCollection InServiceCollection)
        {
            InServiceCollection.AddRageMpCommandHandler<BuyVehicleRequest, BuyVehicleHandler>();
        }
    }
}