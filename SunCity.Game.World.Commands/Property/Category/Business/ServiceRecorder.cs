﻿using Microsoft.Extensions.DependencyInjection;
using SunCity.Game.World.Commands.Property.Category.Business.Bank;
using SunCity.Game.World.Commands.Property.Category.Business.GasStation;
using SunCity.Game.World.Commands.Property.Category.Business.VehicleShowRoom;

namespace SunCity.Game.World.Commands.Property.Category.Business
{
    internal static class ServiceRecorder
    {
        internal static void AddBusinessCommands(this IServiceCollection InServiceCollection)
        {
            InServiceCollection.AddBankBusinessCommands();
            InServiceCollection.AddVehicleShowRoomCommands();
            InServiceCollection.AddGasStationCommands();
        }
    }
}