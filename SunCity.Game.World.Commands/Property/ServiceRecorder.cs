﻿using Microsoft.Extensions.DependencyInjection;
using SunCity.Game.World.Commands.Property.Category.Business;
using SunCity.Game.World.Commands.Property.Controls;

namespace SunCity.Game.World.Commands.Property
{
    internal static class ServiceRecorder
    {
        internal static void AddPropertyCommands(this IServiceCollection InServiceCollection)
        {
            InServiceCollection.AddBusinessCommands();
            InServiceCollection.AddPropertyControlsCommands();
        }
    }
}