﻿using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.DependencyInjection;
using SunCity.Common.Commands.Interfaces;
using SunCity.Game.World.Commands.Admin;
using SunCity.Game.World.Commands.Common;
using SunCity.Game.World.Commands.Common.Validators.Property.Category.Business;
using SunCity.Game.World.Commands.Property;

namespace SunCity.Game.World.Commands
{
    public static class ServiceRecorder
    {
        public static void AddGameWorldCommands(this IServiceCollection InServiceCollection)
        {
            InServiceCollection.AddScoped<IRageMpCommandMiddleware, RageMpCommandsPlayerResolver>();

            InServiceCollection.AddSingleton<BankCategory>();
            InServiceCollection.AddSingleton<GasStationCategory>();
            InServiceCollection.AddSingleton<VehicleShowRoomCategory>();

            InServiceCollection.AddAdminCommands();
            InServiceCollection.AddPropertyCommands();
        }
    }
}