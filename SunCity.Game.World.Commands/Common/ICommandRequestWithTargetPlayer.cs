﻿namespace SunCity.Game.World.Commands.Common
{
    public interface ICommandRequestWithTargetPlayer
    {
        ushort TargetPlayerId { get; set; }
    }
}