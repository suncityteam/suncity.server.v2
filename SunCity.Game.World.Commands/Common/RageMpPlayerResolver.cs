﻿using SunCity.Common.Commands.Interfaces;
using SunCity.Common.Operation;
using SunCity.Common.RageMp.Pools.Players;
using SunCity.Game.Identity.Services.Sessions;
using SunCity.Game.Rage.Connections;

namespace SunCity.Game.World.Commands.Common
{
    internal class RageMpCommandsPlayerResolver : IRageMpCommandMiddleware
    {
        private IGameSessionManager GameSessionManager { get; }
        private IRageConnectionStorage RageConnectionStorage { get; }

        public RageMpCommandsPlayerResolver(IRageConnectionStorage rageConnectionStorage, IGameSessionManager gameSessionManager)
        {
            RageConnectionStorage = rageConnectionStorage;
            GameSessionManager = gameSessionManager;
        }

        public OperationResponse Prepare(IRagePlayer player)
        {
            var ensureResponse = RageConnectionStorage.Ensure(player);
            if (ensureResponse.IsNotCorrect)
                return ensureResponse.Error;

            if (RageConnectionStorage.WorldPlayer != null)
            {
                var processGameSession = GameSessionManager.ProcessGameSession(RageConnectionStorage.WorldPlayer.Account.EntityId);
                if (processGameSession.IsNotCorrect)
                    return processGameSession.Error;
            }

            return OperationResponseCache.Successfully;
        }
    }
}