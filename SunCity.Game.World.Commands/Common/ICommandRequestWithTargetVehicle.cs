﻿namespace SunCity.Game.World.Commands.Common
{
    public interface ICommandRequestWithTargetVehicle
    {
        ushort TargetVehicleId { get; set; }
    }
    
    public interface ICommandRequestWithTargetVehicleOptional
    {
        ushort? TargetVehicleId { get; set; }
    }
}