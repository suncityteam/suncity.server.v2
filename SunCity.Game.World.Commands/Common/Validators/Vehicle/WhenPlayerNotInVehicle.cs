﻿using System.Threading;
using System.Threading.Tasks;
using MediatR;
using SunCity.Common.Commands.Interfaces;
using SunCity.Common.MediatR;
using SunCity.Common.Operation;
using SunCity.Game.World.Player;

namespace SunCity.Game.World.Commands.Common.Validators.Vehicle
{
    public class WhenPlayerNotInVehicle<TRequest> : IOperationPipelineBehavior<TRequest> where TRequest : IRageMpCommandRequest
    {
        private WorldPlayer WorldPlayer { get; }

        public WhenPlayerNotInVehicle(WorldPlayer worldPlayer)
        {
            WorldPlayer = worldPlayer;
        }

        public Task<OperationResponse> Handle(TRequest request, CancellationToken cancellationToken, RequestHandlerDelegate<OperationResponse> next)
        {
            if (WorldPlayer.RagePlayer.Vehicle != null)
            {
                WorldPlayer.SendClientMessage("Вы должны находиться не в машине");
                return TaskOperationResponseCache.Successfully;
            }

            return next();
        }
    }
}
