﻿using System.Threading;
using System.Threading.Tasks;
using MediatR;
using SunCity.Common.Commands.Interfaces;
using SunCity.Common.MediatR;
using SunCity.Common.Operation;
using SunCity.Game.World.Player;

namespace SunCity.Game.World.Commands.Common.Validators.Vehicle
{
    public class WhenPlayerIsNotVehicleDriver<TRequest> : IOperationPipelineBehavior<TRequest> where TRequest : IRageMpCommandRequest
    {
        private WorldPlayer WorldPlayer { get; }

        public WhenPlayerIsNotVehicleDriver(WorldPlayer worldPlayer)
        {
            WorldPlayer = worldPlayer;
        }

        public Task<OperationResponse> Handle(TRequest request, CancellationToken cancellationToken, RequestHandlerDelegate<OperationResponse> next)
        {
            if (WorldPlayer.RagePlayer.Vehicle != null)
            {
                WorldPlayer.SendClientMessage("Вы должны быть пассажиром");
                return TaskOperationResponseCache.Successfully;
            }

            return next();
        }
    }
}
