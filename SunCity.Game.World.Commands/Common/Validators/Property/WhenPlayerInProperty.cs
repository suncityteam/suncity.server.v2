﻿using System.Threading;
using System.Threading.Tasks;
using MediatR;
using SunCity.Common.Commands.Interfaces;
using SunCity.Common.MediatR;
using SunCity.Common.Operation;
using SunCity.Game.World.Commands.Common.Validators.Property.Category;
using SunCity.Game.World.Player;

namespace SunCity.Game.World.Commands.Common.Validators.Property
{
    public class WhenPlayerInProperty<TRequest, TCategory> : IOperationPipelineBehavior<TRequest>
        where TRequest : IRageMpCommandRequest
        where TCategory : IPropertyCategory
    {
        private TCategory Category { get; }
        private WorldPlayer WorldPlayer { get; }

        public WhenPlayerInProperty(WorldPlayer worldPlayer, TCategory category)
        {
            WorldPlayer = worldPlayer;
            Category = category;
        }

        public Task<OperationResponse> Handle(TRequest request, CancellationToken cancellationToken, RequestHandlerDelegate<OperationResponse> next)
        {
            if (WorldPlayer.EnteredProperty == null)
            {
                WorldPlayer.SendClientMessage(Category.Message);
                return TaskOperationResponseCache.Successfully;
            }

            return next();
        }
    }
}