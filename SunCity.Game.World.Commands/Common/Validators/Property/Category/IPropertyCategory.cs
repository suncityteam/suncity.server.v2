﻿namespace SunCity.Game.World.Commands.Common.Validators.Property.Category
{
    public interface IPropertyCategory
    {
        string Message { get; }
    }
}
