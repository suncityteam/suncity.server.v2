﻿namespace SunCity.Game.World.Commands.Common.Validators.Property.Category.Business
{
    public sealed class GasStationCategory : IPropertyCategory
    {
        public string Message { get; } = "Вы не на АЗС";
    }
}