﻿namespace SunCity.Game.World.Commands.Common.Validators.Property.Category.Business
{
    public sealed class BankCategory : IPropertyCategory
    {
        public string Message { get; } = "Вы не в банке";
    }
}