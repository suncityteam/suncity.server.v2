﻿namespace SunCity.Game.World.Commands.Common.Validators.Property.Category.Business
{
    public sealed class VehicleShowRoomCategory : IPropertyCategory
    {
        public string Message { get; } = "Вы не в автосалоне";
    }
}