﻿using System.Threading;
using System.Threading.Tasks;
using MediatR;
using SunCity.Common.Commands.Interfaces;
using SunCity.Common.MediatR;
using SunCity.Common.Operation;
using SunCity.Game.World.Player;

namespace SunCity.Game.World.Commands.Common.Validators.Health
{
    public class WhenPlayerAlive<TRequest> : IOperationPipelineBehavior<TRequest> where TRequest : IRageMpCommandRequest
    {
        private WorldPlayer WorldPlayer { get; }

        public WhenPlayerAlive(WorldPlayer worldPlayer)
        {
            WorldPlayer = worldPlayer;
        }

        public Task<OperationResponse> Handle(TRequest request, CancellationToken cancellationToken, RequestHandlerDelegate<OperationResponse> next)
        {
            if (WorldPlayer.IsDead)
            {
                WorldPlayer.SendClientMessage("Вы мертвы");
                return TaskOperationResponseCache.Successfully;
            }

            return next();
        }
    }
}
