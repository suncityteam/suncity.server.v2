﻿using System;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using SunCity.Common.MediatR.Extensions;
using SunCity.Game.World.Property.Business.VehicleShowRoom.Operations.GetCatalog;
using SunCity.Game.World.Property.Business.VehicleShowRoom.Operations.Purchase;

namespace SunCity.Game.World.Property.Business.VehicleShowRoom.Operations
{
    public static class ServiceRecorder
    {
        public static IServiceCollection AddVehicleShowRoomOperationHandlers(this IServiceCollection InServiceCollection, IConfiguration InConfiguration)
        {
            InServiceCollection.AddGetVehiclesShowRoomCatalogRequest(InConfiguration);
            InServiceCollection.AddPurchaseVehicleInVehicleShowRoomRequest(InConfiguration);
            return InServiceCollection;
        }
        
        private static void AddGetVehiclesShowRoomCatalogRequest(this IServiceCollection InServiceCollection, IConfiguration InConfiguration)
        {
            InServiceCollection.AddPipelineBehavior<GetVehiclesShowRoomCatalogRequest, GetVehiclesShowRoomCatalogResponse, CanGetVehiclesShowRoomCatalogBehavior>();
            InServiceCollection.AddRequestHandler<GetVehiclesShowRoomCatalogRequest, GetVehiclesShowRoomCatalogResponse, GetVehiclesShowRoomCatalogHandler>();
        }

        private static void AddPurchaseVehicleInVehicleShowRoomRequest(this IServiceCollection InServiceCollection, IConfiguration InConfiguration)
        {
            InServiceCollection.AddPipelineBehavior<PurchaseVehicleInVehicleShowRoomRequest, PurchaseVehicleInVehicleShowRoomResponse, CanPurchaseVehicleInVehicleShowBehavior>();
            InServiceCollection.AddRequestHandler<PurchaseVehicleInVehicleShowRoomRequest, PurchaseVehicleInVehicleShowRoomResponse, PurchaseVehicleInVehicleShowRoomHandler>();
        }
    }
}