﻿using SunCity.Domain.Interfaces.Entity;
using SunCity.Game.Types.Cost;
using SunCity.Game.Types.Property.Entity.Property;
using SunCity.Game.World.Models.Vehicles;

namespace SunCity.Game.World.Property.Business.VehicleShowRoom.Operations.GetCatalog
{
    public class GetVehiclesShowRoomCatalogResponse
        : IEntityWithId<PropertyId>
        , IEntityWithName<PropertyName>
    {
        public PropertyId EntityId { get; set; }
        public PropertyName Name { get; set; }
        public GetVehiclesShowRoomCatalogItem[] Items { get; set; }
        
        public class GetVehiclesShowRoomCatalogItem
            : IEntityWithId<CarShowroomVehicleId>
        {
            public CarShowroomVehicleId EntityId { get; set; }
            public VehicleModelInformation Information { get; set; }
            public GameCost Cost { get; set; }
        }
    }
}