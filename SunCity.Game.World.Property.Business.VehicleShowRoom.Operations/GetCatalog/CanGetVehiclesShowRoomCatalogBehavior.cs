﻿using System.Threading;
using System.Threading.Tasks;
using MediatR;
using SunCity.Common.MediatR;
using SunCity.Common.Operation;
using SunCity.Game.World.Property.Category.Business.CarShowroom;

namespace SunCity.Game.World.Property.Business.VehicleShowRoom.Operations.GetCatalog
{
    internal class CanGetVehiclesShowRoomCatalogBehavior : IOperationPipelineBehavior<GetVehiclesShowRoomCatalogRequest, GetVehiclesShowRoomCatalogResponse>
    {
        private IWorldPropertyStorage PropertyStorage { get; }

        public CanGetVehiclesShowRoomCatalogBehavior(IWorldPropertyStorage propertyStorage)
        {
            PropertyStorage = propertyStorage;
        }

        public Task<OperationResponse<GetVehiclesShowRoomCatalogResponse>> Handle(GetVehiclesShowRoomCatalogRequest request, CancellationToken cancellationToken,
            RequestHandlerDelegate<OperationResponse<GetVehiclesShowRoomCatalogResponse>> next)
        {
            var property = PropertyStorage.GetById(request.PropertyId);
            if (property == null)
                return Task.FromResult<OperationResponse<GetVehiclesShowRoomCatalogResponse>>((OperationResponseCode.PropertyIdNotFound, request.PropertyId));

            if (property is CarShowroomWorldProperty)
            {
                return next();
            }

            return Task.FromResult<OperationResponse<GetVehiclesShowRoomCatalogResponse>>((OperationResponseCode.PropertyIdNotFound, request.PropertyId));
        }
    }
}