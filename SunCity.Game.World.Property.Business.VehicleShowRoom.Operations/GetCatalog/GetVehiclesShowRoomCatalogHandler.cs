﻿using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using GTANetworkAPI;
using SunCity.Common.Enum;
using SunCity.Common.MediatR;
using SunCity.Common.Operation;
using SunCity.Game.Enums.World.Property;
using SunCity.Game.World.Models.Vehicles;
using SunCity.Game.World.Property.Category.Business.CarShowroom;

namespace SunCity.Game.World.Property.Business.VehicleShowRoom.Operations.GetCatalog
{
    internal class GetVehiclesShowRoomCatalogHandler : IOperationHandler<GetVehiclesShowRoomCatalogRequest, GetVehiclesShowRoomCatalogResponse>
    {        
        private IWorldPropertyStorage PropertyStorage { get; }

        public GetVehiclesShowRoomCatalogHandler(IWorldPropertyStorage propertyStorage)
        {
            PropertyStorage = propertyStorage;
        }
        
        public async Task<OperationResponse<GetVehiclesShowRoomCatalogResponse>> Handle(GetVehiclesShowRoomCatalogRequest request, CancellationToken cancellationToken)
        {
            var property = PropertyStorage.GetById<CarShowroomWorldProperty>(request.PropertyId);

            var items = property.SellingVehicles.Select(q => new GetVehiclesShowRoomCatalogResponse.GetVehiclesShowRoomCatalogItem
            {
                Cost = q.Cost,
                EntityId = q.EntityId,

                Information = new VehicleModelInformation
                {
                    Model = new EnumDescription<VehicleHash>(q.Model.EntityId, q.Model.Name),

                    FuelType = q.Model.FuelType,
                    FuelTankSize = q.Model.FuelTankSize,
                    FuelConsumptionPerMinute = q.Model.FuelConsumptionPerMinute,

                    Category = q.Model.ModelClass,
                    Class = new EnumDescription<WorldPropertyClass>(WorldPropertyClass.None),
                    
                    Acceleration = q.Model.Acceleration,
                    Handleability = q.Model.Handleability,
                    
                    BagCapacity = q.Model.BagCapacity,
                    MaxSpeed = q.Model.MaxSpeed,
                    
                    NumberOfSeats = q.Model.NumberOfSeats
                }
            }).ToArray();

            return new GetVehiclesShowRoomCatalogResponse
            {
                EntityId = property.EntityId,
                Name = property.Name,
                Items = items
            };
        }
    }
}