﻿using SunCity.Common.MediatR;
using SunCity.Domain.Interfaces.Entity;

namespace SunCity.Game.World.Property.Business.VehicleShowRoom.Operations.GetCatalog
{
    public class GetVehiclesShowRoomCatalogRequest 
        : IEntityWithPropertyId
        , IOperationRequest<GetVehiclesShowRoomCatalogResponse>
    {
        public PropertyId PropertyId { get; set; }

        public override string ToString()
        {
            return $"PropertyId: {PropertyId}";
        }
    }
}