﻿using System.Threading;
using System.Threading.Tasks;
using MediatR;
using SunCity.Common.MediatR;
using SunCity.Common.Operation;
using SunCity.Domain.Interfaces.Extensions;
using SunCity.Game.Enums;
using SunCity.Game.Rage.Connections;
using SunCity.Game.World.Player;
using SunCity.Game.World.Property.Category.Business.CarShowroom;
using SunCity.Game.World.Services.Property.Category.Business.Bank.Service.Client.BankCard.Player;

namespace SunCity.Game.World.Property.Business.VehicleShowRoom.Operations.Purchase
{
    internal class CanPurchaseVehicleInVehicleShowBehavior : IOperationPipelineBehavior<PurchaseVehicleInVehicleShowRoomRequest, PurchaseVehicleInVehicleShowRoomResponse>
    {
        private IWorldPropertyStorage PropertyStorage { get; }
        private IBankCardPlayerService BankCardService { get; }
        private WorldPlayer WorldPlayer { get; }
        
        public CanPurchaseVehicleInVehicleShowBehavior
            (
            IWorldPropertyStorage propertyStorage, 
            IBankCardPlayerService bankCardService,
            IWorldPlayerStorage worldPlayerStorage,
            IRageConnectionStorage rageConnectionStorage
        )
        {
            PropertyStorage = propertyStorage;
            BankCardService = bankCardService;
            worldPlayerStorage.TryGetPlayer(rageConnectionStorage.RagePlayer, out var worldPlayer);
            WorldPlayer = worldPlayer;
        }

        public async Task<OperationResponse<PurchaseVehicleInVehicleShowRoomResponse>> Handle(PurchaseVehicleInVehicleShowRoomRequest request, CancellationToken cancellationToken,
            RequestHandlerDelegate<OperationResponse<PurchaseVehicleInVehicleShowRoomResponse>> next)
        {
            var property = PropertyStorage.GetById<CarShowroomWorldProperty>(request.PropertyId);
            if (property == null)
                return (OperationResponseCode.PropertyIdNotFound, request.PropertyId);

            if (!property.OwnerBankCardId.HasValue)
                return (OperationResponseCode.PropertyRequisitesNotFilled, request.PropertyId);
            
            var sellingVehicle = property.SellingVehicles.GetById(request.ItemId);
            if (sellingVehicle == null)
                return (OperationResponseCode.ItemNotFound, request.ItemId);

            if (sellingVehicle.Amount < 1)
                return (OperationResponseCode.ItemNotNotEnough, request.ItemId);

            if (sellingVehicle.Cost.Currency != GameCurrency.Money) // TODO: Fix error code
                return (OperationResponseCode.PropertySellingOnlyWithMoney, request.ItemId);

            var payerAuthorizationResult = await BankCardService.GetBalance(request.Payment);
            if (payerAuthorizationResult.IsNotCorrect)
                return payerAuthorizationResult.Error;

            if (payerAuthorizationResult.Content.Balance < property.SellingCost.Amount)
                return (OperationResponseCode.NotEnoughMoney);

            var payeeIsValid = await BankCardService.Validate(property.OwnerBankCardId.Value);
            if (payeeIsValid.IsNotCorrect)
                return payeeIsValid.Error;
            
            return await next();
        }
    }
}