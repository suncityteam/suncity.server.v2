﻿using System.Linq;

using System.Threading;
using System.Threading.Tasks;
using SunCity.Common.MediatR;
using SunCity.Common.Operation;
using SunCity.Domain.Interfaces.Extensions;
using SunCity.Game.Rage.Connections;
using SunCity.Game.World.Events.Client.Property.Business.VehicleShowRoom;
using SunCity.Game.World.Item.Factory;
using SunCity.Game.World.Player;
using SunCity.Game.World.Property.Category.Business.CarShowroom;
using SunCity.Game.World.Services.Property.Category.Business.Bank.Forms.Payments;
using SunCity.Game.World.Services.Property.Category.Business.Bank.Service.Client.Payments;
using SunCity.Game.World.Services.Vehicle.Models;
using SunCity.Game.World.Services.Vehicle.Services;
using SunCity.Game.World.Vehicle;

namespace SunCity.Game.World.Property.Business.VehicleShowRoom.Operations.Purchase
{
    internal class PurchaseVehicleInVehicleShowRoomHandler : IOperationHandler<PurchaseVehicleInVehicleShowRoomRequest, PurchaseVehicleInVehicleShowRoomResponse>
    {
        private IWorldPropertyStorage PropertyStorage { get; }
        private IBankCardPropertyPaymentsService PropertyPaymentsService { get; }
        private IWorldVehicleServices WorldVehicleServices { get; }
        private IWorldItemFactory WorldItemFactory { get; }
        private IWorldVehicleStorage VehicleStorage { get; }
        private WorldPlayer WorldPlayer { get; }
        
        public PurchaseVehicleInVehicleShowRoomHandler
        (
            IWorldPropertyStorage propertyStorage,
            IBankCardPropertyPaymentsService propertyPaymentsService,
            IWorldVehicleServices worldVehicleServices, 
            IWorldItemFactory worldItemFactory, 
            IWorldVehicleStorage vehicleStorage,
            IWorldPlayerStorage worldPlayerStorage,
            IRageConnectionStorage rageConnectionStorage
        )
        {
            PropertyStorage = propertyStorage;
            PropertyPaymentsService = propertyPaymentsService;
            WorldVehicleServices = worldVehicleServices;
            WorldItemFactory = worldItemFactory;
            VehicleStorage = vehicleStorage;

            worldPlayerStorage.TryGetPlayer(rageConnectionStorage.RagePlayer, out var worldPlayer);
            WorldPlayer = worldPlayer;
        }

        public async Task<OperationResponse<PurchaseVehicleInVehicleShowRoomResponse>> Handle(PurchaseVehicleInVehicleShowRoomRequest request, CancellationToken cancellationToken)
        {
            var property = PropertyStorage.GetById<CarShowroomWorldProperty>(request.PropertyId);
            var sellingVehicle = property.SellingVehicles.GetById(request.ItemId);
            var spawnPosition = property.SpawnPoints.First();

            var vehicleEntity = await WorldVehicleServices.CreateVehicle(new CreateWorldVehicleModel
            {
                VehicleId = VehicleId.GenerateId(),
                Model = sellingVehicle.Model.EntityId,

                FirstColor = request.FirstColor,
                SecondColor = request.SecondColor,

                Position = spawnPosition.Position,
                Rotation = spawnPosition.Rotation,
            });

            var paymentResult = await PropertyPaymentsService.VehicleBuy(new BankVehicleBuyDbForm()
            {
                PropertyId = property.EntityId,
                PayerId = request.Payment.PayerBankCardId,
                PayeeId = property.OwnerBankCardId!.Value,
                Cost = sellingVehicle.Amount,
                VehicleId = vehicleEntity.EntityId
            });

            if (paymentResult.IsNotCorrect)
                return paymentResult.Error;

            var vehicle = VehicleStorage.AddVehicle(vehicleEntity);
            
            WorldPlayer.SendClientMessage(new []
            {
                $"Поздравляем с приобретением {sellingVehicle.Model.Name}!",
                "Не забудте приобрести страховку на машину в близжайшем страховом агенстве."
            });

            for (var i = 0; i < 3; i++)
            {
                var propertyKey = WorldItemFactory.CreateVehicleKey(vehicleEntity.EntityId);
                WorldPlayer.Inventory.PutItem(propertyKey);
            }

            WorldPlayer.RagePlayer.HideVehicleShowRoomClientMenu();
            return new PurchaseVehicleInVehicleShowRoomResponse();
        }
    }
}