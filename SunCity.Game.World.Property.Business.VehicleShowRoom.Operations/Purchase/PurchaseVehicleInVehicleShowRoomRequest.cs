﻿using SunCity.Common.MediatR;
using SunCity.Game.World.Forms.Payment;
using SunCity.Game.World.Forms.Property.Business.Bank;
using SunCity.Game.World.Player;

namespace SunCity.Game.World.Property.Business.VehicleShowRoom.Operations.Purchase
{
    public class PurchaseVehicleInVehicleShowRoomRequest 
        : IEntityWithPropertyId
        , IOperationRequest<PurchaseVehicleInVehicleShowRoomResponse>
    {
        public BankAuthorizationForm Payment { get; set; }
        public PropertyId PropertyId { get; set; }
        public CarShowroomVehicleId ItemId { get; set; }

        public int FirstColor { get; set; }
        public int SecondColor { get; set; }
        
        public override string ToString()
        {
            return $"PropertyId: {PropertyId}";
        }
    }
}