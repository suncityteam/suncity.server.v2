﻿using SunCity.Common.Enum;

namespace SunCity.Game.Enums.World.Property
{
    public enum WorldPropertyClass : byte
    {
        [EnumDescription("Неизвестно")]
        None,
        
        /// <summary>
        /// Трейлер
        /// </summary>
        [EnumDescription("Трейлер")]
        Trailer,

        /// <summary>
        /// Эконом
        /// </summary>
        [EnumDescription("Эконом")]
        Economy,

        /// <summary>
        /// Эконом
        /// </summary>
        [EnumDescription("Эконом+")]
        EconomyPlus,
        
        /// <summary>
        /// Комфорт
        /// </summary>
        [EnumDescription("Комфорт")]
        Comfort,

        /// <summary>
        /// Комфорт+
        /// </summary>
        [EnumDescription("Комфорт+")]
        ComfortPlus,

        /// <summary>
        /// Бизнес класс
        /// </summary>
        [EnumDescription("Бизнес класс")]
        Business,

        /// <summary>
        /// Элитная недвижимость
        /// </summary>
        [EnumDescription("Элитн")]
        Elite
    }
}