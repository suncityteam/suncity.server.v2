﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SunCity.Game.Enums.World.Property
{
    public enum WorldPropertyEntrance : byte
    {
        NoInterior,
        Enter,
        Exit,
    }
}
