﻿namespace SunCity.Game.Enums.World.Property.Owner
{
    public enum PropertyOwnerGroup
    {
        Player,
        Fraction
    }
}