﻿
namespace SunCity.Game.Enums.World.Property.Owner
{
    public enum PropertyOwnerType
    {
        Owner,
        Renter
    }
}