﻿using SunCity.Common.Enum;

namespace SunCity.Game.Enums.World.Property
{
    public enum WorldPropertyCategory : byte
    {
        [EnumDescription("Неизвестно")]
        None,

        //========================
        #region Жилая недвижимость

        /// <summary>
        /// Частный дом. Может быть с гаражем
        /// </summary>
        [EnumDescription("Частный дом")]
        House,

        /// <summary>
        /// Гараж
        /// </summary>
        [EnumDescription("Гараж")]
        Garage,

        /// <summary>
        /// Комната в отеле
        /// </summary>
        [EnumDescription("Комната в отеле")]
        HotelRoom,

        /// <summary>
        /// Квартира в доме
        /// </summary>
        [EnumDescription("Квартира")]
        Apartment,
        #endregion


        //========================
        #region Бизнессы

        /// <summary>
        /// Магазин
        /// </summary>
        [EnumDescription("Магазин")]
        Store,

        /// <summary>
        /// Офис
        /// </summary>
        [EnumDescription("Офис")]
        Office,

        /// <summary>
        /// Склад
        /// </summary>
        [EnumDescription("Склад")]
        WareHouse,

        /// <summary>
        /// Клуб
        /// </summary>
        [EnumDescription("Клуб")]
        Club,

        /// <summary>
        /// Ресторан
        /// </summary>
        [EnumDescription("Ресторан")]
        Restaurant,

        /// <summary>
        /// Бар
        /// </summary>
        [EnumDescription("Бар")]
        Bar,

        /// <summary>
        /// Казино
        /// </summary>
        [EnumDescription("Казино")]
        Casino,

        /// <summary>
        /// Банк
        /// </summary>
        [EnumDescription("Банк")]
        Bank,

        //------------------------
        #region Добыча ресурсов
        /// <summary>
        /// Ферма
        /// </summary>
        [EnumDescription("Ферма")]
        Farm,

        /// <summary>
        /// Шахта
        /// </summary>
        [EnumDescription("Шахта")]
        Miner,

        /// <summary>
        /// Лесопилка
        /// </summary>
        [EnumDescription("Лесопилка")]
        Sawmill,
        #endregion

        //------------------------  
        #region Автосервисы
        /// <summary>
        /// Автосервис
        /// </summary>
        [EnumDescription("Автосервис")]
        CarService,

        /// <summary>
        /// Автосалон
        /// </summary>
        [EnumDescription("Автосалон")]
        CarShowroom,

        /// <summary>
        /// Магазин мотоциклов
        /// </summary>
        [EnumDescription("Магазин мотоциклов")]
        MotoShowroom,

        /// <summary>
        /// Магазин лодок
        /// </summary>
        [EnumDescription("Магазин лодок")]
        BoatShowroom,

        /// <summary>
        /// Магазин лодок
        /// </summary>
        [EnumDescription("Магазин воздушного транспорта")]
        AirShowroom,

        /// <summary>
        /// Заправка
        /// </summary>
        [EnumDescription("Заправка")]
        GasStation,
        #endregion


        //------------------------
        [EnumDescription("Больница")]
        Hospital,

        //------------------------
        #region Комания

        /// <summary>
        /// Телефонная компания
        /// </summary>
        TelephoneCompany,

        /// <summary>
        /// Электрическая компания
        /// </summary>
        EnergyCompany,

        /// <summary>
        /// Страховая компания
        /// </summary>
        InsuranceCompany,

        /// <summary>
        /// Агентство недвижимости
        /// </summary>
        RealEstateAgency,


        /// <summary>
        /// Адвокатское бюро
        /// </summary>
        LawCompany,
        #endregion

        /// <summary>
        /// Аренда авто
        /// </summary>
        CarSharing,

        /// <summary>
        /// Авторынок
        /// </summary>
        AutoMarket,

        /// <summary>
        /// Нефтеперерабатывающий завод
        /// </summary>
        OilRefinery,

        /// <summary>
        /// Нефтяная вышка
        /// </summary>
        OilDerrick,

        #endregion


        [EnumDescription("Штаб квартира")]
        HeadQuarter,

        [EnumDescription("Мусорная свалка")]
        Trash,

        //------------------------
        [EnumDescription("Автошкола")]
        DrivingSchool,
        
        [EnumDescription("Летная школа")]
        AirSchool,
        
        [EnumDescription("Водная школа")]
        WaterSchool,
        
        [EnumDescription("АТП")]
        BusStation,
        
        [EnumDescription("Уличные гонки")]
        StreetRacing,

        [EnumDescription("Гонки по бездорожью")]
        OffRoadRacing,
        
        [EnumDescription("Гонки на катерах")]
        BoatRacing,
        
        [EnumDescription("Гонки на самолетах")]
        AirRacing,
        
        [EnumDescription("Гонки на велосипедах")]
        BicycleRacing,
        
        [EnumDescription("Гонки на мотоциклах")]
        MotorcycleRacing,
    }

}