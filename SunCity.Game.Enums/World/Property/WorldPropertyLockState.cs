﻿namespace SunCity.Game.Enums.World.Property
{
    public enum WorldPropertyLockState : byte
    {
        NoInterior,
        Opened,
        Locked,
    }
}