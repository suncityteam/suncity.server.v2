﻿namespace SunCity.Game.Enums.World.Property
{
    public enum WorldPropertySellState : byte
    {
        None,
        Selling,
    }
}