﻿using System;
using System.Collections.Generic;
using System.Text;
using SunCity.Common.Enum;

namespace SunCity.Game.Enums.World.Interior
{
    public enum InteriorActionType
    {
        [EnumDescription("Неизвестно")]
        None = 0,

        /// <summary>
        /// Ресепшн
        /// </summary>
        [EnumDescription("Ресепшн")]
        Reception = 1,

        /// <summary>
        /// Сейф
        /// </summary>
        [EnumDescription("Сейф")]
        Safe = 2,

        /// <summary>
        /// Черный выход
        /// </summary>
        [EnumDescription("Черный выход")]
        BlackExit = 3,

        /// <summary>
        /// Гараж
        /// </summary>
        [EnumDescription("Гараж")]
        Garage = 4,

        /// <summary>
        /// Склад
        /// </summary>
        [EnumDescription("Склад")]
        WareHouse = 5,

        /// <summary>
        /// Крыша
        /// </summary>
        [EnumDescription("Крыша")]
        Roof = 6,

        /// <summary>
        /// Чердак
        /// </summary>
        [EnumDescription("Чердак")]
        Attic = 7,

        /// <summary>
        /// Подвал
        /// </summary>
        [EnumDescription("Подвал")]
        Basement = 8,
        
        
        /// <summary>
        /// <para>Ппьедестал / стенд / подставка</para>
        /// <para>Например, для машины, оружия</para>
        /// </summary>
        [EnumDescription("Пьедестал, позиция")]
        StandPoint = 9,
        
        /// <summary>
        /// <para>Камера для пьедестала / стенда / подставки</para>
        /// <para>Например, для машины, оружия</para>
        /// </summary>
        [EnumDescription("Пьедестал, камера")]    
        StandCamera = 10,
        
        /// <summary>
        /// <para>Ппьедестал / стенд / подставка</para>
        /// <para>Например, для машины, оружия</para>
        /// </summary>
        [EnumDescription("Пьедестал, угол")]    
        StandRotation = 11,
    }
}
