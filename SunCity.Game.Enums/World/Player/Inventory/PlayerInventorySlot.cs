﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SunCity.Game.Enums.World.Player.Inventory
{
    public enum PlayerInventorySlot : byte
    {
        None = 0,

        /// <summary>
        /// Очки
        /// </summary>
        Glasses = 1,

        /// <summary>
        /// Шляпы
        /// </summary>
        Hats = 2,

        /// <summary>
        /// Серьги
        /// </summary>
        Еarrings = 3,

        /// <summary>
        /// Руки
        /// </summary>
        Arms = 4,

        /// <summary>
        /// верхняя одежда
        /// </summary>
        Outerwear = 5,

        /// <summary>
        /// штаны
        /// </summary>
        Pants = 6,

        /// <summary>
        /// обувь
        /// </summary>
        Footwear = 7,

        /// <summary>
        /// Сумка
        /// </summary>
        Bag = 8,

        LeftHand = 9,

        RightHand = 10,

    }
}
