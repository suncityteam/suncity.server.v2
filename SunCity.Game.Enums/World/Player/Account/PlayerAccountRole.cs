﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SunCity.Game.Enums.World.Player.Account
{
    public enum PlayerAccountRole : byte
    {
        None,
        Helper,
        Moderator,
        Administrator,
        SuperAdministrator
    }
}
