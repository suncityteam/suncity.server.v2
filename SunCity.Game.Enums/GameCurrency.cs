﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SunCity.Game.Enums
{
    /// <summary>
    /// Типы внутриигровой валюты
    /// </summary>
    public enum GameCurrency : byte
    {
        None,

        /// <summary>
        /// Игровые деньги
        /// </summary>
        Money,

        /// <summary>
        /// Донатские монеты
        /// </summary>    
        Donate
    }
}
