﻿using System.Threading.Tasks;

namespace SunCity.Game.Enums.Fraction
{
    public enum FractionCategory : byte
    {
        /// <summary>
        /// Гражданский
        /// </summary>
        Civic,

        /// <summary>
        /// Криминал
        /// </summary>
        Crime,

        /// <summary>
        /// Правительство
        /// </summary>
        Government
    }
}