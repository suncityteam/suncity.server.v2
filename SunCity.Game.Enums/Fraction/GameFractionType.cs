﻿using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Text;

namespace SunCity.Game.Enums.Fraction
{
    [SuppressMessage("ReSharper", "InconsistentNaming")]
    [SuppressMessage("ReSharper", "IdentifierTypo")]
    public enum FractionType
    {
        None,

        //============================
        //  Government
        FBI,
        Army,

        Police,
        Emergency,
        Government,

        //============================
        //  Crime

        /// <summary>
        /// Банда
        /// </summary>
        Gang,

        /// <summary>
        /// Мафия
        /// </summary>
        Mafia
    }
}
