﻿using System;

namespace SunCity.Game.Enums.Fraction
{
    [Flags]
    public enum GameFractionRankAccess : int
    {
        None,

        /// <summary>
        /// Принимать
        /// </summary>
        Invite = 1,

        /// <summary>
        /// Исключать
        /// </summary>
        UnInvite = 1 << 1,

        /// <summary>
        /// Выдавать ранг
        /// </summary>
        GiveRank = 1 << 2,

        /// <summary>
        /// Изменять ранг
        /// </summary>
        AddRank = 1 << 3,

        /// <summary>
        /// Изменять ранг
        /// </summary>
        EditRank = 1 << 4,

        /// <summary>
        /// Изменять ранг
        /// </summary>
        RemoveRank = 1 << 5,

        /// <summary>
        /// Менять лидера фракции
        /// </summary>
        ChangeLeader = 1 << 6,

        /// <summary>
        /// Давать предупреждения
        /// </summary>
        GiveWarn = 1 << 7,

        /// <summary>
        /// Использовать чат
        /// </summary>
        UseChat = 1 << 8,

        /// <summary>
        /// Использовать фракционный транспорт
        /// </summary>
        UseVehicles = 1 << 9,

        /// <summary>
        /// Использовать склад
        /// </summary>
        UseWareHouse = 1 << 10,

        /// <summary>
        /// Покупка недвижимости
        /// </summary>
        BuyProperty = 1 << 11,

        /// <summary>
        /// Продажа недвижимости
        /// </summary>
        SellProperty = 1 << 12,

        /// <summary>
        /// Управление банковскими счетами
        /// </summary>
        BankAccountManage = 1 << 13,

        /// <summary>
        /// Просмотр истории операций по счетам
        /// </summary>
        BankAccountAnalytics = 1 << 14,

        /// <summary>
        /// Совершение платежей с фракционного счета
        /// </summary>
        BankAccountTransfer = 1 << 15,

        All = 1 << 31
    }
}