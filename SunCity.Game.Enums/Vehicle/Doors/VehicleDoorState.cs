﻿using System.Threading.Tasks;

namespace SunCity.Game.Enums.Vehicle.Doors
{
    public enum VehicleDoorState : byte
    {
        DoorClosed,
        DoorOpen,
        DoorBroken,
    }
}