﻿using System.Threading.Tasks;

namespace SunCity.Game.Enums.Vehicle.Doors
{
    public enum VehicleDoorId : byte
    {
        DoorFrontLeft,
        DoorFrontRight,
        DoorRearLeft,
        DoorRearRight,
        DoorHood,
        DoorTrunk
    }
}