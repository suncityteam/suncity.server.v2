﻿using System;
using System.Threading.Tasks;

namespace SunCity.Game.Enums.Vehicle
{
    [Flags]
    public enum VehiclePartsState
    {
        None,

        Engine = 1,
        Lights = 1 << 1,
        LockedDoors = 1 << 2,

        LeftLightIndicator = 1 << 3,
        RightLightIndicator = 1 << 4,

        Nitro = 1 << 5,

        SirenSound = 1 << 6


    }
}