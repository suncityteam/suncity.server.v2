﻿using System.Threading.Tasks;

namespace SunCity.Game.Enums.Vehicle.Wheels
{
    /// <summary>
    /// https://wiki.rage.mp/index.php?title=Vehicle::setWheelType
    /// </summary>
    public enum VehicleWheelType : byte
    {
        Sport = 0,
        Muscle = 1,
        Lowrider = 2,
        SUV = 3,
        Offroad = 4,
        Tuner = 5,
        BikeWheels = 6,
        HighEnd = 7,
        BennysOriginal = 8,
        BennysBespoke = 9
    }
}