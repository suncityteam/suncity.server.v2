﻿using System.Threading.Tasks;

namespace SunCity.Game.Enums.Vehicle.Wheels
{
    public enum VehicleWheelId : byte
    {
        Wheel0,
        Wheel1,
        Wheel2,
        Wheel3,
        Wheel4,
        Wheel5,
        Wheel6,
        Wheel7,
        Wheel8,
        Wheel9
    }
}