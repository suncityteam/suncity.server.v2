﻿using System.Threading.Tasks;

namespace SunCity.Game.Enums.Vehicle.Wheels
{
    public enum VehicleWheelState : byte
    {
        WheelFixed,
        WheelBurst,
        WheelOnRim
    }
}