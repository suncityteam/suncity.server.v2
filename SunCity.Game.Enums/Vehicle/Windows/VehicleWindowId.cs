﻿using System.Threading.Tasks;

namespace SunCity.Game.Enums.Vehicle.Windows
{
    public enum VehicleWindowId : byte
    {
        WindowFrontRight,
        WindowFrontLeft,
        WindowRearRight,
        WindowRearLeft
    }
}