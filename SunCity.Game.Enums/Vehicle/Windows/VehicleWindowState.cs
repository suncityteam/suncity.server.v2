﻿using System;
using System.Threading.Tasks;

namespace SunCity.Game.Enums.Vehicle.Windows
{
    public enum VehicleWindowStates : byte
    {
        WindowFixed,
        WindowDown,
        WindowBroken,
    }
}