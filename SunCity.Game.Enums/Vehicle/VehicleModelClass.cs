﻿using System.Threading.Tasks;
using SunCity.Common.Enum;

namespace SunCity.Game.Enums.Vehicle
{
    /// <summary>
    /// https://wiki.rage.mp/index.php?title=Vehicles
    /// </summary>
    public enum VehicleModelClass
    {
        [EnumDescription("Неизвестно")]
        None = 0,

        [EnumDescription("Лодки")]
        Boats = 1,
        
        [EnumDescription("Малолитражные")]
        Compacts = 2,

        [EnumDescription("Купе")]
        Coupes = 3,

        [EnumDescription("Велосипеды")]
        Cycles = 4,

        [EnumDescription("Служебные")]
        Emergency = 5,

        [EnumDescription("Промышленные")]
        Industrial = 6,
        
        Service,

        [EnumDescription("Военные")]
        Military,

        [EnumDescription("Мотоциклы")]
        Motorcycles,

        [EnumDescription("Muscle car")]
        Muscle,

        [EnumDescription("Sport car")]
        Sports,

        [EnumDescription("Классические автомобили")]
        SportsClassic,

        [EnumDescription("Super car")]
        SuperCars,

        [EnumDescription("Внедорожники")]
        OffRoad,

        [EnumDescription("S.U.V.")]
        SUVs,

        [EnumDescription("Седан")]
        Sedans,

        [EnumDescription("Вертолет")]
        Helicopters,

        [EnumDescription("Самолет")]
        Planes,

        [EnumDescription("Прицеп")]
        Trailer,

        [EnumDescription("Поезд")]
        Trains,

        [EnumDescription("Служебные")]
        Utility,

        [EnumDescription("Фургоны")]
        Vans

    }
}