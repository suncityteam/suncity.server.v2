﻿using System;
using System.Collections.Generic;
using System.Text;
using SunCity.Common.Enum;

namespace SunCity.Game.Enums.Vehicle
{
    /// <summary>
    /// Виды топлива для ТС
    /// </summary>
    [Flags]
    public enum VehicleFuelType : byte
    {
        None = 0,

        /// <summary>
        /// Бензин
        /// </summary>
        [EnumDescription("Бензин")]
        Petrol = 1,
        
        /// <summary>
        /// Дизель
        /// </summary>
        [EnumDescription("Дизель")]
        Diesel = 1 << 1,
        
        /// <summary>
        /// Электричество
        /// </summary>
        [EnumDescription("Электричество")]
        Electricity = 1 << 2,

        /// <summary>
        /// Газ
        /// </summary>
        [EnumDescription("Газ")]
        Gas = 1 << 3,

        [EnumDescription("+")]
        Premium = 1 << 4
    }
}
