﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SunCity.Game.Enums.Character.Components
{
    public enum GameCharacterComponent
    {
        None,

        /// <summary>
        /// Голод
        /// </summary>
        Health,

        /// <summary>
        /// Голод
        /// </summary>
        Hunger,

        /// <summary>
        /// Жажда
        /// </summary>
        Thirst,
    }
}
