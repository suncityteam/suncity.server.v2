﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SunCity.Game.Enums.Character
{
    public enum GameCharacterSex : byte
    {
        None,
        Male,
        Female,
    }
}
