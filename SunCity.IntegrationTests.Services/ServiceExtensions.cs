﻿using System;
using Microsoft.Extensions.DependencyInjection;
using SunCity.IntegrationTests.RageMp;
using SunCity.IntegrationTests.RageMp.Pools;
using SunCity.IntegrationTests.Services.Connection;
using SunCity.IntegrationTests.Services.Identity;
using SunCity.IntegrationTests.Services.World;

namespace SunCity.IntegrationTests.Services
{
    public static class ServiceExtensions
    {
        public static void AddTestServices(this IServiceCollection services)
        {
            services.UseTestRageMpServer();
            services.AddTestIdentityServices();
            services.AddWorldServices();

            services.AddSingleton<RageConnectionService>();
        }
    }
}
