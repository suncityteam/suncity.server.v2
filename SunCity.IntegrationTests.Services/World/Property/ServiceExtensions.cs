﻿using Microsoft.Extensions.DependencyInjection;
using SunCity.IntegrationTests.Services.World.Property.Business;

namespace SunCity.IntegrationTests.Services.World.Property
{
    internal static class ServiceExtensions
    {
        public static void AddWorldPropertyServices(this IServiceCollection services)
        {
            services.AddWorldBusinessPropertyServices();
        }
    }
}
