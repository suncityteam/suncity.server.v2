﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SunCity.Game.World.Property.Business.Bank.Operations.OpenCard;
using SunCity.IntegrationTests.Common.Generators;

namespace SunCity.IntegrationTests.Services.World.Property.Business.Bank.Client.Generators
{
    public sealed class BankOpenCardRequestGenerator
    {
        public BankOpenCardRequest Generate(PropertyId propertyId, BankCardTariffId bankCardTariffId, Action<BankOpenCardRequest> customize = default)
        {
            var request = Generate(r =>
            {
                r.PropertyId = propertyId;
                r.TariffEntityId = bankCardTariffId;
            });

            customize?.Invoke(request);
            return request;
        }

        public BankOpenCardRequest Generate(Action<BankOpenCardRequest> customize = default)
        {
            var request = new BankOpenCardRequest
            {
                PinCode = DataGenerator.RandomNumeric(1000, 9999),
                SecureWord = DataGenerator.RandomString(8),

            };

            customize?.Invoke(request);
            return request;
        }
    }
}
