﻿using Microsoft.Extensions.DependencyInjection;
using SunCity.IntegrationTests.Services.World.Property.Business.Bank.Client.Generators;
using SunCity.IntegrationTests.Services.World.Property.Business.Bank.Client.Services;

namespace SunCity.IntegrationTests.Services.World.Property.Business.Bank.Client
{
    internal static class ServiceExtensions
    {
        public static void AddWorldClientBankBusinessPropertyServices(this IServiceCollection services)
        {
            services.AddSingleton<BankOpenCardRequestGenerator>();
            services.AddSingleton<ClientBankCardAccountService>();
        }
    }
}
