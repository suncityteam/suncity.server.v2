﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SunCity.Game.World.Property.Business.Bank.Operations.Authorization;
using SunCity.Game.World.Property.Business.Bank.Operations.OpenCard;
using SunCity.IntegrationTests.Common.Extensions;
using SunCity.IntegrationTests.HttpClients.Base.Connections;
using SunCity.IntegrationTests.HttpClients.World.Property.Business.Bank.Client;
using SunCity.IntegrationTests.Services.World.Property.Business.Bank.Client.Generators;

namespace SunCity.IntegrationTests.Services.World.Property.Business.Bank.Client.Services
{
    public sealed class ClientBankCardAccountService
    {
        private BankCardAccountHttpClient BankCardAccountHttpClient { get; }

        private BankOpenCardRequestGenerator OpenCardRequestGenerator { get; }

        public ClientBankCardAccountService
        (
            BankCardAccountHttpClient InBankCardAccountHttpClient,
            BankOpenCardRequestGenerator InOpenCardRequestGenerator
        )
        {
            BankCardAccountHttpClient = InBankCardAccountHttpClient;
            OpenCardRequestGenerator = InOpenCardRequestGenerator;
        }

        public async Task<BankCardId> Authorization(RageMpClientConnection connection, BankAuthorizationRequest request)
        {
            var authorizationResponse = await BankCardAccountHttpClient.Authorization(connection, request);
            authorizationResponse.AssertIsCorrect($"Failed Authorization({request}) for {connection}");
            return authorizationResponse.Response.Content;
        }

        public Task<BankOpenCardResponse> OpenCard(RageMpClientConnection connection, Action<BankOpenCardRequest> customize = default)
        {
            var request = OpenCardRequestGenerator.Generate(customize);
            return OpenCard(connection, request);
        }

        public async Task<BankOpenCardResponse> OpenCard(RageMpClientConnection connection, BankOpenCardRequest request)
        {
            var openCardResponse = await BankCardAccountHttpClient.OpenCard(connection, request);
            openCardResponse.AssertIsCorrect($"Failed OpenCard({request}) for {connection}");
            return openCardResponse.Response.Content;
        }
    }
}