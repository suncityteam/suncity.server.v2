﻿using Microsoft.Extensions.DependencyInjection;
using SunCity.IntegrationTests.Services.World.Property.Business.Bank.Client;

namespace SunCity.IntegrationTests.Services.World.Property.Business.Bank
{
    internal static class ServiceExtensions
    {
        public static void AddWorldBankBusinessPropertyServices(this IServiceCollection services)
        {
            services.AddWorldClientBankBusinessPropertyServices();
        }
    }
}
