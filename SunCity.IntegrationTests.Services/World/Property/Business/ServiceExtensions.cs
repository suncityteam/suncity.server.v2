﻿using Microsoft.Extensions.DependencyInjection;
using SunCity.IntegrationTests.Services.World.Property.Business.Bank;

namespace SunCity.IntegrationTests.Services.World.Property.Business
{
    internal static class ServiceExtensions
    {
        public static void AddWorldBusinessPropertyServices(this IServiceCollection services)
        {
            services.AddWorldBankBusinessPropertyServices();
        }
    }
}
