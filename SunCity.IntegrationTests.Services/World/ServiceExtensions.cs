﻿using Microsoft.Extensions.DependencyInjection;
using SunCity.IntegrationTests.Services.World.Property;

namespace SunCity.IntegrationTests.Services.World
{
    internal static class ServiceExtensions
    {
        public static void AddWorldServices(this IServiceCollection services)
        {
            services.AddWorldPropertyServices();
        }
    }
}
