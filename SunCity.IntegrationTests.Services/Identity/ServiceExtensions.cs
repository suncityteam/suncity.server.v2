﻿using System;
using Microsoft.Extensions.DependencyInjection;
using SunCity.IntegrationTests.Services.Identity.Authorization;
using SunCity.IntegrationTests.Services.Identity.Character;

namespace SunCity.IntegrationTests.Services.Identity
{
    public static class ServiceExtensions
    {
        public static void AddTestIdentityServices(this IServiceCollection services)
        {
            services.AddTestIdentityAuthorizationServices();
            services.AddTestCharacterIdentityServices();
        }
    }
}
