﻿using SunCity.IntegrationTests.HttpClients.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SunCity.Game.World.Enums.Character;
using SunCity.Game.World.Identity.Operations.Character.AccountInformation;
using SunCity.Game.World.Identity.Operations.Character.JoinToWorld;
using SunCity.IntegrationTests.Common.Extensions;
using SunCity.IntegrationTests.HttpClients.Base.Connections;
using SunCity.IntegrationTests.Services.Identity.Character.Generators;

namespace SunCity.IntegrationTests.Services.Identity.Character
{
    public sealed class CharacterIdentityService
    {
        private CreateCharacterRequestGenerator CreateCharacterRequestGenerator { get; }

        private CharacterHttpClient CharacterHttpClient { get; }

        public CharacterIdentityService(CharacterHttpClient characterHttpClient, CreateCharacterRequestGenerator createCharacterRequestGenerator)
        {
            CharacterHttpClient = characterHttpClient;
            CreateCharacterRequestGenerator = createCharacterRequestGenerator;
        }

        public async Task<AuthorizationWhatToDo> WhatToDo(RageMpClientConnection connection)
        {
            var response = await CharacterHttpClient.WhatToDo(connection);
            response.AssertIsCorrect($"Failed fetch WahatToDo for {connection}");

            return response.Response.Content;
        }

        public async Task Select(PlayerCharacterId playerCharacterId, RageMpClientConnection connection)
        {
            var response = await CharacterHttpClient.Select(connection, new CharacterJoinToWorldRequest
            {
                PlayerCharacterId = playerCharacterId,
                UseAsDefault = true
            });

            response.AssertIsCorrect($"Failed select character {playerCharacterId} for {connection}");
        }

        public async Task Create(RageMpClientConnection connection)
        {
            var request = CreateCharacterRequestGenerator.Generate();

            var response = await CharacterHttpClient.Create(connection, request);

            response.AssertIsCorrect($"Failed create character {request} for {connection}");
        }

        public async Task<GetAccountInformationResponse> GetList(RageMpClientConnection connection)
        {
            var response = await CharacterHttpClient.GetList(connection);

            response.AssertIsCorrect($"Failed fetch account information for {connection}");
            response.Response.Content.Characters.AssertIsNotNullAndEmpty($"Failed fetch account information for {connection}");

            return response.Response.Content;
        }

    }
}
