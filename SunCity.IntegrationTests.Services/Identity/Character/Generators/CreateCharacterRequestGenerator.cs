﻿using System;
using SunCity.Common.Extensions;
using SunCity.Game.Enums.Character;
using SunCity.Game.Identity.Forms.Character.Create;
using SunCity.Game.World.Identity.Operations.Character.CreateCharacter;
using SunCity.IntegrationTests.Common.Generators;

namespace SunCity.IntegrationTests.Services.Identity.Character.Generators
{
    public class CreateCharacterRequestGenerator
    {
        public CreateCharacterPlayerName GenerateName(Action<CreateCharacterPlayerName> customize = default)
        {
            var model = new CreateCharacterPlayerName
            {
                FirstName = NameGenerators.UniqueFirstName(),
                LastName = NameGenerators.UniqueLastName(),
            };

            customize?.Invoke(model);
            return model;
        }

        public CreateCharacterGeneralInformation GenerateCharacterGeneralInformation(Action<CreateCharacterGeneralInformation> customize = default)
        {
            var model = new CreateCharacterGeneralInformation
            {
                Sex = GameCharacterSex.Male,
                Ageing = DataGenerator.RandomNumeric(0, 10),
                FatherId = (byte) DataGenerator.RandomNumeric(0, 10),
                MotherId = (byte) DataGenerator.RandomNumeric(0, 10),
                Freckles = (short) DataGenerator.RandomNumeric(0, 10),
                Similarity = DataGenerator.RandomNumeric(0, 10),
                Skin = DataGenerator.RandomNumeric(0, 10),
                Sundamage = (short) DataGenerator.RandomNumeric(0, 10),
            };

            customize?.Invoke(model);
            return model;
        }

        public CreateCharacterFacialFeatures GenerateFacialFeatures(Action<CreateCharacterFacialFeatures> customize = default)
        {
            var model = new CreateCharacterFacialFeatures
            {
                BrowHeight = DataGenerator.RandomNumeric(0, 10),
                BrowWidth = DataGenerator.RandomNumeric(0, 10),
                CheekboneHeight = DataGenerator.RandomNumeric(0, 10),
                CheekboneWidth = DataGenerator.RandomNumeric(0, 10),
                CheeksWidth = DataGenerator.RandomNumeric(0, 10),
                ChinLength = DataGenerator.RandomNumeric(0, 10),
                ChinPosition = DataGenerator.RandomNumeric(0, 10),
                ChinShape = DataGenerator.RandomNumeric(0, 10),
                ChinWidth = DataGenerator.RandomNumeric(0, 10),
                Eyes = DataGenerator.RandomNumeric(0, 10),
                JawHeight = DataGenerator.RandomNumeric(0, 10),
                JawWidth = DataGenerator.RandomNumeric(0, 10),
                Lips = DataGenerator.RandomNumeric(0, 10),
                NeckWidth = DataGenerator.RandomNumeric(0, 10),
                NoseBridge = DataGenerator.RandomNumeric(0, 10),
                NoseHeight = DataGenerator.RandomNumeric(0, 10),
                NoseLength = DataGenerator.RandomNumeric(0, 10),
                NoseShift = DataGenerator.RandomNumeric(0, 10),
                NoseTip = DataGenerator.RandomNumeric(0, 10),
                NoseWidth = DataGenerator.RandomNumeric(0, 10),
            };

            customize?.Invoke(model);
            return model;
        }

        public CreateCharacterRequest Generate
        (
            Action<CreateCharacterPlayerName> customizeName = default,
            Action<CreateCharacterGeneralInformation> customizeCharacter = default,
            Action<CreateCharacterFacialFeatures> customizeFacialFeatures = default
        )
        {
            var request = new CreateCharacterRequest
            {
                Name = GenerateName(customizeName),
                Character = GenerateCharacterGeneralInformation(customizeCharacter),

                FacialFeatures = GenerateFacialFeatures(customizeFacialFeatures),
                Hairs = new CreateCharacterHairAndColor()
            };

            return request;
        }
    }
}