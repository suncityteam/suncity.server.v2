﻿using Microsoft.Extensions.DependencyInjection;
using SunCity.IntegrationTests.Services.Identity.Character.Generators;

namespace SunCity.IntegrationTests.Services.Identity.Character
{
    public static class ServiceExtensions
    {
        public static void AddTestCharacterIdentityServices(this IServiceCollection services)
        {
            services.AddSingleton<CreateCharacterRequestGenerator>();
            services.AddSingleton<CharacterIdentityService>();
        }
    }
}
