﻿using Microsoft.Extensions.DependencyInjection;
using SunCity.IntegrationTests.Services.Identity.Authorization.Generators;

namespace SunCity.IntegrationTests.Services.Identity.Authorization
{
    internal static class ServiceExtensions
    {
        public static void AddTestIdentityAuthorizationServices(this IServiceCollection services)
        {
            services.AddSingleton<AuthorizationService>();
            services.AddSingleton<AuthorizationLoginRequestGenerator>();
            services.AddSingleton<AuthorizationRegisterRequestGenerator>();
        }
    }
}
