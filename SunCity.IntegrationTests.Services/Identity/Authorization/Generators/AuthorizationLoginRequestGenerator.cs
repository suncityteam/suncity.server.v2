﻿using System;
using SunCity.Game.World.Identity.Operations.Authorization.Login;
using SunCity.Game.World.Identity.Operations.Authorization.Register;

namespace SunCity.IntegrationTests.Services.Identity.Authorization.Generators
{
    public sealed class AuthorizationLoginRequestGenerator
    {
        public AuthorizationLoginRequest Generate(AuthorizationRegisterRequest registerRequest, Action<AuthorizationLoginRequest> customize = default)
        {
            var request = new AuthorizationLoginRequest
            {
                Login = registerRequest.Login,
                Password = registerRequest.Password,
            };

            customize?.Invoke(request);
            return request;
        }
    }
}