﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SunCity.Game.Types.Player.Entity.Player;
using SunCity.Game.World.Identity.Operations.Authorization.Register;
using SunCity.IntegrationTests.Common.Generators;

namespace SunCity.IntegrationTests.Services.Identity.Authorization.Generators
{
    public sealed class AuthorizationRegisterRequestGenerator
    {
        public AuthorizationRegisterRequest Generate(Action<AuthorizationRegisterRequest> customize = default)
        {
            var request = new AuthorizationRegisterRequest
            {
                Login = new PlayerAccountLogin(DataGenerator.RandomString(32)),
                Password = DataGenerator.RandomString(8),

                Email = EmailGenerators.GenerateRandomEmail()
            };

            customize?.Invoke(request);
            return request;
        }
    }
}
