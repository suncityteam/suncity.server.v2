﻿using System;
using System.Threading.Tasks;
using SunCity.Game.World.Identity.Operations.Authorization.Login;
using SunCity.Game.World.Identity.Operations.Authorization.Register;
using SunCity.IntegrationTests.Common.Extensions;
using SunCity.IntegrationTests.HttpClients.Base.Connections;
using SunCity.IntegrationTests.HttpClients.Identity;
using SunCity.IntegrationTests.Services.Identity.Authorization.Generators;

namespace SunCity.IntegrationTests.Services.Identity.Authorization
{
    public sealed class AuthorizationService
    {
        private AuthorizationHttpClient AuthorizationHttpClient { get; }
        private AuthorizationLoginRequestGenerator LoginRequestGenerator { get; }
        private AuthorizationRegisterRequestGenerator RegisterRequestGenerator { get; }

        public AuthorizationService
        (
            AuthorizationHttpClient authorizationHttpClient,
            AuthorizationLoginRequestGenerator loginRequestGenerator,
            AuthorizationRegisterRequestGenerator registerRequestGenerator
        )
        {
            AuthorizationHttpClient = authorizationHttpClient;
            LoginRequestGenerator = loginRequestGenerator;
            RegisterRequestGenerator = registerRequestGenerator;
        }

        public async Task<AuthorizationLoginResponse> Login(RageMpClientConnection connection, AuthorizationRegisterRequest request)
        {
            var loginRequest = LoginRequestGenerator.Generate(request);

            var loginResponse = await AuthorizationHttpClient.Login(connection, loginRequest);
            loginResponse.AssertIsCorrect($"Failed Login({request})");

            return loginResponse.Response.Content;
        }

        public async Task<AuthorizationLoginResponse> Login(RageMpClientConnection connection, AuthorizationLoginRequest request)
        {
            var loginResponse = await AuthorizationHttpClient.Login(connection, request);
            loginResponse.AssertIsCorrect($"Failed Login({request})");

            return loginResponse.Response.Content;
        }

        public async Task<AuthorizationRegisterResponse> Register(RageMpClientConnection connection, AuthorizationRegisterRequest request)
        {
            var registerResponse = await AuthorizationHttpClient.Register(connection, request);
            registerResponse.AssertIsCorrect($"Failed Register({request})");

            return registerResponse.Response.Content;
        }

        public Task<AuthorizationRegisterResponse> Register(RageMpClientConnection connection, Action<AuthorizationRegisterRequest> customize = default)
        {
            var request = RegisterRequestGenerator.Generate(customize);
            return Register(connection, request);
        }
    }
}