﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SunCity.Common.RageMp.Pools.Players;
using SunCity.IntegrationTests.HttpClients.Base.Connections;
using SunCity.IntegrationTests.RageMp.Pools.Player;

namespace SunCity.IntegrationTests.Services.Connection
{
    public sealed class RageConnectionService
    {
        private ITestPlayerPool PlayerPool { get; }

        public RageConnectionService(ITestPlayerPool playerPool)
        {
            PlayerPool = playerPool;
        }

        public RageMpClientConnection CreatePlayer(Action<IRagePlayer> customize = default)
        {
            var player = PlayerPool.Create();
            customize?.Invoke(player);
            return new RageMpClientConnection(player);
        }
    }
}
