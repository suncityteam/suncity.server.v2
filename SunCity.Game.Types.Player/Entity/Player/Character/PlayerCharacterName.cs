﻿using System;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;
using SunCity.Domain.Interfaces.Types;

namespace SunCity.Game.Types.Player.Entity.Player.Character
{
    public readonly struct PlayerCharacterName : IName<PlayerCharacterName>
    {
        public string Name { get; }
        public const int MaxLenght = 64;

        public PlayerCharacterName(string InName)
        {
            Name = InName;
        }


        #region Interfaces
        public bool Equals(PlayerCharacterName InCharacterName) => Name == InCharacterName.Name;
        public int CompareTo(PlayerCharacterName InCharacterName) => Name.CompareTo(InCharacterName);
        public override int GetHashCode() => (Name != null ? Name.GetHashCode() : 0);
        public string ToString(string? InFormat, IFormatProvider? InFormatProvider) => Name;
        public int CompareTo(object? InCharacterName) => Name.CompareTo(InCharacterName);
        #endregion

        public sealed class ValueConverter : ValueConverter<PlayerCharacterName, string>
        {
            public ValueConverter() : base(InName => InName.Name, InName => new PlayerCharacterName(InName)) { }
        }
    }
}