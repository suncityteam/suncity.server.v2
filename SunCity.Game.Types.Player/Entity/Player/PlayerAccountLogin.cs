﻿using System;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;
using SunCity.Domain.Interfaces.Types;

namespace SunCity.Game.Types.Player.Entity.Player
{
    public readonly struct PlayerAccountLogin
        : IName<PlayerAccountLogin>
        , IFormattable
    {
        public override bool Equals(object? obj)
        {
            return obj is PlayerAccountLogin other && Equals(other);
        }

        public string Name { get; }
        public const int MaxLenght = 32;

        public PlayerAccountLogin(string InLogin) => Name = InLogin;

        public static bool operator ==(PlayerAccountLogin InLhs, PlayerAccountLogin InRhs) => InLhs.Equals(InRhs);
        public static bool operator !=(PlayerAccountLogin InLhs, PlayerAccountLogin InRhs) => !InLhs.Equals(InRhs);

        public bool Equals(PlayerAccountLogin InLogin) => Name == InLogin.Name;
        public int CompareTo(PlayerAccountLogin InLogin) => Name.CompareTo(InLogin);
        public override int GetHashCode() => (Name != null ? Name.GetHashCode() : 0);
        public string ToString(string? InFormat, IFormatProvider? InFormatProvider) => Name;
    }
}