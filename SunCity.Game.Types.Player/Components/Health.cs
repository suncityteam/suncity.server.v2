﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;
using SunCity.Domain.Interfaces.Types;

namespace SunCity.Game.Types.Player.Components
{
    public struct Health : IAmount<long>
    {
        public long Amount { get; private set; }

        public Health(long InAmount)
        {
            Amount = InAmount;
        }

        public static Health operator +(Health InA, Health InB)
        {
            return new Health(InA.Amount + InB.Amount);
        }

        public void Set(long InHealth)
        {
            Amount = InHealth;
        }

        public void Give(long InHealth)
        {
            Amount += InHealth;
        }

        public static Health Default { get; }= new Health(100);
        public sealed class ValueConverter : ValueConverter<Health, long>
        {
            public ValueConverter() : base(InHealth => InHealth.Amount, InHealth => new Health(InHealth)) { }
        }
    }
}
