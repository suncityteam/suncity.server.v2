﻿using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;
using SunCity.Domain.Interfaces.Types;

namespace SunCity.Game.Types.Player.Components
{
    public struct Hunger : IAmount<long>
    {
        public long Amount { get; private set; }

        public Hunger(long InAmount)
        {
            Amount = InAmount;
        }

        public void Set(long InHunger)
        {
            Amount = InHunger;
        }

        public void Give(long InHunger)
        {
            Amount += InHunger;
        }

        public static Hunger operator +(Hunger InA, Hunger InB)
        {
            return new Hunger(InA.Amount + InB.Amount);
        }

        public static Hunger Default { get; } = new Hunger(100);

        public sealed class ValueConverter : ValueConverter<Hunger, long>
        {
            public ValueConverter() : base(InHunger => InHunger.Amount, InHunger => new Hunger(InHunger)) { }
        }
    }
}