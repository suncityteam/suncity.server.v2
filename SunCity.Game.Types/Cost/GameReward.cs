﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SunCity.Game.Types.Cost
{
    public class GameReward
    {
        public long Experience { get; set; }
        public virtual GameCost Currency { get; set; }
    }
}
