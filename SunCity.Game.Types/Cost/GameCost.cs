﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Diagnostics;
using System.Text;
using Microsoft.EntityFrameworkCore;
using SunCity.Common.Reflection;
using SunCity.Game.Enums;

namespace SunCity.Game.Types.Cost
{
    [Owned]
    
    [DebuggerDisplay("[GameCost][Currency: {Currency}][Amount: {Amount}]")]
    public class GameCost : ICloneableObject<GameCost>
    {
        public GameCost() { }

        public GameCost(long InAmount)
        {
            Amount = InAmount;
            Currency = GameCurrency.Money;
        }

        public GameCost(long InAmount, GameCurrency InCurrency)
        {
            Amount = InAmount;
            Currency = InCurrency;
        }

        public long Amount { get; set; }
        public GameCurrency Currency { get; set; }
        public override string ToString()
        {
            if (Currency == GameCurrency.Money)
            {
                return $"{Amount}$";
            }
            return $"{Amount}DP";
        }

        public GameCost CloneObject()
        {
            return new GameCost
            {
                Amount = Amount,
                Currency = Currency
            };
        }

        public static GameCost operator +(GameCost InLeft, long InValue)
        {
            return new GameCost
            {
                Currency = InLeft.Currency,
                Amount = InLeft.Amount + InValue
            };
        }

        public static GameCost operator /(GameCost InLeft, long InValue)
        {
            var safe = InValue;
            if (safe == 0)
            {
                safe = 1;
            }

            return new GameCost
            {
                Currency = InLeft.Currency,
                Amount = InLeft.Amount / safe
            };
        }

        public static GameCost operator /(GameCost InLeft, float InValue)
        {
            double safe = InValue;
            if (System.Math.Abs(safe) < 0.1f)
            {
                safe = 1.0;
            }

            return new GameCost
            {
                Currency = InLeft.Currency,
                Amount = Convert.ToInt64(System.Math.Round(InLeft.Amount / safe))
            };
        }

    }
}
