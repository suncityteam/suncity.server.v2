﻿using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace SunCity.Game.Types.Vehicle.States
{
    [Owned]
    public class VehicleHealth
    {
        /// <summary>
        /// Состояние здовья корпуса ТС
        /// </summary>
        public virtual float Body { get; set; } = 1000.0f;

        /// <summary>
        /// Состояние здовья двигателя ТС
        /// </summary>
        public virtual float Engine { get; set; } = 1000.0f;

        /// <summary>
        /// Состояние здовья коробки передач ТС
        /// </summary>
        public virtual float Gears { get; set; } = 1000.0f;
    }
}