﻿using System;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.EntityFrameworkCore;
using SunCity.Game.Enums.Vehicle.Doors;

namespace SunCity.Game.Types.Vehicle.States
{
    [Owned]
    public class VehicleDoorsState
    {
        public virtual VehicleDoorState DoorFrontLeft { get; set; }
        public virtual VehicleDoorState DoorFrontRight { get; set; }
        public virtual VehicleDoorState DoorRearLeft { get; set; }
        public virtual VehicleDoorState DoorRearRight { get; set; }
        public virtual VehicleDoorState DoorHood { get; set; }
        public virtual VehicleDoorState DoorTrunk { get; set; }

        public VehicleDoorState this[VehicleDoorId InVehicleDoorId]
        {
            get
            {
                switch (InVehicleDoorId)
                {
                    case VehicleDoorId.DoorFrontLeft:
                        return DoorFrontLeft;
                    case VehicleDoorId.DoorFrontRight:
                        return DoorFrontRight;
                    case VehicleDoorId.DoorRearLeft:
                        return DoorRearLeft;
                    case VehicleDoorId.DoorRearRight:
                        return DoorRearRight;
                    case VehicleDoorId.DoorHood:
                        return DoorHood;
                    case VehicleDoorId.DoorTrunk:
                        return DoorTrunk;
                    default:
                        throw new ArgumentOutOfRangeException(nameof(InVehicleDoorId), InVehicleDoorId, null);
                }
            }
        }
    }
}