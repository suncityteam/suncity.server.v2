﻿using System.ComponentModel.DataAnnotations.Schema;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.EntityFrameworkCore;

namespace SunCity.Game.Types.Vehicle.States
{
    [Owned]
    public class VehicleColor
    {
        public virtual int FirstColor { get; set; }
        public virtual int SecondColor { get; set; }
        public virtual int Pearlescent { get; set; }
    }
}