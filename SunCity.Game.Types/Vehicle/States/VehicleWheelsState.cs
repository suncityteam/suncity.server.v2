﻿using System.ComponentModel.DataAnnotations.Schema;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.EntityFrameworkCore;
using SunCity.Game.Enums.Vehicle.Wheels;

namespace SunCity.Game.Types.Vehicle.States
{
    [Owned]
    public class VehicleWheelsState
    {
        public VehicleWheelState Wheel0 { get; set; }
        public VehicleWheelState Wheel1 { get; set; }
        public VehicleWheelState Wheel2 { get; set; }
        public VehicleWheelState Wheel3 { get; set; }
        public VehicleWheelState Wheel4 { get; set; }
        public VehicleWheelState Wheel5 { get; set; }
        public VehicleWheelState Wheel6 { get; set; }
        public VehicleWheelState Wheel7 { get; set; }
        public VehicleWheelState Wheel8 { get; set; }
        public VehicleWheelState Wheel9 { get; set; }
    }
}