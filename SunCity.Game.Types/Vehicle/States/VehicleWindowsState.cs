﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using SunCity.Game.Enums.Vehicle.Windows;

namespace SunCity.Game.Types.Vehicle.States
{
    [Owned]
    public class VehicleWindowsState
    {
        public VehicleWindowStates WindowFrontLeft { get; set; }
        public VehicleWindowStates WindowFrontRight { get; set; }
        public VehicleWindowStates WindowRearRight { get; set; }
        public VehicleWindowStates WindowRearLeft { get; set; }

        public VehicleWindowStates this[VehicleWindowId InVehicleWindowId]
        {
            get
            {
                switch (InVehicleWindowId)
                {
                    case VehicleWindowId.WindowFrontRight:
                        return WindowFrontRight;
                    case VehicleWindowId.WindowFrontLeft:
                        return WindowFrontLeft;

                    case VehicleWindowId.WindowRearRight:
                        return WindowRearRight;
                    case VehicleWindowId.WindowRearLeft:
                        return WindowRearLeft;

                    default:
                        throw new ArgumentOutOfRangeException(nameof(InVehicleWindowId), InVehicleWindowId, null);
                }
            }
        }
    }
}