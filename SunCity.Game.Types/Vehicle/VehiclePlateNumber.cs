﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;
using Microsoft.EntityFrameworkCore;
using SunCity.Common.RageMp.Enums;

namespace SunCity.Game.Types.Vehicle
{
    [Owned]
    public class VehiclePlateNumber
    {
        [Required]
        [MaxLength(16)]
        public virtual string Number { get; set; }

        public virtual VehicleNumberPlateType Style { get; set; } = VehicleNumberPlateType.BlueWhite2;
    }
}
