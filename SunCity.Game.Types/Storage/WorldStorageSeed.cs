﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using SunCity.Domain.Interfaces.Entity;
using SunCity.Domain.Interfaces.Types;

namespace SunCity.Game.Types.Storage
{
    public interface IWorldStorageSeed<in TDbContext, TKey, TModel>
        where TDbContext : DbContext
        where TModel : IEntityWithId<TKey>
    {
        Task Initialize(TDbContext InDbContext);
    }

    public abstract class WorldStorageSeed<TDbContext, TKey, TModel> : IWorldStorageSeed<TDbContext, TKey, TModel>
        where TModel : class, IEntityWithId<TKey>
        where TDbContext : DbContext
        where TKey : IGuidId<TKey>
    {
        protected ILogger Logger { get; }
        protected abstract Func<TDbContext, DbSet<TModel>> Table { get; }
        
        protected WorldStorageSeed(ILogger logger)
        {
            Logger = logger;
        }
        
        public abstract Task Initialize(TDbContext InDbContext);
       
        protected async Task AddIfNotExist(TDbContext InDbContext, TModel model)
        {
            var table = Table(InDbContext);
            var existLocal = table.Local.Any(q => q.EntityId.Equals(model.EntityId));
            if (existLocal)
            {
                throw new ArgumentException($"Duplicate for {model.EntityId}");
            }
            
            var exist = await table.AnyAsync(q => q.EntityId.Equals(model.EntityId));
            if (!exist)
            {
                Logger.LogDebug($"Added {model.EntityId} to {typeof(TModel).Name}");
                await table.AddAsync(model);
            }
        }
    }
}