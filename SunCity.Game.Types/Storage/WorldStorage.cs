﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using SunCity.Domain.Interfaces.Entity;

namespace SunCity.Game.Types.Storage
{
    public interface IWorldStorage<in TKey, TModel>
        where TModel : IEntityWithId<TKey>
    {
        TModel GetByKey(TKey InKey);

        IReadOnlyCollection<TModel> Values { get; }
        IReadOnlyCollection<TModel> ValuesCopy { get; }

        TModel this[TKey InCharacterId] { get; }
        TModel Put(TModel InItem);
    }

    public abstract class WorldStorage<TKey, TModel> : IWorldStorage<TKey, TModel>
        where TModel : IEntityWithId<TKey>
    {
        private ReaderWriterLockSlim Lock { get; } = new ReaderWriterLockSlim(LockRecursionPolicy.NoRecursion);

        protected Dictionary<TKey, TModel> Items { get; } = new Dictionary<TKey, TModel>(8192);

        public IReadOnlyCollection<TModel> Values => Items.Values;

        public IReadOnlyCollection<TModel> ValuesCopy
        {
            get
            {
                Lock.EnterReadLock();
                var copy = Values.ToArray();
                Lock.ExitReadLock();
                return copy;
            }
        }


        public virtual TModel this[TKey InKey] => GetByKey(InKey);

        public virtual TModel GetByKey(TKey InKey)
        {
            Lock.EnterReadLock();
            Items.TryGetValue(InKey, out var item);
            Lock.ExitReadLock();

            return item;
        }

        public virtual TModel Put(TModel InItem)
        {
            Lock.EnterWriteLock();
            if (Items.ContainsKey(InItem.EntityId))
            {
                Items.Remove(InItem.EntityId);
            }

            Items.Add(InItem.EntityId, InItem);
            Lock.ExitWriteLock();
            return InItem;
        }
    }
}
