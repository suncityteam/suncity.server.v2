﻿using System;
using System.Collections.Generic;
using System.Linq;

using System.Threading.Tasks;
using GTANetworkAPI;
using RAGE;
using SunCity.Common.RageMp.Pools.Players;
using SunCity.Common.Types.Extensions;
using SunCity.Game.World.Common.Chat;
using SunCity.Game.World.Enums.Chat;
using SunCity.Game.World.Models.Chat;

namespace SunCity.Game.World.Extensions.Chat
{
    public static class WorldChatService
    {
        public static Vector3 GetForwardPosition(this IRagePlayer InClient, float InDistance = 10f)
        {
            
            var x = InDistance * Math.Cos(InClient.Rotation.Z) + InClient.Position.X;
            var y = InDistance * Math.Sin(InClient.Rotation.Z) + InClient.Position.Y;
            var z = InClient.Position.Z;

            return new Vector3((float)x, (float)y, (float)z);
        }

        public static async Task SendMessageToNearbyPlayers(this IRagePlayer InSenderPlayer, WorldChatMessage InChatMessage)
        {
            //=======================================
            var distanceGap = InChatMessage.Range / ChatColors.CHAT_RANGES;

            //=======================================
            //  Cache
            var position = InSenderPlayer.Position;
            var dimension = InSenderPlayer.Dimension;

            //=======================================
            var nearbyPlayers = await GetNearbyPlayers(InSenderPlayer, InChatMessage.Recive, InChatMessage.Range);
            var sendMessagesTasks = new List<Task>(nearbyPlayers.Length * InChatMessage.Message.Length);

            //=======================================
            NAPI.Task.Run(() =>
            {
                foreach (var player in nearbyPlayers)
                {
                    var distance = player.Position.DistanceTo(position);
                    var chatMessageColor = GetChatMessageColor(distance, distanceGap);
                    foreach (var message in InChatMessage.Message)
                    {
                        player.SendChatMessage($"{chatMessageColor}{message}");
                    }
                }
            });
        }

        private static Task<GTANetworkAPI.Player[]> GetNearbyPlayers(IRagePlayer InSenderPlayer, WorldChatMessaveRecive InRecive, float InDistanceRange)
        {
            return Task.Run(() =>
            {
                if (InRecive == WorldChatMessaveRecive.ExcludeSenderPlayer)
                {
                    return Entities.Players.All.AsParallel().Where(q => q.Id != InSenderPlayer.Id && IsNearbyPlayer(InSenderPlayer, q, InDistanceRange)).ToArray();
                }
                return Entities.Players.All.AsParallel().Where(q => IsNearbyPlayer(InSenderPlayer, q, InDistanceRange)).ToArray();
            });
        }

        private static bool IsNearbyPlayer(IRagePlayer InSenderPlayer, GTANetworkAPI.Player InPlayer, float InDistanceRange)
        {
            if (InPlayer.Exists == false)
                return false;

            if (InSenderPlayer.Dimension != InPlayer.Dimension)
                return false;

            return InSenderPlayer.Position.InDistanceRange(InPlayer.Position, InDistanceRange);
        }

        private static string GetChatMessageColor(double InDistance, float InDistanceGap)
        {
            if (InDistance < InDistanceGap)
            {
                return ChatColors.COLOR_CHAT_CLOSE;
            }
            else if (InDistance < InDistanceGap * 2)
            {
                return ChatColors.COLOR_CHAT_NEAR;
            }
            else if (InDistance < InDistanceGap * 3)
            {
                return ChatColors.COLOR_CHAT_MEDIUM;
            }
            else if (InDistance < InDistanceGap * 4)
            {
                return ChatColors.COLOR_CHAT_FAR;
            }
            else
            {
                return ChatColors.COLOR_CHAT_LIMIT;
            }
        }
    }
}
