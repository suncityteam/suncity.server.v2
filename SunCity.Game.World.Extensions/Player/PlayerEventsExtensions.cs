﻿using System.Threading.Tasks;
using GTANetworkAPI;
using SunCity.Common.Json;
using SunCity.Common.Operation;
using SunCity.Common.RageMp.Pools.Players;
using SunCity.Game.Types.Property.Entity.Interior;

namespace SunCity.Game.World.Extensions.Player
{
    public static class PlayerEventsExtensions
    {
        //=================================
        public static Task CallClientEvent<TObject>(this GTANetworkAPI.Player InPlayer, string InEvent, OperationResponse<TObject> InObject = default)
        {
            var json = CommonJsonSerializer.Instance.Serialize(InObject);
            NAPI.Task.Run(() => InPlayer.TriggerEvent($"Event:{InEvent}", json));
            return Task.CompletedTask;
        }

        public static Task CallClientEvent(this GTANetworkAPI.Player InPlayer, string InEvent)
        {
            NAPI.Task.Run(() => InPlayer.TriggerEvent($"Event:{InEvent}"));
            return Task.CompletedTask;
        }

        public static Task CallClientEventHandler<TObject>(this GTANetworkAPI.Player InPlayer, string InEvent, OperationResponse<TObject> InObject = default)
        {
            var json = CommonJsonSerializer.Instance.Serialize(InObject);
            NAPI.Task.Run(() => InPlayer.TriggerEvent($"Handler:{InEvent}", json));
            return Task.CompletedTask;
        }

        //=================================
        public static void RequestIpl(this IRagePlayer InPlayer, InteriorItemPlacement InItemPlacements) => InPlayer.TriggerEvent("RequestIpl", InItemPlacements.Name);
        public static void RemoveIpl(this IRagePlayer InPlayer, InteriorItemPlacement InItemPlacements) => InPlayer.TriggerEvent("RemoveIpl", InItemPlacements.Name);
    }
}