﻿using System;
using System.Collections.Generic;
using System.Text;
using GTANetworkAPI;

namespace SunCity.Game.World.Extensions.Player
{
    public static class PlayerDataExtensions
    {
        public static bool TryGetData(this Entity InPlayer, string InPropertyName, out bool InValue)
        {
            InValue = default;
            return false;
            //return InPlayer.TryGetData(InPropertyName, out var obj) && bool.TryParse(obj?.ToString(), out InValue);
        }

        public static bool TryGetData(this Entity InPlayer, string InPropertyName, out int InValue)
        {
            InValue = default;
            return false;

            //return InPlayer.TryGetData(InPropertyName, out var obj) && int.TryParse(obj?.ToString(), out InValue);
        }

        public static bool TryGetData(this Entity InPlayer, string InPropertyName, out long InValue)
        {
            InValue = default;
            return false;

            //return InPlayer.TryGetData(InPropertyName, out var obj) && long.TryParse(obj?.ToString(), out InValue);
        }

        public static bool TryGetData(this Entity InPlayer, string InPropertyName, out Guid InValue)
        {
            InValue = default;
            return false;
            //return InPlayer.TryGetData(InPropertyName, out var obj) && Guid.TryParse(obj?.ToString(), out InValue);
        }

        public static bool TryGetData(this Entity InPlayer, string InPropertyName, out string InValue)
        {
            InValue = default;
            return false;

            /*if (InPlayer.TryGetData(InPropertyName, out var obj))
            {
                InValue = obj.ToString();
                return true;
            }
            return false;*/
        }
    }
}
