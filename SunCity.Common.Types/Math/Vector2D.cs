﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

using System.Text;
using Microsoft.EntityFrameworkCore;
using SunCity.Common.Types.Extensions;

namespace SunCity.Common.Types.Math
{
    [Owned]
    
    public class Vector2D
    {
        public float X { get; set; }
        public float Y { get; set; }

        public Vector2D() { }

        public Vector2D(float InUniform)
        {
            X = Y = InUniform;
        }

        public Vector2D(float InX, float InY)
        {
            X = InX;
            Y = InY;
        }

        public override string ToString()
        {
            return $"X: {X:F}, Y: {Y:F}";
        }
    }
}
