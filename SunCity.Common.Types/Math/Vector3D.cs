﻿using GTANetworkAPI;
using Microsoft.EntityFrameworkCore;
using SunCity.Common.Types.Extensions;

namespace SunCity.Common.Types.Math
{
    [Owned]
    public class Vector3D : Vector2D
    {
        public float Z { get; set; }

        public Vector3D() { }

        public Vector3D(float InUniform)
            : base(InUniform)
        {
            Z = InUniform;
        }

        public Vector3D(float x, float y, float z)
            : base(x, y)
        {
            Z = z;
        }

        public override string ToString()
        {
            return $"X: {X:F}, Y: {Y:F}, Z: {Z:F}";
        }

        public static implicit operator Vector3D(Vector3 InVector3D) => InVector3D.AsVector3D();
        public static implicit operator Vector3(Vector3D InVector3D) => InVector3D.AsVector3();
    }
}