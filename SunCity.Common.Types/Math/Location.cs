﻿using Microsoft.EntityFrameworkCore;

namespace SunCity.Common.Types.Math
{
    [Owned]
    public class Location : Vector4D
    {
        public int Interior { get; set; }
        public int VirtualWorld { get; set; }

        public override string ToString()
        {
            return $"X: {X:F}, Y: {Y:F}, Z: {Z:F}, R: {R:F}, Interior: {Interior}, VirtualWorld: {VirtualWorld}";
        }
    }
}