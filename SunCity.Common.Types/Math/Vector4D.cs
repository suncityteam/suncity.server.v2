﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.EntityFrameworkCore;

namespace SunCity.Common.Types.Math
{
    [Owned]
    public class Vector4D : Vector3D
    {
        public float R { get; set; }

        public override string ToString()
        {
            return $"X: {X:F}, Y: {Y:F}, Z: {Z:F}, R: {R:F}";
        }
    }
}
