﻿
using System.Threading.Tasks;
using AutoMapper;
using GTANetworkAPI;
using Microsoft.EntityFrameworkCore;

namespace SunCity.Common.Types.Math
{
    [Owned]
    public class Bounds
    {
        public float MinX { get; set; }
        public float MinY { get; set; }
        public float MinZ { get; set; }


        public float MaxX { get; set; }
        public float MaxY { get; set; }
        public float MaxZ { get; set; }

        public bool InBounds(Vector3 InPosition)
        {
            return
                InPosition.X >= MinX && InPosition.X <= MaxX &&
                InPosition.Y >= MinY && InPosition.Y <= MaxY &&
                InPosition.Z >= MinZ && InPosition.Z <= MaxZ;
        }

        //public bool InBounds(Vector2 InPosition)
        //{
        //    return
        //        InPosition.X >= MinX && InPosition.X <= MaxX &&
        //        InPosition.Y >= MinY && InPosition.Y <= MaxY;
        //}

        public override string ToString()
        {
            return $"Min.X: {MinX:F}, Min.Y: {MinY:F}, Min.Z: {MinZ:F}, Max.X: {MaxX:F}, Max.Y: {MaxY:F}, Max.Z: {MaxZ:F}";
        }
    }
}