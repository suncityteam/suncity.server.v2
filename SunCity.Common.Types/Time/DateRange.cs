﻿using System;
using Microsoft.EntityFrameworkCore;

namespace SunCity.Common.Types.Time
{
    [Owned]
    public class DateRange
    {
        public DateTime Begin { get; set; }
        public DateTime End { get; set; }

        public override string ToString()
        {
            return $"{Begin:d} -- {End:d}";
        }
    }
}