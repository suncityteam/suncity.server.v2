﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.EntityFrameworkCore;

namespace SunCity.Common.Types.Time
{
    [Owned]
    public class TimeRange
    {
        public TimeSpan Begin { get; set; }
        public TimeSpan End { get; set; }

        public override string ToString()
        {
            return $"{Begin} -- {End}";
        }
    }
}
