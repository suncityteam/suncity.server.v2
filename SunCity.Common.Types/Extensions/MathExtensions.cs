﻿using System;
using System.Collections.Generic;
using System.Text;
using GTANetworkAPI;
using SunCity.Common.Types.Math;

namespace SunCity.Common.Types.Extensions
{
    public static class MathExtensions
    {
        public static uint AsUint32(this int? InValue)
        {
            if (InValue == null) return 0;
            return (uint) InValue.Value;
        }

        public static uint? AsUint32Null(this int? InValue)
        {
            if (InValue == null) return null;
            return (uint)InValue.Value;
        }

        public static int AsInt32(this uint InValue)
        {
            return (int)InValue;
        }

        public static Vector3 AsVector3(this float InVector)
        {
            return new Vector3(InVector, InVector, InVector);
        }

        public static Vector3 AsVector3X(this float InVector)
        {
            return new Vector3(InVector, 0, 0);
        }

        public static Vector3 AsVector3Y(this float InVector)
        {
            return new Vector3(0, InVector, 0);
        }

        public static Vector3 AsVector3Z(this float InVector)
        {
            return new Vector3(0, 0, InVector);
        }

        public static Vector3 AsVector3(this Vector3D InVector)
        {
            return new Vector3(InVector.X, InVector.Y, InVector.Z);
        }

        public static Vector3D AsVector3D(this Vector3 InVector)
        {
            return new Vector3D(InVector.X, InVector.Y, InVector.Z);
        }

        public static Vector3 AddX(this Vector3 InVector, float InOffSet)
        {
            return new Vector3(InVector.X + InOffSet, InVector.Y, InVector.Z);
        }

        public static Vector3 AddY(this Vector3 InVector, float InOffSet)
        {
            return new Vector3(InVector.X, InVector.Y + InOffSet, InVector.Z);
        }

        public static Vector3 AddZ(this Vector3 InVector, float InOffSet)
        {
            return new Vector3(InVector.X, InVector.Y, InVector.Z + InOffSet);
        }


        public static double DistanceToSquared(this Vector3 InLeft, Vector3 InRight)
        {
            double num1 = InLeft.X - InRight.X;
            double num2 = InLeft.Y - InRight.Y;
            double num3 = InLeft.Z - InRight.Z;
            return num1 * num1 + num2 * num2 + num3 * num3;
        }

        public static double DistanceTo(this Vector3 InLeft, Vector3 InRight)
        {
            return System.Math.Sqrt(InLeft.DistanceToSquared(InRight));
        }

        public static bool InDistanceRange(this Vector3 InLeft, Vector3 InRight, double InDistance)
        {
            return InLeft.DistanceTo(InRight) <= InDistance;
        }

        public static bool OutDistance(this Vector3 InLeft, Vector3 InRight, float InDistance)
        {
            return InLeft.DistanceTo(InRight) >= InDistance;
        }


        public static Vector3 Lerp(this Vector3 InStart, Vector3 InEnd, float InN)
        {
            return new Vector3()
            {
                X = InStart.X + (InEnd.X - InStart.X) * InN,
                Y = InStart.Y + (InEnd.Y - InStart.Y) * InN,
                Z = InStart.Z + (InEnd.Z - InStart.Z) * InN
            };
        }

    }
}
