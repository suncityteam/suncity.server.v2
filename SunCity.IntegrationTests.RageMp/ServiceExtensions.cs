﻿using Microsoft.Extensions.DependencyInjection;
using SunCity.Common.RageMp.Events;
using SunCity.Common.RageMp.Pools;
using SunCity.Common.RageMp.Server;
using SunCity.Common.RageMp.Tasks;
using SunCity.Common.RageMp.World;
using SunCity.IntegrationTests.RageMp.Events;
using SunCity.IntegrationTests.RageMp.Pools;
using SunCity.IntegrationTests.RageMp.World;

namespace SunCity.IntegrationTests.RageMp
{
    public static class ServiceExtensions
    {
        public static void UseTestRageMpServer(this IServiceCollection services)
        {
            services.AddSingleton<IRageMpServer, RageMpServer>();
            services.AddSingleton<IRageMpPool, RageMpPool>();
            services.AddSingleton<IRageMpGameWorld, TestRageGameWorld>();
            services.AddSingleton<IRageMpTaskManager, TestRageTaskManager>();

            services.AddSingleton<ITestRageEventManager, TestRageEventManager>();
            services.AddSingleton<IRageMpEventManager>(provider => provider.GetRequiredService<ITestRageEventManager>());

            services.UseTestRageMpPools();

        }
    }
}
