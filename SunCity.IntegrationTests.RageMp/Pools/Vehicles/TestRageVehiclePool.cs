﻿    using GTANetworkAPI;
using SunCity.Common.RageMp.Pools;
using SunCity.Common.RageMp.Pools.Vehicles;
using SunCity.IntegrationTests.RageMp.Pools.Entity;

namespace SunCity.IntegrationTests.RageMp.Pools.Vehicles
{
    internal class TestRageVehiclePool : TestRageEntityPool<IRageVehicle>, ITestRageVehiclePool
    {
        public IRageVehicle New(VehicleHash model, Vector3 pos, float rot, string plateNumber, uint dimension = RageMpConsts.GlobalDimension)
        {
            return Add(new TestRageVehicle(GetCreateIndex()));
        }
    }
}