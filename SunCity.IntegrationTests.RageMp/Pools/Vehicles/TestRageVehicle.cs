﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GTANetworkAPI;
using SunCity.Common.RageMp.Pools.Vehicles;
using SunCity.IntegrationTests.RageMp.Pools.Entity;

namespace SunCity.IntegrationTests.RageMp.Pools.Vehicles
{
    internal class TestRageVehicle : TestRageEntity, IRageVehicle
    {
        public TestRageVehicle(ushort id)
        {
            Id = id;
        }

        public override ushort Id { get; }
        public override EntityType Type { get; } = EntityType.Vehicle;

        public int PrimaryColor { get; set; }
        public int SecondaryColor { get; set; }
        public Color CustomPrimaryColor { get; set; }
        public Color CustomSecondaryColor { get; set; }
        public int DashboardColor { get; set; }
        public int TrimColor { get; set; }
        public VehiclePaint PrimaryPaint { get; set; }
        public VehiclePaint SecondaryPaint { get; set; }
        public int PearlescentColor { get; set; }
        public int WheelColor { get; set; }
        public int WheelType { get; set; }
        public int WindowTint { get; set; }
        public float Health { get; set; }
        public float EnginePowerMultiplier { get; set; }
        public float EngineTorqueMultiplier { get; set; }
        public int Livery { get; set; }
        public bool Siren { get; set; }
        public bool Neons { get; set; }
        public bool SpecialLight { get; set; }
        public bool CustomTires { get; set; }
        public bool BulletproofTyres { get; set; }
        public bool EngineStatus { get; set; }
        public bool Locked { get; set; }
        public Color TyreSmokeColor { get; set; }
        public Color NeonColor { get; set; }
        public string NumberPlate { get; set; }
        public int NumberPlateStyle { get; set; }

        public void Repair()
        {
            Health = 1000;
        }

        public void Spawn(Vector3 position, float heading = 0)
        {
            Health = 1000;
            Position = position;
            Rotation = new Vector3(0, 0, heading);
        }
    }
}
