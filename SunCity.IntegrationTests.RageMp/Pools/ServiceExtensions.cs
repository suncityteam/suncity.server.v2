﻿using Microsoft.Extensions.DependencyInjection;
using SunCity.Common.RageMp.Pools.Blips;
using SunCity.Common.RageMp.Pools.Markers;
using SunCity.Common.RageMp.Pools.Players;
using SunCity.Common.RageMp.Pools.TextLabels;
using SunCity.Common.RageMp.Pools.Vehicles;
using SunCity.IntegrationTests.RageMp.Pools.Blips;
using SunCity.IntegrationTests.RageMp.Pools.Markers;
using SunCity.IntegrationTests.RageMp.Pools.Player;
using SunCity.IntegrationTests.RageMp.Pools.TextLabels;
using SunCity.IntegrationTests.RageMp.Pools.Vehicles;

namespace SunCity.IntegrationTests.RageMp.Pools
{
    public static class ServiceExtensions
    {
        public static void UseTestRageMpPools(this IServiceCollection services)
        {
            services.AddSingleton<ITestPlayerPool, TestPlayerPool>();
            services.AddSingleton<IPlayerPool>(provider => provider.GetRequiredService<ITestPlayerPool>());

            services.AddSingleton<ITestRageVehiclePool, TestRageVehiclePool>();
            services.AddSingleton<IVehiclePool>(provider => provider.GetRequiredService<ITestRageVehiclePool>());

            services.AddSingleton<ITestRageBlipPool, TestRageBlipPool>();
            services.AddSingleton<IBlipPool>(provider => provider.GetRequiredService<ITestRageBlipPool>());

            services.AddSingleton<ITestRageMarkerPool, TestRageMarkerPool>();
            services.AddSingleton<IMarkerPool>(provider => provider.GetRequiredService<ITestRageMarkerPool>());

            services.AddSingleton<ITestTextLabelPool, TestTextLabelPool>();
            services.AddSingleton<ITextLabelPool>(provider => provider.GetRequiredService<ITestTextLabelPool>());

        }
    }
}
