﻿using GTANetworkAPI;
using SunCity.Common.RageMp.Pools;
using SunCity.Common.RageMp.Pools.Blips;
using SunCity.IntegrationTests.RageMp.Pools.Entity;

namespace SunCity.IntegrationTests.RageMp.Pools.Blips
{
    internal class TestRageBlipPool : TestRageEntityPool<IRageBlip>, ITestRageBlipPool
    {
        public IRageBlip New(uint sprite, Vector3 position, float scale, byte color, string name = "", byte alpha = RageMpConsts.Alpha, float drawDistance = RageMpConsts.BlipDrawDistance, bool shortRange = false,
            short rotation = 0, uint dimension = RageMpConsts.GlobalDimension)
        {
            return Add(new TestRageBlip(GetCreateIndex()));
        }
    }
}