﻿using GTANetworkAPI;
using SunCity.Common.RageMp.Pools.Blips;
using SunCity.IntegrationTests.RageMp.Pools.Entity;

namespace SunCity.IntegrationTests.RageMp.Pools.Blips
{
    internal class TestRageBlip : TestRageEntity, IRageBlip
    {
        public TestRageBlip(ushort id)
        {
            Id = id;
        }

        public override ushort Id { get; }
        public override EntityType Type { get; } = EntityType.Blip;

        public int Color { get; set; }
        public int Transparency { get; set; }
        public uint Sprite { get; set; }
        public float Scale { get; set; }
        public bool ShortRange { get; set; }
        public bool RouteVisible { get; set; }
        public int RouteColor { get; set; }
        public string Name { get; set; }
    }
}