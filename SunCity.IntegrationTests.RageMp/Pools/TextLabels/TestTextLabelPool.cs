﻿using System.Collections.Generic;
using GTANetworkAPI;
using SunCity.Common.RageMp.Pools;
using SunCity.Common.RageMp.Pools.TextLabels;
using SunCity.IntegrationTests.RageMp.Pools.Entity;

namespace SunCity.IntegrationTests.RageMp.Pools.TextLabels
{
    internal class TestTextLabelPool : TestRageEntityPool<IRageTextLabel>, ITestTextLabelPool
    {
        public IRageTextLabel New(Vector3 position, string text, byte font, Color color, float drawDistance = RageMpConsts.TextLabelDrawDistance, bool los = false, uint dimension = RageMpConsts.GlobalDimension)
        {
            return Add(new TestRageTextLabel(GetCreateIndex()));
        }
    }
}