﻿using GTANetworkAPI;
using SunCity.Common.RageMp.Pools.TextLabels;
using SunCity.IntegrationTests.RageMp.Pools.Entity;

namespace SunCity.IntegrationTests.RageMp.Pools.TextLabels
{
    internal class TestRageTextLabel : TestRageEntity, IRageTextLabel
    {
        public override ushort Id { get; }
        public override EntityType Type => EntityType.TextLabel;

        public string Text { get; set; }
        public Color Color { get; set; }
        public bool Seethrough { get; set; }
        public float Range { get; set; }

        public TestRageTextLabel(ushort id)
        {
            Id = id;
        }


    }
}