﻿using SunCity.Common.RageMp.Pools.Players;

namespace SunCity.IntegrationTests.RageMp.Pools.Player
{
    public interface ITestPlayerPool : IPlayerPool
    {
        IRagePlayer Create();
    }
}