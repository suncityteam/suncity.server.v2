﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using GTANetworkAPI;
using SunCity.Common.RageMp.Pools.Players;
using SunCity.Common.RageMp.Pools.Vehicles;
using SunCity.IntegrationTests.RageMp.Pools.Entity;

namespace SunCity.IntegrationTests.RageMp.Pools.Player
{
    internal class TestRagePlayer : TestRageEntity, IRagePlayer
    {
        private Animation PlayedAnimation { get; set; }
        private List<string> ChatMessages { get; } = new List<string>(128);
        private List<string> Notifications { get; } = new List<string>(128);

        public override ushort Id { get; }
        public override EntityType Type => EntityType.Player;

        public Vector3 Velocity { get; set; }
        public IRageVehicle Vehicle { get; private set; }
        public bool IsInVehicle => Vehicle != null;

        public int VehicleSeat { get; private set; }
        public string Name { get; set; }
        public int Health { get; set; }
        public int Armor { get; set; }

        public string Serial { get; }
        public string SocialClubName { get; }

        public TestRagePlayer(ushort id)
        {
            Id = id;
            Serial = Guid.NewGuid().ToString();
            SocialClubName = Guid.NewGuid().ToString();
        }

        public class Animation
        {
            public Animation(string dictionary, string animationName, int flags)
            {
                Dictionary = dictionary;
                AnimationName = animationName;
                Flags = flags;
            }

            public int Flags { get; }
            public string AnimationName { get; }
            public string Dictionary { get; }
        }

        public void SendChatMessage(string message)
        {
            ChatMessages.Add(message);
        }

        public void SendNotification(string message, bool flashing = true)
        {
            Notifications.Add(message);
        }

        public void SetSkin(PedHash newSkin)
        {
            Model = (ushort) newSkin;
        }

        public void PlayAnimation(string animDict, string animName, int flag)
        {
            PlayedAnimation = new Animation(animDict, animName, flag);
        }

        public void StopAnimation()
        {
            PlayedAnimation = null;
        }

        public void ClearDecorations()
        {

        }

        public void TriggerEvent(string eventName, params object[] args)
        {

        }

        public void Kick(string reason)
        {
            Delete();
        }


        public void SetIntoVehicle(IRageVehicle vehicle, int seat)
        {
            Vehicle = vehicle;
            VehicleSeat = seat;
        }

        public void RemoveAllWeapons()
        {
        }

        public void GiveWeapon(WeaponHash weaponHash, int ammo)
        {
        }

        public void Spawn(Vector3 position, float rotationZ)
        {
            Position = position;
            Rotation = new Vector3(0, 0, rotationZ);

            VehicleSeat = 0;
            Vehicle = null;
            Health = 100;
            StopAnimation();
        }


    }
}