﻿using System;
using SunCity.Common.RageMp.Pools.Players;
using SunCity.IntegrationTests.RageMp.Pools.Entity;

namespace SunCity.IntegrationTests.RageMp.Pools.Player
{
    internal class TestPlayerPool : TestRageEntityPool<IRagePlayer>, ITestPlayerPool
    {
        public IRagePlayer Convert(GTANetworkAPI.Player player)
        {
            throw new NotImplementedException();
        }

        public IRagePlayer Create()
        {
            return Add(new TestRagePlayer(GetCreateIndex()));
        }
    }
}