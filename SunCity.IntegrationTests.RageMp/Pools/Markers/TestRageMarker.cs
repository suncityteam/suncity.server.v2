﻿using GTANetworkAPI;
using SunCity.Common.RageMp.Pools.Markers;
using SunCity.IntegrationTests.RageMp.Pools.Entity;

namespace SunCity.IntegrationTests.RageMp.Pools.Markers
{
    internal class TestRageMarker : TestRageEntity, IRageMarker
    {
        public override ushort Id { get; }
        public override EntityType Type => EntityType.Marker;
        public uint MarkerType { get; set; }
        public float Scale { get; set; }
        public Vector3 Direction { get; set; }
        public Color Color { get; set; }

        public TestRageMarker(ushort id)
        {
            Id = id;
        }
    }
}