﻿using GTANetworkAPI;
using SunCity.Common.RageMp.Pools;
using SunCity.Common.RageMp.Pools.Markers;
using SunCity.IntegrationTests.RageMp.Pools.Entity;

namespace SunCity.IntegrationTests.RageMp.Pools.Markers
{
    internal class TestRageMarkerPool : TestRageEntityPool<IRageMarker>, ITestRageMarkerPool
    {
        public IRageMarker New(MarkerType type, Vector3 position, Vector3 rotation, Vector3 direction, float scale, Color color, bool visible, uint dimension = RageMpConsts.GlobalDimension)
        {
            return Add(new TestRageMarker(GetCreateIndex()));
        }
    }
}