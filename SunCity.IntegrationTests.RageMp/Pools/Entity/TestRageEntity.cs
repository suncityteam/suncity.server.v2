﻿using System;
using System.Collections.Concurrent;
using GTANetworkAPI;
using SunCity.Common.RageMp.Pools.Entity;

namespace SunCity.IntegrationTests.RageMp.Pools.Entity
{
    internal abstract class TestRageEntity : IRageEntity
    {
        protected TestRageEntity()
        {
            Exists = true;
        }

        private ConcurrentDictionary<string, object> Data { get; } = new ConcurrentDictionary<string, object>(Environment.ProcessorCount, 32);
        private ConcurrentDictionary<string, object> SharedData { get; } = new ConcurrentDictionary<string, object>(Environment.ProcessorCount, 32);

        public NetHandle NetHandle => new NetHandle(Id, Type);

        public abstract ushort Id { get; }
        public bool Exists { get; private set; }
        public abstract EntityType Type { get; }
        public uint Model { get; protected set; }
        public Vector3 Position { get; set; }
        public Vector3 Rotation { get; set; }
        public uint Dimension { get; set; }

        public void Delete()
        {
            Exists = false;
        }

        public T GetData<T>(string key)
        {
            if (Data.TryGetValue(key, out var obj))
            {
                return obj is T value ? value : default;
            }

            return default(T);
        }

        public void SetData<T>(string key, T value)
        {
            Data.TryAdd(key, value);
        }

        public bool ResetData(string key)
        {
            return Data.TryRemove(key, out var _);
        }

        public void ResetData()
        {
            Data.Clear();
        }

        public bool HasData(string key)
        {
            return Data.ContainsKey(key);
        }


        public void SetSharedData(string key, bool value)
        {
            SharedData.TryAdd(key, value);
        }

        public void SetSharedData(string key, string value)
        {
            SharedData.TryAdd(key, value);
        }

        public void SetSharedData(string key, int value)
        {
            SharedData.TryAdd(key, value);
        }

        public void SetSharedData<TValue>(string key, TValue value)
        {
            SharedData.TryAdd(key, value);
        }

        public T GetSharedData<T>(string key)
        {
            if (SharedData.TryGetValue(key, out var obj))
            {
                return obj is T value ? value : default;
            }

            return default(T);
        }
    }
}