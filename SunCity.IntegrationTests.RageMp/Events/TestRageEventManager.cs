﻿using System.Reflection;
using SunCity.Common.RageMp.Events;

namespace SunCity.IntegrationTests.RageMp.Events
{
    internal class TestRageEventManager : ITestRageEventManager
    {
        public void Register(string eventName, MethodInfo method, object classInstance)
        {
            
        }

        public void Register(string eventName, RageMpEventResponse handler)
        {
        }
    }
}