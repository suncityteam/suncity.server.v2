﻿using System;
using SunCity.Common.RageMp.Tasks;

namespace SunCity.IntegrationTests.RageMp.Events
{
    internal class TestRageTaskManager : IRageMpTaskManager
    {
        public void Schedule(Action action, long delay = 0)
        {
            action();
        }
    }
}