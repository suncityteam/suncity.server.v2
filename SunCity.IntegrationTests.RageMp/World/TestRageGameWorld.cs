﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GTANetworkAPI;
using SunCity.Common.RageMp.World;

namespace SunCity.IntegrationTests.RageMp.World
{
    internal class TestRageGameWorld : IRageMpGameWorld
    {
        private Weather Weather { get; set; }
        private TimeSpan Time { get; set; }

        public void SetWeather(Weather weather)
        {
            Weather = weather;
        }

        public void SetWeather(string weather)
        {
            Weather = (Weather) Enum.Parse(typeof(Weather), weather, true);
        }

        public Weather GetWeather()
        {
            return Weather;
        }

        public void SetTime(int hours, int minutes, int seconds)
        {
            Time = new TimeSpan(hours, minutes, seconds);
        }

        public TimeSpan GetTime()
        {
            return Time;
        }
    }
}