﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Extensions.DependencyInjection;

namespace SunCity.Common.Data
{
    public static class ServiceRecorder
    {
        public static void AddGameDataManager(this IServiceCollection InServiceCollection)
        {
            InServiceCollection.AddSingleton<DataManager>();
        }
    }
}
