﻿using System;
using System.IO;
using System.Text.Json;
using System.Text.Json.Serialization;
using System.Threading.Tasks;
using SunCity.Common.Json;
using SunCity.Common.Json.Converters.AutoNumberToString;

namespace SunCity.Common.Data
{
    public class DataFile<TData>
        where TData : class, new()
    {
        public bool Exist { get; internal set; }
        public string Path { get; internal set; }
        public TData Data { get; internal set; }
    }

    public class DataManager
    {
        private ICommonJsonSerializer JsonSerializer { get; }

        public DataManager(ICommonJsonSerializer InJsonSerializer)
        {
            JsonSerializer = InJsonSerializer;
        }

        public async Task Write<TData>(string InFileName, TData InData) where TData : class, new()
        {
            var filePath = Path.Combine("Data", InFileName);
            var json = JsonSerializer.Serialize(InData);
            await File.WriteAllTextAsync(filePath, json);
        }

        public async Task<DataFile<TData>> Read<TData>(string InFileName) where TData : class, new()
        {
            var filePath = Path.Combine("Data", InFileName);
            var existFile = File.Exists(filePath);
            if (existFile == false)
            {
                return new DataFile<TData>()
                {
                    Data = null,
                    Exist = false,
                    Path = filePath
                };
            }

            var json = await File.ReadAllTextAsync(filePath);
            if (string.IsNullOrWhiteSpace(json))
            {
                return new DataFile<TData>()
                {
                    Data = null,
                    Exist = false,
                    Path = filePath
                };
            }

            var data = JsonSerializer.Deserialize<TData>(json);
            return new DataFile<TData>
            {
                Path = InFileName,
                Exist = true,
                Data = data
            };
        }
    }
}
