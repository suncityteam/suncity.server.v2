﻿using System;

namespace SunCity.Game.World.Settings
{
    public class GameWorldSettings
    {
        /// <summary>
        /// <para>Длительность серверной минуты</para>
        /// <para>Например, значение 20: 1 минута на сервере - 20 секунд в реальности</para>
        /// </summary>
        public TimeSpan ServerMinuteDuration { get; set; }
        public TimeSpan WeatherUpdateFrequency { get; set; }
    }
}
