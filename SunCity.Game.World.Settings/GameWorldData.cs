﻿using System;
using System.Threading.Tasks;
using GTANetworkAPI;

namespace SunCity.Game.World.Settings
{
    public class GameWorldTimeData
    {
        public const string FileName = "GameWorldTime.json";
        public TimeSpan Time { get; set; }
    }

    public class GameWorldWeatherData
    {
        public const string FileName = "GameWorldWeather.json";
        public Weather Weather { get; set; }
    }
}