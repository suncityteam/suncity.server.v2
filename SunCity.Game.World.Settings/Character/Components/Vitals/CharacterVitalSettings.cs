﻿using System.Threading.Tasks;

namespace SunCity.Game.World.Settings.Character.Components.Vitals
{
    public class CharacterVitalSettings
    {
        public CharacterVitalProperties[] Hunger { get; set; }
        public CharacterVitalProperties[] Thirst { get; set; }
    }
}