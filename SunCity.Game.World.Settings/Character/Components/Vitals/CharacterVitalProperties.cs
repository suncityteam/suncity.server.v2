﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SunCity.Game.World.Settings.Character.Components.Vitals
{
    public class CharacterVitalProperties
    {
        public int Max { get; set; } = 60;
        public int Min { get; set; } = 0;
        public int Give { get; set; } = 1;
        public int Warning { get; set; } = 20;
        public int Critical { get; set; } = 10;
        public int DamageOnCritical { get; set; } = 5;
        public int DamageOnWarning { get; set; } = 1;

        public CharacterVitalProperties() { }

        public CharacterVitalProperties
        (
            int InMax,
            int InMin,
            int InGive,
            int InWarning,
            int InCritical,
            int InDamageOnCritical,
            int InDamageOnWarning
        )
        {
            Max              = InMax;
            Min              = InMin;
            Give             = InGive;
            Warning          = InWarning;
            Critical         = InCritical;
            DamageOnCritical = InDamageOnCritical;
            DamageOnWarning  = InDamageOnWarning;
        }
    }
}
