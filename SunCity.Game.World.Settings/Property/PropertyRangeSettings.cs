﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SunCity.Game.World.Settings.Property
{
    public class PropertyRangeSettings
    {
        public double ActionDistance { get; set; } = 10.0;
    }
}
