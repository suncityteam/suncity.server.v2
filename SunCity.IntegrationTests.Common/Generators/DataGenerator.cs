﻿using System;
using System.Linq;
using SunCity.Common.Extensions;

namespace SunCity.IntegrationTests.Common.Generators
{
    public static class DataGenerator
    {
        public static Random Random { get; } = new Random();

        public static string LargeNumericVariations => new string(RandomNumericStringV1(128).Shuffle().ToArray());
        public static string Large { get; } = "QAZwsxEDCrfvTGByhnUJMujmIK<1232%$%#^@#QAZwsxEDCrfvTGByhnUJMujmIK@#FDSLLSS::EQAZwsxEDCrfvTGByhnUJMujmIKEPPPWQEQWQAZwsxEDCrfvTGByhnUJMujmIKQAZwsxEDCrfvTGByhnUJMujmIK";

        private static string Numeric { get; } = "0123456789";
        private static string Alpha { get; } = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        private static string Chars { get; } = Alpha + Numeric;


        public static string RandomZipCode()
        {
            return Random.Next(100000, 9999999).ToString();
        }

        public static string RandomNumericStringV1(int length)
        {
            return new string(Enumerable.Repeat(Numeric, length)
                .Select(s => s[Random.Next(s.Length)]).ToArray());
        }

        public static string RandomNumericStringV2(int length)
        {
            return new string(Enumerable.Repeat(LargeNumericVariations, length)
                .Select(s => s[Random.Next(s.Length)]).ToArray());
        }

        public static string RandomString(int length)
        {
            return new string(Enumerable.Repeat(Chars, length)
                .Select(s => s[Random.Next(s.Length)]).ToArray());
        }

        public static int RandomNumeric(int min, int max)
        {
            return Random.Next(min, max);
        }

        public static int RandomNumeric(int max)
        {
            return Random.Next(max);
        }

        public static string RandomText(int length)
        {
            return new string(Enumerable.Repeat(Alpha, length)
                .Select(s => s[Random.Next(s.Length)]).ToArray());
        }

        public static string RandomPatientIdentifier()
        {
            return $"{Random.Next(100, 999)}-{Random.Next(100, 999)}-{Random.Next(100, 999)}-{Random.Next(100, 999)}";
        }

        public static string RandomPatientIdentifier(int prefix)
        {
            return $"{prefix}-{Random.Next(100, 999)}-{Random.Next(100, 999)}-{Random.Next(100, 999)}";
        }
    }
}