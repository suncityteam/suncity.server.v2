﻿using System;

namespace SunCity.IntegrationTests.Common.Generators
{
    public static class PhoneNumberGenerators
    {
        public static Random Random { get; } = new Random();
        public static string GenerateRandomPhoneNumber()
        {
            return $"+1{Random.Next(100, 999)}-{Random.Next(100, 999)}-{Random.Next(100, 999)}-{Random.Next(100, 999)}";
        }
    }
}