﻿using System.Collections.Generic;
using System.Text;
using SunCity.Common.Extensions;

namespace SunCity.IntegrationTests.Common.Generators
{
    public static class NameGenerators
    {
        public static string Empty { get; } = string.Empty;

        public static string[] FirstNames { get; } = new[]
        {
            "Oliver", "Jake", "Jack", "Connor", "Harry", "Jacob", "Kyle", "Thomas", "Mia", "Ava",
            "Alastair", "Alistair", "Alister", "Zyana", "Adam", "Ben", "Martin", "Gordon", "Jerry",
            "Fergus", "Euan", "Ewan", "Lachlan", "Sadek", "Kevin", "Brad", "Edgar", "Annie",
            "Crispin", "Kenzie", "Gladys", "Rylee", "Sadeek", "Owen", "Tom", "Jenny", "Freddy",
            "Ellis", "Piers", "Conall", "Clyve", "Rycroft", "Caleb", "Raymond", "Jessie", "David", "Dan", "Maris",
            "Cnidel", "Connal", "Marilyn", "Marisol", "Mark", "Marsten", "Mary", "Menw", "Bart", "Key", "Keily", "Esh",
            "Cnut", "Olivia", "Amelia", "Poppy", "Ella", "Ellie", "Alice", "Samuel", "Archie", "Austin", "Theodore"
        };

        public static string[] LastNames { get; } = new[]
        {
            "Smith", "Murphy", "Brown", "Walsh", "Davies",
            "Wilson", "Byrne", "Davis", "Wang", "Li", "Freddy", "Jackie", "Nicky", "Gilbert", "Jackson",
            "Bertie", "Danny", "Adamson", "Aldridge", "Evans", "Johnson", "Davies", "Parson", "Ellington", "Flatcher",
            "Taylor", "Walker", "Harris", "Lewis", "Young", "Moore", "Phillips", "Hill", "Morgan", "Turner", "Edwards", "Green"
        };

        public static string UniqueFirstName()
        {
            return FirstNames.TakeRandom() + DataGenerator.RandomText(4).ToLowerInvariant();
        }

        public static string UniqueLastName()
        {
            return LastNames.TakeRandom() + DataGenerator.RandomText(4).ToLowerInvariant();
        }

        public static string FullName()
        {
            return $"{FirstNames.TakeRandom()} {LastNames.TakeRandom()}";
        }
    }
}