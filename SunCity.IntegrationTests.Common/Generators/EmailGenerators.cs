﻿using System;

namespace SunCity.IntegrationTests.Common.Generators
{
    public static class EmailGenerators
    {
        public static string Empty { get; } = string.Empty;
        public static string VeryShortEmail { get; } = "z@g.g";

        public static string GenerateRandomEmail(int length)
        {
            return $"{DataGenerator.RandomString(length)}@gmail.com";
        }

        public static string GenerateRandomEmail()
        {
            return $"{Guid.NewGuid():N}@gm{DateTime.UtcNow.Ticks % 1000}a{DateTime.UtcNow.Ticks % 10000}.com";
        }
    }
}