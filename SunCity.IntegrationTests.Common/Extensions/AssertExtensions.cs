﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using SunCity.Common.HttpClient;
using SunCity.Common.Operation;

namespace SunCity.IntegrationTests.Common.Extensions
{
    public static class AssertExtensions
    {
        public static void AssertIsNotNullAndEmpty<T>(this IReadOnlyList<T> collection, string errorMessage)
        {
            Assert.IsNotNull(collection, $"{errorMessage} | Collection is null");
            Assert.IsNotEmpty(collection, $"{errorMessage} | Collection is null");
        }

        public static void AssertIsCorrect<T>(this OperationResponse<T> response, string errorMessage)
        {
            Assert.IsNotNull(response, $"{errorMessage} | Invalid response");
            Assert.IsTrue(response.IsCorrect, $"{errorMessage} | Bad response: {response.Error}");

            Assert.IsNotNull(response.Content, $"{errorMessage} | Invalid response, content is null");
            Assert.IsNull(response.Error, $"{errorMessage} | Invalid response, exist error: {response.Error}");
        }

        public static void AssertIsCorrect(this OperationResponse response, string errorMessage)
        {
            Assert.IsNotNull(response, $"{errorMessage} | Invalid response");
            Assert.IsTrue(response.IsCorrect, $"{errorMessage} | Bad response: {response.Error}");
            Assert.IsNull(response.Error, $"{errorMessage} | Invalid response, exist error: {response.Error}");
        }

        public static void AssertIsNotCorrect(this HttpClientResponse<OperationResponse> response, OperationResponseCode responseCode, string errorMessage)
        {
            Assert.IsNotNull(response, $"{errorMessage} | Invalid response");
            Assert.IsTrue(response.IsSuccessful, $"{errorMessage} | Invalid response | StatusCode: {response.StatusCode} | Content: {response.Content}");

            Assert.IsNotNull(response.Response.Error, $"{errorMessage} | Invalid response | StatusCode: {response.StatusCode} | Content: {response.Content}");
            Assert.AreEqual(responseCode, response.Response.Error.Code, $"{errorMessage} | Invalid response");
        }

        public static void AssertIsCorrect<T>(this HttpClientResponse<OperationResponse<T>> response, string errorMessage)
        {
            Assert.IsNotNull(response, $"{errorMessage} | Invalid response");
            Assert.IsTrue(response.IsSuccessful, $"{errorMessage} | Invalid response | StatusCode: {response.StatusCode} | Content: {response.Content}");
            response.Response.AssertIsCorrect(errorMessage);
        }

        public static void AssertIsCorrect(this HttpClientResponse<OperationResponse> response, string errorMessage)
        {
            Assert.IsNotNull(response, $"{errorMessage} | Invalid response");
            Assert.IsTrue(response.IsSuccessful, $"{errorMessage} | Invalid response | StatusCode: {response.StatusCode} | Content: {response.Content}");
            response.Response.AssertIsCorrect(errorMessage);
        }
    }
}