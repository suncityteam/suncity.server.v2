﻿using System.Linq;
using Microsoft.Extensions.DependencyInjection;
using NUnit.Framework;

namespace SunCity.Common.Extensions
{
    public static class ServicesExtensions
    {
        public static void ReplaceService<TInterface, TImpl>(this IServiceCollection services)
            where TImpl : class, TInterface
            where TInterface : class
        {
            var interfaceDescriptor = services.FirstOrDefault(q => q.ServiceType == typeof(TInterface));
            Assert.NotNull(interfaceDescriptor, $"Failed get base {typeof(TInterface).Name}");

            var removed = services.Remove(interfaceDescriptor);
            Assert.IsTrue(removed, $"Failed remove base {typeof(TInterface).Name}");

            services.AddSingleton<TInterface, TImpl>();
        } 
        
        public static void ReplaceService<TInterface>(this IServiceCollection services, TInterface instance)
            where TInterface : class
        {
            var interfaceDescriptor = services.FirstOrDefault(q => q.ServiceType == typeof(TInterface));
            Assert.NotNull(interfaceDescriptor, $"Failed get base {typeof(TInterface).Name}");

            var removed = services.Remove(interfaceDescriptor);
            Assert.IsTrue(removed, $"Failed remove base {typeof(TInterface).Name}");

            services.AddSingleton(instance);
        }
    }
}