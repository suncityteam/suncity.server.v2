﻿using SunCity.Domain.Interfaces.Entity;

namespace SunCity.Game.World.Models.Licenses
{
    public class LicenseModel : IEntityWithId<LicenseId>
    {
        public LicenseId EntityId { get; set; }
        public string Name { get; set; }

    }
}