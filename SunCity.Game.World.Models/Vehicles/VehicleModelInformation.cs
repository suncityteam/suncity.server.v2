﻿using GTANetworkAPI;
using SunCity.Common.Enum;
using SunCity.Game.Enums.Vehicle;
using SunCity.Game.Enums.World.Property;

namespace SunCity.Game.World.Models.Vehicles
{
    public class VehicleModelInformation
    {
        /// <summary>
        /// <para>Модель, название</para>
        /// </summary>
        public EnumDescription<VehicleHash> Model { get; set; }
        
        /// <summary>
        /// <para>Категория</para>
        /// </summary>
        public EnumDescription<VehicleModelClass> Category { get; set; }
        
        /// <summary>
        /// <para>Класс автомобиля</para>
        /// </summary>
        public EnumDescription<WorldPropertyClass> Class { get; set; }
        
        /// <summary>
        /// <para>Тип топлива</para>
        /// </summary>
        public EnumDescription<VehicleFuelType> FuelType { get; set; }
        
        /// <summary>
        /// <para>Управляемость</para>
        /// </summary>
        public int Handleability { get; set; }
        
        /// <summary>
        /// <para>Вместимость багажника</para>
        /// </summary>
        public int BagCapacity { get; set; }
        
        /// <summary>
        /// <para>Количество мест</para>
        /// </summary>
        public int NumberOfSeats { get; set; }
        
        /// <summary>
        /// <para>Разгон 0-100 за N сек</para>
        /// </summary>
        public float Acceleration { get; set; }
        
        /// <summary>
        /// <para>Максимальная скорость в км/ч</para>
        /// </summary>
        public int MaxSpeed { get; set; }
        
        /// <summary>
        /// <para>Объем бензобазка</para>
        /// </summary>
        public float FuelTankSize { get; set; }
        
        /// <summary>
        /// <para>Расход топлива N литров / минуту </para>
        /// </summary>
        public float FuelConsumptionPerMinute { get; set; } 
    }
}