﻿using System;
using SunCity.Game.World.Enums.Chat;

namespace SunCity.Game.World.Models.Chat
{
    public class WorldChatMessage
    {
        public WorldChatMessaveRecive Recive { get; }
        public string[] Message { get; }
        public float Range { get; }

        public WorldChatMessage(string InMessage, WorldChatMessaveRecive InRecive = WorldChatMessaveRecive.ExcludeSenderPlayer, float InRange = 10.0f)
        {
            Message = new[]
            {
                InMessage
            };

            Recive = InRecive;
            Range = InRange;
        }

        public WorldChatMessage(string[] InMessage, WorldChatMessaveRecive InRecive = WorldChatMessaveRecive.ExcludeSenderPlayer, float InRange = 10.0f)
        {
            Message = InMessage;
            Recive = InRecive;
            Range = InRange;
        }
    }
}
