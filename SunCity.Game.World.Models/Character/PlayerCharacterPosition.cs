﻿using GTANetworkAPI;

namespace SunCity.Game.World.Models.Character
{
    public class PlayerCharacterPosition
    {
        public PropertyId? PropertyId { get; set; }
        public Vector3 Position { get; set; }
        public float Rotation { get; set; }
    }
}