﻿using System;
using System.Collections.Generic;
using System.Text;
using SunCity.Game.Enums.World.Player.Inventory;
using SunCity.Game.World.Models.Item.World;

namespace SunCity.Game.World.Models.Character.Inventory
{
    public class PlayerInventoryItemModel
    {
        public WorldItemId EntityId { get; set; }
        public GameItemId ModelId { get; set; }
        public string Name { get; set; }
        public PlayerInventoryItemModel[] Child { get; set; }
    }

    public class PlayerInventorySlotModel
    {
        public CharacterInventoryItemId EntityId { get; set; }
        public PlayerInventorySlot InventorySlot { get; set; }
        public PlayerInventoryItemModel Item { get; set; }
    }

    public class PlayerInventoryModel
    {
        public PlayerInventorySlotModel[] Slots { get; set; }
    }
}
