﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SunCity.Game.World.Models.Character
{
    public class CharacterExperience
    {
        public byte Level { get; set; }
        public long Experience { get; set; }

        public override string ToString()
        {
            return $"Level: {(int) Level}, Experience: {Experience}";
        }
    }
}
