﻿using System;
using System.Collections.Generic;
using System.Text;
using SunCity.Domain.Interfaces.Entity;
using SunCity.Game.Types.Player.Entity.Player;
using SunCity.Identity.DataBase.Keys;

namespace SunCity.Game.World.Models.Account
{
    public class PlayerAccountProperties 
        : IEntityWithId<AccountId>
        , IEntityWithName<PlayerAccountLogin>
    {
        public AccountId EntityId { get; set; }
        public PlayerAccountLogin Name { get; set; }
    }
}
