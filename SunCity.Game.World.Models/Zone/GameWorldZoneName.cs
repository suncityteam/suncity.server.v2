﻿using System;
using System.Text;
using SunCity.Common.Types.Math;

namespace SunCity.Game.World.Models.Zone
{
    public class GameWorldZoneName
    {
        public string Name { get; set; }
        public Bounds Bounds { get; set; }

        public override string ToString()
        {
            return $"Name: {Name}, Bounds: {Bounds}";
        }
    }
}
