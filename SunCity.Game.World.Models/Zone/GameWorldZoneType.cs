﻿using System.Threading.Tasks;
using AutoMapper;
using SunCity.Common.Types.Math;

namespace SunCity.Game.World.Models.Zone
{
    public class GameWorldZoneType
    {
        public string Type { get; set; }
        public Bounds Bounds { get; set; }

        public override string ToString()
        {
            return $"Name: {Type}, Bounds: {Bounds}";
        }
    }
}