﻿using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;

namespace SunCity.Game.World.Models.Zone
{
    public class GameWorldZones
    {
        public IReadOnlyCollection<GameWorldZoneName> ZoneNames { get; set; }
        public IReadOnlyCollection<GameWorldZoneType> ZoneTypes { get; set; }

        public GameWorldZones Initialize()
        {
            foreach (var zone in ZoneNames)
            {
                zone.Name = string.Intern(zone.Name);
            }

            return this;
        }
    }
}