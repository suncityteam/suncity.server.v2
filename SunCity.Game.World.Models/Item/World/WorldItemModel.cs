﻿using System;
using System.Collections.Generic;
using System.Text;
using SunCity.Domain.Interfaces.Entity;
using SunCity.Game.World.DataBase.Entitites.Item.World;

namespace SunCity.Game.World.Models.Item.World
{
    public class WorldItemModel : IEntityWithId<WorldItemId>
    {
        public WorldItemId EntityId { get; set; }
        public WorldItemGroup Group { get; set; }
        public GameItemId ModelId { get; set; }
        public DateTime CreatedDate { get; set; }
        public WorldItemModel[] Child { get; set; } = Array.Empty<WorldItemModel>();
        public WorldItemData Data { get; set; }
    }
}
