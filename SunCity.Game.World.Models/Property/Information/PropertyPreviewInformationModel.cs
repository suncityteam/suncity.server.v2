﻿using System.Threading.Tasks;
using AutoMapper;
using SunCity.Game.Enums.World.Property;
using SunCity.Game.Types.Property.Entity.Property;

namespace SunCity.Game.World.Models.Property.Information
{
    public class PropertyPreviewInformationModel
    {
        public PropertyId EntityId { get; set; }
        public PropertyName Name { get; set; }

        public WorldPropertyCategory Category { get; set; }
        public WorldPropertyClass Class { get; set; }

        public string Owner { get; set; }
    }
}