﻿using System;
using System.Collections.Generic;
using System.Text;
using SunCity.Domain.Interfaces.Entity;
using SunCity.Game.Interfaces.Property.Bank;
using SunCity.Game.Types.Cost;
using SunCity.Game.Types.Property.Category.Bank;

namespace SunCity.Game.World.Models.Property.Business.Bank
{
    public class BankCardTariffModel 
        : IBankTariffProperties
        , IEntityWithId<BankCardTariffId>
    {
        public BankCardTariffId EntityId { get; set; }
        public string Name { get; set; }

        public BankTariffFee Deposit { get; set; }
        public BankTariffFee WithDraw { get; set; }
        public BankTariffFee Transfer { get; set; }
        public GameCost MaintenanceCost { get; set; }
        public GameCost TariffCost { get; set; }
    }
}
