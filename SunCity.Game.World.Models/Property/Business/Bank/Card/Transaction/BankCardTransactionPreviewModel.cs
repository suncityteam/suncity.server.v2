﻿using System;
using System.Collections.Generic;
using System.Text;
using SunCity.Domain.Interfaces.Entity;
using SunCity.Game.Types.Player.Entity.Player.Character;
using SunCity.Game.World.Enums.Property.Business.Bank;

namespace SunCity.Game.World.Models.Property.Business.Bank.Card.Transaction
{
    public class BankCardWithOwner
    {
        public PlayerCharacterName PlayerName { get; set; }
        public long BankCardNumber { get; set; }
    }

    public class BankCardTransactionPreviewModel : IEntityWithId<BankCardTransactionId>
    {
        public BankCardTransactionId EntityId { get; set; }
        public BankCardTransactionCategory Category { get; set; }
        public BankCardWithOwner Member { get; set; }

        public DateTime CreatedAt { get; set; }
        public long Amount { get; set; }
    }

    public class BankCardTransactionDetailedModel : BankCardTransactionPreviewModel
    {
        public string Comment { get; set; }
    }
}
