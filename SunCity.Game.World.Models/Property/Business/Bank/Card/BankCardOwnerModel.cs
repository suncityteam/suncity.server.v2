﻿using System.Threading.Tasks;
using AutoMapper;
using SunCity.Game.Types.Player.Entity.Player.Character;

namespace SunCity.Game.World.Models.Property.Business.Bank.Card
{
    public class BankCardOwnerModel
    {
        public BankCardId EntityId { get; set; }
        public long Number { get; set; }
        public PlayerCharacterName OwnerName { get; set; }

        public override string ToString()
        {
            return $"EntityId: {EntityId}, Number: {Number}, Balance: {OwnerName}";
        }
    }
}