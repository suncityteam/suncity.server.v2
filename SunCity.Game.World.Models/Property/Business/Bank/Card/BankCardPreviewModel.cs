﻿using System;
using System.Threading.Tasks;
using AutoMapper;

namespace SunCity.Game.World.Models.Property.Business.Bank.Card
{
    public class BankCardPreviewModel
    {
        public BankCardId EntityId { get; set; }
        public DateTime Validity { get; set; }
        public long Number { get; set; }
        public string Name { get; set; }

        public override string ToString()
        {
            return $"EntityId: {EntityId}, Number: {Number} / {Name}, Validity: {Validity:d}";
        }
    }
}