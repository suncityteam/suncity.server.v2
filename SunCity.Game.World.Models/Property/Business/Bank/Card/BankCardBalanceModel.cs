﻿using System.Threading.Tasks;
using AutoMapper;

namespace SunCity.Game.World.Models.Property.Business.Bank.Card
{
    public class BankCardBalanceModel
    {
        public BankCardId EntityId { get; set; }
        public long Number { get; set; }
        public long Balance { get; set; }

        public override string ToString()
        {
            return $"EntityId: {EntityId}, Number: {Number}, Balance: {Balance}";
        }
    }
}