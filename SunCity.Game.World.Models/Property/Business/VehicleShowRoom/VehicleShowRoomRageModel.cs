﻿using System;
using SunCity.Common.Types.Math;

namespace SunCity.Game.World.Models.Property.Business.VehicleShowRoom
{
    public class VehicleShowRoomRageModel
    {
        public Guid EntityId { get; set; }
        public Vector3D StandPosition { get; set; }
        public Vector3D StandRotation { get; set; }
        public Vector3D StandCamera { get; set; }
    }
}