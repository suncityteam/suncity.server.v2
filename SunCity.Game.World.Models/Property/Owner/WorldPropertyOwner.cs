﻿using SunCity.Game.Enums.World.Property.Owner;
using SunCity.Game.World.DataBase.Entitites.Property;

namespace SunCity.Game.World.Models.Property.Owner
{
    public abstract class WorldPropertyOwner
    {
        public abstract PropertyOwnerGroup Group { get; }
        public PropertyOwnerType Type { get; set; }
        public string Name { get; set; }
    }
}