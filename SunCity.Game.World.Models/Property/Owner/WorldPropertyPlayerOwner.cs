﻿using SunCity.Game.Enums.World.Property.Owner;
using SunCity.Game.World.DataBase.Entitites.Property;

namespace SunCity.Game.World.Models.Property.Owner
{
    public class WorldPropertyPlayerOwner : WorldPropertyOwner
    {
        public override PropertyOwnerGroup Group => PropertyOwnerGroup.Player;
        public PlayerCharacterId CharacterId { get; set; }
    }
}