﻿using SunCity.Game.Enums.World.Property.Owner;
using SunCity.Game.World.DataBase.Entitites.Property;

namespace SunCity.Game.World.Models.Property.Owner
{
    public class WorldPropertyFractionOwner : WorldPropertyOwner
    {
        public override PropertyOwnerGroup Group => PropertyOwnerGroup.Fraction;
        public FractionId FractionId { get; set; }
    }
}