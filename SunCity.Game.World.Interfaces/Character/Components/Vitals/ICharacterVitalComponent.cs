﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using SunCity.Game.World.Settings.Character.Components.Vitals;

namespace SunCity.Game.World.Interfaces.Character.Components.Vitals
{
    public delegate Task OnCharacterVitalDamageDelegate(int InDamage);

    public interface ICharacterVitalComponent
    {
        TimeSpan Interval { get; }

        OnCharacterVitalDamageDelegate OnWarning { get; set; }
        OnCharacterVitalDamageDelegate OnCritical { get; set; }

        DateTime NextCheckDate { get; }
        CharacterVitalProperties Level { get; }

        int Get();
        void Set(int InAmount);
        void Give(int InAmount);
        Task OnProcess();
    }
}
