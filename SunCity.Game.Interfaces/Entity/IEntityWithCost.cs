﻿
using SunCity.Game.Types.Cost;

namespace SunCity.Game.Interfaces.Entity
{
    public interface IEntityWithSellingCost
    {
        GameCost SellingCost { get; set; }
    }

    public interface IEntityWithStateCost
    {
        GameCost StateCost { get; set; }
    }
}