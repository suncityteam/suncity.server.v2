﻿using SunCity.Common.Types.Math;

namespace SunCity.Game.Interfaces.Entity
{
    public interface IEntityWithPosition<out TPosition>
        where TPosition : new()

    {
        TPosition Position { get; }
    }
}