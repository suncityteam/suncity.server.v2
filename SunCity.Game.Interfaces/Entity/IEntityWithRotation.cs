﻿namespace SunCity.Game.Interfaces.Entity
{
    public interface IEntityWithRotation
    {
        float Rotation { get; set; }
    }
}