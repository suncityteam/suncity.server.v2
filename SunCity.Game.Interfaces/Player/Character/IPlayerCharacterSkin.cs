﻿using System.Threading.Tasks;
using AutoMapper;

namespace SunCity.Game.Interfaces.Player.Character
{
    public interface IPlayerCharacterSkin
    {
        //===============================================
        byte FirstHeadShape { get; set; }
        byte SecondHeadShape { get; set; }
        
        byte FirstSkinTone { get; set; }
        byte SecondSkinTone { get; set; }

        float HeadMix { get; set; }
        float SkinMix { get; set; }

        byte HairModel { get; set; }
        byte FirstHairColor { get; set; }
        byte SecondHairColor { get; set; }

        short BeardModel { get; set; }
        short BeardColor { get; set; }

        short ChestModel { get; set; }
        short ChestColor { get; set; }

        short BlemishesModel { get; set; }
        short AgeingModel { get; set; }
        short ComplexionModel { get; set; }
        short SundamageModel { get; set; }
        short FrecklesModel { get; set; }

        byte EyesColor { get; set; }
        short EyebrowsModel { get; set; }
        short EyebrowsColor { get; set; }

        short MakeupModel { get; set; }
        short BlushModel { get; set; }
        short BlushColor { get; set; }
        short LipstickModel { get; set; }
        short LipstickColor { get; set; }


        float NoseWidth { get; set; }
        float NoseHeight { get; set; }
        float NoseLength { get; set; }
        float NoseBridge { get; set; }
        float NoseTip { get; set; }
        float NoseShift { get; set; }
        float BrowHeight { get; set; }
        float BrowWidth { get; set; }
        float CheekboneHeight { get; set; }
        float CheekboneWidth { get; set; }
        float CheeksWidth { get; set; }
        float Eyes { get; set; }
        float Lips { get; set; }
        float JawWidth { get; set; }
        float JawHeight { get; set; }
        float ChinLength { get; set; }
        float ChinPosition { get; set; }
        float ChinWidth { get; set; }
        float ChinShape { get; set; }
        float NeckWidth { get; set; }
    }
}