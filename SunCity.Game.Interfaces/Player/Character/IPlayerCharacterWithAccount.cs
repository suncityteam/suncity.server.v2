﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SunCity.Game.Interfaces.Player.Character
{
    public interface IPlayerCharacterWithAccountId
    {
        Guid PlayerAccountId { get; set; }
    }

    public interface IPlayerCharacterEnteredInProperty
    {
        int? EnteredPropertyId { get; set; }
    }

}
