﻿namespace SunCity.Game.Interfaces.Player.Account
{
    public interface IPlayerAccountWithDonatePoints
    {
        long DonatePoints { get; set; }
    }
}