﻿using System;
using System.Collections.Generic;
using System.Text;
using SunCity.Game.Enums.World.Player.Account;

namespace SunCity.Game.Interfaces.Player.Account
{
    public interface IPlayerAccountWithRole
    {
        PlayerAccountRole AccountRole { get; set; }
    }
}
