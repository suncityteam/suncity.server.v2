﻿using System;
using System.Collections.Generic;
using System.Text;
using SunCity.Game.Enums.World.Property;

namespace SunCity.Game.Interfaces.Property
{
    public interface IPropertyWithCategory
    {
        WorldPropertyCategory PropertyCategory { get; set; }
    }
}
