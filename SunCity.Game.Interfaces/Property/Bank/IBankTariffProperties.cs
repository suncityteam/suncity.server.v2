﻿using System;
using System.Collections.Generic;
using System.Text;
using SunCity.Domain.Interfaces.Entity;
using SunCity.Game.Types.Cost;
using SunCity.Game.Types.Property.Category.Bank;

namespace SunCity.Game.Interfaces.Property.Bank
{
    public interface IBankTariffProperties
    {
        string Name { get; set; }

        //=================================================
        /// <summary>
        /// Комисия за внесение денег
        /// </summary>
        BankTariffFee Deposit { get; set; }

        /// <summary>
        /// Комисия за снятие денег
        /// </summary>
        BankTariffFee WithDraw { get; set; }

        /// <summary>
        /// Комисия за перевод денег
        /// </summary>
        BankTariffFee Transfer { get; set; }

        //=================================================
        /// <summary>
        /// Ежедневное обслуживание
        /// </summary>
        GameCost MaintenanceCost { get; set; }

        /// <summary>
        /// Стоимость тарифа
        /// </summary>
        GameCost TariffCost { get; set; }
    }
}
