﻿namespace SunCity.Game.Interfaces.Property
{
    public interface IPropertyWithSellingState
    {
        bool PropertyIsSelling { get; set; }
    }
}