﻿namespace SunCity.Game.Interfaces.Property
{
    public interface IPropertyWithSquare
    {
        float Square { get; set; }
    }
}