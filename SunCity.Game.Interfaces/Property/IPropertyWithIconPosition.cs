﻿using SunCity.Common.Types.Math;

namespace SunCity.Game.Interfaces.Property
{
    public interface IPropertyWithIconPosition
    {
        Vector3D IconPosition { get; set; }
    }
}