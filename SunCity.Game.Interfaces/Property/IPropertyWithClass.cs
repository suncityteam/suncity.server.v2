﻿
using SunCity.Game.Enums.World.Property;

namespace SunCity.Game.Interfaces.Property
{
    public interface IPropertyWithClass
    {
        WorldPropertyClass PropertyClass { get; set; }
    }
}