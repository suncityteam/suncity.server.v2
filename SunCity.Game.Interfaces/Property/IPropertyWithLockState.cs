﻿namespace SunCity.Game.Interfaces.Property
{
    public interface IPropertyWithLockState
    {
        bool DoorsIsLocked { get; set; }
    }
}