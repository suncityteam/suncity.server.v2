﻿namespace SunCity.Game.Interfaces.Property
{
    public interface IPropertyWithInteriorId
    {
        int? InteriorId { get; set; }
    }
}