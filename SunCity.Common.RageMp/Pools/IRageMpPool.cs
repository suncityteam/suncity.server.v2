﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SunCity.Common.RageMp.Pools.Blips;
using SunCity.Common.RageMp.Pools.Markers;
using SunCity.Common.RageMp.Pools.Players;
using SunCity.Common.RageMp.Pools.TextLabels;
using SunCity.Common.RageMp.Pools.Vehicles;

namespace SunCity.Common.RageMp.Pools
{
    public interface IRageMpPool
    {
        IBlipPool Blips { get; }
        IMarkerPool Markers { get; }
        ITextLabelPool TextLabels { get; }

        IPlayerPool Players { get; }
        IVehiclePool Vehicles { get; }
    }
}
