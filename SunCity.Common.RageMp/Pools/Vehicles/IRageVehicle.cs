﻿using GTANetworkAPI;
using SunCity.Common.RageMp.Pools.Entity;

namespace SunCity.Common.RageMp.Pools.Vehicles
{
    public interface IRageVehicle : IRageEntity
    {
        int PrimaryColor { get; set; }
        int SecondaryColor { get; set; }

        Color CustomPrimaryColor { get; set; }
        Color CustomSecondaryColor { get; set; }

        int DashboardColor { get; set; }
        int TrimColor { get; set; }

        VehiclePaint PrimaryPaint { get; set; }
        VehiclePaint SecondaryPaint { get; set; }

        int PearlescentColor { get; set; }
        int WheelColor { get; set; }
        int WheelType { get; set; }
        int WindowTint { get; set; }

        float Health { get; set; }
        float EnginePowerMultiplier { get; set; }
        float EngineTorqueMultiplier { get; set; }

        int Livery { get; set; }
        bool Siren { get;  }
        bool Neons { get; set; }
        bool SpecialLight { get; set; }
        bool CustomTires { get; set; }
        bool BulletproofTyres { get; set; }
        bool EngineStatus { get; set; }
        bool Locked { get; set; }

        Color TyreSmokeColor { get; set; }
        Color NeonColor { get; set; }

        string NumberPlate { get; set; }
        int NumberPlateStyle { get; set; }

        void Repair();
        void Spawn(Vector3 position, float heading = 0.0f);
    }
}