﻿using GTANetworkAPI;
using RAGE;
using SunCity.Common.RageMp.Pools.Entity;

namespace SunCity.Common.RageMp.Pools.Vehicles
{
    internal sealed class VehiclePool : RageEntityPool<IRageVehicle>, IVehiclePool
    {
        public IRageVehicle New(VehicleHash model, Vector3 pos, float rot, string plateNumber, uint dimension)
        {
            return new RageVehicle(NAPI.Vehicle.CreateVehicle
            (
                model,
                pos,
                rot,
                0,
                0,
                plateNumber,
                255,
                false,
                false,
                dimension
            ));
        }
    }
}