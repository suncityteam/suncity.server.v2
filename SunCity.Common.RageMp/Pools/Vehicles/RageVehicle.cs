﻿using GTANetworkAPI;
using SunCity.Common.RageMp.Pools.Entity;

namespace SunCity.Common.RageMp.Pools.Vehicles
{
    internal class RageVehicle : RageEntityImpl<Vehicle>, IRageVehicle
    {
        public RageVehicle(Vehicle entity) : base(entity)
        {

        }

        public int PrimaryColor
        {
            get => Entity.PrimaryColor;
            set => Entity.PrimaryColor = value;
        }

        public int SecondaryColor
        {
            get => Entity.SecondaryColor;
            set => Entity.SecondaryColor = value;
        }

        public Color CustomPrimaryColor
        {
            get => Entity.CustomPrimaryColor;
            set => Entity.CustomPrimaryColor = value;
        }

        public Color CustomSecondaryColor
        {
            get => Entity.CustomSecondaryColor;
            set => Entity.CustomSecondaryColor = value;
        }

        public int DashboardColor
        {
            get => Entity.DashboardColor;
            set => Entity.DashboardColor = value;
        }

        public int TrimColor
        {
            get => Entity.TrimColor;
            set => Entity.TrimColor = value;
        }

        public VehiclePaint PrimaryPaint
        {
            get => Entity.PrimaryPaint;
            set => Entity.PrimaryPaint = value;
        }

        public VehiclePaint SecondaryPaint
        {
            get => Entity.SecondaryPaint;
            set => Entity.SecondaryPaint = value;
        }

        public int PearlescentColor
        {
            get => Entity.PearlescentColor;
            set => Entity.PearlescentColor = value;
        }

        public int WheelColor
        {
            get => Entity.WheelColor;
            set => Entity.WheelColor = value;
        }

        public int WheelType
        {
            get => Entity.WheelType;
            set => Entity.WheelType = value;
        }

        public int WindowTint
        {
            get => Entity.WindowTint;
            set => Entity.WindowTint = value;
        }

        public float Health
        {
            get => Entity.Health;
            set => Entity.Health = value;
        }

        public float EnginePowerMultiplier
        {
            get => Entity.EnginePowerMultiplier;
            set => Entity.EnginePowerMultiplier = value;
        }

        public float EngineTorqueMultiplier
        {
            get => Entity.EngineTorqueMultiplier;
            set => Entity.EngineTorqueMultiplier = value;
        }

        public int Livery
        {
            get => Entity.Livery;
            set => Entity.Livery = value;
        }

        public bool Siren
        {
            get => Entity.Siren;
        }

        public bool Neons
        {
            get => Entity.Neons;
            set => Entity.Neons = value;
        }

        public bool SpecialLight
        {
            get => Entity.SpecialLight;
            set => Entity.SpecialLight = value;
        }

        public bool CustomTires
        {
            get => Entity.CustomTires;
            set => Entity.CustomTires = value;
        }

        public bool BulletproofTyres
        {
            get => Entity.BulletproofTyres;
            set => Entity.BulletproofTyres = value;
        }

        public bool EngineStatus
        {
            get => Entity.EngineStatus;
            set => Entity.EngineStatus = value;
        }

        public bool Locked
        {
            get => Entity.Locked;
            set => Entity.Locked = value;
        }

        public Color TyreSmokeColor
        {
            get => Entity.TyreSmokeColor;
            set => Entity.TyreSmokeColor = value;
        }

        public Color NeonColor
        {
            get => Entity.NeonColor;
            set => Entity.NeonColor = value;
        }

        public string NumberPlate
        {
            get => Entity.NumberPlate;
            set => Entity.NumberPlate = value;
        }

        public int NumberPlateStyle
        {
            get => Entity.NumberPlateStyle;
            set => Entity.NumberPlateStyle = value;
        }

        public void Repair()
        {
            Entity.Repair();
        }

        public void Spawn(Vector3 position, float heading = 0)
        {
            Entity.Spawn(position, heading);
        }

    }
}