﻿using GTANetworkAPI;
using SunCity.Common.RageMp.Pools.Entity;

namespace SunCity.Common.RageMp.Pools.Vehicles
{
    public interface IVehiclePool : IEntityPool<IRageVehicle>
    {
        IRageVehicle New(VehicleHash model, Vector3 pos, float rot, string plateNumber, uint dimension = RageMpConsts.GlobalDimension);
    }
}