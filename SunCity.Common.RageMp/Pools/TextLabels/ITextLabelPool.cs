﻿using GTANetworkAPI;
using SunCity.Common.RageMp.Pools.Entity;

namespace SunCity.Common.RageMp.Pools.TextLabels
{
    public interface ITextLabelPool : IEntityPool<IRageTextLabel>
    {
        public IRageTextLabel New(Vector3 position, string text, byte font, Color color, float drawDistance = RageMpConsts.TextLabelDrawDistance, bool los = false,
            uint dimension = RageMpConsts.GlobalDimension);
    }
}