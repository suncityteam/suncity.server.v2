﻿using GTANetworkAPI;
using SunCity.Common.RageMp.Pools.Entity;

namespace SunCity.Common.RageMp.Pools.TextLabels
{
    internal class RageTextLabel : RageEntityImpl<TextLabel>, IRageTextLabel
    {
        public string Text
        {
            get => Entity.Text;
            set => Entity.Text = value;
        }

        public Color Color
        {
            get => Entity.Color;
            set => Entity.Color = value;
        }

        public bool Seethrough
        {
            get => Entity.Seethrough;
            set => Entity.Seethrough = value;
        }

        public float Range
        {
            get => Entity.Range;
            set => Entity.Range = value;
        }

        public RageTextLabel(TextLabel entity) : base(entity)
        {
        }
    }
}