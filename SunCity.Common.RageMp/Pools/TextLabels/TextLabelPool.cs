﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GTANetworkAPI;
using RAGE;
using SunCity.Common.RageMp.Pools.Entity;

namespace SunCity.Common.RageMp.Pools.TextLabels
{
    internal sealed class TextLabelPool : RageEntityPool<IRageTextLabel>, ITextLabelPool
    {
        public IRageTextLabel New(Vector3 position, string text, byte font, Color color, float drawDistance, bool los, uint dimension)
        {
            return new RageTextLabel(NAPI.TextLabel.CreateTextLabel
            (
                text,
                position,
                1,
                1,
                font,
                color,
                los,
                dimension
            ));
        }
    }
}