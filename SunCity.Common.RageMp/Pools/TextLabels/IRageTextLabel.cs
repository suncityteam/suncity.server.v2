﻿using GTANetworkAPI;
using SunCity.Common.RageMp.Pools.Entity;

namespace SunCity.Common.RageMp.Pools.TextLabels
{
    public interface IRageTextLabel : IRageEntity
    {
        string Text { get; set; }

        Color Color { get; set; }

        bool Seethrough { get; set; }

        float Range { get; set; }
    }
}