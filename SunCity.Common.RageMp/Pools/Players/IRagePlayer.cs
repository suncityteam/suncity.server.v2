﻿using GTANetworkAPI;
using SunCity.Common.RageMp.Pools.Entity;
using SunCity.Common.RageMp.Pools.Vehicles;

namespace SunCity.Common.RageMp.Pools.Players
{
    public interface IRagePlayer : IRageEntity
    {
        Vector3 Velocity { get; set; }


        IRageVehicle Vehicle { get;  }
        bool IsInVehicle { get; }
        int VehicleSeat { get; }
        string Name { get; set; }

        int Health { get; set; }
        int Armor { get; set; }
        string Serial { get; }
        string SocialClubName { get; }

        void SendChatMessage(string message);
        void SendNotification(string message, bool flashing = true);

        void SetSkin(PedHash newSkin);

        void PlayAnimation(string animDict, string animName, int flag);
        void StopAnimation();

        void ClearDecorations();

        void TriggerEvent(string eventName, params object[] args);
        void Kick(string reason);

        void SetIntoVehicle(IRageVehicle vehicle, int seat);
        void RemoveAllWeapons();
        void GiveWeapon(WeaponHash weaponHash, int ammo);
        void Spawn(Vector3 position, float rotationZ);
    }
}