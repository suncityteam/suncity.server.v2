﻿using GTANetworkAPI;
using SunCity.Common.RageMp.Pools.Entity;
using SunCity.Common.RageMp.Pools.Vehicles;

namespace SunCity.Common.RageMp.Pools.Players
{
    internal sealed class RagePlayerImpl : RageEntityImpl<Player>, IRagePlayer
    {
        public RagePlayerImpl(Player entity) : base(entity)
        {
        }

        public Vector3 Velocity
        {
            get => Entity.Velocity;
            set => Entity.Velocity = value;
        }

        public IRageVehicle Vehicle => new RageVehicle(Entity.Vehicle);
        public bool IsInVehicle => Entity.IsInVehicle;
        public int VehicleSeat => Entity.VehicleSeat;

        public string Name
        {
            get => Entity.Name;
            set => Entity.Name = value;
        }

        public int Health
        {
            get => Entity.Health;
            set => Entity.Health = value;
        }

        public int Armor
        {
            get => Entity.Armor;
            set => Entity.Armor = value;
        }

        public string Serial => Entity.Serial;
        public string SocialClubName => Entity.SocialClubName;

        public void SendChatMessage(string message)
        {
            Entity.SendChatMessage(message);
        }

        public void SendNotification(string message, bool flashing = true)
        {
            Entity.SendNotification(message, flashing);
        }

        public void SetSkin(PedHash newSkin)
        {
            Entity.SetSkin(newSkin);
        }

        public void PlayAnimation(string animDict, string animName, int flag)
        {
            Entity.PlayAnimation(animDict, animName, flag);
        }

        public void StopAnimation()
        {
            Entity.StopAnimation();
        }

        public void ClearDecorations()
        {
            Entity.ClearDecorations();
        }

        public void TriggerEvent(string eventName, params object[] args)
        {
            Entity.TriggerEvent(eventName, args);
        }

        public void Kick(string reason)
        {
            Entity.Kick(reason);
        }

        public void SetIntoVehicle(IRageVehicle vehicle, int seat)
        {
            Entity.SetIntoVehicle(vehicle.NetHandle, seat);
        }

        public void RemoveAllWeapons()
        {
            Entity.RemoveAllWeapons();
        }

        public void GiveWeapon(WeaponHash weaponHash, int ammo)
        {
            Entity.GiveWeapon(weaponHash, ammo);
        }

        public void Spawn(Vector3 position, float rotationZ)
        {
            NAPI.Player.SpawnPlayer(Entity, position, rotationZ);
        }
    }
}