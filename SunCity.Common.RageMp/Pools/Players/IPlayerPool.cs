﻿using GTANetworkAPI;
using SunCity.Common.RageMp.Pools.Entity;

namespace SunCity.Common.RageMp.Pools.Players
{
    public interface IPlayerPool : IEntityPool<IRagePlayer>
    {
        IRagePlayer Convert(Player player);
    }
}
