﻿using System.Collections.Generic;
using System.Linq;
using GTANetworkAPI;
using RAGE;

namespace SunCity.Common.RageMp.Pools.Players
{
    internal sealed class PlayerPool : IPlayerPool
    {
        private EntityPoolBase<Player> Pool { get; }

        public PlayerPool()
        {
            Pool = Entities.Players;
        }

        public IRagePlayer this[ushort id] => GetAt(id);

        public int Count => Pool.Count;
        public IReadOnlyList<IRagePlayer> All => Pool.All.Select(q => Convert(q)).ToArray();

        public IRagePlayer GetAt(ushort id)
        {
            var entity = Pool.GetAt(id);
            return Convert(entity);
        }

        public IRagePlayer Convert(Player entity)
        {
            return new RagePlayerImpl(entity);
        }
    }
}