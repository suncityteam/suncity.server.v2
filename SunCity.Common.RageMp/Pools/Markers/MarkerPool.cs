﻿using GTANetworkAPI;
using SunCity.Common.RageMp.Pools.Entity;

namespace SunCity.Common.RageMp.Pools.Markers
{
    internal sealed class MarkerPool : RageEntityPool<IRageMarker>, IMarkerPool
    {
        public IRageMarker New(MarkerType type, Vector3 position, Vector3 rotation, Vector3 direction, float scale, Color color, bool visible, uint dimension)
        {
            return new RageMarker(NAPI.Marker.CreateMarker
            (
                type,
                position,
                direction,
                rotation,
                scale,
                color,
                false,
                dimension
            ));
        }
    }
}