﻿using GTANetworkAPI;
using SunCity.Common.RageMp.Pools.Entity;

namespace SunCity.Common.RageMp.Pools.Markers
{
    public interface IMarkerPool : IEntityPool<IRageMarker>
    {
        IRageMarker New(MarkerType type, Vector3 position, Vector3 rotation, Vector3 direction, float scale, Color color, bool visible, uint dimension = RageMpConsts.GlobalDimension);
    }
}