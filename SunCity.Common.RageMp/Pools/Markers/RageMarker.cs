﻿using GTANetworkAPI;
using SunCity.Common.RageMp.Pools.Entity;

namespace SunCity.Common.RageMp.Pools.Markers
{
    internal class RageMarker : RageEntityImpl<Marker>, IRageMarker
    {
        public RageMarker(Marker entity) : base(entity)
        {
        }

        public uint MarkerType
        {
            get => Entity.MarkerType;
            set => Entity.MarkerType = value;
        }

        public float Scale
        {
            get => Entity.Scale;
            set => Entity.Scale = value;
        }

        public Vector3 Direction
        {
            get => Entity.Direction;
            set => Entity.Direction = value;
        }

        public Color Color
        {
            get => Entity.Color;
            set => Entity.Color = value;
        }
    }
}