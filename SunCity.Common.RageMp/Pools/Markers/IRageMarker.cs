﻿using GTANetworkAPI;
using SunCity.Common.RageMp.Pools.Entity;

namespace SunCity.Common.RageMp.Pools.Markers
{
    public interface IRageMarker : IRageEntity
    {
        uint MarkerType { get; set; }
        float Scale { get; set; }
        Vector3 Direction { get; set; }
        Color Color { get; set; }
    }
}