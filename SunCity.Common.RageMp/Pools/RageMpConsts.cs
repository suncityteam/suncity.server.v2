﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SunCity.Common.RageMp.Pools
{
    public static class RageMpConsts
    {
        public const float BlipDrawDistance = 10.0f;
        public const float TextLabelDrawDistance = 10.0f;

        public const byte Alpha = byte.MaxValue;
        public const uint GlobalDimension = uint.MaxValue;
    }
}