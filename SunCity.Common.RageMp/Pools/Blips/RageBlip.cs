﻿using GTANetworkAPI;
using SunCity.Common.RageMp.Pools.Entity;

namespace SunCity.Common.RageMp.Pools.Blips
{
    internal class RageBlip : RageEntityImpl<Blip>, IRageBlip
    {
        public RageBlip(Blip entity) : base(entity)
        {
        }

        public int Color
        {
            get => Entity.Color;
            set => Entity.Color = value;
        }

        public int Transparency
        {
            get => Entity.Transparency;
            set => Entity.Transparency = value;
        }

        public uint Sprite
        {
            get => Entity.Sprite;
            set => Entity.Sprite = value;
        }

        public float Scale
        {
            get => Entity.Scale;
            set => Entity.Scale = value;
        }

        public bool ShortRange
        {
            get => Entity.ShortRange;
            set => Entity.ShortRange = value;
        }

        public bool RouteVisible
        {
            get => Entity.RouteVisible;
            set => Entity.RouteVisible = value;
        }

        public int RouteColor
        {
            get => Entity.RouteColor;
            set => Entity.RouteColor = value;
        }

        public string Name
        {
            get => Entity.Name;
            set => Entity.Name = value;
        }
    }
}