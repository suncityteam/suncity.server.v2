﻿using GTANetworkAPI;
using SunCity.Common.RageMp.Pools.Entity;

namespace SunCity.Common.RageMp.Pools.Blips
{
    internal sealed class BlipPool : RageEntityPool<IRageBlip>, IBlipPool
    {
        public IRageBlip New(uint sprite, Vector3 position, float scale, byte color, string name, byte alpha, float drawDistance, bool shortRange, short rotation, uint dimension)
        {
            return new RageBlip( NAPI.Blip.CreateBlip
            (
                sprite,
                position,
                scale,
                color,
                name,
                alpha,
                drawDistance,
                shortRange,
                rotation,
                dimension
            ));
        }
    }
}