﻿using SunCity.Common.RageMp.Pools.Entity;

namespace SunCity.Common.RageMp.Pools.Blips
{
    public interface IRageBlip : IRageEntity
    {
        int Color { get; set; }
        int Transparency { get; set; }
        uint Sprite { get; set; }
        float Scale { get; set; }
        bool ShortRange { get; set; }
        bool RouteVisible { get; set; }
        int RouteColor { get; set; }
        string Name { get; set; }
    }
}