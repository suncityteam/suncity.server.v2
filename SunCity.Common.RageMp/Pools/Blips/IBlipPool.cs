﻿using GTANetworkAPI;
using SunCity.Common.RageMp.Pools.Entity;

namespace SunCity.Common.RageMp.Pools.Blips
{
    public interface IBlipPool : IEntityPool<IRageBlip>
    {
        public IRageBlip New
        (
            uint sprite,
            Vector3 position,
            float scale,
            byte color,
            string name = "",
            byte alpha = RageMpConsts.Alpha,
            float drawDistance = RageMpConsts.BlipDrawDistance,
            bool shortRange = false,
            short rotation = 0,
            uint dimension = RageMpConsts.GlobalDimension
        );
    }
}