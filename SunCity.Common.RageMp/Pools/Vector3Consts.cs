﻿using GTANetworkAPI;

namespace SunCity.Common.RageMp.Pools
{
    public static class Colors
    {
        public static Color White { get; } = new Color(255, 255, 255, 255);
        public static Color DodgerBlue { get; } = new Color(30, 144, 255, 255);
    }

    public static class Vector3Consts
    {
        public static readonly Vector3 Zero = new Vector3(0, 0, 0);
    }
}