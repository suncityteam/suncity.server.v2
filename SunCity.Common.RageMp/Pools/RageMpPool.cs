﻿using System.Reflection.Emit;
using GTANetworkAPI;
using SunCity.Common.RageMp.Pools.Blips;
using SunCity.Common.RageMp.Pools.Markers;
using SunCity.Common.RageMp.Pools.Players;
using SunCity.Common.RageMp.Pools.TextLabels;
using SunCity.Common.RageMp.Pools.Vehicles;

namespace SunCity.Common.RageMp.Pools
{
    public sealed class RageMpPool : IRageMpPool
    {
        public RageMpPool(IBlipPool blips, IMarkerPool markers, ITextLabelPool textLabels, IPlayerPool players, IVehiclePool vehicles)
        {
            Blips = blips;
            Markers = markers;
            TextLabels = textLabels;
            Players = players;
            Vehicles = vehicles;
        }

        public IBlipPool Blips { get; }
        public IMarkerPool Markers { get; }
        public ITextLabelPool TextLabels { get; }
        public IPlayerPool Players { get; }
        public IVehiclePool Vehicles { get; }
    }
}