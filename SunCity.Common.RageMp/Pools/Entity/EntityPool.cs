﻿using System.Collections.Generic;
using RAGE;

namespace SunCity.Common.RageMp.Pools.Entity
{
    internal class EntityPool<TEntity> : IEntityPool<TEntity>
    {
        private EntityPoolBase<TEntity> Pool { get; }

        public EntityPool(EntityPoolBase<TEntity> pool)
        {
            Pool = pool;
        }

        public TEntity this[ushort id] => GetAt(id);

        public int Count => Pool.Count;
        public IReadOnlyList<TEntity> All => Pool.All;

        public TEntity GetAt(ushort id)
        {
            return All[id];
        }
    }
}