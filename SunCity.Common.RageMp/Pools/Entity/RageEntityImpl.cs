﻿using GTANetworkAPI;

namespace SunCity.Common.RageMp.Pools.Entity
{
    internal abstract class RageEntityImpl<TEntity> : IRageEntity
        where TEntity : GTANetworkAPI.Entity
    {
        protected TEntity Entity { get; }

        protected RageEntityImpl(TEntity entity)
        {
            Entity = entity;
        }


        public ushort Id => Entity.Id;
        public bool Exists => Entity.Exists;

        public EntityType Type => Entity.Type;
        public NetHandle NetHandle => Entity.Handle;

        public uint Model => Entity.Model;

        public Vector3 Position
        {
            get => Entity.Position;
            set => Entity.Position = value;
        }

        public Vector3 Rotation
        {
            get => Entity.Rotation;
            set => Entity.Rotation = value;
        }

        public uint Dimension
        {
            get => Entity.Dimension;
            set => Entity.Dimension = value;
        }

        public void Delete()
        {
            Entity.Delete();
        }

        public T GetData<T>(string key)
        {
            return Entity.GetData<T>(key);
        }

        public void SetData<T>(string key, T value)
        {
            Entity.SetData(key, value);
        }

        public bool ResetData(string key)
        {
            return Entity.ResetData(key);
        }

        public void ResetData()
        {
            Entity.ResetData();
        }

        public bool HasData(string key)
        {
            return Entity.HasData(key);
        }

        public T GetSharedData<T>(string key)
        {
            return Entity.GetSharedData<T>(key);
        }

        public void SetSharedData(string key, bool value)
        {
            Entity.SetSharedData(key, value);
        }

        public void SetSharedData(string key, string value)
        {
            Entity.SetSharedData(key, value);
        }

        public void SetSharedData(string key, int value)
        {
            Entity.SetSharedData(key, value);
        }

        public void SetSharedData<TValue>(string key, TValue value)
        {
            Entity.SetSharedData(key, value);
        }
    }
}