﻿using System.Collections.Generic;

namespace SunCity.Common.RageMp.Pools.Entity
{
    public interface IEntityPool<T>
    {
        T this[ushort id] { get; }

        int Count { get; }

        IReadOnlyList<T> All { get; }

        public T GetAt(ushort id);
    }
}