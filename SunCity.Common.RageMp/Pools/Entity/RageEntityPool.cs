﻿using System.Collections.Generic;

namespace SunCity.Common.RageMp.Pools.Entity
{
    public abstract class RageEntityPool<T> : IEntityPool<T>
    {
        private readonly object sync = new object();
        private List<T> Items { get; } = new List<T>(1024);

        public T this[ushort id] => GetAt(id);

        public int Count => Items.Count;
        public IReadOnlyList<T> All => Items;

        protected ushort GetCreateIndex()
        {
            lock (sync)
            {
                return (ushort) Items.Count;
            }
        }

        protected T Add(T entity)
        {
            lock (sync)
            {
                Items.Add(entity);
            }

            return entity;
        }

        public T GetAt(ushort id)
        {
            return Items[id];
        }
    }
}