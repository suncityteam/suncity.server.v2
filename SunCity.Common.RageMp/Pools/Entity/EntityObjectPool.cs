﻿using System;
using System.Collections.Generic;
using System.Linq;
//using Microsoft.Extensions.ObjectPool;
//using RAGE;

//namespace SunCity.Common.RageMp.Pools.Entity
//{
//    public interface IEntityPooledObject<out T> : IDisposable
//    {
//        T Value { get; }
//    }

//    internal class EntityPooledObject<T> : IEntityPooledObject<T> where T : class
//    {
//        private ObjectPool<T> Pool { get; }
//        public T Value { get; }

//        public EntityPooledObject(ObjectPool<T> pool, T value)
//        {
//            Pool = pool;
//            Value = value;
//        }

//        public void Dispose()
//        {
//            Pool.Return(Value);
//        }
//    }

//    internal class EntityObjectPool<TRageEntity, TProxyEntity> : IEntityPool<IEntityPooledObject<TProxyEntity>>
//        where TProxyEntity : RageEntityImpl<TRageEntity>, new()
//        where TRageEntity : GTANetworkAPI.Entity
//    {
//        private EntityPoolBase<TRageEntity> Pool { get; }
//        private ObjectPool<TProxyEntity> ObjectPool { get; }

//        public EntityObjectPool(EntityPoolBase<TRageEntity> pool)
//        {
//            Pool = pool;
//            ObjectPool = new DefaultObjectPool<TProxyEntity>(new DefaultPooledObjectPolicy<TProxyEntity>());
//        }

//        public IEntityPooledObject<TProxyEntity> this[ushort id] => GetAt(id);

//        public int Count => Pool.Count;
//        public IReadOnlyList<IEntityPooledObject<TProxyEntity>> All => Pool.All.Select(CreateProxy).ToArray();

//        private IEntityPooledObject<TProxyEntity> CreateProxy(TRageEntity entity)
//        {
//            var proxy = ObjectPool.Get();
//            //proxy.Entity = entity;
//            return new EntityPooledObject<TProxyEntity>(ObjectPool, proxy);
//        }

//        public IEntityPooledObject<TProxyEntity> GetAt(ushort id)
//        {
//            var entity = Pool.GetAt(id);
//            return CreateProxy(entity);
//        }
//    }
//}