﻿using GTANetworkAPI;

namespace SunCity.Common.RageMp.Pools.Entity
{
    public interface IRageEntity
    {
        NetHandle NetHandle { get; }

        ushort Id { get; }
        bool Exists { get; }
        EntityType Type { get; }
        uint Model { get; }


        Vector3 Position { get; set; }
        Vector3 Rotation { get; set; }
        uint Dimension { get; set; }

        void Delete();

        #region Own data

        T GetData<T>(string key);
        void SetData<T>(string key, T value);
        bool ResetData(string key);
        void ResetData();
        bool HasData(string key);

        #endregion

        #region SharedData

        T GetSharedData<T>(string key);
        void SetSharedData(string key, bool value);
        void SetSharedData(string key, string value);
        void SetSharedData(string key, int value);
        void SetSharedData<TValue>(string name, TValue value);

        #endregion
    }
}