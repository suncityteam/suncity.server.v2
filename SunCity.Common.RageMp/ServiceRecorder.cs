﻿using System;
using Microsoft.Extensions.DependencyInjection;
using SunCity.Common.RageMp.Events;
using SunCity.Common.RageMp.Pools;
using SunCity.Common.RageMp.Pools.Blips;
using SunCity.Common.RageMp.Pools.Markers;
using SunCity.Common.RageMp.Pools.Players;
using SunCity.Common.RageMp.Pools.TextLabels;
using SunCity.Common.RageMp.Pools.Vehicles;
using SunCity.Common.RageMp.Server;
using SunCity.Common.RageMp.Tasks;
using SunCity.Common.RageMp.World;

namespace SunCity.Common.RageMp
{
    public static class ServiceRecorder
    {
        public static void AddRageMpPools(this IServiceCollection services)
        {
            services.AddSingleton<IRageMpServer, RageMpServer>();
            
            services.AddSingleton<IBlipPool, BlipPool>();

            services.AddSingleton<IMarkerPool, MarkerPool>();
            services.AddSingleton<ITextLabelPool, TextLabelPool>();

            services.AddSingleton<IPlayerPool, PlayerPool>();
            services.AddSingleton<IVehiclePool, VehiclePool>();

            services.AddSingleton<IRageMpPool, RageMpPool>();
            
            services.AddSingleton<IRageMpEventManager, RageMpEventManager>();
            services.AddSingleton<IRageMpTaskManager, RageMpTaskManager>();
            services.AddSingleton<IRageMpGameWorld, RageMpGameWorld>();
        }
    }
}