﻿using System;
using System.Reflection;
using GTANetworkMethods;

namespace SunCity.Common.RageMp.Events
{
    public interface IRageMpEventManager
    {
        void Register(string eventName, MethodInfo method, object classInstance);
        void Register(string eventName, RageMpEventResponse handler);
    }
}