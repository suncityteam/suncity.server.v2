﻿using System.Reflection;
using GTANetworkAPI;

namespace SunCity.Common.RageMp.Events
{
    public delegate void RageMpEventResponse(Player player, object[] args);
    
    internal sealed class RageMpEventManager : IRageMpEventManager
    {
        public void Register(string eventName, MethodInfo method, object classInstance)
        {
            NAPI.ClientEvent.Register(method, eventName, classInstance);
        }

        public void Register(string eventName, RageMpEventResponse handler)
        {
            NAPI.ClientEvent.Register(handler.Method, eventName, handler.Target);
        }
    }
}