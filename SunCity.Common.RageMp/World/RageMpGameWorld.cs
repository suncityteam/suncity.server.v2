﻿using System;
using GTANetworkAPI;

namespace SunCity.Common.RageMp.World
{
    internal class RageMpGameWorld : IRageMpGameWorld
    {
        public void SetWeather(Weather weather)
        {
            NAPI.World.SetWeather(weather);
        }

        public void SetWeather(string weather)
        {
            NAPI.World.SetWeather(weather);
        }

        public Weather GetWeather()
        {
            return NAPI.World.GetWeather();
        }

        public void SetTime(int hours, int minutes, int seconds)
        {
            NAPI.World.SetTime(hours, minutes, seconds);
        }

        public TimeSpan GetTime()
        {
            return NAPI.World.GetTime();
        }
    }
}