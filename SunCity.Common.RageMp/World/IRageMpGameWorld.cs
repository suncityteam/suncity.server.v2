﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GTANetworkAPI;

namespace SunCity.Common.RageMp.World
{
    public interface IRageMpGameWorld
    {
        void SetWeather(Weather weather);
        void SetWeather(string weather);
        Weather GetWeather();

        void SetTime(int hours, int minutes, int seconds);
        public TimeSpan GetTime();
    }
}
