﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SunCity.Common.RageMp.Tasks
{
    public interface IRageMpTaskManager
    {
        void Schedule(Action action, long delay = 0);
    }
}
