﻿using System;
using GTANetworkAPI;

namespace SunCity.Common.RageMp.Tasks
{
    internal class RageMpTaskManager : IRageMpTaskManager
    {
        public void Schedule(Action action, long delay = 0)
        {
            NAPI.Task.Run(action, delay);
        }
    }
}