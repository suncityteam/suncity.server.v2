﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SunCity.Common.RageMp.Enums
{
    public enum VehicleNumberPlateType : byte
    {
        BlueWhite = 0,
        YellowBlack = 1,
        YellowBlue = 2,
        BlueWhite2 = 3,
        BlueWhite3 = 4,
        Yankton = 5,
    }
}
