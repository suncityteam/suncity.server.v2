﻿using SunCity.Common.RageMp.Pools;
using SunCity.Common.RageMp.Tasks;
using SunCity.Common.RageMp.World;

namespace SunCity.Common.RageMp.Server
{
    public interface IRageMpServer
    {
        IRageMpGameWorld World { get; }
        IRageMpTaskManager TaskManager { get; }
        IRageMpPool Pool { get; }
    }

    public class RageMpServer : IRageMpServer
    {
        public IRageMpGameWorld World { get; }
        public IRageMpTaskManager TaskManager { get; }
        public IRageMpPool Pool { get; }
        
        public RageMpServer(IRageMpGameWorld world, IRageMpTaskManager taskManager, IRageMpPool pool)
        {
            World = world;
            TaskManager = taskManager;
            Pool = pool;
        }
    }
}