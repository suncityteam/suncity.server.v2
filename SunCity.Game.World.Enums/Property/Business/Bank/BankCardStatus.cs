﻿using System;
using System.Collections.Generic;
using System.Text;
using SunCity.Common.Enum;

namespace SunCity.Game.World.Enums.Property.Business.Bank
{
    public enum BankCardStatus
    {
        [EnumDescription("Активна")]
        Active,

        [EnumDescription("Закрыта")]
        Closed,

        [EnumDescription("Истекла")]
        Expired,
    }
}
