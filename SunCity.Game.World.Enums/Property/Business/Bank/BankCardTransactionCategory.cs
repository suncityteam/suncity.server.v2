﻿using System.Threading.Tasks;
using SunCity.Common.Enum;

namespace SunCity.Game.World.Enums.Property.Business.Bank
{
    public enum BankCardTransactionCategory
    {
        [EnumDescription("Пополнение счета")]
        Deposit,

        [EnumDescription("Снятие наличных")]
        Withdraw,

        [EnumDescription("Перевод от")]
        TransferFrom,

        [EnumDescription("Перевод для")]
        TransferFor,

        [EnumDescription("Продажа недвижимости для")]
        PropertyPurchaseFrom,

        [EnumDescription("Покупка недвижимости у")]
        PropertyPurchaseFor,

        [EnumDescription("Продажа недвижимости для")]
        VehiclePurchaseFrom,

        [EnumDescription("Покупка недвижимости у")]
        VehiclePurchaseFor,
        
        [EnumDescription("Начисление зарплаты для")]
        PaydayFrom,

        [EnumDescription("Начисление зарплаты от")]
        PaydayFor
    }
}