﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SunCity.Game.World.Enums.Property
{
    [Flags]
    public enum PropertyControlStates
    {
        None,

        /// <summary>
        /// Вход в недвижимость, если на улице
        /// </summary>
        Enter = 1 << 1,

        /// <summary>
        /// Выход из недвижимости, если внутри неее
        /// </summary>
        Exit = 1 << 2,

        /// <summary>
        /// Открыть дверь, если она закрыта
        /// </summary>
        LockDoors = 1 << 3,

        /// <summary>
        /// Закрыть дверь, если она открыта
        /// </summary>
        OpenDoors = 1 << 4,

        /// <summary>
        /// Купить недвижимость, если она продается и я не её владелец
        /// </summary>
        BuyProperty = 1 << 5,

        /// <summary>
        /// Продать недвижимость, если я ее владелец
        /// </summary>
        SellProperty = 1 << 6,

        /// <summary>
        /// Снять с продажи, если я ее владелец и она продается
        /// </summary>
        UnSellProperty = 1 << 7,

        /// <summary>
        /// Управлением недвижимостью, если я ее владелец
        /// </summary>
        Manage = 1 << 8,
    }

    //private PropertyControlStates MapToStates(WorldPlayer InWorldPlayer, WorldProperty InWorldProperty)
    //{
    //    var states = PropertyControlStates.None;
    //
    //    if (InWorldPlayer.EnteredProperty != null)
    //    {
    //        states |= PropertyControlStates.Exit;
    //    }
    //    else
    //    {
    //        states |= PropertyControlStates.Enter;
    //    }
    //
    //    if (InWorldProperty.IsOwner)
    //    {
    //        states |= PropertyControlStates.Manage;
    //
    //        if (InWorldProperty.IsSelling)
    //        {
    //            states |= PropertyControlStates.UnSellProperty;
    //        }
    //        else
    //        {
    //            states |= PropertyControlStates.SellProperty;
    //        }
    //
    //        if (InWorldProperty.Locked)
    //        {
    //            states |= PropertyControlStates.OpenDoors;
    //        }
    //        else
    //        {
    //            states |= PropertyControlStates.LockDoors;
    //        }
    //    }
    //    else
    //    {
    //        if (InWorldProperty.IsSelling)
    //        {
    //            states |= PropertyControlStates.BuyProperty;
    //        }
    //    }
    //    return states;
    //}
}
