﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SunCity.Game.World.Enums.Chat
{
    public enum WorldChatMessaveRecive : byte
    {
        ExcludeSenderPlayer,
        IncludeSenderPlayer
    }
}
