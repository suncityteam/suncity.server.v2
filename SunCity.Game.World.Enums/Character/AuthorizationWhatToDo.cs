﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SunCity.Game.World.Enums.Character
{
    public enum AuthorizationWhatToDo
    {
        CreateCharacter,
        SelectCharacter,
        SpawnCharacter
    }
}
