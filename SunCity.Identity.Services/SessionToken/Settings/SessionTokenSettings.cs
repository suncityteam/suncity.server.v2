﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.IdentityModel.Tokens;
using Newtonsoft.Json;

namespace SunCity.Identity.Services.SessionToken.Settings
{
    public sealed class SessionTokenSettings
    {
        public string SecurityKey { get; set; }
        public string Audience { get; set; }
        public string Issuer { get; set; }
        public TimeSpan Expiration { get; set; }

        [JsonIgnore]
        public SymmetricSecurityKey SymmetricSecurityKey { get; private set; }

        [JsonIgnore]
        public TokenValidationParameters ValidationParameters { get; private set; }

        [JsonIgnore]
        public SigningCredentials SigningCredentials { get; private set; }

        public SessionTokenSettings Configure()
        {
            SymmetricSecurityKey = new SymmetricSecurityKey(Encoding.ASCII.GetBytes(SecurityKey));
            ValidationParameters = new TokenValidationParameters
            {
                IssuerSigningKey = SymmetricSecurityKey,
                ValidAudience    = Audience,
                ValidIssuer      = Issuer,
            };

            SigningCredentials = new SigningCredentials(SymmetricSecurityKey, SecurityAlgorithms.HmacSha256/*, SecurityAlgorithms.RsaSha512Signature*/);
            return this;
        }
    }
}

