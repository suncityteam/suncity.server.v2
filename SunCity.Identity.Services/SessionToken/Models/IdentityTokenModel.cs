﻿using System;
using System.Collections.Generic;
using System.Text;
using SunCity.Identity.DataBase.Keys;

namespace SunCity.Identity.Services.SessionToken.Models
{
    public readonly struct IdentityTokenModel
    {
        public IdentityTokenModel(string InToken, DateTime InExpires)
        {
            Token = InToken;
            Expires = InExpires;
        }

        public string Token { get; }
        public DateTime Expires { get;  }
    }
}
