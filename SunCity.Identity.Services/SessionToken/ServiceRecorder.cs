﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Text;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.IdentityModel.Logging;
using SunCity.Identity.Services.SessionToken.DbQueries;
using SunCity.Identity.Services.SessionToken.Generators;
using SunCity.Identity.Services.SessionToken.Service;
using SunCity.Identity.Services.SessionToken.Settings;
using SunCity.Identity.Services.SessionToken.Validators;


namespace SunCity.Identity.Services.SessionToken
{
    public static class ServiceRecorder
    {
        public static IServiceCollection AddSessionSessionTokenServices(this IServiceCollection InServiceCollection, IConfiguration InConfiguration)
        {
            IdentityModelEventSource.ShowPII = true;


            var sessionTokenSettings = InConfiguration.GetSection(nameof(SessionTokenSettings)).Get<SessionTokenSettings>();
            InServiceCollection.AddSingleton(sessionTokenSettings.Configure());
            InServiceCollection.AddSingleton<JwtSecurityTokenHandler>();

            InServiceCollection.AddScoped<ISessionTokenDbQueries, SessionTokenDbQueries>();
            InServiceCollection.AddScoped<ISessionTokenService, SessionTokenService>();

            InServiceCollection.AddScoped<ISessionTokenGenerator, SessionTokenGenerator>();
            InServiceCollection.AddScoped<ISessionTokenValidator, SessionTokenValidator>();

            return InServiceCollection;
        }
    }
}
