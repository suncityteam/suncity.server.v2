﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Http;
using Microsoft.IdentityModel.Tokens;
using SunCity.Common.Operation;
using SunCity.Identity.Services.SessionToken.Settings;

namespace SunCity.Identity.Services.SessionToken.Validators
{
    internal class SessionTokenValidator : ISessionTokenValidator
    {
        private SessionTokenSettings TokenSettings { get; }
        private JwtSecurityTokenHandler JwtSecurityTokenHandler { get; }

        public SessionTokenValidator(SessionTokenSettings InSessionTokenSettings, JwtSecurityTokenHandler InJwtSecurityTokenHandler)
        {
            TokenSettings           = InSessionTokenSettings;
            JwtSecurityTokenHandler = InJwtSecurityTokenHandler;
        }

        public OperationResponse<ClaimsPrincipal> Validate(IHeaderDictionary InHeaders)
        {

            if (!InHeaders.ContainsKey("Authorization"))
                return OperationResponseCode.Successfully;

            try
            {
                var authorization = InHeaders["Authorization"].ToString();
                var rawtoken = authorization.Remove(0, JwtBearerDefaults.AuthenticationScheme.Length + 1);

                var claims = JwtSecurityTokenHandler.ValidateToken(rawtoken, TokenSettings.ValidationParameters, out var tokenSecure);
                return claims;
            }
            catch (SecurityTokenException e)
            {
                return OperationResponseCode.TokenIsExpired;
            }
        }
    }
}
