﻿using System.Security.Claims;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using SunCity.Common.Operation;

namespace SunCity.Identity.Services.SessionToken.Validators
{
    public interface ISessionTokenValidator
    {
        OperationResponse<ClaimsPrincipal> Validate(IHeaderDictionary InHeaders);
    }
}