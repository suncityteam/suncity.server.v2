﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using JetBrains.Annotations;
using Microsoft.EntityFrameworkCore;
using SunCity.Identity.DataBase;
using SunCity.Identity.DataBase.Entities;
using SunCity.Identity.DataBase.Keys;
using SunCity.Identity.Services.SessionToken.Forms;

namespace SunCity.Identity.Services.SessionToken.DbQueries
{
    internal class SessionTokenDbQueries : ISessionTokenDbQueries
    {
        private IdentityDataBaseContext DbContext { get; }

        public SessionTokenDbQueries(IdentityDataBaseContext InDbContext)
        {
            DbContext = InDbContext;
        }

        public async Task<SessionTokenEntity> Create(CreateSessionTokenDbForm InForm)
        {
            var token = DbContext.SessionTokenEntity.Add(new SessionTokenEntity
            {
                EntityId  = InForm.TokenId,
                AccountId = InForm.AccountId,
                CreatedAt = InForm.CreatedAt,
                ExpiresAt = InForm.ExpiresAt,
                Token     = InForm.Token
            });

            await DbContext.SaveChangesAsync();
            return token.Entity;
        }

        public Task<bool> IsExist(SessionTokenId InSessionTokenId)
        {
            return DbContext.SessionTokenEntity.AnyAsync(q => q.EntityId == InSessionTokenId);
        }

        [ItemCanBeNull]
        public Task<SessionTokenEntity> GetById(SessionTokenId InSessionTokenId)
        {
            return DbContext.SessionTokenEntity.FirstOrDefaultAsync(q => q.EntityId == InSessionTokenId);
        }
    }
}
