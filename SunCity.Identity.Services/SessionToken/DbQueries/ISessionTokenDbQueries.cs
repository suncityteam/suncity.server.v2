﻿using System.Threading.Tasks;
using AutoMapper;
using JetBrains.Annotations;
using SunCity.Identity.DataBase.Entities;
using SunCity.Identity.DataBase.Keys;
using SunCity.Identity.Services.SessionToken.Forms;

namespace SunCity.Identity.Services.SessionToken.DbQueries
{
    public interface ISessionTokenDbQueries
    {
        [ItemNotNull]
        [MustUseReturnValue]
        Task<SessionTokenEntity> Create(CreateSessionTokenDbForm InForm);

        [MustUseReturnValue]
        Task<bool> IsExist(SessionTokenId InSessionTokenId);

        [ItemCanBeNull]
        [MustUseReturnValue]
        Task<SessionTokenEntity> GetById(SessionTokenId InSessionTokenId);
    }
}