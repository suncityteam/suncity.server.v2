﻿using System.Threading.Tasks;
using AutoMapper;
using SunCity.Common.Operation;
using SunCity.Identity.DataBase.Keys;
using SunCity.Identity.Models;
using SunCity.Identity.Services.SessionToken.Forms;

namespace SunCity.Identity.Services.SessionToken.Service
{
    public interface ISessionTokenService
    {
        Task<bool> IsExist(SessionTokenId InSessionTokenId);
        Task<HttpNetworkIdentityToken> Create(CreateSessionTokenForm InForm);

        Task<OperationResponse<AccountId?>> GetAccountIdByTokenId(SessionTokenId InSessionTokenId);
    }
}