﻿using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using SunCity.Common.Operation;
using SunCity.Identity.DataBase.Keys;
using SunCity.Identity.Models;
using SunCity.Identity.Services.SessionToken.DbQueries;
using SunCity.Identity.Services.SessionToken.Forms;
using SunCity.Identity.Services.SessionToken.Generators;

namespace SunCity.Identity.Services.SessionToken.Service
{
    internal sealed class SessionTokenService : ISessionTokenService
    {
        private ISessionTokenDbQueries SessionTokenDbQueries { get; }
        private ISessionTokenGenerator TokenGenerator { get; }
        public SessionTokenService(ISessionTokenDbQueries InSessionTokenDbQueries, ISessionTokenGenerator InTokenGenerator)
        {
            SessionTokenDbQueries = InSessionTokenDbQueries;
            TokenGenerator = InTokenGenerator;
        }

        public async Task<OperationResponse<AccountId?>> GetAccountIdByTokenId(SessionTokenId InSessionTokenId)
        {
            var tokenEntity = await SessionTokenDbQueries.GetById(InSessionTokenId);
            if (tokenEntity == null)
                return OperationResponseCode.TokenNotFound;

            if (DateTime.UtcNow >= tokenEntity.ExpiresAt)
                return OperationResponseCode.TokenIsExpired;

            return tokenEntity.AccountId;
        }

        public Task<bool> IsExist(SessionTokenId InSessionTokenId)
        {
            return SessionTokenDbQueries.IsExist(InSessionTokenId);
        }

        public async Task<HttpNetworkIdentityToken> Create(CreateSessionTokenForm InForm)
        {
            var tokenId = Guid.NewGuid();
            var model = TokenGenerator.GenerateToken(new[]{
                new Claim(ClaimTypes.Sid, tokenId.ToString()),
                new Claim(ClaimTypes.Name, InForm.IdentityName.Name),
                new Claim(ClaimTypes.NameIdentifier, InForm.AccountId.ToString()),
            });

            var entity = await SessionTokenDbQueries.Create(new CreateSessionTokenDbForm()
            {
                TokenId = new SessionTokenId(tokenId),
                AccountId = InForm.AccountId,
                ExpiresAt = model.Expires,
                Token = model.Token,
            });

            return new HttpNetworkIdentityToken(entity.EntityId, entity.AccountId, entity.ExpiresAt, entity.Token);
        }
    }
}
