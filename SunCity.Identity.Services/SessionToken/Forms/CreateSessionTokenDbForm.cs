﻿using System;
using System.Collections.Generic;
using System.Text;
using SunCity.Identity.DataBase.Keys;

namespace SunCity.Identity.Services.SessionToken.Forms
{
    public struct CreateSessionTokenDbForm
    {
        public SessionTokenId TokenId { get; set; }
        public AccountId AccountId { get; set; }

        public string Token { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime ExpiresAt { get; set; }
    }
}
