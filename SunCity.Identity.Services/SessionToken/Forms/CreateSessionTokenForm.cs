﻿using System.Threading.Tasks;
using AutoMapper;
using SunCity.Game.Types.Player.Entity.Player;
using SunCity.Identity.DataBase.Keys;

namespace SunCity.Identity.Services.SessionToken.Forms
{
    public readonly struct CreateSessionTokenForm
    {
        public AccountId AccountId { get; }
        public PlayerAccountLogin IdentityName { get; }

        public CreateSessionTokenForm(AccountId InIdentityId, PlayerAccountLogin InIdentityName)
        {
            AccountId   = InIdentityId;
            IdentityName = InIdentityName;
        }
    }
}