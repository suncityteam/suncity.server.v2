﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using Microsoft.IdentityModel.Logging;
using Microsoft.IdentityModel.Tokens;
using SunCity.Identity.Services.SessionToken.Models;
using SunCity.Identity.Services.SessionToken.Settings;

namespace SunCity.Identity.Services.SessionToken.Generators
{
    internal interface ISessionTokenGenerator
    {
        IdentityTokenModel GenerateToken(IReadOnlyCollection<Claim> InClaims);
    }

    internal class SessionTokenGenerator : ISessionTokenGenerator
    {
        private SessionTokenSettings TokenSettings { get; }
        private JwtSecurityTokenHandler JwtSecurityTokenHandler { get; }

        public SessionTokenGenerator(SessionTokenSettings InSessionTokenSettings, JwtSecurityTokenHandler InJwtSecurityTokenHandler)
        {
            TokenSettings = InSessionTokenSettings;
            JwtSecurityTokenHandler = InJwtSecurityTokenHandler;
        }

        public IdentityTokenModel GenerateToken(IReadOnlyCollection<Claim> InClaims)
        {
            var expiration = DateTime.UtcNow.Add(TokenSettings.Expiration);

            var token = new JwtSecurityToken(
                TokenSettings.Issuer,
                TokenSettings.Audience,
                InClaims,
                expires: expiration,
                signingCredentials: TokenSettings.SigningCredentials);


            var tokenString = JwtSecurityTokenHandler.WriteToken(token);
            return new IdentityTokenModel(tokenString, expiration);
        }

    }
}
