﻿using System;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.IdentityModel.Tokens;
using SunCity.Identity.Services.Authorization;
using SunCity.Identity.Services.SessionToken;
using SunCity.Identity.Services.SessionToken.Settings;

namespace SunCity.Identity.Services
{
    public static class ServiceRecorder
    {
        public static IServiceCollection AddIdentityServices(this IServiceCollection InServiceCollection, IConfiguration InConfiguration)
        {
            InServiceCollection.AddAuthorizationService(InConfiguration);
            InServiceCollection.AddSessionSessionTokenServices(InConfiguration);

            return InServiceCollection;
        }

        public static IServiceCollection AddJWTAuthentication(this IServiceCollection InServiceCollection, IConfiguration InConfiguration)
        {
            InServiceCollection
                .AddAuthentication(ConfigureAuthentication)
                .AddJwtBearer(InOptions => ConfigureJwtBearer(InOptions, InConfiguration));

            return InServiceCollection;
        }

        private static void ConfigureJwtBearer(JwtBearerOptions InOptions, IConfiguration InConfiguration)
        {
            var settings = InConfiguration
                           .GetSection(nameof(SessionTokenSettings))
                           .Get<SessionTokenSettings>()
                           .Configure();

            InOptions.IncludeErrorDetails  = true;
            InOptions.RequireHttpsMetadata = false;
            InOptions.TokenValidationParameters = settings.ValidationParameters;
        }

        private static void ConfigureAuthentication(AuthenticationOptions InOptions)
        {
            InOptions.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
            InOptions.DefaultChallengeScheme    = JwtBearerDefaults.AuthenticationScheme;
            InOptions.DefaultScheme             = JwtBearerDefaults.AuthenticationScheme;
            InOptions.DefaultForbidScheme = JwtBearerDefaults.AuthenticationScheme;
            InOptions.DefaultSignInScheme = JwtBearerDefaults.AuthenticationScheme;
            InOptions.DefaultSignOutScheme = JwtBearerDefaults.AuthenticationScheme;
        }
    }
}
