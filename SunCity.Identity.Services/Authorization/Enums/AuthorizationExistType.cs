﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SunCity.Identity.Services.Authorization.Enums
{
    public enum AuthorizationExistType
    {
        Any,
        Email,
        Phone,
    }
}
