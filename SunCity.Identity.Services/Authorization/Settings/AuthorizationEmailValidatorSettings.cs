﻿using System.Threading.Tasks;
using AutoMapper;

namespace SunCity.Identity.Services.Authorization.Settings
{
    internal interface IAuthorizationValidatorSettings
    {
        public int MinLenght { get; set; }
        public int MaxLenght { get; set; }
    }

    public class AuthorizationEmailValidatorSettings : IAuthorizationValidatorSettings
    {
        public int MinLenght { get; set; }
        public int MaxLenght { get; set; }
    }
}