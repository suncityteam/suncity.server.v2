﻿using System.Threading.Tasks;
using AutoMapper;

namespace SunCity.Identity.Services.Authorization.Settings
{
    public class AuthorizationPasswordValidatorSettings : IAuthorizationValidatorSettings
    {
        public int MinLenght { get; set; }
        public int MaxLenght { get; set; }
    }
}