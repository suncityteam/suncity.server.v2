﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Text;

namespace SunCity.Identity.Services.Authorization.Settings
{
    public class PasswordHashGeneratorSettings
    {
        [NotNull]
        public string FirstSecureKey { get; set; }

        [NotNull]
        public string SecondSecureKey { get; set; }
    }
}
