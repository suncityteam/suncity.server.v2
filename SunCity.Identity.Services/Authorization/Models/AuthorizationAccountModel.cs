﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using SunCity.Game.Types.Player.Entity.Player;
using SunCity.Identity.DataBase.Keys;

namespace SunCity.Identity.Services.Authorization.Models
{
    [DebuggerDisplay("EntityId: {EntityId}, Login: {Login}")]
    public class AuthorizationAccountModel
    {
        public AccountId EntityId { get; }
        public PlayerAccountLogin Login { get; }

        public AuthorizationAccountModel(AccountId InEntityId, PlayerAccountLogin InLogin)
        {
            EntityId = InEntityId;
            Login = InLogin;
        }
    }
}
