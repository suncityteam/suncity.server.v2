﻿using System.Threading.Tasks;
using AutoMapper;
using SunCity.Common.Operation;

namespace SunCity.Identity.Services.Authorization.Validators
{
    public interface IAuthorizationValidator
    {
        bool IsShort(string InText);
        bool IsLarge(string InText);
        bool IsInvalid(string InText);

        OperationError Validate(string InText);
    }
}