﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Extensions.Options;
using SunCity.Common.Operation;
using SunCity.Identity.Services.Authorization.Settings;

namespace SunCity.Identity.Services.Authorization.Validators.Login
{
    internal class AuthorizationLoginValidator : AuthorizationValidator<AuthorizationLoginValidatorSettings>, IAuthorizationLoginValidator
    {
        public AuthorizationLoginValidator(IOptionsMonitor<AuthorizationLoginValidatorSettings> InSettings) : base(InSettings)
        {
        }

        public override bool IsInvalid(string InText)
        {
            return string.IsNullOrWhiteSpace(InText);
        }

        public override OperationError Validate(string InText)
        {
            //=============================================
            if (IsInvalid(InText))
                return OperationResponseCode.LoginIsInvalid;

            if (IsShort(InText))
                return (OperationResponseCode.LoginIsShort, Settings.CurrentValue.MinLenght);

            if (IsLarge(InText))
                return (OperationResponseCode.LoginIsLarge, Settings.CurrentValue.MaxLenght);

            return OperationResponseCode.Successfully;
        }
    }
}
