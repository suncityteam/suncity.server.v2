﻿using System.Threading.Tasks;
using AutoMapper;
using Microsoft.Extensions.Options;
using SunCity.Common.Operation;
using SunCity.Identity.Services.Authorization.Settings;

namespace SunCity.Identity.Services.Authorization.Validators
{
    internal abstract class AuthorizationValidator<TSettings> : IAuthorizationValidator
        where TSettings : IAuthorizationValidatorSettings
    {
        protected IOptionsMonitor<TSettings> Settings { get; }

        protected AuthorizationValidator(IOptionsMonitor<TSettings> InSettings)
        {
            Settings = InSettings;
        }

        public bool IsShort(string InText)
        {
            return InText.Length < Settings.CurrentValue.MinLenght;
        }

        public bool IsLarge(string InText)
        {
            return InText.Length > Settings.CurrentValue.MaxLenght;
        }

        public abstract bool IsInvalid(string InText);
        public abstract OperationError Validate(string InText);
    }
}