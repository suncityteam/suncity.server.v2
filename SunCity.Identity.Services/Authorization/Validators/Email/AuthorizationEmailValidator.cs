﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Extensions.Options;
using SunCity.Common.Operation;
using SunCity.Identity.Services.Authorization.Settings;

namespace SunCity.Identity.Services.Authorization.Validators.Email
{
    internal class AuthorizationEmailValidator : AuthorizationValidator<AuthorizationEmailValidatorSettings>, IAuthorizationEmailValidator
    {
        public AuthorizationEmailValidator(IOptionsMonitor<AuthorizationEmailValidatorSettings> InSettings) : base(InSettings)
        {
        }

        public override bool IsInvalid(string InText)
        {
            return string.IsNullOrWhiteSpace(InText);
        }

        public override OperationError Validate(string InText)
        {
            //=============================================
            if (IsInvalid(InText))
                return OperationResponseCode.EmailIsInvalid;

            if (IsShort(InText))
                return (OperationResponseCode.EmailIsShort, Settings.CurrentValue.MinLenght);

            if (IsLarge(InText))
                return (OperationResponseCode.EmailIsLarge, Settings.CurrentValue.MaxLenght);

            return OperationResponseCode.Successfully;
        }
    }
}
