﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Extensions.Options;
using SunCity.Common.Operation;
using SunCity.Identity.Services.Authorization.Settings;

namespace SunCity.Identity.Services.Authorization.Validators.Password
{
    internal class AuthorizationPasswordValidator : AuthorizationValidator<AuthorizationPasswordValidatorSettings>, IAuthorizationPasswordValidator
    {
        public AuthorizationPasswordValidator(IOptionsMonitor<AuthorizationPasswordValidatorSettings> InSettings) : base(InSettings)
        {
        }

        public override bool IsInvalid(string InText)
        {
            return string.IsNullOrWhiteSpace(InText);
        }

        public override OperationError Validate(string InText)
        {
            //=============================================
            if (IsInvalid(InText))
                return OperationResponseCode.PasswordIsInvalid;

            if (IsShort(InText))
                return (OperationResponseCode.PasswordIsShort, Settings.CurrentValue.MinLenght);

            if (IsLarge(InText))
                return (OperationResponseCode.PasswordIsLarge, Settings.CurrentValue.MaxLenght);

            return OperationResponseCode.Successfully;
        }
    }
}
