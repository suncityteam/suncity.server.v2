﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using SunCity.Common.Operation;
using SunCity.Identity.DataBase.Keys;
using SunCity.Identity.Services.Authorization.DbQueries;
using SunCity.Identity.Services.Authorization.Enums;
using SunCity.Identity.Services.Authorization.Forms;
using SunCity.Identity.Services.Authorization.Generators;
using SunCity.Identity.Services.Authorization.Models;
using SunCity.Identity.Services.Authorization.Validators.Email;
using SunCity.Identity.Services.Authorization.Validators.Login;
using SunCity.Identity.Services.Authorization.Validators.Password;

namespace SunCity.Identity.Services.Authorization.Service
{
    internal sealed class AuthorizationService : IAuthorizationService
    {
        private IAuthorizationDbQueries AuthorizationDbQueries { get; }
        private IAuthorizationLoginValidator LoginValidator { get; }
        private IAuthorizationEmailValidator EmailValidator { get; }
        private IAuthorizationPasswordValidator PasswordValidator { get; }
        private IAuthorizationPasswordHashGenerator PasswordHashGenerator { get; }

        public AuthorizationService
            (
            IAuthorizationDbQueries InAuthorizationDbQueries,
            IAuthorizationLoginValidator InLoginValidator,
            IAuthorizationEmailValidator InEmailValidator,
            IAuthorizationPasswordValidator InPasswordValidator,
            IAuthorizationPasswordHashGenerator InPasswordHashGenerator
            )
        {
            AuthorizationDbQueries = InAuthorizationDbQueries;
            LoginValidator = InLoginValidator;
            EmailValidator = InEmailValidator;
            PasswordValidator = InPasswordValidator;
            PasswordHashGenerator = InPasswordHashGenerator;
        }

        public Task<bool> IsExist(AccountId InIdentityId)
        {
            return AuthorizationDbQueries.IsExist(InIdentityId);
        }

        public Task<bool> IsExist(string InCreditional, AuthorizationExistType InExistType)
        {
            return AuthorizationDbQueries.IsExist(InCreditional, InExistType);
        }

        public async ValueTask<OperationResponse<AuthorizationAccountModel>> Login(AuthorizationLoginForm InForm)
        {
            //=============================================
            var isValidLogin = LoginValidator.Validate(InForm.Login.Name);
            if (isValidLogin.IsNotCorrect)
                return isValidLogin;

            //=============================================
            var isValidPassword = PasswordValidator.Validate(InForm.Password);
            if (isValidPassword.IsNotCorrect)
                return isValidPassword;

            //=============================================
            var accountEntity = await AuthorizationDbQueries.GetByLogin(InForm.Login);
            if (accountEntity == null)
                return OperationResponseCode.AccountNotFound;

            //=============================================
            if (accountEntity.Blocked)
                return OperationResponseCode.AccountIsBlocked;

            //=============================================
            if (accountEntity.Deleted)
                return OperationResponseCode.AccountIsDeleted;

            //=============================================
            return new AuthorizationAccountModel(accountEntity.EntityId, accountEntity.Login);
        }

        public async ValueTask<OperationResponse<AuthorizationAccountModel>> Register(AuthorizationRegisterForm InForm)
        {
            //=============================================
            var isValidLogin = LoginValidator.Validate(InForm.Login.Name);
            if (isValidLogin.IsNotCorrect)
                return isValidLogin;

            //=============================================
            var isValidEmail = EmailValidator.Validate(InForm.Email);
            if (isValidEmail.IsNotCorrect)
                return isValidEmail;

            //=============================================
            var isValidPassword = PasswordValidator.Validate(InForm.Password);
            if (isValidPassword.IsNotCorrect)
                return isValidPassword;

            //=============================================
            var existLogin = await AuthorizationDbQueries.IsExist(InForm.Login);
            if (existLogin)
                return (OperationResponseCode.AccountLoginIsExist, InForm.Login);

            //=============================================
            var existEmail = await AuthorizationDbQueries.IsExist(InForm.Email, AuthorizationExistType.Email);
            if (existEmail)
                return OperationResponseCode.AccountEmailIsExist;

            //=============================================
            var passwordHash = await PasswordHashGenerator.GenerateHash(InForm.Password);
            var form = new AuthorizationRegisterDbForm
            {
                Login = InForm.Login,
                Email = InForm.Email,
                PasswordHash = passwordHash,
                RefererId = null
            };
            
            //=============================================
            var accountEntity = await AuthorizationDbQueries.Create(form);
            return new AuthorizationAccountModel(accountEntity.EntityId, accountEntity.Login);
        }
    }
}
