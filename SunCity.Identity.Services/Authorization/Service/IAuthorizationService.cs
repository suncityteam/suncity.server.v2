﻿using System.Threading.Tasks;
using AutoMapper;
using SunCity.Common.Operation;
using SunCity.Identity.DataBase.Keys;
using SunCity.Identity.Services.Authorization.Enums;
using SunCity.Identity.Services.Authorization.Forms;
using SunCity.Identity.Services.Authorization.Models;

namespace SunCity.Identity.Services.Authorization.Service
{
    public interface IAuthorizationService
    {
        Task<bool> IsExist(AccountId InIdentityId);
        Task<bool> IsExist(string InCreditional, AuthorizationExistType InExistType);
        ValueTask<OperationResponse<AuthorizationAccountModel>> Login(AuthorizationLoginForm InForm);
        ValueTask<OperationResponse<AuthorizationAccountModel>> Register(AuthorizationRegisterForm InForm);
    }
}