﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace SunCity.Identity.Services.Authorization.Generators
{
    internal class AuthorizationPasswordHashGenerator : IAuthorizationPasswordHashGenerator
    {
        public ValueTask<string> GenerateHash(string InPassword)
        {
            return new ValueTask<string>(InPassword); 
        }

        public ValueTask<bool> IsEqualHash(string InPassword, string InPasswordHash)
        {
            return new ValueTask<bool>(InPassword == InPasswordHash);
        }
    }
}
