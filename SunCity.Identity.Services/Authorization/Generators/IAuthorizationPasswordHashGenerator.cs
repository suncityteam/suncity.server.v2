﻿using System.Threading.Tasks;
using AutoMapper;

namespace SunCity.Identity.Services.Authorization.Generators
{
    public interface IAuthorizationPasswordHashGenerator
    {
        ValueTask<string> GenerateHash(string InPassword);
        ValueTask<bool> IsEqualHash(string InPassword, string InPasswordHash);
    }
}