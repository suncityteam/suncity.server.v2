﻿using SunCity.Game.Types.Player.Entity.Player;

namespace SunCity.Identity.Services.Authorization.Forms
{
    public struct AuthorizationLoginForm
    {
        public PlayerAccountLogin Login { get; set; }
        public string Password { get; set; }
    }
}