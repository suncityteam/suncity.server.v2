﻿using System;
using SunCity.Game.Types.Player.Entity.Player;

namespace SunCity.Identity.Services.Authorization.Forms
{
    public struct AuthorizationRegisterDbForm
    {
        public PlayerAccountLogin Login { get; set; }
        public string Email { get; set; }
        public string PasswordHash { get; set; }
        public Guid? RefererId { get; set; }
    }
}