﻿using System;
using System.Collections.Generic;
using System.Text;
using SunCity.Game.Types.Player.Entity.Player;

namespace SunCity.Identity.Services.Authorization.Forms
{
    public struct AuthorizationRegisterForm
    {
        public PlayerAccountLogin Login { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public Guid? RefererId { get; set; }
    }
}
