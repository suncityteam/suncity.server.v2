﻿using System;
using System.Threading.Tasks;
using AutoMapper;
using JetBrains.Annotations;
using Microsoft.EntityFrameworkCore;
using SunCity.EntityFrameWork.Common;
using SunCity.Game.Types.Player.Entity.Player;
using SunCity.Identity.DataBase;
using SunCity.Identity.DataBase.Entities;
using SunCity.Identity.DataBase.Keys;
using SunCity.Identity.Services.Authorization.Enums;
using SunCity.Identity.Services.Authorization.Forms;

namespace SunCity.Identity.Services.Authorization.DbQueries
{
    internal sealed class AuthorizationDbQueries 
        : BaseDbQueries<IdentityDataBaseContext, AccountEntity>
        , IAuthorizationDbQueries
    {
        protected override DbSet<AccountEntity> DbTable => DbContext.AccountEntity;
        public AuthorizationDbQueries(IdentityDataBaseContext InDbContext) : base(InDbContext)
        {
        }

        public async Task<AccountEntity> Create(AuthorizationRegisterDbForm InForm)
        {
            var account = DbContext.AccountEntity.Add(new AccountEntity
            {
                EntityId = new AccountId(Guid.NewGuid()),
                Login = InForm.Login,
                Email = InForm.Email,
                Password = InForm.PasswordHash,
                Deleted = false,
                Blocked = false,
                CreatedAt = DateTime.UtcNow,
                EmailConfirmed = false,
                PhoneNumberConfirmed = false,
            });

            await DbContext.SaveChangesAsync();
            return account.Entity;
        }

        [ItemCanBeNull]
        public Task<AccountEntity> GetById(AccountId InIdentityId)
        {
            return DbTable.FirstOrDefaultAsync(q => q.EntityId == InIdentityId);
        }

        [ItemCanBeNull]
        public Task<AccountEntity> GetByLogin(PlayerAccountLogin InLogin)
        {
            return DbTable.FirstOrDefaultAsync(q => q.Login == InLogin);
        }

        public Task<bool> IsExist(AccountId InIdentityId)
        {
            return DbTable.AnyAsync(q => q.EntityId == InIdentityId);
        }

        public Task<bool> IsExist(PlayerAccountLogin InLogin)
        {
            return DbTable.AnyAsync(q => q.Login == InLogin);
        }

        public Task<bool> IsExist(string InCreditional, AuthorizationExistType InExistType)
        {
            switch (InExistType)
            {
                case AuthorizationExistType.Email:
                    return DbTable.AnyAsync(q => q.Email == InCreditional);
                default:
                    return DbTable.AnyAsync(q => q.Login.Name == InCreditional || q.Email == InCreditional);
            }
        }
    }
}