﻿using System.Threading.Tasks;
using AutoMapper;
using JetBrains.Annotations;
using SunCity.Game.Types.Player.Entity.Player;
using SunCity.Identity.DataBase.Entities;
using SunCity.Identity.DataBase.Keys;
using SunCity.Identity.Services.Authorization.Enums;
using SunCity.Identity.Services.Authorization.Forms;

namespace SunCity.Identity.Services.Authorization.DbQueries
{
    public interface IAuthorizationDbQueries
    {
        Task<AccountEntity> Create(AuthorizationRegisterDbForm InForm);

        [ItemCanBeNull]
        Task<AccountEntity> GetById(AccountId InIdentityId);

        [ItemCanBeNull]
        Task<AccountEntity> GetByLogin(PlayerAccountLogin InLogin);

        Task<bool> IsExist(AccountId InIdentityId);
        Task<bool> IsExist(PlayerAccountLogin InLogin);
        Task<bool> IsExist(string InCreditional, AuthorizationExistType InExistType);
    }
}