﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using SunCity.Identity.Services.Authorization.DbQueries;
using SunCity.Identity.Services.Authorization.Generators;
using SunCity.Identity.Services.Authorization.Service;
using SunCity.Identity.Services.Authorization.Settings;
using SunCity.Identity.Services.Authorization.Validators.Email;
using SunCity.Identity.Services.Authorization.Validators.Login;
using SunCity.Identity.Services.Authorization.Validators.Password;

namespace SunCity.Identity.Services.Authorization
{
    public static class ServiceRecorder
    {
        public static IServiceCollection AddAuthorizationService(this IServiceCollection InServiceCollection, IConfiguration InConfiguration)
        {
            var passwordHashGeneratorSettings = InConfiguration.GetSection(nameof(PasswordHashGeneratorSettings));
            InServiceCollection.Configure<PasswordHashGeneratorSettings>(passwordHashGeneratorSettings);

            InServiceCollection.AddScoped<IAuthorizationPasswordHashGenerator, AuthorizationPasswordHashGenerator>();

            InServiceCollection.AddScoped<IAuthorizationDbQueries, AuthorizationDbQueries>();
            InServiceCollection.AddScoped<IAuthorizationService, AuthorizationService>();

            var sectionAuthorizationValidators = InConfiguration.GetSection("Application.Authorization.Validators");
            InServiceCollection.Configure<AuthorizationEmailValidatorSettings>(sectionAuthorizationValidators.GetSection("Email"));
            InServiceCollection.Configure<AuthorizationLoginValidatorSettings>(sectionAuthorizationValidators.GetSection("Login"));
            InServiceCollection.Configure<AuthorizationPasswordValidatorSettings>(sectionAuthorizationValidators.GetSection("Password"));


            InServiceCollection.AddScoped<IAuthorizationEmailValidator, AuthorizationEmailValidator>();
            InServiceCollection.AddScoped<IAuthorizationLoginValidator, AuthorizationLoginValidator>();
            InServiceCollection.AddScoped<IAuthorizationPasswordValidator, AuthorizationPasswordValidator>();


            return InServiceCollection;
        }
    }
}
