﻿using System;
using SunCity.Identity.DataBase.Keys;

namespace SunCity.Identity.Models
{
    public readonly struct HttpNetworkIdentityToken
    {
        public HttpNetworkIdentityToken(SessionTokenId InTokenId, AccountId InAccountId, DateTime InExpiresAt, string InToken)
        {
            TokenId = InTokenId;
            AccountId = InAccountId;
            ExpiresAt = InExpiresAt;
            Token = InToken;
        }

        public string Token { get; }
        public SessionTokenId TokenId { get; }
        public AccountId AccountId { get; }
        public DateTime ExpiresAt { get; }
    }
}
