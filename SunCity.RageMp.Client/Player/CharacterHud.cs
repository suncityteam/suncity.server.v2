﻿using System;
using System.Collections.Generic;
using System.Text;
using RAGE.Elements;

namespace SunCity.RageMp.Client.Player
{
    public class RageMpUtility
    {
        public static RAGE.Game.Entity GetLookingAtEntity(TracePointToPointFlags traceFlags, float distance = 10)
        {
            int x = 0, y = 0;
            RAGE.Game.Graphics.GetActiveScreenResolution(ref x, ref y);

            return null;
        }
    }

    public class CharacterHud
    {
        private void SubscribePropertyChanges(string propertyName)
        {
            RAGE.Events.AddDataHandler(propertyName, (entity, value, oldValue) => OnPropertyChanges(entity, propertyName, value, oldValue));
        }

        private void OnPropertyChanges(Entity entity, string propertyName, object value, object oldValue)
        {
            throw new NotImplementedException();
        }

        private void OnDisplayLoocking()
        {
            if (RAGE.Game.Player.IsPlayerDead())
                return;

            if (RAGE.Game.Ped.IsPedOnVehicle(LocalPlayer.PlayerPedId))
                return;

            if (RAGE.Game.Ped.IsPedInAnyVehicle(LocalPlayer.PlayerPedId, true))
                return;

            if (RAGE.Game.Ped.IsPedSittingInAnyVehicle(LocalPlayer.PlayerPedId))
                return;

            
        }
    }
}
