﻿namespace SunCity.RageMp.Client.Player
{
    public class RageMpPlayerHud
    {
        public static void ConfigureWorldPlayerCharacter()
        {
            RAGE.Game.Player.SetPlayerHealthRechargeMultiplier(0);
            RAGE.Game.Player.DisablePlayerVehicleRewards();
            RAGE.Game.Player.SetPlayerTargetingMode(1);

            RAGE.Game.Player.ClearPlayerHasDamagedAtLeastOneNonAnimalPed();
            RAGE.Game.Player.ClearPlayerHasDamagedAtLeastOnePed();

            RAGE.Game.Player.ResetPlayerArrestState();
            RAGE.Game.Player.ResetPlayerInputGait();
            RAGE.Game.Player.ResetPlayerStamina();
            RAGE.Game.Player.ResetWantedLevelDifficulty();
            RAGE.Game.Player.SpecialAbilityReset();

            RAGE.Game.Ui.DisplayRadar(true);
            RAGE.Game.Ui.DisplayHud(true);

            RAGE.Chat.Show(true);
            RAGE.Chat.Activate(true);
            RAGE.Ui.Cursor.ShowCursor(false, false);
        }

        public static void ToggleGameHud(bool toggle)
        {
            //  cash always hidden
            RAGE.Game.Ui.DisplayCash(false);

            RAGE.Game.Ui.DisplayAreaName(toggle);

            RAGE.Game.Ui.DisplayRadar(toggle);
            RAGE.Game.Ui.DisplayHud(toggle);
        }
    }
}