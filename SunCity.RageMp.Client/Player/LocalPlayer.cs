﻿namespace SunCity.RageMp.Client.Player
{
    //  https://wiki.rage.mp/index.php?title=Raycasting::testPointToPoint

    public class LocalPlayer
    {
        public static int PlayerPedId => RAGE.Game.Player.PlayerPedId();
        public static int PlayerId => RAGE.Game.Player.PlayerId();
    }
}