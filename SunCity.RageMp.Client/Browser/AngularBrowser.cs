﻿using System;
using System.Collections.Generic;
using System.Text;
using RAGE.Ui;
using RAGE.Util;

namespace SunCity.RageMp.Client.Browser
{
    public class AngularBrowser
    {
        private const string ANGULAR_HOST = "http://localhost:4200/";
        private RAGE.Ui.HtmlWindow Browser { get; }

        public AngularBrowser()
        {
            Browser = new HtmlWindow(ANGULAR_HOST);
            Browser.ExecuteJs($"onInitRageClientId('${RAGE.Game.Player.NetworkPlayerIdToInt()}');");

            RAGE.Events.Add("Handler:BrowserEvent", OnCallCustomRageEvent);
        }

        public void CallAngularEvent<TPayload>(string eventName, TPayload payload)
        {
            var json = Json.Serialize(payload);
            Browser.ExecuteJs($"onCallCustomRageEventV2('{json}');");
        }

        private void OnCallCustomRageEvent(object[] args)
        {
            Browser.ExecuteJs($"onCallCustomRageEventV2('${args[0]}');");
        }
    }
}
