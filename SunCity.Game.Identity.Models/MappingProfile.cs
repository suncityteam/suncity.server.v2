﻿using System.Threading.Tasks;
using AutoMapper;
using SunCity.Game.Identity.Models.Character;

namespace SunCity.Game.Identity.Models
{
    public sealed class MappingProfile : Profile
    {
        public MappingProfile() : base("SunCity.Game.Identity.Models")
        {
            //CreateMap<PlayerCharacterEntity, GameCharacterModel>();
        }
    }
}