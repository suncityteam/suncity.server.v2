﻿using System.Threading.Tasks;
using AutoMapper;
using SunCity.Domain.Interfaces.Entity;
using SunCity.Game.Types.Player.Entity.Player.Character;

namespace SunCity.Game.Identity.Models.Character
{
    public class GameCharacterNameModel
        : IEntityWithId<PlayerCharacterId>
          , IEntityWithName<PlayerCharacterName>
    {
        public PlayerCharacterId EntityId { get; set; }
        public PlayerCharacterName Name { get; set; }
    }
}