﻿using System;
using System.Collections.Generic;
using System.Text;
using SunCity.Domain.Interfaces.Entity;
using SunCity.Game.Enums.Character;
using SunCity.Game.Types.Player.Entity.Player.Character;

namespace SunCity.Game.Identity.Models.Character
{
    public class GameCharacterModel 
        : IEntityWithId<PlayerCharacterId>
        , IEntityWithName<PlayerCharacterName>
    {
        public PlayerCharacterId EntityId { get; set; }
        public PlayerCharacterName Name { get; set; }
        public string FractionName { get; set; }
        public GameCharacterSex Sex { get; set; }

        public TimeSpan PlayedTime { get; set; }
        public DateTime RegistrationDate { get; set; }

        public int Level { get; set; }
        public long Experience { get; set; }
        public long NextLevelExperience { get; set; }

        public long Money { get; set; }
    }
}
