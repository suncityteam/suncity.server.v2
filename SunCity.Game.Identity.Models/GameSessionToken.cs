﻿using System;
using SunCity.Game.Types.Player.Entity.Player.Character;
using SunCity.Identity.DataBase.Keys;

namespace SunCity.Game.Identity.Models
{
    public sealed class GameSessionToken
    {
        public SessionTokenId TokenId { get; set; }
        public PlayerCharacterId CharacterId { get; set; }
        public AccountId AccountId { get; set; }
    }
}
