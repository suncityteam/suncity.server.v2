﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using SunCity.Game.World.Services.License.Services;

namespace SunCity.Game.World.Services.License
{
    public static class ServiceRecorder
    {
        public static IServiceCollection AddGameWorldLicensesServices(this IServiceCollection InServiceCollection, IConfiguration InConfiguration)
        {
            InServiceCollection.AddScoped<IWorldLicensesService, WorldLicensesService>();
            
            return InServiceCollection;
        }
    }
}
