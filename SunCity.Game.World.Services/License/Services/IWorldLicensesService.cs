﻿using System.Collections.Generic;
using SunCity.Common.Operation;
using SunCity.Game.World.Models.Licenses;

namespace SunCity.Game.World.Services.License.Services
{
    public interface IWorldLicensesService
    {
        OperationResponse<LicenseModel> GetById(LicenseId licenseId);
        IReadOnlyList<LicenseModel> GetById(IReadOnlyList<LicenseId> licenseIds);
    }
}