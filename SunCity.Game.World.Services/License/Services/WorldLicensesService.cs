﻿using System.Collections.Generic;
using System.Linq;
using SunCity.Common.Operation;
using SunCity.Domain.Interfaces.Extensions;
using SunCity.Game.World.Licenses;
using SunCity.Game.World.Models.Licenses;

namespace SunCity.Game.World.Services.License.Services
{
    internal class WorldLicensesService : IWorldLicensesService
    {
        private IGameLicenseStorage Storage { get; }

        public OperationResponse<LicenseModel> GetById(LicenseId licenseId)
        {
            var license = Storage[licenseId];
            if (license == null)
                return (OperationResponseCode.LicenseNotFound, licenseId);

            return new LicenseModel
            {
                EntityId = license.EntityId,
                Name = license.Name
            };
        }

        public IReadOnlyList<LicenseModel> GetById(IReadOnlyList<LicenseId> licenseIds)
        {
            var licenses = Storage.Values.GetByIds(licenseIds);
            
            return licenses.Select(license => new LicenseModel
            {
                EntityId = license.EntityId,
                Name = license.Name
            }).ToArray();
        }
    }
}