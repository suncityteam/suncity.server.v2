﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using SunCity.Game.World.Services.Character;
using SunCity.Game.World.Services.Fraction;
using SunCity.Game.World.Services.Items;
using SunCity.Game.World.Services.License;
using SunCity.Game.World.Services.Property;
using SunCity.Game.World.Services.Vehicle;

namespace SunCity.Game.World.Services
{
    public static class ServiceRecorder
    {
        public static IServiceCollection AddGameWorldServices(this IServiceCollection InServiceCollection, IConfiguration InConfiguration)
        {
            InServiceCollection.AddFractionServices(InConfiguration);

            InServiceCollection.AddGameWorldCharacterServices(InConfiguration);
            InServiceCollection.AddGameWorldItemsServices(InConfiguration);
            InServiceCollection.AddGameWorldVehicleServices(InConfiguration);
            InServiceCollection.AddGameWorldPropertyServices(InConfiguration);
            
            InServiceCollection.AddGameWorldLicensesServices(InConfiguration);
            return InServiceCollection;
        }
    }
}