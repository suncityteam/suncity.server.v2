﻿using SunCity.Domain.Interfaces.Entity;

namespace SunCity.Game.World.Services.Fraction.Models
{
    public class FractionNameModel : IEntityWithId<FractionId>
    {
        public FractionId EntityId { get; set; }
        public string Name { get; set; }
    }
}
