﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using SunCity.Game.World.Services.Fraction.Services;

namespace SunCity.Game.World.Services.Fraction
{
    public static class ServiceRecorder
    {
        public static IServiceCollection AddFractionServices(this IServiceCollection InServiceCollection, IConfiguration InConfiguration)
        {
            InServiceCollection.AddSingleton<IFractionNameService, FractionNameService>();
            return InServiceCollection;
        }
    }
}
