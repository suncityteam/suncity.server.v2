﻿using System.Collections.Generic;
using SunCity.Common.Operation;
using SunCity.Game.World.Services.Fraction.Models;

namespace SunCity.Game.World.Services.Fraction.Services
{
    public interface IFractionNameService
    {
        OperationResponse<FractionNameModel> GetById(FractionId InFractionId);
        IReadOnlyList<FractionNameModel> GetByIds(IReadOnlyList<FractionId> InFractionId);
    }
}