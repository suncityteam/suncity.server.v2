﻿using System.Collections.Generic;
using SunCity.Common.Operation;
using SunCity.Game.World.Fraction;
using SunCity.Game.World.Services.Fraction.Models;

namespace SunCity.Game.World.Services.Fraction.Services
{
    internal class FractionNameService : IFractionNameService
    {
        private IWorldFractionStorage FractionStorage { get; }

        public FractionNameService(IWorldFractionStorage InFractionStorage)
        {
            FractionStorage = InFractionStorage;
        }


        public OperationResponse<FractionNameModel> GetById(FractionId InFractionId)
        {
            var fraction = FractionStorage[InFractionId];
            if (fraction == null)
                return (OperationResponseCode.FractionNotFound, InFractionId);

            return new FractionNameModel
            {
                EntityId = fraction.EntityId,
                Name     = fraction.Name
            };
        }

        public IReadOnlyList<FractionNameModel> GetByIds(IReadOnlyList<FractionId> InFractionIds)
        {
            var names = new List<FractionNameModel>(InFractionIds.Count);

            foreach (var fractionId in InFractionIds)
            {
                var fraction = FractionStorage[fractionId];
                if (fraction != null)
                {
                    names.Add(new FractionNameModel
                    {
                        EntityId = fraction.EntityId,
                        Name = fraction.Name
                    });
                }
            }

            return names;
        }
    }
}
