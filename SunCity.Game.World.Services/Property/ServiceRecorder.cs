﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using SunCity.Game.World.Services.Property.Category.Business.Bank;
using SunCity.Game.World.Services.Property.DbQueries;

namespace SunCity.Game.World.Services.Property
{
    public static class ServiceRecorder
    {
        public static IServiceCollection AddGameWorldPropertyServices(this IServiceCollection InServiceCollection, IConfiguration InConfiguration)
        {
            InServiceCollection.AddBankServices(InConfiguration);
            InServiceCollection.AddScoped<IPropertyOwnerDbQueries, PropertyOwnerDbQueries>();
        
            return InServiceCollection;
        }
    }
}
