﻿using SunCity.Domain.Interfaces.Entity;
using SunCity.Game.Enums.World.Property.Owner;

namespace SunCity.Game.World.Services.Property.Forms
{
    public class RemovePropertyFromSellingDbForm : IEntityWithId<PropertyId>
    {
        public PropertyId EntityId { get; set; }
        public BankCardId BankCardId { get; set; }
    }

    public class AddPropertyOwnerDbForm : IEntityWithId<PropertyId>
    {
        public PropertyId EntityId { get; set; }
        public PlayerCharacterId OwnerId { get; set; }
        public PropertyOwnerType Type { get; set; }
    }
}
