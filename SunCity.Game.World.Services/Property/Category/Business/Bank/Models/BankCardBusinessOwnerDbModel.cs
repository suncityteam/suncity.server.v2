﻿using SunCity.Game.World.DataBase.Entitites.Property.Business.Bank.Account;

namespace SunCity.Game.World.Services.Property.Category.Business.Bank.Models
{
    public class BankCardBusinessOwnerDbModel : BankCardOwnerDbModel
    {
        public override BankCardAccountCategory Category => BankCardAccountCategory.Business;
        public string BusinessName { get; set; }

        public override string ToString()
        {
            return $"{base.ToString()}, BusinessName: {BusinessName}";
        }
    }
}