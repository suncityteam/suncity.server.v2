﻿using SunCity.Game.World.DataBase.Entitites.Property.Business.Bank.Account;

namespace SunCity.Game.World.Services.Property.Category.Business.Bank.Models
{
    public class BankCardPlayerOwnerDbModel : BankCardOwnerDbModel
    {
        public override BankCardAccountCategory Category => BankCardAccountCategory.Player;
        public PlayerCharacterId CharacterId { get; set; }

        public override string ToString()
        {
            return $"{base.ToString()}, CharacterId: {CharacterId}";
        }
    }
}
