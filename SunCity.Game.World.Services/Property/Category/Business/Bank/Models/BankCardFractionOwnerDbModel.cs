﻿using SunCity.Game.World.DataBase.Entitites.Property.Business.Bank.Account;

namespace SunCity.Game.World.Services.Property.Category.Business.Bank.Models
{
    public class BankCardFractionOwnerDbModel : BankCardOwnerDbModel
    {
        public override BankCardAccountCategory Category => BankCardAccountCategory.Fraction;
        public FractionId FractionId { get; set; }


        public override string ToString()
        {
            return $"{base.ToString()}, FractionId: {FractionId}";
        }
    }
}