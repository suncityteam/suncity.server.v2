﻿using SunCity.Domain.Interfaces.Entity;
using SunCity.Game.World.DataBase.Entitites.Property.Business.Bank.Account;

namespace SunCity.Game.World.Services.Property.Category.Business.Bank.Models
{
    public abstract class BankCardOwnerDbModel : IEntityWithId<BankCardId>
    {
        public BankCardId EntityId { get; set; }
        public long BankCardNumber { get; set; }
        public abstract BankCardAccountCategory Category { get; }

        public override string ToString()
        {
            return $"{Category} | EntityId: {EntityId}, BankCardNumber: {BankCardNumber}";
        }
    }
}