﻿using System.Threading.Tasks;
using SunCity.Common.Operation;

namespace SunCity.Game.World.Services.Property.Category.Business.Bank.Generators
{
    public interface IBankCardNumberGenerator
    {
        Task<OperationResponse<long>> GenerateCardNumber(int InBankPrefix = 777);
    }
}