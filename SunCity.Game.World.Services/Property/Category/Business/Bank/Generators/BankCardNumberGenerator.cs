﻿using System;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using SunCity.Common.Operation;
using SunCity.EntityFrameWork.Common;
using SunCity.Game.World.DataBase;

namespace SunCity.Game.World.Services.Property.Category.Business.Bank.Generators
{
    public class BankCardNumberGenerator 
        : BaseDbQueries<GameWorldDbContext>, 
          IBankCardNumberGenerator
    {
        public const int CardNumberLenght = 12;
        private Random Random { get; } = new Random();

        public BankCardNumberGenerator(GameWorldDbContext InDbContext) : base(InDbContext)
        {
        }

        public async Task<OperationResponse<long>> GenerateCardNumber(int InBankPrefix = 777)
        {
            for (var i = 0; i < 30; i++)
            {
                var number = Generate(InBankPrefix);
                var numberExist = await DbContext.BankCardAccountEntity.AnyAsync(q => q.Number == number);
                if (numberExist == false)
                    return number;
            }

            return OperationResponseCode.BankCardNumberFailedGenerate;
        }

        private long Generate(int InBankPrefix)
        {
            var bankPrefix = InBankPrefix.ToString();
            var capacity = CardNumberLenght - bankPrefix.Length;

            var pow = (int)Math.Pow(10, capacity);
            var number = Random.Next(1 * pow, 10 * pow);

            var cardNumber = bankPrefix + number;
            return long.Parse(cardNumber);
        }


    }
}
