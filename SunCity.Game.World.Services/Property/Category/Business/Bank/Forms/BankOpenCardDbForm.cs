﻿namespace SunCity.Game.World.Services.Property.Category.Business.Bank.Forms
{
    public class BankOpenCardDbForm
    {
        public PlayerCharacterId CharacterId { get; set; }
        public BankCardTariffId TariffEntityId { get; set; }
        public long PinCodeHash { get; set; }
        public long SecureWordHash { get; set; }
        public long Number { get; set; }
    }
}
