﻿namespace SunCity.Game.World.Services.Property.Category.Business.Bank.Forms
{
    public class BankDepositDbForm
    {
        public PlayerCharacterId CharacterId { get; set; }
        public BankCardId EntityId { get; set; }
        public long Amount { get; set; }
    }
}