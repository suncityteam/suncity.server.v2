﻿namespace SunCity.Game.World.Services.Property.Category.Business.Bank.Forms.Payments
{
    public class BankVehicleBuyDbForm : IEntityWithVehicleId
    {
        public VehicleId VehicleId { get; set; }
        public BankCardId PayerId { get; set; }
        public BankCardId PayeeId { get; set; }
        public PropertyId PropertyId { get; set; }
        public long Cost { get; set; }
    }
    
    public class BankPropertyBuyDbForm : IEntityWithPropertyId
    {
        public PropertyId PropertyId { get; set; }
        public BankCardId PayerId { get; set; }
        public BankCardId PayeeId { get; set; }
        public long Cost { get; set; }
    }
}
