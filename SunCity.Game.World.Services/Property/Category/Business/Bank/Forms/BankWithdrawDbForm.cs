﻿namespace SunCity.Game.World.Services.Property.Category.Business.Bank.Forms
{
    public class BankWithdrawDbForm
    {
        public PlayerCharacterId CharacterId { get; set; }
        public BankCardId EntityId { get; set; }
        public long Amount { get; set; }
    }

    public class BankTransferDbForm
    {
        public BankCardId PayerId { get; set; }
        public BankCardId PayeeId { get; set; }

        public long Commission { get; set; }
        public long Amount { get; set; }
    }
}