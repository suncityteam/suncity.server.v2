﻿using System.Threading.Tasks;
using SunCity.Common.Crypto.Crc64;
using SunCity.Common.Operation;
using SunCity.Game.World.Forms.Property.Business.Bank;
using SunCity.Game.World.Models.Property.Business.Bank.Card;
using SunCity.Game.World.Services.Property.Category.Business.Bank.DbQueries.Client.BankCard.Player;

namespace SunCity.Game.World.Services.Property.Category.Business.Bank.Service.Client.BankCard
{
    internal class BankCardAuthorizationService : IBankCardAuthorizationService
    {
        private IBankCardPlayerDbQueries DbQueries { get; }

        public BankCardAuthorizationService(IBankCardPlayerDbQueries dbQueries)
        {
            DbQueries = dbQueries;
        }
        
        public async Task<OperationResponse<BankCardBalanceModel>> Authorization(BankAuthorizationForm form, bool isExpiredValidate = true)
        {
            var bankCard = await DbQueries.GetById(form.PayerBankCardId);
            if (bankCard == null)
                return OperationResponseCode.BankCardNotFound;

            var pinCodeHash = form.PinCode.Crc64Hash();
            if (bankCard.PinCodeHash != pinCodeHash)
                return OperationResponseCode.BankCardPinCodeNotValid;

            if (bankCard.IsExpired && isExpiredValidate)
                return (OperationResponseCode.BankCardNotValid, bankCard.Number);

            return new BankCardBalanceModel
            {
                EntityId = bankCard.EntityId,
                Balance = bankCard.Balance,
                Number = bankCard.Number
            };
        }
    }
}