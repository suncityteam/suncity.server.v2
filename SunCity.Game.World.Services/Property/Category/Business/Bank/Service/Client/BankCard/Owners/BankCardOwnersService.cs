﻿using System;
using System.Threading.Tasks;
using SunCity.Common.Operation;
using SunCity.Game.Identity.Services.Character.Services;
using SunCity.Game.Types.Player.Entity.Player.Character;
using SunCity.Game.World.DataBase.Entitites.Property.Business.Bank.Account;
using SunCity.Game.World.Forms.Property.Business.Bank;
using SunCity.Game.World.Models.Property.Business.Bank.Card;
using SunCity.Game.World.Services.Fraction.Services;
using SunCity.Game.World.Services.Property.Category.Business.Bank.DbQueries.Client.BankCard.Owners;
using SunCity.Game.World.Services.Property.Category.Business.Bank.Models;

namespace SunCity.Game.World.Services.Property.Category.Business.Bank.Service.Client.BankCard.Owners
{
    internal class BankCardOwnersService : IBankCardOwnersService
    {
        private IBankCardOwnersDbQueries OwnersDbQueries { get; }
        private IGameCharactersNameService CharactersNameService { get; }
        private IFractionNameService FractionNameService { get; }

        public BankCardOwnersService(IBankCardOwnersDbQueries InOwnersDbQueries, IGameCharactersNameService InCharactersNameService, IFractionNameService InFractionNameService)
        {
            OwnersDbQueries       = InOwnersDbQueries;
            CharactersNameService = InCharactersNameService;
            FractionNameService = InFractionNameService;
        }

        public async Task<OperationResponse<BankCardOwnerModel>> GetOwner(BankCardOwnerForm InForm)
        {
            var bankCard = await OwnersDbQueries.GetOwner(InForm.BankCardNumber);
            if (bankCard == null)
                return (OperationResponseCode.BankCardNotFound, InForm.BankCardNumber);

            if (bankCard.Category == BankCardAccountCategory.Player && bankCard is BankCardPlayerOwnerDbModel playerOwner)
            {
                var characterName = await CharactersNameService.GetById(playerOwner.CharacterId);
                if (characterName.IsNotCorrect)
                    return characterName.Error;

                var model = new BankCardOwnerModel
                {
                    EntityId  = bankCard.EntityId,
                    Number    = bankCard.BankCardNumber,
                    OwnerName = characterName.Content.Name
                };

                return model;
            }

            if (bankCard.Category == BankCardAccountCategory.Fraction && bankCard is BankCardFractionOwnerDbModel fractionOwner)
            {
                var fractionName = FractionNameService.GetById(fractionOwner.FractionId);
                if (fractionName.IsNotCorrect)
                    return fractionName.Error;

                var model = new BankCardOwnerModel
                {
                    EntityId  = bankCard.EntityId,
                    Number    = bankCard.BankCardNumber,
                    OwnerName = new PlayerCharacterName(fractionName.Content.Name)
                };

                return model;
            }

            throw new NotImplementedException();
        }
    }
}