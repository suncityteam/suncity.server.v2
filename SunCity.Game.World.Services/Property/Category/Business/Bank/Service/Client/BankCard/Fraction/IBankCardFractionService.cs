﻿using System.Threading.Tasks;
using SunCity.Common.Operation;
using SunCity.Game.World.Models.Property.Business.Bank.Card;
using SunCity.Game.World.Services.Property.Category.Business.Bank.Service.Client.BankCard.Abstract;

namespace SunCity.Game.World.Services.Property.Category.Business.Bank.Service.Client.BankCard.Fraction
{
    public interface IBankCardFractionService : IBankCardService
    {
        Task<OperationResponse<BankCardPreviewModel[]>> GetList(FractionId InFractionId);
    }
}