﻿using System.ComponentModel;
using System.Threading.Tasks;
using SunCity.Common.Operation;
using SunCity.Game.World.Forms.Property.Business.Bank;
using SunCity.Game.World.Models.Property.Business.Bank.Card;

namespace SunCity.Game.World.Services.Property.Category.Business.Bank.Service.Client.BankCard.Abstract
{
    [EditorBrowsable(EditorBrowsableState.Never)]
    public interface IBankCardService
    {
        Task<OperationResponse<BankCardBalanceModel>> GetBalance(BankAuthorizationForm InForm);
        
        Task<OperationResponse> Validate(BankCardId InBankCardId);

    }
}