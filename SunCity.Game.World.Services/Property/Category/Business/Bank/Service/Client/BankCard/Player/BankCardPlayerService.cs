﻿using AutoMapper;
using SunCity.Game.World.Services.Property.Category.Business.Bank.DbQueries.Client.BankCard.Player;
using SunCity.Game.World.Services.Property.Category.Business.Bank.Service.Client.BankCard.Abstract;

namespace SunCity.Game.World.Services.Property.Category.Business.Bank.Service.Client.BankCard.Player
{
    internal class BankCardPlayerService : BankCardService<IBankCardPlayerDbQueries>, IBankCardPlayerService
    {
        public BankCardPlayerService(IBankCardPlayerDbQueries InDbQueries, IMapper InMapper) : base(InDbQueries, InMapper)
        {
        }
    }
}