﻿using System.Threading.Tasks;
using SunCity.Common.Operation;
using SunCity.Game.World.Forms.Property.Business.Bank;
using SunCity.Game.World.Models.Property.Business.Bank.Card;

namespace SunCity.Game.World.Services.Property.Category.Business.Bank.Service.Client.BankCard.Owners
{
    public interface IBankCardOwnersService
    {
        Task<OperationResponse<BankCardOwnerModel>> GetOwner(BankCardOwnerForm InForm);
    }
}
