﻿using System.Threading.Tasks;
using AutoMapper;
using SunCity.Common.Operation;
using SunCity.Game.World.Models.Property.Business.Bank.Card;
using SunCity.Game.World.Services.Property.Category.Business.Bank.DbQueries.Client.BankCard.Fraction;
using SunCity.Game.World.Services.Property.Category.Business.Bank.Service.Client.BankCard.Abstract;

namespace SunCity.Game.World.Services.Property.Category.Business.Bank.Service.Client.BankCard.Fraction
{
    internal class BankCardFractionService : BankCardService<IBankCardFractionDbQueries>, IBankCardFractionService
    {

        public async Task<OperationResponse<BankCardPreviewModel[]>> GetList(FractionId InFractionId)
        {
            var bankCards = await DbQueries.GetList(InFractionId);
            var models = Mapper.Map<BankCardPreviewModel[]>(bankCards);
            return models;
        }

        public BankCardFractionService(IBankCardFractionDbQueries InDbQueries, IMapper InMapper) : base(InDbQueries, InMapper)
        {
        }
    }
}