﻿using System.Threading.Tasks;
using AutoMapper;
using SunCity.Common.Crypto.Crc64;
using SunCity.Common.Operation;
using SunCity.Game.World.DataBase.Entitites.Property.Business.Bank.Account;
using SunCity.Game.World.Forms.Property.Business.Bank;
using SunCity.Game.World.Models.Property.Business.Bank.Card;
using SunCity.Game.World.Services.Property.Category.Business.Bank.DbQueries.Client.BankCard.Abstract;

namespace SunCity.Game.World.Services.Property.Category.Business.Bank.Service.Client.BankCard.Abstract
{
    internal abstract class BankCardService<TDbQueries> : IBankCardService
        where TDbQueries : IBankCardDbQueries
    {
        protected IMapper Mapper { get; }
        protected TDbQueries DbQueries { get; }

        protected BankCardService
        (
            TDbQueries InDbQueries,
            IMapper InMapper
        )
        {
            DbQueries = InDbQueries;
            Mapper = InMapper;
        }
        
        public async Task<OperationResponse<BankCardBalanceModel>> GetBalance(BankAuthorizationForm InForm)
        {
            var bankCard = await GetWithAuthorization(InForm);
            if (bankCard.IsNotCorrect)
                return bankCard.Error;

            return new BankCardBalanceModel
            {
                EntityId = bankCard.Content.EntityId,
                Balance = bankCard.Content.Balance,
                Number = bankCard.Content.Number
            };
        }
        
        public async Task<OperationResponse> Validate(BankCardId InBankCardId)
        {
            var bankCard = await DbQueries.GetById(InBankCardId);
            if (bankCard == null)
                return OperationResponseCode.BankCardNotFound;

            if (bankCard.IsExpired)
                return (OperationResponseCode.BankCardNotValid, bankCard.Number);

            return OperationResponseCache.Successfully;
        }

        private async Task<OperationResponse<BankCardAccountEntity>> GetWithAuthorization(BankAuthorizationForm InForm, bool isExpiredValidate = true)
        {
            var bankCard = await DbQueries.GetById(InForm.PayerBankCardId);
            if (bankCard == null)
                return OperationResponseCode.BankCardNotFound;

            var pinCodeHash = InForm.PinCode.Crc64Hash();
            if (bankCard.PinCodeHash != pinCodeHash)
                return OperationResponseCode.BankCardPinCodeNotValid;

            if (bankCard.IsExpired && isExpiredValidate)
                return (OperationResponseCode.BankCardNotValid, bankCard.Number);

            return bankCard;
        }
    }
}