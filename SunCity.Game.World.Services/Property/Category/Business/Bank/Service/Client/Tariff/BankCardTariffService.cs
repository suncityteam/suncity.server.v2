﻿using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using SunCity.Common.Operation;
using SunCity.Game.World.Forms.Property.Business.Bank.Tariff;
using SunCity.Game.World.Models.Property.Business.Bank;
using SunCity.Game.World.Property;
using SunCity.Game.World.Property.Category.Business.Bank;
using SunCity.Game.World.Services.Property.Category.Business.Bank.DbQueries.Client.Tariff;

namespace SunCity.Game.World.Services.Property.Category.Business.Bank.Service.Client.Tariff
{
    public class BankCardTariffService : IBankCardTariffService
    {
        private IMapper Mapper { get; }
        private IWorldPropertyStorage PropertyStorage { get; }
        private IBankCardTariffDbQuries BankCardTariffDbQuries { get; }

        public BankCardTariffService(IMapper InMapper, IWorldPropertyStorage InPropertyStorage, IBankCardTariffDbQuries InBankCardTariffDbQuries)
        {
            PropertyStorage = InPropertyStorage;
            BankCardTariffDbQuries = InBankCardTariffDbQuries;
            Mapper = InMapper;
        }

        public async Task<OperationResponse> Create(CreateBankCardTariffForm InForm)
        {
            var bankProperty = PropertyStorage.GetById<BankWorldProperty>(InForm.EntityId);
            if (bankProperty == null)
                return (OperationResponseCode.PropertyIdNotFound, InForm.EntityId);

            var tariffEntity = await BankCardTariffDbQuries.Create(InForm);
            var tariffModel = Mapper.Map<BankCardTariffModel>(tariffEntity);

            bankProperty.BankCardTariff.Add(tariffModel);

            return OperationResponseCode.Successfully;
        }

        public async Task<OperationResponse> Update(UpdateBankCardTariffForm InForm)
        {
            var tariffEntity = await BankCardTariffDbQuries.GetById(InForm.EntityId);
            if(tariffEntity == null)
                return (OperationResponseCode.BankCardTariffNotFound, InForm.EntityId);

            var bankProperty = PropertyStorage.GetById<BankWorldProperty>(tariffEntity.PropertyId);
            if (bankProperty == null)
                return (OperationResponseCode.PropertyIdNotFound, tariffEntity.PropertyId);

            var tariffModel = bankProperty.GetBankCardTariff(tariffEntity.EntityId);
            if (tariffModel == null)
            {
                tariffModel = Mapper.Map<BankCardTariffModel>(tariffEntity);
                bankProperty.BankCardTariff.Add(tariffModel);
            }
            else
            {
                tariffModel.Name            = InForm.Name;
                tariffModel.Deposit         = InForm.Deposit;
                tariffModel.MaintenanceCost = InForm.MaintenanceCost;
                tariffModel.TariffCost      = InForm.TariffCost;
                tariffModel.Transfer        = InForm.Transfer;
                tariffModel.WithDraw        = InForm.WithDraw;
            }
            
            await BankCardTariffDbQuries.Update(InForm);
            return OperationResponseCode.Successfully;
        }

        public OperationResponse<List<BankCardTariffModel>> GetList(PropertyId InBankPropertyId)
        {
            var bankProperty = PropertyStorage.GetById<BankWorldProperty>(InBankPropertyId);
            if (bankProperty == null)
                return (OperationResponseCode.PropertyIdNotFound, InBankPropertyId);

            return bankProperty.BankCardTariff;
        }

        public async Task<OperationResponse<BankCardTariffModel>> GetById(BankCardTariffId InBankCardTariffId)
        {
            var tariffEntity = await BankCardTariffDbQuries.GetById(InBankCardTariffId);
            if (tariffEntity == null)
                return (OperationResponseCode.BankCardTariffNotFound, InBankCardTariffId);

            var tariffModel = Mapper.Map<BankCardTariffModel>(tariffEntity);
            return tariffModel;
        }
    }
}