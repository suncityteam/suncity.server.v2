﻿using System.Collections.Generic;
using System.Threading.Tasks;
using SunCity.Common.Operation;
using SunCity.Game.World.Forms.Property.Business.Bank.Tariff;
using SunCity.Game.World.Models.Property.Business.Bank;

namespace SunCity.Game.World.Services.Property.Category.Business.Bank.Service.Client.Tariff
{
    public interface IBankCardTariffService
    {
        Task<OperationResponse> Create(CreateBankCardTariffForm InForm);
        Task<OperationResponse> Update(UpdateBankCardTariffForm InForm);
        OperationResponse<List<BankCardTariffModel>> GetList(PropertyId InBankPropertyId);
        Task<OperationResponse<BankCardTariffModel>> GetById(BankCardTariffId InBankCardTariffId);
    }
}