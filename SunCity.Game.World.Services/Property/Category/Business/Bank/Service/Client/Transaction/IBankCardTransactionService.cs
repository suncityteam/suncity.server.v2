﻿using System.Threading.Tasks;
using SunCity.Common.Operation;
using SunCity.Game.World.Models.Property.Business.Bank.Card.Transaction;

namespace SunCity.Game.World.Services.Property.Category.Business.Bank.Service.Client.Transaction
{
    public interface IBankCardTransactionService
    {
        Task<OperationResponse<BankCardTransactionPreviewModel[]>> GetTransactions(BankCardId InBankCardId);
        Task<OperationResponse<BankCardTransactionDetailedModel>> GetTransaction(BankCardTransactionId InBankCardTransactionId);

    }
}