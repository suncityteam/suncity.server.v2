﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using SunCity.Common.Operation;
using SunCity.Domain.Interfaces.Extensions;
using SunCity.Game.Identity.Models.Character;
using SunCity.Game.Identity.Services.Character.Services;
using SunCity.Game.Types.Player.Entity.Player.Character;
using SunCity.Game.World.DataBase.Entitites.Property.Business.Bank.Transaction;
using SunCity.Game.World.Enums.Property.Business.Bank;
using SunCity.Game.World.Models.Property.Business.Bank.Card.Transaction;
using SunCity.Game.World.Services.Fraction.Models;
using SunCity.Game.World.Services.Fraction.Services;
using SunCity.Game.World.Services.Property.Category.Business.Bank.DbQueries.Client.BankCard.Owners;
using SunCity.Game.World.Services.Property.Category.Business.Bank.DbQueries.Client.Transaction;
using SunCity.Game.World.Services.Property.Category.Business.Bank.Models;

namespace SunCity.Game.World.Services.Property.Category.Business.Bank.Service.Client.Transaction
{
    internal class BankCardTransactionService : IBankCardTransactionService
    {
        protected IBankCardTransactionDbQueries TransactionsDbQueries { get; }
        protected IBankCardOwnersDbQueries OwnersDbQueries { get; }
        protected IGameCharactersNameService CharactersNameService { get; }
        protected IFractionNameService FractionNameService { get; }

        public BankCardTransactionService(IBankCardTransactionDbQueries InDbQueries, IGameCharactersNameService InCharactersNameService, IBankCardOwnersDbQueries InOwnersDbQueries, IFractionNameService InFractionNameService)
        {
            TransactionsDbQueries = InDbQueries;
            CharactersNameService = InCharactersNameService;
            OwnersDbQueries = InOwnersDbQueries;
            FractionNameService = InFractionNameService;
        }

        public async Task<OperationResponse<BankCardTransactionPreviewModel[]>> GetTransactions(BankCardId InBankCardId)
        {
            var transactions = await TransactionsDbQueries.GetTransactions(InBankCardId);
            var bankCardIds = GetBankCardIdsInTransactions(transactions);


            var bankCardPlayerOwners = await OwnersDbQueries.GetPlayerOwners(bankCardIds);
            var bankCardFractionOwners = await OwnersDbQueries.GetFractionOwners(bankCardIds);
            var bankCardBusinessOwners = await OwnersDbQueries.GetBusinessOwners(bankCardIds);

            
            var context = new CreateTransactionContext
            {
                FractionNames  = FractionNameService.GetByIds(bankCardFractionOwners.Select(q => q.FractionId).ToArray()),
                CharacterNames = await CharactersNameService.GetByIds(bankCardPlayerOwners.Select(q => q.CharacterId).ToArray()),
                
                BusinessAccounts =  bankCardBusinessOwners,
                CharacterAccounts = bankCardPlayerOwners,
                FractionAccounts = bankCardFractionOwners
            };

            return transactions.Select(q => CreateTransactionModel(context, q)).ToArray();
        }

        private IReadOnlyCollection<BankCardId> GetBankCardIdsInTransactions(BankCardTransactionEntity[] InTransactions)
        {
            var transactedBankCardIds = new[]
            {
                InTransactions.Where(q => q.Category == BankCardTransactionCategory.TransferFrom).Select(q => q.TransferFromTransaction.PayerId),
                InTransactions.Where(q => q.Category == BankCardTransactionCategory.TransferFor).Select(q => q.TransferToTransaction.PayeeId),

                InTransactions.Where(q => q.Category == BankCardTransactionCategory.PropertyPurchaseFrom).Select(q => q.PropertyPaymentFromTransaction.PayerId),
                InTransactions.Where(q => q.Category == BankCardTransactionCategory.PropertyPurchaseFor).Select(q => q.PropertyPaymentToTransaction.PayeeId)
            };

            var bankCardIds = transactedBankCardIds.SelectMany(q => q).Distinct().ToArray();
            return bankCardIds;
        }

        public Task<OperationResponse<BankCardTransactionDetailedModel>> GetTransaction(BankCardTransactionId InBankCardTransactionId)
        {
            throw new NotImplementedException();
        }
        
        private BankCardTransactionPreviewModel CreateTransactionModel(CreateTransactionContext InContext, BankCardTransactionEntity InTransactionEntity)
        {
            var model = new BankCardTransactionPreviewModel
            {
                EntityId  = InTransactionEntity.EntityId,
                Amount    = InTransactionEntity.Amount,
                Category  = InTransactionEntity.Category,
                CreatedAt = InTransactionEntity.CreatedDate,
            };

            //==============
            if (InTransactionEntity.Category == BankCardTransactionCategory.TransferFrom)
            {
                model.Member = GetOwner(InContext, InTransactionEntity.TransferFromTransaction.PayerId);
            }
            else if (InTransactionEntity.Category == BankCardTransactionCategory.TransferFor)
            {
                model.Member = GetOwner(InContext, InTransactionEntity.TransferToTransaction.PayeeId);
            }
            
            //==============
            else if (InTransactionEntity.Category == BankCardTransactionCategory.PropertyPurchaseFrom)
            {
                model.Member = GetOwner(InContext, InTransactionEntity.PropertyPaymentFromTransaction.PayerId);
            }
            else if (InTransactionEntity.Category == BankCardTransactionCategory.PropertyPurchaseFor)
            {
                model.Member = GetOwner(InContext, InTransactionEntity.PropertyPaymentToTransaction.PayeeId);
            }
            
            //==============
            else if (InTransactionEntity.Category == BankCardTransactionCategory.VehiclePurchaseFor)
            {
                model.Member = GetOwner(InContext, InTransactionEntity.VehiclePaymentFromTransaction.PayerId);
            }
            else if (InTransactionEntity.Category == BankCardTransactionCategory.VehiclePurchaseFrom)
            {
                model.Member = GetOwner(InContext, InTransactionEntity.VehiclePaymentToTransaction.PayeeId);
            }
            
            //==============
            return model;
        }

        private BankCardWithOwner GetOwner(CreateTransactionContext InContext, BankCardId InBankCardId)
        {
            var playerOwner = InContext.CharacterAccounts.GetById(InBankCardId);
            if (playerOwner != null)
            {
                return new BankCardWithOwner
                {
                    BankCardNumber = playerOwner.BankCardNumber,
                    PlayerName     = InContext.CharacterNames.GetById(playerOwner.CharacterId).Name
                };
            }

            var fractionOwner = InContext.FractionAccounts.GetById(InBankCardId);
            if (fractionOwner != null)
            {
                return new BankCardWithOwner
                {
                    BankCardNumber = fractionOwner.BankCardNumber,
                    PlayerName     = new PlayerCharacterName(InContext.FractionNames.GetById(fractionOwner.FractionId).Name)
                };
            }
            
            var businessOwner = InContext.BusinessAccounts.GetById(InBankCardId);
            if (businessOwner != null)
            {
                return new BankCardWithOwner
                {
                    BankCardNumber = businessOwner.BankCardNumber,
                    PlayerName     = new PlayerCharacterName(businessOwner.BusinessName)
                };
            }

            throw new NotImplementedException();
        }

        private class CreateTransactionContext
        {
            public IReadOnlyList<GameCharacterNameModel> CharacterNames { get; set; }
            public IReadOnlyList<FractionNameModel> FractionNames { get; set; }
            public IReadOnlyList<BankCardBusinessOwnerDbModel> BusinessAccounts { get; set; }

            public IReadOnlyList<BankCardPlayerOwnerDbModel> CharacterAccounts { get; set; }
            public IReadOnlyList<BankCardFractionOwnerDbModel> FractionAccounts { get; set; }
        }
    }
}
