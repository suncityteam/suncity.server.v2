﻿using System.Threading.Tasks;
using SunCity.Common.Operation;
using SunCity.Game.World.Enums.Property.Business.Bank;
using SunCity.Game.World.Models.Property.Business.Bank.Card.Transaction;
using SunCity.Game.World.Services.Property.Category.Business.Bank.DbQueries.Client.Payments;
using SunCity.Game.World.Services.Property.Category.Business.Bank.Forms.Payments;

namespace SunCity.Game.World.Services.Property.Category.Business.Bank.Service.Client.Payments
{
    internal class BankCardPropertyPaymentsService : IBankCardPropertyPaymentsService
    {
        private IBankCardPropertyPaymentsDbQueries PropertyPaymentsDbQueries { get; }

        public BankCardPropertyPaymentsService(IBankCardPropertyPaymentsDbQueries InPropertyPaymentsDbQueries)
        {
            PropertyPaymentsDbQueries = InPropertyPaymentsDbQueries;
        }

        public async Task<OperationResponse<BankCardTransactionPreviewModel>> PropertyBuy(BankPropertyBuyDbForm InForm)
        {
            var transactionEntity = await PropertyPaymentsDbQueries.PropertyBuy(InForm);
            return new BankCardTransactionPreviewModel
            {
                EntityId  = transactionEntity.EntityId,
                Amount    = transactionEntity.Amount,
                Category  = BankCardTransactionCategory.PropertyPurchaseFor,
                CreatedAt = transactionEntity.CreatedDate,
            };
        }

        public async Task<OperationResponse<BankCardTransactionPreviewModel>> VehicleBuy(BankVehicleBuyDbForm InForm)
        {
            var transactionEntity = await PropertyPaymentsDbQueries.VehicleBuy(InForm);
            return new BankCardTransactionPreviewModel
            {
                EntityId  = transactionEntity.EntityId,
                Amount    = transactionEntity.Amount,
                Category  = BankCardTransactionCategory.PropertyPurchaseFor,
                CreatedAt = transactionEntity.CreatedDate,
            };        }
    }
}