﻿using System.Threading.Tasks;
using SunCity.Common.Operation;
using SunCity.Game.World.Models.Property.Business.Bank.Card.Transaction;
using SunCity.Game.World.Services.Property.Category.Business.Bank.Forms.Payments;

namespace SunCity.Game.World.Services.Property.Category.Business.Bank.Service.Client.Payments
{
    public interface IBankCardPropertyPaymentsService
    {
        //Task<OperationResponse> PhoneNumber(BankCardOwnerForm InForm);
        //Task<OperationResponse> PropertyVat(BankCardOwnerForm InForm);
        Task<OperationResponse<BankCardTransactionPreviewModel>> PropertyBuy(BankPropertyBuyDbForm InForm);
        Task<OperationResponse<BankCardTransactionPreviewModel>> VehicleBuy(BankVehicleBuyDbForm InForm);

    }
}