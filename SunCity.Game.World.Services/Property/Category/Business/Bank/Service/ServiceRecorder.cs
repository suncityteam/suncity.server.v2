﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using SunCity.Game.World.Services.Property.Category.Business.Bank.Service.Client.BankCard;
using SunCity.Game.World.Services.Property.Category.Business.Bank.Service.Client.BankCard.Fraction;
using SunCity.Game.World.Services.Property.Category.Business.Bank.Service.Client.BankCard.Owners;
using SunCity.Game.World.Services.Property.Category.Business.Bank.Service.Client.BankCard.Player;
using SunCity.Game.World.Services.Property.Category.Business.Bank.Service.Client.Payments;
using SunCity.Game.World.Services.Property.Category.Business.Bank.Service.Client.Tariff;
using SunCity.Game.World.Services.Property.Category.Business.Bank.Service.Client.Transaction;

namespace SunCity.Game.World.Services.Property.Category.Business.Bank.Service
{
    internal static class ServiceRecorder
    {
        public static IServiceCollection AddBankClientServices(this IServiceCollection InServiceCollection, IConfiguration InConfiguration)
        {
            InServiceCollection.AddScoped<IBankCardOwnersService, BankCardOwnersService>();
            InServiceCollection.AddScoped<IBankCardAuthorizationService, BankCardAuthorizationService>();

            InServiceCollection.AddScoped<IBankCardPropertyPaymentsService, BankCardPropertyPaymentsService>();
            InServiceCollection.AddScoped<IBankCardTransactionService, BankCardTransactionService>();

            InServiceCollection.AddScoped<IBankCardPlayerService, BankCardPlayerService>();
            InServiceCollection.AddScoped<IBankCardFractionService, BankCardFractionService>();

            InServiceCollection.AddScoped<IBankCardTariffService, BankCardTariffService>();

            return InServiceCollection;
        }
    }
}
