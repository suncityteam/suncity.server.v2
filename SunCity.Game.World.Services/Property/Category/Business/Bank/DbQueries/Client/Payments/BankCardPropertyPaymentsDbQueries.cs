﻿using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using SunCity.EntityFrameWork.Common;
using SunCity.Game.World.DataBase;
using SunCity.Game.World.DataBase.Entitites.Property.Business.Bank.Account;
using SunCity.Game.World.DataBase.Entitites.Property.Business.Bank.Transaction;
using SunCity.Game.World.DataBase.Entitites.Property.Business.Bank.Transaction.Payments.Property;
using SunCity.Game.World.DataBase.Entitites.Property.Business.Bank.Transaction.Payments.Vehicle;
using SunCity.Game.World.Enums.Property.Business.Bank;
using SunCity.Game.World.Services.Property.Category.Business.Bank.DbQueries.Client.BankCard;
using SunCity.Game.World.Services.Property.Category.Business.Bank.Forms.Payments;

namespace SunCity.Game.World.Services.Property.Category.Business.Bank.DbQueries.Client.Payments
{
    internal class BankCardPropertyPaymentsDbQueries
        : BaseDbQueries<GameWorldDbContext, BankCardAccountEntity>
          , IBankCardPropertyPaymentsDbQueries
    {
        protected override DbSet<BankCardAccountEntity> DbTable => DbContext.BankCardAccountEntity;

        public BankCardPropertyPaymentsDbQueries(GameWorldDbContext InDbContext) : base(InDbContext)
        {
        }

        public async Task<BankCardTransactionEntity> PropertyBuy(BankPropertyBuyDbForm InForm)
        {
            var payerBankCard = DbTable.Local.First(q => q.EntityId == InForm.PayerId);
            payerBankCard.Balance -= InForm.Cost;

            var payeeBankCard = DbTable.Local.First(q => q.EntityId == InForm.PayeeId);
            payeeBankCard.Balance += InForm.Cost;

            //  TODO: Use transactions
            //  https://www.thinktecture.com/entity-framework/entity-framework-core-use-transactionscope-with-caution/
            //  Mb? https://thinkrethink.net/2017/10/02/implement-pessimistic-concurrency-in-entity-framework-core/

            var payerTransaction = BankCardTransactionFactory.CreateTransaction(DbContext.BankCardTransactionEntity, payerBankCard.EntityId, InForm.Cost, InEntity => InEntity.PropertyPaymentToTransaction = new BankCardPropertyPaymentToTransactionEntity()
            {
                PayeeId = payeeBankCard.EntityId,
                PropertyId  = InForm.PropertyId
            }, BankCardTransactionCategory.PropertyPurchaseFor);

            var payeeTransaction = BankCardTransactionFactory.CreateTransaction(DbContext.BankCardTransactionEntity, payeeBankCard.EntityId, InForm.Cost, InEntity => InEntity.PropertyPaymentFromTransaction = new BankCardPropertyPaymentFromTransactionEntity()
            {
                PayerId    = payerBankCard.EntityId,
                PropertyId = InForm.PropertyId
            }, BankCardTransactionCategory.PropertyPurchaseFrom);

            await DbContext.SaveChangesAsync();

            return payerTransaction;
        }

        public async Task<BankCardTransactionEntity> VehicleBuy(BankVehicleBuyDbForm InForm)
        {
            var payerBankCard = DbTable.Local.First(q => q.EntityId == InForm.PayerId);
            payerBankCard.Balance -= InForm.Cost;

            var payeeBankCard = DbTable.Local.First(q => q.EntityId == InForm.PayeeId);
            payeeBankCard.Balance += InForm.Cost;

            //  TODO: Use transactions
            //  https://www.thinktecture.com/entity-framework/entity-framework-core-use-transactionscope-with-caution/
            //  Mb? https://thinkrethink.net/2017/10/02/implement-pessimistic-concurrency-in-entity-framework-core/

            var payerTransaction = BankCardTransactionFactory.CreateTransaction(DbContext.BankCardTransactionEntity, payerBankCard.EntityId, InForm.Cost, InEntity => InEntity.VehiclePaymentToTransaction = new BankCardVehiclePaymentToTransactionEntity()
            {
                PayeeId = payeeBankCard.EntityId,
                PropertyId  = InForm.PropertyId,
                VehicleId = InForm.VehicleId
            }, BankCardTransactionCategory.PropertyPurchaseFor);

            var payeeTransaction = BankCardTransactionFactory.CreateTransaction(DbContext.BankCardTransactionEntity, payeeBankCard.EntityId, InForm.Cost, InEntity => InEntity.VehiclePaymentFromTransaction = new BankCardVehiclePaymentFromTransactionEntity()
            {
                PayerId    = payerBankCard.EntityId,
                PropertyId = InForm.PropertyId,
                VehicleId = InForm.VehicleId

            }, BankCardTransactionCategory.PropertyPurchaseFrom);

            await DbContext.SaveChangesAsync();

            return payerTransaction;        
        }
    }
}