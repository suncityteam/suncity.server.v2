﻿using System.Threading.Tasks;
using SunCity.Game.World.DataBase.Entitites.Property.Business.Bank.Transaction;
using SunCity.Game.World.Services.Property.Category.Business.Bank.Forms.Payments;

namespace SunCity.Game.World.Services.Property.Category.Business.Bank.DbQueries.Client.Payments
{
    public interface IBankCardPropertyPaymentsDbQueries
    {
        Task<BankCardTransactionEntity> PropertyBuy(BankPropertyBuyDbForm InForm);
        Task<BankCardTransactionEntity> VehicleBuy(BankVehicleBuyDbForm InForm);
    }
}