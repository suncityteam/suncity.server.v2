﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using SunCity.Game.World.DataBase;
using SunCity.Game.World.DataBase.Entitites.Property.Business.Bank.Account;
using SunCity.Game.World.Services.Property.Category.Business.Bank.DbQueries.Client.BankCard.Abstract;
using SunCity.Game.World.Services.Property.Category.Business.Bank.Forms;

namespace SunCity.Game.World.Services.Property.Category.Business.Bank.DbQueries.Client.BankCard.Player
{
    internal class BankCardPlayerDbQueries : BankCardDbQueries, IBankCardPlayerDbQueries
    {
        public BankCardPlayerDbQueries(GameWorldDbContext InDbContext) : base(InDbContext)
        {

        }

        public Task<BankCardAccountEntity[]> GetList(PlayerCharacterId InCharacterId)
        {
            return DbContext.BankCardPlayerAccountEntity
                            .Where(q => q.CharacterId == InCharacterId)
                            .Select(q => q.AccountEntity)
                            .ToArrayAsync();
        }

        public override Task<BankCardAccountEntity> GetById(BankCardId InBankCardId)
        {
            return DbTable.FirstOrDefaultAsync(q => q.EntityId == InBankCardId);
        }

        public override Task<BankCardAccountEntity> GetByNumber(long InBankCardNumber)
        {
            return DbTable
                .Include(q => q.PlayerAccountEntity)
                .FirstOrDefaultAsync(q => q.Number == InBankCardNumber);
        }

        public override Task<BankCardAccountEntity> OpenCard(BankOpenCardDbForm InForm)
        {
            return AddAndSaveAsync(new BankCardAccountEntity
            {
                EntityId = BankCardId.GenerateId(),
                Category = BankCardAccountCategory.Player,

                PlayerAccountEntity = new BankCardPlayerAccountEntity
                {
                    CharacterId = InForm.CharacterId,
                },

                PinCodeHash    = InForm.PinCodeHash,
                SecureWordHash = InForm.SecureWordHash,
                Number         = InForm.Number,

                Name = "Новая банковская карта",

                CreatedDate = DateTime.UtcNow,
                ExpiredDate = DateTime.UtcNow.AddDays(60),

                Balance = 0,
            });
        }

    }
}