﻿using System.ComponentModel;
using System.Threading.Tasks;
using JetBrains.Annotations;
using SunCity.Game.World.DataBase.Entitites.Property.Business.Bank.Account;
using SunCity.Game.World.DataBase.Entitites.Property.Business.Bank.Transaction;
using SunCity.Game.World.Services.Property.Category.Business.Bank.Forms;

namespace SunCity.Game.World.Services.Property.Category.Business.Bank.DbQueries.Client.BankCard.Abstract
{
    [EditorBrowsable(EditorBrowsableState.Never)]
    public interface IBankCardDbQueries
    {
        Task<bool> IsExist(BankCardId InBankCardId);
        Task<BankCardAccountEntity> GetById(BankCardId InBankCardId);
        Task<BankCardAccountEntity> GetByNumber(long InBankCardNumber);

        [ItemNotNull]
        Task<BankCardAccountEntity> OpenCard(BankOpenCardDbForm InForm);
        Task<BankCardTransactionEntity> Deposit(BankDepositDbForm InForm);
        Task<BankCardTransactionEntity> Withdraw(BankWithdrawDbForm InForm);
        Task<BankCardTransactionEntity> Transfer(BankTransferDbForm InForm);
    }
}