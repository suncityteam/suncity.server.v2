﻿using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using SunCity.EntityFrameWork.Common;
using SunCity.Game.World.DataBase;
using SunCity.Game.World.DataBase.Entitites.Property.Business.Bank.Account;
using SunCity.Game.World.DataBase.Entitites.Property.Business.Bank.Transaction;
using SunCity.Game.World.DataBase.Entitites.Property.Business.Bank.Transaction.Deposit;
using SunCity.Game.World.DataBase.Entitites.Property.Business.Bank.Transaction.Transfer;
using SunCity.Game.World.Enums.Property.Business.Bank;
using SunCity.Game.World.Services.Property.Category.Business.Bank.Forms;

namespace SunCity.Game.World.Services.Property.Category.Business.Bank.DbQueries.Client.BankCard.Abstract
{
    internal abstract class BankCardDbQueries
        : BaseDbQueries<GameWorldDbContext, BankCardAccountEntity>
        , IBankCardDbQueries
    {
        protected override DbSet<BankCardAccountEntity> DbTable => DbContext.BankCardAccountEntity;

        protected BankCardDbQueries(GameWorldDbContext InDbContext) : base(InDbContext)
        {
        }

        public abstract Task<BankCardAccountEntity> GetById(BankCardId InBankCardId);

        public abstract Task<BankCardAccountEntity> GetByNumber(long InBankCardNumber);

        public abstract Task<BankCardAccountEntity> OpenCard(BankOpenCardDbForm InForm);


        public Task<bool> IsExist(BankCardId InBankCardId)
        {
            return DbTable.AnyAsync(q => q.EntityId == InBankCardId);
        }

        public async Task<BankCardTransactionEntity> Deposit(BankDepositDbForm InForm)
        {
            var bankCard = DbTable.Local.First(q => q.EntityId == InForm.EntityId);
            bankCard.Balance += InForm.Amount;

            var transaction = BankCardTransactionFactory.CreateTransaction(DbContext.BankCardTransactionEntity, bankCard.EntityId, InForm.Amount, InEntity => InEntity.DepositTransaction = new BankCardDepositTransactionEntity()
            {
                SenderId    = InForm.CharacterId,
            }, BankCardTransactionCategory.Deposit);

            await DbContext.SaveChangesAsync();

            return transaction;
        }

        public async Task<BankCardTransactionEntity> Withdraw(BankWithdrawDbForm InForm)
        {
            var bankCard = DbTable.Local.First(q => q.EntityId == InForm.EntityId);
            bankCard.Balance -= InForm.Amount;

            var transaction = BankCardTransactionFactory.CreateTransaction(DbContext.BankCardTransactionEntity, bankCard.EntityId, InForm.Amount, InEntity => InEntity.WithdrawTransaction = new BankCardWithdrawTransactionEntity
            {
                RecipientId = InForm.CharacterId
            }, BankCardTransactionCategory.Withdraw);

            await DbContext.SaveChangesAsync();

            return transaction;
        }

        public async Task<BankCardTransactionEntity> Transfer(BankTransferDbForm InForm)
        {
            var payerBankCard = DbTable.Local.First(q => q.EntityId == InForm.PayerId);
            payerBankCard.Balance -= InForm.Amount;

            var payeeBankCard = DbTable.Local.First(q => q.EntityId == InForm.PayeeId);
            payeeBankCard.Balance += InForm.Amount;

            //  TODO: Use transactions
            //  https://www.thinktecture.com/entity-framework/entity-framework-core-use-transactionscope-with-caution/
            //  Mb? https://thinkrethink.net/2017/10/02/implement-pessimistic-concurrency-in-entity-framework-core/

            var payerTransaction = BankCardTransactionFactory.CreateTransaction(DbContext.BankCardTransactionEntity, payerBankCard.EntityId, InForm.Amount, InEntity => InEntity.TransferToTransaction = new BankCardTransferToTransactionEntity()
            {
                PayeeId = payeeBankCard.EntityId,
            }, BankCardTransactionCategory.TransferFor);

            var payeeTransaction = BankCardTransactionFactory.CreateTransaction(DbContext.BankCardTransactionEntity, payeeBankCard.EntityId, InForm.Amount, InEntity => InEntity.TransferFromTransaction = new BankCardTransferFromTransactionEntity()
            {
                PayerId = payerBankCard.EntityId,
            }, BankCardTransactionCategory.TransferFrom);

            await DbContext.SaveChangesAsync();

            return payerTransaction;
        }


    }
}
