﻿using System.Threading.Tasks;
using SunCity.Game.World.DataBase.Entitites.Property.Business.Bank.Account;
using SunCity.Game.World.Services.Property.Category.Business.Bank.DbQueries.Client.BankCard.Abstract;

namespace SunCity.Game.World.Services.Property.Category.Business.Bank.DbQueries.Client.BankCard.Fraction
{
    public interface IBankCardFractionDbQueries : IBankCardDbQueries
    {
        Task<BankCardAccountEntity[]> GetList(FractionId InFractionId);
    }
}