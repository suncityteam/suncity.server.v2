﻿using System.Collections.Generic;
using System.Threading.Tasks;
using SunCity.Game.World.Services.Property.Category.Business.Bank.Models;

namespace SunCity.Game.World.Services.Property.Category.Business.Bank.DbQueries.Client.BankCard.Owners
{
    public interface IBankCardOwnersDbQueries
    {
        Task<BankCardOwnerDbModel> GetOwner(long InBankCardNumber);
        Task<BankCardOwnerDbModel> GetOwner(BankCardId InBankCardId);
        Task<BankCardPlayerOwnerDbModel[]> GetPlayerOwners(IReadOnlyCollection<BankCardId> InBankCardIds);
        Task<BankCardFractionOwnerDbModel[]> GetFractionOwners(IReadOnlyCollection<BankCardId> InBankCardIds);
        Task<BankCardBusinessOwnerDbModel[]> GetBusinessOwners(IReadOnlyCollection<BankCardId> InBankCardIds);

    }
}