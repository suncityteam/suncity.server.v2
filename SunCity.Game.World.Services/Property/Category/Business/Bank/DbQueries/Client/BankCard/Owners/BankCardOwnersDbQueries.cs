﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using SunCity.EntityFrameWork.Common;
using SunCity.Game.World.DataBase;
using SunCity.Game.World.DataBase.Entitites.Property.Business.Bank.Account;
using SunCity.Game.World.Services.Property.Category.Business.Bank.Models;

namespace SunCity.Game.World.Services.Property.Category.Business.Bank.DbQueries.Client.BankCard.Owners
{
    internal class BankCardOwnersDbQueries : BaseDbQueries<GameWorldDbContext>, IBankCardOwnersDbQueries
    {
        public BankCardOwnersDbQueries(GameWorldDbContext InDbContext) : base(InDbContext)
        {

        }

        
        public async Task<BankCardOwnerDbModel> GetOwner(long InBankCardNumber)
        {
            var bankCard = await DbContext.BankCardAccountEntity.FirstOrDefaultAsync(q => q.Number == InBankCardNumber);
            if (bankCard == null)
                return null;

            return await GetOwner(bankCard);
        }

        public async Task<BankCardOwnerDbModel> GetOwner(BankCardId InBankCardId)
        {
            var bankCard = await DbContext.BankCardAccountEntity.FirstOrDefaultAsync(q => q.EntityId == InBankCardId);
            if (bankCard == null)
                return null;

            return await GetOwner(bankCard);
        }

        public Task<BankCardPlayerOwnerDbModel[]> GetPlayerOwners(IReadOnlyCollection<BankCardId> InBankCardIds)
        {
            var uniqueIds = InBankCardIds.Distinct().ToArray();

            return DbContext.BankCardPlayerAccountEntity
                            .Include(q => q.AccountEntity)
                            .Where(q => uniqueIds.Contains(q.EntityId))
                            .Select(q => new BankCardPlayerOwnerDbModel
                            {
                                EntityId       = q.EntityId,
                                CharacterId    = q.CharacterId,
                                BankCardNumber = q.AccountEntity.Number
                            }).ToArrayAsync();
        }

        public Task<BankCardFractionOwnerDbModel[]> GetFractionOwners(IReadOnlyCollection<BankCardId> InBankCardIds)
        {
            var uniqueIds = InBankCardIds.Distinct().ToArray();

            return DbContext.BankCardFractionAccountEntity
                            .Include(q => q.AccountEntity)
                            .Where(q => uniqueIds.Contains(q.EntityId))
                            .Select(q => new BankCardFractionOwnerDbModel
                            {
                                EntityId       = q.EntityId,
                                FractionId = q.FractionId,
                                BankCardNumber = q.AccountEntity.Number
                            }).ToArrayAsync();
        }
        
        public Task<BankCardBusinessOwnerDbModel[]> GetBusinessOwners(IReadOnlyCollection<BankCardId> InBankCardIds)
        {
            var uniqueIds = InBankCardIds.Distinct().ToArray();

            return DbContext.BankCardBusinessAccountEntity
                .Include(q => q.AccountEntity)
                .Where(q => uniqueIds.Contains(q.EntityId))
                .Select(q => new BankCardBusinessOwnerDbModel
                {
                    EntityId       = q.EntityId,
                    BusinessName = q.BusinessName,
                    BankCardNumber = q.AccountEntity.Number
                }).ToArrayAsync();
        }

        private async Task<BankCardOwnerDbModel> GetOwner(BankCardAccountEntity InBankCardEntity)
        {
            if (InBankCardEntity.Category == BankCardAccountCategory.Player)
            {
                var playerBankCardAccount = await DbContext.BankCardPlayerAccountEntity.FirstAsync(q => q.EntityId == InBankCardEntity.EntityId);
                return new BankCardPlayerOwnerDbModel
                {
                    EntityId       = InBankCardEntity.EntityId,
                    BankCardNumber = InBankCardEntity.Number,
                    CharacterId    = playerBankCardAccount.CharacterId
                };
            }

            if (InBankCardEntity.Category == BankCardAccountCategory.Fraction)
            {
                var fractionBankCardAccount = await DbContext.BankCardFractionAccountEntity.FirstAsync(q => q.EntityId == InBankCardEntity.EntityId);
                return new BankCardFractionOwnerDbModel()
                {
                    EntityId       = InBankCardEntity.EntityId,
                    BankCardNumber = InBankCardEntity.Number,
                    FractionId     = fractionBankCardAccount.FractionId
                };
            }

            if (InBankCardEntity.Category == BankCardAccountCategory.Business)
            {
                var businessBankCardAccount = await DbContext.BankCardBusinessAccountEntity.FirstAsync(q => q.EntityId == InBankCardEntity.EntityId);
                return new BankCardBusinessOwnerDbModel()
                {
                    EntityId       = InBankCardEntity.EntityId,
                    BankCardNumber = InBankCardEntity.Number,
                    BusinessName     = businessBankCardAccount.BusinessName
                };
            }
            
            throw new ArgumentException($"Invalid category ({InBankCardEntity.Category} / {(int)InBankCardEntity.Category})", nameof(InBankCardEntity.Category));
        }
    }
}
