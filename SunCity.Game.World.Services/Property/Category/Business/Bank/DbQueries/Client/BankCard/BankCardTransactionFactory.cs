﻿using System;
using Microsoft.EntityFrameworkCore;
using SunCity.Game.World.DataBase.Entitites.Property.Business.Bank.Transaction;
using SunCity.Game.World.Enums.Property.Business.Bank;

namespace SunCity.Game.World.Services.Property.Category.Business.Bank.DbQueries.Client.BankCard
{
    internal static class BankCardTransactionFactory
    {
        public static BankCardTransactionEntity CreateTransaction(DbSet<BankCardTransactionEntity> InDbSet, BankCardId InBankCardId, long InAmount, Action<BankCardTransactionEntity> InAction, BankCardTransactionCategory InCategory)
        {
            var transaction = InDbSet.Add(new BankCardTransactionEntity
            {
                EntityId    = BankCardTransactionId.GenerateId(),
                BankCardId  = InBankCardId,
                CreatedDate = DateTime.UtcNow,
                Amount      = InAmount,
                Commission  = 0,
                Category    = InCategory
            }).Entity;

            InAction.Invoke(transaction);
            return transaction;
        }

    }
}