﻿using System.Threading.Tasks;
using SunCity.Game.World.DataBase.Entitites.Property.Business.Bank.Tariff;
using SunCity.Game.World.Forms.Property.Business.Bank.Tariff;

namespace SunCity.Game.World.Services.Property.Category.Business.Bank.DbQueries.Client.Tariff
{
    public interface IBankCardTariffDbQuries
    {
        Task<BankCardTariffEntity> GetById(BankCardTariffId InBankCardTariffId);
        Task<BankCardTariffEntity> Create(CreateBankCardTariffForm InForm);
        Task<BankCardTariffEntity> Update(UpdateBankCardTariffForm InForm);
    }
}
