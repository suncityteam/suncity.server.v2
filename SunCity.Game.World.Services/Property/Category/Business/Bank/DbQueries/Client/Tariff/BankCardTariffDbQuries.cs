﻿using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using SunCity.EntityFrameWork.Common;
using SunCity.Game.World.DataBase;
using SunCity.Game.World.DataBase.Entitites.Property.Business.Bank.Tariff;
using SunCity.Game.World.Forms.Property.Business.Bank.Tariff;

namespace SunCity.Game.World.Services.Property.Category.Business.Bank.DbQueries.Client.Tariff
{
    internal class BankCardTariffDbQuries 
        : BaseDbQueries<GameWorldDbContext, BankCardTariffEntity>
          , IBankCardTariffDbQuries
    {
        protected override DbSet<BankCardTariffEntity> DbTable => DbContext.BankCardTariffEntity;
        public BankCardTariffDbQuries(GameWorldDbContext InDbContext) : base(InDbContext)
        {
        }

        public Task<BankCardTariffEntity> GetById(BankCardTariffId InBankCardTariffId)
        {
            return DbTable.FirstOrDefaultAsync(q => q.EntityId == InBankCardTariffId);
        }

        public Task<BankCardTariffEntity> Create(CreateBankCardTariffForm InForm)
        {
            return AddAndSaveAsync(new BankCardTariffEntity
            {
                PropertyId = InForm.EntityId,
                EntityId   = BankCardTariffId.GenerateId(),

                Name            = InForm.Name,
                Deposit         = InForm.Deposit,
                MaintenanceCost = InForm.MaintenanceCost,
                TariffCost      = InForm.TariffCost,
                Transfer        = InForm.Transfer,
                WithDraw        = InForm.WithDraw,
            });
        }

        public async Task<BankCardTariffEntity> Update(UpdateBankCardTariffForm InForm)
        {
            var tariff = DbTable.Local.First(q => q.EntityId == InForm.EntityId);
            tariff.Name            = InForm.Name;
            tariff.Deposit         = InForm.Deposit;
            tariff.MaintenanceCost = InForm.MaintenanceCost;
            tariff.TariffCost      = InForm.TariffCost;
            tariff.Transfer        = InForm.Transfer;
            tariff.WithDraw        = InForm.WithDraw;
            await DbContext.SaveChangesAsync();
            return tariff;
        }



    }
}