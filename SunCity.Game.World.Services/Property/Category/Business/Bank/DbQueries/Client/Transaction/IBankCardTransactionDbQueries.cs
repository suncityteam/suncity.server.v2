﻿using System.Threading.Tasks;
using SunCity.Game.World.DataBase.Entitites.Property.Business.Bank.Transaction;

namespace SunCity.Game.World.Services.Property.Category.Business.Bank.DbQueries.Client.Transaction
{
    public interface IBankCardTransactionDbQueries
    {
        Task<BankCardTransactionEntity[]> GetTransactions(BankCardId InBankCardId);
        Task<BankCardTransactionEntity> GetTransaction(BankCardTransactionId InBankCardTransactionId);

    }
}