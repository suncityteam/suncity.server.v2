﻿using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using SunCity.EntityFrameWork.Common;
using SunCity.Game.World.DataBase;
using SunCity.Game.World.DataBase.Entitites.Property.Business.Bank.Transaction;

namespace SunCity.Game.World.Services.Property.Category.Business.Bank.DbQueries.Client.Transaction
{
    internal class BankCardTransactionDbQueries : BaseDbQueries<GameWorldDbContext, BankCardTransactionEntity>, IBankCardTransactionDbQueries
    {
        protected override DbSet<BankCardTransactionEntity> DbTable => DbContext.BankCardTransactionEntity;

        public BankCardTransactionDbQueries(GameWorldDbContext InDbContext) : base(InDbContext)
        {
        }

        public Task<BankCardTransactionEntity[]> GetTransactions(BankCardId InBankCardId)
        {
            return DbTable
                            .Where(q => q.BankCardId == InBankCardId)
                            .OrderByDescending(q => q.CreatedDate)
                           
                            .Include(q => q.TransferFromTransaction)
                            .Include(q => q.TransferToTransaction)


                            .Include(q => q.PropertyPaymentFromTransaction)
                            .Include(q => q.PropertyPaymentToTransaction)

                            .ToArrayAsync();
        }

        public Task<BankCardTransactionEntity> GetTransaction(BankCardTransactionId InBankCardTransactionId)
        {
            return DbTable
                   .Include(q => q.TransferFromTransaction)
                   .Include(q => q.TransferToTransaction)
                   .FirstOrDefaultAsync(q => q.EntityId == InBankCardTransactionId);
        }
    }
}
