﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using SunCity.Game.World.Services.Property.Category.Business.Bank.DbQueries.Client.BankCard.Fraction;
using SunCity.Game.World.Services.Property.Category.Business.Bank.DbQueries.Client.BankCard.Owners;
using SunCity.Game.World.Services.Property.Category.Business.Bank.DbQueries.Client.BankCard.Player;
using SunCity.Game.World.Services.Property.Category.Business.Bank.DbQueries.Client.Payments;
using SunCity.Game.World.Services.Property.Category.Business.Bank.DbQueries.Client.Tariff;
using SunCity.Game.World.Services.Property.Category.Business.Bank.DbQueries.Client.Transaction;

namespace SunCity.Game.World.Services.Property.Category.Business.Bank.DbQueries
{
    internal static class ServiceRecorder
    {
        public static IServiceCollection AddBankClientDbQueries(this IServiceCollection InServiceCollection, IConfiguration InConfiguration)
        {
            
            InServiceCollection.AddScoped<IBankCardOwnersDbQueries, BankCardOwnersDbQueries>();

            InServiceCollection.AddScoped<IBankCardPlayerDbQueries, BankCardPlayerDbQueries>();
            InServiceCollection.AddScoped<IBankCardFractionDbQueries, BankCardFractionDbQueries>();
            InServiceCollection.AddScoped<IBankCardTransactionDbQueries, BankCardTransactionDbQueries>();

            InServiceCollection.AddScoped<IBankCardTariffDbQuries, BankCardTariffDbQuries>();
            InServiceCollection.AddScoped<IBankCardPropertyPaymentsDbQueries, BankCardPropertyPaymentsDbQueries>();

            return InServiceCollection;
        }
    }
}
