﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using SunCity.Game.World.Services.Property.Category.Business.Bank.DbQueries;
using SunCity.Game.World.Services.Property.Category.Business.Bank.Generators;
using SunCity.Game.World.Services.Property.Category.Business.Bank.Service;

namespace SunCity.Game.World.Services.Property.Category.Business.Bank
{
    public static class ServiceRecorder
    {
        public static IServiceCollection AddBankServices(this IServiceCollection InServiceCollection, IConfiguration InConfiguration)
        {
            InServiceCollection.AddBankClientDbQueries(InConfiguration);
            InServiceCollection.AddBankClientServices(InConfiguration);

            
            InServiceCollection.AddScoped<IBankCardNumberGenerator, BankCardNumberGenerator>();
            return InServiceCollection;
        }
    }
}
