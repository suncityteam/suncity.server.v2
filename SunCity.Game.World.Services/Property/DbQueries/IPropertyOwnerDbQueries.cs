﻿using System.Threading.Tasks;
using SunCity.Game.World.Services.Property.Forms;

namespace SunCity.Game.World.Services.Property.DbQueries
{
    public interface IPropertyOwnerDbQueries
    {
        Task RemoveOwners(PropertyId InPropertyId);
        Task AddOwner(AddPropertyOwnerDbForm InForm);
        Task RemoveFromSelling(RemovePropertyFromSellingDbForm InForm);
    }
}