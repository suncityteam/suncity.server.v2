﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using SunCity.Domain.Interfaces.Extensions;
using SunCity.EntityFrameWork.Common;
using SunCity.Game.Enums.World.Property.Owner;
using SunCity.Game.World.DataBase;
using SunCity.Game.World.DataBase.Entitites.Property.Owners;
using SunCity.Game.World.Services.Property.Forms;

namespace SunCity.Game.World.Services.Property.DbQueries
{
    internal class PropertyOwnerDbQueries : BaseDbQueries<GameWorldDbContext>, IPropertyOwnerDbQueries
    {
        public PropertyOwnerDbQueries(GameWorldDbContext InDbContext) : base(InDbContext)
        {
        }

        public async Task RemoveOwners(PropertyId InPropertyId)
        {
            var owners = await DbContext.PropertyOwnerEntity
                                        .Where(q => q.Type == PropertyOwnerType.Owner && q.End != null)
                                        .ToArrayAsync();

            foreach (var owner in owners)
            {
                owner.End = DateTime.UtcNow;
            }

            await DbContext.SaveChangesAsync();
        }

        public Task AddOwner(AddPropertyOwnerDbForm InForm)
        {
            DbContext.PropertyOwnerEntity.Add(new PropertyOwnerEntity
            {
                EntityId = PropertyOwnerId.GenerateId(),
                PropertyId = InForm.EntityId,

                Type = InForm.Type,
                Group = PropertyOwnerGroup.Player,

                Begin = DateTime.UtcNow,

                PlayerOwnerEntity = new PropertyPlayerOwnerEntity
                {
                    CharacterId = InForm.OwnerId
                }
            });
            return DbContext.SaveChangesAsync();
        }

        public async Task RemoveFromSelling(RemovePropertyFromSellingDbForm InForm)
        {
            var property = await DbContext.PropertyEntity.GetById(InForm.EntityId);

            property.IsSelling = false;
            property.OwnerBankCardId = InForm.BankCardId;

            await DbContext.SaveChangesAsync();
        }
    }
}
