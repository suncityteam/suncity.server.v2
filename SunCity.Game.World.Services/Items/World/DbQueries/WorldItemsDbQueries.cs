﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using SunCity.EntityFrameWork.Common;
using SunCity.Game.World.DataBase;
using SunCity.Game.World.DataBase.Entitites.Item.World;

namespace SunCity.Game.World.Services.Items.World.DbQueries
{
    public interface IWorldItemsDbQueries
    {
        Task<WorldItemEntity[]> GetByIds(IReadOnlyCollection<WorldItemId> InIds);
    }

    internal class WorldItemsDbQueries : BaseDbQueries<GameWorldDbContext, WorldItemEntity>, IWorldItemsDbQueries
    {
        protected override DbSet<WorldItemEntity> DbTable => DbContext.WorldItemEntity;

        public WorldItemsDbQueries(GameWorldDbContext InDbContext) : base(InDbContext)
        {
        }

        public Task<WorldItemEntity[]> GetByIds(IReadOnlyCollection<WorldItemId> InIds)
        {
            return DbTable
                   .Include(q => q.ChildEntities)
                   .ThenInclude(q => q.ChildEntities)
                   .Where(q => InIds.Contains(q.EntityId))
                   .ToArrayAsync();
        }

    }
}