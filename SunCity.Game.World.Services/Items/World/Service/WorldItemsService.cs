﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using SunCity.Game.World.DataBase.Entitites.Item.World;
using SunCity.Game.World.Models.Item.World;
using SunCity.Game.World.Services.Items.World.DbQueries;

namespace SunCity.Game.World.Services.Items.World.Service
{
    internal class WorldItemsService : IWorldItemsService
    {
        private IWorldItemsDbQueries DbQueries { get; }

        public WorldItemsService(IWorldItemsDbQueries InDbQueries)
        {
            DbQueries = InDbQueries;
        }

        public async Task<WorldItemModel[]> GetByIds(IReadOnlyCollection<WorldItemId> InIds)
        {
            var items = await DbQueries.GetByIds(InIds);
            var models = items.Select(MapToModel).ToArray();
            return models;
        }

        private static WorldItemModel MapToModel(WorldItemEntity InEntity)
        {
            return new WorldItemModel
            {
                EntityId = InEntity.EntityId,
                ModelId  = InEntity.ModelId,

                CreatedDate = InEntity.CreatedDate,
                Group       = InEntity.Group,

                Child = InEntity.ChildEntities.Select(MapToModel).ToArray(),
                Data = InEntity.Data
            };
        }

    }
}
