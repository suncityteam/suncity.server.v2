﻿using System.Collections.Generic;
using System.Threading.Tasks;
using SunCity.Game.World.Models.Item.World;

namespace SunCity.Game.World.Services.Items.World.Service
{
    public interface IWorldItemsService
    {
        Task<WorldItemModel[]> GetByIds(IReadOnlyCollection<WorldItemId> InIds);
    }
}