﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using SunCity.Game.World.Services.Items.World.DbQueries;
using SunCity.Game.World.Services.Items.World.Service;

namespace SunCity.Game.World.Services.Items
{
    public static class ServiceRecorder
    {
        public static IServiceCollection AddGameWorldItemsServices(this IServiceCollection InServiceCollection, IConfiguration InConfiguration)
        {
            InServiceCollection.AddScoped<IWorldItemsDbQueries, WorldItemsDbQueries>();
            InServiceCollection.AddScoped<IWorldItemsService, WorldItemsService>();

            return InServiceCollection;
        }
    }
}
