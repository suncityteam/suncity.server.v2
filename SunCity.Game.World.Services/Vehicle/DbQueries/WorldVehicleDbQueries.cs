﻿using System;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using SunCity.EntityFrameWork.Common;
using SunCity.Game.Enums.Vehicle;
using SunCity.Game.Types.Vehicle;
using SunCity.Game.Types.Vehicle.States;
using SunCity.Game.World.DataBase;
using SunCity.Game.World.DataBase.Entitites.Vehicle;
using SunCity.Game.World.Services.Vehicle.Models;

namespace SunCity.Game.World.Services.Vehicle.DbQueries
{
    internal class WorldVehicleDbQueries : BaseDbQueries<GameWorldDbContext, VehicleEntity>, IWorldVehicleDbQueries
    {
        protected override DbSet<VehicleEntity> DbTable => DbContext.VehicleEntity;

        public WorldVehicleDbQueries(GameWorldDbContext InDbContext) : base(InDbContext)
        {
        }

        public Task<VehicleEntity> CreateVehicle(CreateWorldVehicleModel model)
        {
            return AddAndSaveAsync(new VehicleEntity
            {
                EntityId = model.VehicleId,
                ModelId = model.Model,

                Color = new VehicleColor()
                {
                    FirstColor =model.FirstColor,
                    SecondColor = model.SecondColor
                },

                Health = new VehicleHealth(),

                PlateNumber = new VehiclePlateNumber()
                {
                    Number = DateTime.UtcNow.ToShortDateString()
                },

                Rotation = model.Rotation.Z,
                Position = model.Position,

                DoorsState = new VehicleDoorsState(),
                PartsState = VehiclePartsState.None,
                WheelsState = new VehicleWheelsState(),
                WindowsState = new VehicleWindowsState(),

                DirtyLevel = 0,
                Fuel = 40,
            });
        }

    }
}