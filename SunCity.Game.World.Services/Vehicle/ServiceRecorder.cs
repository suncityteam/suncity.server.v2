﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using SunCity.Game.World.Services.Vehicle.DbQueries;
using SunCity.Game.World.Services.Vehicle.Services;

namespace SunCity.Game.World.Services.Vehicle
{
    public static class ServiceRecorder
    {
        public static IServiceCollection AddGameWorldVehicleServices(this IServiceCollection InServiceCollection, IConfiguration InConfiguration)
        {
            InServiceCollection.AddScoped<IWorldVehicleServices, WorldVehicleServices>();
            InServiceCollection.AddScoped<IWorldVehicleDbQueries, WorldVehicleDbQueries>();
        
            return InServiceCollection;
        }
    }
}
