﻿
using GTANetworkAPI;

namespace SunCity.Game.World.Services.Vehicle.Models
{
    public class CreateWorldVehicleModel
    {
        public VehicleId VehicleId { get; set; }
        public VehicleHash Model { get; set; }
        
        public Vector3 Position { get; set; }
        public Vector3 Rotation { get; set; }

        public int FirstColor { get; set; }
        public int SecondColor { get; set; }
    }
}