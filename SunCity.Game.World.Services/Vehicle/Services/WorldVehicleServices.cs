﻿using System.Threading.Tasks;
using SunCity.Game.World.DataBase.Entitites.Vehicle;
using SunCity.Game.World.Services.Vehicle.DbQueries;
using SunCity.Game.World.Services.Vehicle.Models;

namespace SunCity.Game.World.Services.Vehicle.Services
{
    internal class WorldVehicleServices : IWorldVehicleServices
    {        
        private IWorldVehicleDbQueries WorldVehicleDbQueries { get; }

        public WorldVehicleServices(IWorldVehicleDbQueries worldVehicleDbQueries)
        {
            WorldVehicleDbQueries = worldVehicleDbQueries;
        }

        
        public async Task<VehicleEntity> CreateVehicle(CreateWorldVehicleModel model)
        {
            return await WorldVehicleDbQueries.CreateVehicle(model);
           
        }
    }
}