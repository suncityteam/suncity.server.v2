﻿using System.Threading.Tasks;
using SunCity.Game.World.DataBase.Entitites.Vehicle;
using SunCity.Game.World.Services.Vehicle.Models;

namespace SunCity.Game.World.Services.Vehicle.Services
{
    public interface IWorldVehicleServices
    {
        Task<VehicleEntity> CreateVehicle(CreateWorldVehicleModel model);
    }
}