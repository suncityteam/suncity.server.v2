﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using SunCity.EntityFrameWork.Common;
using SunCity.Game.World.DataBase;
using SunCity.Game.World.DataBase.Entitites.Item.World;
using SunCity.Game.World.DataBase.Entitites.Player.Inventory;

namespace SunCity.Game.World.Services.Character.Inventory.DbQueries
{
    public interface ICharacterInventoryDbQueries
    {
        Task<PlayerCharacterInventoryItemEntity[]> GetInventoryItems(PlayerCharacterId InCharacterId);
    }

    internal class CharacterInventoryDbQueries : BaseDbQueries<GameWorldDbContext, PlayerCharacterInventoryItemEntity>, ICharacterInventoryDbQueries
    {
        protected override DbSet<PlayerCharacterInventoryItemEntity> DbTable => DbContext.PlayerCharacterInventoryItemEntity;
        public CharacterInventoryDbQueries(GameWorldDbContext InDbContext) : base(InDbContext)
        {
        }

        public async Task<PlayerCharacterInventoryItemEntity[]> GetInventoryItems(PlayerCharacterId InCharacterId)
        {
            //if (await DbTable.AnyAsync(q => q.CharacterId == InCharacterId) == false)
            //{
            //    var items = await DbContext.ItemEntity.ToArrayAsync();
            //    foreach (var slot in DbContext.ClothingItemEntity
            //                                        .Include(q => q.ItemEntity).ToArray().GroupBy(q => q.Slot).ToArray())
            //    {
            //        var item = slot.First();
            //        DbTable.Add(new PlayerCharacterInventoryItemEntity
            //        {
            //            EntityId      = CharacterInventoryItemId.GenerateId(),
            //            CharacterId   = InCharacterId,
            //            InventorySlot = item.Slot,
            //            ItemEntity = new WorldItemEntity()
            //            {
            //                EntityId = WorldItemId.GenerateId(),
            //                Group = WorldItemGroup.Player,
            //                CreatedDate = DateTime.UtcNow,
            //                ModelId = item.ItemEntity.EntityId,
            //                Data = new WorldItemData(),
            //
            //                ChildEntities = items.Select(q => new WorldItemEntity
            //                {
            //                    EntityId = WorldItemId.GenerateId(),
            //                    ModelId = q.EntityId,
            //                    CreatedDate = DateTime.UtcNow,
            //                    Group = WorldItemGroup.Player,
            //                    Data = new WorldItemData()
            //                }).ToList()
            //            }
            //        });
            //    }
            //
            //    await DbContext.SaveChangesAsync();
            //}

            return await DbTable.Where(q => q.CharacterId == InCharacterId).ToArrayAsync();
        }
    }
}
