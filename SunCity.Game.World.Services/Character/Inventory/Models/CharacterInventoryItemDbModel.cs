﻿using SunCity.Domain.Interfaces.Entity;
using SunCity.Game.Enums.World.Player.Inventory;

namespace SunCity.Game.World.Services.Character.Inventory.Models
{
    internal class CharacterInventoryItemDbModel : IEntityWithId<CharacterInventoryItemId>
    {
        public CharacterInventoryItemId EntityId { get; set; }
        public PlayerInventorySlot InventorySlot { get; set; }
        public WorldItemId ItemId { get; set; }
    }
}
