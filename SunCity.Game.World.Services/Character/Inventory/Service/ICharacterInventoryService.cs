﻿using System.Threading.Tasks;
using SunCity.Common.Operation;
using SunCity.Game.World.Forms.Character.Inventory;
using SunCity.Game.World.Models.Character.Inventory;
using SunCity.Game.World.Player;

namespace SunCity.Game.World.Services.Character.Inventory.Service
{
    public interface ICharacterInventoryService
    {
        Task<OperationResponse> EquipItem(WorldPlayer InWorldPlayer, InventroryItemEquipForm InForm);
        Task<OperationResponse> MoveItemTo(WorldPlayer InWorldPlayer, InventroryItemMoveForm InForm);
        Task<OperationResponse<PlayerInventoryModel>> GetPlayerInventory(WorldPlayer InWorldPlayer);
        Task<OperationResponse<LoadCharacterInventory>> LoadInventory(PlayerCharacterId InPlayerCharacterId);
    }
}