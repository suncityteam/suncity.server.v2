﻿using System;
using System.Linq;
using System.Threading.Tasks;
using SunCity.Common.Operation;
using SunCity.Game.World.Forms.Character.Inventory;
using SunCity.Game.World.Models.Character.Inventory;
using SunCity.Game.World.Player;
using SunCity.Game.World.Services.Character.Inventory.DbQueries;
using SunCity.Game.World.Services.Items.World.Service;

namespace SunCity.Game.World.Services.Character.Inventory.Service
{
    internal class CharacterInventoryService : ICharacterInventoryService
    {
        private ICharacterInventoryDbQueries InventoryDbQueries { get; }
        private IWorldItemsService WorldItemsService { get; }

        public CharacterInventoryService(ICharacterInventoryDbQueries InInventoryDbQueries, IWorldItemsService InWorldItemsService)
        {
            InventoryDbQueries = InInventoryDbQueries;
            WorldItemsService = InWorldItemsService;
        }

        public async Task<OperationResponse> EquipItem(WorldPlayer InWorldPlayer, InventroryItemEquipForm InForm)
        {
            var moveItemInInventoryResponse = InWorldPlayer.Inventory.Equip(InForm);
            if (moveItemInInventoryResponse.IsNotCorrect)
                return moveItemInInventoryResponse;

            return OperationResponseCache.Successfully;
        }

        public async Task<OperationResponse> MoveItemTo(WorldPlayer InWorldPlayer, InventroryItemMoveForm InForm)
        {
            var moveItemInInventoryResponse = InWorldPlayer.Inventory.MoveItem(InForm);
            if (moveItemInInventoryResponse.IsNotCorrect)
                return moveItemInInventoryResponse;

            return OperationResponseCache.Successfully;
        }

        public Task<OperationResponse<PlayerInventoryModel>> GetPlayerInventory(WorldPlayer InWorldPlayer)
        {
            var model = new PlayerInventoryModel
            {
                Slots = InWorldPlayer.Inventory.ItemSlots.Select(q => new PlayerInventorySlotModel
                {
                    EntityId      = q.EntityId,
                    InventorySlot = q.InventorySlot,
                    Item = new PlayerInventoryItemModel
                    {
                        EntityId = q.Item.EntityId,
                        ModelId = q.ItemModel.EntityId,
                        Name = q.ItemModel.Name,

                        Child = q.Items.Select(w => new PlayerInventoryItemModel
                        {
                            EntityId = w.EntityId,
                            ModelId = w.ItemModel.EntityId,
                            Name = w.ItemModel.Name,
                            Child = Array.Empty<PlayerInventoryItemModel>()
                        }).ToArray()
                    }
                }).ToArray()
            };


            return Task.FromResult(new OperationResponse<PlayerInventoryModel>(model));
        }

        public async Task<OperationResponse<LoadCharacterInventory>> LoadInventory(PlayerCharacterId InPlayerCharacterId)
        {
            var slots = await InventoryDbQueries.GetInventoryItems(InPlayerCharacterId);
            var itemIds = slots.Select(q => q.ItemId).ToArray();
            var items = await WorldItemsService.GetByIds(itemIds);

            var model = new LoadCharacterInventory
            {
                Slots = slots.Select(q => new LoadCharacterInventoryItemForm
                {
                    EntityId      = q.EntityId,
                    InventorySlot = q.InventorySlot,
                    Item          = items.First(w => w.EntityId == q.ItemId)
                }).ToArray()
            };

            return model;
        }
    }
}
