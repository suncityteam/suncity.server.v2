﻿using System;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using SunCity.EntityFrameWork.Common;
using SunCity.Game.World.DataBase;
using SunCity.Game.World.DataBase.Entitites.Player.Licenses;

namespace SunCity.Game.World.Services.Character.Licenses.DbQueries
{
    internal class PlayerLicensesDbQueries : BaseDbQueries<GameWorldDbContext, PlayerLicenseEntity>, IPlayerLicensesDbQueries
    {
        protected override DbSet<PlayerLicenseEntity> DbTable => DbContext.PlayerLicenseEntity;

        public PlayerLicensesDbQueries(GameWorldDbContext InDbContext) : base(InDbContext)
        {
        }
        
        public Task<bool> IsExist(PlayerLicenseId playerLicenseId)
        {
            return DbTable.AnyAsync(q => q.EntityId == playerLicenseId);
        }

        public Task<bool> IsExist(PlayerCharacterId playerCharacterId, LicenseId licenseId)
        {
            return DbTable.AnyAsync(q => q.CharacterId == playerCharacterId && q.LicenseId == licenseId && q.ExpiredDate < DateTime.UtcNow);
        }


    }
}