﻿using System.Threading.Tasks;

namespace SunCity.Game.World.Services.Character.Licenses.DbQueries
{
    public interface IPlayerLicensesDbQueries
    {
        Task<bool> IsExist(PlayerLicenseId playerLicenseId);
        Task<bool> IsExist(PlayerCharacterId playerCharacterId, LicenseId licenseId);
    }
}