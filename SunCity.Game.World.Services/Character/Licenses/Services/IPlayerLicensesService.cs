﻿using System.Threading.Tasks;

namespace SunCity.Game.World.Services.Character.Licenses.Services
{
    public interface IPlayerLicensesService
    {
        Task<bool> IsExist(PlayerLicenseId playerLicenseId);
        Task<bool> IsExist(PlayerCharacterId playerCharacterId, LicenseId licenseId);

    }
}