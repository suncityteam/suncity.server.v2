﻿using System.Threading.Tasks;
using SunCity.Game.World.Services.Character.Licenses.DbQueries;

namespace SunCity.Game.World.Services.Character.Licenses.Services
{
    internal class PlayerLicensesService : IPlayerLicensesService
    {      
        private IPlayerLicensesDbQueries DbQueries { get; }
        
        public PlayerLicensesService(IPlayerLicensesDbQueries dbQueries)
        {
            DbQueries = dbQueries;
        }

        
        public Task<bool> IsExist(PlayerLicenseId playerLicenseId)
        {
            return DbQueries.IsExist(playerLicenseId);
        }

        public Task<bool> IsExist(PlayerCharacterId playerCharacterId, LicenseId licenseId)
        {
            return DbQueries.IsExist(playerCharacterId, licenseId);
        }
    }
}