﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using SunCity.Game.World.Services.Character.Inventory.DbQueries;
using SunCity.Game.World.Services.Character.Inventory.Service;
using SunCity.Game.World.Services.Character.Licenses.DbQueries;
using SunCity.Game.World.Services.Character.Licenses.Services;

namespace SunCity.Game.World.Services.Character
{
    public static class ServiceRecorder
    {
        public static IServiceCollection AddGameWorldCharacterServices(this IServiceCollection InServiceCollection, IConfiguration InConfiguration)
        {
            InServiceCollection.AddScoped<ICharacterInventoryDbQueries, CharacterInventoryDbQueries>();
            InServiceCollection.AddScoped<ICharacterInventoryService, CharacterInventoryService>();

            InServiceCollection.AddScoped<IPlayerLicensesDbQueries, PlayerLicensesDbQueries>();
            InServiceCollection.AddScoped<IPlayerLicensesService, PlayerLicensesService>();
            
            return InServiceCollection;
        }
    }
}
