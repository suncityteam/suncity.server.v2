﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using SunCity.Common;
using SunCity.Common.Operation;
using SunCity.Game.Types.Player.Entity.Player;
using SunCity.Identity.DataBase.Keys;

namespace SunCity.Game.Identity.Services.Sessions
{
    internal class GameSessionManager : IGameSessionManager
    {
        private Task ValidationTask { get; }
        private CancellationTokenSource CancellationToken { get; }

        private ConcurrentDictionary<AccountId, GameSession> Sessions { get; } = new ConcurrentDictionary<AccountId, GameSession>(EnvironmentNames.ConcurrencyLevel, short.MaxValue);
        private readonly object Lock = new object();

        public GameSessionManager()
        {
            CancellationToken = new CancellationTokenSource();
            ValidationTask = Task.Factory.StartNew(OnValidateSessions, TaskCreationOptions.LongRunning);
        }

        public GameSession GetSession(AccountId InAccountId)
        {
            lock (Lock)
            {
                var session = Sessions[InAccountId];
                return session;
            }
        }
        
        public void InvalidateSession(AccountId InAccountId)
        {
            lock (Lock)
            {
                var sessions = ((IDictionary<AccountId, GameSession>) Sessions);
                sessions.Remove(InAccountId);
            }
        }

        public bool HasActiveSession(AccountId accountId)
        {
            lock (Lock)
            {
                if (Sessions.TryGetValue(accountId, out var session))
                {
                    return session.IsActive();
                }
                return false;
            }
        }

        public void StartGameSession(AccountId accountId, PlayerAccountLogin login)
        {
            lock (Lock)
            {
                Sessions.GetOrAdd(accountId, InId => new GameSession(accountId, login)).UpdateLastActivity();
            }
        }

        public OperationResponse ProcessGameSession(AccountId accountId)
        {
            lock (Lock)
            {
                if (Sessions.TryGetValue(accountId, out var session))
                {
                    session.UpdateLastActivity();
                    return OperationResponseCache.Successfully;
                }
            }

            return OperationResponseCache.UnknownError;
        }

        private void OnValidateSessionsProcess()
        {
            var nonActiveSessions = Sessions
                .Where(q => q.Value.IsActive())
                .ToArray();

            var sessions = ((IDictionary<AccountId, GameSession>) Sessions);
            foreach (var session in nonActiveSessions)
            {
                sessions.Remove(session.Key);
            }
        }

        private async Task OnValidateSessions()
        {
            while (CancellationToken.IsCancellationRequested == false)
            {
                await Task.Delay(60 * 1000);

                lock (Lock)
                {
                    OnValidateSessionsProcess();
                }
            }
        }

        public void Dispose()
        {
            CancellationToken.Cancel();
            ValidationTask?.Dispose();
        }
    }
}