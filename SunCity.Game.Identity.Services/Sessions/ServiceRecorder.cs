﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace SunCity.Game.Identity.Services.Sessions
{
    public static class ServiceRecorder
    {
        public static IServiceCollection AddGameSessionsServices(this IServiceCollection InServiceCollection, IConfiguration InConfiguration)
        {           
            InServiceCollection.AddScoped<GameSessionProvider>();
            InServiceCollection.AddSingleton<IGameSessionManager, GameSessionManager>();
            
            InServiceCollection.AddScoped(provider => provider.GetRequiredService<GameSessionProvider>().Provide());
            
            return InServiceCollection;
        }
    }
}
