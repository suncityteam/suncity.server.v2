﻿using SunCity.Common.RageMp.Pools.Players;
using SunCity.Identity.DataBase.Keys;

namespace SunCity.Game.Identity.Services.Sessions
{
    internal class GameSessionProvider
    {
        private IRagePlayer Player { get; }
        private IGameSessionManager SessionManager { get; }
        
        public GameSessionProvider(IRagePlayer player, IGameSessionManager sessionManager)
        {
            Player = player;
            SessionManager = sessionManager;
        }
        
        public IGameSession Provide()
        {
            var accountId = Player.GetData<AccountId>(nameof(AccountId));
            if (accountId.IsValid)
            {
                var session = SessionManager.GetSession(accountId);
                session.UpdateLastActivity();
                return session;
            }

            return null;
        }
    }
}