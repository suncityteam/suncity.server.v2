﻿using System;

namespace SunCity.Game.Identity.Services.Sessions
{
    public class GameSessionSettings
    {
        public static TimeSpan Timeout { get; set; } = TimeSpan.FromSeconds(10);
    }
}