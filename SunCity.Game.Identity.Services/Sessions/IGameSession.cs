﻿using System;
using SunCity.Game.Types.Player.Entity.Player;
using SunCity.Identity.DataBase.Keys;

namespace SunCity.Game.Identity.Services.Sessions
{
    public interface IGameSession
    {
        AccountId AccountId { get; }
        PlayerAccountLogin AccountLogin { get; }
        DateTime LastActivityDate { get; }
    }
}