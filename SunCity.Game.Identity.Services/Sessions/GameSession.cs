﻿using System;
using System.Threading;
using SunCity.Game.Types.Player.Entity.Player;
using SunCity.Identity.DataBase.Keys;

namespace SunCity.Game.Identity.Services.Sessions
{
    public class GameSession : IGameSession
    {
        public GameSession(AccountId accountId, PlayerAccountLogin login)
        {
            AccountId = accountId;
            AccountLogin = login;
        }

        public AccountId AccountId { get; }
        public PlayerAccountLogin AccountLogin { get; }
        public DateTime LastActivityDate { get; private set; }
        private ReaderWriterLockSlim Lock { get; } = new ReaderWriterLockSlim();

        public void UpdateLastActivity()
        {
            Lock.EnterWriteLock();
            LastActivityDate = DateTime.UtcNow;
            Lock.ExitWriteLock();
        }

        public bool IsActive()
        {
            Lock.EnterReadLock();
            var isActive = DateTime.UtcNow - LastActivityDate < GameSessionSettings.Timeout;
            Lock.ExitReadLock();

            return isActive;
        }
    }
}