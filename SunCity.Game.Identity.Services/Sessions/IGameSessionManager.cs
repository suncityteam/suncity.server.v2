﻿using System;
using SunCity.Common.Operation;
using SunCity.Game.Types.Player.Entity.Player;
using SunCity.Identity.DataBase.Keys;

namespace SunCity.Game.Identity.Services.Sessions
{
    public interface IGameSessionManager : IDisposable
    {
        GameSession GetSession(AccountId InAccountId);
        void InvalidateSession(AccountId InAccountId);
        bool HasActiveSession(AccountId accountId);
        void StartGameSession(AccountId accountId, PlayerAccountLogin login);
        OperationResponse ProcessGameSession(AccountId accountId);
    }
}