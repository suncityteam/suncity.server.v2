﻿
using SunCity.Game.Identity.Forms.Character.Create;
using SunCity.Identity.DataBase.Keys;
using GTANetworkAPI;

namespace SunCity.Game.Identity.Services.Character.Forms
{
    public class CreateCharacterDbForm 
    {
        public AccountId AccountId { get; set; }

        public Vector3 SpawnPosition { get; set; }
        public long VirtualWorld { get; set; }

        public CreateCharacterFacialFeatures FacialFeatures { get; set; }
        public CreateCharacterHairAndColor Hairs { get; set; }
        public CreateCharacterGeneralInformation Character { get; set; }
        public CreateCharacterPlayerName Name { get; set; }        
    }
}
