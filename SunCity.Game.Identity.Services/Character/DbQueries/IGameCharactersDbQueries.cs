﻿using System.Collections.Generic;
using System.Threading.Tasks;
using JetBrains.Annotations;
using SunCity.Game.Identity.Models.Character;
using SunCity.Game.Identity.Services.Character.Enums;
using SunCity.Game.Identity.Services.Character.Forms;
using SunCity.Game.World.DataBase.Entitites.Player.Character;
using SunCity.Identity.DataBase.Keys;

namespace SunCity.Game.Identity.Services.Character.DbQueries
{
    public interface IGameCharactersDbQueries
    {
        [ItemNotNull]
        Task<PlayerCharacterEntity[]> GetList(AccountId InPlayerAccountId, IncludeCharacterProps InIncludeCharacterProps);
        Task<PlayerCharacterEntity> GetById(PlayerCharacterId InPlayerCharacterId, IncludeCharacterProps InIncludeCharacterProps);
        Task<PlayerCharacterEntity> Create(CreateCharacterDbForm InForm);
        Task UpdateDefaultCharacter(AccountId InPlayerAccountId, PlayerCharacterId InPlayerCharacterId);

        Task<GameCharacterNameModel[]> GetNames(IReadOnlyCollection<PlayerCharacterId> InCharacterIds);

    }
}