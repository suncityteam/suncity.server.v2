﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using SunCity.EntityFrameWork.Common;
using SunCity.Game.Identity.Models.Character;
using SunCity.Game.Identity.Services.Character.Enums;
using SunCity.Game.Identity.Services.Character.Forms;
using SunCity.Game.Types.Player.Components;
using SunCity.Game.Types.Player.Entity.Player.Character;
using SunCity.Game.World.DataBase;
using SunCity.Game.World.DataBase.Entitites.Player.Character;
using SunCity.Identity.DataBase.Keys;

namespace SunCity.Game.Identity.Services.Character.DbQueries
{
    internal class GameCharactersDbQueries : BaseDbQueries<GameWorldDbContext, PlayerCharacterEntity>, IGameCharactersDbQueries
    {
        protected override DbSet<PlayerCharacterEntity> DbTable => DbContext.PlayerCharacterEntity;
        public GameCharactersDbQueries(GameWorldDbContext InDbContext) : base(InDbContext)
        {
        }

        public Task<PlayerCharacterEntity[]> GetList(AccountId InPlayerAccountId, IncludeCharacterProps InIncludeCharacterProps)
        {
            var query = DbTable.AsQueryable();

            if (InIncludeCharacterProps.HasFlag(IncludeCharacterProps.Skin))
                query = query.Include(q => q.Skin);

            if (InIncludeCharacterProps.HasFlag(IncludeCharacterProps.Experience))
                query = query.Include(q => q.Experience);

            if (InIncludeCharacterProps.HasFlag(IncludeCharacterProps.Account))
                query = query.Include(q => q.PlayerAccountEntity);

            return query
                   .Where(q => q.PlayerAccountId == InPlayerAccountId)
                   .Include(q => q.Experience)
                   .Include(q => q.Skin)
                   .ToArrayAsync();
        }

        public Task<PlayerCharacterEntity> GetById(PlayerCharacterId InPlayerCharacterId, IncludeCharacterProps InIncludeCharacterProps)
        {
            var query = DbTable.AsQueryable();

            if (InIncludeCharacterProps.HasFlag(IncludeCharacterProps.Skin))
                query = query.Include(q => q.Skin);

            if (InIncludeCharacterProps.HasFlag(IncludeCharacterProps.Experience))
                query = query.Include(q => q.Experience);

            if (InIncludeCharacterProps.HasFlag(IncludeCharacterProps.Account))
                query = query.Include(q => q.PlayerAccountEntity);

            return query
                .FirstOrDefaultAsync(q => q.EntityId == InPlayerCharacterId);
        }

        public Task<PlayerCharacterEntity> Create(CreateCharacterDbForm InForm)
        {
            return AddAndSaveAsync(new PlayerCharacterEntity()
            {
                Name = new PlayerCharacterName($"{InForm.Name.FirstName}_{InForm.Name.LastName}"),

                EntityId = PlayerCharacterId.GenerateId(),
                PlayerAccountId = InForm.AccountId,
                Position = InForm.SpawnPosition,

                CreatedDate = DateTime.UtcNow,
                Health = Health.Default,
                Hunger = Hunger.Default,
                Thirst = Hunger.Default,

                Skin = CreteCharacterSkin(InForm),
                Experience = new PlayerCharacterExperienceEntity
                {
                    Level = 0,
                    Experience = 0,
                    PlayedForPayDay = TimeSpan.Zero,
                    PlayedTotalTime = TimeSpan.Zero,
                }
            });
        }

        public async Task UpdateDefaultCharacter(AccountId InPlayerAccountId, PlayerCharacterId InPlayerCharacterId)
        {
            var account = await DbContext.PlayerAccountEntity.FirstAsync(q => q.EntityId == InPlayerAccountId);
            account.DefaultCharacterId = InPlayerCharacterId;
            await DbContext.SaveChangesAsync();
        }

        public Task<GameCharacterNameModel[]> GetNames(IReadOnlyCollection<PlayerCharacterId> InCharacterIds)
        {
            var uniqueIds = InCharacterIds.Distinct().ToArray();
            return DbTable.Where(q => uniqueIds.Contains(q.EntityId))
                          .Select(q => new GameCharacterNameModel
                          {
                              EntityId = q.EntityId,
                              Name = q.Name
                          }).ToArrayAsync();
        }

        private static PlayerCharacterSkinEntity CreteCharacterSkin(CreateCharacterDbForm InForm)
        {
            return new PlayerCharacterSkinEntity
            {
                //==============================
                Sex = InForm.Character.Sex,

                //==============================
                NoseWidth = InForm.FacialFeatures.NoseWidth,
                NoseHeight = InForm.FacialFeatures.NoseHeight,
                NoseLength = InForm.FacialFeatures.NoseLength,
                NoseBridge = InForm.FacialFeatures.NoseBridge,
                NoseTip = InForm.FacialFeatures.NoseTip,
                NoseShift = InForm.FacialFeatures.NoseShift,

                BrowHeight = InForm.FacialFeatures.BrowHeight,
                BrowWidth = InForm.FacialFeatures.BrowWidth,

                CheekboneHeight = InForm.FacialFeatures.CheekboneHeight,
                CheekboneWidth = InForm.FacialFeatures.CheekboneWidth,
                CheeksWidth = InForm.FacialFeatures.CheeksWidth,

                Eyes = InForm.FacialFeatures.Eyes,
                Lips = InForm.FacialFeatures.Lips,

                JawWidth = InForm.FacialFeatures.JawWidth,
                JawHeight = InForm.FacialFeatures.JawHeight,

                ChinLength = InForm.FacialFeatures.ChinLength,
                ChinPosition = InForm.FacialFeatures.ChinPosition,
                ChinWidth = InForm.FacialFeatures.ChinWidth,
                ChinShape = InForm.FacialFeatures.ChinShape,

                NeckWidth = InForm.FacialFeatures.NeckWidth,

                //==============================
                HairModel = InForm.Hairs.Hair,
                FirstHairColor = InForm.Hairs.HairColor,

                EyebrowsModel = InForm.Hairs.Eyebrows,
                EyebrowsColor = InForm.Hairs.EyebrowsColor,

                EyesColor = InForm.Hairs.EyeColor,

                LipstickModel = InForm.Hairs.Lipstick,
                LipstickColor = InForm.Hairs.LipstickColor,

                BeardModel = InForm.Hairs.Eyebrows,
                BeardColor = InForm.Hairs.EyebrowsColor,

                //==============================
                FirstHeadShape = InForm.Character.FatherId,
                SecondHeadShape = InForm.Character.MotherId,

                SkinMix = InForm.Character.Skin,
                SundamageModel = InForm.Character.Sundamage,
                HeadMix = InForm.Character.Similarity,
                FrecklesModel = InForm.Character.Freckles,
            };
        }
    }
}
