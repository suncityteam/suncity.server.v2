﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using SunCity.Game.Identity.Services.Character.Cache;
using SunCity.Game.Identity.Services.Character.DbQueries;
using SunCity.Game.Identity.Services.Character.Services;

namespace SunCity.Game.Identity.Services.Character
{
    public static class ServiceRecorder
    {
        public static IServiceCollection AddGameCharacterServices(this IServiceCollection InServiceCollection, IConfiguration InConfiguration)
        {
            InServiceCollection.AddSingleton<IGameCharactersNameCacheService, GameCharactersNameCacheService>();

            InServiceCollection.AddScoped<IGameCharactersDbQueries, GameCharactersDbQueries>();
            InServiceCollection.AddScoped<IGameCharactersService, GameCharactersService>();

            InServiceCollection.AddScoped<IGameCharactersNameService, GameCharactersNameService>();
            
            return InServiceCollection;
        }
    }
}
