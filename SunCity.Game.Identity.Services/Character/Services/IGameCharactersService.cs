﻿using System.Threading.Tasks;
using JetBrains.Annotations;
using SunCity.Game.Identity.Models.Character;
using SunCity.Identity.DataBase.Keys;

namespace SunCity.Game.Identity.Services.Character.Services
{
    public interface IGameCharactersService
    {
        [ItemNotNull]
        Task<GameCharacterModel[]> GetList(AccountId InAccountId);
    }
}