﻿using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using SunCity.Game.Enums.Character;
using SunCity.Game.Identity.Models.Character;
using SunCity.Game.Identity.Services.Character.DbQueries;
using SunCity.Game.Identity.Services.Character.Enums;
using SunCity.Identity.DataBase.Keys;

namespace SunCity.Game.Identity.Services.Character.Services
{
    internal class GameCharactersService : IGameCharactersService
    {
        private IMapper Mapper { get; }
        private IGameCharactersDbQueries GameCharactersDbQueries { get; }

        public GameCharactersService(IGameCharactersDbQueries InGameCharactersDbQueries, IMapper InMapper)
        {
            GameCharactersDbQueries = InGameCharactersDbQueries;
            Mapper = InMapper;
        }

        public async Task<GameCharacterModel[]> GetList(AccountId InAccountId)
        {
            var characterEntities = await GameCharactersDbQueries.GetList(InAccountId, IncludeCharacterProps.Experience | IncludeCharacterProps.Skin);
            return characterEntities.Select(q => new GameCharacterModel
            {
                EntityId = q.EntityId,
                Name = q.Name,
                FractionName = string.Empty,
                RegistrationDate = q.CreatedDate,
                PlayedTime = q.Experience.PlayedTotalTime,

                Level = q.Experience.Level,
                Experience = q.Experience.Experience,
                NextLevelExperience = 1000 + q.Experience.Level * 1500,

                Sex = GameCharacterSex.Male,
            }).ToArray();
        }
    }
}
