﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using SunCity.Common.Extensions;
using SunCity.Common.Operation;
using SunCity.Game.Identity.Models.Character;
using SunCity.Game.Identity.Services.Character.Cache;
using SunCity.Game.Identity.Services.Character.DbQueries;
using SunCity.Game.Identity.Services.Character.Enums;

namespace SunCity.Game.Identity.Services.Character.Services
{
    internal class GameCharactersNameService : IGameCharactersNameService
    {
        private IGameCharactersDbQueries GameCharactersDbQueries { get; }
        private IGameCharactersNameCacheService CharactersNameCacheService { get; }

        public GameCharactersNameService(IGameCharactersDbQueries InGameCharactersDbQueries, IGameCharactersNameCacheService InCharactersNameCacheService)
        {
            GameCharactersDbQueries = InGameCharactersDbQueries;
            CharactersNameCacheService = InCharactersNameCacheService;
        }

        public async Task<OperationResponse<GameCharacterNameModel>> GetById(PlayerCharacterId InCharacterId)
        {
            if (CharactersNameCacheService.TryGetValue(InCharacterId, out var playerCharacterName))
                return playerCharacterName;

            var character = await GameCharactersDbQueries.GetById(InCharacterId, IncludeCharacterProps.None);
            if (character == null)
                return (OperationResponseCode.PlayerCharacterNotFound, InCharacterId);

            CharactersNameCacheService.Put(character.EntityId, character.Name);

            return new GameCharacterNameModel
            {
                EntityId = character.EntityId,
                Name = character.Name
            };
        }

        public async Task<IReadOnlyList<GameCharacterNameModel>> GetByIds(IReadOnlyCollection<PlayerCharacterId> InCharacterIds)
        {
            var uniqueIds = InCharacterIds.DistinctToArray();
            var names = CharactersNameCacheService.TryGetValues(uniqueIds, out var notCachedCharacterIds);
            
            if (notCachedCharacterIds.Any())
            {
                var namesForCache = await GameCharactersDbQueries.GetNames(notCachedCharacterIds);
                foreach (var name in namesForCache)
                {
                    CharactersNameCacheService.Put(name.EntityId, name.Name);
                }

                return namesForCache.Concat(names).ToArray();
            }

            return names;
        }
    }
}