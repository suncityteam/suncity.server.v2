﻿using System.Collections.Generic;
using System.Threading.Tasks;
using SunCity.Common.Operation;
using SunCity.Game.Identity.Models.Character;

namespace SunCity.Game.Identity.Services.Character.Services
{
    public interface IGameCharactersNameService
    {
        Task<OperationResponse<GameCharacterNameModel>> GetById(PlayerCharacterId InCharacterId);
        Task<IReadOnlyList<GameCharacterNameModel>> GetByIds(IReadOnlyCollection<PlayerCharacterId> InCharacterIds);

    }
}