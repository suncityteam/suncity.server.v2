﻿using System.Collections.Generic;
using Microsoft.Extensions.Caching.Memory;
using SunCity.Game.Identity.Models.Character;
using SunCity.Game.Types.Player.Entity.Player.Character;

namespace SunCity.Game.Identity.Services.Character.Cache
{
    internal class GameCharactersNameCacheService : IGameCharactersNameCacheService
    {
        private IMemoryCache MemoryCache { get; }

        public GameCharactersNameCacheService(IMemoryCache InMemoryCache)
        {
            MemoryCache = InMemoryCache;
        }

        public IReadOnlyList<GameCharacterNameModel> TryGetValues(IReadOnlyCollection<PlayerCharacterId> InCharacterIds, out IReadOnlyCollection<PlayerCharacterId> InNotCachedCharacterIds)
        {
            var names = new List<GameCharacterNameModel>(InCharacterIds.Count);
            var notCachedCharacterIds = new List<PlayerCharacterId>(InCharacterIds.Count);

            foreach (var characterId in InCharacterIds)
            {
                if (TryGetValue(characterId, out var characterNameModel))
                {
                    names.Add(characterNameModel);
                }
                else
                {
                    notCachedCharacterIds.Add(characterId);
                }
            }

            InNotCachedCharacterIds = notCachedCharacterIds;
            return names;
        }

        public bool TryGetValue(PlayerCharacterId InCharacterId, out GameCharacterNameModel InCharacterName)
        {
            if(MemoryCache.TryGetValue<PlayerCharacterName>(InCharacterId, out var characterName))
            {
                InCharacterName = new GameCharacterNameModel
                {
                    EntityId = InCharacterId,
                    Name     = characterName
                };

                return true;
            }

            InCharacterName = null;
            return false;
        }

        public void Put(PlayerCharacterId InCharacterId, PlayerCharacterName InCharacterName)
        {
            MemoryCache.Set(InCharacterId, InCharacterName);
        }
    }
}