﻿using System.Collections.Generic;
using SunCity.Game.Identity.Models.Character;
using SunCity.Game.Types.Player.Entity.Player.Character;

namespace SunCity.Game.Identity.Services.Character.Cache
{
    public interface IGameCharactersNameCacheService
    {
        IReadOnlyList<GameCharacterNameModel> TryGetValues(IReadOnlyCollection<PlayerCharacterId> InCharacterIds, out IReadOnlyCollection<PlayerCharacterId> InNotCachedCharacterIds);
        bool TryGetValue(PlayerCharacterId InCharacterId, out GameCharacterNameModel InCharacterName);
        void Put(PlayerCharacterId InCharacterId, PlayerCharacterName InCharacterName);
    }
}
