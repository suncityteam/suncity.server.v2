﻿using System;

namespace SunCity.Game.Identity.Services.Character.Enums
{
    [Flags]
    public enum IncludeCharacterProps
    {
        None = 0,
        Skin = 1 << 1,
        Experience = 1 << 2,
        Account = 1 << 3,
    }
}
