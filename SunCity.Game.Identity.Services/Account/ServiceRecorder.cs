﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using SunCity.Game.Identity.Services.Account.DbQueries;
using SunCity.Game.Identity.Services.Account.Service;

namespace SunCity.Game.Identity.Services.Account
{
    public static class ServiceRecorder
    {
        public static IServiceCollection AddGameAccountServices(this IServiceCollection InServiceCollection, IConfiguration InConfiguration)
        {
            InServiceCollection.AddScoped<IGameAccountDbQueries, GameAccountDbQueries>();
            InServiceCollection.AddScoped<IGameAccountService, GameAccountService>();
            return InServiceCollection;
        }
    }
}
