﻿using System;
using SunCity.Game.Types.Player.Entity.Player;
using SunCity.Identity.DataBase.Keys;

namespace SunCity.Game.Identity.Services.Account.Forms
{
    public class CreateGameAccountForm
    {
        public AccountId AccountId { get; set; }
        public PlayerAccountLogin AccountLogin { get; set; }
        public string DeviceId { get; set; }
        public long Balance { get; set; }

        public TimeSpan PremiunAccount { get; set; }
    }
}
