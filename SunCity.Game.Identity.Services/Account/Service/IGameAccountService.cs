﻿using System.Threading.Tasks;
using SunCity.Common.Operation;
using SunCity.Game.Identity.Services.Account.Forms;
using SunCity.Identity.DataBase.Keys;

namespace SunCity.Game.Identity.Services.Account.Service
{
    public interface IGameAccountService
    {
        Task<bool> IsExist(AccountId InAccountId);
        Task<OperationResponse> Create(CreateGameAccountForm InForm);

    }
}