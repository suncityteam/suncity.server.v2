﻿using System.Threading.Tasks;
using SunCity.Common.Operation;
using SunCity.Game.Identity.Services.Account.DbQueries;
using SunCity.Game.Identity.Services.Account.Forms;
using SunCity.Game.Identity.Services.Character.Services;
using SunCity.Identity.DataBase.Keys;

namespace SunCity.Game.Identity.Services.Account.Service
{
    internal class GameAccountService : IGameAccountService
    {
        private IGameCharactersService CharactersService { get; }
        private IGameAccountDbQueries AccountDbQueries { get; }

        public GameAccountService(IGameCharactersService InCharactersService, IGameAccountDbQueries InAccountDbQueries)
        {
            CharactersService = InCharactersService;
            AccountDbQueries = InAccountDbQueries;
        }

        public async Task<OperationResponse> Create(CreateGameAccountForm InForm)
        {
            var existGameAccount = await AccountDbQueries.IsExist(InForm.AccountId);
            if (existGameAccount)
                return OperationResponseCode.GameAccountIsExist;

            await AccountDbQueries.Create(InForm);
            return OperationResponseCache.Successfully;
        }

        public Task<bool> IsExist(AccountId InAccountId)
        {
            return AccountDbQueries.IsExist(InAccountId);
        }
    }
}