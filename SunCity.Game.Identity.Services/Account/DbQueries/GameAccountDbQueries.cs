﻿using System;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using SunCity.EntityFrameWork.Common;
using SunCity.Game.Identity.Services.Account.Forms;
using SunCity.Game.World.DataBase;
using SunCity.Game.World.DataBase.Entitites.Player;
using SunCity.Identity.DataBase.Keys;

namespace SunCity.Game.Identity.Services.Account.DbQueries
{
    internal class GameAccountDbQueries : BaseDbQueries<GameWorldDbContext, PlayerAccountEntity>, IGameAccountDbQueries
    {
        protected override DbSet<PlayerAccountEntity> DbTable => DbContext.PlayerAccountEntity;
        public GameAccountDbQueries(GameWorldDbContext InDbContext)
            : base(InDbContext)
        {

        }

        public Task<PlayerAccountEntity> Create(CreateGameAccountForm InForm)
        {
            return AddAndSaveAsync(new PlayerAccountEntity
            {
                EntityId = InForm.AccountId,
                Login = InForm.AccountLogin,

                Balance = InForm.Balance,
                DeviceId = InForm.DeviceId,

                PremiumAccountEndDate = DateTime.UtcNow.Add(InForm.PremiunAccount),
                CreatedDate = DateTime.UtcNow
            });
        }

        public Task<PlayerAccountEntity> GetById(AccountId InAccountId)
        {
            return DbTable.FirstOrDefaultAsync(q => q.EntityId == InAccountId);
        }

        public Task<bool> IsExist(AccountId InAccountId)
        {
            return DbTable.AnyAsync(q => q.EntityId == InAccountId);
        }

    }
}
