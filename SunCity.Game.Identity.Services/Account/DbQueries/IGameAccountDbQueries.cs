﻿using System.Threading.Tasks;
using SunCity.Game.Identity.Services.Account.Forms;
using SunCity.Game.World.DataBase.Entitites.Player;
using SunCity.Identity.DataBase.Keys;

namespace SunCity.Game.Identity.Services.Account.DbQueries
{
    public interface IGameAccountDbQueries
    {
        Task<PlayerAccountEntity> Create(CreateGameAccountForm InForm);
        Task<PlayerAccountEntity> GetById(AccountId InAccountId);
        Task<bool> IsExist(AccountId InAccountId);
    }
}