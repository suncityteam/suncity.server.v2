﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using SunCity.Game.Identity.Services.Account;
using SunCity.Game.Identity.Services.Character;
using SunCity.Game.Identity.Services.Sessions;

namespace SunCity.Game.Identity.Services
{
    public static class ServiceRecorder
    {
        public static IServiceCollection AddGameIdentityServices(this IServiceCollection InServiceCollection, IConfiguration InConfiguration)
        {
            InServiceCollection.AddGameCharacterServices(InConfiguration);
            InServiceCollection.AddGameSessionsServices(InConfiguration);
            InServiceCollection.AddGameAccountServices(InConfiguration);

            return InServiceCollection;
        }
    }
}