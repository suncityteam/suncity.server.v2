﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using SunCity.Domain.Interfaces.Entity;
using SunCity.Domain.Interfaces.Types;

namespace SunCity.Domain.Interfaces.Extensions
{
    public static class EntityExtensions
    {
        public static Task<Dictionary<TKey, T>> ToContainsDictionaryAsync<TKey, T>(this IQueryable<T> InCollection, IReadOnlyCollection<TKey> InKeys) where T : IEntityWithId<TKey>
        {
            return InCollection.Where(q => InKeys.Contains(q.EntityId)).ToDictionaryAsync(q => q.EntityId, q => q);
        }

        public static Task<T> GetById<TKey, T>(this IQueryable<T> InCollection, TKey InKey) where T : IEntityWithId<TKey>
        {
            return InCollection.FirstOrDefaultAsync(q => q.EntityId.Equals(InKey));
        }

        public static bool IsExist<TKey, T>(this IEnumerable<T> InCollection, TKey InKey) where T : IEntityWithId<TKey>
        {
            return InCollection.Any(q => q.EntityId.Equals(InKey));
        }
        
        public static T GetById<TKey, T>(this IEnumerable<T> InCollection, TKey InKey) where T : IEntityWithId<TKey>
        {
            return InCollection.FirstOrDefault(q => q.EntityId.Equals(InKey));
        }

        public static IReadOnlyCollection<T> GetByIds<TKey, T>(this IEnumerable<T> InCollection, IReadOnlyCollection<TKey> InKeys) where T : IEntityWithId<TKey>
        {
            return InCollection.Where(q => InKeys.Contains(q.EntityId)).ToArray();
        }

        public static IReadOnlyList<TKey> GetEntityIds<TKey, T>(this IEnumerable<T> InCollection) where T : IEntityWithId<TKey>
        {
            return InCollection.Select(q => q.EntityId).ToArray();
        }

        public static IReadOnlyList<TKey> GetUniqueEntityIds<TKey, T>(this IEnumerable<T> InCollection) where T : IEntityWithId<TKey>
        {
            return InCollection.Select(q => q.EntityId).Distinct().ToArray();
        }

        public static IReadOnlyList<TName> GetEntityNames<TName, T>(this IEnumerable<T> InCollection) 
            where T : IEntityWithName<TName>
            where TName : IName<TName>
        {
            return InCollection.Select(q => q.Name).ToArray();
        }
    }
}
