﻿using System.Threading.Tasks;

namespace SunCity.Domain.Interfaces.Entity
{
    public interface IEntityWithExperience
    {
        byte Level { get; }
        long Experience { get; }
    }
}