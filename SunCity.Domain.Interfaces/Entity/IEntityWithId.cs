﻿using System.Collections.Generic;
using System.Text;

namespace SunCity.Domain.Interfaces.Entity
{
    public interface IEntityWithId<out TEntityIdType>
    {
        TEntityIdType EntityId { get; }
    }
}
