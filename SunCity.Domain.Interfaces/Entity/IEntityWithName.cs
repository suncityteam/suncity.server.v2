﻿using SunCity.Domain.Interfaces.Types;

namespace SunCity.Domain.Interfaces.Entity
{
    public interface IEntityWithName<out TName> 
        where TName : IName<TName>
    {
        TName Name { get; }
    }
}