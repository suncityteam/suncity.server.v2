﻿using System;

namespace SunCity.Domain.Interfaces.Entity
{
    public interface IEntityWithLastActivityDate
    {
        DateTime LastActivityDate { get; set; }
    }
}