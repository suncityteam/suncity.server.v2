﻿using System;

namespace SunCity.Domain.Interfaces.Entity
{
    public interface IEntityWithDeleted
    {
        bool Deleted { get; set; }
        DateTime? DeletedDate { get; set; }
    }
}