﻿using System;

namespace SunCity.Domain.Interfaces.Entity
{
    public interface IEntityWithExpiredDate
    {
        DateTime ExpiredDate { get; }
    }
}