﻿using System;

namespace SunCity.Domain.Interfaces.Entity
{
    public interface IEntityWithCreatedDate
    {
        DateTime CreatedDate { get; }
    }
}