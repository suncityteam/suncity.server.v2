﻿using System;
using System.Threading.Tasks;

namespace SunCity.Domain.Interfaces.Types
{
    public interface IAmount<out TValue>
        where TValue : IConvertible
    {
        TValue Amount { get; }
    }
}