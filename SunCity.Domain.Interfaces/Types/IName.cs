﻿using System;
using System.Threading.Tasks;

namespace SunCity.Domain.Interfaces.Types
{
    public interface IName<TName>
        : IEquatable<TName>
        , IComparable<TName>
        where TName : IName<TName>
    {
        string Name { get; }
    }
}