﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SunCity.Domain.Interfaces.Types
{
    public interface IGuidId<TKey>
        : IEquatable<TKey>
        , IComparable<TKey>
        , IFormattable
        where TKey : IGuidId<TKey>
    {
        Guid Id { get; }
        bool IsValid{ get; }
        bool IsNotValid { get; }
    }
}
