﻿using System;
using System.Diagnostics;
using System.Linq.Expressions;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;
using SunCity.Domain.Interfaces.Types;

namespace SunCity.Identity.DataBase.Keys
{
    public readonly struct SessionTokenId : IGuidId<SessionTokenId>
    {
        public Guid Id { get; }
        public bool IsValid => Id != Guid.Empty;
        public bool IsNotValid => !IsValid;
        
        public SessionTokenId(Guid InEntityId)
        {
            Id = InEntityId;
        }

        public static bool operator ==(SessionTokenId InLhs, SessionTokenId InRhs) => InLhs.Equals(InRhs);
        public static bool operator !=(SessionTokenId InLhs, SessionTokenId InRhs) => !InLhs.Equals(InRhs);
        public bool Equals(SessionTokenId InOther) => Id.Equals(InOther.Id);
        public override bool Equals(object? obj) => obj is SessionTokenId other && Equals(other);
        public int CompareTo(SessionTokenId InOther) => Id.CompareTo(InOther);
        public string ToString(string? InFormat, IFormatProvider? InFormatProvider) => Id.ToString(InFormat, InFormatProvider);
        public override string ToString() => Id.ToString();
        public override int GetHashCode() => Id.GetHashCode();

    }
}