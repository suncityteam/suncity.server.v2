﻿using System;
using System.Diagnostics;
using System.Linq.Expressions;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;
using SunCity.Domain.Interfaces.Types;

namespace SunCity.Identity.DataBase.Keys
{
    [DebuggerDisplay("{EntityId}")]
    public readonly struct SessionHistoryId : IGuidId<SessionHistoryId>
    {
        public Guid Id { get; }
        public bool IsValid => Id != Guid.Empty;
        public bool IsNotValid => !IsValid;
        
        public SessionHistoryId(Guid InEntityId)
        {
            Id = InEntityId;
        }

        public static bool operator ==(SessionHistoryId InLhs, SessionHistoryId InRhs) => InLhs.Equals(InRhs);
        public static bool operator !=(SessionHistoryId InLhs, SessionHistoryId InRhs) => !InLhs.Equals(InRhs);
        public bool Equals(SessionHistoryId InOther) => Id.Equals(InOther.Id);
        public override bool Equals(object? InObj) => InObj is SessionHistoryId other && Equals(other);
        public int CompareTo(SessionHistoryId InOther) => Id.CompareTo(InOther);
        public string ToString(string? InFormat, IFormatProvider? InFormatProvider) => Id.ToString(InFormat, InFormatProvider);
        public override string ToString() => Id.ToString();
        public override int GetHashCode() => Id.GetHashCode();
    }
}