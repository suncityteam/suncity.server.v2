﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using SunCity.Domain.Interfaces.Types;

namespace SunCity.Identity.DataBase.Keys
{
    public readonly struct AccountId 
        : IGuidId<AccountId>
    {
        public Guid Id { get; }
        public bool IsValid => Id != Guid.Empty;
        public bool IsNotValid => !IsValid;

        public AccountId(Guid InEntityId)
        {
            Id = InEntityId;
        }

        public static bool operator ==(AccountId InLhs, AccountId InRhs) => InLhs.Equals(InRhs);
        public static bool operator !=(AccountId InLhs, AccountId InRhs) => !InLhs.Equals(InRhs);

        #region Interfaces
        public override bool Equals(object? InObj) => InObj is AccountId other && Equals(other);
        public bool Equals(AccountId InOther) => Id.Equals(InOther.Id);
        public int CompareTo(AccountId InOther) => Id.CompareTo(InOther);
        public string ToString(string? InFormat, IFormatProvider? InFormatProvider) => Id.ToString(InFormat, InFormatProvider);
        public override int GetHashCode() => Id.GetHashCode();
        public override string ToString() => Id.ToString();

        #endregion
    }
}
