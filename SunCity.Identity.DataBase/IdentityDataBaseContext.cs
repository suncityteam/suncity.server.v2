﻿using System;
using System.IO;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;
using Microsoft.Extensions.Configuration;
using SunCity.EntityFrameWork;
using SunCity.Identity.DataBase.Entities;
using SunCity.Identity.DataBase.Keys;

namespace SunCity.Identity.DataBase
{
    public sealed class IdentityDataBaseContext : DbContext
    {
        public const string SchemaName = "Identity";
        public IdentityDataBaseContext(DbContextOptions<IdentityDataBaseContext> InOptions) : base(InOptions)
        {

        }

        protected override void OnConfiguring(DbContextOptionsBuilder InOptionsBuilder)
        {
            InOptionsBuilder.EnableSensitiveDataLogging();
            InOptionsBuilder.EnableDetailedErrors();
        }

        protected override void OnModelCreating(ModelBuilder InModelBuilder)
        {
            base.OnModelCreating(InModelBuilder);
            InModelBuilder.HasDefaultSchema(SchemaName);
        }


        public DbSet<AccountEntity> AccountEntity { get; set; }

        public DbSet<SessionHistoryEntity> SessionHistoryEntity { get; set; }
        public DbSet<SessionTokenEntity> SessionTokenEntity { get; set; }
    }


}
