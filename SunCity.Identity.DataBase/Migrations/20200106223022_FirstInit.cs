﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace SunCity.Identity.DataBase.Migrations
{
    public partial class FirstInit : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.EnsureSchema(
                name: "Identity");

            migrationBuilder.CreateTable(
                name: "AccountEntity",
                schema: "Identity",
                columns: table => new
                {
                    EntityId = table.Column<Guid>(nullable: false),
                    Login = table.Column<string>(maxLength: 32, nullable: false),
                    Email = table.Column<string>(maxLength: 64, nullable: false),
                    PhoneNumber = table.Column<string>(maxLength: 32, nullable: true),
                    Password = table.Column<string>(maxLength: 512, nullable: false),
                    CreatedAt = table.Column<DateTime>(nullable: false),
                    EmailConfirmed = table.Column<bool>(nullable: false),
                    PhoneNumberConfirmed = table.Column<bool>(nullable: false),
                    Deleted = table.Column<bool>(nullable: false),
                    Blocked = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AccountEntity", x => x.EntityId);
                });

            migrationBuilder.CreateTable(
                name: "SessionHistoryEntity",
                schema: "Identity",
                columns: table => new
                {
                    EntityId = table.Column<Guid>(nullable: false),
                    AccountId = table.Column<Guid>(nullable: false),
                    IpAddress = table.Column<string>(maxLength: 64, nullable: false),
                    CreatedAt = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SessionHistoryEntity", x => x.EntityId);
                    table.ForeignKey(
                        name: "FK_SessionHistoryEntity_AccountEntity_AccountId",
                        column: x => x.AccountId,
                        principalSchema: "Identity",
                        principalTable: "AccountEntity",
                        principalColumn: "EntityId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "SessionTokenEntity",
                schema: "Identity",
                columns: table => new
                {
                    EntityId = table.Column<Guid>(nullable: false),
                    AccountId = table.Column<Guid>(nullable: false),
                    Token = table.Column<string>(maxLength: 2048, nullable: false),
                    CreatedAt = table.Column<DateTime>(nullable: false),
                    ExpiresAt = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SessionTokenEntity", x => x.EntityId);
                    table.ForeignKey(
                        name: "FK_SessionTokenEntity_AccountEntity_AccountId",
                        column: x => x.AccountId,
                        principalSchema: "Identity",
                        principalTable: "AccountEntity",
                        principalColumn: "EntityId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_SessionHistoryEntity_AccountId",
                schema: "Identity",
                table: "SessionHistoryEntity",
                column: "AccountId");

            migrationBuilder.CreateIndex(
                name: "IX_SessionTokenEntity_AccountId",
                schema: "Identity",
                table: "SessionTokenEntity",
                column: "AccountId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "SessionHistoryEntity",
                schema: "Identity");

            migrationBuilder.DropTable(
                name: "SessionTokenEntity",
                schema: "Identity");

            migrationBuilder.DropTable(
                name: "AccountEntity",
                schema: "Identity");
        }
    }
}
