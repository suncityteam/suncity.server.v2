﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Diagnostics;
using System.Threading.Tasks;
using AutoMapper;
using SunCity.Identity.DataBase.Keys;

namespace SunCity.Identity.DataBase.Entities
{
    [DebuggerDisplay("[{EntityId}][AccountId: {AccountId}][CreatedAt: {CreatedAt}][ExpiresAt: {ExpiresAt}]")]
    public class SessionTokenEntity
    {
        [Key]
        public virtual SessionTokenId EntityId { get; set; }

        [ForeignKey(nameof(AccountEntity))]
        public virtual AccountId AccountId { get; set; }
        public virtual AccountEntity AccountEntity { get; set; }

        [Required]
        [MaxLength(2048)]
        public virtual string Token { get; set; }
        public virtual DateTime CreatedAt { get; set; }
        public virtual DateTime ExpiresAt { get; set; }

    }
}