﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Diagnostics;
using System.Threading.Tasks;
using AutoMapper;
using SunCity.Identity.DataBase.Keys;

namespace SunCity.Identity.DataBase.Entities
{
    [DebuggerDisplay("[{EntityId}][AccountId: {AccountId}][{IpAddress}][{CreatedAt}]")]
    public class SessionHistoryEntity
    {
        [Key]
        public Guid EntityId { get; set; }

        [ForeignKey(nameof(AccountEntity))]
        public AccountId AccountId { get; set; }
        public virtual AccountEntity AccountEntity { get; set; }

        [Required]
        [MaxLength(64)]
        public virtual string IpAddress { get; set; }
        public virtual DateTime CreatedAt { get; set; }
    }
}