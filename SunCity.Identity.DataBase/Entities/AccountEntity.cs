﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Diagnostics;
using System.Text;
using SunCity.Game.Types.Player.Entity.Player;
using SunCity.Identity.DataBase.Keys;

namespace SunCity.Identity.DataBase.Entities
{
    [DebuggerDisplay("[{EntityId}][{Login} / Deleted: {Deleted} / Blocked: {Blocked}][{Email} / {EmailConfirmed}][{PhoneNumber} / {PhoneNumberConfirmed}][CreatedAt: {CreatedAt}]")]
    public class AccountEntity
    {
        [Key]
        public virtual AccountId EntityId { get; set; }

        [Required]
        [MaxLength(32)]
        public virtual PlayerAccountLogin Login { get; set; }

        [Required]
        [MaxLength(64)]
        public virtual string Email { get; set; }

        [MaxLength(32)]
        public virtual string PhoneNumber { get; set; }

        [Required]
        [MaxLength(512)]
        public virtual string Password { get; set; }
        public virtual DateTime CreatedAt { get; set; }

        public virtual bool EmailConfirmed { get; set; }
        public virtual bool PhoneNumberConfirmed { get; set; }

        public virtual bool Deleted { get; set; }
        public virtual bool Blocked { get; set; }

        //[InverseProperty(nameof(SessionHistoryEntity.AccountId))]
        public virtual List<SessionHistoryEntity> SessionHistories { get; set; }

        //[InverseProperty(nameof(SessionHistoryEntity.AccountId))]
        public virtual List<SessionTokenEntity> SessionTokens { get; set; }
    }
}
