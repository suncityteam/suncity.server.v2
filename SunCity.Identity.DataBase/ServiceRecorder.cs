﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using SunCity.EntityFrameWork.Converters;

namespace SunCity.Identity.DataBase
{
    public static class ServiceRecorder
    {
        public static IServiceCollection AddIdentityDataBase(this IServiceCollection InServiceCollection, IConfiguration InConfiguration)
        {
            var connectionString = InConfiguration.GetSection("Application.DataBase").GetSection(nameof(IdentityDataBaseContext)).Get<string>();
            Console.WriteLine($"AddIdentityDataBase: {connectionString}");

            InServiceCollection.AddDbContext<IdentityDataBaseContext>(InBuilder =>
            {
                InBuilder.UseNpgsql(connectionString, InOptions =>
                {
                    InOptions.EnableRetryOnFailure();
                    InOptions.MigrationsHistoryTable("__EFMigrationsHistory", IdentityDataBaseContext.SchemaName);
                });
                InBuilder.UseStronglyTypedIdValueConverter();
            });

            return InServiceCollection;
        }
    }
}
