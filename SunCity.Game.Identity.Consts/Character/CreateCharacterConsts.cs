﻿using System;
using System.Collections.Generic;

using System.Text;
using GTANetworkAPI;
using SunCity.Common.RageMp.Pools;

namespace SunCity.Game.Identity.Consts.Character
{
    public static class CreateCharacterConsts
    {
        public static Vector3 SpawnPosition { get; } = new Vector3(200.6641f, -932.0939f, 30.6868f);
        public static uint SpawnVirtualWorld { get; } = RageMpConsts.GlobalDimension;
    }
}
