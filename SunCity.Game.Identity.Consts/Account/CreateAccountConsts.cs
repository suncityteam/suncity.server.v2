﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SunCity.Game.Identity.Consts.Account
{
    public static class CreateAccountConsts
    {
        public static long StartBalance { get; } = 100;
        public static TimeSpan PremiunAccount { get; } = TimeSpan.FromHours(3);
    }
}
