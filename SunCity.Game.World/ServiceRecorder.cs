﻿using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using SunCity.Game.World.Chat;
using SunCity.Game.World.Fraction;
using SunCity.Game.World.Interior;
using SunCity.Game.World.Item.Factory;
using SunCity.Game.World.ItemModels;
using SunCity.Game.World.Licenses;
using SunCity.Game.World.Models.Zone;
using SunCity.Game.World.Player;
using SunCity.Game.World.Player.Process;
using SunCity.Game.World.Process.Time;
using SunCity.Game.World.Process.Weather;
using SunCity.Game.World.Property;
using SunCity.Game.World.Property.Category;
using SunCity.Game.World.Property.Category.Business.StreetRacing;
using SunCity.Game.World.Property.Loaders;
using SunCity.Game.World.Property.Process;
using SunCity.Game.World.Settings;
using SunCity.Game.World.StationPoint;
using SunCity.Game.World.Vehicle;
using SunCity.Game.World.Vehicle.Process;
using SunCity.Game.World.VehicleModels;
using SunCity.Game.World.Zone;

namespace SunCity.Game.World
{
    public static class ServiceRecorder
    {
        public static IServiceCollection AddGameWorld(this IServiceCollection InServiceCollection, IConfiguration InConfiguration)
        {
            InServiceCollection.Configure<GameWorldSettings>(InConfiguration.GetSection(nameof(GameWorldSettings)));

            //var zones = InConfiguration.GetSection(nameof(GameWorldZones)).Get<GameWorldZones>()/*.Initialize()*/;
            InServiceCollection.AddSingleton(new GameWorldZones());

            InServiceCollection.AddSingleton<GameWorldTimeProcess>();
            InServiceCollection.AddSingleton<GameWorldWeatherProcess>();
            

            InServiceCollection.AddSingleton<IGameItemStorage, GameItemStorage>();
            InServiceCollection.AddSingleton<GameItemSeed>();

            
            InServiceCollection.AddSingleton<IGameWorldZone, GameWorldZone>();

            InServiceCollection.AddSingleton<IGameWorldChat, GameWorldChat>();
            InServiceCollection.AddSingleton<IGameWorld, GameWorld>();

            InServiceCollection.AddSingleton<IWorldFractionStorage, WorldFractionStorage>();
            InServiceCollection.AddSingleton<WorldFractionFactory>();
            InServiceCollection.AddSingleton<WorldFractionSeed>();


            InServiceCollection.AddSingleton<IVehicleModelStorage, VehicleModelStorage>();

            InServiceCollection.AddSingleton<IWorldVehicleStorage, WorldVehicleStorage>();
            InServiceCollection.AddSingleton<SaveWorldVehicleStorageProcess>();
            InServiceCollection.AddSingleton<TickWorldVehicleStorageProcess>();


            InServiceCollection.AddSingleton<IWorldPlayerStorage, WorldPlayerStorage>();
            InServiceCollection.AddSingleton<SaveWorldPlayerStorageProcess>();

            InServiceCollection.AddSingleton<IWorldInteriorStorage, WorldInteriorStorage>();

            InServiceCollection.AddSingleton<IWorldPropertyStorage, WorldPropertyStorage>();
            InServiceCollection.AddSingleton<WorldPropertySaveProcess>();

            InServiceCollection.AddSingleton<WorldPropertyFactory>();

            InServiceCollection.AddSingleton<VehicleModelSeed>();
            InServiceCollection.AddGameWorldPropertyLoaders(InConfiguration);
            InServiceCollection.AddGameWorldPropertySeed(InConfiguration);

            InServiceCollection.AddSingleton<IWorldItemFactory, WorldItemFactory>();

            InServiceCollection.AddSingleton<IGameLicenseStorage, GameLicenseStorage>();
            InServiceCollection.AddSingleton<GameLicenseSeed>();
            InServiceCollection.AddSingleton<StreetRacingManager>();

            
            
            InServiceCollection.AddSingleton<IWorldStationPointStorage, WorldStationPointStorage>();
            
            return InServiceCollection;
        }

        public static void UseGameWorld(this IApplicationBuilder InApplicationBuilder)
        {
            InApplicationBuilder.ApplicationServices.GetService<WorldPropertyFactory>().Initialize();
            InApplicationBuilder.ApplicationServices.GetService<WorldPropertySaveProcess>();

            InApplicationBuilder.ApplicationServices.GetService<GameWorldTimeProcess>();
            InApplicationBuilder.ApplicationServices.GetService<GameWorldWeatherProcess>();

            InApplicationBuilder.ApplicationServices.GetService<SaveWorldPlayerStorageProcess>();
            //InApplicationBuilder.ApplicationServices.GetService<SaveWorldVehicleStorageProcess>();
            InApplicationBuilder.ApplicationServices.GetService<TickWorldVehicleStorageProcess>();
            
            InApplicationBuilder.ApplicationServices.GetService<StreetRacingManager>();
        }

    }
}
