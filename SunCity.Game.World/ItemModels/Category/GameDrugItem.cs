﻿using System;
using SunCity.Game.World.DataBase.Entitites.Item;

namespace SunCity.Game.World.ItemModels.Category
{
    public class GameDrugItem : GameItem
    {
        //========================================
        /// <summary>
        /// Алкоголь, опьянение
        /// </summary>
        public int Alcohol { get; }

        /// <summary>
        /// Сытость
        /// </summary>
        public int Satiety { get; }

        /// <summary>
        /// Зависимость
        /// </summary>
        public int Addiction { get; }

        //========================================
        public int HealthRegenerationAmount { get; }
        public TimeSpan HealthRegenerationTime { get; }
        public TimeSpan HealthRegenerationInterval { get;  }

        public GameDrugItem(GameItemEntity InEntity) : base(InEntity)
        {
            //---------------------------------------
            Alcohol   = InEntity.DrugEntity.Alcohol;
            Addiction = InEntity.DrugEntity.Addiction;
            Satiety   = InEntity.DrugEntity.Satiety;

            //---------------------------------------
            HealthRegenerationAmount   = InEntity.DrugEntity.HealthRegenerationAmount;
            HealthRegenerationTime     = InEntity.DrugEntity.HealthRegenerationTime;
            HealthRegenerationInterval = InEntity.DrugEntity.HealthRegenerationInterval;

        }
    }
}
