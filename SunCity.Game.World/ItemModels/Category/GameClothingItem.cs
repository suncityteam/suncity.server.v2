﻿using GTANetworkAPI;
using SunCity.Game.World.DataBase.Entitites.Item;

namespace SunCity.Game.World.ItemModels.Category
{
    public class GameClothingItem : GameItem
    {
        //public PlayerInventorySlot Slot { get; }
        public int Temperature { get; }
        public ComponentVariation ClothData { get; }
        public int ClothSlot { get; }

        public GameClothingItem(GameItemEntity InEntity) : base(InEntity)
        {
            Temperature = InEntity.ClothingEntity.Temperature;

            ClothSlot = InEntity.ClothingEntity.ClothSlot;

            ClothData = new ComponentVariation(InEntity.ClothingEntity.Drawable, InEntity.ClothingEntity.Texture, InEntity.ClothingEntity.Palette);

            Slot = InEntity.ClothingEntity.Slot;
        }
    }
}
