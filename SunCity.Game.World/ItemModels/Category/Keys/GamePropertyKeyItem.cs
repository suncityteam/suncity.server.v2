﻿using SunCity.Game.World.DataBase.Entitites.Item;
using SunCity.Game.World.DataBase.Entitites.Item.Category.Keys;

namespace SunCity.Game.World.ItemModels.Category.Keys
{
    public class GamePropertyKeyItem : GameItem
    {
        public PropertyKeyCategory Category { get; }
        public GamePropertyKeyItem(GameItemEntity InEntity) : base(InEntity)
        {
            Category = InEntity.PropertyKeyItemEntity.Category;
        }
    }

    public class GameVehicleKeyItem : GameItem
    {
        public VehicleKeyCategory Category { get; }
        public GameVehicleKeyItem(GameItemEntity InEntity) : base(InEntity)
        {         
            Category = InEntity.VehicleKeyItemEntity.Category;
        }
    }
}