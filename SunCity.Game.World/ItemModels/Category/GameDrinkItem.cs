﻿using SunCity.Game.World.DataBase.Entitites.Item;

namespace SunCity.Game.World.ItemModels.Category
{
    public class GameDrinkItem : GameItem
    {
        /// <summary>
        /// Алкоголь, опьянение
        /// </summary>
        public int Alcohol { get; }

        /// <summary>
        /// Зависимость
        /// </summary>
        public int Addiction { get; }

        /// <summary>
        /// Жажда
        /// </summary>
        public int Thirst { get; }

        public GameDrinkItem(GameItemEntity InEntity) : base(InEntity)
        {
            Alcohol   = InEntity.DrinkEntity.Alcohol;
            Addiction = InEntity.DrinkEntity.Addiction;
            Thirst    = InEntity.DrinkEntity.Thirst;
        }
    }
}