﻿using System;
using SunCity.Game.World.DataBase.Entitites.Item;

namespace SunCity.Game.World.ItemModels.Category
{
    public class GameFoodItem : GameItem
    { 
        //========================================
        /// <summary>
        /// Сытость
        /// </summary>
        public int Satiety { get; set; }

        /// <summary>
        /// Срок годности
        /// </summary>
        public TimeSpan ExpirationDate { get; set; }

        public GameFoodItem(GameItemEntity InEntity) : base(InEntity)
        { 
            //---------------------------------------
            ExpirationDate = InEntity.FoodEntity.ExpirationDate;
            Satiety        = InEntity.FoodEntity.Satiety;
        }
    }
}
