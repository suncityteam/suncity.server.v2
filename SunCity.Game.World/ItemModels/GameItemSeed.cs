﻿using System;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using SunCity.Common.Enum;
using SunCity.Game.Enums.Item;
using SunCity.Game.Enums.World.Player.Inventory;
using SunCity.Game.World.DataBase;
using SunCity.Game.World.DataBase.Entitites.Item;
using SunCity.Game.World.DataBase.Entitites.Item.Category;
using SunCity.Game.World.DataBase.Entitites.Item.Category.Keys;

namespace SunCity.Game.World.ItemModels
{
    internal class GameItemSeed
    {
        protected GameItemEntity Factory(ItemCategory InCategory, string InName)
        {
            return new GameItemEntity
            {
                EntityId = GameItemId.GenerateId(),

                Name = InName,
                ItemCategory = InCategory,
                Stack = 8
            };
        }

        protected GameItemEntity FactoryFood(string InName, int InSatiety)
        {
            var item = Factory(ItemCategory.Food, InName);
            item.FoodEntity = new FoodItemEntity {ExpirationDate = TimeSpan.FromDays(1), Satiety = InSatiety};
            return item;
        }

        protected GameItemEntity FactoryDrink(string InName, int InThirst = 10, int InAlcohol = 0, int InAddiction = 0)
        {
            var item = Factory(ItemCategory.Drink, InName);
            item.DrinkEntity = new DrinkItemEntity {Alcohol = InAlcohol, Addiction = InAddiction, Thirst = InThirst};
            return item;
        }

        protected GameItemEntity FactoryDrug(string InName, int InAlcohol = 0, int InAddiction = 0, int InHealth = 2)
        {
            var item = Factory(ItemCategory.Drug, InName);
            item.DrugEntity = new DrugItemEntity
            {
                Alcohol = InAlcohol, Addiction = InAddiction, HealthRegenerationAmount = InHealth, HealthRegenerationTime = TimeSpan.FromSeconds(15),
                HealthRegenerationInterval = TimeSpan.FromSeconds(3)
            };
            return item;
        }

        protected GameItemEntity FactoryClothing(string InName, PlayerInventorySlot InSlot, int InCapacity)
        {
            var item = Factory(ItemCategory.Clothing, InName);
            item.Capacity = InCapacity;
            item.ClothingEntity = new ClothingItemEntity {Slot = InSlot};
            return item;
        }

        protected GameItemEntity FactoryPropertyKey(PropertyKeyCategory category)
        {
            var item = Factory(ItemCategory.PropertyKey, category.GetDescription());
            item.PropertyKeyItemEntity = new PropertyKeyItemEntity
            {
                Category = category
            };
            return item;
        }

        protected GameItemEntity FactoryVehicleKey(VehicleKeyCategory category)
        {
            var item = Factory(ItemCategory.VehicleKey, category.GetDescription());
            item.VehicleKeyItemEntity = new VehicleKeyItemEntity()
            {
                Category = category
            };
            return item;
        }

        protected async Task AddIfNotExist(GameWorldDbContext InDbContext, GameItemEntity InEntity)
        {
            if (await InDbContext.ItemEntity.AnyAsync(InP => InP.ItemCategory == InEntity.ItemCategory && InP.Name == InEntity.Name) == false)
            {
                InDbContext.ItemEntity.Add(InEntity);
            }
        }

        public async Task Initialize(GameWorldDbContext InDbContext)
        {
            await AddIfNotExist(InDbContext, FactoryClothing("Майка", PlayerInventorySlot.Outerwear, 1));
            await AddIfNotExist(InDbContext, FactoryClothing("Педжак", PlayerInventorySlot.Outerwear, 5));

            await AddIfNotExist(InDbContext, FactoryClothing("Штаны", PlayerInventorySlot.Pants, 2));
            await AddIfNotExist(InDbContext, FactoryClothing("Джинсы", PlayerInventorySlot.Pants, 4));

            await AddIfNotExist(InDbContext, FactoryClothing("Сумка", PlayerInventorySlot.Bag, 10));
            await AddIfNotExist(InDbContext, FactoryClothing("Рюкзак", PlayerInventorySlot.Bag, 15));


            await AddIfNotExist(InDbContext, FactoryFood("Хот-дог", 5));
            await AddIfNotExist(InDbContext, FactoryFood("Гамбургер", 10));
            await AddIfNotExist(InDbContext, FactoryFood("Чипсы", 2));
            await AddIfNotExist(InDbContext, FactoryFood("Мороженное", 2));

            await AddIfNotExist(InDbContext, FactoryDrink("Coca-cola", 10));
            await AddIfNotExist(InDbContext, FactoryDrink("Sprite", 10));
            await AddIfNotExist(InDbContext, FactoryDrink("Fanta", 10));

            await AddIfNotExist(InDbContext, FactoryDrink("Водка", 1, 10, 5));
            await AddIfNotExist(InDbContext, FactoryDrink("Пиво", 3, 2, 5));

            await AddIfNotExist(InDbContext, FactoryDrug("Мет в ромбик", 50, 50, 5));
            await AddIfNotExist(InDbContext, FactoryDrug("Голубой мет", 75, 10, 5));
            await AddIfNotExist(InDbContext, FactoryDrug("Марихуана", 5, 2, 1));

            await AddIfNotExist(InDbContext, FactoryDrug("Марихуана", 5, 2, 1));

            await AddIfNotExist(InDbContext, FactoryPropertyKey(PropertyKeyCategory.House));
            await AddIfNotExist(InDbContext, FactoryPropertyKey(PropertyKeyCategory.Apartment));
            await AddIfNotExist(InDbContext, FactoryPropertyKey(PropertyKeyCategory.Garage));
            await AddIfNotExist(InDbContext, FactoryPropertyKey(PropertyKeyCategory.HotelRoom));       
            await AddIfNotExist(InDbContext, FactoryPropertyKey(PropertyKeyCategory.Warehouse));
            await AddIfNotExist(InDbContext, FactoryPropertyKey(PropertyKeyCategory.Business));

            await AddIfNotExist(InDbContext, FactoryVehicleKey(VehicleKeyCategory.Airplane));
            await AddIfNotExist(InDbContext, FactoryVehicleKey(VehicleKeyCategory.Helicopter));
            await AddIfNotExist(InDbContext, FactoryVehicleKey(VehicleKeyCategory.Car));
            await AddIfNotExist(InDbContext, FactoryVehicleKey(VehicleKeyCategory.Motorcycle));
            await AddIfNotExist(InDbContext, FactoryVehicleKey(VehicleKeyCategory.Bus));
            await AddIfNotExist(InDbContext, FactoryVehicleKey(VehicleKeyCategory.QuadBike));
            await AddIfNotExist(InDbContext, FactoryVehicleKey(VehicleKeyCategory.Airship));
            await AddIfNotExist(InDbContext, FactoryVehicleKey(VehicleKeyCategory.Tractor));
            await AddIfNotExist(InDbContext, FactoryVehicleKey(VehicleKeyCategory.Train));

            await InDbContext.SaveChangesAsync();
        }
    }
}