﻿using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using SunCity.Common.EfCore;
using SunCity.Game.Enums.Item;
using SunCity.Game.Types.Storage;
using SunCity.Game.World.DataBase;
using SunCity.Game.World.DataBase.Entitites.Item.Category.Keys;
using SunCity.Game.World.ItemModels.Category;
using SunCity.Game.World.ItemModels.Category.Keys;

namespace SunCity.Game.World.ItemModels
{
    internal class GameItemStorage : WorldStorage<GameItemId, GameItem>, IGameItemStorage
    {
        public GamePropertyKeyItem PropertyKey { get; private set; }
        public GameVehicleKeyItem VehicleKey { get; private set; }

        private ILogger<GameItemStorage> Logger { get; }
        private GameItemSeed Seed { get; }

        public GameItemStorage(ILogger<GameItemStorage> InLogger, GameItemSeed InSeed)
        {
            Logger = InLogger;
            Seed = InSeed;
        }

        public async Task Initialize(GameWorldDbContext InDbContext)
        {
            await Seed.Initialize(InDbContext);

            Logger.LogInformation("Begin Initialize");

            var models = await InDbContext.ItemEntity
                .HasTracking(hasTracking: false)
                .OrderBy(q => q.EntityId)
                .Include(q => q.DrinkEntity)
                .Include(q => q.FoodEntity)
                .Include(q => q.DrugEntity)
                .Include(q => q.ClothingEntity)
                .Include(q => q.ResourceEntity)
                .Include(q => q.PropertyKeyItemEntity)
                .Include(q => q.VehicleKeyItemEntity)
                .ToArrayAsync();

            Logger.LogInformation($"Fetched {models.Length} models");

            foreach (var model in models)
            {
                if (model.ItemCategory == ItemCategory.Clothing)
                {
                    Put(new GameClothingItem(model));
                }
                else if (model.ItemCategory == ItemCategory.Drink)
                {
                    Put(new GameDrinkItem(model));
                }
                else if (model.ItemCategory == ItemCategory.Drug)
                {
                    Put(new GameDrugItem(model));
                }
                else if (model.ItemCategory == ItemCategory.Food)
                {
                    Put(new GameFoodItem(model));
                }
                else if (model.ItemCategory == ItemCategory.Resource)
                {
                    Put(new GameResourceItem(model));
                }
                else if (model.ItemCategory == ItemCategory.PropertyKey)
                {
                    Put(new GamePropertyKeyItem(model));
                }
                else if (model.ItemCategory == ItemCategory.VehicleKey)
                {
                    Put(new GameVehicleKeyItem(model));
                }
                else
                {
                    Logger.LogError($"Failed create game item {model.EntityId} / {model.ItemCategory} / {model.Name} | Not found category factory");
                }
            }

            PropertyKey = Values.Where(q => q.ItemCategory == ItemCategory.PropertyKey).Cast<GamePropertyKeyItem>().First(q => q.Category == PropertyKeyCategory.Apartment);
            VehicleKey = Values.Where(q => q.ItemCategory == ItemCategory.VehicleKey).Cast<GameVehicleKeyItem>().First(q => q.Category == VehicleKeyCategory.Car);

            Logger.LogInformation($"Loaded {Items.Count} models");
            Logger.LogInformation("End Initialize");
        }
    }
}