﻿using SunCity.Domain.Interfaces.Entity;
using SunCity.Game.Enums.Item;
using SunCity.Game.Enums.World.Player.Inventory;
using SunCity.Game.World.DataBase.Entitites.Item;

namespace SunCity.Game.World.ItemModels
{
    public abstract class GameItem : IEntityWithId<GameItemId>
    {
        public GameItemId EntityId { get; } 
        public string Name { get; }
        public string ImageClass { get; }
        public string Description { get; }
        public PlayerInventorySlot Slot { get; protected set; }

        public long? ModelHash { get; }

        //====================================
        public ItemCategory ItemCategory { get; }

        //====================================
        /// <summary>
        /// Вес
        /// </summary>
        public int Weight { get; }

        /// <summary>
        /// Вместимость
        /// </summary>
        public int Capacity { get; }

        //====================================
        public short? Stack { get; }

        protected GameItem(GameItemEntity InEntity)
        {
            //-------------------------------
            Name = InEntity.Name;
            EntityId = InEntity.EntityId;
            ModelHash = InEntity.ModelHash;
            ImageClass = InEntity.ImageClass;
            Description = InEntity.Description;

            //-------------------------------
            ItemCategory = InEntity.ItemCategory;

            //-------------------------------
            Weight = InEntity.Weight;
            Capacity = InEntity.Capacity;
            Stack = InEntity.Stack;
        }

        public override string ToString()
        {
            return $"[ItemModel][{EntityId} / {ItemCategory}][{Name}][ImageClass: {ImageClass}]";
        }

    }
}
