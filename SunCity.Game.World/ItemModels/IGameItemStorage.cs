﻿using System.Threading.Tasks;
using SunCity.Game.Types.Storage;
using SunCity.Game.World.DataBase;
using SunCity.Game.World.Item.Factory;
using SunCity.Game.World.ItemModels.Category.Keys;

namespace SunCity.Game.World.ItemModels
{
    public class GameItemFactoryContext
    {
        public IWorldItemFactory WorldItemFactory { get; }
        public IGameItemStorage GameItemStorage { get; }

        public GameItemFactoryContext(IWorldItemFactory InWorldItemFactory, IGameItemStorage InGameItemStorage)
        {
            WorldItemFactory = InWorldItemFactory;
            GameItemStorage = InGameItemStorage;
        }
    }

    public interface IGameItemStorage : IWorldStorage<GameItemId, GameItem>
    {
        Task Initialize(GameWorldDbContext InDbContext);
        GamePropertyKeyItem PropertyKey { get; }
        GameVehicleKeyItem VehicleKey { get; }
    }
}