﻿
using GTANetworkAPI;
using JetBrains.Annotations;
using SunCity.Game.World.Models.Zone;

namespace SunCity.Game.World.Zone
{
    public interface IGameWorldZone
    {
        [CanBeNull]
        GameWorldZoneName GetZoneName(Vector3 InPosition);

        //[CanBeNull]
        //GameWorldZoneName GetZoneName(Vector2 InPosition);
        //
        //[CanBeNull]
        //GameWorldZoneType GetZoneType(Vector2 InPosition);
    }
}