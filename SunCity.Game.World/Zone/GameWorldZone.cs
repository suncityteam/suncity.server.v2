﻿using System.Linq;
using GTANetworkAPI;
using SunCity.Game.World.Models.Zone;

namespace SunCity.Game.World.Zone
{
    internal class GameWorldZone : IGameWorldZone
    {
        private GameWorldZones Zones { get; }

        public GameWorldZone(GameWorldZones InZones)
        {
            Zones = InZones;
        }

        public GameWorldZoneName GetZoneName(Vector3 InPosition)
        {
            var zone = Zones.ZoneNames.AsParallel().FirstOrDefault(q => q.Bounds.InBounds(InPosition));
            return zone;
        }

        //public GameWorldZoneName GetZoneName(Vector2 InPosition)
        //{
        //    var zone = Zones.ZoneNames.AsParallel().FirstOrDefault(q => q.Bounds.InBounds(InPosition));
        //    return zone;
        //}
        //
        //public GameWorldZoneType GetZoneType(Vector2 InPosition)
        //{
        //    var zone = Zones.ZoneTypes.AsParallel().FirstOrDefault(q => q.Bounds.InBounds(InPosition));
        //    return zone;
        //}

    }
}
