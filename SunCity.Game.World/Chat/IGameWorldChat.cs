﻿using System.Collections.Generic;
using System.Threading.Tasks;
using SunCity.Game.World.Enums.Chat;
using SunCity.Game.World.Models.Chat;
using SunCity.Game.World.Player;

namespace SunCity.Game.World.Chat
{
    public interface IGameWorldChat
    {
        Task SendMessageToNearbyPlayers(WorldPlayer player, WorldChatMessage InChatMessage);
    }
}