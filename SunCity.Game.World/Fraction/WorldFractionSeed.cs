﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using SunCity.Game.Enums.Fraction;
using SunCity.Game.World.DataBase;
using SunCity.Game.World.DataBase.Entitites.Fraction;

namespace SunCity.Game.World.Fraction
{
    internal class WorldFractionSeed
    {
        private ILogger<WorldFractionSeed> Logger { get; }

        public WorldFractionSeed(ILogger<WorldFractionSeed> InLogger)
        {
            Logger = InLogger;
        }


        public async Task Initialize(GameWorldDbContext InDbContext)
        {
            await AddIfNotExist(InDbContext, FractionType.Police, FractionCategory.Government, "LSPD", 487);

            await AddIfNotExist(InDbContext, FractionType.Gang, FractionCategory.Crime, "Families", 437);
            await AddIfNotExist(InDbContext, FractionType.Gang, FractionCategory.Crime, "Ballas", 437);
            //await AddIfNotExist(InDbContext, FractionType.Gang, FractionCategory.Crime, "Vagos", 437);

            await AddIfNotExist(InDbContext, FractionType.Mafia, FractionCategory.Crime, "Russian mafia", 674);
            await AddIfNotExist(InDbContext, FractionType.Mafia, FractionCategory.Crime, "LCN", 674);

        }

        private async Task AddIfNotExist(GameWorldDbContext InDbContext, FractionType InType, FractionCategory InCategory, string InName, short InMapIcon)
        {
            var exist = await InDbContext.FractionEntity.AnyAsync(q => q.Type == InType && q.Category == InCategory && q.Name == InName);
            if (exist)
                return;

            var headQuarter = await InDbContext.HeadQuarterPropertyEntity.FirstOrDefaultAsync(q => q.Category == InCategory && q.Level == 0);
            if(headQuarter == null)
                return;

            headQuarter.Level = 1;

            InDbContext.FractionEntity.Add(new FractionEntity
            {
                EntityId = FractionId.GenerateId(),
                Type = InType,
                Category = InCategory,

                Name = InName,
                ShortName = InName,

                MapIcon = InMapIcon,
                HeadQuarterPropertyEntity = headQuarter,

                RankEntities = new List<FractionRankEntity>()
                {
                    new FractionRankEntity
                    {
                        EntityId = FractionRankId.GenerateId(),
                        Name = "Лидер",
                        Access = GameFractionRankAccess.All,
                        CreatedDate = DateTime.UtcNow,
                        Deleted = false,
                        SalaryPerHour = 1000,
                        SalaryPerWeek = 10000,
                    },

                    new FractionRankEntity
                    {
                        EntityId      = FractionRankId.GenerateId(),
                        Name          = "Новобранец",
                        Access        = GameFractionRankAccess.UseChat,
                        CreatedDate   = DateTime.UtcNow,
                        Deleted       = false,
                        SalaryPerHour = 10,
                        SalaryPerWeek = 100,
                    }
                }
            });
            await InDbContext.SaveChangesAsync();
        }
    }
}