﻿using SunCity.Domain.Interfaces.Entity;
using SunCity.Game.Enums.Fraction;
using SunCity.Game.World.DataBase.Entitites.Fraction;

namespace SunCity.Game.World.Fraction
{
    public class WorldFractionRank : IEntityWithId<FractionRankId>
    {
        public FractionRankId EntityId { get; }
        public GameFractionRankAccess Access { get; }
        public string Name { get; }

        public WorldFractionRank(FractionRankEntity InFractionRankEntity)
        {
            EntityId = InFractionRankEntity.EntityId;
            Access   = InFractionRankEntity.Access;
            Name     = InFractionRankEntity.Name;
        }

        public override string ToString()
        {
            return $"{EntityId} | {Name} | {Access}";
        }
    }
}