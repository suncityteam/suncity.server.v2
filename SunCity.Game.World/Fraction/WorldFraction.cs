﻿using System.Collections.Generic;
using System.Linq;
using SunCity.Domain.Interfaces.Entity;
using SunCity.Game.Enums.Fraction;
using SunCity.Game.World.DataBase.Entitites.Fraction;

namespace SunCity.Game.World.Fraction
{
    public class WorldFraction : IEntityWithId<FractionId>
    {
        public FractionId EntityId { get; }
        public FractionType Type { get; }
        public FractionCategory Category { get; }

        public string Name { get; }
        public string ShortName { get; }

        public int MapIcon { get; set; }
        public List<WorldFractionRank> Ranks { get; }

        public BankCardId? MainBankCardId { get; private set; }

        public WorldFraction(FractionEntity InFractionEntity)
        {
            EntityId = InFractionEntity.EntityId;
            ShortName = InFractionEntity.ShortName;
            Category = InFractionEntity.Category;
            Type = InFractionEntity.Type;
            Name = InFractionEntity.Name;
            MapIcon = InFractionEntity.MapIcon;
            MainBankCardId = InFractionEntity.MainBankCardId;

            Ranks = InFractionEntity.RankEntities
                                    .Where(q => q.Deleted == false)
                                    .Select(q => new WorldFractionRank(q))
                                    .ToList();
        }

        public override string ToString()
        {
            return $"{Type} | {Category} | {Name} | {EntityId}";
        }
    }
}
