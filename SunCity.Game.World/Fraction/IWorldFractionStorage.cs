﻿using System.Threading.Tasks;
using SunCity.Game.Types.Storage;
using SunCity.Game.World.Fraction.Crime.Gang;
using SunCity.Game.World.Fraction.Crime.Mafia;
using SunCity.Game.World.Fraction.Government.Army;
using SunCity.Game.World.Fraction.Government.CityHall;
using SunCity.Game.World.Fraction.Government.Police;

namespace SunCity.Game.World.Fraction
{
    public interface IWorldFractionStorage : IWorldStorage<FractionId, WorldFraction>
    {
        MafiaWorldFraction[] Mafia { get; }
        GangWorldFraction[] Gang { get; }
        ArmyWorldFraction[] Army { get; }
        PoliceWorldFraction[] Police { get; }
        CityHallWorldFraction Government { get; }

        Task Initialize();
    }
}