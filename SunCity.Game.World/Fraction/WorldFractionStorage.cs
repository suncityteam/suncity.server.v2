﻿using System;
using System.Linq;
using System.Threading.Tasks;
using GTANetworkAPI;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using SunCity.Common.EfCore;
using SunCity.Common.RageMp.Tasks;
using SunCity.Game.Enums.Fraction;
using SunCity.Game.Types.Storage;
using SunCity.Game.World.DataBase;
using SunCity.Game.World.Fraction.Crime.Gang;
using SunCity.Game.World.Fraction.Crime.Mafia;
using SunCity.Game.World.Fraction.Government.Army;
using SunCity.Game.World.Fraction.Government.CityHall;
using SunCity.Game.World.Fraction.Government.Police;

namespace SunCity.Game.World.Fraction
{
    internal class WorldFractionStorage : WorldStorage<FractionId, WorldFraction>, IWorldFractionStorage
    {
        public MafiaWorldFraction[] Mafia { get; private set; }
        public GangWorldFraction[] Gang { get; private set; }
        public ArmyWorldFraction[] Army { get; private set; }
        public PoliceWorldFraction[] Police { get; private set; }
        public CityHallWorldFraction Government { get; private set; }

        private ILogger<WorldFractionStorage> Logger { get; }
        private WorldFractionFactory Factory { get; }
        private WorldFractionSeed Seed { get; }
        private IServiceProvider ServiceProvider { get; }
        private IRageMpTaskManager RageMpTaskManager { get; }

        public WorldFractionStorage
        (
            ILogger<WorldFractionStorage> InLogger,
            WorldFractionSeed InSeed,
            WorldFractionFactory InFactory,
            IServiceProvider serviceProvider,
            IRageMpTaskManager rageMpTaskManager
        )
        {
            Logger = InLogger;
            Seed = InSeed;
            Factory = InFactory;
            ServiceProvider = serviceProvider;
            RageMpTaskManager = rageMpTaskManager;
        }

        public async Task Initialize()
        {
            using var scope = ServiceProvider.CreateScope();
            var dbContext = scope.ServiceProvider.GetService<GameWorldDbContext>();

            await Seed.Initialize(dbContext);

            Logger.LogInformation("Begin Initialize");

            var properties = await dbContext.FractionEntity
                .HasTracking(false)
                .Include(q => q.HeadQuarterPropertyEntity)
                .ThenInclude(q => q.PropertyEntity)
                .ThenInclude(q => q.OwnerEntities)
                .Include(q => q.MemberEntities)
                .Include(q => q.RankEntities)
                .ToArrayAsync();

            Logger.LogInformation($"Fetched {properties.Length} fractions");

            RageMpTaskManager.Schedule(() =>
            {
                foreach (var entity in properties)
                {
                    var fraction = Factory.CreateFraction(entity);
                    if (fraction.IsNotCorrect)
                    {
                        Logger.LogError($"Failed create fraction {entity.EntityId} / {entity.Type} | {entity.Name} | Error: {fraction.Error}");
                    }
                    else
                    {
                        Put(fraction.Content);
                    }
                }
            });

            Army = Values.Where(q => q.Type == FractionType.Army).Cast<ArmyWorldFraction>().ToArray();
            Police = Values.Where(q => q.Type == FractionType.Police).Cast<PoliceWorldFraction>().ToArray();

            Mafia = Values.Where(q => q.Type == FractionType.Mafia).Cast<MafiaWorldFraction>().ToArray();
            Gang = Values.Where(q => q.Type == FractionType.Gang).Cast<GangWorldFraction>().ToArray();

            Government = Values.FirstOrDefault(q => q.Type == FractionType.Government) as CityHallWorldFraction;

            Logger.LogInformation($"Loaded {Items.Count} fractions");
            Logger.LogInformation($"Loaded {Mafia.Length} mafia");
            Logger.LogInformation($"Loaded {Gang.Length} gangs");

            Logger.LogInformation("End Initialize");
        }
    }
}