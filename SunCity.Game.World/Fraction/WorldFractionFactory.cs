﻿using SunCity.Common.Operation;
using SunCity.Game.Enums.Fraction;
using SunCity.Game.World.DataBase.Entitites.Fraction;
using SunCity.Game.World.Fraction.Crime.Gang;
using SunCity.Game.World.Fraction.Crime.Mafia;
using SunCity.Game.World.Fraction.Government.Army;
using SunCity.Game.World.Fraction.Government.CityHall;
using SunCity.Game.World.Fraction.Government.Police;
using SunCity.Game.World.Property;

namespace SunCity.Game.World.Fraction
{
    internal class WorldFractionFactory
    {
        private WorldPropertyFactory PropertyFactory { get; }
        private IWorldPropertyStorage PropertyStorage { get; }

        public WorldFractionFactory(WorldPropertyFactory InPropertyFactory, IWorldPropertyStorage InPropertyStorage)
        {
            PropertyFactory = InPropertyFactory;
            PropertyStorage = InPropertyStorage;
        }


        public OperationResponse<WorldFraction> CreateFraction(FractionEntity InFractionEntity)
        {
            var fraction = Factory(InFractionEntity);
            if (fraction.IsNotCorrect)
                return fraction.Error;

            var property = PropertyFactory.CreateHeadQuarter(fraction.Content, InFractionEntity);
            if (property.IsNotCorrect)
                return property.Error;

            PropertyStorage.Put(property.Content);
            return fraction;
        }

        private OperationResponse<WorldFraction> Factory(FractionEntity InFractionEntity)
        {
            if (InFractionEntity.Type == FractionType.Mafia)
            {
                return new MafiaWorldFraction(InFractionEntity);
            }

            if (InFractionEntity.Type == FractionType.Gang)
            {
                return new GangWorldFraction(InFractionEntity);
            }

            if (InFractionEntity.Type == FractionType.Army)
            {
                return new ArmyWorldFraction(InFractionEntity);
            }

            if (InFractionEntity.Type == FractionType.Police)
            {
                return new PoliceWorldFraction(InFractionEntity);
            }

            if (InFractionEntity.Type == FractionType.Government)
            {
                return new CityHallWorldFraction(InFractionEntity);
            }

            return (OperationResponseCode.FractionFactoryNotFound, InFractionEntity.Type);
        }
    }
}