﻿using SunCity.Game.World.DataBase.Entitites.Fraction;

namespace SunCity.Game.World.Fraction.Crime
{
    public class CrimeWorldFraction : WorldFraction
    {
        public CrimeWorldFraction(FractionEntity InFractionEntity) : base(InFractionEntity)
        {
        }
    }
}
