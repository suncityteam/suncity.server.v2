﻿using SunCity.Game.World.DataBase.Entitites.Fraction;

namespace SunCity.Game.World.Fraction.Government.CityHall
{
    public class CityHallWorldFraction : GovernmentWorldFraction
    {
        public CityHallWorldFraction(FractionEntity InFractionEntity) : base(InFractionEntity)
        {
        }
    }
}