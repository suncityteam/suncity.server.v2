﻿using SunCity.Game.World.DataBase.Entitites.Fraction;

namespace SunCity.Game.World.Fraction.Government
{
    public class GovernmentWorldFraction : WorldFraction
    {
        public GovernmentWorldFraction(FractionEntity InFractionEntity) : base(InFractionEntity)
        {
        }
    }
}
