﻿using SunCity.Game.World.DataBase.Entitites.Fraction;

namespace SunCity.Game.World.Fraction.Government.Police
{
    public class PoliceWorldFraction : GovernmentWorldFraction
    {
        public PoliceWorldFraction(FractionEntity InFractionEntity) : base(InFractionEntity)
        {
        }
    }
}
