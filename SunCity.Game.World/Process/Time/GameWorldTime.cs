﻿using System;

namespace SunCity.Game.World.Process.Time
{
    public class GameWorldTime
    {
        public int Hours { get; set; }
        public int Minutes { get; set; }

        public void FromTimeSpan(TimeSpan InTimeSpan)
        {
            Hours = InTimeSpan.Hours;
            Minutes = InTimeSpan.Minutes;
        }

        public void Increment()
        {
            if (Minutes++ == 60)
            {
                if (Hours >= 23)
                {
                    Hours = 0;
                    Minutes = 0;
                }
                else
                {
                    Hours++;
                    Minutes = 0;
                }
            }
        }

        public override string ToString()
        {
            return $"Time: {Hours}:{Minutes}";
        }
    }
}