﻿using System;
using System.Threading.Tasks;
using Microsoft.Extensions.Options;
using SunCity.Common.Data;
using SunCity.Common.RageMp.World;
using SunCity.Game.World.Settings;

namespace SunCity.Game.World.Process.Time
{
    public class GameWorldTimeProcess
    {
        //=========================
        public GameWorldTime Time { get; } = new GameWorldTime();

        //=========================
        private IOptionsMonitor<GameWorldSettings> Settings { get; }
        private DataManager DataManager { get; }
        private IRageMpGameWorld RageMpGameWorld { get; }

        //=========================
        public GameWorldTimeProcess
        (
            IOptionsMonitor<GameWorldSettings> InSettings,
            DataManager InDataManager,
            IRageMpGameWorld rageMpGameWorld
        )
        {
            Settings = InSettings;
            DataManager = InDataManager;
            RageMpGameWorld = rageMpGameWorld;
            Task.Factory.StartNew(OnProcess, TaskCreationOptions.LongRunning);
        }

        private async Task OnProcess()
        {
            await Initialize();

            while (true)
            {
                Time.Increment();
                RageMpGameWorld.SetTime(Time.Hours, Time.Minutes, 0);
                await Task.Delay(Settings.CurrentValue.ServerMinuteDuration);
                await Save();
            }
        }

        private async Task Initialize()
        {
            var worldData = await DataManager.Read<GameWorldTimeData>(GameWorldTimeData.FileName);
            if (worldData.Exist && worldData.Data != null)
            {
                Time.FromTimeSpan(worldData.Data.Time);
            }
        }

        private Task Save()
        {
            return DataManager.Write(GameWorldTimeData.FileName, new GameWorldTimeData
            {
                Time = new TimeSpan(Time.Hours, Time.Minutes, seconds: 0)
            });
        }
    }
}