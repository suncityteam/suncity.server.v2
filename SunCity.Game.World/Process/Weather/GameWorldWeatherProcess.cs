﻿using System;
using System.Threading.Tasks;
using Microsoft.Extensions.Options;
using SunCity.Common.Data;
using SunCity.Common.Extensions;
using SunCity.Common.RageMp.World;
using SunCity.Game.World.Settings;

namespace SunCity.Game.World.Process.Weather
{
    public class GameWorldWeatherProcess
    {
        public GTANetworkAPI.Weather CurrentWeather { get; private set; }

        //=========================
        private DataManager DataManager { get; }
        private IOptionsMonitor<GameWorldSettings> Settings { get; }
        private IRageMpGameWorld RageMpGameWorld { get; }

        private GTANetworkAPI.Weather[] WeatherTypes { get; } = new GTANetworkAPI.Weather[]
        {
            GTANetworkAPI.Weather.CLEAR,
            GTANetworkAPI.Weather.CLEARING,

            GTANetworkAPI.Weather.EXTRASUNNY,
            GTANetworkAPI.Weather.CLOUDS,

            GTANetworkAPI.Weather.OVERCAST,
            GTANetworkAPI.Weather.RAIN,

            GTANetworkAPI.Weather.THUNDER
        };

        //=========================
        public GameWorldWeatherProcess
        (
            IOptionsMonitor<GameWorldSettings> InSettings,
            DataManager InDataManager,
            IRageMpGameWorld rageMpGameWorld
        )
        {
            Settings = InSettings;
            DataManager = InDataManager;
            RageMpGameWorld = rageMpGameWorld;
            Task.Factory.StartNew(OnProcess, TaskCreationOptions.LongRunning);
        }

        private async Task OnProcess()
        {
            await Initialize();
            while (true)
            {
                CurrentWeather = WeatherTypes.TakeRandom();

                var weatherTransition = Convert.ToSingle(Settings.CurrentValue.WeatherUpdateFrequency.TotalSeconds) / 2.0f;
                RageMpGameWorld.SetWeather(CurrentWeather /*, weatherTransition*/);
                await Task.Delay(Settings.CurrentValue.WeatherUpdateFrequency);
                await Save();
            }
        }

        private async Task Initialize()
        {
            var worldData = await DataManager.Read<GameWorldWeatherData>(GameWorldWeatherData.FileName);
            if (worldData.Exist && worldData.Data != null)
            {
                CurrentWeather = worldData.Data.Weather;
            }
        }

        private Task Save()
        {
            return DataManager.Write(GameWorldWeatherData.FileName, new GameWorldWeatherData
            {
                Weather = CurrentWeather
            });
        }
    }
}