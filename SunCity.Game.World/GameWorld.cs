﻿using System;
using System.Threading.Tasks;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using SunCity.Common.RageMp.Server;
using SunCity.Game.World.Chat;
using SunCity.Game.World.DataBase;
using SunCity.Game.World.Fraction;
using SunCity.Game.World.Interior;
using SunCity.Game.World.ItemModels;
using SunCity.Game.World.Licenses;
using SunCity.Game.World.Player;
using SunCity.Game.World.Property;
using SunCity.Game.World.StationPoint;
using SunCity.Game.World.Vehicle;
using SunCity.Game.World.VehicleModels;

namespace SunCity.Game.World
{
    internal class GameWorld : IGameWorld
    {
        private ILogger<GameWorld> Logger { get; }
        public IRageMpServer Server { get; }
        public IGameWorldChat Chat { get; }
        public IWorldPlayerStorage Players { get; }
        public IWorldPropertyStorage Properties { get; }
        public IWorldInteriorStorage Interiors { get; }
        public IWorldFractionStorage Fractions { get; }
        public IVehicleModelStorage VehicleModels { get; }
        public IWorldVehicleStorage Vehicles { get; }
        public IGameItemStorage Items { get; }
        public IGameLicenseStorage Licenses { get; }
        public IWorldStationPointStorage StationPoints { get; }
        private IServiceProvider ServiceProvider { get; }

        public GameWorld
        (
            ILogger<GameWorld> InLogger,
            IServiceProvider InServiceProvider,
            IWorldPlayerStorage InPlayers,
            IWorldPropertyStorage InProperties,
            IWorldInteriorStorage InInteriors,
            IWorldFractionStorage InFractions,
            IVehicleModelStorage InVehicleModels,
            IWorldVehicleStorage InVehicles,
            IGameItemStorage InItems,
            IGameLicenseStorage licenses,
            IWorldStationPointStorage stationPoints,
            IRageMpServer server, IGameWorldChat chat)
        {
            Players = InPlayers;
            Logger = InLogger;
            ServiceProvider = InServiceProvider;
            Properties = InProperties;
            Interiors = InInteriors;
            Fractions = InFractions;
            VehicleModels = InVehicleModels;
            Vehicles = InVehicles;
            Items = InItems;
            Licenses = licenses;
            StationPoints = stationPoints;
            Server = server;
            Chat = chat;
        }

        public async Task BeginPlay()
        {
            Logger.LogDebug($"BeginPlay with runtime {Environment.Version}");

            using var scope = ServiceProvider.CreateScope();
            var dbContext = scope.ServiceProvider.GetService<GameWorldDbContext>();

            var begin = DateTime.Now;
            await StationPoints.Initialize();
            
            await Licenses.Initialize(dbContext);
            
            await Items.Initialize(dbContext);
            //await VehicleModels.Initialize(dbContext);
            
            await Interiors.Initialize(dbContext);
            await Properties.Initialize();
            
            //await Fractions.Initialize();
            
            //await Vehicles.Initialize();
            var end = DateTime.Now;

            Logger.LogDebug($"Game world start takes: {end - begin}");
        }

        public Task EndPlay()
        {
            Logger.LogDebug("EndPlay");
            return Task.CompletedTask;
        }
    }
}