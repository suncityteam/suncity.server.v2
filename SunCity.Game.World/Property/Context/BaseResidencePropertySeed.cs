﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using SunCity.Common.Types.Math;
using SunCity.Game.Enums;
using SunCity.Game.Enums.World.Property;
using SunCity.Game.Types.Cost;
using SunCity.Game.Types.Property.Entity.Property;
using SunCity.Game.Types.Storage;
using SunCity.Game.World.DataBase;
using SunCity.Game.World.DataBase.Entitites.Property;
using SunCity.Game.World.DataBase.Entitites.Property.Residence;
using SunCity.Game.World.Interior;

namespace SunCity.Game.World.Property.Context
{
    public abstract class BaseResidencePropertySeed : WorldStorageSeed<GameWorldDbContext, PropertyId, PropertyEntity>, IPropertySeed
    {
        protected abstract int Prefix { get; }
        protected sealed override Func<GameWorldDbContext, DbSet<PropertyEntity>> Table => dbContext => dbContext.PropertyEntity;

        protected BaseResidencePropertySeed(ILogger logger) : base(logger)
        {
        }

        protected virtual void CustomizeProperty(PropertyEntity entity, float x, float y, float z, GameCost cost, InteriorId? interiorId = null)
        {
            entity.StateCost = new GameCost(cost.Amount, cost.Currency);
            entity.SellingCost = new GameCost(cost.Amount, cost.Currency);

            entity.Position = new Vector3D(x, y, z);
            entity.IconPosition = new Vector3D(x, y, z);

            entity.InteriorId = interiorId;
            if (entity.InteriorId == InteriorId.Parse(WorldInteriorSeed.Trailer))
            {
                entity.PropertyClass = WorldPropertyClass.Trailer;
            }
            else if (entity.InteriorId == InteriorId.Parse(WorldInteriorSeed.Economy1))
            {
                entity.PropertyClass = WorldPropertyClass.Economy;
            } 
            else if (entity.InteriorId == InteriorId.Parse(WorldInteriorSeed.Economy2))
            {
                entity.PropertyClass = WorldPropertyClass.EconomyPlus;
            }     
            else if (entity.InteriorId == InteriorId.Parse(WorldInteriorSeed.Comfort1))
            {
                entity.PropertyClass = WorldPropertyClass.Comfort;
            } 
            else if (entity.InteriorId == InteriorId.Parse(WorldInteriorSeed.Comfort2))
            {
                entity.PropertyClass = WorldPropertyClass.ComfortPlus;
            }       
            else if (entity.InteriorId == InteriorId.Parse(WorldInteriorSeed.Premium1))
            {
                entity.PropertyClass = WorldPropertyClass.Business;
            } 
            else if (entity.InteriorId == InteriorId.Parse(WorldInteriorSeed.Premium2))
            {
                entity.PropertyClass = WorldPropertyClass.Elite;
            }
        }

        protected virtual void CustomizeProperty(PropertyEntity entity, float x, float y, float z, InteriorId? interiorId = null)
        {
            CustomizeProperty(entity, x, y, z, new GameCost(100000, GameCurrency.Money), interiorId);
        }

        protected PropertyEntity CreatePropertyEntity(int idSuffix, Action<PropertyEntity> customizeProperty, Action<ResidencePropertyEntity> customize = null)
        {
            var entity = new ResidencePropertyEntity()
            {
                EntityId = CreatePropertyId(idSuffix),
                PropertyEntity = CreatePropertyEntity(idSuffix, string.Empty, customizeProperty),
            };

            customize?.Invoke(entity);
            customizeProperty?.Invoke(entity.PropertyEntity);

            entity.PropertyEntity.ResidencePropertyEntity = entity;

            return entity.PropertyEntity;
        }

        private PropertyId CreatePropertyId(int idSuffix)
        {
            var prefix = Prefix.ToString().PadLeft(8, '0');
            var suffix = idSuffix.ToString().PadLeft(12, '0');
            var id = $"{prefix}-69d7-48a9-b7a0-{suffix}";
            return new PropertyId(Guid.Parse(id));
        }

        private PropertyEntity CreatePropertyEntity(int idSuffix, string name, Action<PropertyEntity> customize)
        {
            var entity = new PropertyEntity
            {
                EntityId = CreatePropertyId(idSuffix),
                Name = new PropertyName(name),
                PropertyCategory = WorldPropertyCategory.House,
            };
            customize(entity);
            return entity;
        }
    }
}