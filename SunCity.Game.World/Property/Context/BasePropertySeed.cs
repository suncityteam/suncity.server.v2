﻿using SunCity.Game.Types.Storage;
using SunCity.Game.World.DataBase;
using SunCity.Game.World.DataBase.Entitites.Property;

namespace SunCity.Game.World.Property.Context
{
    public interface IPropertySeed : IWorldStorageSeed<GameWorldDbContext, PropertyId, PropertyEntity>
    {
        
    }
}