﻿using System;
using AutoMapper;
using Microsoft.Extensions.DependencyInjection;
using SunCity.Common.RageMp.Pools;

namespace SunCity.Game.World.Property.Context
{
    public class PropertyInitializeContext<TEntity>
    {
        public TEntity Entity { get; }
        public IServiceProvider ServiceProvider { get; }
                
        public IMapper Mapper => ServiceProvider.GetService<IMapper>();
        public IRageMpPool Pool => ServiceProvider.GetRequiredService<IRageMpPool>();

        public T GetService<T>() => ServiceProvider.GetService<T>();

        public PropertyInitializeContext(IServiceProvider InServiceProvider, TEntity InEntity)
        {
            ServiceProvider = InServiceProvider;
            Entity = InEntity;
        }
    }
}