﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using SunCity.Common.Types.Math;
using SunCity.Common.Types.Time;
using SunCity.Game.Enums;
using SunCity.Game.Types.Cost;
using SunCity.Game.Types.Property.Entity.Property;
using SunCity.Game.Types.Storage;
using SunCity.Game.World.DataBase;
using SunCity.Game.World.DataBase.Entitites.Property;
using SunCity.Game.World.DataBase.Entitites.Property.Business;
using SunCity.Game.World.DataBase.Entitites.Property.Interfaces;

namespace SunCity.Game.World.Property.Context
{
    public abstract class BaseBusinessPropertySeed<TSubPropertyEntity> : WorldStorageSeed<GameWorldDbContext, PropertyId, PropertyEntity>, IPropertySeed
        where TSubPropertyEntity : IWithBusinessPropertyEntity, new()
    {        
        protected abstract int Prefix { get; }
        protected sealed override Func<GameWorldDbContext, DbSet<PropertyEntity>> Table => dbContext => dbContext.PropertyEntity;
        protected abstract Action<BusinessPropertyEntity, TSubPropertyEntity> BusinessPropertyFiller { get; }

        protected BaseBusinessPropertySeed(ILogger logger) : base(logger)
        {
        }
        
        protected virtual void CustomizeProperty(PropertyEntity entity, float x, float y, float z, GameCost cost, InteriorId? interiorId = null)
        {
            entity.StateCost = new GameCost(cost.Amount, cost.Currency);
            entity.SellingCost = new GameCost(cost.Amount, cost.Currency);
            
            entity.Position = new Vector3D(x, y, z);
            entity.IconPosition = new Vector3D(x, y, z);
            
            entity.InteriorId = interiorId;
        }
        
        protected virtual void CustomizeProperty(PropertyEntity entity, float x, float y, float z, InteriorId? interiorId = null)
        {            
            CustomizeProperty(entity, x, y, z, new GameCost(100000, GameCurrency.Money), interiorId);
        }
        
        protected PropertyEntity CreatePropertyEntity(int idSuffix, string name, Action<TSubPropertyEntity> customize, Action<PropertyEntity> customizeProperty)
        {
            var entity = new TSubPropertyEntity
            {
                BusinessPropertyEntity = new BusinessPropertyEntity()
                {
                    EntityId = CreatePropertyId(idSuffix),
                    PropertyEntity = CreatePropertyEntity(idSuffix, name, customizeProperty),

                    Experience = 0,
                    Level = 0,
                    
                    WorkingTime = new TimeRange()
                    {
                        Begin = TimeSpan.FromHours(8.5),
                        End = TimeSpan.FromHours(19)
                    },
                }
            };

            customize(entity);
            BusinessPropertyFiller(entity.BusinessPropertyEntity, entity);
            entity.BusinessPropertyEntity.PropertyEntity.BusinessPropertyEntity = entity.BusinessPropertyEntity;
            return entity.BusinessPropertyEntity.PropertyEntity;
        }

        private PropertyId CreatePropertyId(int idSuffix)
        {
            var prefix = Prefix.ToString().PadLeft(8, '0');
            var suffix = idSuffix.ToString().PadLeft(12, '0');
            var id = $"{prefix}-69d7-48a9-b7a0-{suffix}";
            return new PropertyId(Guid.Parse(id));
        }

        private PropertyEntity CreatePropertyEntity(int idSuffix, string name, Action<PropertyEntity> customize)
        {
            var entity = new PropertyEntity
            {
                EntityId = CreatePropertyId(idSuffix),
                Name = new PropertyName(name),
            };
            customize(entity);
            return entity;
        }
    }
}