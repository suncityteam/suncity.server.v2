﻿using System;
using System.Collections.Generic;
using System.Linq;

using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using SunCity.Common.Types.Time;
using SunCity.Game.Enums;
using SunCity.Game.Enums.World.Property;
using SunCity.Game.Types.Cost;
using SunCity.Game.Types.Property.Category.Bank;
using SunCity.Game.Types.Property.Entity.Property;
using SunCity.Game.World.DataBase;
using SunCity.Game.World.DataBase.Entitites.Property;
using SunCity.Game.World.DataBase.Entitites.Property.Business;
using SunCity.Game.World.DataBase.Entitites.Property.Business.Bank;
using SunCity.Game.World.DataBase.Entitites.Property.Business.Bank.Tariff;
using SunCity.Game.World.DataBase.Entitites.Property.Business.CarShowroom;
using SunCity.Game.World.DataBase.Entitites.Property.Business.Hospital;
using SunCity.Game.World.DataBase.Entitites.Property.HeadQuarter;
using SunCity.Game.World.Property.Context;
using GTANetworkAPI;
using SunCity.Common.RageMp.Pools;

namespace SunCity.Game.World.Property
{
    public class WorldPropertySeed
    {
        private ILogger<WorldPropertySeed> Logger { get; }
        private IReadOnlyList<IPropertySeed> PropertySeeds { get; }
        
        public WorldPropertySeed(ILogger<WorldPropertySeed> InLogger, IServiceProvider serviceProvider)
        {
            Logger = InLogger;
            PropertySeeds = serviceProvider.GetServices<IPropertySeed>().ToArray();
        }


        public async Task Initialize(GameWorldDbContext InDbContext)
        {
            return;
            foreach (var seed in PropertySeeds)
            {
                await seed.Initialize(InDbContext);
            }
            
            //===================================
            //  Hospital
            await AddIfNotExist(InDbContext, WorldPropertyCategory.Hospital, new Vector3(-1385.481f, -976.4036f, 9.273162f), "Hospital");

            //===================================
            //  Car dealer
            await AddIfNotExist(InDbContext, WorldPropertyCategory.CarShowroom, new Vector3(-56.88f, -1097.12f, 26.52f), "CarShowroom");
            await AddIfNotExist(InDbContext, WorldPropertyCategory.MotoShowroom, new Vector3(286.76f, -1148.36f, 29.29f), "MotoShowroom");
            await AddIfNotExist(InDbContext, WorldPropertyCategory.BoatShowroom, new Vector3(-711.6249f, -1299.427f, 5.41f), "BoatShowroom");


            //===================================
            //  Fractions HQ
            //===================================

            //  Army
            await AddIfNotExist(InDbContext, WorldPropertyCategory.HeadQuarter, new Vector3(-598.51f, -929.95f, 23.87f), "Army");

            //  FBI
            await AddIfNotExist(InDbContext, WorldPropertyCategory.HeadQuarter, new Vector3(-177.19f, -1158.32f, 23.81f), "FBI");

            //  Government
            await AddIfNotExist(InDbContext, WorldPropertyCategory.HeadQuarter, new Vector3(-1285.544f, -567.0439f, 31.71239f), "Government");

            //  Police
            await AddIfNotExist(InDbContext, WorldPropertyCategory.HeadQuarter, new Vector3(-1111.952f, -824.9194f, 19.31578f), "LSPD");

            //===================================
            await AddBank(InDbContext, "Казначейство");
            await AddBank(InDbContext, "Maze");
        }

        private async Task AddBank(GameWorldDbContext InDbContext, string InBankName)
        {
            var exist = await InDbContext.PropertyEntity.AnyAsync(q => q.PropertyCategory == WorldPropertyCategory.Bank && q.Name == new PropertyName(InBankName));
            if (exist)
                return;

            var property = CreatePropertyEntity(WorldPropertyCategory.Bank, Vector3Consts.Zero, InBankName);
            property.BusinessPropertyEntity = CreateBusinessPropertyEntity(InEntity => InEntity.BankPropertyEntity = CreateBankPropertyEntity());

            InDbContext.PropertyEntity.Add(property);
            await InDbContext.SaveChangesAsync();
        }

        private BankPropertyEntity CreateBankPropertyEntity()
        {
            return new BankPropertyEntity
            {
                CardTariffEntities = new List<BankCardTariffEntity>()
                {
                    new BankCardTariffEntity()
                    {
                        EntityId = BankCardTariffId.GenerateId(),

                        Deposit  = new BankTariffFee(0, 0),
                        WithDraw = new BankTariffFee(0, 0),
                        Transfer = new BankTariffFee(0, 0),
                        MaintenanceCost = new GameCost(),
                        TariffCost = new GameCost(),
                        Name = "Эконом",
                    }
                }
            };
        }

        private BusinessPropertyEntity CreateBusinessPropertyEntity(Action<BusinessPropertyEntity> InFunc)
        {
            var property = new BusinessPropertyEntity
            {
                WorkingTime = new TimeRange()
                {
                    Begin = TimeSpan.FromHours(8.5),
                    End = TimeSpan.FromHours(19),
                }
            };

            InFunc.Invoke(property);
            return property;
        }

        private PropertyEntity CreatePropertyEntity(WorldPropertyCategory InPropertyCategory, Vector3 InPosition, string InName = null)
        {
            return new PropertyEntity
            {
                EntityId = PropertyId.GenerateId(),
                Name = new PropertyName(InName ?? string.Empty),
                PropertyCategory = InPropertyCategory,
                Position = InPosition,
                IconPosition = InPosition,
                PropertyClass = WorldPropertyClass.None,
                StateCost = new GameCost(1000, GameCurrency.Money),
                SellingCost = new GameCost(1000, GameCurrency.Money),
            };
        }

        private async Task AddIfNotExist(GameWorldDbContext InDbContext, WorldPropertyCategory InPropertyCategory, Vector3 InPosition, string InName = null)
        {
            var exist = await InDbContext.PropertyEntity.AnyAsync(q => q.PropertyCategory == InPropertyCategory && q.Position.X == InPosition.X && q.Position.Y == InPosition.Y && q.Position.Z == InPosition.Z);
            if (exist)
                return;

            var property = CreatePropertyEntity(InPropertyCategory, InPosition, InName);

            if (InPropertyCategory == WorldPropertyCategory.Hospital)
            {
                property.BusinessPropertyEntity = CreateBusinessPropertyEntity(InEntity => InEntity.HospitalPropertyEntity = new HospitalPropertyEntity());
            }
            else if (InPropertyCategory == WorldPropertyCategory.HeadQuarter)
            {
                property.HeadQuarterPropertyEntity = new HeadQuarterPropertyEntity();
            }
            else if (InPropertyCategory == WorldPropertyCategory.CarShowroom ||
                     InPropertyCategory == WorldPropertyCategory.MotoShowroom ||
                     InPropertyCategory == WorldPropertyCategory.BoatShowroom ||
                     InPropertyCategory == WorldPropertyCategory.AirShowroom)
            {
                property.BusinessPropertyEntity = CreateBusinessPropertyEntity(InEntity => InEntity.CarShowroomPropertyEntity = new CarShowroomPropertyEntity());
            }


            InDbContext.PropertyEntity.Add(property);
            await InDbContext.SaveChangesAsync();
        }
    }
}