﻿using System;
using SunCity.Domain.Interfaces.Entity;
using SunCity.Game.World.DataBase.Entitites.Property;

namespace SunCity.Game.World.Property
{
    public class WorldPropertyRequiredLicense
        : IEntityWithId<PropertyRequiredLicenseId>
        , IEntityWithCreatedDate
    {
        public WorldProperty Property { get; }
        
        public PropertyRequiredLicenseId EntityId { get; }
        public DateTime CreatedDate { get; }

        public LicenseId LicenseId { get; }
        public string AdminComment { get; }

        public WorldPropertyRequiredLicense(WorldProperty property, PropertyRequiredLicenseEntity entity)
        {
            Property = property;
            
            EntityId = entity.EntityId;
            CreatedDate = entity.CreatedDate;

            LicenseId = entity.RequiredLicenseId;
            AdminComment = entity.AdminComment;
        }

    }
}