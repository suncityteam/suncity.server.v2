﻿
using System.Threading.Tasks;
using GTANetworkAPI;
using SunCity.Common.RageMp.Pools.Players;
using SunCity.Game.Enums.World.Property;
using SunCity.Game.Types.Storage;
using SunCity.Game.World.Player;
using SunCity.Game.World.Property.Category.Business.GasStation;
using SunCity.Game.World.Property.Category.Business.Hospital;

namespace SunCity.Game.World.Property
{
    public interface IWorldPropertyStorage : IWorldStorage<PropertyId, WorldProperty>
    {
        bool IsExist(PropertyId InPropertyId);
        WorldProperty GetById(PropertyId InPropertyId);
        TWorldProperty GetById<TWorldProperty>(PropertyId InPropertyId) where TWorldProperty : WorldProperty;

        WorldProperty this[PropertyId? InPropertyId] { get; }
        Task Initialize();
        Task AddProperty(WorldProperty InProperty);

        Task<WorldProperty> FindNearProperty(Vector3 InPosition, WorldPropertyCategory category, float InRange = 3.0f);
        Task<TWorldProperty> FindNearProperty<TWorldProperty>(Vector3 InPosition, WorldPropertyCategory category, float InRange = 3.0f) where TWorldProperty : WorldProperty;
        
        Task<WorldProperty> FindNearProperty(Vector3 InPosition, uint InDimession, float InRange = 3.0f);
        Task<WorldProperty> FindNearProperty(IRagePlayer InPlayer);
        Task<GasStationPoint> FindNearGasStationPoint(IRagePlayer InPlayer);
        Task<WorldProperty> FindNearProperty(WorldPlayer InPlayer);

        Task<HospitalWorldProperty> FindNearHospital(Vector3 InPosition);
    }
}