﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using Microsoft.Extensions.Logging;
using SunCity.Common.Operation;
using SunCity.Common.Reflection;
using SunCity.Game.Enums.Fraction;
using SunCity.Game.Enums.World.Property;
using SunCity.Game.World.DataBase.Entitites.Fraction;
using SunCity.Game.World.DataBase.Entitites.Property;
using SunCity.Game.World.Fraction;
using SunCity.Game.World.Interior;
using SunCity.Game.World.Property.Category.HeadQuarter.Crime;
using SunCity.Game.World.Property.Category.HeadQuarter.Government;
using SunCity.Game.World.Property.Category.HeadQuarter.Police;
using SunCity.Game.World.Property.Context;
using SunCity.Game.World.Zone;

namespace SunCity.Game.World.Property
{
    [AttributeUsage(AttributeTargets.Class, AllowMultiple = true)]
    public class PropertyFactoryAttribute : Attribute
    {
        public WorldPropertyCategory Category { get; }

        public PropertyFactoryAttribute(WorldPropertyCategory InCategory)
        {
            Category = InCategory;
        }

    }

    public class WorldPropertyFactory
    {
        private IServiceProvider ServiceProvider { get; }
        private ILogger<WorldPropertyFactory> Logger { get; }
        private Dictionary<WorldPropertyCategory, Type> Factories { get; } = new Dictionary<WorldPropertyCategory, Type>(128);
        private IWorldInteriorStorage Interiors { get; }
        private IGameWorldZone WorldZone { get; }

        public WorldPropertyFactory(IServiceProvider InServiceProvider, ILogger<WorldPropertyFactory> InLogger, IWorldInteriorStorage InInteriors, IGameWorldZone InWorldZone)
        {
            ServiceProvider = InServiceProvider;
            Logger          = InLogger;
            Interiors       = InInteriors;
            WorldZone       = InWorldZone;
        }

        internal void Initialize()
        {
            Logger.LogInformation("Begin Initialize");

            var propertyTypes = 
                GenericReflectionExtensions.GetLoadedTypes()
               .Where(q => q.IsAbstract == false && q.IsImplementedClass<WorldProperty>())
               .ToArray();

            Logger.LogInformation($"Loaded property types: {propertyTypes.Length}");

            foreach (var propertyType in propertyTypes)
            {
                var attributes = propertyType.GetCustomAttributes<PropertyFactoryAttribute>();

                foreach (var attribute in attributes)
                {
                    if (Factories.ContainsKey(attribute.Category))
                    {
                        Logger.LogWarning($"> [Category: {attribute.Category}][Type: {propertyType.FullName}]: Exist duplicate");
                    }
                    else
                    {
                        Logger.LogInformation($"> [Category: {attribute.Category}][Type: {propertyType.FullName}]: Added");
                        Factories.Add(attribute.Category, propertyType);
                    }
                }
            }

            Logger.LogInformation("End Initialize");
        }

        public OperationResponse<WorldProperty> CreateHeadQuarter(WorldFraction InFraction, FractionEntity InFractionEntity)
        {
            var property = CreateHeadQuarter(InFraction);
            if (property.IsCorrect)
            {
                var context = CreateContext(InFractionEntity.HeadQuarterPropertyEntity.PropertyEntity);
                return property.Content.Initialize(context, null);
            }
            return property;
        }

        private OperationResponse<WorldProperty> CreateHeadQuarter(WorldFraction InFraction)
        {
            if (InFraction.Type == FractionType.Police)
            {
                return new PoliceHeadQuarterWorldProperty(InFraction);
            }

            if (InFraction.Type == FractionType.Government)
            {
                return new GovernmentHeadQuarterWorldProperty(InFraction);
            }

            if (InFraction.Type == FractionType.Gang)
            {
                return new CrimeHeadQuarterWorldProperty(InFraction);
            }

            if (InFraction.Type == FractionType.Mafia)
            {
                return new CrimeHeadQuarterWorldProperty(InFraction);
            }

            return (OperationResponseCode.PropertyFactoryNotFound, InFraction.Type);
        }

        private PropertyInitializeContext<PropertyEntity> CreateContext(PropertyEntity InPropertyEntity)
        {
            return new PropertyInitializeContext<PropertyEntity>(ServiceProvider, InPropertyEntity);
        }

        public OperationResponse<WorldProperty> CreateProperty(PropertyEntity InPropertyEntity)
        {
            if (Factories.TryGetValue(InPropertyEntity.PropertyCategory, out var propertyType))
            {
                var property = (WorldProperty)Activator.CreateInstance(propertyType);
                var interior = InPropertyEntity.InteriorId.HasValue ? Interiors[InPropertyEntity.InteriorId.Value] : null;
                return property.Initialize(CreateContext(InPropertyEntity), interior);
            }
            return (OperationResponseCode.PropertyFactoryNotFound, InPropertyEntity.PropertyCategory);
        }
    }
}