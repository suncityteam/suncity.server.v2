﻿using System;
using System.Linq;

using System.Threading.Tasks;

using GTANetworkAPI;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.DependencyInjection;
using SunCity.Common.RageMp.Pools.Players;
using SunCity.Common.RageMp.Tasks;
using SunCity.Common.Types.Extensions;
using SunCity.Game.Enums.World.Property;
using SunCity.Game.Types.Storage;
using SunCity.Game.World.DataBase;
using SunCity.Game.World.Player;
using SunCity.Game.World.Property.Category.Business.GasStation;
using SunCity.Game.World.Property.Category.Business.Hospital;
using SunCity.Game.World.Property.Loaders;

namespace SunCity.Game.World.Property
{
    internal sealed class WorldPropertyStorage : WorldStorage<PropertyId, WorldProperty>, IWorldPropertyStorage
    {
        //====================================================
        public WorldProperty this[PropertyId? InPropertyId] => InPropertyId.HasValue ? GetByKey(InPropertyId.Value) : null;

        //====================================================
        private WorldPropertyFactory Factory { get; }
        private ILogger<WorldPropertyStorage> Logger { get; }
        private WorldPropertySeed Seed { get; }
        private AbstractPropertyLoader[] Loaders { get; }

        //====================================================
        private HospitalWorldProperty[] Hospitals => ValuesCopy.Where(q => q.PropertyCategory == WorldPropertyCategory.Hospital).Cast<HospitalWorldProperty>().ToArray();
        private GasStationPoint[] GasStationPoints => ValuesCopy.Where(q => q.PropertyCategory == WorldPropertyCategory.GasStation).Cast<GasStationWorldProperty>().SelectMany(q => q.Points).ToArray();

        private IServiceProvider ServiceProvider { get; }
        private IRageMpTaskManager RageMpTaskManager { get; }

        //====================================================
        public WorldPropertyStorage
        (
            WorldPropertyFactory InFactory,
            ILogger<WorldPropertyStorage> InLogger,
            WorldPropertySeed InSeed,
            IServiceProvider serviceProvider, 
            IRageMpTaskManager rageMpTaskManager)
        {
            Loaders = serviceProvider.GetServices<AbstractPropertyLoader>().ToArray();
            Factory = InFactory;
            Logger = InLogger;
            Seed = InSeed;
            ServiceProvider = serviceProvider;
            RageMpTaskManager = rageMpTaskManager;
        }

        public async Task Initialize()
        {
            using var scope = ServiceProvider.CreateScope();
            var dbContext = scope.ServiceProvider.GetService<GameWorldDbContext>();

            await Seed.Initialize(dbContext);
        
            Logger.LogInformation("Begin Initialize");
            foreach (var loader in Loaders)
            {
                var properties = await loader.LoadProperties(dbContext);

                Logger.LogInformation($"Fetched {properties.Length} properties for {loader}");
                RageMpTaskManager.Schedule(() =>
                {
                    foreach (var entity in properties)
                    {
                        var property = Factory.CreateProperty(entity);
                        if (property.IsCorrect)
                        {
                            Put(property.Content);
                        }
                        else
                        {
                            Logger.LogError($"Failed load property {entity.EntityId} / {property.Error}");
                        }
                    }

                    Logger.LogInformation($"Loaded {Items.Count} properties");
                    Logger.LogInformation("End Initialize");
                });
            }
        }

        public Task AddProperty(WorldProperty InProperty)
        {
            Put(InProperty);
            return Task.CompletedTask;
        }

        public Task<WorldProperty> FindNearProperty(Vector3 InPosition, WorldPropertyCategory category, float InRange = 3)
        {
            return Task.Run(() => Values.AsParallel().FirstOrDefault(q => q.PropertyCategory == category && q.Position.InDistanceRange(InPosition, InRange)));
        }

        public async Task<TWorldProperty> FindNearProperty<TWorldProperty>(Vector3 InPosition, WorldPropertyCategory category, float InRange = 3) where TWorldProperty : WorldProperty
        {
            return await FindNearProperty(InPosition, category, InRange) as TWorldProperty;
        }

        public Task<WorldProperty> FindNearProperty(Vector3 InPosition, uint InDimession, float InRange = 3.0f)
        {
            return Task.Run(() => Values.AsParallel().FirstOrDefault(q => q.Position.InDistanceRange(InPosition, InRange)));
        }

        public Task<WorldProperty> FindNearProperty(IRagePlayer InPlayer)
        {
            var position = InPlayer.Position;
            var dimession = InPlayer.Dimension;
            return FindNearProperty(position, dimession);
        }

        public Task<GasStationPoint> FindNearGasStationPoint(IRagePlayer InPlayer)
        {
            return Task.Run(() => GasStationPoints.FirstOrDefault(q => q.Position.InDistanceRange(InPlayer.Position, 3.0f)));
        }

        public Task<WorldProperty> FindNearProperty(WorldPlayer InPlayer)
        {
            return FindNearProperty(InPlayer.RagePlayer);
        }

        public Task<HospitalWorldProperty> FindNearHospital(Vector3 InPosition)
        {
            return Task.Run(() => Hospitals.AsParallel().FirstOrDefault( /*q => q.Position.InDistanceRange(InPosition, 1000000)*/));
        }

        public TWorldProperty GetById<TWorldProperty>(PropertyId InPropertyId)
            where TWorldProperty : WorldProperty
        {
            return this[InPropertyId] as TWorldProperty;
        }

        public bool IsExist(PropertyId InPropertyId)
        {
            return ValuesCopy.Any(q => q.EntityId == InPropertyId);
        }

        public WorldProperty GetById(PropertyId InPropertyId)
        {
            return this[InPropertyId];
        }
    }
}