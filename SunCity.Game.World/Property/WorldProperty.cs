﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Threading.Tasks;
using GTANetworkAPI;
using SunCity.Common.Enum;
using SunCity.Common.RageMp.Pools;
using SunCity.Common.RageMp.Pools.Blips;
using SunCity.Common.RageMp.Pools.Markers;
using SunCity.Common.RageMp.Pools.TextLabels;
using SunCity.Common.Types.Extensions;
using SunCity.Domain.Interfaces.Entity;
using SunCity.Game.Enums.World.Property;
using SunCity.Game.Enums.World.Property.Owner;
using SunCity.Game.Types.Cost;
using SunCity.Game.Types.Property.Entity.Property;
using SunCity.Game.World.DataBase.Entitites.Property;
using SunCity.Game.World.DataBase.Entitites.Property.Interfaces;
using SunCity.Game.World.DataBase.Entitites.Property.Owners;
using SunCity.Game.World.Enums.Chat;
using SunCity.Game.World.Forms.Property.Interior;
using SunCity.Game.World.Interior;
using SunCity.Game.World.Models.Property.Owner;
using SunCity.Game.World.Player;
using SunCity.Game.World.Property.Context;
using SunCity.Game.World.Property.Interior;
using Color = System.Drawing.Color;

namespace SunCity.Game.World.Property
{
    public readonly struct BlipIconData
    {
        // https://wiki.rage.mp/index.php?title=Blips#Blip_model
        public uint Model { get; }

        // https://wiki.rage.mp/index.php?title=Blips#Blip_colors
        public byte Color { get; }

        public MarkerType Marker { get; }

        /// <summary>
        /// Название на карте
        /// </summary>
        public string Name { get; }

        public BlipIconData(MarkerType InMarker, uint InModel, byte InColor, string InName)
        {
            Marker = InMarker;
            Model = InModel;
            Color = InColor;
            Name = InName;
        }
    }

    public abstract class WorldProperty<TPropertyEntity> : WorldProperty
        where TPropertyEntity : IWithPropertyEntity
    {
        private protected abstract Func<PropertyEntity, TPropertyEntity> CategoryEntitySelector { get; }
        protected abstract WorldProperty Initialize(PropertyInitializeContext<TPropertyEntity> InEntity);

        public override WorldProperty Initialize(PropertyInitializeContext<PropertyEntity> InContext, WorldInterior InWorldInterior)
        {
            base.Initialize(InContext, InWorldInterior);
            var categoryEntity = CategoryEntitySelector.Invoke(InContext.Entity);
            return Initialize(new PropertyInitializeContext<TPropertyEntity>(InContext.ServiceProvider, categoryEntity));
        }
    }

    public abstract class WorldProperty
        : IEntityWithId<PropertyId>
            , IEntityWithName<PropertyName>
    {
        private IRageMpPool Pool { get; set; }

        //=======================================
        public PropertyId EntityId { get; private set; }
        public PropertyName Name { get; private set; }

        //==================================
        public IReadOnlyList<WorldPropertyRequiredLicense> RequiredLicenses { get; private set; }

        //==================================
        public WorldPropertyCategory PropertyCategory { get; private set; }
        public WorldPropertyClass PropertyClass { get; private set; }

        public string PropertyCategoryName => PropertyCategory.GetDescription();
        public string PropertyClassName => PropertyClass.GetDescription();


        //=======================================
        public Vector3 IconPosition { get; private set; }
        public Vector3 Position { get; private set; }

        public float Rotation { get; private set; }
        public uint VirtualWorld { get; private set; }

        //=======================================
        [NotNull] public IRageBlip MapIcon { get; protected set; }

        [NotNull] public IRageMarker Marker { get; protected set; }

        [NotNull] public IRageTextLabel Label { get; protected set; }

        //=======================================
        public bool Locked { get; private set; }
        public float Square { get; private set; }

        [MaybeNull] public WorldPropertyInterior Interior { get; private set; }
        public bool HasInterior => Interior != null;

        //==================================
        [NotNull] public GameCost StateCost { get; private set; }

        [NotNull] public GameCost OwnerCost { get; private set; }

        [MaybeNull]
        public GameCost SellingCost
        {
            get
            {
                if (IsSelling)
                {
                    if (HasOwner)
                        return OwnerCost;

                    return StateCost;
                }

                return null;
            }
        }

        //==================================
        /// <summary>
        /// <para>Банковская карта для</para>
        /// <para>списания налогов, оплаты за услуги</para>
        /// <para>начисления средств после продажи недвижимости</para>
        /// </summary>
        public BankCardId? OwnerBankCardId { get; set; }

        private bool OwnerSelling { get; set; }
        public bool HasOwner => Owners.Any(q => q.Type == PropertyOwnerType.Owner);

        public List<WorldPropertyOwner> Owners { get; private set; }
        public bool IsSelling => HasOwner == false || OwnerSelling;
        public bool IsNotSelling => !IsSelling;


        //=======================================
        public abstract BlipIconData BlipPurchased { get; }
        public abstract BlipIconData BlipForSale { get; }
        public BlipIconData ActualBlipIcon => IsSelling ? BlipForSale : BlipPurchased;
        public bool IsOwner { get; set; }


        //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

        public virtual Task OnAction(WorldPlayer InWorldPlayer, WorldPropertyInteriorAction InAction)
        {
            return Task.CompletedTask;
        }

        public virtual bool IsAllowPlayerEnter(WorldPlayer InWorldPlayer)
        {
            if (Locked)
            {
                return false;
            }

            return true;
        }

        public virtual async Task OnPlayerEnter(WorldPlayer InWorldPlayer)
        {
            var bAllowPlayerEnter = IsAllowPlayerEnter(InWorldPlayer);

            if (bAllowPlayerEnter)
            {
                if (Interior != null)
                {
                    //===========================================
                    //  Для игроков на улице
                    await InWorldPlayer.SendMessageToNearbyPlayers(10.0f, WorldChatMessaveRecive.IncludeSenderPlayer, new[]
                    {
                        "Открывает дверь",
                        "Входит в квартиру",
                    });

                    //===========================================
                    InWorldPlayer.OnEnterProperty(this);

                    //===========================================
                    //  Для игроков в интерьере
                    await InWorldPlayer.SendMessageToNearbyPlayers(10.0f, WorldChatMessaveRecive.ExcludeSenderPlayer, new[]
                    {
                        "Открывает дверь",
                        "Входит в квартиру",
                    });

                    //===========================================
                    //await ShowWelcomeMessage(InPlayer.Player);
                }
            }
            else
            {
                await InWorldPlayer.SendMessageToNearbyPlayers(10.0f, WorldChatMessaveRecive.IncludeSenderPlayer, $"{InWorldPlayer.Name}: Пытается открыть дверь (дверь заперта)");
            }
        }

        public virtual async Task OnPlayerExit(WorldPlayer InPlayer)
        {
            InPlayer.SendMessageToNearbyPlayers(10.0f, WorldChatMessaveRecive.IncludeSenderPlayer, new[]
            {
                " Открывает дверь",
                " Выходит из квартиры"
            });

            InPlayer.OnExitProperty(this);

            await InPlayer.SendMessageToNearbyPlayers(10.0f, WorldChatMessaveRecive.ExcludeSenderPlayer, new[]
            {
                " Открывает дверь",
                " Выходит из квартиры"
            });
        }

        protected virtual void PostInitialize()
        {
            if (HasInterior)
            {
                Interior.Initialize(InitializeInterior(), Pool);
            }
        }

        public virtual WorldProperty Initialize(PropertyInitializeContext<PropertyEntity> InContext, WorldInterior InWorldInterior)
        {
            Pool = InContext.Pool;

            //---------------------------------------
            EntityId = InContext.Entity.EntityId;
            OwnerBankCardId = InContext.Entity.OwnerBankCardId;

            //---------------------------------------
            VirtualWorld = BitConverter.ToUInt32(EntityId.Id.ToByteArray());

            //---------------------------------------
            Name = InContext.Entity.Name;
            PropertyClass = InContext.Entity.PropertyClass;
            PropertyCategory = InContext.Entity.PropertyCategory;

            //---------------------------------------
            Position = InContext.Entity.Position;
            IconPosition = InContext.Entity.IconPosition.AsVector3();
            Rotation = InContext.Entity.Rotation;

            //---------------------------------------
            StateCost = InContext.Entity.StateCost;
            OwnerCost = InContext.Entity.SellingCost;
            OwnerSelling = InContext.Entity.IsSelling;

            //---------------------------------------
            if (InContext.Entity.RequiredLicenses == null)
            {
                RequiredLicenses = Array.Empty<WorldPropertyRequiredLicense>();
            }
            else
            {
                RequiredLicenses = InContext.Entity.RequiredLicenses.Select(q => new WorldPropertyRequiredLicense(this, q)).ToArray();
            }

            //---------------------------------------
            if (InWorldInterior != null)
            {
                Interior = new WorldPropertyInterior(this, InWorldInterior);
            }

            //---------------------------------------
            if (InContext.Entity.OwnerEntities == null)
            {
                Owners = new List<WorldPropertyOwner>();
            }
            else
            {
                Owners = CreateOwners(InContext.Entity.OwnerEntities);
            }

            //---------------------------------------
            Locked = false;

            //---------------------------------------
            CreateOrUpdateIcon();

            //---------------------------------------
            PostInitialize();

            //---------------------------------------
            return this;
        }

        public void UpdateOwners(WorldPropertyOwner InPropertyOwner, BankCardId InOwnerBankCardId)
        {
            OwnerSelling = false;
            OwnerBankCardId = InOwnerBankCardId;

            Owners.RemoveAll(q => q.Type == PropertyOwnerType.Owner);
            Owners.Add(InPropertyOwner);

            CreateOrUpdateIcon();
        }

        private static List<WorldPropertyOwner> CreateOwners(List<PropertyOwnerEntity> InOwnerEntities)
        {
            var owners = new List<WorldPropertyOwner>(InOwnerEntities.Count);

            foreach (var ownerEntity in InOwnerEntities.Where(q => q.Group == PropertyOwnerGroup.Player && q.End == null))
            {
                owners.Add(new WorldPropertyPlayerOwner
                {
                    CharacterId = ownerEntity.PlayerOwnerEntity.CharacterId,
                    Type = ownerEntity.Type
                });
            }

            foreach (var ownerEntity in InOwnerEntities.Where(q => q.Group == PropertyOwnerGroup.Fraction && q.End == null))
            {
                owners.Add(new WorldPropertyFractionOwner()
                {
                    FractionId = ownerEntity.FractionOwnerEntity.FractionId,
                    Type = ownerEntity.Type
                });
            }

            return owners;
        }

        protected abstract string GetPickupTextLabelMessage();


        //=======================================
        protected abstract PropertyInteriorInitializeForm InitializeInterior();

        protected virtual void CreateOrUpdateMapIcon()
        {
            if (MapIcon == null)
            {
                MapIcon = Pool.Blips.New(ActualBlipIcon.Model, Position, 1.0f, ActualBlipIcon.Color, ActualBlipIcon.Name ?? string.Empty);
            }
            else
            {
                // MapIcon.Model = ActualBlipIcon.Model;
                MapIcon.Color=ActualBlipIcon.Color;
                MapIcon.Name=ActualBlipIcon.Name ?? string.Empty;
            }
        }

        protected virtual void CreateOrUpdatePickupIcon()
        {
            if (Marker == null)
            {
                Marker = Pool.Markers.New(ActualBlipIcon.Marker, Position, Vector3Consts.Zero, Vector3Consts.Zero, 1.0f, Colors.White, true);
            }
            else
            {
                // Marker.Model = (uint) ActualBlipIcon.Marker;
            }
        }

        protected virtual void CreateOrUpdatePickupLabel()
        {
            var message = GetPickupTextLabelMessage();
            if (Label == null)
            {
                Label = Pool.TextLabels.New(Position.AddZ(0.75f), message, 0, Colors.White);
            }
            else
            {
                Label.Text = message;
            }
        }

        protected virtual void CreateOrUpdateIcon()
        {
            CreateOrUpdatePickupIcon();
            CreateOrUpdatePickupLabel();
            CreateOrUpdateMapIcon();
        }

        public async Task OnSaveChanges(PropertyEntity InPropertyEntity)
        {
            InPropertyEntity.IsSelling = OwnerSelling;
            InPropertyEntity.Name = Name;
        }
    }
}