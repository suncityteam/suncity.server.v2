﻿using System.Diagnostics.CodeAnalysis;
using System.Drawing;
using System.Linq;

using SunCity.Common.Types.Extensions;
using SunCity.Game.Types.Property.Entity.Interior;
using SunCity.Game.World.Forms.Property.Interior;
using SunCity.Game.World.Interior;
using GTANetworkAPI;
using SunCity.Common.RageMp.Pools;
using SunCity.Common.RageMp.Pools.Blips;
using SunCity.Common.RageMp.Pools.Markers;
using SunCity.Common.RageMp.Pools.TextLabels;

namespace SunCity.Game.World.Property.Interior
{
    public sealed class WorldPropertyInterior
    {
        [NotNull]
        internal WorldProperty Property { get; }

        [NotNull]
        public WorldInterior Interior { get; }

        [NotNull]
        public IRageTextLabel ExitTextLabel { get; private set; }

        [NotNull]
        public IRageMarker Marker { get; private set; }

        #region Proxy
        public InteriorId EntityId => Interior.EntityId;
        public InteriorItemPlacement IPL => Interior.IPL;

        public Vector3 Position => Interior.Position;
        public float Rotation => Interior.Rotation;

        public Vector3 SpawnPosition => Interior.SpawnPosition;
        public float SpawnRotation => Interior.SpawnRotation;
        #endregion

        internal WorldPropertyInteriorAction[] Actions { get; }

        public bool TryGetNearAction(Vector3 InPlayerPosition, out WorldPropertyInteriorAction InInteriorAction)
        {
            InInteriorAction = Actions.FirstOrDefault(q => q.Action.Position.InDistanceRange(InPlayerPosition, 2.5));
            return InInteriorAction != null;
        }


        internal WorldPropertyInterior(WorldProperty InProperty, WorldInterior InInterior)
        {
            Property = InProperty;
            Interior = InInterior;
            Actions = InInterior.Actions.Select(q => new WorldPropertyInteriorAction(this, q)).ToArray();
        }

        internal WorldPropertyInterior Initialize(PropertyInteriorInitializeForm InInitializeForm, IRageMpPool pool)
        {
            ExitTextLabel = pool.TextLabels.New(Interior.Position, InInitializeForm.TextLabel, 0, Colors.White, dimension: Property.VirtualWorld);
            Marker = pool.Markers.New(InInitializeForm.Marker, Interior.Position, Vector3Consts.Zero, Vector3Consts.Zero, 1.0f, Colors.White, true, dimension: Property.VirtualWorld);
            
            foreach (var action in Actions)
            {
                action.Initialize(pool);
            }
            
            return this;
        }

        public override string ToString()
        {
            return $"Property: {Property.EntityId}, {Interior}, Actions: {Actions.Length}";
        }
    }
}
