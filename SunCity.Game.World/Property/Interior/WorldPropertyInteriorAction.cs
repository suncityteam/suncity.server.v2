﻿using GTANetworkAPI;
using SunCity.Common.RageMp.Pools;
using SunCity.Common.RageMp.Pools.Blips;
using SunCity.Common.RageMp.Pools.Markers;
using SunCity.Common.RageMp.Pools.TextLabels;
using SunCity.Game.Enums.World.Interior;
using SunCity.Game.World.Interior;

namespace SunCity.Game.World.Property.Interior
{
    public sealed class WorldPropertyInteriorAction
    {
        public WorldPropertyInterior Interior { get; }
        public WorldInteriorAction Action { get; }
        public InteriorActionType ActionType => Action.Action;

        private IRageTextLabel TextLabel { get; set; }
        private IRageMarker Marker { get; set; }

        private WorldProperty Property => Interior.Property;

        internal WorldPropertyInteriorAction(WorldPropertyInterior InInterior, WorldInteriorAction InAction)
        {
            Action = InAction;
            Interior = InInterior;
        }

        internal WorldPropertyInteriorAction Initialize(IRageMpPool pool)
        {
            TextLabel = pool.TextLabels.New(Action.Position, Action.ActionString, 0, Colors.White, dimension: Property.VirtualWorld);
            Marker    = pool.Markers.New(MarkerType.CheckeredFlagCircle, Action.Position, Vector3Consts.Zero, Vector3Consts.Zero, 1.0f, Colors.White, true, dimension: Property.VirtualWorld);
            return this;
        }

        public override string ToString()
        {
            return $"{Action.ActionString} | Property: {Property.EntityId}, {Interior}";
        }
    }
}