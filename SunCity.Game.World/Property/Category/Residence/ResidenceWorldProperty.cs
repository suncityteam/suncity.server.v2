﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GTANetworkAPI;
using SunCity.Game.Enums.World.Property;
using SunCity.Game.Enums.World.Property.Owner;
using SunCity.Game.World.DataBase.Entitites.Property;
using SunCity.Game.World.DataBase.Entitites.Property.Residence;
using SunCity.Game.World.Forms.Property.Interior;
using SunCity.Game.World.Property.Context;

namespace SunCity.Game.World.Property.Category.Residence
{
    [PropertyFactory(WorldPropertyCategory.House)]
    [PropertyFactory(WorldPropertyCategory.Apartment)]
    public class ResidenceWorldProperty : WorldProperty<ResidencePropertyEntity>
    {
        private protected sealed override Func<PropertyEntity, ResidencePropertyEntity> CategoryEntitySelector => InEntity => InEntity.ResidencePropertyEntity;

        public override BlipIconData BlipPurchased { get; } = new BlipIconData(/*MarkerType.Dollar*/ MarkerType.ChevronUpx2, 411, 1, "Дом");
        public override BlipIconData BlipForSale { get; } = new BlipIconData(MarkerType.ChevronUpx2, 374, 2, "Продается дом");

        protected override PropertyInteriorInitializeForm InitializeInterior() => new PropertyInteriorInitializeForm
        {
            Marker    = MarkerType.CheckeredFlagCircle,
            TextLabel = "Выйти из дома"
        };


        protected override WorldProperty Initialize(PropertyInitializeContext<ResidencePropertyEntity> InEntity)
        {
            return this;
        }

        protected override string GetPickupTextLabelMessage()
        {
            var sb = new StringBuilder(256);

            //----------------------
            if (IsSelling)
            {
                sb.AppendLine($"{PropertyCategoryName} продается");
                sb.AppendLine($"Класс ~y~{PropertyClassName}");

                sb.AppendLine($"~w~Площадь: ~y~{Square:F}м²");
                sb.AppendLine($"~w~Стоимость:\n{SellingCost}");
            }
            else
            {
                sb.AppendLine(PropertyCategoryName);
                sb.AppendLine($"Класс ~y~{PropertyClassName}");

                var owner = Owners.First(q => q.Type == PropertyOwnerType.Owner);
                sb.AppendLine($"~w~Владелец: ~g~{owner.Name}");

                sb.AppendLine($"~w~Для входа нажмите ~y~ALT");
            }

            //----------------------
            return sb.ToString();
        }
    }
}
