﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SunCity.Common.Types.Time;
using SunCity.Game.Enums.World.Property;
using SunCity.Game.Enums.World.Property.Owner;
using SunCity.Game.World.DataBase.Entitites.Property;
using SunCity.Game.World.DataBase.Entitites.Property.Business;
using SunCity.Game.World.DataBase.Entitites.Property.Interfaces;
using SunCity.Game.World.Interior;
using SunCity.Game.World.Property.Context;

namespace SunCity.Game.World.Property.Category.Business
{
    public abstract class BusinessWorldProperty<TPropertyEntity> : WorldProperty<BusinessPropertyEntity>
        where TPropertyEntity : IWithBusinessPropertyEntity
    {
        protected abstract Func<BusinessPropertyEntity, TPropertyEntity> SubCategoryEntitySelector { get; }
        private protected sealed override Func<PropertyEntity, BusinessPropertyEntity> CategoryEntitySelector => InEntity => InEntity.BusinessPropertyEntity;

        public TimeRange WorkingTime { get; private set; }

        //=======================================================
        public int Level { get; private set; }
        public long Experience { get; private set; }

        //=======================================================
        protected abstract WorldProperty Initialize(PropertyInitializeContext<TPropertyEntity> InContext);

        protected override WorldProperty Initialize(PropertyInitializeContext<BusinessPropertyEntity> InContext)
        {
            //-------------------------------------------
            Level      = InContext.Entity.Level;
            Experience = InContext.Entity.Experience;

            //-------------------------------------------
            WorkingTime = InContext.Entity.WorkingTime;

            //-------------------------------------------
            return this;
        }

        protected override string GetPickupTextLabelMessage()
        {
            var sb = new StringBuilder(256);

            //----------------------
            if (IsSelling)
            {
                sb.AppendLine($"{PropertyCategoryName} продается");
                if (PropertyClass != WorldPropertyClass.None)
                {
                    sb.AppendLine($"Класс ~y~{PropertyClassName}");
                }

                sb.AppendLine($"~w~Стоимость:\n{SellingCost}");
            }
            else
            {
                if (string.IsNullOrWhiteSpace(Name.Name))
                {
                    sb.AppendLine(PropertyCategoryName);
                }
                else
                {
                    sb.AppendLine(Name.Name);
                }

                if (PropertyClass != WorldPropertyClass.None)
                {
                    sb.AppendLine($"Класс ~y~{PropertyClassName}");
                }

                sb.AppendLine($"~w~Режим работы ~y~{WorkingTime}");

                var owner = Owners.First(q => q.Type == PropertyOwnerType.Owner);
                sb.AppendLine($"~w~Владелец: ~g~{owner.Name}");

                if (HasInterior)
                {
                    sb.AppendLine($"~w~Для входа нажмите ~y~ALT");
                }
                else
                {
                    sb.AppendLine($"~w~Для взаимодействия нажмите ~y~ALT");
                }
            }

            //----------------------
            return sb.ToString();
        }

        public override WorldProperty Initialize(PropertyInitializeContext<PropertyEntity> InContext, WorldInterior InWorldInterior)
        {
            base.Initialize(InContext, InWorldInterior);

            var сategoryEntity = CategoryEntitySelector.Invoke(InContext.Entity);
            var subCategoryEntity = SubCategoryEntitySelector.Invoke(сategoryEntity);
            this.Initialize(new PropertyInitializeContext<TPropertyEntity>(InContext.ServiceProvider, subCategoryEntity));

            return this;
        }
    }
}
