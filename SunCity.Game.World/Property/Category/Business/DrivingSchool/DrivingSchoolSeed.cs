﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using SunCity.Game.Enums;
using SunCity.Game.Enums.World.Property;
using SunCity.Game.Types.Cost;
using SunCity.Game.World.DataBase;
using SunCity.Game.World.DataBase.Entitites.Property.Business;
using SunCity.Game.World.DataBase.Entitites.Property.Business.DrivingSchool;
using SunCity.Game.World.Licenses;
using SunCity.Game.World.Property.Context;

namespace SunCity.Game.World.Property.Category.Business.DrivingSchool
{
    public class DrivingSchoolSeed : BaseBusinessPropertySeed<DrivingSchoolPropertyEntity>
    {
        protected override int Prefix { get; } = (int) WorldPropertyCategory.DrivingSchool;
        protected override Action<BusinessPropertyEntity, DrivingSchoolPropertyEntity> BusinessPropertyFiller => (business, entity) => business.DrivingSchoolPropertyEntity = entity;

        public DrivingSchoolSeed(ILogger<DrivingSchoolSeed> logger) : base(logger)
        {
        }
        
        public override async Task Initialize(GameWorldDbContext InDbContext)
        {
            await AddIfNotExist(InDbContext, CreatePropertyEntity(1, "Автошкола", FillDrivingLicense, entity => CustomizeProperty(entity, 1, 1, 1)));
            await AddIfNotExist(InDbContext, CreatePropertyEntity(2, "Автошкола", FillDrivingLicense, entity => CustomizeProperty(entity, 1, 1, 1)));
            await AddIfNotExist(InDbContext, CreatePropertyEntity(3, "Автошкола", FillDrivingLicense, entity => CustomizeProperty(entity, 1, 1, 1)));

            await AddIfNotExist(InDbContext, CreatePropertyEntity(4, "Летная школа", FillAirLicense, entity => CustomizeProperty(entity, 1, 1, 1)));
            await AddIfNotExist(InDbContext, CreatePropertyEntity(5, "Летная школа", FillAirLicense, entity => CustomizeProperty(entity, 1, 1, 1)));

            await InDbContext.SaveChangesAsync();
        }

        private void FillAirLicense(DrivingSchoolPropertyEntity entity)
        {
            entity.Licenses = new List<DrivingSchoolLicenseEntity>
            {
                //    "Лицензия на управление самолетом"
                //    За внутри игровую валюту
                new DrivingSchoolLicenseEntity()
                {
                    EntityId = DrivingSchoolLicenseId.GenerateId(),
                    LicenseId = LicenseId.Parse(GameLicenseSeed.AirDrivingLicenceId),
                    Cost = new GameCost(100000, GameCurrency.Money),
                },

                //    "Лицензия на управление самолетом"
                //    За донат
                new DrivingSchoolLicenseEntity()
                {
                    EntityId = DrivingSchoolLicenseId.GenerateId(),
                    LicenseId = LicenseId.Parse(GameLicenseSeed.AirDrivingLicenceId),
                    Cost = new GameCost(10000, GameCurrency.Donate),
                },

                //    "Лицензия на управление вертолетом"
                //    За донат
                new DrivingSchoolLicenseEntity()
                {
                    EntityId = DrivingSchoolLicenseId.GenerateId(),
                    LicenseId = LicenseId.Parse(GameLicenseSeed.HelicopterDrivingLicenceId),
                    Cost = new GameCost(5000, GameCurrency.Donate),
                },

                //    "Лицензия на управление вертолетом"
                //    За внутри игровую валюту
                new DrivingSchoolLicenseEntity()
                {
                    EntityId = DrivingSchoolLicenseId.GenerateId(),
                    LicenseId = LicenseId.Parse("95bc30e8-69d7-48a9-b7a0-1102ba665505"),
                    Cost = new GameCost(100000, GameCurrency.Money),
                },
            };
        }

        private void FillDrivingLicense(DrivingSchoolPropertyEntity entity)
        {
            entity.Licenses = new List<DrivingSchoolLicenseEntity>
            {
                //    "Лицензия на управление легковым автомобилем"
                //    За внутри игровую валюту
                new DrivingSchoolLicenseEntity()
                {
                    EntityId = DrivingSchoolLicenseId.GenerateId(),
                    LicenseId = LicenseId.Parse(GameLicenseSeed.DrivingLicenceId),
                    Cost = new GameCost(1000, GameCurrency.Money),
                },

                //    "Лицензия на управление легковым автомобилем"
                //    За донат
                new DrivingSchoolLicenseEntity()
                {
                    EntityId = DrivingSchoolLicenseId.GenerateId(),
                    LicenseId = LicenseId.Parse(GameLicenseSeed.DrivingLicenceId),
                    Cost = new GameCost(1000, GameCurrency.Donate),
                },

                //    "Лицензия на управление грузовым автомобилем"
                //    За внутри игровую валюту
                new DrivingSchoolLicenseEntity()
                {
                    EntityId = DrivingSchoolLicenseId.GenerateId(),
                    LicenseId = LicenseId.Parse(GameLicenseSeed.HeavyDrivingLicenceId),
                    Cost = new GameCost(5000, GameCurrency.Money),
                },

                //    "Лицензия на управление мотоциклом"
                //    За внутри игровую валюту
                new DrivingSchoolLicenseEntity()
                {
                    EntityId = DrivingSchoolLicenseId.GenerateId(),
                    LicenseId = LicenseId.Parse(GameLicenseSeed.MotoDrivingLicenceId),
                    Cost = new GameCost(500, GameCurrency.Money),
                },
            };
        }
    }
}