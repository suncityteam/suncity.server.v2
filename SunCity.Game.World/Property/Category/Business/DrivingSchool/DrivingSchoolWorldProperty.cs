﻿using System;
using System.Collections.Generic;
using System.Linq;
using GTANetworkAPI;
using SunCity.Game.Enums.World.Property;
using SunCity.Game.World.DataBase.Entitites.Property.Business;
using SunCity.Game.World.DataBase.Entitites.Property.Business.DrivingSchool;
using SunCity.Game.World.Forms.Property.Interior;
using SunCity.Game.World.Property.Category.Business.DrivingSchool.Routes;
using SunCity.Game.World.Property.Context;

namespace SunCity.Game.World.Property.Category.Business.DrivingSchool
{
    [PropertyFactory(WorldPropertyCategory.DrivingSchool)]
    [PropertyFactory(WorldPropertyCategory.AirSchool)]
    [PropertyFactory(WorldPropertyCategory.WaterSchool)]
    public class DrivingSchoolWorldProperty : BusinessWorldProperty<DrivingSchoolPropertyEntity>
    {
        protected sealed override Func<BusinessPropertyEntity, DrivingSchoolPropertyEntity> SubCategoryEntitySelector => InEntity => InEntity.DrivingSchoolPropertyEntity;

        public override BlipIconData BlipPurchased => new BlipIconData(MarkerType.UpsideDownCone, 545, 1, PropertyCategoryName);
        public override BlipIconData BlipForSale => new BlipIconData(MarkerType.UpsideDownCone, 545, 2, PropertyCategoryName);

        public List<DrivingSchoolLicense> Licenses { get; private set; }
        public List<DrivingSchoolRoute> Routes { get; set; }

        protected override WorldProperty Initialize(PropertyInitializeContext<DrivingSchoolPropertyEntity> InEntity)
        {
            Licenses = InEntity.Entity.Licenses.Select(q => new DrivingSchoolLicense(this, q)).ToList();
            Routes = InEntity.Entity.Routes.Select(q => new DrivingSchoolRoute(this, q)).ToList();

            return this;
        }

        protected override PropertyInteriorInitializeForm InitializeInterior()
        {
            return new PropertyInteriorInitializeForm
            {
                Marker = MarkerType.CheckeredFlagCircle,
                TextLabel = $"Выйти из {PropertyCategoryName}"
            };
        }
    }
}