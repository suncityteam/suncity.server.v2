﻿using System;
using System.Collections.Generic;
using System.Linq;
using SunCity.Domain.Interfaces.Entity;
using SunCity.Game.World.DataBase.Entitites.Property.Business.DrivingSchool.Routes;

namespace SunCity.Game.World.Property.Category.Business.DrivingSchool.Routes
{
    public class DrivingSchoolRoute
        : IEntityWithId<DrivingSchoolRouteId>
        , IEntityWithCreatedDate
    {
        public DrivingSchoolWorldProperty DrivingSchool { get; }

        public DrivingSchoolRouteId EntityId { get; }
        public DateTime CreatedDate { get; }
        public string Name { get; }

        public IReadOnlyList<DrivingSchoolRoutePoint> Points { get; }

        public DrivingSchoolRoute(DrivingSchoolWorldProperty drivingSchool, DrivingSchoolRouteEntity entity)
        {
            DrivingSchool = drivingSchool;

            EntityId = entity.EntityId;
            Name = entity.Name;
            CreatedDate = entity.CreatedDate;

            Points = entity.Points.Select(q => new DrivingSchoolRoutePoint(this, q)).ToArray();
        }
    }
}