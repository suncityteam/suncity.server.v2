﻿
using SunCity.Domain.Interfaces.Entity;
using SunCity.Game.Interfaces.Entity;
using SunCity.Game.World.DataBase.Entitites.Property.Business.DrivingSchool.Routes;
using GTANetworkAPI;

namespace SunCity.Game.World.Property.Category.Business.DrivingSchool.Routes
{
    public class DrivingSchoolRoutePoint
        : IEntityWithId<DrivingSchoolRoutePointId>
        , IEntityWithPosition<Vector3>
    {
        public DrivingSchoolRoute Route { get; }

        public DrivingSchoolRoutePointId EntityId { get; }
        public Vector3 Position { get; }

        public DrivingSchoolRoutePoint PrevPoint { get; }
        public DrivingSchoolRoutePoint NextPoint { get; }

        public DrivingSchoolRoutePoint(DrivingSchoolRoute route, DrivingSchoolRoutePointEntity entity)
        {
            Route = route;
            EntityId = entity.EntityId;
            Position = entity.Position;
        }
    }
}