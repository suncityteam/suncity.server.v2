﻿using System;
using SunCity.Domain.Interfaces.Entity;
using SunCity.Game.Types.Cost;
using SunCity.Game.World.DataBase.Entitites.Property.Business.DrivingSchool;

namespace SunCity.Game.World.Property.Category.Business.DrivingSchool
{
    public class DrivingSchoolLicense
        : IEntityWithId<DrivingSchoolLicenseId>
        , IEntityWithCreatedDate
    {
        public DrivingSchoolWorldProperty DrivingSchool { get; }
        public DrivingSchoolLicenseId EntityId { get; }
        public DateTime CreatedDate { get; }
        public GameCost Cost { get; }
        
        public DrivingSchoolLicense(DrivingSchoolWorldProperty drivingSchool, DrivingSchoolLicenseEntity entity)
        {
            DrivingSchool = drivingSchool;
            
            EntityId = entity.EntityId;
            Cost = entity.Cost;
            CreatedDate = entity.CreatedDate;
        }
    }
}