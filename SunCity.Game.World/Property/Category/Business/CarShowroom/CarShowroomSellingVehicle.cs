﻿using SunCity.Domain.Interfaces.Entity;
using SunCity.Game.Enums;
using SunCity.Game.Types.Cost;
using SunCity.Game.World.DataBase.Entitites.Property.Business.CarShowroom;
using SunCity.Game.World.Property.Context;
using SunCity.Game.World.VehicleModels;

namespace SunCity.Game.World.Property.Category.Business.CarShowroom
{
    public class CarShowroomSellingVehicle : IEntityWithId<CarShowroomVehicleId>
    {
        public CarShowroomVehicleId EntityId { get; }
        public VehicleModel Model { get; }

        //==============================
        public GameCost Cost { get; set; }
        public int Amount { get; set; }

        public CarShowroomSellingVehicle(PropertyInitializeContext<CarShowroomVehicleEntity> InEntity)
        {
            Model = InEntity.GetService<IVehicleModelStorage>()[InEntity.Entity.ModelId];
            EntityId = InEntity.Entity.EntityId;
            Amount = InEntity.Entity.Amount;
            Cost = InEntity.Entity.Cost;
            Cost = new GameCost(1000, GameCurrency.Money);
        }
    }
}