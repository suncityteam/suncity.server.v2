﻿
using GTANetworkAPI;
using SunCity.Domain.Interfaces.Entity;
using SunCity.Game.World.DataBase.Entitites.Property.Business.CarShowroom;

namespace SunCity.Game.World.Property.Category.Business.CarShowroom
{
    public class CarShowroomSpawnPoint : IEntityWithId<CarShowroomVehicleSpawnId>
    {
        public CarShowroomVehicleSpawnId EntityId { get; }
        public Vector3 Position { get; }
        public Vector3 Rotation { get; }

        public CarShowroomSpawnPoint(CarShowroomVehicleSpawnPointEntity InEntity)
        {
            EntityId = InEntity.EntityId;
            Position = InEntity.Position;
            Rotation = InEntity.Rotation;
        }
    }
}