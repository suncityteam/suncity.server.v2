﻿using System;
using System.Collections.Generic;
using System.Linq;
using GTANetworkAPI;
using SunCity.Game.Enums.World.Property;
using SunCity.Game.World.DataBase.Entitites.Property.Business;
using SunCity.Game.World.DataBase.Entitites.Property.Business.CarShowroom;
using SunCity.Game.World.Forms.Property.Interior;
using SunCity.Game.World.Property.Context;

namespace SunCity.Game.World.Property.Category.Business.CarShowroom
{
    [PropertyFactory(WorldPropertyCategory.CarShowroom)]
    [PropertyFactory(WorldPropertyCategory.MotoShowroom)]
    [PropertyFactory(WorldPropertyCategory.BoatShowroom)]
    [PropertyFactory(WorldPropertyCategory.AirShowroom)]
    public class CarShowroomWorldProperty : BusinessWorldProperty<CarShowroomPropertyEntity>
    {
        public List<CarShowroomSpawnPoint> SpawnPoints { get; private set; }
        public List<CarShowroomSellingVehicle> SellingVehicles { get; private set; }

        protected sealed override Func<BusinessPropertyEntity, CarShowroomPropertyEntity> SubCategoryEntitySelector => InEntity => InEntity.CarShowroomPropertyEntity;

        public override BlipIconData BlipPurchased => GetBlipFromCategory(1);
        public override BlipIconData BlipForSale => GetBlipFromCategory(2);

        protected override WorldProperty Initialize(PropertyInitializeContext<CarShowroomPropertyEntity> InEntity)
        {
            SellingVehicles = InEntity.Entity.Vehicles.Select(q => new CarShowroomSellingVehicle(new PropertyInitializeContext<CarShowroomVehicleEntity>(InEntity.ServiceProvider, q))).ToList();
            SpawnPoints = InEntity.Entity.SpawnPoints.Select(q => new CarShowroomSpawnPoint(q)).ToList();
            return this;
        }

        public CarShowroomSpawnPoint AddSpawnPoint(CarShowroomVehicleSpawnPointEntity InPointEntity)
        {
            var point = new CarShowroomSpawnPoint(InPointEntity);
            SpawnPoints.Add(point);
            return point;
        }

        protected override PropertyInteriorInitializeForm InitializeInterior()
        {
            return new PropertyInteriorInitializeForm
            {
                Marker = MarkerType.CheckeredFlagCircle,
                TextLabel = $"Выйти из {PropertyCategoryName}"
            };
        }

        private BlipIconData GetBlipFromCategory(byte InColor)
        {
            if (PropertyCategory == WorldPropertyCategory.MotoShowroom)
            {
                return new BlipIconData(MarkerType.UpsideDownCone, 226, InColor, PropertyCategoryName);
            }

            if (PropertyCategory == WorldPropertyCategory.BoatShowroom)
            {
                return new BlipIconData(MarkerType.UpsideDownCone, 455, InColor, PropertyCategoryName);
            }

            if (PropertyCategory == WorldPropertyCategory.AirShowroom)
            {
                return new BlipIconData(MarkerType.UpsideDownCone, 251, InColor, PropertyCategoryName);
            }

            //=============================================
            //PropertyCategory == WorldPropertyCategory.CarShowroom
            return new BlipIconData(MarkerType.UpsideDownCone, 225, InColor, PropertyCategoryName);
        }
    }
}
