﻿using System;
using System.Threading.Tasks;
using GTANetworkAPI;
using SunCity.Game.Enums.World.Property;
using SunCity.Game.World.DataBase.Entitites.Property.Business;
using SunCity.Game.World.DataBase.Entitites.Property.Business.Hospital;
using SunCity.Game.World.Forms.Property.Interior;
using SunCity.Game.World.Player;
using SunCity.Game.World.Property.Context;

namespace SunCity.Game.World.Property.Category.Business.Hospital
{
    [PropertyFactory(WorldPropertyCategory.Hospital)]
    public class HospitalWorldProperty : BusinessWorldProperty<HospitalPropertyEntity>
    {
        protected sealed override Func<BusinessPropertyEntity, HospitalPropertyEntity> SubCategoryEntitySelector => InEntity => InEntity.HospitalPropertyEntity;

        protected override WorldProperty Initialize(PropertyInitializeContext<HospitalPropertyEntity> InContext)
        {
            return this;
        }

        public override BlipIconData BlipPurchased => new BlipIconData(MarkerType.UpsideDownCone, 153, 1, "Больница");
        public override BlipIconData BlipForSale => new BlipIconData(MarkerType.UpsideDownCone, 153, 1, "Больница");

        public async Task OnPlayerDeath(WorldPlayer InPlayer, uint InReason, WorldPlayer InKillerPlayer)
        {
            //====================================
            InPlayer.SendClientMessage(new[]
                {
                    $"Вы попали в больницу после ${InReason}",
                    $"Для скорейшего выздоравления следуйте инструкциям врачей",
                    $"Для начала найдите вашего лечащего врача и получите дальнейшие указания"
                }
            );

            //====================================
            InPlayer.OnEnterProperty(this);

            //====================================
            //await ShowWelcomeMessage(player);

            //====================================
            InPlayer.RagePlayer.Spawn(Interior.SpawnPosition, Interior.SpawnRotation);
            InPlayer.Character.Health.Set(1);

            //====================================
            //InPlayer.RagePlayer.SetCloth((ClothSlot)11, 12, 0, 0); // Tops
            //InPlayer.RagePlayer.SetCloth((ClothSlot)3, 12, 0, 0);  // Tops
            //InPlayer.RagePlayer.SetCloth((ClothSlot)8, 12, 0, 0);  // Tops
            //
            //InPlayer.RagePlayer.SetCloth((ClothSlot)4, 20, 0, 0); // Legs

            //====================================
        }

        protected override PropertyInteriorInitializeForm InitializeInterior()
        {
            return new PropertyInteriorInitializeForm
            {
                Marker = MarkerType.CheckeredFlagCircle,
                TextLabel = "Выйти из больницы"
            };
        }
    }
}