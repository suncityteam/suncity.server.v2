﻿
using SunCity.Domain.Interfaces.Entity;
using SunCity.Game.Interfaces.Entity;
using SunCity.Game.World.DataBase.Entitites.Property.Business.PassengerTransportation.BusStation.Routes;
using GTANetworkAPI;

namespace SunCity.Game.World.Property.Category.Business.PassengerTransportation.BusStation.Routes
{
    public class BusStationRoutePoint
        : IEntityWithId<BusStationRoutePointId>
        , IEntityWithPosition<Vector3>
    {
        public BusStationRoute Route { get; }

        public BusStationRoutePointId EntityId { get; }
        public Vector3 Position { get; }

        public BusStationRoutePoint PrevPoint { get; }
        public BusStationRoutePoint NextPoint { get; }

        public BusStationRoutePoint(BusStationRoute route, BusStationRoutePointEntity entity)
        {
            Route = route;
            EntityId = entity.EntityId;
            //Position = entity.Position;
        }
    }
}