﻿using System;
using System.Collections.Generic;
using System.Linq;
using SunCity.Domain.Interfaces.Entity;
using SunCity.Game.World.DataBase.Entitites.Property.Business.PassengerTransportation.BusStation.Routes;

namespace SunCity.Game.World.Property.Category.Business.PassengerTransportation.BusStation.Routes
{
    public class BusStationRoute
        : IEntityWithId<BusStationRouteId>
        , IEntityWithCreatedDate
    {
        public BusStationWorldProperty BusStation { get; }

        public BusStationRouteId EntityId { get; }
        public DateTime CreatedDate { get; }
        public string Name { get; }

        public IReadOnlyList<BusStationRoutePoint> Points { get; }

        public BusStationRoute(BusStationWorldProperty busStation, BusStationRouteEntity entity)
        {
            BusStation = busStation;

            EntityId = entity.EntityId;
            Name = entity.Name;
            CreatedDate = entity.CreatedDate;

            Points = entity.Points.Select(q => new BusStationRoutePoint(this, q)).ToArray();
        }
    }
}