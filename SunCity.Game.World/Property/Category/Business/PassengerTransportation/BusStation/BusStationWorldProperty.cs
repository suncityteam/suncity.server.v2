﻿using System;
using System.Collections.Generic;
using System.Linq;
using GTANetworkAPI;
using SunCity.Game.Enums.World.Property;
using SunCity.Game.World.DataBase.Entitites.Property.Business;
using SunCity.Game.World.DataBase.Entitites.Property.Business.PassengerTransportation.BusStation;
using SunCity.Game.World.Forms.Property.Interior;
using SunCity.Game.World.Property.Category.Business.PassengerTransportation.BusStation.Routes;
using SunCity.Game.World.Property.Context;

namespace SunCity.Game.World.Property.Category.Business.PassengerTransportation.BusStation
{
    [PropertyFactory(WorldPropertyCategory.BusStation)]
    public class BusStationWorldProperty: BusinessWorldProperty<BusStationPropertyEntity>
    {
        protected sealed override Func<BusinessPropertyEntity, BusStationPropertyEntity> SubCategoryEntitySelector => InEntity => InEntity.BusStationPropertyEntity;

        public override BlipIconData BlipPurchased => new BlipIconData(MarkerType.UpsideDownCone, 473, 1, PropertyCategoryName);
        public override BlipIconData BlipForSale => new BlipIconData(MarkerType.UpsideDownCone, 474, 2, PropertyCategoryName);

        public List<BusStationRoute> Routes { get; set; }

        protected override WorldProperty Initialize(PropertyInitializeContext<BusStationPropertyEntity> InEntity)
        {
            Routes = InEntity.Entity.Routes.Select(q => new BusStationRoute(this, q)).ToList();

            return this;
        }

        protected override PropertyInteriorInitializeForm InitializeInterior()
        {
            return new PropertyInteriorInitializeForm
            {
                Marker = MarkerType.CheckeredFlagCircle,
                TextLabel = $"Выйти из {PropertyCategoryName}"
            };
        }
    }
}