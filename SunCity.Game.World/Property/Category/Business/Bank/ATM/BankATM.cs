﻿using System;
using GTANetworkAPI;
using SunCity.Common.RageMp.Pools;
using SunCity.Common.RageMp.Pools.Blips;
using SunCity.Common.RageMp.Pools.TextLabels;
using SunCity.Domain.Interfaces.Entity;
using SunCity.Game.World.DataBase.Entitites.Property.Business.Bank.ATM;

namespace SunCity.Game.World.Property.Category.Business.Bank.ATM
{
    public class BankATM        
        : IEntityWithId<BankATMId>
        , IEntityWithCreatedDate
    {
        public BankATMId EntityId { get; }
        public DateTime CreatedDate { get; }
        
        public Vector3 Position { get;  }
        public Vector3 Rotation { get;  }

        public BankWorldProperty Property { get; }
        public string AdminComment { get; }
        
        public IRageBlip MapIcon { get; private set; }
        public IRageTextLabel TextLabel { get; private set; }
        
        public BankATM(BankWorldProperty property, BankATMEntity entity)
        {
            Property = property;
            
            EntityId = entity.EntityId;
            CreatedDate = entity.CreatedDate;
            AdminComment = entity.AdminComment;

            Position = entity.Position;
            Rotation = entity.Rotation;
        }

        public BankATM Initialize(IRageMpPool pool)
        {
            var textLabel = $"АТМ\nБанк: {Property.Name}";
            MapIcon = pool.Blips.New(277, Position, 1, 0, "АТМ");
            TextLabel = pool.TextLabels.New(Position, textLabel, 0, Colors.White);

            return this;
        }

    }
}