﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GTANetworkAPI;
using JetBrains.Annotations;
using SunCity.Game.Enums.World.Interior;
using SunCity.Game.Enums.World.Property;
using SunCity.Game.World.DataBase.Entitites.Property.Business;
using SunCity.Game.World.DataBase.Entitites.Property.Business.Bank;
using SunCity.Game.World.Events.Client.Property.Business.Bank;
using SunCity.Game.World.Forms.Property.Interior;
using SunCity.Game.World.Models.Property.Business.Bank;
using SunCity.Game.World.Player;
using SunCity.Game.World.Property.Category.Business.Bank.ATM;
using SunCity.Game.World.Property.Context;
using SunCity.Game.World.Property.Interior;

namespace SunCity.Game.World.Property.Category.Business.Bank
{
    [PropertyFactory(WorldPropertyCategory.Bank)]
    public class BankWorldProperty : BusinessWorldProperty<BankPropertyEntity>
    {
        protected sealed override Func<BusinessPropertyEntity, BankPropertyEntity> SubCategoryEntitySelector => InEntity => InEntity.BankPropertyEntity;

        public List<BankCardTariffModel> BankCardTariff { get; private set; }
        public List<BankCardTariffModel> BankDepositTariff { get; private set; }
        public List<BankATM> ATM { get; private set; }

        public override BlipIconData BlipPurchased { get; } = new BlipIconData(MarkerType.UpsideDownCone, 617, 1, "Банк");
        public override BlipIconData BlipForSale { get; } = new BlipIconData(MarkerType.UpsideDownCone, 617, 2, "Банк");


        protected override WorldProperty Initialize(PropertyInitializeContext<BankPropertyEntity> InContext)
        {
            ATM = InContext.Entity.AtmEntities.Where(q => q.Deleted == false).Select(q => new BankATM(this, q)).ToList();
            foreach (var atm in ATM)
            {
                atm.Initialize(InContext.Pool);
            }
            
            var tariffList = InContext.Entity.CardTariffEntities.Where(q => q.Deleted == false).ToList();
            BankCardTariff = InContext.Mapper.Map<List<BankCardTariffModel>>(tariffList);

            return this;
        }

        protected override PropertyInteriorInitializeForm InitializeInterior()
        {
            return new PropertyInteriorInitializeForm
            {
                Marker    = MarkerType.CheckeredFlagCircle,
                TextLabel = "Выйти из банка"
            };
        }

        [CanBeNull]
        public BankCardTariffModel GetBankCardTariff(BankCardTariffId InBankCardTariffId)
        {
            return BankCardTariff.FirstOrDefault(q => q.EntityId == InBankCardTariffId);
        }

        public override async Task OnAction(WorldPlayer InWorldPlayer, WorldPropertyInteriorAction InAction)
        {
            await base.OnAction(InWorldPlayer, InAction);
            if (InAction.ActionType == InteriorActionType.Reception)
            {
                InWorldPlayer.RagePlayer.ShowBankClientMenu(EntityId);
            }
        }

    }
}
