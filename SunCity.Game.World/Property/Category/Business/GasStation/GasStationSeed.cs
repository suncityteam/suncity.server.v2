﻿using System;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using SunCity.Game.Enums.World.Property;
using SunCity.Game.World.DataBase;
using SunCity.Game.World.DataBase.Entitites.Property.Business;
using SunCity.Game.World.DataBase.Entitites.Property.Business.GasStation;
using SunCity.Game.World.Property.Context;

namespace SunCity.Game.World.Property.Category.Business.GasStation
{
    public class GasStationSeed : BaseBusinessPropertySeed<GasStationPropertyEntity>
    {        
        protected override int Prefix { get; } = (int) WorldPropertyCategory.GasStation;
        protected override Action<BusinessPropertyEntity, GasStationPropertyEntity> BusinessPropertyFiller => (business, entity) => business.GasStationPropertyEntity = entity;

        public GasStationSeed(ILogger<GasStationSeed> logger) : base(logger)
        {
        }
        
        public override async Task Initialize(GameWorldDbContext InDbContext)
        {
            await AddIfNotExist(InDbContext, CreatePropertyEntity(1, "АЗС", FillGasStation, entity => CustomizeProperty(entity, 1, 1, 1)));
            await InDbContext.SaveChangesAsync();
        }

        private void FillGasStation(GasStationPropertyEntity entity)
        {
        }
    }
}