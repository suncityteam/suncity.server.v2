﻿using System;
using System.Collections.Generic;
using System.Linq;
using GTANetworkAPI;
using SunCity.Common.RageMp.Pools;
using SunCity.Game.Enums.World.Property;
using SunCity.Game.World.DataBase.Entitites.Property.Business;
using SunCity.Game.World.DataBase.Entitites.Property.Business.GasStation;
using SunCity.Game.World.Forms.Property.Interior;
using SunCity.Game.World.Property.Context;

namespace SunCity.Game.World.Property.Category.Business.GasStation
{
    [PropertyFactory(WorldPropertyCategory.GasStation)]
    public class GasStationWorldProperty : BusinessWorldProperty<GasStationPropertyEntity>
    {
        protected sealed override Func<BusinessPropertyEntity, GasStationPropertyEntity> SubCategoryEntitySelector => InEntity => InEntity.GasStationPropertyEntity;

        public override BlipIconData BlipPurchased { get; } = new BlipIconData(MarkerType.UpsideDownCone, 361, 1, "АЗС");
        public override BlipIconData BlipForSale { get; } = new BlipIconData(MarkerType.UpsideDownCone, 361, 2, "АЗС");

        internal List<GasStationPoint> Points { get; set; }

        protected override WorldProperty Initialize(PropertyInitializeContext<GasStationPropertyEntity> InEntity)
        {
            Points = InEntity.Entity.Points.Select(q => new GasStationPoint(this, q)).ToList();
            foreach (var point in Points)
            {
                point.Initialize(InEntity.Pool);
            }

            return this;
        }

        public GasStationPoint AddPoint(GasStationPointEntity InPointEntity, IRageMpPool pool)
        {
            var point = new GasStationPoint(this, InPointEntity);
            point.Initialize(pool);
            Points.Add(point);
            return point;
        }

        protected override PropertyInteriorInitializeForm InitializeInterior()
        {
            return new PropertyInteriorInitializeForm
            {
                Marker    = MarkerType.CheckeredFlagCircle,
                TextLabel = "Выйти из заправки"
            };
        }
    }
}
