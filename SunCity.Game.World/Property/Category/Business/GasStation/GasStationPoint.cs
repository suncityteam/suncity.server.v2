﻿using GTANetworkAPI;
using SunCity.Common.RageMp.Pools;
using SunCity.Common.RageMp.Pools.Blips;
using SunCity.Common.RageMp.Pools.Markers;
using SunCity.Common.RageMp.Pools.TextLabels;
using SunCity.Domain.Interfaces.Entity;
using SunCity.Game.World.DataBase.Entitites.Property.Business.GasStation;

namespace SunCity.Game.World.Property.Category.Business.GasStation
{
    public class GasStationPoint : IEntityWithId<GasStationPointId>
    {
        public GasStationPointId EntityId { get; }
        public Vector3 Position { get; }

        private IRageTextLabel TextLabel { get; set; }
        private IRageMarker Marker { get; set; }

        public GasStationWorldProperty Property { get; }

        public GasStationPoint(GasStationWorldProperty InProperty, GasStationPointEntity InPointEntity)
        {
            Property = InProperty;
            EntityId = InPointEntity.EntityId;
            Position = InPointEntity.Position;
        }

        public void Initialize(IRageMpPool pool)
        {
            TextLabel = pool.TextLabels.New(Position, "Для заправки нажмите ALT", 1, Colors.White);
            Marker = pool.Markers.New(GTANetworkAPI.MarkerType.VerticalCylinder, Position, Vector3Consts.Zero, Vector3Consts.Zero, 2, Colors.DodgerBlue, true);
        }
    }
}