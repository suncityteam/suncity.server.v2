﻿using System;
using System.Collections.Generic;
using System.Linq;
using GTANetworkAPI;
using SunCity.Game.Enums.World.Property;
using SunCity.Game.World.DataBase.Entitites.Property.Business;
using SunCity.Game.World.DataBase.Entitites.Property.Business.StreetRacing;
using SunCity.Game.World.Forms.Property.Interior;
using SunCity.Game.World.Property.Category.Business.PassengerTransportation.StreetRacing.Routes;
using SunCity.Game.World.Property.Context;

namespace SunCity.Game.World.Property.Category.Business.StreetRacing
{
    [PropertyFactory(WorldPropertyCategory.StreetRacing)]
    [PropertyFactory(WorldPropertyCategory.AirRacing)]
    [PropertyFactory(WorldPropertyCategory.BicycleRacing)]
    [PropertyFactory(WorldPropertyCategory.BoatRacing)]
    [PropertyFactory(WorldPropertyCategory.MotorcycleRacing)]
    [PropertyFactory(WorldPropertyCategory.OffRoadRacing)]
    public class StreetRacingWorldProperty : BusinessWorldProperty<StreetRacingPropertyEntity>
    {
        protected sealed override Func<BusinessPropertyEntity, StreetRacingPropertyEntity> SubCategoryEntitySelector => InEntity => InEntity.StreetRacingPropertyEntity;

        public override BlipIconData BlipPurchased => new BlipIconData(MarkerType.UpsideDownCone, 473, 1, PropertyCategoryName);
        public override BlipIconData BlipForSale => new BlipIconData(MarkerType.UpsideDownCone, 474, 2, PropertyCategoryName);

        public List<StreetRacingRoute> Routes { get; set; }
        public StreetRacingHeatManager HeatManager { get; }

        public StreetRacingWorldProperty()
        {
            HeatManager = new StreetRacingHeatManager(this);
        }


        protected override WorldProperty Initialize(PropertyInitializeContext<StreetRacingPropertyEntity> InEntity)
        {
            Routes = InEntity.Entity.Routes.Select(q => new StreetRacingRoute(this, q)).ToList();

            foreach (var route in Routes)
            {
                route.Initialize(InEntity.Pool);
            }

            return this;
        }

        protected override PropertyInteriorInitializeForm InitializeInterior()
        {
            return new PropertyInteriorInitializeForm
            {
                Marker = MarkerType.CheckeredFlagCircle,
                TextLabel = $"Выйти из {PropertyCategoryName}"
            };
        }
    }
}