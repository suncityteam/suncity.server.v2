﻿using System;
using System.Collections.Generic;
using System.Linq;
using SunCity.Common.RageMp.Pools;
using SunCity.Domain.Interfaces.Entity;
using SunCity.Game.Types.Cost;
using SunCity.Game.World.DataBase.Entitites.Property.Business.StreetRacing.Routes;
using SunCity.Game.World.Property.Category.Business.StreetRacing;

namespace SunCity.Game.World.Property.Category.Business.PassengerTransportation.StreetRacing.Routes
{
    public class StreetRacingRoute
        : IEntityWithId<StreetRacingRouteId>
        , IEntityWithCreatedDate
    {
        public StreetRacingWorldProperty StreetRacing { get; }

        public StreetRacingRouteId EntityId { get; }
        public DateTime CreatedDate { get; }
        public string Name { get; }

        //==================================
        public GameCost Cost { get; set; }
        public StreetRacingReward Reward { get; set; }

        
        public IReadOnlyList<StreetRacingRoutePoint> Points { get; }

        public StreetRacingRoute(StreetRacingWorldProperty streetRacing, StreetRacingRouteEntity entity)
        {
            StreetRacing = streetRacing;

            EntityId = entity.EntityId;
            Name = entity.Name;
            CreatedDate = entity.CreatedDate;

            Cost = entity.Cost;
            Reward = entity.Reward;

            
            Points = entity.Points.Select(q => new StreetRacingRoutePoint(this, q)).ToArray();
        }

        public StreetRacingRoute Initialize(IRageMpPool pool)
        {
            foreach (var point in Points)
            {
                point.Initialize(pool);
            }

            return this;
        }
        
    }
}