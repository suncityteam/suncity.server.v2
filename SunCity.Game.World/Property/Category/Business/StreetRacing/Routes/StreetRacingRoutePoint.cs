﻿using SunCity.Domain.Interfaces.Entity;
using SunCity.Game.Interfaces.Entity;
using SunCity.Game.World.DataBase.Entitites.Property.Business.StreetRacing.Routes;
using GTANetworkAPI;
using SunCity.Common.RageMp.Pools;

namespace SunCity.Game.World.Property.Category.Business.PassengerTransportation.StreetRacing.Routes
{
    public class StreetRacingRoutePoint
        : IEntityWithId<StreetRacingRoutePointId>
        , IEntityWithPosition<Vector3>
    {
        public StreetRacingRoute Route { get; }

        public StreetRacingRoutePointId EntityId { get; }
        public Vector3 Position { get; }

        public StreetRacingRoutePoint PrevPoint { get; }
        public StreetRacingRoutePoint NextPoint { get; }

        public Checkpoint Checkpoint { get; private set; }
        
        public StreetRacingRoutePoint(StreetRacingRoute route, StreetRacingRoutePointEntity entity)
        {
            Route = route;
            EntityId = entity.EntityId;
            Position = entity.Position;
        }

        public StreetRacingRoutePoint Initialize(IRageMpPool pool)
        {
            //Checkpoint = pool.Checkpoints.New(1, Position, Position, 5.0f, Colorss.White, false);
            
            return this;
        }
        
    }
}