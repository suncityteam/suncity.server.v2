﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using SunCity.Game.World.DataBase.Entitites.Property.Business.StreetRacing.Heats;

namespace SunCity.Game.World.Property.Category.Business.StreetRacing
{
    public class StreetRacingManager
    {
        private IWorldPropertyStorage PropertyStorage { get; }
        private IReadOnlyList<StreetRacingWorldProperty> Properties => PropertyStorage.ValuesCopy.Cast<StreetRacingWorldProperty>().ToArray();

        public StreetRacingManager(IWorldPropertyStorage propertyStorage)
        {
            PropertyStorage = propertyStorage;
            Task.Factory.StartNew(Tick);
        }
        
        private async Task Tick()
        {
            return;
            while (true)
            {
                await TickProcess();
                await Task.Delay(5000);
            }
        }
        
        private async Task TickProcess()
        {
            foreach (var property in Properties)
            {
                var activeHeats = property.HeatManager.GetHeats();
                foreach (var heat in activeHeats)
                {
                    if (heat.Status == StreetRacingHeatStatus.Created && DateTime.UtcNow > heat.StartedDate)
                    {
                        await heat.Cancel();
                    }
                }
            }
        }
    }
}