﻿using SunCity.Domain.Interfaces.Entity;
using SunCity.Game.World.Player;

namespace SunCity.Game.World.Property.Category.Business.StreetRacing.Heats
{
    public class StreetRacingHeatMember: IEntityWithId<StreetRacingHeatMemberId>
    {
        public StreetRacingHeat Heat { get; }
        public StreetRacingHeatMemberId EntityId { get; }
        public WorldPlayer Player { get; }
        
        public StreetRacingHeatMember(WorldPlayer player, StreetRacingHeat heat)
        {
            Player = player;
            Heat = heat;
            EntityId = StreetRacingHeatMemberId.GenerateId();
        }
    }
}