﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SunCity.Common.Extensions;
using SunCity.Common.RageMp.Pools.Players;
using SunCity.Domain.Interfaces.Entity;
using SunCity.Game.World.DataBase.Entitites.Property.Business.StreetRacing.Heats;
using SunCity.Game.World.Extensions.Player;
using SunCity.Game.World.Player;
using SunCity.Game.World.Property.Category.Business.PassengerTransportation.StreetRacing.Routes;

namespace SunCity.Game.World.Property.Category.Business.StreetRacing.Heats
{
    public class StreetRacingHeat
        : IEntityWithId<StreetRacingHeatId>
        , IEntityWithCreatedDate
    {
        public StreetRacingHeat(StreetRacingRoute route)
        {
            Route = route;
            Status = StreetRacingHeatStatus.Created;
            EntityId = StreetRacingHeatId.GenerateId();
            CreatedDate = DateTime.UtcNow;
            Members = new List<StreetRacingHeatMember>(16);
        }

        public StreetRacingHeatMember WithMember(WorldPlayer player)
        {
            var member = new StreetRacingHeatMember(player, this);
            Members.Add(member);
            return member;
        }

        public async Task Alarm()
        {
            var players = GetRagePlayers();
            var raceResult = $"Гонка {Route.Name} начнется через {StartedDate - DateTime.UtcNow}";
            
            foreach (var player in players)
            {
                //await player.SendClientMessage(raceResult);
            }
        }
        
        public async Task Start()
        {
            var players = GetRagePlayers();
            Status = StreetRacingHeatStatus.Started;

            foreach (var point in Route.Points)
            {
                // point.Checkpoint.ShowFor(players);
            }
            
            var sb = new StringBuilder(512);
            sb.AppendLine($"Гонка {Route.Name} была начата");
            sb.AppendLine($"Призовой фонд: ");

            for (var i = 1; i < 3; i++)
            {
                sb.AppendLine($"{i} место награда {Route.Reward[i]}");
            }
            
            var raceResult = sb.ToString();
            foreach (var player in players)
            {
                //player.SendClientMessage(raceResult);
            }
        }

        public async Task Cancel()
        {
            var players = GetRagePlayers();
            Status = StreetRacingHeatStatus.Canceled;

            foreach (var point in Route.Points)
            {
                // await point.Checkpoint.HideForAsync(players);
            }

            var raceResult = $"Гонка {Route.Name} была отменена";
            foreach (var player in players)
            {
                //player.SendClientMessage(raceResult);
            }
        }

        public async Task Finish()
        {
            var players = GetRagePlayers();
            Status = StreetRacingHeatStatus.Completed;

            foreach (var point in Route.Points)
            {
               // await point.Checkpoint.HideForAsync(players);
            }

            var sb = new StringBuilder(512);
            sb.AppendLine($"Гонка {Route.Name} была завершена");
            sb.AppendLine($"Итоги гонки: ");

            foreach (var (winner, index) in Winners.WithIndex())
            {
                sb.AppendLine($"{index}. {winner.Player.Name}, награда {Route.Reward[index]}");
            }

            var raceResult = sb.ToString();
            foreach (var player in players)
            {
                player.SendChatMessage(raceResult);
            }
        }

        private IRagePlayer[] GetRagePlayers()
        {
            return Members.Select(q => q.Player.RagePlayer).Where(q => q.Exists).ToArray();
        }

        public StreetRacingHeatMember[] Winners { get; }
        public StreetRacingHeatStatus Status { get; private set; }
        public StreetRacingRoute Route { get; }
        public StreetRacingHeatId EntityId { get; }
        public List<StreetRacingHeatMember> Members { get; }
        public DateTime CreatedDate { get; }
        public DateTime StartedDate { get; }
    }
}