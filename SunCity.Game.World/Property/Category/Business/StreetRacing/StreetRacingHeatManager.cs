﻿using System.Collections.Generic;
using System.Threading;
using SunCity.Game.World.Property.Category.Business.PassengerTransportation.StreetRacing.Routes;
using SunCity.Game.World.Property.Category.Business.StreetRacing.Heats;

namespace SunCity.Game.World.Property.Category.Business.StreetRacing
{
    public class StreetRacingHeatManager
    {
        private ReaderWriterLockSlim Lock { get; }
        public StreetRacingWorldProperty Property { get; }
        private List<StreetRacingHeat> Heats { get; }

        public StreetRacingHeatManager(StreetRacingWorldProperty property)
        {
            Property = property;
            Lock = new ReaderWriterLockSlim();
            Heats = new List<StreetRacingHeat>(32);
        }

        public IReadOnlyList<StreetRacingHeat> GetHeats()
        {
            Lock.EnterReadLock();
            var copy = Heats.ToArray();
            Lock.ExitReadLock();
            return copy;
        }

        public StreetRacingHeat CreateHeat(StreetRacingRoute route)
        {
            var heat = new StreetRacingHeat(route);
            Lock.EnterWriteLock();
            Heats.Add(heat);
            Lock.ExitWriteLock();
            return heat;
        }
    }
}