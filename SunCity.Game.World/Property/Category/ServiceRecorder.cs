﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using SunCity.Game.World.Interior;

namespace SunCity.Game.World.Property.Category
{
    public static class ServiceRecorder
    {
        public static IServiceCollection AddGameWorldPropertySeed(this IServiceCollection InServiceCollection, IConfiguration InConfiguration)
        {
            InServiceCollection.AddSingleton<WorldInteriorSeed>();

            InServiceCollection.AddSingleton<WorldPropertySeed>();

            //InServiceCollection.AddSingleton<IPropertySeed, DrivingSchoolSeed>();
            //InServiceCollection.AddSingleton<IPropertySeed, GasStationSeed>();
            //InServiceCollection.AddSingleton<IPropertySeed, ResidenceSeed>();
            
            return InServiceCollection;
        }
    }
}