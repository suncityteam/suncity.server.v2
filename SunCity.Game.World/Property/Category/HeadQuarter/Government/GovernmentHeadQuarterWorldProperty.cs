﻿using SunCity.Game.World.Fraction;

namespace SunCity.Game.World.Property.Category.HeadQuarter.Government
{
    public class GovernmentHeadQuarterWorldProperty : HeadQuarterWorldProperty
    {
        public GovernmentHeadQuarterWorldProperty(WorldFraction InFraction) : base(InFraction)
        {
        }
    }
}