﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GTANetworkAPI;
using SunCity.Game.World.DataBase.Entitites.Property;
using SunCity.Game.World.DataBase.Entitites.Property.HeadQuarter;
using SunCity.Game.World.Forms.Property.Interior;
using SunCity.Game.World.Fraction;
using SunCity.Game.World.Property.Context;

namespace SunCity.Game.World.Property.Category.HeadQuarter
{
    //[PropertyFactory(WorldPropertyCategory.HeadQuarter)]
    public class HeadQuarterWorldProperty : WorldProperty<HeadQuarterPropertyEntity>
    {
        public WorldFraction Fraction { get; }

        private protected sealed override Func<PropertyEntity, HeadQuarterPropertyEntity> CategoryEntitySelector => InEntity => InEntity.HeadQuarterPropertyEntity;

        public override BlipIconData BlipPurchased => new BlipIconData(MarkerType/*.Dollar*/.ThickChevronUp, (uint)Fraction.MapIcon, 1, Fraction.Name);
        public override BlipIconData BlipForSale => new BlipIconData(MarkerType/*.Dollar*/.ThickChevronUp, Fraction == null ? 374u : (uint)Fraction?.MapIcon, 2, Fraction?.Name ?? "HQ");
        
        public HeadQuarterWorldProperty()
        {
            Fraction = null;
        }

        protected HeadQuarterWorldProperty(WorldFraction InFraction)
        {
            Fraction = InFraction;
        }

        protected override PropertyInteriorInitializeForm InitializeInterior() => new PropertyInteriorInitializeForm
        {
            Marker    = MarkerType.CheckeredFlagCircle,
            TextLabel = "Выйти из здания"
        };

        protected override WorldProperty Initialize(PropertyInitializeContext<HeadQuarterPropertyEntity> InEntity)
        {
            return this;
        }

        protected override string GetPickupTextLabelMessage()
        {
            var sb = new StringBuilder(256);

            //----------------------
            if (IsSelling)
            {
                sb.AppendLine($"{PropertyCategoryName} продается");
                sb.AppendLine($"~w~Стоимость:\n{SellingCost}");
            }
            else
            {
                sb.AppendLine(Fraction.Name);
                sb.AppendLine($"Для входа нажмите ~y~ALT");
            }

            //----------------------
            return sb.ToString();
        }
    }
}
