﻿using SunCity.Game.World.Fraction;

namespace SunCity.Game.World.Property.Category.HeadQuarter.Crime
{
    public class CrimeHeadQuarterWorldProperty : HeadQuarterWorldProperty
    {
        public CrimeHeadQuarterWorldProperty(WorldFraction InFraction) : base(InFraction)
        {
        }
    }
}
