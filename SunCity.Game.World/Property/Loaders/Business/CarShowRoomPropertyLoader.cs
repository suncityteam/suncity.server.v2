﻿using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using SunCity.Game.Enums.World.Property;
using SunCity.Game.World.DataBase;
using SunCity.Game.World.DataBase.Entitites.Property;

namespace SunCity.Game.World.Property.Loaders.Business
{
    internal class CarShowRoomPropertyLoader : AbstractPropertyLoader
    {
        public static WorldPropertyCategory[] Categories { get; } = new WorldPropertyCategory[]
        {
            WorldPropertyCategory.CarShowroom,
            WorldPropertyCategory.MotoShowroom,
            WorldPropertyCategory.BoatShowroom,
            WorldPropertyCategory.AirShowroom,
        };

        public CarShowRoomPropertyLoader() : base(Categories)
        {
        }

        public override Task<PropertyEntity[]> LoadProperties(GameWorldDbContext InDbContext)
        {
            
            /*var random = new Random();
            var vehicles = InDbContext.VehicleModelEntity.ToArray();
            foreach (var vehicle in vehicles)
            {
                vehicle.FuelConsumptionPerMinute = (float) random.Next(1, 20);
                vehicle.Acceleration = random.Next(2, 30);
                vehicle.Handleability = (byte) random.Next(10, 90);
                
                vehicle.MaxSpeed = (short)random.Next(100, 350);
                
                vehicle.NumberOfSeats = (byte)random.Next(1, 4);
                vehicle.BagCapacity = (short)random.Next(1000, 10000);

                var rnd  =random.Next(0, 100);
                if (rnd >= 75)
                {
                    vehicle.FuelType = VehicleFuelType.Diesel;
                }
                else if (rnd >= 70)
                {
                    vehicle.FuelType = VehicleFuelType.Electricity;
                }
                else if (rnd >= 50)
                {
                    vehicle.FuelType = VehicleFuelType.Gas;
                }
                else 
                {
                    vehicle.FuelType = VehicleFuelType.Petrol;
                }
            }

            InDbContext.SaveChanges();*/
            
            //var vehicles = InDbContext.VehicleModelEntity.ToArray();
            //var rooms = InDbContext.CarShowroomPropertyEntity.ToArray();
            //var random = new Random();
            //
            //foreach (var room in rooms)
            //{
            //    var rnd = vehicles.TakeRandom(300);
            //    foreach (var vehicle in rnd)
            //    {
            //        var isDonate = random.Next(0, 10) >= 6;
            //        room.Vehicles.Add(new CarShowroomVehicleEntity
            //        {
            //            EntityId = CarShowroomVehicleId.GenerateId(),
            //            CarShowroomId = room.EntityId,
            //            ModelId = vehicle.EntityId,
            //            Cost = new GameCost(random.Next(10000, 90000000), isDonate ? GameCurrency.Donate : GameCurrency.Money)
            //        });
            //    }
            //}
            //
            //
            //InDbContext.SaveChanges();
            //    return null;
            
            return BuildQuery(InDbContext)

                   .Include(q => q.BusinessPropertyEntity)
                   .ThenInclude(q => q.CarShowroomPropertyEntity)
                   .ThenInclude(q => q.SpawnPoints)

                   .Include(q => q.BusinessPropertyEntity)
                   .ThenInclude(q => q.CarShowroomPropertyEntity)
                   .ThenInclude(q => q.Vehicles)

                   .ToArrayAsync();
        }
    }
}