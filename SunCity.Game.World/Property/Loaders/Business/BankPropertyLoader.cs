﻿using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using SunCity.Game.Enums.World.Property;
using SunCity.Game.World.DataBase;
using SunCity.Game.World.DataBase.Entitites.Property;

namespace SunCity.Game.World.Property.Loaders.Business
{
    internal class BankPropertyLoader : AbstractPropertyLoader
    {
        public static WorldPropertyCategory[] Categories { get; } = new WorldPropertyCategory[]
        {
            WorldPropertyCategory.Bank,
        };

        public BankPropertyLoader() : base(Categories)
        {
        }

        public override Task<PropertyEntity[]> LoadProperties(GameWorldDbContext InDbContext)
        {
            return BuildQuery(InDbContext)

                   .Include(q => q.BusinessPropertyEntity)
                   .ThenInclude(q => q.BankPropertyEntity)
                   .ThenInclude(q => q.CardTariffEntities)

                   .Include(q => q.BusinessPropertyEntity)
                   .ThenInclude(q => q.BankPropertyEntity)
                   .ThenInclude(q => q.AtmEntities)
                   
                   .ToArrayAsync();
        }
    }
}