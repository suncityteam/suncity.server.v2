﻿using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using SunCity.Game.Enums.World.Property;
using SunCity.Game.World.DataBase;
using SunCity.Game.World.DataBase.Entitites.Property;

namespace SunCity.Game.World.Property.Loaders.Business
{
    internal class DrivingSchoolPropertyLoader : AbstractPropertyLoader
    {
        public static WorldPropertyCategory[] Categories { get; } = new WorldPropertyCategory[]
        {
            WorldPropertyCategory.DrivingSchool,
            WorldPropertyCategory.AirSchool,
            WorldPropertyCategory.WaterSchool,
        };

        public DrivingSchoolPropertyLoader() : base(Categories)
        {
        }

        public override Task<PropertyEntity[]> LoadProperties(GameWorldDbContext InDbContext)
        {
            return BuildQuery(InDbContext)

                .Include(q => q.BusinessPropertyEntity)
                .ThenInclude(q => q.DrivingSchoolPropertyEntity)
                .ThenInclude(q => q.Licenses)

                .Include(q => q.BusinessPropertyEntity)
                .ThenInclude(q => q.DrivingSchoolPropertyEntity)
                .ThenInclude(q => q.Routes)
                .ThenInclude(q => q.Points)
                
                .ToArrayAsync();
        }
    }
}