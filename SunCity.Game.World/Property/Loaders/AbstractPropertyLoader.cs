﻿using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using SunCity.Common.EfCore;
using SunCity.Game.Enums.World.Property;
using SunCity.Game.World.DataBase;
using SunCity.Game.World.DataBase.Entitites.Property;

namespace SunCity.Game.World.Property.Loaders
{
    internal abstract class AbstractPropertyLoader
    {
        private WorldPropertyCategory[] Categories { get; }

        protected AbstractPropertyLoader(WorldPropertyCategory[] InCategories)
        {
            Categories = InCategories;
        }

        public virtual Task<PropertyEntity> LoadProperty(GameWorldDbContext InDbContext, PropertyId InPropertyId)
        {
            return BuildQuery(InDbContext).FirstAsync(q => q.EntityId == InPropertyId);
        }

        public abstract Task<PropertyEntity[]> LoadProperties(GameWorldDbContext InDbContext);

        protected virtual IQueryable<PropertyEntity> BuildQuery(GameWorldDbContext InDbContext)
        {
            return InDbContext.PropertyEntity
                .HasTracking(hasTracking: false)
                .Include(q => q.RequiredLicenses)
                .Include(q => q.OwnerEntities).ThenInclude(q => q.PlayerOwnerEntity)
                .Include(q => q.OwnerEntities).ThenInclude(q => q.FractionOwnerEntity)
                .Where(q => Categories.Contains(q.PropertyCategory));
        }

        public override string ToString()
        {
            return $"Loader for {string.Join(", ", Categories)}";
        }
    }
}