﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using SunCity.Game.World.Property.Loaders.Business;

namespace SunCity.Game.World.Property.Loaders
{
    public static class ServiceRecorder
    {
        public static IServiceCollection AddGameWorldPropertyLoaders(this IServiceCollection InServiceCollection, IConfiguration InConfiguration)
        {
            InServiceCollection.AddSingleton<AbstractPropertyLoader, ResidencePropertyLoader>();
            InServiceCollection.AddSingleton<AbstractPropertyLoader, GasStationPropertyLoader>();
            InServiceCollection.AddSingleton<AbstractPropertyLoader, CarShowRoomPropertyLoader>();
            InServiceCollection.AddSingleton<AbstractPropertyLoader, BankPropertyLoader>();
            InServiceCollection.AddSingleton<AbstractPropertyLoader, HospitalPropertyLoader>();
            
            InServiceCollection.AddSingleton<AbstractPropertyLoader, DrivingSchoolPropertyLoader>();
            InServiceCollection.AddSingleton<AbstractPropertyLoader, StreetRacingPropertyLoader>();
            InServiceCollection.AddSingleton<AbstractPropertyLoader, BusStationPropertyLoader>();

            return InServiceCollection;
        }
    }
}