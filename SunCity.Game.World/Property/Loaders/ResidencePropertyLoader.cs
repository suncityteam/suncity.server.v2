﻿using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using SunCity.Game.Enums.World.Property;
using SunCity.Game.World.DataBase;
using SunCity.Game.World.DataBase.Entitites.Property;

namespace SunCity.Game.World.Property.Loaders
{
    internal class ResidencePropertyLoader : AbstractPropertyLoader
    {
        public static WorldPropertyCategory[] Categories { get; } = new WorldPropertyCategory[]
        {
            WorldPropertyCategory.HotelRoom,
            WorldPropertyCategory.House,
            WorldPropertyCategory.Apartment
        };

        public ResidencePropertyLoader() : base(Categories)
        {
        }

        public override Task<PropertyEntity[]> LoadProperties(GameWorldDbContext InDbContext)
        {
            return BuildQuery(InDbContext).Include(q => q.ResidencePropertyEntity).ToArrayAsync();
        }
    }
}