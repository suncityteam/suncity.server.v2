﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.DependencyInjection;
using SunCity.Common.Extensions;
using SunCity.Domain.Interfaces.Extensions;
using SunCity.Game.World.Common.Process;
using SunCity.Game.World.DataBase;
using SunCity.Game.World.DataBase.Entitites.Property;

namespace SunCity.Game.World.Property.Process
{
    internal class WorldPropertySaveProcess : GameWorldProcess
    {
        public WorldPropertySaveProcess(IServiceProvider InServiceProvider, IWorldPropertyStorage InPropertyStorage) : base(InServiceProvider)
        {
            PropertyStorage = InPropertyStorage;
        }

        private IWorldPropertyStorage PropertyStorage { get; }



        protected override async Task OnProcess()
        {
            return;

            while (true)
            {
                await Task.Delay(5000);

                using var scope = CreateScope();
                var dbContext = scope.ServiceProvider.GetService<GameWorldDbContext>();
                await SaveStorageProcess(dbContext);
            }
        }

        private async Task SaveStorageProcess(GameWorldDbContext InDbContext)
        {
            foreach (var batch in PropertyStorage.ValuesCopy.Batch(10))
            {
                var propertiesArr = batch.ToArray();
                var properties = await GetProperties(InDbContext, propertiesArr);

                foreach (var worldProperty in propertiesArr)
                {
                    if (properties.TryGetValue(worldProperty.EntityId, out var propertyEntity))
                    {
                        await worldProperty.OnSaveChanges(propertyEntity);
                    }
                }

                await InDbContext.SaveChangesAsync();
            }

            await InDbContext.SaveChangesAsync();
        }

        private Task<Dictionary<PropertyId, PropertyEntity>> GetProperties(GameWorldDbContext InDbContext, WorldProperty[] InProperties)
        {
            var propertyIds = InProperties.GetEntityIds<PropertyId, WorldProperty>();
            return InDbContext.PropertyEntity.ToContainsDictionaryAsync(propertyIds);
        }
    }
}
