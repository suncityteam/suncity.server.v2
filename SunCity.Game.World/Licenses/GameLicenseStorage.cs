﻿using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using SunCity.Common.EfCore;
using SunCity.Game.Types.Storage;
using SunCity.Game.World.DataBase;

namespace SunCity.Game.World.Licenses
{
    internal class GameLicenseStorage : WorldStorage<LicenseId, GameLicense>, IGameLicenseStorage
    {
        private ILogger<GameLicenseStorage> Logger { get; }
        private GameLicenseSeed Seed { get; }

        public GameLicenseStorage(ILogger<GameLicenseStorage> logger, GameLicenseSeed seed)
        {
            Logger = logger;
            Seed = seed;
        }

        public async Task Initialize(GameWorldDbContext InDbContext)
        {
            await Seed.Initialize(InDbContext);

            Logger.LogInformation("Begin Initialize");

            var models = await InDbContext.GameLicenseEntity
                .HasTracking(hasTracking: false)
                .ToArrayAsync();

            Logger.LogInformation($"Fetched {models.Length} licenses");

            foreach (var model in models)
            {
                Put(new GameLicense(model));
            }

            Logger.LogInformation($"Loaded {Items.Count} licenses");
            Logger.LogInformation("End Initialize");
        }
    }
}