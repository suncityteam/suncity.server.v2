﻿using System;
using SunCity.Domain.Interfaces.Entity;
using SunCity.Game.World.DataBase.Entitites.Licenses;

namespace SunCity.Game.World.Licenses
{
    public class GameLicense        
        : IEntityWithId<LicenseId>
        , IEntityWithCreatedDate
    {
        public LicenseId EntityId { get; }
        public string Name { get; }
        public DateTime CreatedDate { get; }

        public GameLicense(GameLicenseEntity entity)
        {          
            CreatedDate = entity.CreatedDate;

            EntityId = entity.EntityId;
            Name = entity.Name;
        }
    }
}