﻿using System.Threading.Tasks;
using SunCity.Game.Types.Storage;
using SunCity.Game.World.DataBase;

namespace SunCity.Game.World.Licenses
{
    public interface IGameLicenseStorage: IWorldStorage<LicenseId, GameLicense>
    {        
        Task Initialize(GameWorldDbContext InDbContext);
    }
}