﻿using System;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using SunCity.Game.Types.Storage;
using SunCity.Game.World.DataBase;
using SunCity.Game.World.DataBase.Entitites.Licenses;

namespace SunCity.Game.World.Licenses
{
    internal class GameLicenseSeed : WorldStorageSeed<GameWorldDbContext, LicenseId, GameLicenseEntity>
    {
        public const string DrivingLicenceId = "95bc30e8-69d7-48a9-b7a0-1102ba665500";
        public const string HeavyDrivingLicenceId = "95bc30e8-69d7-48a9-b7a0-1102ba665501";
        public const string MotoDrivingLicenceId = "95bc30e8-69d7-48a9-b7a0-1102ba665502";

        public const string BoatDrivingLicenceId = "95bc30e8-69d7-48a9-b7a0-1102ba665503";
        public const string HelicopterDrivingLicenceId = "95bc30e8-69d7-48a9-b7a0-1102ba665504";
        public const string AirDrivingLicenceId = "95bc30e8-69d7-48a9-b7a0-1102ba665505";

        
        protected override Func<GameWorldDbContext, DbSet<GameLicenseEntity>> Table => dbContext => dbContext.GameLicenseEntity;

        public GameLicenseSeed(ILogger<GameLicenseSeed> logger) : base(logger)
        {
        }
        
        public override async Task Initialize(GameWorldDbContext InDbContext)
        {
            //    Управление транспортом
            await AddIfNotExist(InDbContext, Create(DrivingLicenceId, "Лицензия на управление легковым автомобилем"));
            await AddIfNotExist(InDbContext, Create(HeavyDrivingLicenceId, "Лицензия на управление грузовым автомобилем"));

            await AddIfNotExist(InDbContext, Create(MotoDrivingLicenceId, "Лицензия на управление мотоциклом"));

            await AddIfNotExist(InDbContext, Create(BoatDrivingLicenceId, "Лицензия на управление лодкой"));
            await AddIfNotExist(InDbContext, Create(AirDrivingLicenceId, "Лицензия на управление самолетом"));
            await AddIfNotExist(InDbContext, Create(HelicopterDrivingLicenceId, "Лицензия на управление вертолетом"));

            //    Бизнес
            await AddIfNotExist(InDbContext, Create("95bc30e8-69d7-48a9-b7a0-2102ba665500", "Лицензия на ведение бизнесса"));

            await AddIfNotExist(InDbContext, Create("95bc30e8-69d7-48a9-b7a0-2102ba665501", "Лицензия на продажу алкоголя"));
            await AddIfNotExist(InDbContext, Create("95bc30e8-69d7-48a9-b7a0-2102ba665502", "Лицензия на продажу табака"));
            
            await AddIfNotExist(InDbContext, Create("95bc30e8-69d7-48a9-b7a0-2102ba665503", "Лицензия на продажу драгоценностей"));

            await AddIfNotExist(InDbContext, Create("95bc30e8-69d7-48a9-b7a0-2102ba665504", "Лицензия на продажу лекарств"));
            await AddIfNotExist(InDbContext, Create("95bc30e8-69d7-48a9-b7a0-2102ba665505", "Лицензия на продажу оружия"));

            await AddIfNotExist(InDbContext, Create("95bc30e8-69d7-48a9-b7a0-2102ba665506", "Лицензия на продажу недвижимости"));

            //await AddIfNotExist(InDbContext, Create("95bc30e8-69d7-48a9-b7a0-2102ba665507", "Лицензия на перевозку пассажиров"));
            //await AddIfNotExist(InDbContext, Create("95bc30e8-69d7-48a9-b7a0-2102ba665508", "Лицензия на перевозку грузов"));

            await InDbContext.SaveChangesAsync();
        }

        private GameLicenseEntity Create(string id, string name)
        {
            return new GameLicenseEntity
            {
                EntityId = new LicenseId(Guid.Parse(id)),
                Name = name,
                CreatedDate = DateTime.UtcNow
            };
        }
    }
}