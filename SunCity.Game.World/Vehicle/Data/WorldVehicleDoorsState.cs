﻿using SunCity.Game.Enums.Vehicle.Doors;
using SunCity.Game.World.Common.Components;

namespace SunCity.Game.World.Vehicle.Data
{
    public class WorldVehicleDoorsState : BaseEntityComponent<WorldVehicle>
    {
        public WorldVehicleData<VehicleDoorState> DoorFrontLeft { get;  }
        public WorldVehicleData<VehicleDoorState> DoorFrontRight { get; }
        public WorldVehicleData<VehicleDoorState> DoorRearLeft { get; }
        public WorldVehicleData<VehicleDoorState> DoorRearRight { get; }
        public WorldVehicleData<VehicleDoorState> DoorHood { get; }
        public WorldVehicleData<VehicleDoorState> DoorTrunk { get; }

        public WorldVehicleDoorsState(WorldVehicle InOwner) : base(InOwner)
        {
            DoorFrontLeft = new WorldVehicleData<VehicleDoorState>(InOwner, nameof(DoorFrontLeft));
            DoorFrontRight = new WorldVehicleData<VehicleDoorState>(InOwner, nameof(DoorFrontRight));

            DoorRearLeft = new WorldVehicleData<VehicleDoorState>(InOwner, nameof(DoorRearLeft));
            DoorRearRight = new WorldVehicleData<VehicleDoorState>(InOwner, nameof(DoorRearRight));

            DoorHood = new WorldVehicleData<VehicleDoorState>(InOwner, nameof(DoorHood));
            DoorTrunk = new WorldVehicleData<VehicleDoorState>(InOwner, nameof(DoorTrunk));
        }
    }
}