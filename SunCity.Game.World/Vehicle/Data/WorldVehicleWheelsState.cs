﻿using SunCity.Game.Enums.Vehicle.Wheels;
using SunCity.Game.World.Common.Components;

namespace SunCity.Game.World.Vehicle.Data
{
    public class WorldVehicleWheelsState : BaseEntityComponent<WorldVehicle>
    {
        public WorldVehicleData<VehicleWheelState> Wheel0 { get; }
        public WorldVehicleData<VehicleWheelState> Wheel1 { get; }
        public WorldVehicleData<VehicleWheelState> Wheel2 { get; }
        public WorldVehicleData<VehicleWheelState> Wheel3 { get; }
        public WorldVehicleData<VehicleWheelState> Wheel4 { get; }
        public WorldVehicleData<VehicleWheelState> Wheel5 { get; }
        public WorldVehicleData<VehicleWheelState> Wheel6 { get; }
        public WorldVehicleData<VehicleWheelState> Wheel7 { get; }
        public WorldVehicleData<VehicleWheelState> Wheel8 { get; }
        public WorldVehicleData<VehicleWheelState> Wheel9 { get; }

        public WorldVehicleWheelsState(WorldVehicle InOwner) : base(InOwner)
        {
            Wheel0 = new WorldVehicleData<VehicleWheelState>(InOwner, nameof(Wheel0));
            Wheel1 = new WorldVehicleData<VehicleWheelState>(InOwner, nameof(Wheel1));
            Wheel2 = new WorldVehicleData<VehicleWheelState>(InOwner, nameof(Wheel2));
            Wheel3 = new WorldVehicleData<VehicleWheelState>(InOwner, nameof(Wheel3));
            Wheel4 = new WorldVehicleData<VehicleWheelState>(InOwner, nameof(Wheel4));
            Wheel5 = new WorldVehicleData<VehicleWheelState>(InOwner, nameof(Wheel5));
            Wheel6 = new WorldVehicleData<VehicleWheelState>(InOwner, nameof(Wheel6));
            Wheel7 = new WorldVehicleData<VehicleWheelState>(InOwner, nameof(Wheel7));
            Wheel8 = new WorldVehicleData<VehicleWheelState>(InOwner, nameof(Wheel8));
            Wheel9 = new WorldVehicleData<VehicleWheelState>(InOwner, nameof(Wheel9));
        }
    }
}