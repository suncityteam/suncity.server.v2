﻿using SunCity.Game.World.Common.Components;

namespace SunCity.Game.World.Vehicle.Data
{
    /// <summary>
    /// https://wiki.rage.mp/index.php?title=Vehicle_Colors
    /// </summary>
    public class WorldVehicleColor : BaseEntityComponent<WorldVehicle>
    {
        public WorldVehicleData<int> FirstColor { get; }
        public WorldVehicleData<int> SecondColor { get; }

        /// <summary>
        /// <para>https://wiki.rage.mp/index.php?title=Vehicle::pearlescentColor</para>
        /// </summary>
        public WorldVehicleData<int> Pearlescent { get; }

        public WorldVehicleColor(WorldVehicle InOwner) : base(InOwner)
        {
            FirstColor = new WorldVehicleData<int>(InOwner, nameof(FirstColor));
            FirstColor.OnValueChanged += OnFirstColorChanged;

            SecondColor = new WorldVehicleData<int>(InOwner, nameof(SecondColor));
            SecondColor.OnValueChanged += OnSecondColorChanged;

            Pearlescent =  new WorldVehicleData<int>(InOwner, nameof(Pearlescent));
            Pearlescent.OnValueChanged += OnPearlescentColorChanged;
        }

        private void OnPearlescentColorChanged(WorldVehicle InWorldVehicle, int InPearlescentColor)
        {
            InWorldVehicle.Vehicle.PearlescentColor  = InPearlescentColor;
        }

        private void OnFirstColorChanged(WorldVehicle InWorldVehicle, int InFirstColor)
        {
            InWorldVehicle.Vehicle.PrimaryColor = InFirstColor;
        }

        private void OnSecondColorChanged(WorldVehicle InWorldVehicle, int InSecondColor)
        {
            InWorldVehicle.Vehicle.SecondaryColor = InSecondColor;
        }
    }
}
