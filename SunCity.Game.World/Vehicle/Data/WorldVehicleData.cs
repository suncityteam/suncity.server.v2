﻿using SunCity.Game.World.Common.Components;

namespace SunCity.Game.World.Vehicle.Data
{
    public class WorldVehicleData<TValue> : BaseEntitySharedData<WorldVehicle, TValue>
    {
        public override TValue Value
        {
            set
            {
                base.Value = value;
                Owner.Vehicle.SetSharedData(Name, value);
                OnValueChanged?.Invoke(Owner, value);
            }
        }

        public WorldVehicleData(WorldVehicle InWorldVehicle, string InName) : base(InWorldVehicle, InName)
        {

        }
    }
}
