﻿using SunCity.Game.Enums.Vehicle.Windows;
using SunCity.Game.World.Common.Components;

namespace SunCity.Game.World.Vehicle.Data
{
    public class WorldVehicleWindowsState : BaseEntityComponent<WorldVehicle>
    {
        public WorldVehicleData<VehicleWindowStates> WindowFrontLeft { get; }
        public WorldVehicleData<VehicleWindowStates> WindowFrontRight { get; }
        public WorldVehicleData<VehicleWindowStates> WindowRearRight { get; }
        public WorldVehicleData<VehicleWindowStates> WindowRearLeft { get; }

        public WorldVehicleWindowsState(WorldVehicle InOwner) : base(InOwner)
        {
            WindowFrontLeft = new WorldVehicleData<VehicleWindowStates>(InOwner, nameof(WindowFrontLeft));
            WindowFrontRight = new WorldVehicleData<VehicleWindowStates>(InOwner, nameof(WindowFrontRight));
            WindowRearRight = new WorldVehicleData<VehicleWindowStates>(InOwner, nameof(WindowRearRight));
            WindowRearLeft = new WorldVehicleData<VehicleWindowStates>(InOwner, nameof(WindowRearLeft));
        }
    }
}