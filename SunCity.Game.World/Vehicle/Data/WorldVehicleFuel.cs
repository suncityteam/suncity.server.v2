﻿using System.Threading.Tasks;
using SunCity.Game.Enums.Vehicle;
using SunCity.Game.World.Common.Components;
using SunCity.Game.World.Extensions.Player;

namespace SunCity.Game.World.Vehicle.Data
{
    public class WorldVehicleFuel : BaseEntityComponent<WorldVehicle>
    {
        public WorldVehicleData<float> Fuel { get; }
        public WorldVehicleData<float> FuelTankSize { get; }
        public WorldVehicleData<VehicleFuelType> FuelType { get; }

        public WorldVehicleFuel(WorldVehicle InOwner) : base(InOwner)
        {
            Fuel         = new WorldVehicleData<float>(InOwner, nameof(Fuel));
            FuelType     = new WorldVehicleData<VehicleFuelType>(InOwner, nameof(FuelType));
            FuelTankSize = new WorldVehicleData<float>(InOwner, nameof(FuelTankSize));
            Task.Factory.StartNew(OnTick, TaskCreationOptions.LongRunning);
        }

        private async Task OnTick()
        {
            while (Owner.Vehicle != null && Owner.Vehicle.Exists)
            {
                await Task.Delay(1000);

                if (Owner.PartsState.EngineState.Value)
                {
                    var fuelConsumption = Owner.Model.FuelConsumptionPerSec;
                    if (Fuel.Value >= fuelConsumption)
                    {
                        Fuel.Value -= fuelConsumption;
                    }
                    else
                    {
                        Owner.PartsState.EngineState.Value = false;
                        //var driver = Owner.Vehicle.Occupants(-1);
                        //if (driver != null)
                        //{
                        //    await driver.SendClientMessage("В транспорте закончилось топливо");
                        //}
                    }
                }
            }
        }


    }
}