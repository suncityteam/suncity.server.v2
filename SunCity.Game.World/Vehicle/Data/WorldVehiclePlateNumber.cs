﻿using SunCity.Common.RageMp.Enums;
using SunCity.Game.Enums.Vehicle;
using SunCity.Game.World.Common.Components;

namespace SunCity.Game.World.Vehicle.Data
{
    public class WorldVehiclePlateNumber : BaseEntityComponent<WorldVehicle>
    {
        public WorldVehicleData<string> PlateNumber { get; }
        public WorldVehicleData<VehicleNumberPlateType> PlateNumberStyle { get; }

        public WorldVehiclePlateNumber(WorldVehicle InOwner) : base(InOwner)
        {
            PlateNumber = new WorldVehicleData<string>(InOwner, nameof(PlateNumber));
            PlateNumber.OnValueChanged += OnPlateNumberChanged;

            PlateNumberStyle = new WorldVehicleData<VehicleNumberPlateType>(InOwner, nameof(PlateNumberStyle));
            PlateNumberStyle.OnValueChanged += OnPlateNumberStyleChanged;
        }

        private void OnPlateNumberStyleChanged(WorldVehicle InWorldVehicle, VehicleNumberPlateType InPlateNumberStyle)
        {
            InWorldVehicle.Vehicle.NumberPlateStyle = (int)InPlateNumberStyle;
        }

        private void OnPlateNumberChanged(WorldVehicle InWorldVehicle, string InPlateNumber)
        {
            InWorldVehicle.Vehicle.NumberPlate = InPlateNumber;
        }
    }
}