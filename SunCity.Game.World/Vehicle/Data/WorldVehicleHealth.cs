﻿using SunCity.Game.World.Common.Components;

namespace SunCity.Game.World.Vehicle.Data
{
    public class WorldVehicleHealth : BaseEntityComponent<WorldVehicle>
    {
        public WorldVehicleData<float> DirtyLevel { get; }
        public WorldVehicleData<float> BodyHealth { get; }
        public WorldVehicleData<float> EngineHealth { get; }
        public WorldVehicleData<float> GearsHealth { get; }
        public WorldVehicleData<double> Mileage { get; }

        public WorldVehicleHealth(WorldVehicle InOwner) : base(InOwner)
        {
            DirtyLevel = new WorldVehicleData<float>(InOwner, nameof(DirtyLevel));
            BodyHealth = new WorldVehicleData<float>(InOwner, nameof(BodyHealth));
            EngineHealth = new WorldVehicleData<float>(InOwner, nameof(EngineHealth));
            GearsHealth = new WorldVehicleData<float>(InOwner, nameof(GearsHealth));
            Mileage = new WorldVehicleData<double>(InOwner, nameof(Mileage));
        }
    }
}