﻿using System;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using SunCity.Common.EfCore;
using SunCity.Common.RageMp.Pools;
using SunCity.Common.RageMp.Pools.Vehicles;
using SunCity.Common.RageMp.Tasks;
using SunCity.Game.Types.Storage;
using SunCity.Game.World.DataBase;
using SunCity.Game.World.DataBase.Entitites.Vehicle;
using SunCity.Game.World.VehicleModels;

namespace SunCity.Game.World.Vehicle
{
    public class WorldVehicleStorage : WorldStorage<VehicleId, WorldVehicle>, IWorldVehicleStorage
    {
        private ILogger<WorldVehicleStorage> Logger { get; }
        private IVehicleModelStorage ModelStorage { get; }
        private IServiceProvider ServiceProvider { get; }
        private IRageMpPool RageMpPool { get; }
        private IRageMpTaskManager RageMpTaskManager { get; }

        public WorldVehicleStorage
        (
            ILogger<WorldVehicleStorage> InLogger,
            IVehicleModelStorage InModelStorage,
            IServiceProvider serviceProvider, 
            IRageMpPool rageMpPool, 
            IRageMpTaskManager rageMpTaskManager
            )
        {
            Logger = InLogger;
            ModelStorage = InModelStorage;
            ServiceProvider = serviceProvider;
            RageMpPool = rageMpPool;
            RageMpTaskManager = rageMpTaskManager;
        }

        public WorldVehicle this[IRageVehicle InVehicle]
        {
            get
            {
                if (InVehicle == null || InVehicle.Exists == false)
                {
                    return null;
                }

                var vehicleId = InVehicle.GetData<VehicleId>(nameof(WorldVehicle.EntityId));
                if (vehicleId.IsValid)
                {
                    var vehicle = this[vehicleId];
                    return vehicle;
                }

                return null;
            }
        }

        public WorldVehicle AddVehicle(VehicleEntity InVehicleEntity)
        {
            var vehicle = new WorldVehicle(RageMpPool, ModelStorage, InVehicleEntity).Initialize(InVehicleEntity);
            Put(vehicle);
            return vehicle;
        }

        public async Task Initialize()
        {
            using var scope = ServiceProvider.CreateScope();
            var dbContext = scope.ServiceProvider.GetService<GameWorldDbContext>();

            Logger.LogInformation("Begin Initialize");

            var models = await dbContext.VehicleEntity
                .HasTracking(hasTracking: false)
                .ToArrayAsync();

            Logger.LogInformation($"Fetched {models.Length} vehicles");
            RageMpTaskManager.Schedule(() =>
            {
                foreach (var model in models)
                {
                    AddVehicle(model);
                }

                Logger.LogInformation($"Loaded {Items.Count} vehicles");
                Logger.LogInformation("End Initialize");
            });


        }
    }
}