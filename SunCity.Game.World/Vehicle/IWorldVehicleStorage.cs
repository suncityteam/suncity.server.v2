﻿using System.Threading.Tasks;
using SunCity.Common.RageMp.Pools.Vehicles;
using SunCity.Game.Types.Storage;
using SunCity.Game.World.DataBase.Entitites.Vehicle;

namespace SunCity.Game.World.Vehicle
{
    public interface IWorldVehicleStorage : IWorldStorage<VehicleId, WorldVehicle>
    {
        WorldVehicle this[IRageVehicle InVehicle] { get; }

        WorldVehicle AddVehicle(VehicleEntity InVehicleEntity);
        Task Initialize();
    }
}