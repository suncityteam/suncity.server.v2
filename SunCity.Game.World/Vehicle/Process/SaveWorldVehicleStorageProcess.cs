﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using SunCity.Common.Extensions;
using SunCity.Game.World.Common.Process;
using SunCity.Game.World.DataBase;
using SunCity.Game.World.DataBase.Entitites.Vehicle;
using SunCity.Game.World.Vehicle.Validators;

namespace SunCity.Game.World.Vehicle.Process
{
    public sealed class SaveWorldVehicleStorageProcess : GameWorldProcess
    {
        private IWorldVehicleStorage VehicleStorage { get; }

        public SaveWorldVehicleStorageProcess(IServiceProvider InServiceProvider, IWorldVehicleStorage InVehicleStorage) : base(InServiceProvider)
        {
            VehicleStorage = InVehicleStorage;
        }

        protected override async Task OnProcess()
        {
            while (true)
            {
                await Task.Delay(5000);

                using var scope = CreateScope();
                var dbContext = scope.ServiceProvider.GetService<GameWorldDbContext>();
                await SaveStorageProcess(dbContext);
            }
        }

        private async Task SaveStorageProcess(GameWorldDbContext InDbContext)
        {
            var avalibaleVehicles = VehicleStorage.ValuesCopy.Where(WorldVehicleValidators.IsValidVehicle).ToArray();
            foreach (var batch in avalibaleVehicles.Batch(10))
            {
                var vehiclesArr = batch.ToArray();
                var vehicles = await GetVehicles(InDbContext, vehiclesArr);

                foreach (var worldVehicle in vehiclesArr)
                {
                    if (vehicles.TryGetValue(worldVehicle.EntityId, out var vehicleEntity))
                    {
                        worldVehicle.OnSaveChanges(vehicleEntity);
                    }
                }
                await InDbContext.SaveChangesAsync();
            }

            await InDbContext.SaveChangesAsync();
        }

        private Task<Dictionary<VehicleId, VehicleEntity>> GetVehicles(GameWorldDbContext InDbContext, WorldVehicle[] InVehicles)
        {
            var vehicleIds = InVehicles.Select(q => q.EntityId).ToArray();
            return InDbContext.VehicleEntity
                   .Where(q => vehicleIds.Contains(q.EntityId))
                   .ToDictionaryAsync(q => q.EntityId, q => q);
        }

    }
}
