﻿using System;
using System.Linq;
using System.Threading.Tasks;
using SunCity.Game.World.Common.Process;
using SunCity.Game.World.Vehicle.Validators;

namespace SunCity.Game.World.Vehicle.Process
{
    public class TickWorldVehicleStorageProcess : GameWorldProcess
    {
        private IWorldVehicleStorage VehicleStorage { get; }

        public TickWorldVehicleStorageProcess(IWorldVehicleStorage InVehicleStorage, IServiceProvider InServiceProvider) : base(InServiceProvider)
        {
            VehicleStorage = InVehicleStorage;
        }

        protected override async Task OnProcess()
        {
            while (true)
            {
                await Task.Delay(1000);

                var vehicles = VehicleStorage.ValuesCopy
                                         .Where(WorldVehicleValidators.IsValidVehicle)
                                         .ToArray();

                foreach (var vehicle in vehicles)
                {
                    await vehicle.OnTick();
                }
            }
        }
    }
}