﻿using System;
using System.Threading.Tasks;
using SunCity.Common.RageMp.Pools;
using SunCity.Common.RageMp.Pools.Vehicles;
using SunCity.Common.Types.Extensions;
using SunCity.Domain.Interfaces.Entity;
using SunCity.Game.Enums.Vehicle;
using SunCity.Game.World.DataBase.Entitites.Vehicle;
using SunCity.Game.World.Vehicle.Data;
using SunCity.Game.World.VehicleModels;

namespace SunCity.Game.World.Vehicle
{
    public class WorldVehicle : IEntityWithId<VehicleId>
    {
        public VehicleId EntityId { get; }
        public VehicleModel Model { get; private set; }
        public IRageVehicle Vehicle { get; private set; }

        public WorldVehiclePlateNumber PlateNumber { get; }
        public WorldVehicleFuel Fuel { get; }

        public WorldVehicleColor Color { get; }
        public WorldVehicleHealth Health { get; }

        public WorldVehiclePartsState PartsState { get; }
        public WorldVehicleWindowsState WindowsState { get; }
        public WorldVehicleWheelsState WheelsState { get; }
        public WorldVehicleDoorsState DoorsState { get; }

        public WorldVehicle(IRageMpPool pool, IVehicleModelStorage InVehicleModelStorage, VehicleEntity InVehicleEntity)
        {
            EntityId = InVehicleEntity.EntityId;
            Model = InVehicleModelStorage[InVehicleEntity.ModelId];

            Vehicle = pool.Vehicles.New(Model.EntityId, InVehicleEntity.Position.AsVector3(), InVehicleEntity.Rotation, InVehicleEntity.PlateNumber.Number);
            Vehicle.SetData(nameof(EntityId), InVehicleEntity.EntityId);


            Fuel = new WorldVehicleFuel(this);

            Health = new WorldVehicleHealth(this);

            PlateNumber = new WorldVehiclePlateNumber(this);

            Color = new WorldVehicleColor(this);
            PartsState = new WorldVehiclePartsState(this);

            WindowsState = new WorldVehicleWindowsState(this);
            WheelsState = new WorldVehicleWheelsState(this);
            DoorsState = new WorldVehicleDoorsState(this);
        }

        public WorldVehicle Initialize(VehicleEntity InVehicleEntity)
        {
            //=====================
            OnInitialize(InVehicleEntity);

            //=====================
            return this;
        }

        private void OnInitialize(VehicleEntity InVehicleEntity)
        {
            //=====================
            //  Health
            Health.BodyHealth.Value = InVehicleEntity.Health.Body;
            Health.GearsHealth.Value = InVehicleEntity.Health.Gears;
            Health.EngineHealth.Value = InVehicleEntity.Health.Engine;
            Health.DirtyLevel.Value = InVehicleEntity.DirtyLevel;
            Health.Mileage.Value = InVehicleEntity.Mileage;

            //=====================
            //  Fuel
            Fuel.FuelTankSize.Value = Model.FuelTankSize;
            Fuel.Fuel.Value = InVehicleEntity.Fuel;
            Fuel.FuelType.Value = Model.FuelType;

            //=====================
            //  Color
            Color.FirstColor.Value = InVehicleEntity.Color.FirstColor;
            Color.SecondColor.Value = InVehicleEntity.Color.SecondColor;
            Color.Pearlescent.Value = InVehicleEntity.Color.Pearlescent;

            //=====================
            //  PlateNumber
            PlateNumber.PlateNumber.Value = InVehicleEntity.PlateNumber.Number;
            PlateNumber.PlateNumberStyle.Value = InVehicleEntity.PlateNumber.Style;

            //=====================
            //  WindowsState
            WindowsState.WindowRearLeft.Value = InVehicleEntity.WindowsState.WindowRearLeft;
            WindowsState.WindowRearRight.Value = InVehicleEntity.WindowsState.WindowRearRight;

            WindowsState.WindowFrontLeft.Value = InVehicleEntity.WindowsState.WindowFrontLeft;
            WindowsState.WindowFrontRight.Value = InVehicleEntity.WindowsState.WindowFrontRight;

            //=====================
            //  WheelsState
            WheelsState.Wheel0.Value = InVehicleEntity.WheelsState.Wheel0;
            WheelsState.Wheel1.Value = InVehicleEntity.WheelsState.Wheel1;
            WheelsState.Wheel2.Value = InVehicleEntity.WheelsState.Wheel2;
            WheelsState.Wheel3.Value = InVehicleEntity.WheelsState.Wheel3;
            WheelsState.Wheel4.Value = InVehicleEntity.WheelsState.Wheel4;
            WheelsState.Wheel5.Value = InVehicleEntity.WheelsState.Wheel5;
            WheelsState.Wheel6.Value = InVehicleEntity.WheelsState.Wheel6;
            WheelsState.Wheel7.Value = InVehicleEntity.WheelsState.Wheel7;
            WheelsState.Wheel8.Value = InVehicleEntity.WheelsState.Wheel8;
            WheelsState.Wheel9.Value = InVehicleEntity.WheelsState.Wheel9;

            //=====================
            //  DoorsState
            DoorsState.DoorHood.Value = InVehicleEntity.DoorsState.DoorHood;
            DoorsState.DoorTrunk.Value = InVehicleEntity.DoorsState.DoorTrunk;

            DoorsState.DoorFrontLeft.Value = InVehicleEntity.DoorsState.DoorFrontLeft;
            DoorsState.DoorFrontRight.Value = InVehicleEntity.DoorsState.DoorFrontRight;

            DoorsState.DoorRearLeft.Value = InVehicleEntity.DoorsState.DoorRearLeft;
            DoorsState.DoorRearRight.Value = InVehicleEntity.DoorsState.DoorRearRight;

            //=====================
            //  PartsState
            PartsState.EngineState.Value = InVehicleEntity.PartsState.HasFlag(VehiclePartsState.Engine);
            PartsState.LockedDoors.Value = InVehicleEntity.PartsState.HasFlag(VehiclePartsState.LockedDoors);

            PartsState.LightsState.Value = InVehicleEntity.PartsState.HasFlag(VehiclePartsState.Lights);
            PartsState.LeftLightIndicator.Value = InVehicleEntity.PartsState.HasFlag(VehiclePartsState.LeftLightIndicator);
            PartsState.RightLightIndicator.Value = InVehicleEntity.PartsState.HasFlag(VehiclePartsState.RightLightIndicator);

            PartsState.SirenSound.Value = InVehicleEntity.PartsState.HasFlag(VehiclePartsState.SirenSound);
            PartsState.Nitro.Value = InVehicleEntity.PartsState.HasFlag(VehiclePartsState.Nitro);

        }

        public void OnVehicleDamage(float InBodyDamage, float InEngineDamage)
        {
            Health.BodyHealth.Value -= InBodyDamage;
            Health.EngineHealth.Value -= InEngineDamage;
        }

        public void OnSaveChanges(VehicleEntity InVehicleEntity)
        {
            //=====================
            //  Health
            InVehicleEntity.Health.Body = Health.BodyHealth.Value;
            InVehicleEntity.Health.Engine = Health.EngineHealth.Value;
            InVehicleEntity.Health.Gears = Health.GearsHealth.Value;
            InVehicleEntity.DirtyLevel = Convert.ToByte(Health.DirtyLevel.Value);

            //=====================
            InVehicleEntity.Position = Vehicle.Position;
            InVehicleEntity.Rotation = Vehicle.Rotation.Z;

            //=====================
            //  Fuel
            InVehicleEntity.Fuel = Fuel.Fuel.Value;

            //=====================
            //  Color
            InVehicleEntity.Color.FirstColor = Color.FirstColor.Value;
            InVehicleEntity.Color.SecondColor = Color.SecondColor.Value;

            //=====================
            //  PlateNumber
            InVehicleEntity.PlateNumber.Number = PlateNumber.PlateNumber.Value;
            InVehicleEntity.PlateNumber.Style = PlateNumber.PlateNumberStyle.Value;

            //=====================
            //  WindowsState
            InVehicleEntity.WindowsState.WindowRearLeft = WindowsState.WindowRearLeft.Value;
            InVehicleEntity.WindowsState.WindowRearRight = WindowsState.WindowRearRight.Value;

            InVehicleEntity.WindowsState.WindowFrontLeft = WindowsState.WindowFrontLeft.Value;
            InVehicleEntity.WindowsState.WindowFrontRight = WindowsState.WindowFrontRight.Value;

            //=====================
            //  WheelsState
            InVehicleEntity.WheelsState.Wheel0 = WheelsState.Wheel0.Value;
            InVehicleEntity.WheelsState.Wheel1 = WheelsState.Wheel1.Value;
            InVehicleEntity.WheelsState.Wheel2 = WheelsState.Wheel2.Value;
            InVehicleEntity.WheelsState.Wheel3 = WheelsState.Wheel3.Value;
            InVehicleEntity.WheelsState.Wheel4 = WheelsState.Wheel4.Value;
            InVehicleEntity.WheelsState.Wheel5 = WheelsState.Wheel5.Value;
            InVehicleEntity.WheelsState.Wheel6 = WheelsState.Wheel6.Value;
            InVehicleEntity.WheelsState.Wheel7 = WheelsState.Wheel7.Value;
            InVehicleEntity.WheelsState.Wheel8 = WheelsState.Wheel8.Value;
            InVehicleEntity.WheelsState.Wheel9 = WheelsState.Wheel9.Value;

            //=====================
            //  DoorsState
            InVehicleEntity.DoorsState.DoorHood = DoorsState.DoorHood.Value;
            InVehicleEntity.DoorsState.DoorHood = DoorsState.DoorTrunk.Value;

            InVehicleEntity.DoorsState.DoorFrontLeft = DoorsState.DoorFrontLeft.Value;
            InVehicleEntity.DoorsState.DoorFrontRight = DoorsState.DoorFrontRight.Value;

            InVehicleEntity.DoorsState.DoorRearLeft = DoorsState.DoorRearLeft.Value;
            InVehicleEntity.DoorsState.DoorRearRight = DoorsState.DoorRearRight.Value;

            //=====================
            //  PartsState
            InVehicleEntity.PartsState = PartsState.State;
        }

        public async Task OnTick()
        {
        }
    }
}
