﻿using System.Diagnostics.CodeAnalysis;

namespace SunCity.Game.World.Vehicle.Validators
{
    public static class WorldVehicleValidators
    {
        [SuppressMessage("ReSharper", "UseNullPropagation")]
        public static bool IsValidVehicle(WorldVehicle InWorldPlayer)
        {
            if (InWorldPlayer == null)
                return false;

            if (InWorldPlayer.Vehicle == null || InWorldPlayer.Vehicle.Exists == false)
                return false;

            return true;
        }
    }
}
