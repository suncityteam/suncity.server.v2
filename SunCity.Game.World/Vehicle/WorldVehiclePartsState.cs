﻿using SunCity.Game.Enums.Vehicle;
using SunCity.Game.World.Common.Components;
using SunCity.Game.World.Vehicle.Data;

namespace SunCity.Game.World.Vehicle
{
    public class WorldVehiclePartsState : BaseEntityComponent<WorldVehicle>
    {
        public WorldVehicleData<bool> EngineState { get; }
        public WorldVehicleData<bool> LockedDoors { get; }

        public WorldVehicleData<bool> LightsState { get; }

        public WorldVehicleData<bool> LeftLightIndicator { get; }
        public WorldVehicleData<bool> RightLightIndicator { get; }

        public WorldVehicleData<bool> Nitro { get; }
        public WorldVehicleData<bool> SirenSound { get; }
        
        public WorldVehiclePartsState(WorldVehicle InOwner) : base(InOwner)
        {
            EngineState = new WorldVehicleData<bool>(InOwner, nameof(EngineState));
            LockedDoors = new WorldVehicleData<bool>(InOwner, nameof(LockedDoors));

            LightsState = new WorldVehicleData<bool>(InOwner, nameof(LightsState));

            LeftLightIndicator = new WorldVehicleData<bool>(InOwner, nameof(LeftLightIndicator));
            RightLightIndicator = new WorldVehicleData<bool>(InOwner, nameof(RightLightIndicator));


            Nitro = new WorldVehicleData<bool>(InOwner, nameof(Nitro));
            SirenSound = new WorldVehicleData<bool>(InOwner, nameof(SirenSound));

            EngineState.OnValueChanged += OnEngineStateChanged;
            LightsState.OnValueChanged += OnLightsStateChanged;
            LockedDoors.OnValueChanged += OnDoorsStateChanged;
        }

        public VehiclePartsState State
        {
            get
            {
                var state = VehiclePartsState.None;

                if (EngineState.Value)
                    state |= VehiclePartsState.Engine;

                if (LockedDoors.Value)
                    state |= VehiclePartsState.LockedDoors;

                if (LightsState.Value)
                    state |= VehiclePartsState.Lights;

                if (LeftLightIndicator.Value)
                    state |= VehiclePartsState.LeftLightIndicator;

                if (RightLightIndicator.Value)
                    state |= VehiclePartsState.RightLightIndicator;

                if (SirenSound.Value)
                    state |= VehiclePartsState.SirenSound;

                if (Nitro.Value)
                    state |= VehiclePartsState.Nitro;
                return state;
            }
        }

        private void OnEngineStateChanged(WorldVehicle InWorldVehicle, bool InEngineState)
        {
            InWorldVehicle.Vehicle.EngineStatus =InEngineState;
        }

        private void OnLightsStateChanged(WorldVehicle InWorldVehicle, bool InLightsState)
        {
            //InWorldVehicle.Vehicle.SetLightsActive(InLightsState);
        }

        private void OnDoorsStateChanged(WorldVehicle InWorldVehicle, bool InDoorsState)
        {
            InWorldVehicle.Vehicle.Locked = InDoorsState;
        }
    }
}