﻿using GTANetworkAPI;
using SunCity.Domain.Interfaces.Entity;
using SunCity.Game.Enums.Vehicle;
using SunCity.Game.World.DataBase.Entitites.Vehicle;

namespace SunCity.Game.World.VehicleModels
{
    public class VehicleModel 
        : IEntityWithId<VehicleHash>
    {
        public VehicleHash EntityId { get; }
        public string Name { get; }

        public VehicleModelClass ModelClass { get; }

        public VehicleFuelType FuelType { get; }
        public float FuelTankSize { get; }

        /// <summary>
        /// Расход топлива в литрах за минуту
        /// </summary>
        public float FuelConsumptionPerMinute { get; } = 3.0f;

        /// <summary>
        /// Расход топлива в литрах за секунду
        /// </summary>
        public float FuelConsumptionPerSec => FuelConsumptionPerMinute / 60.0f;

        /// <summary>
        /// <para>Управляемость</para>
        /// </summary>
        public byte Handleability { get;  }
        
        /// <summary>
        /// <para>Вместимость багажника</para>
        /// </summary>
        public short BagCapacity { get;  }
        
        /// <summary>
        /// <para>Количество мест</para>
        /// </summary>
        public byte NumberOfSeats { get;  }
        
        /// <summary>
        /// <para>Разгон 0-100 за N сек</para>
        /// </summary>
        public float Acceleration { get;  }
        
        /// <summary>
        /// <para>Максимальная скорость в км/ч</para>
        /// </summary>
        public short MaxSpeed { get;  }
        
        public VehicleModel(VehicleModelEntity InVehicleModelEntity)
        {
            EntityId = InVehicleModelEntity.EntityId;

            Name = InVehicleModelEntity.Name;
            ModelClass = InVehicleModelEntity.ModelClass;

            FuelType = InVehicleModelEntity.FuelType;
            FuelTankSize = InVehicleModelEntity.FuelTankSize;
            FuelConsumptionPerMinute = InVehicleModelEntity.FuelConsumptionPerMinute;
          
            
            MaxSpeed = InVehicleModelEntity.MaxSpeed;
            Acceleration = InVehicleModelEntity.Acceleration;

            NumberOfSeats = InVehicleModelEntity.NumberOfSeats;
            BagCapacity = InVehicleModelEntity.BagCapacity;

            Handleability = InVehicleModelEntity.Handleability;

        }
    }
}