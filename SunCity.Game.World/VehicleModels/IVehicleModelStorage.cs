﻿using System.Threading.Tasks;
using GTANetworkAPI;
using SunCity.Game.Types.Storage;
using SunCity.Game.World.DataBase;

namespace SunCity.Game.World.VehicleModels
{
    public interface IVehicleModelStorage : IWorldStorage<VehicleHash, VehicleModel>
    {
        Task Initialize(GameWorldDbContext InDbContext);
    }
}