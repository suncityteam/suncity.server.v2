﻿using System;
using System.Linq;
using System.Threading.Tasks;
using GTANetworkAPI;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using SunCity.Game.Enums.Vehicle;
using SunCity.Game.World.DataBase;
using SunCity.Game.World.DataBase.Entitites.Vehicle;

namespace SunCity.Game.World.VehicleModels
{
    internal class VehicleModelSeed
    {
        private ILogger<VehicleModelSeed> Logger { get; }

        public VehicleModelSeed(ILogger<VehicleModelSeed> InLogger)
        {
            Logger = InLogger;
        }


        public async Task Initialize(GameWorldDbContext InDbContext)
        {
            //=========================
            var available = await InDbContext.VehicleModelEntity.ToArrayAsync();
            var availableModels = available.Select(p => p.EntityId).ToArray();
            var models = Enum.GetValues(typeof(VehicleHash)).Cast<VehicleHash>().ToArray();

            //=========================
            var required = models.Except(availableModels).ToArray();

            //=========================
            Logger.LogDebug($"[GameVehicleRepository][models: {available.Length} / {models.Length} | required: {required.Length}]");

            //=========================
            foreach (var model in required)
            {
                InDbContext.VehicleModelEntity.Add(new VehicleModelEntity()
                {
                    EntityId = model,
                    Name = model.ToString(),
                    ModelClass = VehicleModelClass.None,
                    FuelTankSize = 50,
                    FuelType = VehicleFuelType.Petrol
                });
            }

            //=========================
            await InDbContext.SaveChangesAsync();

        }
    }
}