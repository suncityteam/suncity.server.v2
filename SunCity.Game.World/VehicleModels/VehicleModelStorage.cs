﻿using System.Threading.Tasks;
using GTANetworkAPI;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using SunCity.Common.EfCore;
using SunCity.Game.Types.Storage;
using SunCity.Game.World.DataBase;

namespace SunCity.Game.World.VehicleModels
{
    internal sealed class VehicleModelStorage : WorldStorage<VehicleHash, VehicleModel>, IVehicleModelStorage
    {
        private ILogger<VehicleModelStorage> Logger { get; }
        private VehicleModelSeed Seed { get; }

        public VehicleModelStorage(ILogger<VehicleModelStorage> InLogger, VehicleModelSeed InSeed)
        {
            Logger = InLogger;
            Seed = InSeed;
        }

        public async Task Initialize(GameWorldDbContext InDbContext)
        {
            await Seed.Initialize(InDbContext);

            Logger.LogInformation("Begin Initialize");

            var models = await InDbContext.VehicleModelEntity
                .HasTracking(hasTracking: false)
                .ToArrayAsync();

            Logger.LogInformation($"Fetched {models.Length} models");

            foreach (var model in models)
            {
                Put(new VehicleModel(model));
            }

            Logger.LogInformation($"Loaded {Items.Count} models");
            Logger.LogInformation("End Initialize");
        }
    }

}