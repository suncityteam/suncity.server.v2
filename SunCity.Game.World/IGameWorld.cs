﻿using System.Threading.Tasks;
using SunCity.Common.RageMp.Pools;
using SunCity.Common.RageMp.Server;
using SunCity.Game.World.Chat;
using SunCity.Game.World.Fraction;
using SunCity.Game.World.Interior;
using SunCity.Game.World.ItemModels;
using SunCity.Game.World.Licenses;
using SunCity.Game.World.Player;
using SunCity.Game.World.Property;
using SunCity.Game.World.StationPoint;
using SunCity.Game.World.Vehicle;
using SunCity.Game.World.VehicleModels;

namespace SunCity.Game.World
{
    public interface IGameWorld
    {
        IRageMpServer Server { get; }
        IGameWorldChat Chat { get; }
        IWorldPlayerStorage Players { get; }
        IWorldPropertyStorage Properties { get; }
        IWorldInteriorStorage Interiors { get; }
        IWorldFractionStorage Fractions { get; }
        IVehicleModelStorage VehicleModels { get; }
        IWorldVehicleStorage Vehicles { get; }
        IGameItemStorage Items { get; }

        IGameLicenseStorage Licenses { get; }
        IWorldStationPointStorage StationPoints { get; }
        
        Task BeginPlay();
        Task EndPlay();
    }
}