﻿using System;
using System.Collections.Generic;

using System.Threading.Tasks;
using GTANetworkAPI;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using SunCity.Common.Enum;
using SunCity.Game.Enums.World.Interior;
using SunCity.Game.Types.Property.Entity.Interior;
using SunCity.Game.Types.Storage;
using SunCity.Game.World.DataBase;
using SunCity.Game.World.DataBase.Entitites.Interior;

namespace SunCity.Game.World.Interior
{
    internal sealed class WorldInteriorSeed : WorldStorageSeed<GameWorldDbContext, InteriorId, InteriorEntity>
    {
        public static string CarShowRoom { get; }= "cc2fbda7-eb2c-42d3-99b4-36ef3798623b";
        
        public static string Trailer { get; }= "23a1f959-f689-490a-9d78-8f4e66b4711b";
        
        public static string Economy1 { get; }= "23a1f959-f689-490a-9d78-8f4e66b4712b";
        public static string Economy2 { get; }= "23a1f959-f689-490a-9d78-8f4e66b4713b";
            
        public static string Comfort1 { get; }= "23a1f959-f689-490a-9d78-8f4e66b4714b";
        public static string Comfort2 { get; }= "23a1f959-f689-490a-9d78-8f4e66b4715b";
                   
        public static string Premium1 { get; }= "23a1f959-f689-490a-9d78-8f4e66b4716b";
        public static string Premium2 { get; }= "23a1f959-f689-490a-9d78-8f4e66b4717b";
        
        protected override Func<GameWorldDbContext, DbSet<InteriorEntity>> Table => dbContext => dbContext.InteriorEntity;

        public WorldInteriorSeed(ILogger<WorldInteriorSeed> logger) : base(logger)
        {
        }

        public override async Task Initialize(GameWorldDbContext InDbContext)
        {
        return;
        
            await AddIfNotExist(InDbContext, Create("ca9d7a30-cda8-4c4a-889f-3bc5e17f6f39", InteriorType.Interior, "apa_v_mp_h_01_a", new Vector3(-786.8663f, 315.7642f, 217.6385f)));
            await AddIfNotExist(InDbContext, Create("b43c4aaa-40e7-4f69-8d18-7881e6ddb96f", InteriorType.Interior, "house_no_ipl_a", new Vector3(265.9776f, -1006.97f, -100.8839f)));
            await AddIfNotExist(InDbContext, Create("04a267d9-33b7-4006-b8ee-e1d021ac374e", InteriorType.Interior, "house_no_ipl_b", new Vector3(-30.58078f, -595.3096f, 80.03086f)));
            await AddIfNotExist(InDbContext, Create("a17ba936-abd9-4973-b63b-7563cee46454", InteriorType.Interior, "house_no_ipl_c", new Vector3(-30.58078f, -595.3096f, 80.03086f)));
            await AddIfNotExist(InDbContext, Create("f1bc4e96-f6f3-467d-b187-c6f13661b539", InteriorType.Interior, "house_no_ipl_d", new Vector3(-17.72512f, -588.8995f, 90.1148f)));
            await AddIfNotExist(InDbContext, Create("6ce8bae1-bae5-4f81-b10b-66e7369f1e74", InteriorType.Interior, "house_no_ipl_e", new Vector3(-1451.652f, -523.7687f, 56.92904f)));
            await AddIfNotExist(InDbContext, Create("d5fe1b52-8c6d-4f0a-91cb-5650462e7cbe", InteriorType.Interior, "house_no_ipl_f", new Vector3(-1451.652f, -523.7687f, 56.92904f)));
            await AddIfNotExist(InDbContext, Create("e969acd5-a99b-4635-864e-a5a82243b167", InteriorType.Interior, "house_no_ipl_g", new Vector3(-1451.652f, -523.7687f, 56.92904f)));
            await AddIfNotExist(InDbContext, Create("470b5ec3-b782-4a7f-8534-a6c031bbf033", InteriorType.Interior, "house_no_ipl_h", new Vector3(-1451.652f, -523.7687f, 56.92904f)));
            await AddIfNotExist(InDbContext, Create("1e87bb73-1f12-449a-be25-32d69712c69e", InteriorType.Interior, "house_no_ipl_i", new Vector3(-174.2659f, 497.3836f, 137.667f)));
            await AddIfNotExist(InDbContext, Create("252f8b03-8019-4746-8bdd-177b4b8cfe73", InteriorType.Interior, "house_no_ipl_j", new Vector3(-1451.652f, -523.7687f, 56.92904f)));
            await AddIfNotExist(InDbContext, Create("2916a5e7-cc49-4383-9cd1-d3e4c6687a4d", InteriorType.Interior, "house_no_ipl_k", new Vector3(-1451.652f, -523.7687f, 56.92904f)));
            await AddIfNotExist(InDbContext, Create("6dcdf68f-c1c6-4594-be4d-dbe0fba86127", InteriorType.Interior, "house_no_ipl_l", new Vector3(-1451.652f, -523.7687f, 56.92904f)));
            await AddIfNotExist(InDbContext, Create("53c4fc3c-2769-41f5-a179-0bb6212cc027", InteriorType.Interior, "house_no_ipl_m", new Vector3(-1451.652f, -523.7687f, 56.92904f)));
            await AddIfNotExist(InDbContext, Create("54afdd23-2643-4f69-81b0-1b2d3e15bc0a", InteriorType.Interior, "house_no_ipl_n", new Vector3(-1451.652f, -523.7687f, 56.92904f)));
            await AddIfNotExist(InDbContext, Create("5cd6da8c-db69-44c0-877e-605ebd707636", InteriorType.Interior, "house_no_ipl_o", new Vector3(-1451.652f, -523.7687f, 56.92904f)));
            await AddIfNotExist(InDbContext, Create("f7a44b34-5cbf-4fca-b89c-7a103f00a63b", InteriorType.Interior, "house_no_ipl_p", new Vector3(-1451.652f, -523.7687f, 56.92904f)));
            await AddIfNotExist(InDbContext, Create("54818381-bcb2-4898-a372-5b8dd99b9d6a", InteriorType.Interior, "ex_dt1_02_office_02b", new Vector3(-141.1987f, -620.913f, 168.8205f)));
            await AddIfNotExist(InDbContext, Create("a6df871a-bde0-4193-8009-211e5d3dfedc", InteriorType.Interior, "Coroner_Int_On", new Vector3(275.446f, -1361.11f, 24.5378f)));
            await AddIfNotExist(InDbContext, Create("e5bf1d78-2004-4db4-ba41-1865b5425c33", InteriorType.Interior, "driving_school", new Vector3(-177.19f, -1158.32f, 23.81f)));
            await AddIfNotExist(InDbContext, Create("56b4e15c-0ea8-49ad-a716-f5ab34601126", InteriorType.Interior, "apa_v_mp_h_08_b", new Vector3(-774.0349f, 342.0296f, 196.6862f)));
            await AddIfNotExist(InDbContext, Create("55105b3e-284e-4bff-b155-37776145d435", InteriorType.Interior, "facelobby", new Vector3(-1082.433f, -258.7667f, 37.76331f)));
            await AddIfNotExist(InDbContext, Create("ac6eaf2b-0a20-412f-bac9-68caec39ec43", InteriorType.Interior, "apa_v_mp_h_08_c", new Vector3(-786.9756f, 315.723f, 187.9134f)));
            await AddIfNotExist(InDbContext, Create("448726ef-4b79-47d4-a3d1-3898b5a7a2a8", InteriorType.Interior, "apa_v_mp_h_07_b", new Vector3(-774.0109f, 342.0965f, 196.6863f)));
            await AddIfNotExist(InDbContext, Create("917f6864-feca-41aa-bd5c-e7be5bd5dd01", InteriorType.Interior, "apa_v_mp_h_01_c", new Vector3(-786.9563f, 315.6229f, 187.9136f)));
            await AddIfNotExist(InDbContext, Create("450166bd-d55e-4f60-9bee-da1e646334a8", InteriorType.Interior, "apa_v_mp_h_01_b", new Vector3(-774.0126f, 342.0428f, 196.6864f)));
            await AddIfNotExist(InDbContext, Create("7d4c8eba-4a28-4297-8b04-70e35dc394fb", InteriorType.Interior, "apa_v_mp_h_02_a", new Vector3(-787.0749f, 315.8198f, 217.6386f)));
            await AddIfNotExist(InDbContext, Create("038921a6-2fa9-4976-9978-16da8e600643", InteriorType.Interior, "apa_v_mp_h_02_c", new Vector3(-786.8195f, 315.5634f, 187.9137f)));
            await AddIfNotExist(InDbContext, Create("fe70b90e-8387-4252-b4a6-98833216d087", InteriorType.Interior, "apa_v_mp_h_02_b", new Vector3(-774.1382f, 342.0316f, 196.6864f)));
            await AddIfNotExist(InDbContext, Create("2a378f4f-cc26-4b88-b4bf-8eafb4e47d2f", InteriorType.Interior, "apa_v_mp_h_03_a", new Vector3(-786.6245f, 315.6175f, 217.6385f)));
            await AddIfNotExist(InDbContext, Create("c587d914-01b4-4712-ab7f-7d13a7580493", InteriorType.Interior, "apa_v_mp_h_03_c", new Vector3(-786.9584f, 315.7974f, 187.9135f)));
            await AddIfNotExist(InDbContext, Create("eb9c72cc-0787-43a5-9972-6d67f59c3433", InteriorType.Interior, "apa_v_mp_h_03_b", new Vector3(-774.0223f, 342.1718f, 196.6863f)));
            await AddIfNotExist(InDbContext, Create("edb45682-8883-481f-9e63-d8dc55cc8964", InteriorType.Interior, "apa_v_mp_h_04_a", new Vector3(-787.0902f, 315.7039f, 217.6384f)));
            await AddIfNotExist(InDbContext, Create("78b8c071-6cd7-414c-80a3-86599adb07e3", InteriorType.Interior, "apa_v_mp_h_04_c", new Vector3(-787.0155f, 315.7071f, 187.9135f)));
            await AddIfNotExist(InDbContext, Create("00c8038c-e9d0-4fe9-9204-3679c83c5200", InteriorType.Interior, "apa_v_mp_h_04_b", new Vector3(-773.8976f, 342.1525f, 196.6863f)));
            await AddIfNotExist(InDbContext, Create("1f34c11f-da4e-46fb-b99f-cdfd5ec8ee99", InteriorType.Interior, "apa_v_mp_h_05_a", new Vector3(-786.9887f, 315.7393f, 217.6386f)));
            await AddIfNotExist(InDbContext, Create("f7d0242c-577f-4fd7-b56f-9118f39870eb", InteriorType.Interior, "apa_v_mp_h_05_c", new Vector3(-786.8809f, 315.6634f, 187.9136f)));
            await AddIfNotExist(InDbContext, Create("ba9cf479-14d1-4804-9f62-9d9703557eb3", InteriorType.Interior, "apa_v_mp_h_05_b", new Vector3(-774.0675f, 342.0773f, 196.6864f)));
            await AddIfNotExist(InDbContext, Create("c0c4de84-24f4-4ec3-a030-a1fb2c4fe1d2", InteriorType.Interior, "apa_v_mp_h_06_a", new Vector3(-787.1423f, 315.6943f, 217.6384f)));
            await AddIfNotExist(InDbContext, Create("1f9a2b37-c241-4139-925f-6d49603b30a3", InteriorType.Interior, "apa_v_mp_h_06_c", new Vector3(-787.0961f, 315.815f, 187.9135f)));
            await AddIfNotExist(InDbContext, Create("4a5d2a86-08fd-4d61-bd9c-62a8e7b78514", InteriorType.Interior, "apa_v_mp_h_06_b", new Vector3(-773.9552f, 341.9892f, 196.6862f)));
            await AddIfNotExist(InDbContext, Create("ce6071c9-dec4-418a-a44d-2ed32f8c9a7a", InteriorType.Interior, "apa_v_mp_h_07_a", new Vector3(-787.029f, 315.7113f, 217.6385f)));
            await AddIfNotExist(InDbContext, Create("e926ef67-7c47-4532-99a3-c4aa4e6cedc4", InteriorType.Interior, "apa_v_mp_h_07_c", new Vector3(-787.0574f, 315.6567f, 187.9135f)));
            await AddIfNotExist(InDbContext, Create("67f54618-7b7a-48df-84f7-d799adc45b1d", InteriorType.Interior, "apa_v_mp_h_08_a", new Vector3(-786.9469f, 315.5655f, 217.6383f)));
            await AddIfNotExist(InDbContext, Create("23a1f959-f689-490a-9d78-8f4e66b4749b", InteriorType.Interior, "police_station", new Vector3(435.4738f, -981.7497f, 30.69148f)));
            await AddIfNotExist(InDbContext, Create("23a1f959-f689-490a-9d78-8f4e66b4710b", InteriorType.Interior, "ex_sm_15_office_02b", new Vector3(-1392.667f, -480.4736f, 72.04217f)));
            
            //    Trailer
            await AddIfNotExist(InDbContext, Create(Trailer, InteriorType.Interior, "etrevorstrailer", new Vector3(1973.124f, 3816.065f, 32.30873f)));
            
            //    Econom
            await AddIfNotExist(InDbContext, Create(Economy1, InteriorType.Interior, "hei_hw1_blimp_interior_v_motel_mp_milo_", new Vector3(151.2052f, -1008.007f, -100.12f)));
            
            //    Econom+
            await AddIfNotExist(InDbContext, Create(Economy2, InteriorType.Interior, "hei_hw1_blimp_interior_v_studio_lo_milo_", new Vector3(265.9691f, -1007.078f, -102.0758f)));
            
            //    Comfort
            await AddIfNotExist(InDbContext, Create(Comfort1, InteriorType.Interior, "hei_hw1_blimp_interior_v_apart_midspaz_milo_", new Vector3(346.6991f, -1013.023f, -100.3162f)));
            
            //    Comfort+
            await AddIfNotExist(InDbContext, Create(Comfort2, InteriorType.Interior, "hei_hw1_blimp_interior_32_dlc_apart_high2_new_milo_", new Vector3(-31.35483f, -594.9686f, 78.9109f)));
            
            //    Premium
            await AddIfNotExist(InDbContext, Create(Premium1, InteriorType.Interior, "hei_hw1_blimp_interior_10_dlc_apart_high_new_milo_", new Vector3(-17.85757f, -589.0983f, 88.99482f)));
            
            //    Premium+
            await AddIfNotExist(InDbContext, Create(Premium2, InteriorType.Interior, "apa_ch2_05e_interior_0_v_mp_stilts_b_milo_", new Vector3(-164.9799f, 480.7568f, 137.1526f)));

            
            await AddIfNotExist(InDbContext, Create(CarShowRoom, InteriorType.Exterior, string.Empty, new Vector3(-56.88f, -1097.12f, 26.52f), entity => entity.ActionEntities = new List<InteriorActionEntity>
            {
                CreatePoint(InteriorActionType.StandPoint, new Vector3(-42.79771f, -1095.676f, 26.0117f)),
                CreatePoint(InteriorActionType.StandCamera, new Vector3(-42.3758f, -1101.672f, 26.42235f)),
                CreatePoint(InteriorActionType.StandRotation, new Vector3(0f, 0f, -136.246f)),
            }));

            /*var interiors = await InDbContext.InteriorEntity.ToArrayAsync();
            var rows = interiors.Select(q => $"await AddIfNotExist(InDbContext, Create(\"{q.EntityId}\", \"{q.IPL.Name}\", new Vector3({q.Position.X}f, {q.Position.Y}f, {q.Position.Z}f)));");
            var str = string.Join(Environment.NewLine, rows);*/

            await InDbContext.SaveChangesAsync();
        }

        private InteriorActionEntity CreatePoint(InteriorActionType action, Vector3 InLocation, Action<InteriorActionEntity> customizeProperty = null)
        {
            var entity = new InteriorActionEntity
            {
                EntityId = InteriorActionId.GenerateId(),
                Action = action,
                Position = InLocation,
                AdminDescription = $"{action} / {action.GetDescription()}"
            };
            
            customizeProperty?.Invoke(entity);
            return entity;
        }
        
        private InteriorEntity Create(string id, InteriorType interiorType, string InIPl, Vector3 InLocation, Action<InteriorEntity> customizeProperty = null)
        {
            return new InteriorEntity
            {
                EntityId = InteriorId.Parse(id),
                IPL = new InteriorItemPlacement(InIPl),

                Type = interiorType,
                Position = InLocation,
                SpawnPosition = InLocation,
            };
        }
    }
}