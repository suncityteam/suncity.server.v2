﻿using System.Linq;
using GTANetworkAPI;
using SunCity.Domain.Interfaces.Entity;
using SunCity.Game.Interfaces.Entity;
using SunCity.Game.Types.Property.Entity.Interior;
using SunCity.Game.World.DataBase.Entitites.Interior;

namespace SunCity.Game.World.Interior
{
    public class WorldInterior
        : IEntityWithId<InteriorId>
        , IEntityWithPosition<Vector3>
    {
        public InteriorId EntityId { get;  }
        public InteriorItemPlacement IPL { get; }

        public Vector3 Position { get;  }
        public float Rotation { get; }

        public Vector3 SpawnPosition { get; }
        public float SpawnRotation { get;  }
        public WorldInteriorAction[] Actions { get; }

        internal WorldInterior(InteriorEntity InInteriorEntity)
        {
            EntityId = InInteriorEntity.EntityId;
            IPL = InInteriorEntity.IPL;

            Position = InInteriorEntity.Position;
            Rotation = InInteriorEntity.Rotation;

            SpawnPosition = InInteriorEntity.SpawnPosition;
            SpawnRotation = InInteriorEntity.SpawnRotation;

            Actions = InInteriorEntity.ActionEntities.Select(q => new WorldInteriorAction(this, q)).ToArray();
        }

        public override string ToString()
        {
            return $"Interior: {EntityId}, IPL: {IPL}, Position: {Position} / {Rotation}, SpawnPosition: {SpawnPosition} / {SpawnRotation}";
        }
    }
}
