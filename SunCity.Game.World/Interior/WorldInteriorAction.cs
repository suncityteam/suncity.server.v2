﻿
using GTANetworkAPI;
using SunCity.Common.Enum;
using SunCity.Domain.Interfaces.Entity;
using SunCity.Game.Enums.World.Interior;
using SunCity.Game.Interfaces.Entity;
using SunCity.Game.World.DataBase.Entitites.Interior;

namespace SunCity.Game.World.Interior
{
    public class WorldInteriorAction
        : IEntityWithId<InteriorActionId>
          , IEntityWithPosition<Vector3>
    {
        public InteriorActionId EntityId { get; }

        public Vector3 Position { get; }
        public float Rotation { get; }

        public InteriorActionType Action { get;  }
        public string ActionString => Action.GetDescription();

        public string AdminDescription { get;  }
        public WorldInterior Interior { get; }

        internal WorldInteriorAction(WorldInterior InInterior, InteriorActionEntity InInteriorAction)
        {
            Interior = InInterior;
            EntityId = InInteriorAction.EntityId;
            Action   = InInteriorAction.Action;

            Position = InInteriorAction.Position;
            Rotation = InInteriorAction.Rotation;

            AdminDescription = InInteriorAction.AdminDescription;
        }

        public override string ToString()
        {
            return $"{Action} | Interior: {EntityId}, Position: {Position} / {Rotation} / {AdminDescription}";
        }
    }
}