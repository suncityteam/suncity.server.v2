﻿using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using SunCity.Common.EfCore;
using SunCity.Game.Types.Storage;
using SunCity.Game.World.DataBase;

namespace SunCity.Game.World.Interior
{
    public interface IWorldInteriorStorage : IWorldStorage<InteriorId, WorldInterior>
    {
        Task Initialize(GameWorldDbContext InDbContext);
    }

    internal sealed class WorldInteriorStorage : WorldStorage<InteriorId, WorldInterior>, IWorldInteriorStorage
    {
        private ILogger<WorldInteriorStorage> Logger { get; }
        private WorldInteriorSeed Seed { get; }

        public WorldInteriorStorage(ILogger<WorldInteriorStorage> InLogger, WorldInteriorSeed InSeed)
        {
            Logger = InLogger;
            Seed = InSeed;
        }


        public async Task Initialize(GameWorldDbContext InDbContext)
        {
            await Seed.Initialize(InDbContext);

            Logger.LogInformation("Begin Initialize");

            var interiors = await InDbContext.InteriorEntity
                .HasTracking(hasTracking: false)
                .Include(q => q.ActionEntities)
                .ToArrayAsync();

            Logger.LogInformation($"Fetched {interiors.Length} interiors");

            foreach (var entity in interiors)
            {
                Put(new WorldInterior(entity));
            }

            Logger.LogInformation($"Loaded {Items.Count} interiors");
            Logger.LogInformation("End Initialize");
        }
    }
}