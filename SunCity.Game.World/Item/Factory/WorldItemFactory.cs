﻿using System;
using System.Collections.Generic;
using System.Linq;
using SunCity.Game.Enums.Item;
using SunCity.Game.World.DataBase.Entitites.Item.World;
using SunCity.Game.World.Item.Category;
using SunCity.Game.World.ItemModels;
using SunCity.Game.World.Models.Item.World;

namespace SunCity.Game.World.Item.Factory
{
    internal class WorldItemFactory : IWorldItemFactory
    {
        private IGameItemStorage GameItemStorage { get; }

        public WorldItemFactory(IGameItemStorage InGameItemStorage)
        {
            GameItemStorage = InGameItemStorage;
        }

        public WorldItem Factory(WorldItemModel InItemModel, WorldItem InParrentItem = null)
        {
            var gameItemModel = GameItemStorage[InItemModel.ModelId];
            if (gameItemModel.ItemCategory == ItemCategory.PropertyKey)
            {
                return new WorldItemPropertyKey(InParrentItem, gameItemModel, InItemModel, new PropertyId(InItemModel.Data.Guid.Value));
            }

            return new WorldItem(InParrentItem, gameItemModel, InItemModel, FactoryChildItems);
        }

        public IList<WorldItem> FactoryChildItems(WorldItem InParrentItem, WorldItemModel InItemModel)
        {
            return InItemModel.Child.Select(item => Factory(item, InParrentItem)).ToList();
        }

        public WorldItem CreateVehicleKey(VehicleId InVehicleId)
        {
            return Factory(new WorldItemModel
            {
                EntityId = WorldItemId.GenerateId(),
                Group = WorldItemGroup.Player,
                CreatedDate = DateTime.UtcNow,
                ModelId = GameItemStorage.VehicleKey.EntityId,
                Data = new WorldItemData
                {
                    Guid = InVehicleId.Id
                }
            });
        }

        public WorldItem CreatePropertyKey(PropertyId InPropertyId)
        {
            return Factory(new WorldItemModel
            {
                EntityId = WorldItemId.GenerateId(),
                Group = WorldItemGroup.Player,
                CreatedDate = DateTime.UtcNow,
                ModelId = GameItemStorage.PropertyKey.EntityId,
                Data = new WorldItemData
                {
                    Guid = InPropertyId.Id
                }
            });
        }
    }
}