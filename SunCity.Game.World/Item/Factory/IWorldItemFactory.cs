﻿using System.Collections.Generic;
using SunCity.Game.World.Models.Item.World;

namespace SunCity.Game.World.Item.Factory
{
    public interface IWorldItemFactory
    {
        WorldItem Factory(WorldItemModel InItemModel, WorldItem InParentItem = null);
        IList<WorldItem> FactoryChildItems(WorldItem InParentItem, WorldItemModel InItemModel);

        
        WorldItem CreateVehicleKey(VehicleId InVehicleId);
        WorldItem CreatePropertyKey(PropertyId InPropertyId);
    }
}