﻿using System;
using System.Collections.Generic;
using SunCity.Game.World.ItemModels;
using SunCity.Game.World.Models.Item.World;
using SunCity.Game.World.Player.Character.Components.Inventory;

namespace SunCity.Game.World.Item
{
    /// <summary>
    /// <para>Items eg Clothes, weapons</para>
    /// <para>Root items in inventory</para>
    /// </summary>
    public class WorldPlayerItem : WorldItem
    {
        public CharacterInventoryItem Inventory { get; }

        internal WorldPlayerItem(CharacterInventoryItem InCharacterInventoryItem, WorldItem InWorldItem): base(null, InWorldItem)
        {
            Inventory = InCharacterInventoryItem;
        }

        public WorldPlayerItem(CharacterInventoryItem InCharacterInventoryItem, GameItem InGameItemModel, WorldItemModel InItemModel, Func<WorldItem, WorldItemModel, IList<WorldItem>> InStorageItems)
            : base(null, InGameItemModel, InItemModel, InStorageItems)
        {
            Inventory = InCharacterInventoryItem;
        }
    }
}