﻿using SunCity.Game.Types.Storage;

namespace SunCity.Game.World.Item
{
    public interface IWorldItemStorage : IWorldStorage<WorldItemId, WorldItem>
    {

    }
}