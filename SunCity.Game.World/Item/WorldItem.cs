﻿using System;
using System.Collections.Generic;
using SunCity.Domain.Interfaces.Entity;
using SunCity.Game.World.ItemModels;
using SunCity.Game.World.Models.Item.World;

namespace SunCity.Game.World.Item
{
    public class WorldItem : IEntityWithId<WorldItemId>
    {
        public WorldItemId EntityId { get; }
        public GameItem ItemModel { get; }

        //===================================
        public IList<WorldItem> Storage { get; }
        public WorldItem Parrent { get; set; }

        //===================================
        public DateTime CreatedDate { get; set; }

        //===================================
        internal WorldItem(WorldItem InParrentItem, WorldItem InCopyFrom)
        {
            EntityId    = InCopyFrom.EntityId;
            CreatedDate = InCopyFrom.CreatedDate;

            Parrent = InParrentItem;

            ItemModel = InCopyFrom.ItemModel;
            Storage = InCopyFrom.Storage;
        }

        public WorldItem(WorldItem InParrentItem, GameItem InGameItemModel, WorldItemModel InItemModel, Func<WorldItem, WorldItemModel, IList<WorldItem>> InStorageItems)
        {
            EntityId    = InItemModel.EntityId;
            CreatedDate = InItemModel.CreatedDate;

            Parrent = InParrentItem;

            ItemModel = InGameItemModel;
            Storage = InStorageItems?.Invoke(this, InItemModel) ?? Array.Empty<WorldItem>();
        }

        //===================================
        public void RemovedFromParrent()
        {
            Parrent.Storage.Remove(this);
        }

        public bool PutItem(WorldItem InWorldItem)
        {
            if(ItemModel.Capacity > Storage.Count)
            {
                InWorldItem.Parrent = this;
                Storage.Add(InWorldItem);
                return true;
            }

            return false;
        }

        //===================================
        public override string ToString()
        {
            return $"{ItemModel.Name} | {ItemModel.ItemCategory} | {EntityId} | Storage: {Storage.Count}";
        }
    }
}
