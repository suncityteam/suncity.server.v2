﻿using SunCity.Game.World.ItemModels;
using SunCity.Game.World.Models.Item.World;

namespace SunCity.Game.World.Item.Category
{
    public class WorldItemPropertyKey : WorldItem
    {
        public PropertyId PropertyId { get; }

        public WorldItemPropertyKey(WorldItem InParrentItem, GameItem InGameItemModel, WorldItemModel InItemModel, PropertyId InPropertyId) 
            : base(InParrentItem, InGameItemModel, InItemModel, null)
        {
            PropertyId = InPropertyId;
        }
    }
}