﻿using SunCity.Game.World.Common.Components;

namespace SunCity.Game.World.Player.Data
{
    public class WorldPlayerData<TValue> : BaseEntitySharedData<WorldPlayer, TValue>
    {
        public override TValue Value
        {
            set
            {
                base.Value = value;
                Owner.RagePlayer.SetSharedData(Name, value);
            }
        }

        public WorldPlayerData(WorldPlayer InPlayer, string InName) : base(InPlayer, InName)
        {

        }
    }
}