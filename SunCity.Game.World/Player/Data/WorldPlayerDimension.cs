﻿
namespace SunCity.Game.World.Player.Data
{
    public class WorldPlayerDimension : WorldPlayerComponent
    {
        internal uint Dimension { get; private set; }
        internal WorldPlayerDimension(WorldPlayer InPlayer) : base(InPlayer)
        {
        }

        /// <summary>
        /// <para>Synchronous Dimension Assignment Method</para>
        /// <para>Use <see cref="IRageMpUtility.Schedule"/> for optimal perfomance</para>
        /// </summary>
        /// <param name="InDimension"></param>
        public void SetDimension(uint InDimension)
        {
            Dimension = InDimension;
            Player.RagePlayer.Dimension=InDimension;
        }

        public uint GetRealDimension() => Player.RagePlayer.Dimension;


    }
}
