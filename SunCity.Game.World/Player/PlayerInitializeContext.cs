﻿using System;
using Microsoft.Extensions.DependencyInjection;

namespace SunCity.Game.World.Player
{
    public class PlayerInitializeContext<TForm>
    {
        public IServiceProvider ServiceProvider { get; }
        public TForm Form { get; }

        public TService GetService<TService>() => ServiceProvider.GetService<TService>();


        public PlayerInitializeContext(IServiceProvider InServiceProvider, TForm InForm)
        {
            ServiceProvider = InServiceProvider;
            Form            = InForm;
        }
    }
}