﻿using System;
using System.Threading.Tasks;

namespace SunCity.Game.World.Player.AntiCheat
{
    internal sealed class DimensionPlayerAntiCheat : BasePlayerAnticheat
    {
        private static readonly TimeSpan Delay = TimeSpan.FromSeconds(1);
        public DimensionPlayerAntiCheat(WorldPlayer InPlayer) : base(InPlayer, Delay)
        {

        }

        protected override async Task Process()
        {
            var validationFirstResult = await GetAndValidateDimension();
            if (validationFirstResult.Item1)
                return;

            await Task.Delay(150);

            var validationSecondResult = await GetAndValidateDimension();
            if (validationSecondResult.Item1)
                return;


        }

        private async Task<(bool, uint)> GetAndValidateDimension()
        {
            var currentDimension = Player.Dimension.Dimension;
            var actualDimension = Player.Dimension.GetRealDimension();
            var isValidDimension = Player.Dimension.Dimension == currentDimension || Player.Dimension.Dimension == actualDimension;
            return (isValidDimension, currentDimension);
        }
    }
}
