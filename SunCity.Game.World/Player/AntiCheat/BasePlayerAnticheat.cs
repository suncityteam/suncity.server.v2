﻿using System;
using System.Threading.Tasks;

namespace SunCity.Game.World.Player.AntiCheat
{
    internal abstract class BasePlayerAnticheat : WorldPlayerComponent
    {
        private Task ProcessTask { get; }
        private TimeSpan ProtectionDelay { get; }

        protected BasePlayerAnticheat(WorldPlayer InPlayer, TimeSpan InProtectionDelay) : base(InPlayer)
        {
            ProtectionDelay = InProtectionDelay;
            ProcessTask = Task.Factory.StartNew(ProcessProxy);
        }

        private async Task ProcessProxy()
        {
            while (Player?.RagePlayer != null && Player.RagePlayer.Exists)
            {
                await Process();
                await Task.Delay(ProtectionDelay);
            }
        }

        protected abstract Task Process();
    }
}
