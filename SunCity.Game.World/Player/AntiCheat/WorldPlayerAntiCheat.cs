﻿namespace SunCity.Game.World.Player.AntiCheat
{
    internal sealed class WorldPlayerAntiCheat : WorldPlayerComponent
    {
        private DimensionPlayerAntiCheat DimensionAntiCheat { get; }
        private HealthPlayerAntiCheat HealthAntiCheat { get; }

        public WorldPlayerAntiCheat(WorldPlayer InPlayer) : base(InPlayer)
        {
            HealthAntiCheat = new HealthPlayerAntiCheat(InPlayer);
            DimensionAntiCheat = new DimensionPlayerAntiCheat(InPlayer);
        }
    }
}