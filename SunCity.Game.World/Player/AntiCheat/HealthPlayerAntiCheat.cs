﻿using System;
using System.Threading.Tasks;
using SunCity.Game.World.Extensions.Player;

namespace SunCity.Game.World.Player.AntiCheat
{
    internal sealed class HealthPlayerAntiCheat : BasePlayerAnticheat
    {
        private static readonly TimeSpan Delay = TimeSpan.FromSeconds(1);
        public HealthPlayerAntiCheat(WorldPlayer InPlayer) : base(InPlayer, Delay)
        {

        }

        protected override async Task Process()
        {
            var actualHealth = Player.RagePlayer.Health;
            if (actualHealth > 100)
            {
                Player.SendClientMessage("Вы были кикнуты системой по подозрению в читерстве [Код:0001]");
                Player.RagePlayer.Kick("Вы были кикнуты системой по подозрению в читерстве [Код:0001]");
            }

        }
    }
}