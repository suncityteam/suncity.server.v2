﻿
using SunCity.Common.RageMp.Pools.Vehicles;

namespace SunCity.Game.World.Player
{
    public class WorldPlayerBeltState : WorldPlayerComponent
    {
        public bool Enabled { get; private set; }
        public IRageVehicle Vehicle { get; private set; }

        public WorldPlayerBeltState(WorldPlayer InPlayer) : base(InPlayer)
        {
        }

        public void UseBelt(IRageVehicle InVehicle)
        {
            Vehicle = InVehicle;
            Enabled = true;
        }

        public void RemoveBelt()
        {
            Enabled = false;
            Vehicle = null;
        }
    }
}