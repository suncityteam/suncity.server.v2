﻿using System;
using System.Threading.Tasks;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using SunCity.Common.RageMp.Pools;
using SunCity.Common.RageMp.Pools.Players;
using SunCity.Game.Types.Storage;
using SunCity.Game.World.Events.Client.Player.HUD;
using SunCity.Game.World.Forms.Character;
using SunCity.Game.World.Property;

namespace SunCity.Game.World.Player
{
    //  https://github.com/Micky5991/VehicleInfoLoader
    public interface IWorldPlayerStorage : IWorldStorage<PlayerCharacterId, WorldPlayer>
    {
        WorldPlayer this[IRagePlayer InPlayer] { get; }


        void JoinToWorld(WorldJoinCharacterForm InForm, WorldProperty InProperty);
        bool TryGetPlayer(IRagePlayer InPlayer, out WorldPlayer InWorldPlayer);
    }

    public sealed class WorldPlayerStorage : WorldStorage<PlayerCharacterId, WorldPlayer>, IWorldPlayerStorage
    {
        private ILogger<WorldPlayerStorage> Logger { get; }
        private IServiceProvider ServiceProvider { get; }

        public WorldPlayerStorage
        (
            ILogger<WorldPlayerStorage> InLogger,
            IServiceProvider InServiceProvider
        )
        {
            Logger = InLogger;
            ServiceProvider = InServiceProvider;
        }

        public WorldPlayer this[IRagePlayer InPlayer]
        {
            get
            {
                TryGetPlayer(InPlayer, out var worldPlayer);
                return worldPlayer;
            }
        }

        public void JoinToWorld(WorldJoinCharacterForm InForm, WorldProperty InProperty)
        {
            var world = ServiceProvider.GetRequiredService<IGameWorld>();
            var player = Put(new WorldPlayer(world, InForm).Initialize(new PlayerInitializeContext<WorldJoinCharacterForm>(ServiceProvider, InForm)));
            if (InProperty == null)
            {
                player.RagePlayer.Dimension = RageMpConsts.GlobalDimension;
            }
            else
            {
                player.OnEnterProperty(InProperty);
            }

            player.RagePlayer.TogglePlayerHud(true);
        }

        public bool TryGetPlayer(IRagePlayer InPlayer, out WorldPlayer InWorldPlayer)
        {
            var characterId = InPlayer.GetData<PlayerCharacterId>(WorldPlayerConsts.CharacterId);
            InWorldPlayer = characterId.IsValid ? this[characterId] : null;

            return characterId.IsValid;
        }
    }
}