﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using SunCity.Common.Extensions;
using SunCity.Game.World.Common.Process;
using SunCity.Game.World.DataBase;
using SunCity.Game.World.DataBase.Entitites.Player.Character;

namespace SunCity.Game.World.Player.Process
{
    public class SaveWorldPlayerStorageProcess : GameWorldProcess
    {
        private IWorldPlayerStorage PlayerStorage { get; }
        public SaveWorldPlayerStorageProcess(IWorldPlayerStorage InPlayerStorage, IServiceProvider InServiceProvider): base(InServiceProvider)
        {
            PlayerStorage = InPlayerStorage;
        }

        protected override async Task OnProcess()
        {
            while (true)
            {
                await Task.Delay(5000);

                using var scope = CreateScope();
                var dbContext = scope.ServiceProvider.GetService<GameWorldDbContext>();
                await SaveStorageProcess(dbContext);
            }
        }

        private async Task SaveStorageProcess(GameWorldDbContext InDbContext)
        {
            var avalibalePlayers = PlayerStorage.ValuesCopy.Where(IsValidPlayer).ToArray();
            foreach (var batch in avalibalePlayers.Batch(10))
            {
                var playersArr = batch.ToArray();
                var characters = await GetCharacters(InDbContext, playersArr);

                foreach (var player in playersArr)
                {
                    if (characters.TryGetValue(player.EntityId, out var character))
                    {
                        await player.OnSaveChanges(character);
                    }
                }
                await InDbContext.SaveChangesAsync();
            }

            await InDbContext.SaveChangesAsync();
        }

        private Task<Dictionary<PlayerCharacterId, PlayerCharacterEntity>> GetCharacters(GameWorldDbContext InDbContext, WorldPlayer[] InPlayers)
        {
            var characterIds = InPlayers.Select(q => q.Character.EntityId).ToArray();
            return InDbContext.PlayerCharacterEntity
                   .Where(q => characterIds.Contains(q.EntityId))
                   .ToDictionaryAsync(q => q.EntityId, q => q);
        }


        private static bool IsValidPlayer(WorldPlayer InWorldPlayer)
        {
            if (InWorldPlayer.RagePlayer == null || InWorldPlayer.RagePlayer.Exists == false)
                return false;

            return true;
        }


    }
}
