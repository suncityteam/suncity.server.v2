﻿using SunCity.Game.Enums.Character.Components;
using SunCity.Game.World.Player.Data;

namespace SunCity.Game.World.Player
{
    public abstract class WorldPlayerComponent<TValue> : WorldPlayerComponent
    {
        protected GameCharacterComponent Component { get; }
        protected WorldPlayerData<TValue> Value { get; }
        public short CurrentLevel { get; set; }

        protected WorldPlayerComponent(WorldPlayer InPlayer, GameCharacterComponent InComponent) : base(InPlayer)
        {
            Component = InComponent;
            Value = new WorldPlayerData<TValue>(InPlayer, InComponent.ToString());
        }
    }

    public abstract class WorldPlayerComponent
    {
        protected WorldPlayer Player { get; }

        protected WorldPlayerComponent(WorldPlayer InPlayer)
        {
            Player = InPlayer;
        }

    }
}