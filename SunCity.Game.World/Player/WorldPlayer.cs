﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using GTANetworkAPI;
using JetBrains.Annotations;
using SunCity.Common.RageMp.Pools;
using SunCity.Common.RageMp.Pools.Players;
using SunCity.Common.Types.Extensions;
using SunCity.Domain.Interfaces.Entity;
using SunCity.Game.Types.Player.Components;
using SunCity.Game.Types.Player.Entity.Player.Character;
using SunCity.Game.Types.Property.Entity.Interior;
using SunCity.Game.World.DataBase.Entitites.Player.Character;
using SunCity.Game.World.Enums.Chat;
using SunCity.Game.World.Extensions.Chat;
using SunCity.Game.World.Extensions.Player;
using SunCity.Game.World.Forms.Character;
using SunCity.Game.World.Forms.Character.Inventory;
using SunCity.Game.World.Interfaces.Character.Components.Vitals;
using SunCity.Game.World.Models.Account;
using SunCity.Game.World.Models.Character;
using SunCity.Game.World.Models.Chat;
using SunCity.Game.World.Player.AntiCheat;
using SunCity.Game.World.Player.Character;
using SunCity.Game.World.Player.Character.Components.Inventory;
using SunCity.Game.World.Player.Character.Components.Vital;
using SunCity.Game.World.Player.Character.Documents;
using SunCity.Game.World.Player.Data;
using SunCity.Game.World.Property;

namespace SunCity.Game.World.Player
{
    public sealed class WorldPlayer
        : IEntityWithId<PlayerCharacterId>
            , IEntityWithName<PlayerCharacterName>
    {
        private IGameWorld World { get; }
        
        public IRagePlayer RagePlayer { get; }
        public WorldPlayerCharacter Character { get; }
        public WorldPlayerVehicle Vehicle { get; }
        
        public PlayerCharacterId EntityId => Character.EntityId;
        public PlayerCharacterName Name => Character.Name;

        public PlayerAccountProperties Account { get; }
        public CharacterInventoryComponent Inventory { get; }
        
        public Vector3 Position
        {
            get => RagePlayer.Position;
            set => RagePlayer.Position = value;
        }

        public Vector3 Rotation
        {
            get => RagePlayer.Rotation;
            set => RagePlayer.Rotation = value;
        }

        public WorldPlayerDimension Dimension { get; }
        private WorldPlayerAntiCheat AntiCheat { get; }

        public IWorldPlayerLicenses Licenses { get; }
        public DateTime LastSaveDate { get; private set; }

        public WorldPlayer(IGameWorld world, WorldJoinCharacterForm InForm)
        {
            World = world;
            Vehicle = new WorldPlayerVehicle(this);

            //----------------------------
            Character = new WorldPlayerCharacter(this);
            Licenses = new WorldPlayerLicenses(this);

            //----------------------------
            Inventory = new CharacterInventoryComponent(this);

            //----------------------------
            RagePlayer = InForm.RagePlayer;

            //----------------------------
            Account = InForm.Account;

            //----------------------------
            Dimension = new WorldPlayerDimension(this);
            AntiCheat = new WorldPlayerAntiCheat(this);
        }


        public WorldPlayer Initialize(PlayerInitializeContext<WorldJoinCharacterForm> InContext)
        {
            //----------------------------
            Character.Initialize(InContext);
            Inventory.Initialize(new PlayerInitializeContext<LoadCharacterInventory>(InContext.ServiceProvider, InContext.Form.Inventory));
            
            //----------------------------
            LastSaveDate = DateTime.Now;
            
            //----------------------------
            RagePlayer.SetSharedData(WorldPlayerConsts.IsAuthorized, true);
            
            //----------------------------
            World.Server.TaskManager.Schedule(() =>
            {
                Position = InContext.Form.Position.Position;
                Rotation = InContext.Form.Position.Rotation.AsVector3Z();
                RagePlayer.ApplyCustomization(InContext.Form.Skin);
                RagePlayer.Spawn(Position, Rotation.Z);
            });

            return this;
        }

        public void GiveMoney(long InAmount)
        {
            if (RagePlayer != null)
            {
                Character.Money.Value += InAmount;
            }
        }

        public void OnEnterProperty(WorldProperty InProperty)
        {
            //===========================================
            World.Server.TaskManager.Schedule(() =>
            {
                Dimension.SetDimension(InProperty.VirtualWorld);

                if (InProperty.HasInterior)
                {
                    RequestIpl(InProperty.Interior.IPL);

                    EnteredProperty = InProperty;
                    Position = InProperty.Interior.Position;
                    Rotation = InProperty.Interior.Rotation.AsVector3Z();
                }
            });
        }

        public void OnExitProperty(WorldProperty InProperty)
        {
            if (InProperty.HasInterior)
            {
                RemoveIpl(InProperty.Interior.IPL);
            }

            EnteredProperty = null;
            ExitedProperty = InProperty;

            World.Server.TaskManager.Schedule(() =>
            {
                Position = InProperty.Position;
                Rotation = InProperty.Rotation.AsVector3Z();
                Dimension.SetDimension(RageMpConsts.GlobalDimension);
            });
        }

        public bool IsDead => Character.Health.Get() < 1;

        //=================================
        [CanBeNull]
        public WorldProperty EnteredProperty { get; private set; }

        [CanBeNull]
        public WorldProperty ExitedProperty { get; private set; }

        //=================================
        public void RequestIpl(InteriorItemPlacement InItemPlacements) => RagePlayer.RequestIpl(InItemPlacements);
        public void RemoveIpl(InteriorItemPlacement InItemPlacements) => RagePlayer.RemoveIpl(InItemPlacements);

        //=================================
        public void SendClientMessage(string InChatMessage)
        {
            World.Server.TaskManager.Schedule(() => RagePlayer.SendChatMessage(InChatMessage));
        }

        public void SendClientMessage(IReadOnlyList<string> InChatMessage)
        {
            World.Server.TaskManager.Schedule(() =>
            {
                foreach (var message in InChatMessage)
                {
                    RagePlayer.SendChatMessage(message);
                }
            });
        }

        //=================================
        public Task SendMessageToNearbyPlayers(float InRange, WorldChatMessaveRecive InRecive, params string[] InChatMessage)
        {
            return World.Chat.SendMessageToNearbyPlayers(this, new WorldChatMessage(InChatMessage, InRecive, InRange));
        }
        
        public Task SendMessageToNearbyPlayers(float InRange, WorldChatMessage message)
        {
            return World.Chat.SendMessageToNearbyPlayers(this, message);
        }
        
        public Task OnSaveChanges(PlayerCharacterEntity InCharacterEntity)
        {
            //========================================
            LastSaveDate = DateTime.Now;

            //========================================
            InCharacterEntity.EnteredPropertyId = EnteredProperty?.EntityId;
            InCharacterEntity.Position = Position;
            InCharacterEntity.Rotation = Rotation.Z;

            //========================================
            InCharacterEntity.Health = new Health(Character.Health.Get());
            InCharacterEntity.Hunger = new Hunger(Character.Hunger.Get());
            InCharacterEntity.Thirst = new Hunger(Character.Thirst.Get());

            //========================================
            return Task.CompletedTask;
        }
    }
}