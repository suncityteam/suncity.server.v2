﻿namespace SunCity.Game.World.Player
{
    public sealed class WorldPlayerConsts
    {
        public static string CharacterId { get; } = nameof(CharacterId);
        public static string IsAuthorized { get; } = nameof(IsAuthorized);
    }
}