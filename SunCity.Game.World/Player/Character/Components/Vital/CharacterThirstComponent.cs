﻿using SunCity.Game.Enums.Character.Components;

namespace SunCity.Game.World.Player.Character.Components.Vital
{
    public class CharacterThirstComponent : CharacterVitalComponent
    {
        public CharacterThirstComponent(WorldPlayer InPlayer)
            : base(InPlayer, GameCharacterComponent.Thirst)
        {
        }
    }
}