﻿using SunCity.Game.Enums.Character.Components;

namespace SunCity.Game.World.Player.Character.Components.Vital
{
    public class CharacterHungerComponent : CharacterVitalComponent
    {
        public CharacterHungerComponent(WorldPlayer InPlayer) 
            : base(InPlayer, GameCharacterComponent.Hunger)
        {
        }
    }
}
