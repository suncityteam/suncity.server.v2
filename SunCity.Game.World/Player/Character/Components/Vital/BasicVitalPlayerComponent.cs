﻿using System;
using System.Threading.Tasks;
using SunCity.Game.Enums.Character.Components;
using SunCity.Game.World.Interfaces.Character.Components.Vitals;
using SunCity.Game.World.Settings.Character.Components.Vitals;

namespace SunCity.Game.World.Player.Character.Components.Vital
{
    public abstract class CharacterVitalComponent : WorldPlayerComponent<int>, ICharacterVitalComponent
    {
        public TimeSpan Interval { get; protected set; } = TimeSpan.FromMinutes(1);

        public OnCharacterVitalDamageDelegate OnWarning { get; set; }
        public OnCharacterVitalDamageDelegate OnCritical { get; set; }

        public DateTime NextCheckDate { get; private set; }

        public CharacterVitalProperties Level { get; set; }

        protected CharacterVitalComponent(WorldPlayer InPlayer, GameCharacterComponent InModelId)
            : base(InPlayer, InModelId)
        {
        }

        protected virtual async Task OnProcessProxy()
        {
            //==============================
            Value.Value = Math.Clamp(Value.Value + Level.Give, Level.Min, Level.Max);

            //==============================
            if (Value.Value <= Level.Critical)
            {
                await OnCritical.Invoke(Level.DamageOnCritical);
            }
            else if (Value.Value <= Level.Warning)
            {
                await OnWarning.Invoke(Level.DamageOnWarning);
            }

            //==============================
            NextCheckDate = DateTime.UtcNow.Add(Interval);
        }

        public int Get()
        {
            return Value.Value;
        }

        public virtual void Set(int InAmount)
        {
            Value.Value = InAmount;
        }

        public void Give(int InAmount)
        {
            Value.Value += InAmount;

        }

        public virtual async Task OnProcess()
        {
            if (DateTime.UtcNow >= NextCheckDate)
            {
                await OnProcessProxy();
            }
        }
    }
}