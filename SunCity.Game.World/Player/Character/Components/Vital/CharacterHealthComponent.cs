﻿using System.Threading.Tasks;
using SunCity.Game.Enums.Character.Components;

namespace SunCity.Game.World.Player.Character.Components.Vital
{
    public class CharacterHealthComponent : CharacterVitalComponent
    {
        public CharacterHealthComponent(WorldPlayer InPlayer)
            : base(InPlayer, GameCharacterComponent.Health)
        {
            Task.Factory.StartNew(OnHealthCheck, TaskCreationOptions.LongRunning);
        }

        private async Task OnHealthCheck()
        {
            while (Player.RagePlayer != null && Player.RagePlayer.Exists)
            {
                var actualHealth = Player.RagePlayer.Health;
                if (actualHealth != Value.Value)
                {
                    base.Set(actualHealth);
                }

                await Task.Delay(250);
            }
        }

        public override void Set(int InAmount)
        {
            base.Set(InAmount);
            Player.RagePlayer.Health = InAmount;
        }
    }
}