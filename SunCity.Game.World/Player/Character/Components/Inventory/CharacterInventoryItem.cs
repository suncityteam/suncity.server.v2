﻿using System.Collections.Generic;
using SunCity.Common.Operation;
using SunCity.Domain.Interfaces.Entity;
using SunCity.Domain.Interfaces.Extensions;
using SunCity.Game.Enums.World.Player.Inventory;
using SunCity.Game.World.Forms.Character.Inventory;
using SunCity.Game.World.Item;
using SunCity.Game.World.ItemModels;
using SunCity.Game.World.Models.Item.World;

namespace SunCity.Game.World.Player.Character.Components.Inventory
{
    public abstract class CharacterInventoryItem
        : WorldPlayerComponent
        , IEntityWithId<CharacterInventoryItemId>
    {
        //==================================
        public CharacterInventoryItemId EntityId { get; }
        public PlayerInventorySlot InventorySlot { get; }

        //==================================
        public WorldPlayerItem Item { get; private set; }
        public GameItem ItemModel => Item?.ItemModel;
        public IList<WorldItem> Items => Item.Storage;

        //==================================

        internal CharacterInventoryItem(WorldPlayer InPlayer, LoadCharacterInventoryItemForm InForm) : base(InPlayer)
        {
            EntityId = InForm.EntityId;
            InventorySlot = InForm.InventorySlot;
        }

        internal CharacterInventoryItem(WorldPlayer InPlayer, PlayerInventorySlot InSlot) : base(InPlayer)
        {
            EntityId      = CharacterInventoryItemId.GenerateId();
            InventorySlot = InSlot;
        }

        public CharacterInventoryItem Intialize(WorldItemModel InModel, GameItemFactoryContext InGameItemFactoryContext)
        {
            var model = InGameItemFactoryContext.GameItemStorage[InModel.ModelId];
            Item = new WorldPlayerItem(this, model, InModel, InGameItemFactoryContext.WorldItemFactory.FactoryChildItems);

            return this;
        }

        public OperationResponseCode AllowPutItem(WorldItem InItem)
        {
            if (ItemModel.Capacity <= Items.Count)
                return OperationResponseCode.NotEnoughCapacity;

            return OperationResponseCode.Successfully;
        }

        public WorldItem GetItemById(WorldItemId InItemId)
        {
            return Items.GetById(InItemId);
        }

        public OperationResponseCode RemoveItem(WorldItem InItem)
        {
            Items.Remove(InItem);
            return OperationResponseCode.Successfully;
        }


        public OperationResponseCode PutItem(WorldItem InItem)
        {
            //------------------------------------------
            var allowPutItem = AllowPutItem(InItem);
            if (allowPutItem == OperationResponseCode.Successfully)
            {
                //------------------------------------------
                //  Привязываем предмет к слоту (к предмету) 
                InItem.Parrent = Item;
                Items.Add(InItem);

                //------------------------------------------
                return OperationResponseCode.Successfully;
            }

            //------------------------------------------
            return allowPutItem;
        }

        //==================================
        public virtual void Equip(WorldItem InItem)
        {
            Item = new WorldPlayerItem(this, InItem);
            OnItemEquiped();
        }

        protected abstract void OnItemEquiped();
        protected abstract void OnItemUnequiped();

        public override string ToString()
        {
            return $"{ItemModel.Name} | {ItemModel.ItemCategory} | Slot: {InventorySlot} | EntityId: {EntityId} | Item: {Item.EntityId} | Items: {Items.Count}";
        }
    }
}