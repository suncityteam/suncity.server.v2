﻿using System;
using System.Collections.Generic;
using System.Linq;
using SunCity.Common.Operation;
using SunCity.Game.Enums.Item;
using SunCity.Game.Enums.World.Player.Inventory;
using SunCity.Game.World.Forms.Character.Inventory;
using SunCity.Game.World.Item;
using SunCity.Game.World.Item.Factory;
using SunCity.Game.World.ItemModels;
using SunCity.Game.World.ItemModels.Category;

public class InventroryItemEquipForm
{
    public WorldItemId ItemId { get; set; }
    public WorldItemId FromId { get; set; }
}

public class InventroryItemMoveForm
{
    public WorldItemId ItemId { get; set; }
    public WorldItemId FromId { get; set; }
    public WorldItemId TargetId { get; set; }
}

namespace SunCity.Game.World.Player.Character.Components.Inventory
{
    public class CharacterInventoryComponent : WorldPlayerComponent
    {
        public IReadOnlyCollection<CharacterInventoryItem> ItemSlots => Slots.Values;
        private Dictionary<PlayerInventorySlot, CharacterInventoryItem> Slots { get; } = new Dictionary<PlayerInventorySlot, CharacterInventoryItem>();
        public CharacterInventoryComponent(WorldPlayer InPlayer) : base(InPlayer)
        {
        }

        public CharacterInventoryItem GetSlot(WorldItemId InSlotItemId)
        {
            return ItemSlots.FirstOrDefault(q => q.Item.EntityId == InSlotItemId);
        }

        public OperationResponse MoveItem(InventroryItemMoveForm InItemMoveForm)
        {
            //------------------------------------------
            var from = GetSlot(InItemMoveForm.FromId);
            if (from == null)
                return (OperationResponseCode.StorageItemNotFound, InItemMoveForm.FromId);

            //------------------------------------------
            var target = GetSlot(InItemMoveForm.TargetId);
            if (target == null)
                return (OperationResponseCode.TargetItemNotFound, InItemMoveForm.FromId);

            //------------------------------------------
            var item = from.GetItemById(InItemMoveForm.ItemId);
            if(item == null)
                return (OperationResponseCode.ItemNotFound, InItemMoveForm.ItemId);

            //------------------------------------------
            var putItem = target.PutItem(item);
            if (putItem != OperationResponseCode.Successfully)
                return putItem;

            var removeItem = from.RemoveItem(item);
            if (removeItem != OperationResponseCode.Successfully)
                return removeItem;

            //------------------------------------------
            return OperationResponseCache.Successfully;
        }

        //=====================================
        public OperationResponse PutItem(WorldItem InItem, PlayerInventorySlot? exclude = null)
        {
            //--------------------------
            var lastOperationResponseCode = OperationResponseCode.UnknownError;

            //--------------------------
            foreach (var (key, slot) in Slots)
            {
                if (exclude != key)
                {
                    lastOperationResponseCode = slot.PutItem(InItem);
                    if (lastOperationResponseCode == OperationResponseCode.Successfully)
                    {
                        return OperationResponseCache.Successfully;
                    }
                }
            }

            //--------------------------
            return lastOperationResponseCode;
        }

        //=====================================
        private OperationResponse EquipClothing(WorldItem InWorldItem)
        {
            var model = (GameClothingItem)InWorldItem.ItemModel;
            if (Slots.ContainsKey(model.Slot))
            {
                var slot = Slots[model.Slot];
                if (!InWorldItem.PutItem(slot.Item))
                {
                    var putItem = PutItem(slot.Item, model.Slot);
                    if (putItem.IsNotCorrect)
                        return putItem;
                }

                slot.Equip(InWorldItem);
            }
            else
            {
                var slot = new CharacterClotheInventoryItem(Player, model.Slot);
                slot.Equip(InWorldItem);
                Slots.Add(model.Slot, slot);
            }

            InWorldItem.RemovedFromParrent();

            return OperationResponseCache.Successfully;
        }

        private OperationResponse EquipWeapon(WorldItem InWorldItem)
        {
            throw new NotImplementedException($"Weapon not supported");
        }

        public OperationResponse Equip(InventroryItemEquipForm InEquipForm)
        {
            //------------------------------------------
            var from = GetSlot(InEquipForm.FromId);
            if (from == null)
                return (OperationResponseCode.StorageItemNotFound, InEquipForm.FromId);

            //------------------------------------------
            var item = from.GetItemById(InEquipForm.ItemId);
            if (item == null)
                return (OperationResponseCode.ItemNotFound, InEquipForm.ItemId);

            //------------------------------------------
            return Equip(item);
        }

        public OperationResponse Equip(WorldItem InWorldItem)
        {
            var model = InWorldItem.ItemModel;
            if (model.ItemCategory == ItemCategory.Clothing)
            {
                return EquipClothing(InWorldItem);
            }
            
            if (model.ItemCategory == ItemCategory.Weapon)
            {
                return EquipWeapon(InWorldItem);
            }

            return OperationResponseCode.ItemCategoryEquipNotAllowed;
        }

        internal void Initialize(PlayerInitializeContext<LoadCharacterInventory> InContext)
        {
            var itemStorage = InContext.GetService<IGameItemStorage>();
            var itemFactory = InContext.GetService<IWorldItemFactory>();

            var content = new GameItemFactoryContext(itemFactory, itemStorage);
            foreach (var slot in InContext.Form.Slots)
            {
                if (slot.InventorySlot == PlayerInventorySlot.LeftHand || slot.InventorySlot == PlayerInventorySlot.RightHand)
                {
                    throw new NotImplementedException();
                }
                else
                {
                    Slots.Add(slot.InventorySlot, new CharacterClotheInventoryItem(Player, slot).Intialize(slot.Item, content));
                }
            }
        }
    }
}
