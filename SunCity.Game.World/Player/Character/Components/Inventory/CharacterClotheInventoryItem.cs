﻿using SunCity.Game.Enums.World.Player.Inventory;
using SunCity.Game.World.Forms.Character.Inventory;
using SunCity.Game.World.ItemModels.Category;

namespace SunCity.Game.World.Player.Character.Components.Inventory
{
    public class CharacterClotheInventoryItem : CharacterInventoryItem
    {
        public new GameClothingItem ItemModel => Item?.ItemModel as GameClothingItem;

        internal CharacterClotheInventoryItem(WorldPlayer InPlayer, LoadCharacterInventoryItemForm InForm) : base(InPlayer, InForm)
        {
        }

        internal CharacterClotheInventoryItem(WorldPlayer InPlayer, PlayerInventorySlot InSlot) : base(InPlayer, InSlot)
        {
        }

        protected override void OnItemEquiped()
        {
            //Player.RagePlayer.SetCloth(ItemModel.ClothSlot, ItemModel.ClothData);
        }

        protected override void OnItemUnequiped()
        {
            throw new System.NotImplementedException();
        }
    }
}