﻿using SunCity.Domain.Interfaces.Entity;
using SunCity.Game.Types.Player.Components;
using SunCity.Game.Types.Player.Entity.Player.Character;
using SunCity.Game.World.Common.Components;
using SunCity.Game.World.Forms.Character;
using SunCity.Game.World.Interfaces.Character.Components.Vitals;
using SunCity.Game.World.Player.Character.Components.Vital;
using SunCity.Game.World.Player.Data;

namespace SunCity.Game.World.Player.Character
{
    public sealed class WorldPlayerCharacter
        : BaseEntityComponent<WorldPlayer>
            , IEntityWithId<PlayerCharacterId>
            , IEntityWithName<PlayerCharacterName>
    {
        public PlayerCharacterId EntityId { get; set; }
        public PlayerCharacterName Name { get; set; }

        public WorldPlayerData<long> Money { get; }
        public ICharacterVitalComponent Health { get; }
        public WorldPlayerData<long> Armour { get; }
        public WorldPlayerData<long> DonatePoints { get; }
        public ICharacterVitalComponent Hunger { get; }
        public ICharacterVitalComponent Thirst { get; }

        public WorldPlayerCharacter(WorldPlayer InOwner) : base(InOwner)
        {
            //----------------------------
            Hunger = new CharacterHungerComponent(InOwner);
            Thirst = new CharacterThirstComponent(InOwner);

            //----------------------------
            DonatePoints = new WorldPlayerData<long>(InOwner, nameof(DonatePoints));
            Money = new WorldPlayerData<long>(InOwner, nameof(Money));

            //----------------------------
            Health = new CharacterHealthComponent(InOwner);
            Armour = new WorldPlayerData<long>(InOwner, nameof(Armour));
        }

        public void Initialize(PlayerInitializeContext<WorldJoinCharacterForm> InContext)
        {
            //----------------------------
            EntityId = InContext.Form.PlayerCharacterId;
            Name = InContext.Form.PlayerCharacterName;

            //----------------------------
            Armour.Value = 0;
            Health.Set((int) InContext.Form.Health.Amount);

            //----------------------------
            Hunger.Set((int) InContext.Form.Hunger.Amount);
            Thirst.Set((int) InContext.Form.Thirst.Amount);

            //----------------------------
            Money.Value = 10000;
            DonatePoints.Value = InContext.Form.Balance;

            //----------------------------
            Owner.RagePlayer.SetData(WorldPlayerConsts.CharacterId, InContext.Form.PlayerCharacterId);
        }
    }
}