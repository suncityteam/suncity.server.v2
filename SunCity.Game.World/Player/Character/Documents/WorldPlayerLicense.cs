﻿using System.Collections.Generic;
using SunCity.Game.World.Common.Components;

namespace SunCity.Game.World.Player.Character.Documents
{
    public class WorldPlayerDocument
    {
        
    }

    public interface IWorldPlayerLicenses
    {
        IReadOnlyList<WorldPlayerLicense> Licenses { get; }
    }
    
    public class WorldPlayerLicenses : BaseEntityComponent<WorldPlayer>, IWorldPlayerLicenses
    {
        private List<WorldPlayerLicense> OwnLicenses { get; } = new List<WorldPlayerLicense>();

        public IReadOnlyList<WorldPlayerLicense> Licenses => OwnLicenses;
        
        public WorldPlayerLicenses(WorldPlayer InOwner) : base(InOwner)
        {
        }

    }
    
    public class WorldPlayerLicense : BaseEntityComponent<WorldPlayer>
    {
        public WorldPlayerLicense(WorldPlayer InOwner) : base(InOwner)
        {
        }
    }
}