﻿using System;
using System.Collections.Generic;
using SunCity.Common.RageMp.Pools.Players;
using SunCity.Game.World.Forms.Character;

namespace SunCity.Game.World.Player.Character
{
    internal static class CharacterCustomization
    {
        public const int MaxHeadOverlays = 11;

        public static int[] GetOverlayData(this WorldJoinCharacterSkin InSkin, int index)
        {
            var overlayData = new int[2];

            switch (index)
            {
                case 0:
                    overlayData[0] = InSkin.BlemishesModel;
                    overlayData[1] = 0;
                    break;
                case 1:
                    overlayData[0] = InSkin.BeardModel;
                    overlayData[1] = InSkin.BeardColor;
                    break;
                case 2:
                    overlayData[0] = InSkin.EyebrowsModel;
                    overlayData[1] = InSkin.EyebrowsColor;
                    break;
                case 3:
                    overlayData[0] = InSkin.AgeingModel;
                    overlayData[1] = 0;
                    break;
                case 4:
                    overlayData[0] = InSkin.MakeupModel;
                    overlayData[1] = 0;
                    break;
                case 5:
                    overlayData[0] = InSkin.BlushModel;
                    overlayData[1] = InSkin.BlushColor;
                    break;
                case 6:
                    overlayData[0] = InSkin.ComplexionModel;
                    overlayData[1] = 0;
                    break;
                case 7:
                    overlayData[0] = InSkin.SundamageModel;
                    overlayData[1] = 0;
                    break;
                case 8:
                    overlayData[0] = InSkin.LipstickModel;
                    overlayData[1] = InSkin.LipstickColor;
                    break;
                case 9:
                    overlayData[0] = InSkin.FrecklesModel;
                    overlayData[1] = 0;
                    break;
                case 10:
                    overlayData[0] = InSkin.ChestModel;
                    overlayData[1] = InSkin.ChestColor;
                    break;
            }
            return overlayData;
        }

        public static void ApplyCustomization(this IRagePlayer InPlayer, WorldJoinCharacterSkin InSkin)
        {/*
            //==========================================
            // Populate the head
            var headBlend = new HeadBlendData
            (
                //----------------------------------
                InSkin.FirstHeadShape,
                InSkin.SecondHeadShape,
                0,

                //----------------------------------
                InSkin.FirstSkinTone,
                InSkin.SecondSkinTone,
                0,

                //----------------------------------
                InSkin.HeadMix,
                InSkin.SkinMix,
                0
            );

            //==========================================
            // Add the face features
            var faceFeatures = new[]
            {
                InSkin.NoseWidth, InSkin.NoseHeight, InSkin.NoseLength, InSkin.NoseBridge, InSkin.NoseTip, InSkin.NoseShift, InSkin.BrowHeight,
                InSkin.BrowWidth, InSkin.CheekboneHeight, InSkin.CheekboneWidth, InSkin.CheeksWidth, InSkin.Eyes, InSkin.Lips, InSkin.JawWidth,
                InSkin.JawHeight, InSkin.ChinLength, InSkin.ChinPosition, InSkin.ChinWidth, InSkin.ChinShape, InSkin.NeckWidth
            };

            //==========================================
            // Populate the head overlays
            var headOverlays = new Dictionary<int, HeadOverlayData>();

            for (var i = 0; i < MaxHeadOverlays; i++)
            {
                // Get the overlay model and color
                var overlayData = InSkin.GetOverlayData(i);

                // Create the overlay
                var headOverlay = new HeadOverlayData
                (
                    index: Convert.ToUInt32(overlayData[0]),
                    colorId: Convert.ToUInt32(overlayData[1]),
                    secondaryColorId: 0,
                    opacity: 1.0f
                );

                // Add the overlay
                headOverlays[i] = headOverlay;
            }*/

            //==========================================
           // InPlayer.SetCustomization(InSkin.Sex == GameCharacterSex.Male, headBlend, InSkin.EyesColor, InSkin.FirstHairColor, InSkin.SecondHairColor, faceFeatures, headOverlays, new Dictionary<uint, uint>() { });
           // InPlayer.SetClothes(new Dictionary<ClothSlot, ClothData>
           // {
           //     {
           //         ClothSlot.Hair,
           //         new ClothData(InSkin.HairModel, 0, 0)
           //     }
           // });
        }
    }
}
