﻿using SunCity.Common.RageMp.Pools.Vehicles;
using SunCity.Game.World.Common.Components;
using SunCity.Game.World.Vehicle;

namespace SunCity.Game.World.Player.Character
{
    public sealed class WorldPlayerVehicle: BaseEntityComponent<WorldPlayer>
    {
        public IRageVehicle ActualVehicle => Owner.RagePlayer.Vehicle;
        public WorldVehicle WorldVehicle { get; set; }
        public WorldVehicle ExitedWorldVehicle { get; set; }
        
        public WorldPlayerBeltState SeatBeltState { get; }


        public void ExitVehicle(WorldVehicle vehicle)
        {
            ExitedWorldVehicle = vehicle;
            SeatBeltState.RemoveBelt();
        }
        
        public WorldPlayerVehicle(WorldPlayer InOwner) : base(InOwner)
        {
            SeatBeltState = new WorldPlayerBeltState(InOwner);
        }
    }
}