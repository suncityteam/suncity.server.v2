﻿using System;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using SunCity.Common.EfCore;
using SunCity.Common.RageMp.Pools;
using SunCity.Game.Types.Storage;
using SunCity.Game.World.DataBase;

namespace SunCity.Game.World.StationPoint
{
    internal class WorldStationPointStorage : WorldStorage<StationPointId, WorldStationPoint>, IWorldStationPointStorage
    {
        private ILogger<WorldStationPointStorage> Logger { get; }
        private IServiceProvider ServiceProvider { get; }
        private IRageMpPool RageMpPool { get; }
        public WorldStationPointStorage(ILogger<WorldStationPointStorage> logger, IServiceProvider serviceProvider, IRageMpPool rageMpPool)
        {
            Logger = logger;
            ServiceProvider = serviceProvider;
            RageMpPool = rageMpPool;
        }
        
        public async Task Initialize()
        {
            using var scope = ServiceProvider.CreateScope();
            var dbContext = scope.ServiceProvider.GetService<GameWorldDbContext>();

            Logger.LogInformation("Begin Initialize");

            var models = await dbContext.StationPointEntity
                .HasTracking(hasTracking: false)
                .ToArrayAsync();

            Logger.LogInformation($"Fetched {models.Length} station points");

            foreach (var model in models)
            {
                Put(new WorldStationPoint(model)).Initialize(RageMpPool);
            }

            Logger.LogInformation($"Loaded {Items.Count} station points");
            Logger.LogInformation("End Initialize");
        }
    }
}