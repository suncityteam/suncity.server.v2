﻿using System;
using System.Threading.Tasks;
using GTANetworkAPI;
using SunCity.Common.RageMp.Pools;
using SunCity.Common.RageMp.Pools.Blips;
using SunCity.Common.RageMp.Pools.TextLabels;
using SunCity.Domain.Interfaces.Entity;
using SunCity.Game.World.DataBase.Entitites.StationPoint;

namespace SunCity.Game.World.StationPoint
{
    public class WorldStationPoint 
        : IEntityWithId<StationPointId>
        , IEntityWithCreatedDate
    {
        /// <summary>
        /// https://wiki.rage.mp/index.php?title=Blips
        /// </summary>
        private const int MapIconSprite = 280;
        private const int MapIconColor = 4;

        public StationPointId EntityId { get; }
        public DateTime CreatedDate { get;  }
        public Vector3 Position { get; }
        public string Name { get;  }

        private IRageBlip MapIcon { get;  set; }
        private IRageTextLabel TextLabel { get; set; }
        
        public WorldStationPoint(StationPointEntity entity)
        {
            EntityId = entity.EntityId;
            CreatedDate = entity.CreatedDate;
            Position = entity.Position;
            Name = entity.Name;
        }

        public WorldStationPoint Initialize(IRageMpPool pool)
        {
            MapIcon = pool.Blips.New(MapIconSprite, Position, 0.5f, MapIconColor, $"Автобусная остановка \n{Name}");
            TextLabel = pool.TextLabels.New(Position, $"Автобусная остановка \n{Name}", 0, Colors.White);

            return this;
        }
        
    }
}