﻿using System.Threading.Tasks;
using SunCity.Game.Types.Storage;

namespace SunCity.Game.World.StationPoint
{
    public interface IWorldStationPointStorage : IWorldStorage<StationPointId, WorldStationPoint>
    {
        Task Initialize();
    }
}