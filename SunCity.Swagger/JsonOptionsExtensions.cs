﻿using System.Text.Json.Serialization;
using Microsoft.Extensions.DependencyInjection;

namespace SunCity.Swagger
{
    public static class JsonOptionsExtensions
    {
        public static IMvcBuilder AddJsonStringEnumConverter(this IMvcBuilder builder)
        {
            //    For fix enums in swagger after replace newtonsoft.json to microsoft.json for NET CORE 3.1
            //    https://stackoverflow.com/questions/36452468/swagger-ui-web-api-documentation-present-enums-as-strings/49941775
            builder.AddJsonOptions(options => options.JsonSerializerOptions.Converters.Add(new JsonStringEnumConverter()));
            return builder;
        }
    }
}