﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.OpenApi.Models;
using ProGaudi.Tarantool.Client;
using SunCity.Domain.Interfaces.Types;
using SunCity.Swagger.Converters;
using Swashbuckle.AspNetCore.SwaggerGen;

namespace SunCity.Swagger
{
    public static class SwaggerConfig
    {
        public const string v1 = nameof(v1);
        public const string title = "Suncity";

        public static void EnableSwaggerUi(this IApplicationBuilder app)
        {
            app.UseSwagger();
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint($"/swagger/{v1}/swagger.json", title);
            });
        }
        
        public static void AddSwaggerConfiguration(this IServiceCollection services, string swaggerXml = default)
        {
           
            // Register the Swagger generator, defining 1 or more Swagger documents
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc(v1, new OpenApiInfo
                {
                    Title = title,
                    Version = v1
                });
                
                c.DescribeAllEnumsAsStrings();
                c.DocInclusionPredicate((docName, apiDesc) => true);
                
                c.UseInlineDefinitionsForEnums();
                
                // for file downloading see: https://stackoverflow.com/questions/43844261/what-is-the-correct-way-to-download-a-file-via-the-nswag-code-generator-angular
                c.MapType<FileContentResult>(() => new OpenApiSchema
                 {
                     Type = "file",
                 });

                if (!string.IsNullOrWhiteSpace(swaggerXml) && File.Exists(swaggerXml))
                {
                    c.IncludeXmlComments(swaggerXml);
                }

                c.AddSecurityDefinition("Bearer", new OpenApiSecurityScheme()
                {
                    Description = "JWT Authorization header using the Bearer scheme. Example: \"Authorization: Bearer {token}\"",
                    Name = "Authorization",
                    In = ParameterLocation.Header,
                    Type = SecuritySchemeType.ApiKey
                });
                
                c.AddSecurityRequirement(new OpenApiSecurityRequirement()
                {
                    {
                        new OpenApiSecurityScheme
                        {
                            Reference = new OpenApiReference
                            {
                                Type = ReferenceType.SecurityScheme,
                                Id = "Bearer"
                            },
                            Scheme = "oauth2",
                            Name = "Bearer",
                            In = ParameterLocation.Header,

                        },
                        new List<string>()
                    }
                });



                c.SchemaFilter<ObjectIdToGuid>();
                c.EnableAnnotations();
            });
        }
    }
}