﻿

using System;
using System.Text;

namespace SunCity.Common.Crypto.Crc64
{
	public static class Crc64Extensions 
	{
		public static long Crc64Hash(this Char InValue, long InCrc = 0)
		{
			return BitConverter.GetBytes(InValue).Compute((ulong)InCrc);
		}
		public static long Crc64Hash(this Byte InValue, long InCrc = 0)
		{
			return BitConverter.GetBytes(InValue).Compute((ulong)InCrc);
		}
		public static long Crc64Hash(this Int16 InValue, long InCrc = 0)
		{
			return BitConverter.GetBytes(InValue).Compute((ulong)InCrc);
		}
		public static long Crc64Hash(this Int32 InValue, long InCrc = 0)
		{
			return BitConverter.GetBytes(InValue).Compute((ulong)InCrc);
		}
		public static long Crc64Hash(this Int64 InValue, long InCrc = 0)
		{
			return BitConverter.GetBytes(InValue).Compute((ulong)InCrc);
		}
		public static long Crc64Hash(this SByte InValue, long InCrc = 0)
		{
			return BitConverter.GetBytes(InValue).Compute((ulong)InCrc);
		}
		public static long Crc64Hash(this UInt16 InValue, long InCrc = 0)
		{
			return BitConverter.GetBytes(InValue).Compute((ulong)InCrc);
		}
		public static long Crc64Hash(this UInt32 InValue, long InCrc = 0)
		{
			return BitConverter.GetBytes(InValue).Compute((ulong)InCrc);
		}
		public static long Crc64Hash(this UInt64 InValue, long InCrc = 0)
		{
			return BitConverter.GetBytes(InValue).Compute((ulong)InCrc);
		}
		public static long Crc64Hash(this Single InValue, long InCrc = 0)
		{
			return BitConverter.GetBytes(InValue).Compute((ulong)InCrc);
		}
		public static long Crc64Hash(this Double InValue, long InCrc = 0)
		{
			return BitConverter.GetBytes(InValue).Compute((ulong)InCrc);
		}
	
		public static long Crc64Hash(this string InValue, long InCrc = 0)
		{
			return Encoding.UTF8.GetBytes(InValue).Compute((ulong)InCrc);
		}

		public static long Crc64Hash(this Guid InValue, long InCrc = 0)
		{
			return InValue.ToByteArray().Compute((ulong)InCrc);
		}
	}
}