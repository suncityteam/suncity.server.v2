﻿using System;
using System.Diagnostics.CodeAnalysis;

namespace SunCity.Common
{
    [SuppressMessage("ReSharper", "InconsistentNaming")]
    public class EnvironmentNames
    {
        public static int ConcurrencyLevel { get; }= Environment.ProcessorCount * 2;

        public const string INTEGRATION_TESTS =  nameof(INTEGRATION_TESTS);
        public const string ENTITY_FRAMEWORK_MIGRATION = nameof(ENTITY_FRAMEWORK_MIGRATION);
        public const string USE_MOCK_USERS = nameof(USE_MOCK_USERS);
    }
}