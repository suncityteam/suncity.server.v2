﻿namespace SunCity.Common.Operation
{
    public enum OperationResponseCode
    {
        [ResponseMessage("Неизвестная ошибка")]
        UnknownError,
        
        [ResponseMessage("Исключение")]
        ExceptionError,
        
        [ResponseMessage("Соединение не найдено")]
        ConnectionNotFound,

        [ResponseMessage("Операция выполнена успешно")]
        Successfully,

        [ResponseMessage("Аккаунт {0} не найден")]
        AccountNotFound,

        [ResponseMessage("Аккаунт {0} удален")]
        AccountIsDeleted,

        [ResponseMessage("Аккаунт {0} заблокирован")]
        AccountIsBlocked,

        [ResponseMessage("Логин {0} уже занят")]
        AccountLoginIsExist,

        [ResponseMessage("Email {0} уже занят")]
        AccountEmailIsExist,

        [ResponseMessage("Логин слишком короткий. Минимальная длина {0}")]
        LoginIsShort,

        [ResponseMessage("Логин слишком длинный. Максимальная длина {0}")]
        LoginIsLarge,

        [ResponseMessage("Указан не корректный логин")]
        LoginIsInvalid,

        [ResponseMessage("Email слишком короткий. Минимальная длина {0}")]
        EmailIsShort,

        [ResponseMessage("Email слишком длинный. Максимальная длина {0}")]
        EmailIsLarge,

        [ResponseMessage("Указан не корректный Email")]
        EmailIsInvalid,

        [ResponseMessage("Пароль слишком короткий. Минимальная длина {0}")]
        PasswordIsShort,

        [ResponseMessage("Пароль слишком длинный. Максимальная длина {0}")]
        PasswordIsLarge,

        [ResponseMessage("Пароль не корректный Email")]
        PasswordIsInvalid,

        [ResponseMessage("Токен не найден")]
        TokenNotFound,

        [ResponseMessage("Токен заблокирован")]
        TokenInBlackList,

        [ResponseMessage("Токен истек")]
        TokenIsExpired,

        [ResponseMessage("Токен не корректен")]
        TokenIsInvalid,

        [ResponseMessage("У игрока уже есть активная сессия")]
        PlayerHasActiveSession,

        [ResponseMessage("Некорректный ИД недвижимости")]
        PropertyIdIsInvalid,

        [ResponseMessage("Неудалось найти недвижимость с ИД ({0})")]
        PropertyIdNotFound,

        [ResponseMessage("Неудалось приобрести недвижимость с ИД ({0}). Недвижимость не продается")]
        PropertyIsNotSelling,

        [ResponseMessage("Неудалось приобрести недвижимость с ИД ({0}). Недвижимость можно приобрести только за деньги")]
        PropertySellingOnlyWithMoney,

        [ResponseMessage("Неудалось приобрести недвижимость с ИД ({0}). Недвижимость можно приобрести только за DP")]
        PropertySellingOnlyWithDonate,

        [ResponseMessage("Неудалось приобрести недвижимость с ИД ({0}). Не указаны платежные реквизиты")]
        PropertyRequisitesNotFilled,

        [ResponseMessage("Недвижимсость находится слишком далеко")]
        PropertyNotInRange,

        [ResponseMessage("Неудалось найти фабрику для типа недвижимости {0}")]
        PropertyFactoryNotFound,

        [ResponseMessage("Некорректный ИД игрока")]
        PlayerIdIsInvalid,

        [ResponseMessage("Неудалось найти игрока с ИД ({0})")]
        PlayerIdNotFound,

        [ResponseMessage("Неудалось найти персонажа с ИД ({0})")]
        PlayerCharacterNotFound,

        [ResponseMessage("Персонаж с ИД ({0}) не пренадлежит игроку")]
        PlayerCharacterNotOwned,

        [ResponseMessage("Персонаж с ИД ({0}) временно заблокирован")]
        PlayerCharacterIsBlocked,

        [ResponseMessage("Игровой аккаунт не найден")]
        GameAccountNotFound,

        [ResponseMessage("Игровой аккаунт уже создан")]
        GameAccountIsExist,

        [ResponseMessage("Банковская карта не найдена")]
        BankCardNotFound,

        [ResponseMessage("Введен неверный пин-код для банковской карты")]
        BankCardPinCodeNotValid,

        [ResponseMessage("Банковская карта {0} не действительна")]
        BankCardNotValid,

        [ResponseMessage("Тариф {0} банковской карты не найден")]
        BankCardTariffNotFound,

        [ResponseMessage("Неудалось сгенерировать номер банковской карты")]
        BankCardNumberFailedGenerate,

        [ResponseMessage("Некорректная сумма депозита")]
        BankInvalidDepositAmount,

        [ResponseMessage("Некорректная сумма для снятия")]
        BankInvalidWithdrawAmount,

        [ResponseMessage("Недостаточно средств для совершения операции")]
        NotEnoughMoney,

        [ResponseMessage("Фракция {0} не найдена")]
        FractionNotFound,

        [ResponseMessage("Фабрика для фракции {0} не найдена")]
        FractionFactoryNotFound,

        [ResponseMessage("Предмет с ид {0} не найден")]
        StorageItemNotFound,

        [ResponseMessage("Предмет с ид {0} не найден")]
        TargetItemNotFound,

        [ResponseMessage("Предмета нет в наличии")]
        ItemNotNotEnough,

        [ResponseMessage("Предмет с ид {0} не найден")]
        ItemNotFound,
        
        [ResponseMessage("Недостаточно места в инвентаре")]
        NotEnoughCapacity,

        [ResponseMessage("Экипировать можно только одежду и оружие")]
        ItemCategoryEquipNotAllowed,
        
        [ResponseMessage("Лицензия с ид {0} не найден")]
        LicenseNotFound,
        
        [ResponseMessage("Лицензия с ид {0} удалена")]
        LicenseIsDeleted,
        
        [ResponseMessage("Нет необходимой лицензации")]
        NotExistRequiredLicense,
    }
}