﻿using System.Threading.Tasks;

namespace SunCity.Common.Operation
{
    public static class TaskOperationResponseCache
    {
        public static Task<OperationResponse<bool>> True { get; } = Task.FromResult(OperationResponseCache.True);
        public static Task<OperationResponse<bool>> False { get; } = Task.FromResult(OperationResponseCache.True);

        public static Task<OperationResponse> UnknownError { get; } = Task.FromResult(OperationResponseCache.UnknownError);
        public static Task<OperationResponse> Successfully { get; } = Task.FromResult(OperationResponseCache.Successfully);
        public static Task<OperationResponse> TokenIsInvalid { get; } = Task.FromResult(OperationResponseCache.TokenIsInvalid);
        public static Task<OperationResponse> ConnectionNotFound { get; } = Task.FromResult(OperationResponseCache.ConnectionNotFound);
    }
}