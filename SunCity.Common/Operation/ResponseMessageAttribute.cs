﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace SunCity.Common.Operation
{
    internal class ResponseMessageAttribute : Attribute
    {
        public string Message { get; }

        internal ResponseMessageAttribute(string InMessage)
        {
            Message = InMessage;
        }
    }

    internal class ResponseMessageStorage
    {
        private static ResponseMessageStorage Storage { get; } = Initialize();
        internal Dictionary<OperationResponseCode, string> Messages { get; } = new Dictionary<OperationResponseCode, string>(System.Enum.GetValues(typeof(OperationResponseCode)).Length);

        public static string GetMessage(OperationResponseCode InCode)
        {
            Storage.Messages.TryGetValue(InCode, out var message);
            return message ?? string.Empty;
        }

        public static string GetMessage(OperationResponseCode InCode, params object[] InArgs)
        {
            var message = GetMessage(InCode);
            if (InArgs == null || InArgs.Length == 0)
                return message;

            return string.Format(message, InArgs);
        }

        private static ResponseMessageStorage Initialize()
        {
            var type = typeof(OperationResponseCode);
            var storage = new ResponseMessageStorage();
            var values = System.Enum.GetValues(type).Cast<OperationResponseCode>().ToArray();

            foreach (var value in values)
            {
                var member = type.GetMember(string.Intern(value.ToString())).FirstOrDefault(m => m.DeclaringType == type);
                var attribute = member?.GetCustomAttribute<ResponseMessageAttribute>();
                if (attribute != null)
                {
                    storage.Messages.Add(value, string.Intern(attribute.Message));
                }
            }

            return storage;
        }
    }
}