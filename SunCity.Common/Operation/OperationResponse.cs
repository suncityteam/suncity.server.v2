﻿#define CONTRACTS_FULL

using System.Diagnostics;
using System.Text.Json.Serialization;
using JetBrains.Annotations;

namespace SunCity.Common.Operation
{
    [NotNull]
    [DebuggerDisplay("{Error}[IsCorrect: {IsCorrect}]")]
    public class OperationResponse
    {
        public OperationResponse()
        {

        }

        public OperationResponse(OperationError InError)
        {
            Error = InError;
        }

        [CanBeNull]
        public OperationError Error { get; set; }
        public bool IsCorrect => Error == null || Error.Code == OperationResponseCode.Successfully;
        public bool IsNotCorrect => !IsCorrect;

        public static implicit operator OperationResponse((OperationResponseCode, string, object[]) InResponse) => new OperationResponse((InResponse.Item1, InResponse.Item2, InResponse.Item3));
        public static implicit operator OperationResponse((OperationResponseCode, string, object) InResponse) => new OperationResponse((InResponse.Item1, InResponse.Item2, InResponse.Item3));
        public static implicit operator OperationResponse((OperationResponseCode, object[]) InResponse) => new OperationResponse((InResponse.Item1, InResponse.Item2));
        public static implicit operator OperationResponse((OperationResponseCode, object) InResponse) => new OperationResponse((InResponse.Item1, InResponse.Item2));
        public static implicit operator OperationResponse((OperationResponseCode, string) InResponse) => new OperationResponse((InResponse.Item1, InResponse.Item2));


        public static implicit operator OperationResponse(OperationError InOperationError) => new OperationResponse(InOperationError);
        public static implicit operator OperationResponse(OperationResponseCode InResponseCode) => new OperationResponse(new OperationError(InResponseCode));
    }

    [NotNull]
    [DebuggerDisplay("{Error}[IsCorrect: {IsCorrect}][Content: {Content}]")]
    public class OperationResponse<TContent> : OperationResponse
    {
        public TContent Content { get; set; }

        public OperationResponse()
        {

        }

        public OperationResponse(TContent InContent) : base(null)
        {
            Content = InContent;
        }

        public OperationResponse(OperationError InError) : base(InError)
        {
            Content = default;
        }

        bool IfValidGetContent(out TContent InContent)
        {
            InContent = Content;
            return IsCorrect;
        }

        public static implicit operator OperationResponse<TContent>((OperationResponseCode, string, object[]) InResponse) => new OperationResponse<TContent>((InResponse.Item1, InResponse.Item2, InResponse.Item3));
        public static implicit operator OperationResponse<TContent>((OperationResponseCode, string, object) InResponse) => new OperationResponse<TContent>((InResponse.Item1, InResponse.Item2, InResponse.Item3));
        public static implicit operator OperationResponse<TContent>((OperationResponseCode, object[]) InResponse) => new OperationResponse<TContent>((InResponse.Item1, InResponse.Item2));
        public static implicit operator OperationResponse<TContent>((OperationResponseCode, object) InResponse) => new OperationResponse<TContent>((InResponse.Item1, InResponse.Item2));
        public static implicit operator OperationResponse<TContent>((OperationResponseCode, string) InResponse) => new OperationResponse<TContent>((InResponse.Item1, InResponse.Item2));


        public static implicit operator OperationResponse<TContent>(OperationError InOperationError) => new OperationResponse<TContent>(InOperationError);

        public static implicit operator OperationResponse<TContent>(OperationResponseCode InResponseCode) => new OperationResponse<TContent>(new OperationError(InResponseCode));

        public static implicit operator OperationResponse<TContent>(TContent InContent) => new OperationResponse<TContent>(InContent);
    }

}
