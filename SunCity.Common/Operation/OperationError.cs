﻿using System.Diagnostics.CodeAnalysis;
using System.Runtime.Serialization;

namespace SunCity.Common.Operation
{
    public class OperationError
    {
        public OperationError()
        {

        }

        public OperationError(OperationResponseCode InCode, string InMessage, params object[] InArgs)
        {
            Code    = InCode;

            if (string.IsNullOrWhiteSpace(InMessage))
            {
                Message = ResponseMessageStorage.GetMessage(InCode, InArgs);
            }
            else
            {
                Message = string.Format(InMessage, InArgs);
            }
        }

        public OperationError(OperationResponseCode InCode, params object[] InArgs)
        {
            Code    = InCode;
            Message = ResponseMessageStorage.GetMessage(InCode, InArgs);
        }

        public OperationError(OperationResponseCode InCode, string? InMessage = null)
        {
            Code    = InCode;
            Message = InMessage ?? ResponseMessageStorage.GetMessage(InCode);
        }

        public OperationResponseCode Code { get; }
        public string? Message { get; }

        [IgnoreDataMember]
        public bool IsCorrect => Code == OperationResponseCode.Successfully;

        [IgnoreDataMember]
        public bool IsNotCorrect => !IsCorrect;

        [SuppressMessage("ReSharper", "InconsistentNaming")]
        public bool IfNotCorrect(out OperationError OutOperationError)
        {
            OutOperationError = this;
            return IsNotCorrect;
        }

        public static implicit operator OperationError((OperationResponseCode, string, object[]) InResponse) => new OperationError(InResponse.Item1, InResponse.Item2, InResponse.Item3);
        public static implicit operator OperationError((OperationResponseCode, string, object) InResponse) => new OperationError(InResponse.Item1, InResponse.Item2, InResponse.Item3);

        public static implicit operator OperationError((OperationResponseCode, object[]) InResponse) => new OperationError(InResponse.Item1, InResponse.Item2);
        public static implicit operator OperationError((OperationResponseCode, object) InResponse) => new OperationError(InResponse.Item1, InResponse.Item2);
        public static implicit operator OperationError((OperationResponseCode, string) InResponse) => new OperationError(InResponse.Item1, InResponse.Item2);

        public static implicit operator OperationError(OperationResponseCode InResponseCode) => new OperationError(InResponseCode);

        public override string ToString()
        {
            return $"[Code: {Code}][Message: {Message}]";
        }
    }
}