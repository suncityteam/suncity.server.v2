﻿namespace SunCity.Common.Operation
{
    public static class OperationResponseCache
    {
        public static OperationResponse<bool> True { get; } = new OperationResponse<bool>(true);
        public static OperationResponse<bool> False { get; } = new OperationResponse<bool>(false);

        public static OperationResponse UnknownError { get; } = new OperationResponse(OperationResponseCode.UnknownError);
        public static OperationResponse Successfully { get; } = new OperationResponse(OperationResponseCode.Successfully);
        public static OperationResponse TokenIsInvalid { get; } = new OperationResponse(OperationResponseCode.TokenIsInvalid);
        public static OperationResponse ConnectionNotFound { get; } = new OperationResponse(OperationResponseCode.ConnectionNotFound);
    }
}