﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace SunCity.Common.Enum
{
    public sealed class EnumDescription<TEnum> where TEnum : System.Enum
    {
        public TEnum Value { get; }
        public string Description { get; }


        public static implicit operator EnumDescription<TEnum>(TEnum InEnum) => new EnumDescription<TEnum>(InEnum);

        public EnumDescription(TEnum InValue)
        {
            Value = InValue;
            Description = EnumDescriptionStorage<TEnum>.GetDescription(InValue);
        }
        
        public EnumDescription(TEnum InValue, string InDescription)
        {
            Value = InValue;
            Description = InDescription;
        }

    }

    public sealed class EnumDescriptionAttribute : Attribute
    {
        public string Description { get; }

        public EnumDescriptionAttribute(string InMessage)
        {
            Description = InMessage;
        }
    }

    public static class EnumDescriptionExtensions
    {
        public static string GetDescription<TEnum>(this TEnum InValue)
            where TEnum : System.Enum
        {
            return EnumDescriptionStorage<TEnum>.GetDescription(InValue);
        }
    }

    public sealed class EnumDescriptionStorage<TEnum> where TEnum : System.Enum
    {
        private static Type ThisType { get; } = typeof(TEnum);
        private static Dictionary<TEnum, string> Messages { get; } = new Dictionary<TEnum, string>(System.Enum.GetValues(ThisType).Length);

        static EnumDescriptionStorage()
        {
            Initialize();
        }

        public static string GetDescription(TEnum InValue)
        {
            Messages.TryGetValue(InValue, out var message);
            return message ?? string.Empty;
        }

        private static void Initialize()
        {
            if (!ThisType.IsEnum)
            {
                throw new FormatException("EnumerationValue must be of Enum type");
            }

            var values = System.Enum.GetValues(ThisType).Cast<TEnum>().ToArray();

            foreach (var value in values)
            {
                var member = ThisType.GetMember(string.Intern(value.ToString())).FirstOrDefault(m => m.DeclaringType == ThisType);
                var attribute = member?.GetCustomAttribute<EnumDescriptionAttribute>();
                if (attribute != null)
                {
                    Messages.Add(value, string.Intern(attribute.Description));
                }
            }

        }
    }
}
