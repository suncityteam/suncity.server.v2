﻿using System.Text.Json;
using SunCity.Common.Json.Converters;

namespace SunCity.Common.Json
{
    public class CommonJsonSerializer : ICommonJsonSerializer
    {
        public static ICommonJsonSerializer Instance = new CommonJsonSerializer();
        private static JsonSerializerOptions SerializerOptions { get; }

        static CommonJsonSerializer()
        {
            SerializerOptions = new JsonSerializerOptions
            {
                IgnoreNullValues = true,
                PropertyNamingPolicy = JsonNamingPolicy.CamelCase,
#if DEBUG
                WriteIndented = true
#endif
            };

            SerializerOptions.Converters.UseStronglyConverters();
            SerializerOptions.Converters.UseAutoNumberConverters();
        }


        public string Serialize<T>(T InObject)
        {
            return JsonSerializer.Serialize(InObject, SerializerOptions);
        }

        public T Deserialize<T>(string InJson)
        {
            return JsonSerializer.Deserialize<T>(InJson, SerializerOptions);
        }
    }
}