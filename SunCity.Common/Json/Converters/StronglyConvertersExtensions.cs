﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json.Serialization;
using Microsoft.Extensions.DependencyInjection;
using SunCity.Common.Json.Converters.AutoNumberToString;
using SunCity.Common.Reflection;
using SunCity.Domain.Interfaces.Types;

namespace SunCity.Common.Json.Converters
{
    public static class StronglyConvertersExtensions
    {
        public static void UseCommonJsonSerializer(this IServiceCollection InServiceCollection)
        {
            InServiceCollection.AddSingleton<ICommonJsonSerializer, CommonJsonSerializer>();
        }

        public static void UseAutoNumberConverters(this IList<JsonConverter> InJsonConverters)
        {
            InJsonConverters.Insert(0, new ShortToStringConverter());
            InJsonConverters.Insert(0, new IntToStringConverter());
            InJsonConverters.Insert(0, new LongToStringConverter());

            InJsonConverters.Insert(0, new FloatToStringConverter());
            InJsonConverters.Insert(0, new DoubleToStringConverter());
            InJsonConverters.Insert(0, new DecimalToStringConverter());

            InJsonConverters.Insert(0, new TimeSpanToStringConverter());
        }

        public static void UseStronglyConverters(this IList<JsonConverter> InJsonConverters)
        {
            var loadedTypes = GenericReflectionExtensions.GetLoadedTypes();
            InJsonConverters.Add(new JsonStringEnumConverter());

            InJsonConverters.AddGuidConverters(loadedTypes);
            InJsonConverters.AddNamesConverters(loadedTypes);
        }

        private static void AddNamesConverters(this IList<JsonConverter> InJsonConverters, Type[] InLoadedTypes)
        {
            var types = InLoadedTypes.Where(q => q.IsValueType && q.IsImplementedGenericInterface(typeof(IName<>))).ToArray();

            foreach (var type in types)
            {
                //var converterType = typeof(StronglyNameConverter<>).MakeGenericType(type);
                //if (Activator.CreateInstance(converterType) is JsonConverter converter)
                //{
                //    InJsonConverters.Add(converter);
                //}

                var converterType = typeof(ImmutableNameConverter<>).MakeGenericType(type);
                if (Activator.CreateInstance(converterType) is JsonConverter converter)
                {
                    InJsonConverters.Add(converter);
                }
            }
        }

        private static void AddGuidConverters(this IList<JsonConverter> InJsonConverters, Type[] InLoadedTypes)
        {
            var types = InLoadedTypes.Where(q => q.IsValueType && q.IsImplementedGenericInterface(typeof(IGuidId<>))).ToArray();

            foreach (var type in types)
            {
                var converterType = typeof(ImmutableGuidConverter<>).MakeGenericType(type);
                if (Activator.CreateInstance(converterType) is JsonConverter converter)
                {
                    InJsonConverters.Add(converter);
                }
            }
        }
    }
}