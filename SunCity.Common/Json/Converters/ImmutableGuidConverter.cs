﻿using System;
using System.Text.Json;
using System.Text.Json.Serialization;
using SunCity.Domain.Interfaces.Types;

namespace SunCity.Common.Json.Converters
{
    public class ImmutableGuidConverter <TGuid> : JsonConverter<TGuid> where TGuid : IGuidId<TGuid>
    {  
        public override TGuid Read(
            ref Utf8JsonReader reader,
            Type typeToConvert,
            JsonSerializerOptions options)
        {
            //  Read begin property name
            reader.Read();

            //  Read property value
            reader.Read();
            
            //  Get readed property value
            if (reader.TryGetGuid(out var guid))
            {
                //  Read end property name
                reader.Read();
                //reader.Read();

                if (Activator.CreateInstance(typeToConvert, guid) is TGuid id)
                {
                    return id;
                }
            }

            throw new JsonException();
        }

        public override void Write(Utf8JsonWriter writer, TGuid value, JsonSerializerOptions options)
        {
            writer.WriteStartObject();
            writer.WriteString(nameof(IGuidId<TGuid>.Id).ToLowerInvariant(), value.Id);
            writer.WriteEndObject();
        }
    }
}