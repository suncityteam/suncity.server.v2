﻿using System;
using System.Text.Json;
using System.Text.Json.Serialization;
using SunCity.Domain.Interfaces.Types;

namespace SunCity.Common.Json.Converters
{
    public class ImmutableNameConverter <TName> : JsonConverter<TName> where TName : IName<TName>
    {  
        public override TName Read(
            ref Utf8JsonReader reader,
            Type typeToConvert,
            JsonSerializerOptions options)
        {
            //  Read begin property name
            reader.Read();

            //  Read property value
            reader.Read();

            //  Get readed property value
            var name = reader.GetString();
            
            //  Read end property name
            reader.Read();

            if (Activator.CreateInstance(typeToConvert, name) is TName id)
            {
                return id;
            }
            
            throw new JsonException();
        }

        public override void Write(Utf8JsonWriter writer, TName value, JsonSerializerOptions options)
        {
            writer.WriteStartObject();
            writer.WriteString(nameof(IName<TName>.Name).ToLowerInvariant(), value.Name);
            writer.WriteEndObject();
        }
    }
}