﻿using System;
using System.Text.Json;
using System.Text.Json.Serialization;
using SunCity.Domain.Interfaces.Types;

namespace SunCity.Common.Json.Converters
{
    internal class StronglyGuidConverter<TGuid> : JsonConverter<TGuid> where TGuid : IGuidId<TGuid>
    {
        private static TGuid Default { get; } = Activator.CreateInstance<TGuid>();
        public override TGuid Read(ref Utf8JsonReader InReader, Type InTypeToConvert, JsonSerializerOptions InOptions)
        {
            if (InReader.TryGetGuid(out var guid))
            {
                if (Activator.CreateInstance(InTypeToConvert, guid) is TGuid id)
                {
                    return id;
                }
            }
            return Default;
        }

        public override void Write(Utf8JsonWriter InWriter, TGuid InValue, JsonSerializerOptions InOptions)
        {
            InWriter.WriteStringValue(InValue.Id);
        }
    }
}
