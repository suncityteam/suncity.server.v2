﻿using System;
using System.Text.Json;
using System.Text.Json.Serialization;

namespace SunCity.Common.Json.Converters.AutoNumberToString
{
    public class TimeSpanToStringConverter : JsonConverter<TimeSpan>
    {
        public override TimeSpan Read(ref Utf8JsonReader InReader, Type InTypeToConvert, JsonSerializerOptions InOptions)
        {
            if (InReader.TokenType == JsonTokenType.String)
            {
                var str = InReader.GetString()?.Trim();
                if (double.TryParse(str, out var intResult))
                    return TimeSpan.FromSeconds(intResult);
            }
            else if (InReader.TokenType == JsonTokenType.Number)
            {
                if (InReader.TryGetDouble(out var intResult))
                    return TimeSpan.FromSeconds(intResult);
            }

            using JsonDocument document = JsonDocument.ParseValue(ref InReader);
            throw new Exception($"unable to parse {document.RootElement.ToString()} to number");
        }

        public override void Write(Utf8JsonWriter InWriter, TimeSpan InValue, JsonSerializerOptions InOptions)
        {
            InWriter.WriteNumberValue(InValue.TotalSeconds);
        }
    }
}