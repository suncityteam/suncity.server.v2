﻿using System;
using System.Text.Json;
using System.Text.Json.Serialization;

namespace SunCity.Common.Json.Converters.AutoNumberToString
{
    public class IntToStringConverter : JsonConverter<int>
    {
        public override int Read(ref Utf8JsonReader InReader, Type InTypeToConvert, JsonSerializerOptions InOptions)
        {
            if (InReader.TokenType == JsonTokenType.String)
            {
                var str = InReader.GetString()?.Trim();
                if (int.TryParse(str, out var intResult))
                    return intResult;
            }
            else if (InReader.TokenType == JsonTokenType.Number)
            {
                if (InReader.TryGetInt32(out var intResult))
                    return intResult;
            }

            using JsonDocument document = JsonDocument.ParseValue(ref InReader);
            throw new Exception($"unable to parse {document.RootElement.ToString()} to number");
        }

        public override void Write(Utf8JsonWriter InWriter, int InValue, JsonSerializerOptions InOptions)
        {
            InWriter.WriteStringValue(InValue.ToString());
        }
    }
}