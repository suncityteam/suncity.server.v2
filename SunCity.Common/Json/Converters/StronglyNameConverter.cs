﻿    using System;
using System.Text.Json;
using System.Text.Json.Serialization;
    using SunCity.Domain.Interfaces.Types;

namespace SunCity.Common.Json.Converters
{
    internal class StronglyNameConverter<TName> : JsonConverter<TName> where TName : IName<TName>
    {
        private static TName Default { get; } = Activator.CreateInstance<TName>();
        public override TName Read(ref Utf8JsonReader InReader, Type InTypeToConvert, JsonSerializerOptions InOptions)
        {
            var name = InReader.GetString();
            if (string.IsNullOrWhiteSpace(name) == false)
            {
                if (Activator.CreateInstance(InTypeToConvert, name) is TName id)
                {
                    return id;
                }
            }
            return Default;
        }

        public override void Write(Utf8JsonWriter InWriter, TName InValue, JsonSerializerOptions InOptions)
        {
            InWriter.WriteStringValue(InValue.Name);
        }
    }
}