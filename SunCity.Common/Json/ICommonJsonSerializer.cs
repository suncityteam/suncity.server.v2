﻿namespace SunCity.Common.Json
{
    public interface ICommonJsonSerializer
    {
        string Serialize<T>(T InObject);
        T Deserialize<T>(string InJson);
    }
}