﻿using System;
using System.Net.Http;
using System.Net.Http.Headers;
using SunCity.Common.Json;

namespace SunCity.Common.HttpClient
{
    public class CommonHttpClient : BaseHttpClient
    {
        private readonly IHttpClientFactory httpClientFactory;

        public CommonHttpClient(IHttpClientFactory httpClientFactory, ICommonJsonSerializer commonJsonSerializer): base(commonJsonSerializer)
        {
            this.httpClientFactory = httpClientFactory;
        }

        protected override System.Net.Http.HttpClient CreateClient(Action<HttpRequestHeaders> customizeHeaders = default)
        {
            var httpClient = httpClientFactory.CreateClient();

            customizeHeaders?.Invoke(httpClient.DefaultRequestHeaders);
            return httpClient;
        }
    }
}
