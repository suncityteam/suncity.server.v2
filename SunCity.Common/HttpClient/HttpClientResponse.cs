﻿using System.Diagnostics;
using System.Net;

namespace SunCity.Common.HttpClient
{
    [DebuggerDisplay("StatusCode: {StatusCode} | Content: {Content}")]
    public class HttpClientResponse
    {
        public string Content { get; set; }
        public HttpStatusCode StatusCode { get; set; }

        public bool IsSuccessful => StatusCode >= HttpStatusCode.OK && StatusCode < HttpStatusCode.MultipleChoices;

        internal HttpClientResponse(HttpStatusCode httpStatusCode, string content)
        {
            Content = content;
            StatusCode = httpStatusCode;
        }
    }

    [DebuggerDisplay("StatusCode: {StatusCode} | HasResponse: {Response != null} | Content: {Content}")]
    public class HttpClientResponse<TResponse> : HttpClientResponse
    {
        public TResponse Response { get; }

        internal HttpClientResponse(HttpStatusCode httpStatusCode, string content) : base(httpStatusCode, content)
        {
            Content = content;
            StatusCode = httpStatusCode;
        }


        internal HttpClientResponse(HttpStatusCode httpStatusCode, string content, TResponse response) : base(httpStatusCode, content)
        {
            Response = response;
        }
    }
}