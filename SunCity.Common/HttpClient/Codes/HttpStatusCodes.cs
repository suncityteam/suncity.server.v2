﻿using System.Net;

namespace SunCity.Common.HttpClient.Codes
{
    public static class HttpStatusCodes
    {
        public const int OK = (int) HttpStatusCode.OK;
        public const int BadGateway = (int) HttpStatusCode.BadGateway;
        public const int BadRequest = (int) HttpStatusCode.BadRequest;
    }
}