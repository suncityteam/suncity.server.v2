﻿using System;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using SunCity.Common.Json;

namespace SunCity.Common.HttpClient
{
    public abstract class BaseHttpClient : ICommonHttpClient
    {
        protected readonly ICommonJsonSerializer commonJsonSerializer;

        protected BaseHttpClient(ICommonJsonSerializer commonJsonSerializer)
        {
            this.commonJsonSerializer = commonJsonSerializer;
        }

        public async Task<HttpClientResponse<TResult>> Get<TResult>(string url, Action<HttpRequestHeaders> customizeHeaders = default, CancellationToken cancellationToken = default)
        {
            var httpClient = CreateClient(customizeHeaders);
            var response = await httpClient.GetAsync(url, cancellationToken);
            return await DeserializeContent<TResult>(response);
        }

        public async Task<HttpClientResponse<TResult>> Post<TRequest, TResult>(string url, TRequest body, Action<HttpRequestHeaders> customizeHeaders = default, CancellationToken cancellationToken = default)
        {
            var httpClient = CreateClient(customizeHeaders);
            var content = SerializeContent(body);
            var response = await httpClient.PostAsync(url, content, cancellationToken);
            return await DeserializeContent<TResult>(response);
        }

        public async Task<HttpClientResponse> Post<TRequest>(string url, TRequest body, Action<HttpRequestHeaders> customizeHeaders = default, CancellationToken cancellationToken = default)
        {
            var httpClient = CreateClient(customizeHeaders);
            var content = SerializeContent(body);
            var result = await httpClient.PostAsync(url, content, cancellationToken);
            var resultContent = await result.Content.ReadAsStringAsync();
            return new HttpClientResponse(result.StatusCode, resultContent);
        }

        public async Task<HttpClientResponse> Post(string url, FormUrlEncodedContent content, Action<HttpRequestHeaders> customizeHeaders = default, CancellationToken cancellationToken = default)
        {
            var httpClient = CreateClient(customizeHeaders);
            var result = await httpClient.PostAsync(url, content, cancellationToken);
            var resultContent = await result.Content.ReadAsStringAsync();
            return new HttpClientResponse(result.StatusCode, resultContent);
        }

        public async Task<HttpClientResponse<TResult>> Put<TRequest, TResult>(string url, TRequest body, Action<HttpRequestHeaders> customizeHeaders = default, CancellationToken cancellationToken = default)
        {
            var httpClient = CreateClient(customizeHeaders);
            var content = SerializeContent(body);
            var response = await httpClient.PutAsync(url, content, cancellationToken);
            return await DeserializeContent<TResult>(response);
        }

        public async Task<HttpClientResponse> Put<TRequest>(string url, TRequest body, Action<HttpRequestHeaders> customizeHeaders = default, CancellationToken cancellationToken = default)
        {
            var httpClient = CreateClient(customizeHeaders);
            var content = SerializeContent(body);
            var result = await httpClient.PutAsync(url, content, cancellationToken);
            var resultContent = await result.Content.ReadAsStringAsync();
            return new HttpClientResponse(result.StatusCode, resultContent);
        }

        public async Task<HttpClientResponse<TResult>> Delete<TResult>(string url, Action<HttpRequestHeaders> customizeHeaders = default, CancellationToken cancellationToken = default)
        {
            var httpClient = CreateClient(customizeHeaders);
            var response = await httpClient.DeleteAsync(url, cancellationToken);
            return await DeserializeContent<TResult>(response);
        }

        public async Task<HttpClientResponse> Delete(string url, Action<HttpRequestHeaders> customizeHeaders = default, CancellationToken cancellationToken = default)
        {
            var httpClient = CreateClient(customizeHeaders);
            var response = await httpClient.DeleteAsync(url, cancellationToken);
            var resultContent = await response.Content.ReadAsStringAsync();
            return new HttpClientResponse(response.StatusCode, resultContent);
        }

        private async Task<HttpClientResponse<TResult>> DeserializeContent<TResult>(HttpResponseMessage response)
        {
            var resultContent = await response.Content.ReadAsStringAsync();

            if (response.IsSuccessStatusCode)
            {
                var resultObject = commonJsonSerializer.Deserialize<TResult>(resultContent);
                return new HttpClientResponse<TResult>(response.StatusCode, resultContent, resultObject);
            }

            return new HttpClientResponse<TResult>(response.StatusCode, resultContent);
        }

        private StringContent SerializeContent<T>(T body)
        {
            var jsonBody = commonJsonSerializer.Serialize(body);
            var content = new StringContent(jsonBody, Encoding.UTF8, "application/json");
            return content;
        }

        protected abstract System.Net.Http.HttpClient CreateClient(Action<HttpRequestHeaders> customizeHeaders = default);
    }
}