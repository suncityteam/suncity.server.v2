﻿using System;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading;
using System.Threading.Tasks;

namespace SunCity.Common.HttpClient
{
    public interface ICommonHttpClient
    {
        Task<HttpClientResponse<TResult>> Get<TResult>(string url, Action<HttpRequestHeaders> customizeHeaders = default, CancellationToken cancellationToken = default);
        
        Task<HttpClientResponse<TResult>> Post<TRequest, TResult>(string url, TRequest body, Action<HttpRequestHeaders> customizeHeaders = default, CancellationToken cancellationToken = default);
        Task<HttpClientResponse> Post<TRequest>(string url, TRequest body, Action<HttpRequestHeaders> customizeHeaders = default, CancellationToken cancellationToken = default);

        Task<HttpClientResponse> Post(string url, FormUrlEncodedContent content, Action<HttpRequestHeaders> customizeHeaders = default, CancellationToken cancellationToken = default);

        Task<HttpClientResponse<TResult>> Put<TRequest, TResult>(string url, TRequest body, Action<HttpRequestHeaders> customizeHeaders = default, CancellationToken cancellationToken = default);
        Task<HttpClientResponse> Put<TRequest>(string url, TRequest body, Action<HttpRequestHeaders> customizeHeaders = default, CancellationToken cancellationToken = default);

        Task<HttpClientResponse<TResult>> Delete<TResult>(string url, Action<HttpRequestHeaders> customizeHeaders = default, CancellationToken cancellationToken = default);
        Task<HttpClientResponse> Delete(string url, Action<HttpRequestHeaders> customizeHeaders = default, CancellationToken cancellationToken = default);


    }
}