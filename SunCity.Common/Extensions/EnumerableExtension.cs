﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace SunCity.Common.Extensions
{
    public static class EnumerableExtension
    {
        //public static bool TryGetValue<TKey, TValue>(this Dictionary<TKey, TValue> dictionary, IReadOnlyCollection<TKey> keys, out TValue value)
        //{
        //    value = default(TValue);
        //
        //    foreach (var key in keys)
        //    {
        //        if (dictionary.TryGetValue(key, out value))
        //        {
        //            return true;
        //        }
        //    }
        //
        //    return false;
        //}

        public static IReadOnlyList<T> DistinctToArray<T>(this IEnumerable<T> InSequence)
        {
            return InSequence.Distinct().ToArray();
        }

        public static IEnumerable<(T item, int index)> WithIndex<T>(this IEnumerable<T> source, int startWith = 0)
        {
            return source.Select((item, index) => (item, index + startWith));
        }
        
        public static IReadOnlyCollection<TResult> SelectNonNull<T, TResult>(this IEnumerable<T> InSequence, Func<T, TResult?> InProjection)
            where TResult : struct
        {
            return InSequence.Select(InProjection).Where(InQ => InQ.HasValue).Select(InQ => InQ.Value).ToArray();
        }

        public static T TakeRandom<T>(this IEnumerable<T> InSource)
        {
            return InSource.TakeRandom(1).SingleOrDefault();
        }

        /// <summary>
        /// <para>Skip elements from begin and from end</para>
        /// <para>By default will take all elements except the first and last</para>
        /// </summary>
        /// <param name="InSkipFromBegin">Count of elements for skip from begin</param>
        /// <param name="InSkipFromEnd">Count of elements for skip from end</param>
        public static IEnumerable<T> ExceptFromBeginAndEnd<T>(this IReadOnlyCollection<T> InSource, int InSkipFromBegin = 1, int InSkipFromEnd = 1)
        {
            return InSource.Skip(InSkipFromBegin).Take(InSource.Count - InSkipFromEnd);
        }

        public static IEnumerable<T> TakeRandom<T>(this IEnumerable<T> InSource, int InCount)
        {
            return InSource.Shuffle().Take(InCount);
        }

        public static IEnumerable<T> Shuffle<T>(this IEnumerable<T> InSource)
        {
            return InSource.OrderBy(InX => Guid.NewGuid());
        }

        public static bool IsEmpty<T>(this IEnumerable<T> InSource)
        {
            return !InSource.Any();
        }

        public static IEnumerable<IEnumerable<T>> Batch<T>(this IEnumerable<T> InSource, int InSize)
            where T : class
        {
            T[] bucket = null;
            var count = 0;

            foreach (var item in InSource)
            {
                if (bucket == null)
                    bucket = new T[InSize];

                bucket[count++] = item;

                if (count != InSize)
                    continue;

                yield return bucket.Select(x => x);

                bucket = null;
                count = 0;
            }

            // Return the last bucket with all remaining elements
            if (bucket != null && count > 0)
            {
                Array.Resize(ref bucket, count);
                yield return bucket.Select(x => x);
            }
        }
    }
}
