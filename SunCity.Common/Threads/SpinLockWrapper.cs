﻿using System;
using System.Threading;

namespace SunCity.Common.Threads
{
    public sealed class SpinLockWrapper : IDisposable
    {
        private bool _locked = false;
        private SpinLock Lock { get; }

        public SpinLockWrapper()
        {
            Lock = new SpinLock(true);
        }

        public SpinLockWrapper TryEnter(TimeSpan InTimeOut)
        {
            var locked = false;
            Lock.TryEnter(InTimeOut, ref _locked);
            return this;
        }

        public void Dispose()
        {
            if (_locked)
            {
                Lock.Exit(true);
            }
        }
    }
}
