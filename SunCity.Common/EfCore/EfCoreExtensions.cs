﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace SunCity.Common.EfCore
{
    public static class EfCoreExtensions
    {
        public static IQueryable<T> HasTracking<T>(this IQueryable<T> query, bool hasTracking) where T : class
        {
            if (hasTracking)
            {
                return query.AsTracking();
            }

            return query.AsNoTracking();
        }
    }
}