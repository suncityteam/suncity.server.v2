﻿using System;
using System.Linq;

namespace SunCity.Common.Reflection
{
    public static class GenericReflectionExtensions
    {
        public static Type[] GetLoadedTypes()
        {
            return AppDomain.CurrentDomain.GetAssemblies()
                .Where(q => !q.IsDynamic && !string.IsNullOrWhiteSpace(q.FullName) &&!q.FullName.StartsWith("VisualStudio"))
                .SelectMany(q => q.GetTypes())
                .ToArray();
        }

        public static bool IsImplementedInterface<TInterface>(this Type InType)
        {
            return InType.GetInterfaces().Any(x => x == typeof(TInterface));
        }

        public static bool IsImplementedClass<TClass>(this Type InType)
        {
            return InType.IsSubclassOf(typeof(TClass));
        }

        public static bool IsImplementedGenericInterface(this Type InType, Type InInteface)
        {
            return InType.GetInterfaces().Any(x =>
                x.IsGenericType &&
                x.GetGenericTypeDefinition() == InInteface);
        }

        public static Type? GetImplementedInterface(this Type InType, Type InInteface)
        {
            return InType.GetInterfaces().First(x =>
                x.IsGenericType &&
                x.GetGenericTypeDefinition() == InInteface);
        }

        public static Type[] GetGenericArguments(this Type InType, Type InInteface)
        {
            return InType.GetImplementedInterface(InInteface)?.GetGenericArguments() ?? Array.Empty<Type>();
        }

        /// <summary>
        /// Extract the "real" type T from Nullable if required
        /// </summary>
        public static Type? UnwrapNullableType(this Type InType)
        {
            if (InType is null) { return null; }

            return Nullable.GetUnderlyingType(InType) ?? InType;
        }
    }
}
