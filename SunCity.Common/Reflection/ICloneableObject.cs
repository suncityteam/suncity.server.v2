﻿namespace SunCity.Common.Reflection
{
    public interface ICloneableObject<out T>
    {
        T CloneObject();
    }
}