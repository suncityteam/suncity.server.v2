﻿using System.Threading.Tasks;
using MediatR;
using SunCity.Common.Operation;

namespace SunCity.Common.MediatR
{
    internal class OperationExecutor : IOperationExecutor
    {
        private readonly IMediator mediator;

        public OperationExecutor(IMediator mediator)
        {
            this.mediator = mediator;
        }

        public Task<OperationResponse> Execute(IOperationRequest request)
        {
            return mediator.Send(request);
        }

        public Task<OperationResponse<TResult>> Execute<TResult>(IOperationRequest<TResult> request)
        {
            return mediator.Send(request);
        }
    }
}