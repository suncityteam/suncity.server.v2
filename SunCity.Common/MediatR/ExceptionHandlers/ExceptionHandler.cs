using System;
using System.Threading;
using System.Threading.Tasks;
using MediatR.Pipeline;
using Microsoft.Extensions.Logging;
using SunCity.Common.Operation;

namespace SunCity.Common.MediatR.ExceptionHandlers
{
    public class ExceptionHandler<TRequest, TResult, TException>
        : IOperationExceptionHandler<TRequest, TResult, TException>
        where TRequest : IOperationRequest<TResult>
        where TException : Exception
    {
        private readonly ILogger<ExceptionHandler<TRequest, TResult, TException>> logger;

        public ExceptionHandler(ILogger<ExceptionHandler<TRequest, TResult, TException>> logger)
        {
            this.logger = logger;
        }

        public Task Handle(TRequest request, TException exception, RequestExceptionHandlerState<OperationResponse<TResult>> state,
            CancellationToken cancellationToken)
        {
            state.SetHandled(OperationResponseCode.ExceptionError);

            this.logger.LogError(exception, exception.Message);
            return Task.CompletedTask;
        }
    }

    public class ExceptionHandler<TRequest, TException>
        : IOperationExceptionHandler<TRequest, TException>
        where TRequest : IOperationRequest
        where TException : Exception
    {
        private readonly ILogger<ExceptionHandler<TRequest, TException>> logger;

        public ExceptionHandler(ILogger<ExceptionHandler<TRequest, TException>> logger)
        {
            this.logger = logger;
        }

        public Task Handle(TRequest request, TException exception, RequestExceptionHandlerState<OperationResponse> state,
            CancellationToken cancellationToken)
        {
            state.SetHandled(OperationResponseCode.ExceptionError);

            this.logger.LogError(exception, exception.Message);
            return Task.CompletedTask;
        }
    }
}