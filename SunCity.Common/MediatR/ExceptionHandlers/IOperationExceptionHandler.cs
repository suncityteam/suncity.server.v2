using System;
using MediatR.Pipeline;
using SunCity.Common.Operation;

namespace SunCity.Common.MediatR.ExceptionHandlers
{
    public interface IOperationExceptionHandler<in TRequest, TResult, TException>
        : IRequestExceptionHandler<TRequest, OperationResponse<TResult>, TException>
        where TRequest : IOperationRequest<TResult>
        where TException : Exception
    {

    }

    public interface IOperationExceptionHandler<in TRequest, TException>
        : IRequestExceptionHandler<TRequest, OperationResponse, TException>
        where TRequest : IOperationRequest
        where TException : Exception
    {

    }
}