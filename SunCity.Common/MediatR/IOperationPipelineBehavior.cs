﻿using MediatR;
using SunCity.Common.Operation;

namespace SunCity.Common.MediatR
{
    public interface IOperationPipelineBehavior<in TRequest, TResult> : IPipelineBehavior<TRequest, OperationResponse<TResult>>
        where TRequest : IOperationRequest<TResult>
    {

    }

    public interface IOperationPipelineBehavior<in TRequest> : IPipelineBehavior<TRequest, OperationResponse>
        where TRequest : IOperationRequest
    {

    }
}