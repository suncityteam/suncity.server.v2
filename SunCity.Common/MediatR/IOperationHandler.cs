﻿using MediatR;
using SunCity.Common.Operation;

namespace SunCity.Common.MediatR
{
    public interface IOperationHandler<in TRequest> : IRequestHandler<TRequest, OperationResponse>
        where TRequest : IOperationRequest
    {

    }

    public interface IOperationHandler<in TRequest, TResult> : IRequestHandler<TRequest, OperationResponse<TResult>> 
        where TRequest : IOperationRequest<TResult>
    {

    }
}