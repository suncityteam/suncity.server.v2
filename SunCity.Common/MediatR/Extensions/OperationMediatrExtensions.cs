﻿using MediatR;
using Microsoft.Extensions.DependencyInjection;
using SunCity.Common.Operation;

namespace SunCity.Common.MediatR.Extensions
{
    public static class OperationMediatrExtensions
    {
        public static IServiceCollection AddOperationExecutor(this IServiceCollection serviceCollection)
        {
            serviceCollection.RegisterOperationExceptionHandlers();
            serviceCollection.AddScoped<IOperationExecutor, OperationExecutor>();
            return serviceCollection;
        }

        public static IServiceCollection AddPipelineBehavior<TRequest, TPipelineBehavior>(this IServiceCollection serviceCollection)
            where TRequest : IOperationRequest
            where TPipelineBehavior : class, IOperationPipelineBehavior<TRequest>
        {
            serviceCollection.AddScoped<IPipelineBehavior<TRequest, OperationResponse>, TPipelineBehavior>();
            return serviceCollection;
        }

        public static IServiceCollection AddPipelineBehavior<TRequest, TResult, TPipelineBehavior>(this IServiceCollection serviceCollection)
            where TRequest : IOperationRequest<TResult>
            where TPipelineBehavior : class, IOperationPipelineBehavior<TRequest, TResult>
        {
            serviceCollection.AddScoped<IPipelineBehavior<TRequest, OperationResponse<TResult>>, TPipelineBehavior>();
            return serviceCollection;
        }

        public static IServiceCollection AddRequestHandler<TRequest, THandler>(this IServiceCollection serviceCollection)
            where TRequest : IOperationRequest
            where THandler : class, IOperationHandler<TRequest>
        {
            serviceCollection.AddScoped<IRequestHandler<TRequest, OperationResponse>, THandler>();
            return serviceCollection;
        }

        public static IServiceCollection AddRequestHandler<TRequest, TResult, THandler>(this IServiceCollection serviceCollection)
            where TRequest : IOperationRequest<TResult>
            where THandler : class, IOperationHandler<TRequest, TResult>
        {
            serviceCollection.AddScoped<IRequestHandler<TRequest, OperationResponse<TResult>>, THandler>();
            return serviceCollection;
        }
    }
}
