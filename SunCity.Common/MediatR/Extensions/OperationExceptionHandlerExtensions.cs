using System;
using System.Linq;
using MediatR.Pipeline;
using Microsoft.Extensions.DependencyInjection;
using SunCity.Common.MediatR.ExceptionHandlers;
using SunCity.Common.Operation;
using SunCity.Common.Reflection;

namespace SunCity.Common.MediatR.Extensions
{
    internal static class OperationExceptionHandlerExtensions
    {
        public static IServiceCollection RegisterOperationExceptionHandlers(this IServiceCollection services)
        {
            //  TODO: Add support custom exception handling, see more in RPX-192
            return services
                .RegisterGenericOperations()
                .RegisterVoidOperations();
        }

        private static IServiceCollection RegisterGenericOperations(this IServiceCollection services)
        {
            var operationRequestTypes = GenericReflectionExtensions
                .GetLoadedTypes()
                .Where(type => type.IsImplementedGenericInterface(typeof(IOperationRequest<>)))
                .ToArray();

            foreach (var requestType in operationRequestTypes)
            {
                var resultType = requestType
                    .GetGenericArguments(typeof(IOperationRequest<>))[0];

                var resultTypeInOperationResponse =
                    typeof(OperationResponse<>)
                        .MakeGenericType(resultType);

                var exceptionHandlerBase =
                    typeof(IRequestExceptionHandler<,,>)
                        .MakeGenericType(requestType, resultTypeInOperationResponse, typeof(Exception));

                var exceptionHandlerImpl =
                    typeof(ExceptionHandler<,,>)
                        .MakeGenericType(requestType, resultType, typeof(Exception));

                services.AddScoped(exceptionHandlerBase, exceptionHandlerImpl);
            }

            return services;
        }

        private static IServiceCollection RegisterVoidOperations(this IServiceCollection services)
        {
            var operationRequestTypes = GenericReflectionExtensions
                .GetLoadedTypes()
                .Where(type => type.IsImplementedGenericInterface(typeof(IOperationRequest)))
                .ToArray();

            foreach (var requestType in operationRequestTypes)
            {
                var resultType = typeof(OperationResponse);

                var exceptionHandlerBase =
                    typeof(IRequestExceptionHandler<,,>)
                        .MakeGenericType(requestType, resultType, typeof(Exception));

                var exceptionHandlerImpl =
                    typeof(ExceptionHandler<,,>)
                        .MakeGenericType(requestType, resultType, typeof(Exception));

                services.AddScoped(exceptionHandlerBase, exceptionHandlerImpl);
            }

            return services;
        }
    }
}