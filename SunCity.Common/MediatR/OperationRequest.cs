﻿using MediatR;
using SunCity.Common.Operation;

namespace SunCity.Common.MediatR
{
    public interface IOperationRequest : IRequest<OperationResponse>
    {

    }

    public interface IOperationRequest<TResult> : IRequest<OperationResponse<TResult>>
    {

    }
}
