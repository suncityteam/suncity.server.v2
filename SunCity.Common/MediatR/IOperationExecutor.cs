﻿using System.Threading.Tasks;
using SunCity.Common.Operation;

namespace SunCity.Common.MediatR
{
    public interface IOperationExecutor
    {
        Task<OperationResponse> Execute(IOperationRequest request);
        Task<OperationResponse<TResult>> Execute<TResult>(IOperationRequest<TResult> request);
    }
}