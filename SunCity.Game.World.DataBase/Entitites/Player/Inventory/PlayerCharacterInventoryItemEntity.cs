﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using SunCity.Domain.Interfaces.Entity;
using SunCity.Game.Enums.World.Player.Inventory;
using SunCity.Game.Types.Player.Entity.Player.Character;
using SunCity.Game.World.DataBase.Entitites.Item;
using SunCity.Game.World.DataBase.Entitites.Item.World;
using SunCity.Game.World.DataBase.Entitites.Player.Character;

namespace SunCity.Game.World.DataBase.Entitites.Player.Inventory
{
    public class PlayerCharacterInventoryItemEntity : IEntityWithId<CharacterInventoryItemId>
    {
        //=======================================
        [Key]
        public virtual CharacterInventoryItemId EntityId { get; set; }
        public virtual PlayerInventorySlot InventorySlot { get; set; }

        //=======================================
        [ForeignKey(nameof(CharacterEntity))]
        public virtual PlayerCharacterId CharacterId { get; set; }
        public virtual PlayerCharacterEntity CharacterEntity { get; set; }

        //=======================================
        [ForeignKey(nameof(ItemEntity))]
        public virtual WorldItemId ItemId { get; set; }
        public virtual WorldItemEntity ItemEntity { get; set; }

        //=======================================
        [Obsolete("Remove it")]
        public short Amount { get; set; }
    }
}
