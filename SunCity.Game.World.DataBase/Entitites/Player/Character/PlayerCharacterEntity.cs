﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;
using SunCity.Common.Types.Math;
using SunCity.Domain.Interfaces.Entity;
using SunCity.Game.Types.Player.Components;
using SunCity.Game.Types.Player.Entity.Player;
using SunCity.Game.Types.Player.Entity.Player.Character;
using SunCity.Game.Types.Property.Entity.Property;
using SunCity.Game.World.DataBase.Entitites.Fraction;
using SunCity.Game.World.DataBase.Entitites.Player.Inventory;
using SunCity.Game.World.DataBase.Entitites.Player.Licenses;
using SunCity.Game.World.DataBase.Entitites.Property;
using SunCity.Identity.DataBase.Keys;

namespace SunCity.Game.World.DataBase.Entitites.Player.Character
{
    public class PlayerCharacterEntity 
        : IEntityWithId<PlayerCharacterId>
        , IEntityWithName<PlayerCharacterName>
        , IEntityWithCreatedDate
    {
        [Key]
        public virtual PlayerCharacterId EntityId { get; set; }

        [Required]
        [MaxLength(64)]
        public virtual PlayerCharacterName Name { get; set; }

        public virtual bool IsBlocked { get; set; }

        [ForeignKey(nameof(PlayerAccountEntity))]
        public virtual AccountId PlayerAccountId { get; set; }
        public virtual PlayerAccountEntity PlayerAccountEntity { get; set; }

        public virtual PlayerCharacterExperienceEntity Experience { get; set; }
        
        public virtual PlayerCharacterSkinEntity Skin { get; set; }

        public virtual List<PlayerCharacterInventoryItemEntity> InventoryItems { get; set; }
        public virtual List<FractionMemberEntity> FractionMembers { get; set; }
        public virtual List<PlayerLicenseEntity> Licences { get; set; }
        
        public virtual Health Health { get; set; }
        public virtual Hunger Hunger { get; set; }
        public virtual Hunger Thirst { get; set; }

        [ForeignKey(nameof(EnteredPropertyEntity))]
        public virtual PropertyId? EnteredPropertyId { get; set; }
        public virtual PropertyEntity EnteredPropertyEntity { get; set; }

        public virtual Vector3D Position { get; set; }
        public virtual float Rotation { get; set; }

        public virtual DateTime CreatedDate { get; set; }
    }
}
