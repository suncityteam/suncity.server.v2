﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Threading.Tasks;
using AutoMapper;
using SunCity.Domain.Interfaces.Entity;
using SunCity.Game.Types.Player.Entity.Player.Character;
using SunCity.Game.World.DataBase.Entitites.Player.Interfaces;

namespace SunCity.Game.World.DataBase.Entitites.Player.Character
{
    public class PlayerCharacterExperienceEntity
        : IWithPlayerCharacterEntity
        , IEntityWithExperience
    {
        [Key]
        [ForeignKey(nameof(CharacterEntity))]
        public virtual PlayerCharacterId EntityId { get; set; }
        public virtual PlayerCharacterEntity CharacterEntity { get; set; }

        public byte Level { get; set; }
        public long Experience { get; set; }

        public TimeSpan PlayedTotalTime { get; set; }

        /// <summary>
        /// Время отыгранное за час.
        /// Необходимо что бы выдавать Payday если игрок отыграл меньше часа
        /// </summary>
        public TimeSpan PlayedForPayDay { get; set; }
    }
}