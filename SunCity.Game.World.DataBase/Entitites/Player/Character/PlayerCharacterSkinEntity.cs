﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using SunCity.Game.Enums.Character;
using SunCity.Game.Interfaces.Player.Character;
using SunCity.Game.Types.Player.Entity.Player.Character;
using SunCity.Game.World.DataBase.Entitites.Player.Interfaces;

namespace SunCity.Game.World.DataBase.Entitites.Player.Character
{
    public class PlayerCharacterSkinEntity
        : IWithPlayerCharacterEntity
        , IPlayerCharacterSkin
    {
        //===============================================
        [Key]
        [ForeignKey(nameof(CharacterEntity))]
        public virtual PlayerCharacterId EntityId { get; set; }
        public virtual PlayerCharacterEntity CharacterEntity { get; set; }

        //===============================================
        public GameCharacterSex Sex { get; set; }

        //===============================================
        public virtual byte FirstHeadShape { get; set; }
        public virtual byte SecondHeadShape { get; set; }

        public virtual byte FirstSkinTone { get; set; }
        public virtual byte SecondSkinTone { get; set; }

        public virtual float HeadMix { get; set; }
        public virtual float SkinMix { get; set; }

        public virtual byte HairModel { get; set; }
        public virtual byte FirstHairColor { get; set; }
        public virtual byte SecondHairColor { get; set; }

        public virtual short BeardModel { get; set; }
        public virtual short BeardColor { get; set; }

        public virtual short ChestModel { get; set; }
        public virtual short ChestColor { get; set; }

        public virtual short BlemishesModel { get; set; }
        public virtual short AgeingModel { get; set; }
        public virtual short ComplexionModel { get; set; }
        public virtual short SundamageModel { get; set; }
        public virtual short FrecklesModel { get; set; }

        public virtual byte EyesColor { get; set; }
        public virtual short EyebrowsModel { get; set; }
        public virtual short EyebrowsColor { get; set; }

        public virtual short MakeupModel { get; set; }
        public virtual short BlushModel { get; set; }
        public virtual short BlushColor { get; set; }
        public virtual short LipstickModel { get; set; }
        public virtual short LipstickColor { get; set; }


        public virtual float NoseWidth { get; set; }
        public virtual float NoseHeight { get; set; }
        public virtual float NoseLength { get; set; }
        public virtual float NoseBridge { get; set; }
        public virtual float NoseTip { get; set; }
        public virtual float NoseShift { get; set; }
        public virtual float BrowHeight { get; set; }
        public virtual float BrowWidth { get; set; }
        public virtual float CheekboneHeight { get; set; }
        public virtual float CheekboneWidth { get; set; }
        public virtual float CheeksWidth { get; set; }
        public virtual float Eyes { get; set; }
        public virtual float Lips { get; set; }
        public virtual float JawWidth { get; set; }
        public virtual float JawHeight { get; set; }
        public virtual float ChinLength { get; set; }
        public virtual float ChinPosition { get; set; }
        public virtual float ChinWidth { get; set; }
        public virtual float ChinShape { get; set; }
        public virtual float NeckWidth { get; set; }
    }
}