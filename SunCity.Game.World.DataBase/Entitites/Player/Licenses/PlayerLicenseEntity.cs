﻿using System;
using SunCity.Domain.Interfaces.Entity;
using SunCity.Game.World.DataBase.Entitites.Licenses;
using SunCity.Game.World.DataBase.Entitites.Player.Character;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SunCity.Game.World.DataBase.Entitites.Player.Licenses
{
    public class PlayerLicenseEntity        
        : IEntityWithId<PlayerLicenseId>
        , IEntityWithCreatedDate
        , IEntityWithExpiredDate
    {
        //======================================
        [Key]
        public virtual PlayerLicenseId EntityId { get; set; }
        public virtual DateTime CreatedDate { get; set; }
        public virtual DateTime ExpiredDate { get; set; }
        
        //===================================================
        [ForeignKey(nameof(CharacterEntity))]
        public virtual PlayerCharacterId CharacterId { get; set; }
        public virtual PlayerCharacterEntity CharacterEntity { get; set; }

        //===================================================
        [ForeignKey(nameof(LicenseEntity))]
        public virtual LicenseId LicenseId { get; set; }
        public virtual GameLicenseEntity LicenseEntity { get; set; }
    }
}