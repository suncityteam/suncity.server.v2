﻿using System;
using System.Collections.Generic;
using System.Text;
using SunCity.Domain.Interfaces.Entity;
using SunCity.Game.Types.Player.Entity.Player.Character;
using SunCity.Game.World.DataBase.Entitites.Player.Character;

namespace SunCity.Game.World.DataBase.Entitites.Player.Interfaces
{
    public interface IWithPlayerCharacterEntity : IEntityWithId<PlayerCharacterId>
    {
        PlayerCharacterEntity CharacterEntity { get; set; }
    }
}
