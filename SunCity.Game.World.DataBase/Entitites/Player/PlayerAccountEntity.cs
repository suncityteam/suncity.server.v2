﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using SunCity.Domain.Interfaces.Entity;
using SunCity.Game.Types.Player.Entity.Player;
using SunCity.Game.Types.Player.Entity.Player.Character;
using SunCity.Game.World.DataBase.Entitites.Player.Character;
using SunCity.Identity.DataBase.Keys;

namespace SunCity.Game.World.DataBase.Entitites.Player
{
    public class PlayerAccountEntity 
        : IEntityWithId<AccountId>
        , IEntityWithCreatedDate
    {
        [Key]
        public virtual AccountId EntityId { get; set; }

        [Required]
        [MaxLength(32)]
        public virtual PlayerAccountLogin Login { get; set; }

        public virtual long Balance { get; set; }

        [InverseProperty(nameof(PlayerCharacterEntity.PlayerAccountEntity))]
        public virtual List<PlayerCharacterEntity> CharacterEntities { get; set; }

        [ForeignKey(nameof(DefaultCharacterEntity))]
        public virtual PlayerCharacterId? DefaultCharacterId { get; set; }
        public virtual PlayerCharacterEntity DefaultCharacterEntity { get; set; }

        public virtual DateTime PremiumAccountEndDate { get; set; }
        public virtual DateTime CreatedDate { get; set; }
        public virtual string DeviceId { get; set; }
    }
}
