﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;
using SunCity.Domain.Interfaces.Entity;
using SunCity.Game.Enums.Item;
using SunCity.Game.World.DataBase.Entitites.Item.Category;
using SunCity.Game.World.DataBase.Entitites.Item.Category.Keys;

namespace SunCity.Game.World.DataBase.Entitites.Item
{
    public class GameItemEntity
        : IEntityWithId<GameItemId>
    {
        //====================================
        [Key]
        public virtual GameItemId EntityId { get; set; }

        [Required]
        [MaxLength(32)]
        public virtual string Name { get; set; }
        public virtual long? ModelHash { get; set; }

        [MaxLength(8192)]
        public virtual string Description { get; set; }


        [MaxLength(64)]
        public virtual string ImageClass { get; set; }
        //====================================
        public virtual ItemCategory ItemCategory { get; set; }
        
        //====================================
        /// <summary>
        /// Вес
        /// </summary>
        public virtual int Weight { get; set; }

        /// <summary>
        /// Вместимость
        /// </summary>
        public virtual int Capacity { get; set; }

        //====================================
        public virtual short? Stack { get; set; }

        //====================================
        public virtual DrinkItemEntity DrinkEntity { get; set; }
        public virtual FoodItemEntity FoodEntity { get; set; }
        public virtual DrugItemEntity DrugEntity { get; set; }

        //====================================
        public virtual ClothingItemEntity ClothingEntity { get; set; }

        //====================================
        public virtual ResourceItemEntity ResourceEntity { get; set; }

        //====================================
        public virtual VehicleKeyItemEntity VehicleKeyItemEntity { get; set; }
        public virtual PropertyKeyItemEntity PropertyKeyItemEntity { get; set; }
       
        //====================================
        public override string ToString()
        {
            return $"[GameItemEntity][{EntityId} / {ItemCategory}][{Name}]";
        }
    }
}
