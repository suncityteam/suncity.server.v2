﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using SunCity.Game.Enums.World.Player.Inventory;
using SunCity.Game.World.DataBase.Entitites.Item.Interfaces;

namespace SunCity.Game.World.DataBase.Entitites.Item.Category
{
    public class ClothingItemEntity : IWithItemEntity
    {
        [Key]
        [ForeignKey(nameof(ItemEntity))]
        public virtual GameItemId EntityId { get; set; }
        public virtual GameItemEntity ItemEntity { get; set; }

        //========================================
        public virtual PlayerInventorySlot Slot { get; set; }
        //public virtual ClothSlot ClothSlot { get; set; }
        public virtual int ClothSlot { get; set; }
        public virtual byte Drawable { get; set; }
        public virtual byte Texture { get; set; }
        public virtual byte Palette { get; set; }

        //========================================
        public virtual int Temperature { get; set; }


    }
}
