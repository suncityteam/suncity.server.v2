﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using SunCity.Game.World.DataBase.Entitites.Item.Interfaces;

namespace SunCity.Game.World.DataBase.Entitites.Item.Category
{
    public class DrugItemEntity : IWithItemEntity
    {
        [Key]
        [ForeignKey(nameof(ItemEntity))]
        public virtual GameItemId EntityId { get; set; }
        public virtual GameItemEntity ItemEntity { get; set; }

        //========================================
        /// <summary>
        /// Алкоголь, опьянение
        /// </summary>
        public virtual int Alcohol { get; set; }

        /// <summary>
        /// Сытость
        /// </summary>
        public virtual int Satiety { get; set; }

        /// <summary>
        /// Зависимость
        /// </summary>
        public virtual int Addiction { get; set; }

        //========================================
        public virtual int HealthRegenerationAmount { get; set; }
        public virtual TimeSpan HealthRegenerationTime { get; set; }
        public virtual TimeSpan HealthRegenerationInterval { get; set; }

        //========================================
        public override string ToString()
        {
            return $"[GameItemEntity][{EntityId} / {ItemEntity?.ItemCategory}][{ItemEntity?.Name}][Alcohol: {Alcohol}][Addiction: {Addiction}][Satiety: {Satiety}][HealthRegeneration: {HealthRegenerationAmount}]";
        }

    }
}