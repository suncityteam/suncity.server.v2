﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using SunCity.Game.World.DataBase.Entitites.Item.Interfaces;

namespace SunCity.Game.World.DataBase.Entitites.Item.Category
{
    public class FoodItemEntity : IWithItemEntity
    {
        [Key]
        [ForeignKey(nameof(ItemEntity))]
        public virtual GameItemId EntityId { get; set; }
        public virtual GameItemEntity ItemEntity { get; set; }

        //========================================
        /// <summary>
        /// Сытость
        /// </summary>
        public virtual int Satiety { get; set; }

        /// <summary>
        /// Срок годности
        /// </summary>
        public virtual TimeSpan ExpirationDate { get; set; }

        //==================================
        public override string ToString()
        {
            return $"[GameItemEntity][{EntityId} / {ItemEntity?.ItemCategory}][{ItemEntity?.Name}][Satiety: {Satiety}][ExpirationDate: {ExpirationDate}]";
        }
    }
}