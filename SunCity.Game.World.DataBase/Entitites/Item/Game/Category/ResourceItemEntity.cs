﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using SunCity.Game.World.DataBase.Entitites.Item.Interfaces;

namespace SunCity.Game.World.DataBase.Entitites.Item.Category
{
    public class ResourceItemEntity : IWithItemEntity
    {
        [Key]
        [ForeignKey(nameof(ItemEntity))]
        public virtual GameItemId EntityId { get; set; }
        public virtual GameItemEntity ItemEntity { get; set; }
    }
}