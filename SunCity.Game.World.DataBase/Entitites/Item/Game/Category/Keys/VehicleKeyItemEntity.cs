﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using SunCity.Common.Enum;
using SunCity.Game.World.DataBase.Entitites.Item.Interfaces;

namespace SunCity.Game.World.DataBase.Entitites.Item.Category.Keys
{
    public enum VehicleKeyCategory : byte
    {
        /// <summary>
        /// <para>Ключ от машины</para>
        /// </summary>
        [EnumDescription("Ключ от машины")]
        Car,
        
        /// <summary>
        /// <para>Ключ от вертолета</para>
        /// </summary>
        [EnumDescription("Ключ от вертолета")]
        Helicopter,
        
        /// <summary>
        /// <para>Ключ от мотоцикла</para>
        /// </summary>
        [EnumDescription("Ключ от мотоцикла")]
        Motorcycle,
        
        /// <summary>
        /// <para>Ключ от самолета</para>
        /// </summary>
        [EnumDescription("Ключ от самолета")]
        Airplane,
        
        /// <summary>
        /// <para>Ключ от лодки</para>
        /// </summary>
        [EnumDescription("Ключ от лодки")]
        Boat,
        
        /// <summary>
        /// <para>Ключ от автобуса</para>
        /// </summary>
        [EnumDescription("Ключ от автобуса")]
        Bus,
        
        /// <summary>
        /// <para>Ключ от квадроцикла</para>
        /// </summary>
        [EnumDescription("Ключ от квадроцикла")]
        QuadBike,
        
        /// <summary>
        /// <para>Ключ от дерижабля</para>
        /// </summary>
        [EnumDescription("Ключ от дерижабля")]
        Airship,
                
        /// <summary>
        /// <para>Ключ от трактора</para>
        /// </summary>
        [EnumDescription("Ключ от трактора")]
        Tractor,
        
        /// <summary>
        /// <para>Ключ от поезда</para>
        /// </summary>
        [EnumDescription("Ключ от поезда")]
        Train,
    }
    
    public class VehicleKeyItemEntity: IWithItemEntity
    {
        [Key]
        [ForeignKey(nameof(ItemEntity))]
        public virtual GameItemId EntityId { get; set; }
        public virtual GameItemEntity ItemEntity { get; set; }
        public virtual VehicleKeyCategory Category { get; set; }
    }
}