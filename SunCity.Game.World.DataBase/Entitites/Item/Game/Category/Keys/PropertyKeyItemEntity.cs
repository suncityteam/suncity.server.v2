﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using SunCity.Common.Enum;
using SunCity.Game.World.DataBase.Entitites.Item.Interfaces;

namespace SunCity.Game.World.DataBase.Entitites.Item.Category.Keys
{
    public enum PropertyKeyCategory : byte
    {
        /// <summary>
        /// <para>Ключ от дома</para>
        /// </summary>
        [EnumDescription("Ключ от дома")]
        House,
        
        /// <summary>
        /// <para>Ключ от квартиры</para>
        /// </summary>
        [EnumDescription("Ключ от квартиры")]
        Apartment,
        
        /// <summary>
        /// <para>Ключ от гаража</para>
        /// </summary>
        [EnumDescription("Ключ от гаража")]
        Garage,
        
        /// <summary>
        /// <para>Ключ от номера в отеле</para>
        /// </summary>
        [EnumDescription("Ключ от номера в отеле")]
        HotelRoom,
        
        /// <summary>
        /// <para>Ключ от склада</para>
        /// </summary>
        [EnumDescription("Ключ от склада")]
        Warehouse,
        
        /// <summary>
        /// <para>Ключ от от бизнеса</para>
        /// </summary>
        [EnumDescription("Ключ от бизнеса")]
        Business,
    }
    
    public class PropertyKeyItemEntity: IWithItemEntity
    {
        [Key]
        [ForeignKey(nameof(ItemEntity))]
        public virtual GameItemId EntityId { get; set; }
        public virtual GameItemEntity ItemEntity { get; set; }
        public virtual PropertyKeyCategory Category { get; set; }
    }
}