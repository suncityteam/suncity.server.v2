﻿using System;
using System.Collections.Generic;
using System.Text;
using SunCity.Domain.Interfaces.Entity;

namespace SunCity.Game.World.DataBase.Entitites.Item.Interfaces
{
    public interface IWithItemEntity : IEntityWithId<GameItemId>
    {
        GameItemEntity ItemEntity { get; set; }
    }
}
