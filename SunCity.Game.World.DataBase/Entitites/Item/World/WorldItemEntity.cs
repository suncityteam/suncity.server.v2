﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using SunCity.Domain.Interfaces.Entity;
using SunCity.Game.Types.Player.Entity.Player.Character;

namespace SunCity.Game.World.DataBase.Entitites.Item.World
{
    public enum WorldItemGroup
    {
        None,
        Player,
        Vehicle,
        Property,
        Actor,
    }

    public class WorldItemData
    {
        public Guid? Guid { get; set; }
    }

    public class WorldItemEntity : IEntityWithId<WorldItemId>
    { 
        //===================================
        [Key]
        public virtual WorldItemId EntityId { get; set; }
        public virtual WorldItemGroup Group { get; set; }

        //===================================
        [ForeignKey(nameof(ModelEntity))]
        public virtual GameItemId ModelId { get; set; }
        public virtual GameItemEntity ModelEntity { get; set; }

        //===================================
        public virtual DateTime CreatedDate { get; set; }

        //=======================================
        [ForeignKey(nameof(ParentEntity))]
        public virtual WorldItemId? ParentId { get; set; }
        public virtual WorldItemEntity ParentEntity { get; set; }

        //=======================================
        public virtual List<WorldItemEntity> ChildEntities { get; set; } = new List<WorldItemEntity>(32);

        [Column(TypeName = "jsonb")]
        public virtual WorldItemData Data { get; set; }

    }
}
