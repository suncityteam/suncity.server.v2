﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using SunCity.Domain.Interfaces.Entity;
using SunCity.Game.Enums.Fraction;

namespace SunCity.Game.World.DataBase.Entitites.Fraction
{
    public class FractionRankEntity
        : IEntityWithId<FractionRankId>
        , IEntityWithCreatedDate
    {
        //==========================================
        [Key]
        public virtual FractionRankId EntityId { get; set; }
        public virtual GameFractionRankAccess Access { get; set; }

        //==========================================
        public virtual bool Deleted { get; set; }

        //==========================================
        [Required]
        [MaxLength(64)]
        public virtual string Name { get; set; }
        public virtual DateTime CreatedDate { get; set; }

        //==========================================
        public virtual FractionId FractionId { get; set; }
        public virtual FractionEntity FractionEntity { get; set; }

        //==========================================
        /// <summary>
        /// Зарплата во время игры каждый час
        /// </summary>
        public int SalaryPerHour { get; set; }

        /// <summary>
        /// Еженедельная зарпалата, даже если игрок не заходил в игру
        /// </summary>
        public int SalaryPerWeek { get; set; }

        //==========================================
        public virtual List<FractionMemberEntity> MemberEntities { get; set; }
    }
}
