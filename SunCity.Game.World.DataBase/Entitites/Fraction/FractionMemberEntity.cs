﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using SunCity.Domain.Interfaces.Entity;
using SunCity.Game.Types.Player.Entity.Player.Character;
using SunCity.Game.World.DataBase.Entitites.Player.Character;

namespace SunCity.Game.World.DataBase.Entitites.Fraction
{
    public class FractionMemberEntity
        : IEntityWithId<FractionMemberId>
        , IEntityWithCreatedDate
    {
        //==========================================
        [Key]
        public virtual FractionMemberId EntityId { get; set; }

        //==========================================
        [ForeignKey(nameof(FractionEntity))]
        public virtual FractionId FractionId { get; set; }
        public virtual FractionEntity FractionEntity { get; set; }

        //==========================================
        [ForeignKey(nameof(FractionRankEntity))]
        public virtual FractionRankId FractionRankId { get; set; }
        public virtual FractionRankEntity FractionRankEntity { get; set; }

        //==========================================
        [ForeignKey(nameof(CharacterEntity))]
        public virtual PlayerCharacterId CharacterId { get; set; }
        public virtual PlayerCharacterEntity CharacterEntity { get; set; }

        //==========================================
        public virtual DateTime CreatedDate { get; set; }
        public virtual DateTime? LeaveDate { get; set; }

        //==========================================
        public virtual TimeSpan WeekAtivity { get; set; }
    }
}
