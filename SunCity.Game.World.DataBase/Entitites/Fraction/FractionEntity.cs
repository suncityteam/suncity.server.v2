﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using SunCity.Domain.Interfaces.Entity;
using SunCity.Game.Enums.Fraction;
using SunCity.Game.World.DataBase.Entitites.Property.Business.Bank.Account;
using SunCity.Game.World.DataBase.Entitites.Property.HeadQuarter;

namespace SunCity.Game.World.DataBase.Entitites.Fraction
{
    public class FractionEntity
        : IEntityWithId<FractionId>
    {
        //==============================================
        [Key]
        public virtual FractionId EntityId { get; set; }
        public virtual FractionType Type { get; set; }
        public virtual FractionCategory Category { get; set; }

        //==============================================
        [ForeignKey(nameof(HeadQuarterPropertyEntity))]
        public virtual PropertyId HeadQuarterPropertyId { get; set; }
        public virtual HeadQuarterPropertyEntity HeadQuarterPropertyEntity { get; set; }

        //==============================================
        [ForeignKey(nameof(MainBankCardAccountEntity))]
        public virtual BankCardId? MainBankCardId { get; set; }
        public virtual BankCardFractionAccountEntity MainBankCardAccountEntity { get; set; }

        //==============================================
        [Required]
        [MaxLength(64)]
        public virtual string Name { get; set; }

        [Required]
        [MaxLength(16)]
        public virtual string ShortName { get; set; }

        //==============================================
        public short MapIcon { get; set; }


        //==============================================
        public virtual List<FractionRankEntity> RankEntities { get; set; }
        public virtual List<FractionMemberEntity> MemberEntities { get; set; }

        //==============================================
        //public virtual List<BankCardFractionAccountEntity> BankCardAccountEntities { get; set; }

    }
}
