﻿using System.ComponentModel.DataAnnotations;
using System.Threading.Tasks;
using GTANetworkAPI;
using SunCity.Domain.Interfaces.Entity;
using SunCity.Game.Enums.Vehicle;
using SunCity.Game.Types.Cost;

namespace SunCity.Game.World.DataBase.Entitites.Vehicle
{
    public class VehicleModelEntity : IEntityWithId<VehicleHash>
    {
        [Key]
        public virtual VehicleHash EntityId { get; set; }

        [Required]
        [MaxLength(32)]
        public virtual string Name { get; set; }

        public virtual VehicleModelClass ModelClass { get; set; }


        public virtual VehicleFuelType FuelType { get; set; }
        public virtual float FuelTankSize { get; set; }
        
        /// <summary>
        /// Расход топлива в литрах за минуту
        /// </summary>
        public virtual float FuelConsumptionPerMinute { get; set; }
        
        /// <summary>
        /// <para>Управляемость в процентах</para>
        /// </summary>
        public virtual byte Handleability { get; set; }
        
        /// <summary>
        /// <para>Вместимость багажника</para>
        /// </summary>
        public virtual short BagCapacity { get; set; }
        
        /// <summary>
        /// <para>Количество мест</para>
        /// </summary>
        public virtual byte NumberOfSeats { get; set; }
        
        /// <summary>
        /// <para>Разгон 0-100 за N сек</para>
        /// </summary>
        public virtual float Acceleration { get; set; }
        
        /// <summary>
        /// <para>Максимальная скорость в км/ч</para>
        /// </summary>
        public virtual short MaxSpeed { get; set; }
        
        public virtual GameCost MinCost { get; set; }
        public virtual GameCost MaxCost { get; set; }
    }
}