﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using GTANetworkAPI;
using SunCity.Common.Types.Math;
using SunCity.Domain.Interfaces.Entity;
using SunCity.Game.Enums.Vehicle;
using SunCity.Game.Interfaces.Entity;
using SunCity.Game.Types.Vehicle;
using SunCity.Game.Types.Vehicle.States;

namespace SunCity.Game.World.DataBase.Entitites.Vehicle
{
    public class VehicleEntity
        : IEntityWithId<VehicleId>
        , IEntityWithPosition<Vector3D>
        , IEntityWithRotation
        , IEntityWithDeleted
    {
        [Key]
        public VehicleId EntityId { get; set; }
    
        [ForeignKey(nameof(ModelEntity))]
        public virtual VehicleHash ModelId { get; set; }
        public virtual VehicleModelEntity ModelEntity { get; set; }

        public Vector3D Position { get; set; }
        public float Rotation { get; set; }
        public float Fuel { get; set; }


        public VehicleColor Color { get; set; }
        public VehicleHealth Health { get; set; }

        /// <summary>
        /// <para>Value in range [0-15]</para>
        /// <para>https://wiki.rage.mp/index.php?title=Vehicle::setDirtLevel</para>
        /// </summary>
        public byte DirtyLevel { get; set; }
        public double Mileage { get; set; }

        public VehiclePartsState PartsState { get; set; }
        public VehicleWindowsState WindowsState { get; set; }
        public VehicleWheelsState WheelsState { get; set; }
        public VehicleDoorsState DoorsState { get; set; }
        public VehiclePlateNumber PlateNumber { get; set; }
        
        public bool Deleted { get; set; }
        public DateTime? DeletedDate { get; set; }
    }
}
