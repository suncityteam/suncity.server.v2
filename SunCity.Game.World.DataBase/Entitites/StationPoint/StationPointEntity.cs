﻿using System;
using System.ComponentModel.DataAnnotations;
using SunCity.Common.Types.Math;
using SunCity.Domain.Interfaces.Entity;
using SunCity.Game.Interfaces.Entity;

namespace SunCity.Game.World.DataBase.Entitites.StationPoint
{
    /// <summary>
    /// <para>Station Points:</para>
    /// <para>-  Bus stations</para>
    /// <para>- metro</para>
    /// <para>- train</para>
    /// <para>- etc</para>
    /// </summary>
    public class StationPointEntity
        : IEntityWithId<StationPointId>
        , IEntityWithCreatedDate
        , IEntityWithDeleted
        , IEntityWithPosition<Vector3D> 
    {
        //==================================
        [Key]
        public virtual StationPointId EntityId { get; set; }
        public virtual DateTime CreatedDate { get; set; }
        public virtual string Name { get; set; }
        
        //==================================
        public virtual bool Deleted { get; set; }
        public virtual DateTime? DeletedDate { get; set; }
        
        //==================================
        public virtual Vector3D Position { get; }
    }
}