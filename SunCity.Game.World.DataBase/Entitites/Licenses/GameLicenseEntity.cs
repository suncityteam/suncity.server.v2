﻿using System;
using System.ComponentModel.DataAnnotations;
using SunCity.Domain.Interfaces.Entity;

namespace SunCity.Game.World.DataBase.Entitites.Licenses
{
    public class GameLicenseEntity
        : IEntityWithId<LicenseId>
        , IEntityWithCreatedDate
    {
        [Key] 
        public virtual LicenseId EntityId { get; set; }
        
        [Required]
        [MaxLength(64)]
        public virtual string Name { get; set; }
        public virtual DateTime CreatedDate { get; set; }
    }
}