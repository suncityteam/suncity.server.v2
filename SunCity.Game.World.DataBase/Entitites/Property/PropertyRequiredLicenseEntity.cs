﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using SunCity.Domain.Interfaces.Entity;
using SunCity.Game.World.DataBase.Entitites.Licenses;

namespace SunCity.Game.World.DataBase.Entitites.Property
{
    public class PropertyRequiredLicenseEntity 
        : IEntityWithId<PropertyRequiredLicenseId>
        , IEntityWithCreatedDate
        , IEntityWithDeleted
    {
        //=====================================
        [Key]
        public virtual PropertyRequiredLicenseId EntityId { get; set; }
        public virtual DateTime CreatedDate { get;set; }

        //=====================================
        [ForeignKey(nameof(PropertyEntity))]
        public virtual PropertyId PropertyId { get; set; }
        public virtual PropertyEntity PropertyEntity { get; set; }

        //=====================================
        [ForeignKey(nameof(RequiredLicenseEntity))]
        public virtual LicenseId RequiredLicenseId { get; set; }
        public virtual GameLicenseEntity RequiredLicenseEntity { get; set; }
        
        //=====================================
        public virtual bool Deleted { get; set; }
        public virtual DateTime? DeletedDate { get; set; }
        
        //=====================================
        [MaxLength(512)]
        public string AdminComment { get; set; }
    }
}