﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using SunCity.Game.Enums.Fraction;
using SunCity.Game.World.DataBase.Entitites.Property.Interfaces;

namespace SunCity.Game.World.DataBase.Entitites.Property.HeadQuarter
{
    /// <summary>
    /// Штаб квартиры фракций
    /// </summary>
    public class HeadQuarterPropertyEntity : IWithPropertyEntity
    {
        //=======================================================
        [Key]
        [ForeignKey(nameof(PropertyEntity))]
        public virtual PropertyId EntityId { get; set; }
        public virtual PropertyEntity PropertyEntity { get; set; }

        //=======================================================
        public virtual FractionCategory Category { get; set; }

        //=======================================================
        public virtual int Level { get; set; }
        public virtual long Experience { get; set; }
    }
}
