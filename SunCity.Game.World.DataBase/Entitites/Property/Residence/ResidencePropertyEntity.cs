﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using SunCity.Game.Types.Property.Entity.Property;
using SunCity.Game.World.DataBase.Entitites.Property.Interfaces;

namespace SunCity.Game.World.DataBase.Entitites.Property.Residence
{
    public class ResidencePropertyEntity : IWithPropertyEntity
    {
        [Key]
        [ForeignKey(nameof(PropertyEntity))]
        public virtual PropertyId EntityId { get; set; }
        public virtual PropertyEntity PropertyEntity { get; set; }
    }
}
