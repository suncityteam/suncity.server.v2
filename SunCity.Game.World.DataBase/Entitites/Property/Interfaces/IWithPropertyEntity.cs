﻿using System;
using System.Collections.Generic;
using System.Text;
using SunCity.Domain.Interfaces.Entity;
using SunCity.Game.Types.Property.Entity.Property;
using SunCity.Game.World.DataBase.Entitites.Property.Business;
using SunCity.Game.World.DataBase.Entitites.Property.Residence;

namespace SunCity.Game.World.DataBase.Entitites.Property.Interfaces
{
    public interface IWithBusinessPropertyEntity : IEntityWithId<PropertyId>
    {
        BusinessPropertyEntity BusinessPropertyEntity { get; set; }
    }   
    
    public interface IWithResidencePropertyEntity : IEntityWithId<PropertyId>
    {
        ResidencePropertyEntity ResidencePropertyEntity { get; set; }
    }

    public interface IWithPropertyEntity : IEntityWithId<PropertyId>
    {
        PropertyEntity PropertyEntity { get; set; }
    }
}
