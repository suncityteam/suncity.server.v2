﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using SunCity.Game.World.DataBase.Entitites.Property.Interfaces;

namespace SunCity.Game.World.DataBase.Entitites.Property.Business.Hospital
{
    public class HospitalPropertyEntity : IWithBusinessPropertyEntity
    {
        [Key]
        [ForeignKey(nameof(BusinessPropertyEntity))]
        public virtual PropertyId EntityId { get; set; }
        public virtual BusinessPropertyEntity BusinessPropertyEntity { get; set; }
    }
}
