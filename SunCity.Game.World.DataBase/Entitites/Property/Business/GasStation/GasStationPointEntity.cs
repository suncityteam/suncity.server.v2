﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Threading.Tasks;
using AutoMapper;
using SunCity.Common.Types.Math;
using SunCity.Domain.Interfaces.Entity;

namespace SunCity.Game.World.DataBase.Entitites.Property.Business.GasStation
{
    public class GasStationPointEntity : IEntityWithId<GasStationPointId>
    {
        [Key]
        [ForeignKey(nameof(BusinessPropertyEntity))]
        public virtual GasStationPointId EntityId { get; set; }

        [ForeignKey(nameof(GasStationPropertyEntity))]
        public virtual PropertyId GasStationPropertyId { get; set; }
        public virtual GasStationPropertyEntity GasStationPropertyEntity { get; set; }

        public virtual Vector3D Position { get; set; }

    }
}