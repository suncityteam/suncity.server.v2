﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using SunCity.Game.World.DataBase.Entitites.Property.Business.Bank.ATM;
using SunCity.Game.World.DataBase.Entitites.Property.Business.Bank.Tariff;
using SunCity.Game.World.DataBase.Entitites.Property.Interfaces;

namespace SunCity.Game.World.DataBase.Entitites.Property.Business.Bank
{
    public class BankPropertyEntity : IWithBusinessPropertyEntity
    {
        [Key]
        [ForeignKey(nameof(BusinessPropertyEntity))]
        public virtual PropertyId EntityId { get; set; }

        public virtual BusinessPropertyEntity BusinessPropertyEntity { get; set; }

        public virtual List<BankCardTariffEntity> CardTariffEntities { get; set; }
        public virtual List<BankATMEntity> AtmEntities { get; set; }
    }
}