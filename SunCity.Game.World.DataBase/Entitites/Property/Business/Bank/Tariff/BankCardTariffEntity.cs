﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using AutoMapper.Configuration.Annotations;
using SunCity.Game.Types.Cost;
using SunCity.Game.Types.Property.Category.Bank;
using SunCity.Game.World.DataBase.Entitites.Property.Business.Bank.Account;

namespace SunCity.Game.World.DataBase.Entitites.Property.Business.Bank.Tariff
{
    public class BankCardTariffEntity
    {
        //=================================================
        [Key]
        public virtual BankCardTariffId EntityId { get; set; }

        [Required]
        [MaxLength(64)]
        public virtual string Name { get; set; }

        public virtual bool Deleted { get; set; }
        //=================================================
        [ForeignKey(nameof(PropertyEntity))]
        public virtual PropertyId PropertyId { get; set; }
        public virtual BankPropertyEntity PropertyEntity { get; set; }

        //=================================================
        [InverseProperty(nameof(BankCardAccountEntity.TariffEntity))]
        public virtual List<BankCardAccountEntity> AccountEntities { get; set; }

        //=================================================
        /// <summary>
        /// Комисия за внесение денег
        /// </summary>
        public BankTariffFee Deposit { get; set; }

        /// <summary>
        /// Комисия за снятие денег
        /// </summary>
        public BankTariffFee WithDraw { get; set; }

        /// <summary>
        /// Комисия за перевод денег
        /// </summary>
        public BankTariffFee Transfer { get; set; }

        //=================================================
        /// <summary>
        /// Ежедневное обслуживание
        /// </summary>
        public GameCost MaintenanceCost { get; set; }

        /// <summary>
        /// Стоимость тарифа
        /// </summary>
        public GameCost TariffCost { get; set; }
    }
}
