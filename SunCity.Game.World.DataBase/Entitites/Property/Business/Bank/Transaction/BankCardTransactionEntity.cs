﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using SunCity.Game.World.DataBase.Entitites.Property.Business.Bank.Account;
using SunCity.Game.World.DataBase.Entitites.Property.Business.Bank.Transaction.Deposit;
using SunCity.Game.World.DataBase.Entitites.Property.Business.Bank.Transaction.Payments.Property;
using SunCity.Game.World.DataBase.Entitites.Property.Business.Bank.Transaction.Payments.Vehicle;
using SunCity.Game.World.DataBase.Entitites.Property.Business.Bank.Transaction.Transfer;
using SunCity.Game.World.Enums.Property.Business.Bank;

namespace SunCity.Game.World.DataBase.Entitites.Property.Business.Bank.Transaction
{
    public class BankCardTransactionEntity
    {
        //=======================================
        [Key]
        public virtual BankCardTransactionId EntityId { get; set; }
        public virtual BankCardTransactionCategory Category { get; set; }

        //=======================================
        [ForeignKey(nameof(BankCardEntity))]
        public virtual BankCardId BankCardId { get; set; }
        public virtual BankCardAccountEntity BankCardEntity { get; set; }
        
        //=======================================
        public virtual DateTime CreatedDate { get; set; }

        //=======================================
        public virtual long Amount { get; set; }
        public virtual long Commission { get; set; }

        //=======================================
        //[Column(TypeName = "cidr")]
        //public virtual NpgsqlInet IpAddress { get; set; }

        //=======================================
        public virtual BankCardDepositTransactionEntity DepositTransaction { get; set; }
        public virtual BankCardWithdrawTransactionEntity WithdrawTransaction { get; set; }

        //=======================================
        public virtual BankCardTransferFromTransactionEntity TransferFromTransaction { get; set; }
        public virtual BankCardTransferToTransactionEntity TransferToTransaction { get; set; }

        //=======================================
        public virtual BankCardPropertyPaymentFromTransactionEntity PropertyPaymentFromTransaction { get; set; }
        public virtual BankCardPropertyPaymentToTransactionEntity PropertyPaymentToTransaction { get; set; }
        
        //=======================================
        public virtual BankCardVehiclePaymentFromTransactionEntity VehiclePaymentFromTransaction { get; set; }
        public virtual BankCardVehiclePaymentToTransactionEntity VehiclePaymentToTransaction { get; set; }


    }
}
