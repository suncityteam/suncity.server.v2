﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using SunCity.Game.World.DataBase.Entitites.Property.Business.Bank.Account;
using SunCity.Game.World.Enums.Property.Business.Bank;

namespace SunCity.Game.World.DataBase.Entitites.Property.Business.Bank.Transaction.Payments.Property
{
    /// <summary>
    /// <para>Информация о платеже у получателя (<see cref="BankCardTransactionCategory.PropertyPurchaseFrom"/>)</para>
    /// <para>Номер счета отправителя (<see cref="PayerId"/>)</para>
    /// </summary>
    public class BankCardPropertyPaymentFromTransactionEntity
    {
        //=======================================
        [Key]
        [ForeignKey(nameof(TransactionEntity))]
        public virtual BankCardTransactionId EntityId { get; set; }
        public virtual BankCardTransactionEntity TransactionEntity { get; set; }

        //=======================================
        [ForeignKey(nameof(PropertyEntity))]
        public virtual PropertyId PropertyId { get; set; }
        public virtual PropertyEntity PropertyEntity { get; set; }

        //=======================================
        [ForeignKey(nameof(PayerEntity))]
        public virtual BankCardId PayerId { get; set; }
        public virtual BankCardAccountEntity PayerEntity { get; set; }
    }
}
