﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using SunCity.Game.World.DataBase.Entitites.Property.Business.Bank.Account;
using SunCity.Game.World.Enums.Property.Business.Bank;

namespace SunCity.Game.World.DataBase.Entitites.Property.Business.Bank.Transaction.Payments.Property
{
    /// <summary>
    /// <para>Информация о платеже у отправителя (<see cref="BankCardTransactionCategory.PropertyPurchaseFor"/>)</para>
    /// <para>Номер счета получателя (<see cref="PayeeId"/>)</para>
    /// </summary>
    public class BankCardPropertyPaymentToTransactionEntity
    { 
        //=======================================
        [Key]
        [ForeignKey(nameof(TransactionEntity))]
        public virtual BankCardTransactionId EntityId { get; set; }
        public virtual BankCardTransactionEntity TransactionEntity { get; set; }

        //=======================================
        [ForeignKey(nameof(PropertyEntity))]
        public virtual PropertyId PropertyId { get; set; }
        public virtual PropertyEntity PropertyEntity { get; set; }

        //=======================================
        [ForeignKey(nameof(PayeeEntity))]
        public virtual BankCardId PayeeId { get; set; }
        public virtual BankCardAccountEntity PayeeEntity { get; set; }
    }
}