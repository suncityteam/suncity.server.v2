﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using SunCity.Game.World.DataBase.Entitites.Property.Business.Bank.Account;
using SunCity.Game.World.DataBase.Entitites.Vehicle;
using SunCity.Game.World.Enums.Property.Business.Bank;

namespace SunCity.Game.World.DataBase.Entitites.Property.Business.Bank.Transaction.Payments.Vehicle
{
    /// <summary>
    /// <para>Информация о платеже у отправителя (<see cref="BankCardTransactionCategory.PropertyPurchaseFor"/>)</para>
    /// <para>Номер счета получателя (<see cref="PayeeId"/>)</para>
    /// </summary>
    public class BankCardVehiclePaymentToTransactionEntity
    { 
        //=======================================
        [Key]
        [ForeignKey(nameof(TransactionEntity))]
        public virtual BankCardTransactionId EntityId { get; set; }
        public virtual BankCardTransactionEntity TransactionEntity { get; set; }

        //=======================================
        [ForeignKey(nameof(PropertyEntity))]
        public virtual PropertyId PropertyId { get; set; }
        public virtual PropertyEntity PropertyEntity { get; set; }

        //=======================================
        [ForeignKey(nameof(VehicleEntity))]
        public virtual VehicleId VehicleId { get; set; }
        public virtual VehicleEntity VehicleEntity { get; set; }
        
        //=======================================
        [ForeignKey(nameof(PayeeEntity))]
        public virtual BankCardId PayeeId { get; set; }
        public virtual BankCardAccountEntity PayeeEntity { get; set; }
    }
}