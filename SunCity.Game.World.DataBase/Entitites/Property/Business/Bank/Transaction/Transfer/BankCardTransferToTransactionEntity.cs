﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Net;
using SunCity.Game.World.DataBase.Entitites.Property.Business.Bank.Account;

namespace SunCity.Game.World.DataBase.Entitites.Property.Business.Bank.Transaction.Transfer
{
    /// <summary>
    /// <para>Информация о платеже у получатели</para>
    /// <para>Номер счета отправителя (<see cref="PayerId"/>)</para>
    /// </summary>
    public class BankCardTransferFromTransactionEntity
    {
        //=======================================
        [Key]
        [ForeignKey(nameof(TransactionEntity))]
        public virtual BankCardTransactionId EntityId { get; set; }
        public virtual BankCardTransactionEntity TransactionEntity { get; set; }

        //=======================================
        [ForeignKey(nameof(PayerEntity))]
        public virtual BankCardId PayerId { get; set; }
        public virtual BankCardAccountEntity PayerEntity { get; set; }

        //=======================================
        [MaxLength(256)]
        public string Comment { get; set; }
    }
}