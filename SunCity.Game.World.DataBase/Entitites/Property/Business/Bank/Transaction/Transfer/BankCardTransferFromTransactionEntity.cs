﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Threading.Tasks;
using AutoMapper;
using SunCity.Game.World.DataBase.Entitites.Property.Business.Bank.Account;

namespace SunCity.Game.World.DataBase.Entitites.Property.Business.Bank.Transaction.Transfer
{
    /// <summary>
    /// <para>Информация о платеже у отправителя</para>
    /// <para>Номер счета получателя (<see cref="PayeeId"/>)</para>
    /// </summary>
    public class BankCardTransferToTransactionEntity
    {
        //=======================================
        [Key]
        [ForeignKey(nameof(TransactionEntity))]
        public virtual BankCardTransactionId EntityId { get; set; }
        public virtual BankCardTransactionEntity TransactionEntity { get; set; }

        //=======================================
        [ForeignKey(nameof(PayeeEntity))]
        public virtual BankCardId PayeeId { get; set; }
        public virtual BankCardAccountEntity PayeeEntity { get; set; }


        //=======================================
        [MaxLength(256)]
        public string Comment { get; set; }
    }
}