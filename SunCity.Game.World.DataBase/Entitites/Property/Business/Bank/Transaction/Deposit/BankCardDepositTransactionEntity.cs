﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Net;
using SunCity.Game.Types.Player.Entity.Player.Character;
using SunCity.Game.World.DataBase.Entitites.Player.Character;
using SunCity.Game.World.DataBase.Entitites.Property.Business.Bank.Account;

namespace SunCity.Game.World.DataBase.Entitites.Property.Business.Bank.Transaction.Deposit
{
    public class BankCardDepositTransactionEntity
    {
        //=======================================
        [Key]
        [ForeignKey(nameof(TransactionEntity))]
        public virtual BankCardTransactionId EntityId { get; set; }
        public virtual BankCardTransactionEntity TransactionEntity { get; set; }

        //=======================================
        [ForeignKey(nameof(SenderEntity))]
        public virtual PlayerCharacterId SenderId { get; set; }
        public virtual PlayerCharacterEntity SenderEntity { get; set; }
    }
}