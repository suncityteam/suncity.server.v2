﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Net;
using SunCity.Game.Types.Player.Entity.Player.Character;
using SunCity.Game.World.DataBase.Entitites.Player.Character;
using SunCity.Game.World.DataBase.Entitites.Property.Business.Bank.Account;

namespace SunCity.Game.World.DataBase.Entitites.Property.Business.Bank.Transaction.Deposit
{
    public class BankCardWithdrawTransactionEntity
    {
        //=======================================
        [Key]
        [ForeignKey(nameof(TransactionEntity))]
        public virtual BankCardTransactionId EntityId { get; set; }
        public virtual BankCardTransactionEntity TransactionEntity { get; set; }

        //=======================================
        [ForeignKey(nameof(RecipientEntity))]
        public virtual PlayerCharacterId RecipientId { get; set; }
        public virtual PlayerCharacterEntity RecipientEntity { get; set; }
    }
}