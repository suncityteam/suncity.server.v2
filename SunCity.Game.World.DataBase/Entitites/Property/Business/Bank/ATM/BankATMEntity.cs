﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using SunCity.Common.Types.Math;
using SunCity.Domain.Interfaces.Entity;

namespace SunCity.Game.World.DataBase.Entitites.Property.Business.Bank.ATM
{
    public class BankATMEntity
        : IEntityWithId<BankATMId>
        , IEntityWithCreatedDate
        , IEntityWithDeleted
    {
        //=====================================
        [Key] 
        public virtual BankATMId EntityId { get; set; }
        public virtual DateTime CreatedDate { get; set; }
        
        //=====================================
        public virtual bool Deleted { get; set; }
        public virtual DateTime? DeletedDate { get; set; }
        
        //=====================================
        [ForeignKey(nameof(BankPropertyEntity))]
        public virtual PropertyId BankPropertyId { get; set; }
        public virtual BankPropertyEntity BankPropertyEntity { get; set; }
        
        //=====================================
        public virtual Vector3D Position { get; set; }
        public virtual Vector3D Rotation { get; set; }
        
        //=====================================
        public virtual long Balance { get; set; }
        
        [MaxLength(256)]
        public virtual string AdminComment { get; set; }
        
    }
}