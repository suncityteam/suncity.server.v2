﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using SunCity.Domain.Interfaces.Entity;
using SunCity.Game.World.DataBase.Entitites.Player.Character;

namespace SunCity.Game.World.DataBase.Entitites.Property.Business.Bank.Account
{
    public class BankCardBusinessAccountEntity : IEntityWithId<BankCardId>
    {
        //=======================================
        [Key]
        [ForeignKey(nameof(AccountEntity))]
        public virtual BankCardId EntityId { get; set; }
        public virtual BankCardAccountEntity AccountEntity { get; set; }
        
        //=======================================
        [ForeignKey(nameof(CharacterEntity))]
        public virtual PlayerCharacterId CharacterId { get; set; }
        public virtual PlayerCharacterEntity CharacterEntity { get; set; }
        
        //=======================================
        [Required]
        [MaxLength(128)]
        public string BusinessName { get; set; }
        
        //=======================================
        // public virtual List<BusinessPropertyEntity> AttachedBusiness { get; set; }
    }
}