﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using SunCity.Domain.Interfaces.Entity;
using SunCity.Game.World.DataBase.Entitites.Fraction;

namespace SunCity.Game.World.DataBase.Entitites.Property.Business.Bank.Account
{
    public class BankCardFractionAccountEntity : IEntityWithId<BankCardId>
    {
        //=======================================
        [Key]
        [ForeignKey(nameof(AccountEntity))]
        public virtual BankCardId EntityId { get; set; }
        public virtual BankCardAccountEntity AccountEntity { get; set; }

        //=======================================
        [ForeignKey(nameof(FractionEntity))]
        public virtual FractionId FractionId { get; set; }
        public virtual FractionEntity FractionEntity { get; set; }

    }
}