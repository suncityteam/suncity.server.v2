﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using SunCity.Domain.Interfaces.Entity;
using SunCity.Game.World.DataBase.Entitites.Property.Business.Bank.Tariff;

namespace SunCity.Game.World.DataBase.Entitites.Property.Business.Bank.Account
{
    public enum BankCardAccountCategory
    {
        None,
        Player,
        Fraction,
        Business
    }

    public class BankCardAccountEntity : IEntityWithId<BankCardId>
    {
        //=======================================
        [Key]
        public virtual BankCardId EntityId { get; set; }
        public BankCardAccountCategory Category { get; set; }

        //=======================================
        /// <summary>
        /// Название банковской карты для игрока
        /// </summary>
        [MaxLength(32)]
        public string Name { get; set; }

        //=======================================
        /// <summary>
        /// Номер банковской карты
        /// </summary>
        public long Number { get; set; }

        //=======================================
        public long PinCodeHash { get; set; }
        public long SecureWordHash { get; set; }

        //=======================================
        [ForeignKey(nameof(TariffEntity))]
        public virtual BankCardTariffId TariffId { get; set; }
        public virtual BankCardTariffEntity TariffEntity { get; set; }

        //=======================================
        public virtual DateTime CreatedDate { get; set; }
        public virtual DateTime ExpiredDate { get; set; }
        public bool IsExpired => DateTime.UtcNow >= ExpiredDate;

        //=======================================
        public virtual long Balance { get; set; }

        //=======================================
        public virtual BankCardPlayerAccountEntity PlayerAccountEntity { get; set; }
        public virtual BankCardFractionAccountEntity FractionAccountEntity { get; set; }
        public virtual BankCardBusinessAccountEntity BusinessAccountEntity { get; set; }

    }
}
