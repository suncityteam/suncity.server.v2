﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Threading.Tasks;
using GTANetworkAPI;
using SunCity.Domain.Interfaces.Entity;
using SunCity.Game.Types.Cost;
using SunCity.Game.World.DataBase.Entitites.Vehicle;

namespace SunCity.Game.World.DataBase.Entitites.Property.Business.CarShowroom
{
    public class CarShowroomVehicleEntity : IEntityWithId<CarShowroomVehicleId>
    {
        //==============================
        [Key]
        public virtual CarShowroomVehicleId EntityId { get; set; }

        //==============================
        [ForeignKey(nameof(CarShowroomEntity))]
        public virtual PropertyId CarShowroomId { get; set; }
        public virtual CarShowroomPropertyEntity CarShowroomEntity { get; set; }

        //==============================
        [ForeignKey(nameof(ModelEntity))]
        public virtual VehicleHash ModelId { get; set; }
        public virtual VehicleModelEntity ModelEntity { get; set; }

        //==============================
        public virtual GameCost Cost { get; set; }
        public virtual int Amount { get; set; }
    }
}