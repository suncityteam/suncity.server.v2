﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Threading.Tasks;
using AutoMapper;
using SunCity.Common.Types.Math;
using SunCity.Domain.Interfaces.Entity;

namespace SunCity.Game.World.DataBase.Entitites.Property.Business.CarShowroom
{
    public class CarShowroomVehicleSpawnPointEntity : IEntityWithId<CarShowroomVehicleSpawnId>
    {
        [Key]
        public virtual CarShowroomVehicleSpawnId EntityId { get; set; }

        [ForeignKey(nameof(CarShowroomEntity))]
        public virtual PropertyId CarShowroomId { get; set; }
        public virtual CarShowroomPropertyEntity CarShowroomEntity { get; set; }

        public virtual Vector3D Position { get; set; }
        public virtual Vector3D Rotation { get; set; }
    }
}