﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using SunCity.Common.Types.Math;
using SunCity.Domain.Interfaces.Entity;
using SunCity.Game.Interfaces.Entity;

namespace SunCity.Game.World.DataBase.Entitites.Property.Business.DrivingSchool.Routes
{
    public class DrivingSchoolRoutePointEntity
        : IEntityWithId<DrivingSchoolRoutePointId>
        , IEntityWithPosition<Vector3D>
    {
        //==================================
        [Key] 
        public virtual DrivingSchoolRoutePointId EntityId { get; set; }
        
        //==================================
        [ForeignKey(nameof(RouteEntity))]
        public virtual DrivingSchoolRouteId RouteId { get; set; }
        public virtual DrivingSchoolRouteEntity RouteEntity { get; set; }
        
        //==================================
        public virtual Vector3D Position { get; set; }

        //==================================
        //[ForeignKey(nameof(NextPointEntity))]
        //public virtual DrivingSchoolRoutePointId? NextPointId { get; set; }
        //public virtual DrivingSchoolRoutePointEntity NextPointEntity { get; set; }
    }
}