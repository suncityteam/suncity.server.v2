﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using SunCity.Domain.Interfaces.Entity;

namespace SunCity.Game.World.DataBase.Entitites.Property.Business.DrivingSchool.Routes
{
    public class DrivingSchoolRouteEntity
        : IEntityWithId<DrivingSchoolRouteId>
        , IEntityWithCreatedDate
        , IEntityWithDeleted
    {
        //==================================
        [Key]
        public virtual DrivingSchoolRouteId EntityId { get; set; }
        
        //==================================
        [ForeignKey(nameof(DrivingSchoolEntity))]
        public virtual PropertyId DrivingSchoolId { get; set; }
        public virtual DrivingSchoolPropertyEntity DrivingSchoolEntity { get; set; }
        
        //==================================
        [Required]
        [MaxLength(64)]
        public virtual string Name { get; set; }
        public DateTime CreatedDate { get;  set; }

        //==================================
        public virtual bool Deleted { get; set; }
        public virtual DateTime? DeletedDate { get; set; }
        
        //==================================
        /*[ForeignKey(nameof(StartPointEntity))]
        public virtual DrivingSchoolRoutePointId StartPointId { get; set; }
        public virtual DrivingSchoolRoutePoint StartPointEntity { get; set; }*/
        
        //==================================
        public virtual List<DrivingSchoolRoutePointEntity> Points { get; set; }
    }
}