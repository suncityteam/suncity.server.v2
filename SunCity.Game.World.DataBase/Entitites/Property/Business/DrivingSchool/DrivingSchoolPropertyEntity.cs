﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using SunCity.Game.World.DataBase.Entitites.Property.Business.DrivingSchool.Routes;
using SunCity.Game.World.DataBase.Entitites.Property.Interfaces;

namespace SunCity.Game.World.DataBase.Entitites.Property.Business.DrivingSchool
{
    public class DrivingSchoolPropertyEntity : IWithBusinessPropertyEntity
    {
        [Key]
        [ForeignKey(nameof(BusinessPropertyEntity))]
        public virtual PropertyId EntityId { get; set; }
        public virtual BusinessPropertyEntity BusinessPropertyEntity { get; set; }

        
        public virtual List<DrivingSchoolRouteEntity> Routes { get; set; }
        public virtual List<DrivingSchoolLicenseEntity> Licenses { get; set; }
    }
}