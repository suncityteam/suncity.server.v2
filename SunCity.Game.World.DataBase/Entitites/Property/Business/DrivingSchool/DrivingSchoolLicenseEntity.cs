﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using SunCity.Domain.Interfaces.Entity;
using SunCity.Game.Types.Cost;
using SunCity.Game.World.DataBase.Entitites.Licenses;

namespace SunCity.Game.World.DataBase.Entitites.Property.Business.DrivingSchool
{
    public class DrivingSchoolLicenseEntity
        : IEntityWithId<DrivingSchoolLicenseId>
        , IEntityWithCreatedDate
        , IEntityWithDeleted
    {
        //===================================================
        [Key] 
        public virtual DrivingSchoolLicenseId EntityId { get; set; }
        public virtual DateTime CreatedDate { get; set; }

        //===================================================
        public virtual bool Deleted { get; set; }
        public virtual DateTime? DeletedDate { get; set; }
        
        //===================================================
        [ForeignKey(nameof(LicenseEntity))]
        public virtual LicenseId LicenseId { get; set; }
        public virtual GameLicenseEntity LicenseEntity { get; set; }

        //===================================================
        [ForeignKey(nameof(DrivingSchoolEntity))]
        public virtual PropertyId DrivingSchoolId { get; set; }
        public virtual DrivingSchoolPropertyEntity DrivingSchoolEntity { get; set; }

        //===================================================
        public virtual GameCost Cost { get; set; }
    }
}