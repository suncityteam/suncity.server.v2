﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using SunCity.Game.World.DataBase.Entitites.Property.Business.StreetRacing.Routes;
using SunCity.Game.World.DataBase.Entitites.Property.Interfaces;

namespace SunCity.Game.World.DataBase.Entitites.Property.Business.StreetRacing
{
    public class StreetRacingPropertyEntity: IWithBusinessPropertyEntity
    {
        //==================================
        [Key]
        [ForeignKey(nameof(BusinessPropertyEntity))]
        public virtual PropertyId EntityId { get; set; }
        public virtual BusinessPropertyEntity BusinessPropertyEntity { get; set; }
        
        //==================================
        public virtual List<StreetRacingRouteEntity> Routes { get; set; }

    }
}