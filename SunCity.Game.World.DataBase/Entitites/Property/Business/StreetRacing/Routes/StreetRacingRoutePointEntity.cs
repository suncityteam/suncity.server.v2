﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using SunCity.Common.Types.Math;
using SunCity.Domain.Interfaces.Entity;

namespace SunCity.Game.World.DataBase.Entitites.Property.Business.StreetRacing.Routes
{
    public class StreetRacingRoutePointEntity
        : IEntityWithId<StreetRacingRoutePointId>
    {
        //==================================
        [Key] 
        public virtual StreetRacingRoutePointId EntityId { get; set; }
        
        //==================================
        [ForeignKey(nameof(RouteEntity))]
        public virtual StreetRacingRouteId RouteId { get; set; }
        public virtual StreetRacingRouteEntity RouteEntity { get; set; }
        
        //==================================
        public virtual Vector3D Position { get; }
        
        [MaxLength(256)]
        public virtual string AdminDescription { get; set; }
        //==================================
        //[ForeignKey(nameof(NextPointEntity))]
        //public virtual DrivingSchoolRoutePointId? NextPointId { get; set; }
        //public virtual DrivingSchoolRoutePointEntity NextPointEntity { get; set; }
    }
}