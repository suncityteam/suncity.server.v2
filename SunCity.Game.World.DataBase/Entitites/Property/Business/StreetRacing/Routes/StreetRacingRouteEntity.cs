﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using SunCity.Domain.Interfaces.Entity;
using SunCity.Game.Types.Cost;
using SunCity.Game.World.DataBase.Entitites.Property.Business.StreetRacing.Heats;

namespace SunCity.Game.World.DataBase.Entitites.Property.Business.StreetRacing.Routes
{
    public class StreetRacingRouteEntity
        : IEntityWithId<StreetRacingRouteId>
        , IEntityWithCreatedDate
        , IEntityWithDeleted
    {
        //==================================
        [Key]
        public virtual StreetRacingRouteId EntityId { get; set; }
        
        //==================================
        [ForeignKey(nameof(StreetRacingEntity))]
        public virtual PropertyId StreetRacingId { get; set; }
        public virtual StreetRacingPropertyEntity StreetRacingEntity { get; set; }
        
        //==================================
        [Required]
        [MaxLength(64)]
        public virtual string Name { get; set; }
        public virtual DateTime CreatedDate { get;  set; }

        //==================================
        public virtual bool Deleted { get; set; }
        public virtual DateTime? DeletedDate { get; set; }
        
        //==================================
        public virtual GameCost Cost { get; set; }
        public virtual StreetRacingReward Reward { get; set; }

        //==================================
        public virtual StreetRacingRoutePointId? StartPointId { get; set; }
        public virtual StreetRacingRoutePointEntity StartPointEntity { get; set; }
        
        //==================================
        public virtual List<StreetRacingRoutePointEntity> Points { get; set; }
        public virtual List<StreetRacingHeatEntity> Heats { get; set; }
    }
}