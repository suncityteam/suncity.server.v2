﻿using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;
using SunCity.Game.Types.Cost;

namespace SunCity.Game.World.DataBase.Entitites.Property.Business.StreetRacing.Routes
{
    [Owned]
    
    public class StreetRacingReward
    {
        public virtual GameCost First { get; set; }
        public virtual GameCost Second { get; set; }
        public virtual GameCost Third { get; set; }

        public GameCost this[int index]
        {
            get
            {
                switch (index)
                {
                    case 1:
                        return First;
                    case 2:
                        return Second;
                    case 3:
                        return Third;
                    default:
                        return null;
                }
            }
        }
    }
}