﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using SunCity.Domain.Interfaces.Entity;
using SunCity.Game.World.DataBase.Entitites.Property.Business.StreetRacing.Routes;

namespace SunCity.Game.World.DataBase.Entitites.Property.Business.StreetRacing.Heats
{
    public class StreetRacingHeatEntity
        : IEntityWithId<StreetRacingHeatId>
        , IEntityWithCreatedDate
        , IEntityWithExpiredDate
    {
        //====================================
        [Key] 
        public virtual StreetRacingHeatId EntityId { get; set; }
        public virtual StreetRacingHeatStatus Status { get; set; }
        
        //====================================
        public virtual DateTime CreatedDate { get; set; }
        public virtual DateTime ExpiredDate { get; set; }

        //====================================
        public virtual StreetRacingHeatMemberId? WinnerId { get; set; }
        public virtual StreetRacingHeatMemberEntity WinnerEntity { get; set; }
        
        //====================================
        [ForeignKey(nameof(RouteEntity))]
        public virtual StreetRacingRouteId RouteId { get; set; }
        public virtual StreetRacingRouteEntity RouteEntity { get; set; }

        //====================================
        public virtual List<StreetRacingHeatMemberEntity> Members { get; set; }
    }
}