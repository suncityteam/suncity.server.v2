﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using SunCity.Domain.Interfaces.Entity;
using SunCity.Game.World.DataBase.Entitites.Player.Character;
using SunCity.Game.World.DataBase.Entitites.Vehicle;

namespace SunCity.Game.World.DataBase.Entitites.Property.Business.StreetRacing.Heats
{
    public class StreetRacingHeatMemberEntity
        : IEntityWithId<StreetRacingHeatMemberId>
        , IEntityWithCreatedDate
        , IEntityWithExpiredDate
    {
        //====================================
        [Key]
        public virtual StreetRacingHeatMemberId EntityId { get; set; }
        
        //====================================
        public virtual DateTime CreatedDate { get; set; }
        public virtual DateTime ExpiredDate { get; set; }

        //====================================
        [ForeignKey(nameof(HeatEntity))]
        public virtual StreetRacingHeatId HeatId { get; set; }
        public virtual StreetRacingHeatEntity HeatEntity { get; set; }

        //====================================
        [ForeignKey(nameof(CharacterEntity))]
        public virtual PlayerCharacterId CharacterId { get; set; }
        public virtual PlayerCharacterEntity CharacterEntity { get; set; }
        
        //====================================
        [ForeignKey(nameof(VehicleEntity))]
        public virtual VehicleId VehicleId { get; set; }
        public virtual VehicleEntity VehicleEntity { get; set; }
        
    }
}