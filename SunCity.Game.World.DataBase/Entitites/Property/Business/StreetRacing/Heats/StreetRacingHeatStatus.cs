﻿namespace SunCity.Game.World.DataBase.Entitites.Property.Business.StreetRacing.Heats
{
    public enum StreetRacingHeatStatus : byte
    {
        Created,
        Started,
        Completed,
        Canceled,
    }
}