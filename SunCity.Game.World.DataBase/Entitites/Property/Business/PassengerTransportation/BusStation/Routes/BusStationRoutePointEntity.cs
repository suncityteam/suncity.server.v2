﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using SunCity.Domain.Interfaces.Entity;
using SunCity.Game.World.DataBase.Entitites.StationPoint;

namespace SunCity.Game.World.DataBase.Entitites.Property.Business.PassengerTransportation.BusStation.Routes
{
    public class BusStationRoutePointEntity
        : IEntityWithId<BusStationRoutePointId>
    {
        //==================================
        [Key] 
        public virtual BusStationRoutePointId EntityId { get; set; }
        
        //==================================
        [ForeignKey(nameof(RouteEntity))]
        public virtual BusStationRouteId RouteId { get; set; }
        public virtual BusStationRouteEntity RouteEntity { get; set; }
        
        //==================================
        public virtual StationPointEntity StationPointEntity { get; set; }
        public virtual StationPointId StationPointId { get; set; }
        
        //==================================
        /// <summary>
        /// <para>Passengers waiting time</para>
        /// </summary>
        public virtual TimeSpan WaitingTime { get; set; }
        
        /// <summary>
        /// <para>Max delay betwen points</para>
        /// </summary>
        public virtual TimeSpan MaxDelay { get; set; }
        
        //==================================
        //[ForeignKey(nameof(NextPointEntity))]
        //public virtual DrivingSchoolRoutePointId? NextPointId { get; set; }
        //public virtual DrivingSchoolRoutePointEntity NextPointEntity { get; set; }
    }
}