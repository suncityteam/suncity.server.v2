﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using SunCity.Domain.Interfaces.Entity;
using SunCity.Game.Types.Cost;
using SunCity.Game.World.DataBase.Entitites.Property.Business.PassengerTransportation.BusStation.Flights;

namespace SunCity.Game.World.DataBase.Entitites.Property.Business.PassengerTransportation.BusStation.Routes
{
    public class BusStationRouteEntity
        : IEntityWithId<BusStationRouteId>
        , IEntityWithCreatedDate
        , IEntityWithDeleted
    {
        //==================================
        [Key]
        public virtual BusStationRouteId EntityId { get; set; }
        
        //==================================
        [ForeignKey(nameof(BusStationEntity))]
        public virtual PropertyId BusStationId { get; set; }
        public virtual BusStationPropertyEntity BusStationEntity { get; set; }
        
        //==================================
        [Required]
        [MaxLength(64)]
        public virtual string Name { get; set; }
        public virtual DateTime CreatedDate { get;  set; }

        //==================================
        public virtual bool Deleted { get; set; }
        public virtual DateTime? DeletedDate { get; set; }
        
        //==================================
        /// <summary>
        /// <para>Passenger fare</para>
        /// </summary>
        public virtual GameCost Cost { get; set; }
        
        /// <summary>
        /// <para>Route driver reward</para>
        /// </summary>
        public virtual GameCost Reward { get; set; }
        
        /// <summary>
        /// <para>Route driver reward for each passenger</para>
        /// </summary>
        public virtual GameCost Bonus { get; set; }

        //==================================
        public virtual BusStationRoutePointId? StartPointId { get; set; }
        public virtual BusStationRoutePointEntity StartPointEntity { get; set; }
        
        //==================================
        public virtual List<BusStationRoutePointEntity> Points { get; set; }
        public virtual List<BusStationFlightEntity> Flights { get; set; }
    }
}