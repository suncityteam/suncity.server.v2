﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using SunCity.Game.World.DataBase.Entitites.Property.Business.PassengerTransportation.BusStation.Routes;
using SunCity.Game.World.DataBase.Entitites.Property.Interfaces;

namespace SunCity.Game.World.DataBase.Entitites.Property.Business.PassengerTransportation.BusStation
{
    public class BusStationPropertyEntity: IWithBusinessPropertyEntity
    {
        //==================================
        [Key]
        [ForeignKey(nameof(BusinessPropertyEntity))]
        public virtual PropertyId EntityId { get; set; }
        public virtual BusinessPropertyEntity BusinessPropertyEntity { get; set; }
        
        //==================================
        public virtual List<BusStationRouteEntity> Routes { get; set; }

    }
}