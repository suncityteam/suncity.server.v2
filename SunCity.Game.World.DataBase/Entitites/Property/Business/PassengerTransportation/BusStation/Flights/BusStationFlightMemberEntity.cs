﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using SunCity.Domain.Interfaces.Entity;
using SunCity.Game.World.DataBase.Entitites.Player.Character;

namespace SunCity.Game.World.DataBase.Entitites.Property.Business.PassengerTransportation.BusStation.Flights
{
    public class BusStationFlightMemberEntity
        : IEntityWithId<BusStationFlightMemberId>
        , IEntityWithCreatedDate
        , IEntityWithExpiredDate
    {
        //====================================
        [Key]
        public virtual BusStationFlightMemberId EntityId { get; set; }
        
        //====================================
        public virtual DateTime CreatedDate { get; set; }
        public virtual DateTime ExpiredDate { get; set; }

        //====================================
        [ForeignKey(nameof(FlightEntity))]
        public virtual BusStationFlightId FlightId { get; set; }
        public virtual BusStationFlightEntity FlightEntity { get; set; }

        //====================================
        [ForeignKey(nameof(CharacterEntity))]
        public virtual PlayerCharacterId CharacterId { get; set; }
        public virtual PlayerCharacterEntity CharacterEntity { get; set; }
    }
}