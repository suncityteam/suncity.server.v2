﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using SunCity.Domain.Interfaces.Entity;
using SunCity.Game.World.DataBase.Entitites.Property.Business.PassengerTransportation.BusStation.Routes;

namespace SunCity.Game.World.DataBase.Entitites.Property.Business.PassengerTransportation.BusStation.Flights
{
    public class BusStationFlightEntity
        : IEntityWithId<BusStationFlightId>
        , IEntityWithCreatedDate
        , IEntityWithExpiredDate
    {
        //====================================
        [Key] 
        public virtual BusStationFlightId EntityId { get; set; }
        public virtual BusStationFlightStatus Status { get; set; }
        
        //====================================
        public virtual DateTime CreatedDate { get; set; }
        public virtual DateTime ExpiredDate { get; set; }

        //====================================
        [ForeignKey(nameof(RouteEntity))]
        public virtual BusStationRouteId RouteId { get; set; }
        public virtual BusStationRouteEntity RouteEntity { get; set; }

        //====================================
        public virtual List<BusStationFlightMemberEntity> Members { get; set; }
    }
}