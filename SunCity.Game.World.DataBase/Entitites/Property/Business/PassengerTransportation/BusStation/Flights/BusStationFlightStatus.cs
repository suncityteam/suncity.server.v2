﻿namespace SunCity.Game.World.DataBase.Entitites.Property.Business.PassengerTransportation.BusStation.Flights
{
    public enum BusStationFlightStatus : byte
    {
        Started,
        Completed,
        Canceled,
    }
}