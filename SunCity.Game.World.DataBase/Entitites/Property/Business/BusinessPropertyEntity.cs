﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using SunCity.Common.Types.Time;
using SunCity.Game.Types.Property.Entity.Property;
using SunCity.Game.World.DataBase.Entitites.Property.Business.Bank;
using SunCity.Game.World.DataBase.Entitites.Property.Business.CarShowroom;
using SunCity.Game.World.DataBase.Entitites.Property.Business.DrivingSchool;
using SunCity.Game.World.DataBase.Entitites.Property.Business.GasStation;
using SunCity.Game.World.DataBase.Entitites.Property.Business.Hospital;
using SunCity.Game.World.DataBase.Entitites.Property.Business.PassengerTransportation.BusStation;
using SunCity.Game.World.DataBase.Entitites.Property.Business.StreetRacing;
using SunCity.Game.World.DataBase.Entitites.Property.Interfaces;

namespace SunCity.Game.World.DataBase.Entitites.Property.Business
{
    public class BusinessPropertyEntity : IWithPropertyEntity
    {
        [Key]
        [ForeignKey(nameof(PropertyEntity))]
        public virtual PropertyId EntityId { get; set; }

        public virtual PropertyEntity PropertyEntity { get; set; }

        //=======================================================
        public virtual int Level { get; set; }
        public virtual long Experience { get; set; }

        //=======================================================
        public virtual TimeRange WorkingTime { get; set; }

        //=======================================================
        public virtual BankPropertyEntity BankPropertyEntity { get; set; }
        public virtual GasStationPropertyEntity GasStationPropertyEntity { get; set; }
        public virtual HospitalPropertyEntity HospitalPropertyEntity { get; set; }
        public virtual CarShowroomPropertyEntity CarShowroomPropertyEntity { get; set; }
        public virtual DrivingSchoolPropertyEntity DrivingSchoolPropertyEntity { get; set; }
        public virtual BusStationPropertyEntity BusStationPropertyEntity { get; set; }
        public virtual StreetRacingPropertyEntity StreetRacingPropertyEntity { get; set; }
    }
}