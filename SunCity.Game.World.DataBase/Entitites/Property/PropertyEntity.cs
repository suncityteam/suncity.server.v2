﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using SunCity.Common.Types.Math;
using SunCity.Domain.Interfaces.Entity;
using SunCity.Game.Enums.World.Property;
using SunCity.Game.Interfaces.Entity;
using SunCity.Game.Types.Cost;
using SunCity.Game.Types.Property.Entity.Property;
using SunCity.Game.World.DataBase.Entitites.Interior;
using SunCity.Game.World.DataBase.Entitites.Property.Business;
using SunCity.Game.World.DataBase.Entitites.Property.HeadQuarter;
using SunCity.Game.World.DataBase.Entitites.Property.Interfaces;
using SunCity.Game.World.DataBase.Entitites.Property.Owners;
using SunCity.Game.World.DataBase.Entitites.Property.Residence;

namespace SunCity.Game.World.DataBase.Entitites.Property
{
    public class PropertyEntity 
        : IEntityWithId<PropertyId>
        , IEntityWithName<PropertyName>
        , IEntityWithPosition<Vector3D>
        , IEntityWithRotation
        , IEntityWithStateCost
        , IEntityWithSellingCost
        , IWithResidencePropertyEntity
        , IWithBusinessPropertyEntity
    {
        [Key]
        public virtual PropertyId EntityId { get; set; }
        public virtual PropertyName Name { get; set; }


        [ForeignKey(nameof(InteriorEntity))]
        public virtual InteriorId? InteriorId { get; set; }
        public virtual InteriorEntity InteriorEntity { get; set; }


        public virtual Vector3D IconPosition { get; set; }
        public virtual Vector3D Position { get; set; }
        public virtual float Rotation { get; set; }

        public virtual GameCost StateCost { get; set; }
        public virtual GameCost SellingCost { get; set; }
        public virtual bool IsSelling { get; set; }

        //==================================
        public BankCardId? OwnerBankCardId { get; set; }

        //==================================
        [MaxLength(64)]
        public virtual string AdminDescription { get; set; }


        //==================================
        public virtual WorldPropertyClass PropertyClass { get; set; }
        public virtual WorldPropertyCategory PropertyCategory { get; set; }


        //==================================
        public virtual List<PropertyRequiredLicenseEntity> RequiredLicenses { get; set; }
        
        //==================================
        public virtual List<PropertyOwnerEntity> OwnerEntities { get; set; }

        //==================================
        public virtual HeadQuarterPropertyEntity HeadQuarterPropertyEntity { get; set; }
        public virtual ResidencePropertyEntity ResidencePropertyEntity { get; set; }
        public virtual BusinessPropertyEntity BusinessPropertyEntity { get; set; }
    }
}
