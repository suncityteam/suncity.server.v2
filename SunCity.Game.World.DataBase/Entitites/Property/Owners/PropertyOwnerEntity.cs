﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Threading.Tasks;
using SunCity.Domain.Interfaces.Entity;
using SunCity.Game.Enums.World.Property.Owner;

namespace SunCity.Game.World.DataBase.Entitites.Property.Owners
{
    public class PropertyOwnerEntity : IEntityWithId<PropertyOwnerId>
    {
        //==============================
        [Key]
        public virtual PropertyOwnerId EntityId { get; set; }

        //==============================
        public virtual PropertyOwnerType Type { get; set; }
        public virtual PropertyOwnerGroup Group { get; set; }

        //==============================
        [ForeignKey(nameof(PropertyEntity))]
        public virtual PropertyId PropertyId { get; set; }
        public virtual PropertyEntity PropertyEntity { get; set; }

        //==============================
        public virtual DateTime Begin { get; set; }
        public virtual DateTime? End { get; set; }

        //==============================
        public virtual PropertyFractionOwnerEntity FractionOwnerEntity { get; set; }
        public virtual PropertyPlayerOwnerEntity PlayerOwnerEntity { get; set; }

        //==============================
        public bool IsActive => End != null;
    }
}