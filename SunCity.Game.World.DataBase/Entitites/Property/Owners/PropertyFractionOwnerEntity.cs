﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using SunCity.Domain.Interfaces.Entity;
using SunCity.Game.World.DataBase.Entitites.Fraction;

namespace SunCity.Game.World.DataBase.Entitites.Property.Owners
{
    public class PropertyFractionOwnerEntity : IEntityWithId<PropertyOwnerId>
    {
        [Key]
        [ForeignKey(nameof(OwnerEntity))]
        public virtual PropertyOwnerId EntityId { get; set; }
        public virtual PropertyOwnerEntity OwnerEntity { get; set; }

        [ForeignKey(nameof(FractionEntity))]
        public virtual FractionId FractionId { get; set; }
        public virtual FractionEntity FractionEntity { get; set; }
    }
}