﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Threading.Tasks;
using AutoMapper;
using SunCity.Domain.Interfaces.Entity;
using SunCity.Game.Types.Player.Entity.Player.Character;
using SunCity.Game.World.DataBase.Entitites.Player.Character;

namespace SunCity.Game.World.DataBase.Entitites.Property.Owners
{
    public class PropertyPlayerOwnerEntity : IEntityWithId<PropertyOwnerId>
    {
        [Key]
        [ForeignKey(nameof(OwnerEntity))]
        public virtual PropertyOwnerId EntityId { get; set; }
        public virtual PropertyOwnerEntity OwnerEntity { get; set; }

        [ForeignKey(nameof(CharacterEntity))]
        public virtual PlayerCharacterId CharacterId { get; set; }
        public virtual PlayerCharacterEntity CharacterEntity { get; set; }
    }
}