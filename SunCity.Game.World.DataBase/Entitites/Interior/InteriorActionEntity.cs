﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Threading.Tasks;
using SunCity.Common.Types.Math;
using SunCity.Domain.Interfaces.Entity;
using SunCity.Game.Enums.World.Interior;
using SunCity.Game.Interfaces.Entity;

namespace SunCity.Game.World.DataBase.Entitites.Interior
{
    public class InteriorActionEntity
        : IEntityWithId<InteriorActionId>
        , IEntityWithPosition<Vector3D>
        , IEntityWithRotation
    {
        [Key]
        public virtual InteriorActionId EntityId { get; set; }


        [ForeignKey(nameof(InteriorEntity))]
        public virtual InteriorId InteriorId { get; set; }
        public virtual InteriorEntity InteriorEntity { get; set; }

        public virtual Vector3D Position { get; set; }
        public virtual float Rotation { get; set; }
        public virtual InteriorActionType Action { get; set; }

        [MaxLength(64)]
        public virtual string AdminDescription { get; set; }
    }
}