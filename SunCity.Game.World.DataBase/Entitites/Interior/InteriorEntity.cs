﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;
using SunCity.Common.Types.Math;
using SunCity.Domain.Interfaces.Entity;
using SunCity.Game.Interfaces.Entity;
using SunCity.Game.Types.Property.Entity.Interior;
using SunCity.Game.World.DataBase.Entitites.Property;

namespace SunCity.Game.World.DataBase.Entitites.Interior
{
    public enum InteriorType
    {
        /// <summary>
        /// <para>Интерьер общий для нескольких недвижимости</para>
        /// <para>Требуется виртуальный мир</para>
        /// </summary>
        Interior,
        
        /// <summary>
        /// <para>Уникальный экстерьер для недвижимости</para>
        /// <para>Не требуется виртуальный мир</para>
        /// </summary>
        Exterior,
    }
    
    public class InteriorEntity 
        : IEntityWithId<InteriorId>
        , IEntityWithPosition<Vector3D>
        , IEntityWithRotation
    {
        [Key]
        public virtual InteriorId EntityId { get; set; }
        public virtual InteriorItemPlacement IPL { get; set; }

        public virtual InteriorType Type { get; set; }
        public virtual Vector3D Position { get; set; }
        public virtual float Rotation { get; set; }

        public virtual Vector3D SpawnPosition { get; set; }
        public virtual float SpawnRotation { get; set; }

        public virtual List<PropertyEntity> PropertyEntities { get; set; }
        public virtual List<InteriorActionEntity> ActionEntities { get; set; }

        [MaxLength(256)]
        public virtual string AdminDescription { get; set; }

        public override string ToString()
        {
            return $"Interior: {EntityId}, IPL: {IPL}, Position: {Position} / {Rotation}, SpawnPosition: {SpawnPosition} / {SpawnRotation}";
        }
    }
}
