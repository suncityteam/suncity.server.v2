﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using SunCity.EntityFrameWork.Converters;

namespace SunCity.Game.World.DataBase
{
    public static class ServiceRecorder
    {
        public static IServiceCollection AddGameWorldDataBase(this IServiceCollection InServiceCollection, IConfiguration InConfiguration)
        {
            var connectionString = InConfiguration.GetSection("Application.DataBase").GetSection(nameof(GameWorldDbContext)).Get<string>();
            Console.WriteLine($"AddGameWorldDataBase: {connectionString}");


            InServiceCollection.AddDbContext<GameWorldDbContext>(InBuilder =>
            {
                //InBuilder.UseInMemoryDatabase(GameWorldDbContext.SchemaName);
                InBuilder.UseNpgsql(connectionString, InOptions =>
                {
                    InOptions.EnableRetryOnFailure();
                    InOptions.MigrationsHistoryTable("__EFMigrationsHistory", GameWorldDbContext.SchemaName);
                });
                InBuilder.UseStronglyTypedIdValueConverter();
            });

            return InServiceCollection;
        }
    }
}