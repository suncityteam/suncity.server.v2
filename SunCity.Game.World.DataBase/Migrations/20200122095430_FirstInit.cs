﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace SunCity.Game.World.DataBase.Migrations
{
    public partial class FirstInit : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.EnsureSchema(
                name: "GameWorld");

            migrationBuilder.CreateTable(
                name: "InteriorEntity",
                schema: "GameWorld",
                columns: table => new
                {
                    EntityId = table.Column<Guid>(nullable: false),
                    IPL = table.Column<string>(nullable: false),
                    Position_X = table.Column<float>(nullable: true),
                    Position_Y = table.Column<float>(nullable: true),
                    Position_Z = table.Column<float>(nullable: true),
                    Rotation = table.Column<float>(nullable: false),
                    SpawnPosition_X = table.Column<float>(nullable: true),
                    SpawnPosition_Y = table.Column<float>(nullable: true),
                    SpawnPosition_Z = table.Column<float>(nullable: true),
                    SpawnRotation = table.Column<float>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_InteriorEntity", x => x.EntityId);
                });

            migrationBuilder.CreateTable(
                name: "PropertyEntity",
                schema: "GameWorld",
                columns: table => new
                {
                    EntityId = table.Column<Guid>(nullable: false),
                    Name = table.Column<string>(nullable: false),
                    InteriorId = table.Column<Guid>(nullable: false),
                    IconPosition_X = table.Column<float>(nullable: true),
                    IconPosition_Y = table.Column<float>(nullable: true),
                    IconPosition_Z = table.Column<float>(nullable: true),
                    Position_X = table.Column<float>(nullable: true),
                    Position_Y = table.Column<float>(nullable: true),
                    Position_Z = table.Column<float>(nullable: true),
                    Rotation = table.Column<float>(nullable: false),
                    StateCost_Amount = table.Column<long>(nullable: true),
                    StateCost_Currency = table.Column<byte>(nullable: true),
                    SellingCost_Amount = table.Column<long>(nullable: true),
                    SellingCost_Currency = table.Column<byte>(nullable: true),
                    IsSelling = table.Column<bool>(nullable: false),
                    PropertyClass = table.Column<byte>(nullable: false),
                    PropertyCategory = table.Column<byte>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PropertyEntity", x => x.EntityId);
                    table.ForeignKey(
                        name: "FK_PropertyEntity_InteriorEntity_InteriorId",
                        column: x => x.InteriorId,
                        principalSchema: "GameWorld",
                        principalTable: "InteriorEntity",
                        principalColumn: "EntityId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "BusinessPropertyEntity",
                schema: "GameWorld",
                columns: table => new
                {
                    EntityId = table.Column<Guid>(nullable: false),
                    Level = table.Column<int>(nullable: false),
                    Experience = table.Column<long>(nullable: false),
                    WorkingTime_Begin = table.Column<TimeSpan>(nullable: true),
                    WorkingTime_End = table.Column<TimeSpan>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BusinessPropertyEntity", x => x.EntityId);
                    table.ForeignKey(
                        name: "FK_BusinessPropertyEntity_PropertyEntity_EntityId",
                        column: x => x.EntityId,
                        principalSchema: "GameWorld",
                        principalTable: "PropertyEntity",
                        principalColumn: "EntityId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "ResidencePropertyEntity",
                schema: "GameWorld",
                columns: table => new
                {
                    EntityId = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ResidencePropertyEntity", x => x.EntityId);
                    table.ForeignKey(
                        name: "FK_ResidencePropertyEntity_PropertyEntity_EntityId",
                        column: x => x.EntityId,
                        principalSchema: "GameWorld",
                        principalTable: "PropertyEntity",
                        principalColumn: "EntityId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "BankPropertyEntity",
                schema: "GameWorld",
                columns: table => new
                {
                    EntityId = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BankPropertyEntity", x => x.EntityId);
                    table.ForeignKey(
                        name: "FK_BankPropertyEntity_BusinessPropertyEntity_EntityId",
                        column: x => x.EntityId,
                        principalSchema: "GameWorld",
                        principalTable: "BusinessPropertyEntity",
                        principalColumn: "EntityId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "GasStationPropertyEntity",
                schema: "GameWorld",
                columns: table => new
                {
                    EntityId = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_GasStationPropertyEntity", x => x.EntityId);
                    table.ForeignKey(
                        name: "FK_GasStationPropertyEntity_BusinessPropertyEntity_EntityId",
                        column: x => x.EntityId,
                        principalSchema: "GameWorld",
                        principalTable: "BusinessPropertyEntity",
                        principalColumn: "EntityId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "HospitalPropertyEntity",
                schema: "GameWorld",
                columns: table => new
                {
                    EntityId = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_HospitalPropertyEntity", x => x.EntityId);
                    table.ForeignKey(
                        name: "FK_HospitalPropertyEntity_BusinessPropertyEntity_EntityId",
                        column: x => x.EntityId,
                        principalSchema: "GameWorld",
                        principalTable: "BusinessPropertyEntity",
                        principalColumn: "EntityId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "BankCardTariffEntity",
                schema: "GameWorld",
                columns: table => new
                {
                    EntityId = table.Column<Guid>(nullable: false),
                    Name = table.Column<string>(maxLength: 64, nullable: false),
                    Deleted = table.Column<bool>(nullable: false),
                    PropertyId = table.Column<Guid>(nullable: false),
                    Deposit_Own = table.Column<double>(nullable: true),
                    Deposit_Another = table.Column<double>(nullable: true),
                    WithDraw_Own = table.Column<double>(nullable: true),
                    WithDraw_Another = table.Column<double>(nullable: true),
                    Transfer_Own = table.Column<double>(nullable: true),
                    Transfer_Another = table.Column<double>(nullable: true),
                    MaintenanceCost_Amount = table.Column<long>(nullable: true),
                    MaintenanceCost_Currency = table.Column<byte>(nullable: true),
                    TariffCost_Amount = table.Column<long>(nullable: true),
                    TariffCost_Currency = table.Column<byte>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BankCardTariffEntity", x => x.EntityId);
                    table.ForeignKey(
                        name: "FK_BankCardTariffEntity_BankPropertyEntity_PropertyId",
                        column: x => x.PropertyId,
                        principalSchema: "GameWorld",
                        principalTable: "BankPropertyEntity",
                        principalColumn: "EntityId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "BankCardTransactionEntity",
                schema: "GameWorld",
                columns: table => new
                {
                    EntityId = table.Column<Guid>(nullable: false),
                    SenderId = table.Column<Guid>(nullable: false),
                    RecipientId = table.Column<Guid>(nullable: false),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    Amount = table.Column<long>(nullable: false),
                    IpAddress = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BankCardTransactionEntity", x => x.EntityId);
                });

            migrationBuilder.CreateTable(
                name: "BankCardAccountEntity",
                schema: "GameWorld",
                columns: table => new
                {
                    EntityId = table.Column<Guid>(nullable: false),
                    Name = table.Column<string>(maxLength: 32, nullable: true),
                    Number = table.Column<long>(nullable: false),
                    PinCodeHash = table.Column<long>(nullable: false),
                    SecureWordHash = table.Column<long>(nullable: false),
                    TariffId = table.Column<Guid>(nullable: false),
                    CharacterId = table.Column<Guid>(nullable: false),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    ExpiredDate = table.Column<DateTime>(nullable: false),
                    Balance = table.Column<long>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BankCardAccountEntity", x => x.EntityId);
                    table.ForeignKey(
                        name: "FK_BankCardAccountEntity_BankCardTariffEntity_TariffId",
                        column: x => x.TariffId,
                        principalSchema: "GameWorld",
                        principalTable: "BankCardTariffEntity",
                        principalColumn: "EntityId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "PlayerCharacterEntity",
                schema: "GameWorld",
                columns: table => new
                {
                    EntityId = table.Column<Guid>(nullable: false),
                    Name = table.Column<string>(maxLength: 64, nullable: false),
                    IsBlocked = table.Column<bool>(nullable: false),
                    PlayerAccountId = table.Column<Guid>(nullable: false),
                    Health = table.Column<long>(nullable: false),
                    Hunger = table.Column<long>(nullable: false),
                    EnteredPropertyId = table.Column<Guid>(nullable: true),
                    Position_X = table.Column<float>(nullable: true),
                    Position_Y = table.Column<float>(nullable: true),
                    Position_Z = table.Column<float>(nullable: true),
                    Rotation = table.Column<float>(nullable: false),
                    CreatedDate = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PlayerCharacterEntity", x => x.EntityId);
                });

            migrationBuilder.CreateTable(
                name: "PlayerAccountEntity",
                schema: "GameWorld",
                columns: table => new
                {
                    EntityId = table.Column<Guid>(nullable: false),
                    Login = table.Column<string>(maxLength: 32, nullable: false),
                    Balance = table.Column<long>(nullable: false),
                    DefaultCharacterId = table.Column<Guid>(nullable: true),
                    PremiumAccountEndDate = table.Column<DateTime>(nullable: false),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    DeviceId = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PlayerAccountEntity", x => x.EntityId);
                    table.ForeignKey(
                        name: "FK_PlayerAccountEntity_PlayerCharacterEntity_DefaultCharacterId",
                        column: x => x.DefaultCharacterId,
                        principalSchema: "GameWorld",
                        principalTable: "PlayerCharacterEntity",
                        principalColumn: "EntityId",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "PlayerCharacterExperienceEntity",
                schema: "GameWorld",
                columns: table => new
                {
                    EntityId = table.Column<Guid>(nullable: false),
                    Level = table.Column<byte>(nullable: false),
                    Experience = table.Column<long>(nullable: false),
                    PlayedTotalTime = table.Column<TimeSpan>(nullable: false),
                    PlayedForPayDay = table.Column<TimeSpan>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PlayerCharacterExperienceEntity", x => x.EntityId);
                    table.ForeignKey(
                        name: "FK_PlayerCharacterExperienceEntity_PlayerCharacterEntity_Entit~",
                        column: x => x.EntityId,
                        principalSchema: "GameWorld",
                        principalTable: "PlayerCharacterEntity",
                        principalColumn: "EntityId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "PlayerCharacterSkinEntity",
                schema: "GameWorld",
                columns: table => new
                {
                    EntityId = table.Column<Guid>(nullable: false),
                    Sex = table.Column<byte>(nullable: false),
                    FirstHeadShape = table.Column<byte>(nullable: false),
                    SecondHeadShape = table.Column<byte>(nullable: false),
                    FirstSkinTone = table.Column<byte>(nullable: false),
                    SecondSkinTone = table.Column<byte>(nullable: false),
                    HeadMix = table.Column<float>(nullable: false),
                    SkinMix = table.Column<float>(nullable: false),
                    HairModel = table.Column<byte>(nullable: false),
                    FirstHairColor = table.Column<byte>(nullable: false),
                    SecondHairColor = table.Column<byte>(nullable: false),
                    BeardModel = table.Column<short>(nullable: false),
                    BeardColor = table.Column<short>(nullable: false),
                    ChestModel = table.Column<short>(nullable: false),
                    ChestColor = table.Column<short>(nullable: false),
                    BlemishesModel = table.Column<short>(nullable: false),
                    AgeingModel = table.Column<short>(nullable: false),
                    ComplexionModel = table.Column<short>(nullable: false),
                    SundamageModel = table.Column<short>(nullable: false),
                    FrecklesModel = table.Column<short>(nullable: false),
                    EyesColor = table.Column<byte>(nullable: false),
                    EyebrowsModel = table.Column<short>(nullable: false),
                    EyebrowsColor = table.Column<short>(nullable: false),
                    MakeupModel = table.Column<short>(nullable: false),
                    BlushModel = table.Column<short>(nullable: false),
                    BlushColor = table.Column<short>(nullable: false),
                    LipstickModel = table.Column<short>(nullable: false),
                    LipstickColor = table.Column<short>(nullable: false),
                    NoseWidth = table.Column<float>(nullable: false),
                    NoseHeight = table.Column<float>(nullable: false),
                    NoseLength = table.Column<float>(nullable: false),
                    NoseBridge = table.Column<float>(nullable: false),
                    NoseTip = table.Column<float>(nullable: false),
                    NoseShift = table.Column<float>(nullable: false),
                    BrowHeight = table.Column<float>(nullable: false),
                    BrowWidth = table.Column<float>(nullable: false),
                    CheekboneHeight = table.Column<float>(nullable: false),
                    CheekboneWidth = table.Column<float>(nullable: false),
                    CheeksWidth = table.Column<float>(nullable: false),
                    Eyes = table.Column<float>(nullable: false),
                    Lips = table.Column<float>(nullable: false),
                    JawWidth = table.Column<float>(nullable: false),
                    JawHeight = table.Column<float>(nullable: false),
                    ChinLength = table.Column<float>(nullable: false),
                    ChinPosition = table.Column<float>(nullable: false),
                    ChinWidth = table.Column<float>(nullable: false),
                    ChinShape = table.Column<float>(nullable: false),
                    NeckWidth = table.Column<float>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PlayerCharacterSkinEntity", x => x.EntityId);
                    table.ForeignKey(
                        name: "FK_PlayerCharacterSkinEntity_PlayerCharacterEntity_EntityId",
                        column: x => x.EntityId,
                        principalSchema: "GameWorld",
                        principalTable: "PlayerCharacterEntity",
                        principalColumn: "EntityId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_BankCardAccountEntity_Balance",
                schema: "GameWorld",
                table: "BankCardAccountEntity",
                column: "Balance");

            migrationBuilder.CreateIndex(
                name: "IX_BankCardAccountEntity_CharacterId",
                schema: "GameWorld",
                table: "BankCardAccountEntity",
                column: "CharacterId");

            migrationBuilder.CreateIndex(
                name: "IX_BankCardAccountEntity_Number",
                schema: "GameWorld",
                table: "BankCardAccountEntity",
                column: "Number",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_BankCardAccountEntity_TariffId",
                schema: "GameWorld",
                table: "BankCardAccountEntity",
                column: "TariffId");

            migrationBuilder.CreateIndex(
                name: "IX_BankCardAccountEntity_EntityId_PinCodeHash",
                schema: "GameWorld",
                table: "BankCardAccountEntity",
                columns: new[] { "EntityId", "PinCodeHash" });

            migrationBuilder.CreateIndex(
                name: "IX_BankCardTariffEntity_PropertyId",
                schema: "GameWorld",
                table: "BankCardTariffEntity",
                column: "PropertyId");

            migrationBuilder.CreateIndex(
                name: "IX_BankCardTransactionEntity_RecipientId",
                schema: "GameWorld",
                table: "BankCardTransactionEntity",
                column: "RecipientId");

            migrationBuilder.CreateIndex(
                name: "IX_BankCardTransactionEntity_SenderId",
                schema: "GameWorld",
                table: "BankCardTransactionEntity",
                column: "SenderId");

            migrationBuilder.CreateIndex(
                name: "IX_PlayerAccountEntity_DefaultCharacterId",
                schema: "GameWorld",
                table: "PlayerAccountEntity",
                column: "DefaultCharacterId");

            migrationBuilder.CreateIndex(
                name: "IX_PlayerCharacterEntity_PlayerAccountId",
                schema: "GameWorld",
                table: "PlayerCharacterEntity",
                column: "PlayerAccountId");

            migrationBuilder.CreateIndex(
                name: "IX_PropertyEntity_InteriorId",
                schema: "GameWorld",
                table: "PropertyEntity",
                column: "InteriorId");

            migrationBuilder.AddForeignKey(
                name: "FK_BankCardTransactionEntity_BankCardAccountEntity_RecipientId",
                schema: "GameWorld",
                table: "BankCardTransactionEntity",
                column: "RecipientId",
                principalSchema: "GameWorld",
                principalTable: "BankCardAccountEntity",
                principalColumn: "EntityId",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_BankCardTransactionEntity_BankCardAccountEntity_SenderId",
                schema: "GameWorld",
                table: "BankCardTransactionEntity",
                column: "SenderId",
                principalSchema: "GameWorld",
                principalTable: "BankCardAccountEntity",
                principalColumn: "EntityId",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_BankCardAccountEntity_PlayerCharacterEntity_CharacterId",
                schema: "GameWorld",
                table: "BankCardAccountEntity",
                column: "CharacterId",
                principalSchema: "GameWorld",
                principalTable: "PlayerCharacterEntity",
                principalColumn: "EntityId",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_PlayerCharacterEntity_PlayerAccountEntity_PlayerAccountId",
                schema: "GameWorld",
                table: "PlayerCharacterEntity",
                column: "PlayerAccountId",
                principalSchema: "GameWorld",
                principalTable: "PlayerAccountEntity",
                principalColumn: "EntityId",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_PlayerAccountEntity_PlayerCharacterEntity_DefaultCharacterId",
                schema: "GameWorld",
                table: "PlayerAccountEntity");

            migrationBuilder.DropTable(
                name: "BankCardTransactionEntity",
                schema: "GameWorld");

            migrationBuilder.DropTable(
                name: "GasStationPropertyEntity",
                schema: "GameWorld");

            migrationBuilder.DropTable(
                name: "HospitalPropertyEntity",
                schema: "GameWorld");

            migrationBuilder.DropTable(
                name: "PlayerCharacterExperienceEntity",
                schema: "GameWorld");

            migrationBuilder.DropTable(
                name: "PlayerCharacterSkinEntity",
                schema: "GameWorld");

            migrationBuilder.DropTable(
                name: "ResidencePropertyEntity",
                schema: "GameWorld");

            migrationBuilder.DropTable(
                name: "BankCardAccountEntity",
                schema: "GameWorld");

            migrationBuilder.DropTable(
                name: "BankCardTariffEntity",
                schema: "GameWorld");

            migrationBuilder.DropTable(
                name: "BankPropertyEntity",
                schema: "GameWorld");

            migrationBuilder.DropTable(
                name: "BusinessPropertyEntity",
                schema: "GameWorld");

            migrationBuilder.DropTable(
                name: "PropertyEntity",
                schema: "GameWorld");

            migrationBuilder.DropTable(
                name: "InteriorEntity",
                schema: "GameWorld");

            migrationBuilder.DropTable(
                name: "PlayerCharacterEntity",
                schema: "GameWorld");

            migrationBuilder.DropTable(
                name: "PlayerAccountEntity",
                schema: "GameWorld");
        }
    }
}
