﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace SunCity.Game.World.DataBase.Migrations
{
    public partial class AddedBusStationFlight : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "BusStationFlightEntity",
                schema: "GameWorld",
                columns: table => new
                {
                    EntityId = table.Column<Guid>(nullable: false),
                    Status = table.Column<byte>(nullable: false),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    ExpiredDate = table.Column<DateTime>(nullable: false),
                    RouteId = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BusStationFlightEntity", x => x.EntityId);
                    table.ForeignKey(
                        name: "FK_BusStationFlightEntity_BusStationRouteEntity_RouteId",
                        column: x => x.RouteId,
                        principalSchema: "GameWorld",
                        principalTable: "BusStationRouteEntity",
                        principalColumn: "EntityId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "StreetRacingPropertyEntity",
                schema: "GameWorld",
                columns: table => new
                {
                    EntityId = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_StreetRacingPropertyEntity", x => x.EntityId);
                    table.ForeignKey(
                        name: "FK_StreetRacingPropertyEntity_BusinessPropertyEntity_EntityId",
                        column: x => x.EntityId,
                        principalSchema: "GameWorld",
                        principalTable: "BusinessPropertyEntity",
                        principalColumn: "EntityId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "BusStationFlightMemberEntity",
                schema: "GameWorld",
                columns: table => new
                {
                    EntityId = table.Column<Guid>(nullable: false),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    ExpiredDate = table.Column<DateTime>(nullable: false),
                    FlightId = table.Column<Guid>(nullable: false),
                    CharacterId = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BusStationFlightMemberEntity", x => x.EntityId);
                    table.ForeignKey(
                        name: "FK_BusStationFlightMemberEntity_PlayerCharacterEntity_Characte~",
                        column: x => x.CharacterId,
                        principalSchema: "GameWorld",
                        principalTable: "PlayerCharacterEntity",
                        principalColumn: "EntityId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_BusStationFlightMemberEntity_BusStationFlightEntity_FlightId",
                        column: x => x.FlightId,
                        principalSchema: "GameWorld",
                        principalTable: "BusStationFlightEntity",
                        principalColumn: "EntityId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "StreetRacingRouteEntity",
                schema: "GameWorld",
                columns: table => new
                {
                    EntityId = table.Column<Guid>(nullable: false),
                    StreetRacingId = table.Column<Guid>(nullable: false),
                    Name = table.Column<string>(maxLength: 64, nullable: false),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    Deleted = table.Column<bool>(nullable: false),
                    DeletedDate = table.Column<DateTime>(nullable: true),
                    Cost_Amount = table.Column<long>(nullable: true),
                    Cost_Currency = table.Column<byte>(nullable: true),
                    Reward_First_Amount = table.Column<long>(nullable: true),
                    Reward_First_Currency = table.Column<byte>(nullable: true),
                    Reward_Second_Amount = table.Column<long>(nullable: true),
                    Reward_Second_Currency = table.Column<byte>(nullable: true),
                    Reward_Third_Amount = table.Column<long>(nullable: true),
                    Reward_Third_Currency = table.Column<byte>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_StreetRacingRouteEntity", x => x.EntityId);
                    table.ForeignKey(
                        name: "FK_StreetRacingRouteEntity_StreetRacingPropertyEntity_StreetRa~",
                        column: x => x.StreetRacingId,
                        principalSchema: "GameWorld",
                        principalTable: "StreetRacingPropertyEntity",
                        principalColumn: "EntityId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "StreetRacingRoutePointEntity",
                schema: "GameWorld",
                columns: table => new
                {
                    EntityId = table.Column<Guid>(nullable: false),
                    RouteId = table.Column<Guid>(nullable: false),
                    StationPointEntityEntityId = table.Column<Guid>(nullable: true),
                    StationPointId = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_StreetRacingRoutePointEntity", x => x.EntityId);
                    table.ForeignKey(
                        name: "FK_StreetRacingRoutePointEntity_StreetRacingRouteEntity_RouteId",
                        column: x => x.RouteId,
                        principalSchema: "GameWorld",
                        principalTable: "StreetRacingRouteEntity",
                        principalColumn: "EntityId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_StreetRacingRoutePointEntity_StationPointEntity_StationPoin~",
                        column: x => x.StationPointEntityEntityId,
                        principalSchema: "GameWorld",
                        principalTable: "StationPointEntity",
                        principalColumn: "EntityId",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_BusStationFlightEntity_RouteId",
                schema: "GameWorld",
                table: "BusStationFlightEntity",
                column: "RouteId");

            migrationBuilder.CreateIndex(
                name: "IX_BusStationFlightMemberEntity_CharacterId",
                schema: "GameWorld",
                table: "BusStationFlightMemberEntity",
                column: "CharacterId");

            migrationBuilder.CreateIndex(
                name: "IX_BusStationFlightMemberEntity_FlightId",
                schema: "GameWorld",
                table: "BusStationFlightMemberEntity",
                column: "FlightId");

            migrationBuilder.CreateIndex(
                name: "IX_StreetRacingRouteEntity_StreetRacingId",
                schema: "GameWorld",
                table: "StreetRacingRouteEntity",
                column: "StreetRacingId");

            migrationBuilder.CreateIndex(
                name: "IX_StreetRacingRoutePointEntity_RouteId",
                schema: "GameWorld",
                table: "StreetRacingRoutePointEntity",
                column: "RouteId");

            migrationBuilder.CreateIndex(
                name: "IX_StreetRacingRoutePointEntity_StationPointEntityEntityId",
                schema: "GameWorld",
                table: "StreetRacingRoutePointEntity",
                column: "StationPointEntityEntityId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "BusStationFlightMemberEntity",
                schema: "GameWorld");

            migrationBuilder.DropTable(
                name: "StreetRacingRoutePointEntity",
                schema: "GameWorld");

            migrationBuilder.DropTable(
                name: "BusStationFlightEntity",
                schema: "GameWorld");

            migrationBuilder.DropTable(
                name: "StreetRacingRouteEntity",
                schema: "GameWorld");

            migrationBuilder.DropTable(
                name: "StreetRacingPropertyEntity",
                schema: "GameWorld");
        }
    }
}
