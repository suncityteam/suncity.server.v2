﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace SunCity.Game.World.DataBase.Migrations
{
    public partial class AddedGameItemDescription : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "Description",
                schema: "GameWorld",
                table: "ItemEntity",
                maxLength: 8192,
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Description",
                schema: "GameWorld",
                table: "ItemEntity");
        }
    }
}
