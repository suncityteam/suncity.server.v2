﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace SunCity.Game.World.DataBase.Migrations
{
    public partial class AddedDrivingSchoolLicenseEntity : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "DrivingSchoolLicenseEntity",
                schema: "GameWorld",
                columns: table => new
                {
                    EntityId = table.Column<Guid>(nullable: false),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    Deleted = table.Column<bool>(nullable: false),
                    DeletedDate = table.Column<DateTime>(nullable: true),
                    LicenseId = table.Column<Guid>(nullable: false),
                    DrivingSchoolId = table.Column<Guid>(nullable: false),
                    Cost_Amount = table.Column<long>(nullable: true),
                    Cost_Currency = table.Column<byte>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DrivingSchoolLicenseEntity", x => x.EntityId);
                    table.ForeignKey(
                        name: "FK_DrivingSchoolLicenseEntity_DrivingSchoolPropertyEntity_Driv~",
                        column: x => x.DrivingSchoolId,
                        principalSchema: "GameWorld",
                        principalTable: "DrivingSchoolPropertyEntity",
                        principalColumn: "EntityId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_DrivingSchoolLicenseEntity_GameLicenseEntity_LicenseId",
                        column: x => x.LicenseId,
                        principalSchema: "GameWorld",
                        principalTable: "GameLicenseEntity",
                        principalColumn: "EntityId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_DrivingSchoolLicenseEntity_DrivingSchoolId",
                schema: "GameWorld",
                table: "DrivingSchoolLicenseEntity",
                column: "DrivingSchoolId");

            migrationBuilder.CreateIndex(
                name: "IX_DrivingSchoolLicenseEntity_LicenseId",
                schema: "GameWorld",
                table: "DrivingSchoolLicenseEntity",
                column: "LicenseId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "DrivingSchoolLicenseEntity",
                schema: "GameWorld");
        }
    }
}
