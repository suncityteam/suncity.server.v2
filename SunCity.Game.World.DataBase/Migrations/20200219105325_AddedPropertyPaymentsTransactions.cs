﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace SunCity.Game.World.DataBase.Migrations
{
    public partial class AddedPropertyPaymentsTransactions : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "BankCardPropertyPaymentFromTransactionEntity",
                schema: "GameWorld",
                columns: table => new
                {
                    EntityId = table.Column<Guid>(nullable: false),
                    PropertyId = table.Column<Guid>(nullable: false),
                    RecipientId = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BankCardPropertyPaymentFromTransactionEntity", x => x.EntityId);
                    table.ForeignKey(
                        name: "FK_BankCardPropertyPaymentFromTransactionEntity_BankCardTransa~",
                        column: x => x.EntityId,
                        principalSchema: "GameWorld",
                        principalTable: "BankCardTransactionEntity",
                        principalColumn: "EntityId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_BankCardPropertyPaymentFromTransactionEntity_PropertyEntity~",
                        column: x => x.PropertyId,
                        principalSchema: "GameWorld",
                        principalTable: "PropertyEntity",
                        principalColumn: "EntityId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_BankCardPropertyPaymentFromTransactionEntity_BankCardAccoun~",
                        column: x => x.RecipientId,
                        principalSchema: "GameWorld",
                        principalTable: "BankCardAccountEntity",
                        principalColumn: "EntityId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "BankCardPropertyPaymentToTransactionEntity",
                schema: "GameWorld",
                columns: table => new
                {
                    EntityId = table.Column<Guid>(nullable: false),
                    PropertyId = table.Column<Guid>(nullable: false),
                    PayerId = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BankCardPropertyPaymentToTransactionEntity", x => x.EntityId);
                    table.ForeignKey(
                        name: "FK_BankCardPropertyPaymentToTransactionEntity_BankCardTransact~",
                        column: x => x.EntityId,
                        principalSchema: "GameWorld",
                        principalTable: "BankCardTransactionEntity",
                        principalColumn: "EntityId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_BankCardPropertyPaymentToTransactionEntity_BankCardAccountE~",
                        column: x => x.PayerId,
                        principalSchema: "GameWorld",
                        principalTable: "BankCardAccountEntity",
                        principalColumn: "EntityId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_BankCardPropertyPaymentToTransactionEntity_PropertyEntity_P~",
                        column: x => x.PropertyId,
                        principalSchema: "GameWorld",
                        principalTable: "PropertyEntity",
                        principalColumn: "EntityId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_BankCardPropertyPaymentFromTransactionEntity_PropertyId",
                schema: "GameWorld",
                table: "BankCardPropertyPaymentFromTransactionEntity",
                column: "PropertyId");

            migrationBuilder.CreateIndex(
                name: "IX_BankCardPropertyPaymentFromTransactionEntity_RecipientId",
                schema: "GameWorld",
                table: "BankCardPropertyPaymentFromTransactionEntity",
                column: "RecipientId");

            migrationBuilder.CreateIndex(
                name: "IX_BankCardPropertyPaymentToTransactionEntity_PayerId",
                schema: "GameWorld",
                table: "BankCardPropertyPaymentToTransactionEntity",
                column: "PayerId");

            migrationBuilder.CreateIndex(
                name: "IX_BankCardPropertyPaymentToTransactionEntity_PropertyId",
                schema: "GameWorld",
                table: "BankCardPropertyPaymentToTransactionEntity",
                column: "PropertyId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "BankCardPropertyPaymentFromTransactionEntity",
                schema: "GameWorld");

            migrationBuilder.DropTable(
                name: "BankCardPropertyPaymentToTransactionEntity",
                schema: "GameWorld");
        }
    }
}
