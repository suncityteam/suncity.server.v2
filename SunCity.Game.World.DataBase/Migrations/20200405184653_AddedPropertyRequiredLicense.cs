﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace SunCity.Game.World.DataBase.Migrations
{
    public partial class AddedPropertyRequiredLicense : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "PropertyRequiredLicenseEntity",
                schema: "GameWorld",
                columns: table => new
                {
                    EntityId = table.Column<Guid>(nullable: false),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    PropertyId = table.Column<Guid>(nullable: false),
                    RequiredLicenseId = table.Column<Guid>(nullable: false),
                    Deleted = table.Column<bool>(nullable: false),
                    DeletedDate = table.Column<DateTime>(nullable: true),
                    AdminComment = table.Column<string>(maxLength: 512, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PropertyRequiredLicenseEntity", x => x.EntityId);
                    table.ForeignKey(
                        name: "FK_PropertyRequiredLicenseEntity_PropertyEntity_PropertyId",
                        column: x => x.PropertyId,
                        principalSchema: "GameWorld",
                        principalTable: "PropertyEntity",
                        principalColumn: "EntityId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_PropertyRequiredLicenseEntity_GameLicenseEntity_RequiredLic~",
                        column: x => x.RequiredLicenseId,
                        principalSchema: "GameWorld",
                        principalTable: "GameLicenseEntity",
                        principalColumn: "EntityId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_PropertyRequiredLicenseEntity_PropertyId",
                schema: "GameWorld",
                table: "PropertyRequiredLicenseEntity",
                column: "PropertyId");

            migrationBuilder.CreateIndex(
                name: "IX_PropertyRequiredLicenseEntity_RequiredLicenseId",
                schema: "GameWorld",
                table: "PropertyRequiredLicenseEntity",
                column: "RequiredLicenseId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "PropertyRequiredLicenseEntity",
                schema: "GameWorld");
        }
    }
}
