﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace SunCity.Game.World.DataBase.Migrations
{
    public partial class AddedInteriorActionEntity : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "AdminDescription",
                schema: "GameWorld",
                table: "PropertyEntity",
                maxLength: 64,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "AdminDescription",
                schema: "GameWorld",
                table: "InteriorEntity",
                maxLength: 64,
                nullable: true);

            migrationBuilder.CreateTable(
                name: "InteriorActionEntity",
                schema: "GameWorld",
                columns: table => new
                {
                    EntityId = table.Column<Guid>(nullable: false),
                    InteriorId = table.Column<Guid>(nullable: false),
                    Position_X = table.Column<float>(nullable: true),
                    Position_Y = table.Column<float>(nullable: true),
                    Position_Z = table.Column<float>(nullable: true),
                    Rotation = table.Column<float>(nullable: false),
                    Action = table.Column<int>(nullable: false),
                    AdminDescription = table.Column<string>(maxLength: 64, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_InteriorActionEntity", x => x.EntityId);
                    table.ForeignKey(
                        name: "FK_InteriorActionEntity_InteriorEntity_InteriorId",
                        column: x => x.InteriorId,
                        principalSchema: "GameWorld",
                        principalTable: "InteriorEntity",
                        principalColumn: "EntityId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_InteriorActionEntity_InteriorId",
                schema: "GameWorld",
                table: "InteriorActionEntity",
                column: "InteriorId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "InteriorActionEntity",
                schema: "GameWorld");

            migrationBuilder.DropColumn(
                name: "AdminDescription",
                schema: "GameWorld",
                table: "PropertyEntity");

            migrationBuilder.DropColumn(
                name: "AdminDescription",
                schema: "GameWorld",
                table: "InteriorEntity");
        }
    }
}
