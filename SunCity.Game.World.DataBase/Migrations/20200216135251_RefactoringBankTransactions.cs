﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace SunCity.Game.World.DataBase.Migrations
{
    public partial class RefactoringBankTransactions : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_BankCardTransactionEntity_BankCardAccountEntity_RecipientId",
                schema: "GameWorld",
                table: "BankCardTransactionEntity");

            migrationBuilder.DropForeignKey(
                name: "FK_BankCardTransactionEntity_BankCardAccountEntity_SenderId",
                schema: "GameWorld",
                table: "BankCardTransactionEntity");

            migrationBuilder.DropIndex(
                name: "IX_BankCardTransactionEntity_RecipientId",
                schema: "GameWorld",
                table: "BankCardTransactionEntity");

            migrationBuilder.DropIndex(
                name: "IX_BankCardTransactionEntity_SenderId",
                schema: "GameWorld",
                table: "BankCardTransactionEntity");

            migrationBuilder.DropColumn(
                name: "IpAddress",
                schema: "GameWorld",
                table: "BankCardTransactionEntity");

            migrationBuilder.DropColumn(
                name: "RecipientId",
                schema: "GameWorld",
                table: "BankCardTransactionEntity");

            migrationBuilder.DropColumn(
                name: "SenderId",
                schema: "GameWorld",
                table: "BankCardTransactionEntity");

            migrationBuilder.AddColumn<long>(
                name: "Commission",
                schema: "GameWorld",
                table: "BankCardTransactionEntity",
                nullable: false,
                defaultValue: 0L);

            migrationBuilder.CreateTable(
                name: "BankCardDepositTransactionEntity",
                schema: "GameWorld",
                columns: table => new
                {
                    EntityId = table.Column<Guid>(nullable: false),
                    SenderId = table.Column<Guid>(nullable: false),
                    RecipientId = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BankCardDepositTransactionEntity", x => x.EntityId);
                    table.ForeignKey(
                        name: "FK_BankCardDepositTransactionEntity_BankCardTransactionEntity_~",
                        column: x => x.EntityId,
                        principalSchema: "GameWorld",
                        principalTable: "BankCardTransactionEntity",
                        principalColumn: "EntityId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_BankCardDepositTransactionEntity_BankCardAccountEntity_Reci~",
                        column: x => x.RecipientId,
                        principalSchema: "GameWorld",
                        principalTable: "BankCardAccountEntity",
                        principalColumn: "EntityId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_BankCardDepositTransactionEntity_PlayerCharacterEntity_Send~",
                        column: x => x.SenderId,
                        principalSchema: "GameWorld",
                        principalTable: "PlayerCharacterEntity",
                        principalColumn: "EntityId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "BankCardTransferTransactionEntity",
                schema: "GameWorld",
                columns: table => new
                {
                    EntityId = table.Column<Guid>(nullable: false),
                    SenderId = table.Column<Guid>(nullable: false),
                    RecipientId = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BankCardTransferTransactionEntity", x => x.EntityId);
                    table.ForeignKey(
                        name: "FK_BankCardTransferTransactionEntity_BankCardTransactionEntity~",
                        column: x => x.EntityId,
                        principalSchema: "GameWorld",
                        principalTable: "BankCardTransactionEntity",
                        principalColumn: "EntityId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_BankCardTransferTransactionEntity_BankCardAccountEntity_Rec~",
                        column: x => x.RecipientId,
                        principalSchema: "GameWorld",
                        principalTable: "BankCardAccountEntity",
                        principalColumn: "EntityId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_BankCardTransferTransactionEntity_BankCardAccountEntity_Sen~",
                        column: x => x.SenderId,
                        principalSchema: "GameWorld",
                        principalTable: "BankCardAccountEntity",
                        principalColumn: "EntityId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "BankCardWithdrawTransactionEntity",
                schema: "GameWorld",
                columns: table => new
                {
                    EntityId = table.Column<Guid>(nullable: false),
                    SenderId = table.Column<Guid>(nullable: false),
                    RecipientId = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BankCardWithdrawTransactionEntity", x => x.EntityId);
                    table.ForeignKey(
                        name: "FK_BankCardWithdrawTransactionEntity_BankCardTransactionEntity~",
                        column: x => x.EntityId,
                        principalSchema: "GameWorld",
                        principalTable: "BankCardTransactionEntity",
                        principalColumn: "EntityId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_BankCardWithdrawTransactionEntity_PlayerCharacterEntity_Rec~",
                        column: x => x.RecipientId,
                        principalSchema: "GameWorld",
                        principalTable: "PlayerCharacterEntity",
                        principalColumn: "EntityId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_BankCardWithdrawTransactionEntity_BankCardAccountEntity_Sen~",
                        column: x => x.SenderId,
                        principalSchema: "GameWorld",
                        principalTable: "BankCardAccountEntity",
                        principalColumn: "EntityId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_BankCardDepositTransactionEntity_RecipientId",
                schema: "GameWorld",
                table: "BankCardDepositTransactionEntity",
                column: "RecipientId");

            migrationBuilder.CreateIndex(
                name: "IX_BankCardDepositTransactionEntity_SenderId",
                schema: "GameWorld",
                table: "BankCardDepositTransactionEntity",
                column: "SenderId");

            migrationBuilder.CreateIndex(
                name: "IX_BankCardTransferTransactionEntity_RecipientId",
                schema: "GameWorld",
                table: "BankCardTransferTransactionEntity",
                column: "RecipientId");

            migrationBuilder.CreateIndex(
                name: "IX_BankCardTransferTransactionEntity_SenderId",
                schema: "GameWorld",
                table: "BankCardTransferTransactionEntity",
                column: "SenderId");

            migrationBuilder.CreateIndex(
                name: "IX_BankCardWithdrawTransactionEntity_RecipientId",
                schema: "GameWorld",
                table: "BankCardWithdrawTransactionEntity",
                column: "RecipientId");

            migrationBuilder.CreateIndex(
                name: "IX_BankCardWithdrawTransactionEntity_SenderId",
                schema: "GameWorld",
                table: "BankCardWithdrawTransactionEntity",
                column: "SenderId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "BankCardDepositTransactionEntity",
                schema: "GameWorld");

            migrationBuilder.DropTable(
                name: "BankCardTransferTransactionEntity",
                schema: "GameWorld");

            migrationBuilder.DropTable(
                name: "BankCardWithdrawTransactionEntity",
                schema: "GameWorld");

            migrationBuilder.DropColumn(
                name: "Commission",
                schema: "GameWorld",
                table: "BankCardTransactionEntity");

            migrationBuilder.AddColumn<int>(
                name: "IpAddress",
                schema: "GameWorld",
                table: "BankCardTransactionEntity",
                type: "integer",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<Guid>(
                name: "RecipientId",
                schema: "GameWorld",
                table: "BankCardTransactionEntity",
                type: "uuid",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));

            migrationBuilder.AddColumn<Guid>(
                name: "SenderId",
                schema: "GameWorld",
                table: "BankCardTransactionEntity",
                type: "uuid",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));

            migrationBuilder.CreateIndex(
                name: "IX_BankCardTransactionEntity_RecipientId",
                schema: "GameWorld",
                table: "BankCardTransactionEntity",
                column: "RecipientId");

            migrationBuilder.CreateIndex(
                name: "IX_BankCardTransactionEntity_SenderId",
                schema: "GameWorld",
                table: "BankCardTransactionEntity",
                column: "SenderId");

            migrationBuilder.AddForeignKey(
                name: "FK_BankCardTransactionEntity_BankCardAccountEntity_RecipientId",
                schema: "GameWorld",
                table: "BankCardTransactionEntity",
                column: "RecipientId",
                principalSchema: "GameWorld",
                principalTable: "BankCardAccountEntity",
                principalColumn: "EntityId",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_BankCardTransactionEntity_BankCardAccountEntity_SenderId",
                schema: "GameWorld",
                table: "BankCardTransactionEntity",
                column: "SenderId",
                principalSchema: "GameWorld",
                principalTable: "BankCardAccountEntity",
                principalColumn: "EntityId",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
