﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace SunCity.Game.World.DataBase.Migrations
{
    public partial class AddedGasStationPoints : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_PropertyEntity_InteriorEntity_InteriorId",
                schema: "GameWorld",
                table: "PropertyEntity");

            migrationBuilder.AlterColumn<Guid>(
                name: "InteriorId",
                schema: "GameWorld",
                table: "PropertyEntity",
                nullable: true,
                oldClrType: typeof(Guid),
                oldType: "uuid");

            migrationBuilder.AddColumn<long>(
                name: "Thirst",
                schema: "GameWorld",
                table: "PlayerCharacterEntity",
                nullable: false,
                defaultValue: 0L);

            migrationBuilder.CreateTable(
                name: "GasStationPointEntity",
                schema: "GameWorld",
                columns: table => new
                {
                    EntityId = table.Column<Guid>(nullable: false),
                    GasStationPropertyId = table.Column<Guid>(nullable: false),
                    Position_X = table.Column<float>(nullable: true),
                    Position_Y = table.Column<float>(nullable: true),
                    Position_Z = table.Column<float>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_GasStationPointEntity", x => x.EntityId);
                    table.ForeignKey(
                        name: "FK_GasStationPointEntity_GasStationPropertyEntity_GasStationPr~",
                        column: x => x.GasStationPropertyId,
                        principalSchema: "GameWorld",
                        principalTable: "GasStationPropertyEntity",
                        principalColumn: "EntityId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_GasStationPointEntity_GasStationPropertyId",
                schema: "GameWorld",
                table: "GasStationPointEntity",
                column: "GasStationPropertyId");

            migrationBuilder.AddForeignKey(
                name: "FK_PropertyEntity_InteriorEntity_InteriorId",
                schema: "GameWorld",
                table: "PropertyEntity",
                column: "InteriorId",
                principalSchema: "GameWorld",
                principalTable: "InteriorEntity",
                principalColumn: "EntityId",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_PropertyEntity_InteriorEntity_InteriorId",
                schema: "GameWorld",
                table: "PropertyEntity");

            migrationBuilder.DropTable(
                name: "GasStationPointEntity",
                schema: "GameWorld");

            migrationBuilder.DropColumn(
                name: "Thirst",
                schema: "GameWorld",
                table: "PlayerCharacterEntity");

            migrationBuilder.AlterColumn<Guid>(
                name: "InteriorId",
                schema: "GameWorld",
                table: "PropertyEntity",
                type: "uuid",
                nullable: false,
                oldClrType: typeof(Guid),
                oldNullable: true);

            migrationBuilder.AddForeignKey(
                name: "FK_PropertyEntity_InteriorEntity_InteriorId",
                schema: "GameWorld",
                table: "PropertyEntity",
                column: "InteriorId",
                principalSchema: "GameWorld",
                principalTable: "InteriorEntity",
                principalColumn: "EntityId",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
