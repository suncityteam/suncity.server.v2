﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace SunCity.Game.World.DataBase.Migrations
{
    public partial class AddedDeletedFlagForVehicles : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<bool>(
                name: "Deleted",
                schema: "GameWorld",
                table: "VehicleEntity",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<DateTime>(
                name: "DeletedDate",
                schema: "GameWorld",
                table: "VehicleEntity",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Deleted",
                schema: "GameWorld",
                table: "VehicleEntity");

            migrationBuilder.DropColumn(
                name: "DeletedDate",
                schema: "GameWorld",
                table: "VehicleEntity");
        }
    }
}
