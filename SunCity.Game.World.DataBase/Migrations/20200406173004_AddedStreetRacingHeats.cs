﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace SunCity.Game.World.DataBase.Migrations
{
    public partial class AddedStreetRacingHeats : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<Guid>(
                name: "StartPointId",
                schema: "GameWorld",
                table: "StreetRacingRouteEntity",
                nullable: true);

            migrationBuilder.AddColumn<Guid>(
                name: "StartPointId",
                schema: "GameWorld",
                table: "BusStationRouteEntity",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "StreetRacingHeatMemberEntity",
                schema: "GameWorld",
                columns: table => new
                {
                    EntityId = table.Column<Guid>(nullable: false),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    ExpiredDate = table.Column<DateTime>(nullable: false),
                    HeatId = table.Column<Guid>(nullable: false),
                    CharacterId = table.Column<Guid>(nullable: false),
                    VehicleId = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_StreetRacingHeatMemberEntity", x => x.EntityId);
                    table.ForeignKey(
                        name: "FK_StreetRacingHeatMemberEntity_PlayerCharacterEntity_Characte~",
                        column: x => x.CharacterId,
                        principalSchema: "GameWorld",
                        principalTable: "PlayerCharacterEntity",
                        principalColumn: "EntityId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_StreetRacingHeatMemberEntity_VehicleEntity_VehicleId",
                        column: x => x.VehicleId,
                        principalSchema: "GameWorld",
                        principalTable: "VehicleEntity",
                        principalColumn: "EntityId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "StreetRacingHeatEntity",
                schema: "GameWorld",
                columns: table => new
                {
                    EntityId = table.Column<Guid>(nullable: false),
                    Status = table.Column<byte>(nullable: false),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    ExpiredDate = table.Column<DateTime>(nullable: false),
                    WinnerId = table.Column<Guid>(nullable: true),
                    RouteId = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_StreetRacingHeatEntity", x => x.EntityId);
                    table.ForeignKey(
                        name: "FK_StreetRacingHeatEntity_StreetRacingRouteEntity_RouteId",
                        column: x => x.RouteId,
                        principalSchema: "GameWorld",
                        principalTable: "StreetRacingRouteEntity",
                        principalColumn: "EntityId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_StreetRacingHeatEntity_StreetRacingHeatMemberEntity_WinnerId",
                        column: x => x.WinnerId,
                        principalSchema: "GameWorld",
                        principalTable: "StreetRacingHeatMemberEntity",
                        principalColumn: "EntityId",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_StreetRacingRouteEntity_StartPointId",
                schema: "GameWorld",
                table: "StreetRacingRouteEntity",
                column: "StartPointId");

            migrationBuilder.CreateIndex(
                name: "IX_BusStationRouteEntity_StartPointId",
                schema: "GameWorld",
                table: "BusStationRouteEntity",
                column: "StartPointId");

            migrationBuilder.CreateIndex(
                name: "IX_StreetRacingHeatEntity_RouteId",
                schema: "GameWorld",
                table: "StreetRacingHeatEntity",
                column: "RouteId");

            migrationBuilder.CreateIndex(
                name: "IX_StreetRacingHeatEntity_WinnerId",
                schema: "GameWorld",
                table: "StreetRacingHeatEntity",
                column: "WinnerId");

            migrationBuilder.CreateIndex(
                name: "IX_StreetRacingHeatMemberEntity_CharacterId",
                schema: "GameWorld",
                table: "StreetRacingHeatMemberEntity",
                column: "CharacterId");

            migrationBuilder.CreateIndex(
                name: "IX_StreetRacingHeatMemberEntity_HeatId",
                schema: "GameWorld",
                table: "StreetRacingHeatMemberEntity",
                column: "HeatId");

            migrationBuilder.CreateIndex(
                name: "IX_StreetRacingHeatMemberEntity_VehicleId",
                schema: "GameWorld",
                table: "StreetRacingHeatMemberEntity",
                column: "VehicleId");

            migrationBuilder.AddForeignKey(
                name: "FK_BusStationRouteEntity_BusStationRoutePointEntity_StartPoint~",
                schema: "GameWorld",
                table: "BusStationRouteEntity",
                column: "StartPointId",
                principalSchema: "GameWorld",
                principalTable: "BusStationRoutePointEntity",
                principalColumn: "EntityId",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_StreetRacingRouteEntity_StreetRacingRoutePointEntity_StartP~",
                schema: "GameWorld",
                table: "StreetRacingRouteEntity",
                column: "StartPointId",
                principalSchema: "GameWorld",
                principalTable: "StreetRacingRoutePointEntity",
                principalColumn: "EntityId",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_StreetRacingHeatMemberEntity_StreetRacingHeatEntity_HeatId",
                schema: "GameWorld",
                table: "StreetRacingHeatMemberEntity",
                column: "HeatId",
                principalSchema: "GameWorld",
                principalTable: "StreetRacingHeatEntity",
                principalColumn: "EntityId",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_BusStationRouteEntity_BusStationRoutePointEntity_StartPoint~",
                schema: "GameWorld",
                table: "BusStationRouteEntity");

            migrationBuilder.DropForeignKey(
                name: "FK_StreetRacingRouteEntity_StreetRacingRoutePointEntity_StartP~",
                schema: "GameWorld",
                table: "StreetRacingRouteEntity");

            migrationBuilder.DropForeignKey(
                name: "FK_StreetRacingHeatEntity_StreetRacingHeatMemberEntity_WinnerId",
                schema: "GameWorld",
                table: "StreetRacingHeatEntity");

            migrationBuilder.DropTable(
                name: "StreetRacingHeatMemberEntity",
                schema: "GameWorld");

            migrationBuilder.DropTable(
                name: "StreetRacingHeatEntity",
                schema: "GameWorld");

            migrationBuilder.DropIndex(
                name: "IX_StreetRacingRouteEntity_StartPointId",
                schema: "GameWorld",
                table: "StreetRacingRouteEntity");

            migrationBuilder.DropIndex(
                name: "IX_BusStationRouteEntity_StartPointId",
                schema: "GameWorld",
                table: "BusStationRouteEntity");

            migrationBuilder.DropColumn(
                name: "StartPointId",
                schema: "GameWorld",
                table: "StreetRacingRouteEntity");

            migrationBuilder.DropColumn(
                name: "StartPointId",
                schema: "GameWorld",
                table: "BusStationRouteEntity");
        }
    }
}
