﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace SunCity.Game.World.DataBase.Migrations
{
    public partial class AddedDrivingSchoolRoutes : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "DrivingSchoolRouteEntity",
                schema: "GameWorld",
                columns: table => new
                {
                    EntityId = table.Column<Guid>(nullable: false),
                    DrivingSchoolId = table.Column<Guid>(nullable: false),
                    Name = table.Column<string>(maxLength: 64, nullable: false),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    Deleted = table.Column<bool>(nullable: false),
                    DeletedDate = table.Column<DateTime>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DrivingSchoolRouteEntity", x => x.EntityId);
                    table.ForeignKey(
                        name: "FK_DrivingSchoolRouteEntity_DrivingSchoolPropertyEntity_Drivin~",
                        column: x => x.DrivingSchoolId,
                        principalSchema: "GameWorld",
                        principalTable: "DrivingSchoolPropertyEntity",
                        principalColumn: "EntityId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "DrivingSchoolRoutePointEntity",
                schema: "GameWorld",
                columns: table => new
                {
                    EntityId = table.Column<Guid>(nullable: false),
                    RouteId = table.Column<Guid>(nullable: false),
                    Position_X = table.Column<float>(nullable: true),
                    Position_Y = table.Column<float>(nullable: true),
                    Position_Z = table.Column<float>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DrivingSchoolRoutePointEntity", x => x.EntityId);
                    table.ForeignKey(
                        name: "FK_DrivingSchoolRoutePointEntity_DrivingSchoolRouteEntity_Rout~",
                        column: x => x.RouteId,
                        principalSchema: "GameWorld",
                        principalTable: "DrivingSchoolRouteEntity",
                        principalColumn: "EntityId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_DrivingSchoolRouteEntity_DrivingSchoolId",
                schema: "GameWorld",
                table: "DrivingSchoolRouteEntity",
                column: "DrivingSchoolId");

            migrationBuilder.CreateIndex(
                name: "IX_DrivingSchoolRoutePointEntity_RouteId",
                schema: "GameWorld",
                table: "DrivingSchoolRoutePointEntity",
                column: "RouteId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "DrivingSchoolRoutePointEntity",
                schema: "GameWorld");

            migrationBuilder.DropTable(
                name: "DrivingSchoolRouteEntity",
                schema: "GameWorld");
        }
    }
}
