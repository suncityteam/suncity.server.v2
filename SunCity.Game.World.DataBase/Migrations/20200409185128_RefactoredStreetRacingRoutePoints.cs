﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace SunCity.Game.World.DataBase.Migrations
{
    public partial class RefactoredStreetRacingRoutePoints : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_StreetRacingRoutePointEntity_StationPointEntity_StationPoin~",
                schema: "GameWorld",
                table: "StreetRacingRoutePointEntity");

            migrationBuilder.DropIndex(
                name: "IX_StreetRacingRoutePointEntity_StationPointEntityEntityId",
                schema: "GameWorld",
                table: "StreetRacingRoutePointEntity");

            migrationBuilder.DropColumn(
                name: "StationPointEntityEntityId",
                schema: "GameWorld",
                table: "StreetRacingRoutePointEntity");

            migrationBuilder.DropColumn(
                name: "StationPointId",
                schema: "GameWorld",
                table: "StreetRacingRoutePointEntity");

            migrationBuilder.AddColumn<string>(
                name: "AdminDescription",
                schema: "GameWorld",
                table: "StreetRacingRoutePointEntity",
                maxLength: 256,
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "AdminDescription",
                schema: "GameWorld",
                table: "StreetRacingRoutePointEntity");

            migrationBuilder.AddColumn<Guid>(
                name: "StationPointEntityEntityId",
                schema: "GameWorld",
                table: "StreetRacingRoutePointEntity",
                type: "uuid",
                nullable: true);

            migrationBuilder.AddColumn<Guid>(
                name: "StationPointId",
                schema: "GameWorld",
                table: "StreetRacingRoutePointEntity",
                type: "uuid",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));

            migrationBuilder.CreateIndex(
                name: "IX_StreetRacingRoutePointEntity_StationPointEntityEntityId",
                schema: "GameWorld",
                table: "StreetRacingRoutePointEntity",
                column: "StationPointEntityEntityId");

            migrationBuilder.AddForeignKey(
                name: "FK_StreetRacingRoutePointEntity_StationPointEntity_StationPoin~",
                schema: "GameWorld",
                table: "StreetRacingRoutePointEntity",
                column: "StationPointEntityEntityId",
                principalSchema: "GameWorld",
                principalTable: "StationPointEntity",
                principalColumn: "EntityId",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
