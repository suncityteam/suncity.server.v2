﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace SunCity.Game.World.DataBase.Migrations
{
    public partial class AddedGameItemClothData : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "ClothSlot",
                schema: "GameWorld",
                table: "ClothingItemEntity",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<byte>(
                name: "Drawable",
                schema: "GameWorld",
                table: "ClothingItemEntity",
                nullable: false,
                defaultValue: (byte)0);

            migrationBuilder.AddColumn<byte>(
                name: "Palette",
                schema: "GameWorld",
                table: "ClothingItemEntity",
                nullable: false,
                defaultValue: (byte)0);

            migrationBuilder.AddColumn<byte>(
                name: "Texture",
                schema: "GameWorld",
                table: "ClothingItemEntity",
                nullable: false,
                defaultValue: (byte)0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "ClothSlot",
                schema: "GameWorld",
                table: "ClothingItemEntity");

            migrationBuilder.DropColumn(
                name: "Drawable",
                schema: "GameWorld",
                table: "ClothingItemEntity");

            migrationBuilder.DropColumn(
                name: "Palette",
                schema: "GameWorld",
                table: "ClothingItemEntity");

            migrationBuilder.DropColumn(
                name: "Texture",
                schema: "GameWorld",
                table: "ClothingItemEntity");
        }
    }
}
