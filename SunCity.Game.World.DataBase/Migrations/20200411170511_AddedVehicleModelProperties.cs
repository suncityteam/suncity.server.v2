﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace SunCity.Game.World.DataBase.Migrations
{
    public partial class AddedVehicleModelProperties : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<float>(
                name: "Acceleration",
                schema: "GameWorld",
                table: "VehicleModelEntity",
                nullable: false,
                defaultValue: 0f);

            migrationBuilder.AddColumn<short>(
                name: "BagCapacity",
                schema: "GameWorld",
                table: "VehicleModelEntity",
                nullable: false,
                defaultValue: (short)0);

            migrationBuilder.AddColumn<float>(
                name: "FuelConsumptionPerMinute",
                schema: "GameWorld",
                table: "VehicleModelEntity",
                nullable: false,
                defaultValue: 0f);

            migrationBuilder.AddColumn<byte>(
                name: "Handleability",
                schema: "GameWorld",
                table: "VehicleModelEntity",
                nullable: false,
                defaultValue: (byte)0);

            migrationBuilder.AddColumn<short>(
                name: "MaxSpeed",
                schema: "GameWorld",
                table: "VehicleModelEntity",
                nullable: false,
                defaultValue: (short)0);

            migrationBuilder.AddColumn<byte>(
                name: "NumberOfSeats",
                schema: "GameWorld",
                table: "VehicleModelEntity",
                nullable: false,
                defaultValue: (byte)0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Acceleration",
                schema: "GameWorld",
                table: "VehicleModelEntity");

            migrationBuilder.DropColumn(
                name: "BagCapacity",
                schema: "GameWorld",
                table: "VehicleModelEntity");

            migrationBuilder.DropColumn(
                name: "FuelConsumptionPerMinute",
                schema: "GameWorld",
                table: "VehicleModelEntity");

            migrationBuilder.DropColumn(
                name: "Handleability",
                schema: "GameWorld",
                table: "VehicleModelEntity");

            migrationBuilder.DropColumn(
                name: "MaxSpeed",
                schema: "GameWorld",
                table: "VehicleModelEntity");

            migrationBuilder.DropColumn(
                name: "NumberOfSeats",
                schema: "GameWorld",
                table: "VehicleModelEntity");
        }
    }
}
