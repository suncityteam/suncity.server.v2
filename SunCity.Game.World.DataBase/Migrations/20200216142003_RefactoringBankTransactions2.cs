﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace SunCity.Game.World.DataBase.Migrations
{
    public partial class RefactoringBankTransactions2 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_BankCardDepositTransactionEntity_BankCardAccountEntity_Reci~",
                schema: "GameWorld",
                table: "BankCardDepositTransactionEntity");

            migrationBuilder.DropForeignKey(
                name: "FK_BankCardWithdrawTransactionEntity_BankCardAccountEntity_Sen~",
                schema: "GameWorld",
                table: "BankCardWithdrawTransactionEntity");

            migrationBuilder.DropTable(
                name: "BankCardTransferTransactionEntity",
                schema: "GameWorld");

            migrationBuilder.DropIndex(
                name: "IX_BankCardWithdrawTransactionEntity_SenderId",
                schema: "GameWorld",
                table: "BankCardWithdrawTransactionEntity");

            migrationBuilder.DropIndex(
                name: "IX_BankCardDepositTransactionEntity_RecipientId",
                schema: "GameWorld",
                table: "BankCardDepositTransactionEntity");

            migrationBuilder.DropColumn(
                name: "SenderId",
                schema: "GameWorld",
                table: "BankCardWithdrawTransactionEntity");

            migrationBuilder.DropColumn(
                name: "RecipientId",
                schema: "GameWorld",
                table: "BankCardDepositTransactionEntity");

            migrationBuilder.AddColumn<Guid>(
                name: "BankCardId",
                schema: "GameWorld",
                table: "BankCardTransactionEntity",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));

            migrationBuilder.AddColumn<int>(
                name: "Category",
                schema: "GameWorld",
                table: "BankCardTransactionEntity",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateTable(
                name: "BankCardTransferFromTransactionEntity",
                schema: "GameWorld",
                columns: table => new
                {
                    EntityId = table.Column<Guid>(nullable: false),
                    RecipientId = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BankCardTransferFromTransactionEntity", x => x.EntityId);
                    table.ForeignKey(
                        name: "FK_BankCardTransferFromTransactionEntity_BankCardTransactionEn~",
                        column: x => x.EntityId,
                        principalSchema: "GameWorld",
                        principalTable: "BankCardTransactionEntity",
                        principalColumn: "EntityId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_BankCardTransferFromTransactionEntity_BankCardAccountEntity~",
                        column: x => x.RecipientId,
                        principalSchema: "GameWorld",
                        principalTable: "BankCardAccountEntity",
                        principalColumn: "EntityId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "BankCardTransferToTransactionEntity",
                schema: "GameWorld",
                columns: table => new
                {
                    EntityId = table.Column<Guid>(nullable: false),
                    PayerId = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BankCardTransferToTransactionEntity", x => x.EntityId);
                    table.ForeignKey(
                        name: "FK_BankCardTransferToTransactionEntity_BankCardTransactionEnti~",
                        column: x => x.EntityId,
                        principalSchema: "GameWorld",
                        principalTable: "BankCardTransactionEntity",
                        principalColumn: "EntityId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_BankCardTransferToTransactionEntity_BankCardAccountEntity_P~",
                        column: x => x.PayerId,
                        principalSchema: "GameWorld",
                        principalTable: "BankCardAccountEntity",
                        principalColumn: "EntityId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_BankCardTransactionEntity_BankCardId",
                schema: "GameWorld",
                table: "BankCardTransactionEntity",
                column: "BankCardId");

            migrationBuilder.CreateIndex(
                name: "IX_BankCardTransferFromTransactionEntity_RecipientId",
                schema: "GameWorld",
                table: "BankCardTransferFromTransactionEntity",
                column: "RecipientId");

            migrationBuilder.CreateIndex(
                name: "IX_BankCardTransferToTransactionEntity_PayerId",
                schema: "GameWorld",
                table: "BankCardTransferToTransactionEntity",
                column: "PayerId");

            migrationBuilder.AddForeignKey(
                name: "FK_BankCardTransactionEntity_BankCardAccountEntity_BankCardId",
                schema: "GameWorld",
                table: "BankCardTransactionEntity",
                column: "BankCardId",
                principalSchema: "GameWorld",
                principalTable: "BankCardAccountEntity",
                principalColumn: "EntityId",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_BankCardTransactionEntity_BankCardAccountEntity_BankCardId",
                schema: "GameWorld",
                table: "BankCardTransactionEntity");

            migrationBuilder.DropTable(
                name: "BankCardTransferFromTransactionEntity",
                schema: "GameWorld");

            migrationBuilder.DropTable(
                name: "BankCardTransferToTransactionEntity",
                schema: "GameWorld");

            migrationBuilder.DropIndex(
                name: "IX_BankCardTransactionEntity_BankCardId",
                schema: "GameWorld",
                table: "BankCardTransactionEntity");

            migrationBuilder.DropColumn(
                name: "BankCardId",
                schema: "GameWorld",
                table: "BankCardTransactionEntity");

            migrationBuilder.DropColumn(
                name: "Category",
                schema: "GameWorld",
                table: "BankCardTransactionEntity");

            migrationBuilder.AddColumn<Guid>(
                name: "SenderId",
                schema: "GameWorld",
                table: "BankCardWithdrawTransactionEntity",
                type: "uuid",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));

            migrationBuilder.AddColumn<Guid>(
                name: "RecipientId",
                schema: "GameWorld",
                table: "BankCardDepositTransactionEntity",
                type: "uuid",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));

            migrationBuilder.CreateTable(
                name: "BankCardTransferTransactionEntity",
                schema: "GameWorld",
                columns: table => new
                {
                    EntityId = table.Column<Guid>(type: "uuid", nullable: false),
                    RecipientId = table.Column<Guid>(type: "uuid", nullable: false),
                    SenderId = table.Column<Guid>(type: "uuid", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BankCardTransferTransactionEntity", x => x.EntityId);
                    table.ForeignKey(
                        name: "FK_BankCardTransferTransactionEntity_BankCardTransactionEntity~",
                        column: x => x.EntityId,
                        principalSchema: "GameWorld",
                        principalTable: "BankCardTransactionEntity",
                        principalColumn: "EntityId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_BankCardTransferTransactionEntity_BankCardAccountEntity_Rec~",
                        column: x => x.RecipientId,
                        principalSchema: "GameWorld",
                        principalTable: "BankCardAccountEntity",
                        principalColumn: "EntityId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_BankCardTransferTransactionEntity_BankCardAccountEntity_Sen~",
                        column: x => x.SenderId,
                        principalSchema: "GameWorld",
                        principalTable: "BankCardAccountEntity",
                        principalColumn: "EntityId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_BankCardWithdrawTransactionEntity_SenderId",
                schema: "GameWorld",
                table: "BankCardWithdrawTransactionEntity",
                column: "SenderId");

            migrationBuilder.CreateIndex(
                name: "IX_BankCardDepositTransactionEntity_RecipientId",
                schema: "GameWorld",
                table: "BankCardDepositTransactionEntity",
                column: "RecipientId");

            migrationBuilder.CreateIndex(
                name: "IX_BankCardTransferTransactionEntity_RecipientId",
                schema: "GameWorld",
                table: "BankCardTransferTransactionEntity",
                column: "RecipientId");

            migrationBuilder.CreateIndex(
                name: "IX_BankCardTransferTransactionEntity_SenderId",
                schema: "GameWorld",
                table: "BankCardTransferTransactionEntity",
                column: "SenderId");

            migrationBuilder.AddForeignKey(
                name: "FK_BankCardDepositTransactionEntity_BankCardAccountEntity_Reci~",
                schema: "GameWorld",
                table: "BankCardDepositTransactionEntity",
                column: "RecipientId",
                principalSchema: "GameWorld",
                principalTable: "BankCardAccountEntity",
                principalColumn: "EntityId",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_BankCardWithdrawTransactionEntity_BankCardAccountEntity_Sen~",
                schema: "GameWorld",
                table: "BankCardWithdrawTransactionEntity",
                column: "SenderId",
                principalSchema: "GameWorld",
                principalTable: "BankCardAccountEntity",
                principalColumn: "EntityId",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
