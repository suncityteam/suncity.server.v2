﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;
using SunCity.Game.World.DataBase.Entitites.Item.World;

namespace SunCity.Game.World.DataBase.Migrations
{
    public partial class AddedWorldItemEntity : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_PlayerCharacterInventoryItemEntity_ItemEntity_ItemId",
                schema: "GameWorld",
                table: "PlayerCharacterInventoryItemEntity");

            migrationBuilder.CreateTable(
                name: "WorldItemEntity",
                schema: "GameWorld",
                columns: table => new
                {
                    EntityId = table.Column<Guid>(nullable: false),
                    Group = table.Column<int>(nullable: false),
                    ModelId = table.Column<Guid>(nullable: false),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    ParentId = table.Column<Guid>(nullable: true),
                    Data = table.Column<WorldItemData>(type: "jsonb", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_WorldItemEntity", x => x.EntityId);
                    table.ForeignKey(
                        name: "FK_WorldItemEntity_ItemEntity_ModelId",
                        column: x => x.ModelId,
                        principalSchema: "GameWorld",
                        principalTable: "ItemEntity",
                        principalColumn: "EntityId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_WorldItemEntity_WorldItemEntity_ParentId",
                        column: x => x.ParentId,
                        principalSchema: "GameWorld",
                        principalTable: "WorldItemEntity",
                        principalColumn: "EntityId",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_WorldItemEntity_ModelId",
                schema: "GameWorld",
                table: "WorldItemEntity",
                column: "ModelId");

            migrationBuilder.CreateIndex(
                name: "IX_WorldItemEntity_ParentId",
                schema: "GameWorld",
                table: "WorldItemEntity",
                column: "ParentId");

            migrationBuilder.AddForeignKey(
                name: "FK_PlayerCharacterInventoryItemEntity_WorldItemEntity_ItemId",
                schema: "GameWorld",
                table: "PlayerCharacterInventoryItemEntity",
                column: "ItemId",
                principalSchema: "GameWorld",
                principalTable: "WorldItemEntity",
                principalColumn: "EntityId",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_PlayerCharacterInventoryItemEntity_WorldItemEntity_ItemId",
                schema: "GameWorld",
                table: "PlayerCharacterInventoryItemEntity");

            migrationBuilder.DropTable(
                name: "WorldItemEntity",
                schema: "GameWorld");

            migrationBuilder.AddForeignKey(
                name: "FK_PlayerCharacterInventoryItemEntity_ItemEntity_ItemId",
                schema: "GameWorld",
                table: "PlayerCharacterInventoryItemEntity",
                column: "ItemId",
                principalSchema: "GameWorld",
                principalTable: "ItemEntity",
                principalColumn: "EntityId",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
