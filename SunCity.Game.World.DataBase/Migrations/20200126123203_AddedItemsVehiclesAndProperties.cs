﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace SunCity.Game.World.DataBase.Migrations
{
    public partial class AddedItemsVehiclesAndProperties : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<long>(
                name: "MaxCost_Amount",
                schema: "GameWorld",
                table: "VehicleModelEntity",
                nullable: true);

            migrationBuilder.AddColumn<byte>(
                name: "MaxCost_Currency",
                schema: "GameWorld",
                table: "VehicleModelEntity",
                nullable: true);

            migrationBuilder.AddColumn<long>(
                name: "MinCost_Amount",
                schema: "GameWorld",
                table: "VehicleModelEntity",
                nullable: true);

            migrationBuilder.AddColumn<byte>(
                name: "MinCost_Currency",
                schema: "GameWorld",
                table: "VehicleModelEntity",
                nullable: true);

            migrationBuilder.AddColumn<byte>(
                name: "Category",
                schema: "GameWorld",
                table: "HeadQuarterPropertyEntity",
                nullable: false,
                defaultValue: (byte)0);

            migrationBuilder.AddColumn<int>(
                name: "SalaryPerHour",
                schema: "GameWorld",
                table: "FractionRankEntity",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "SalaryPerWeek",
                schema: "GameWorld",
                table: "FractionRankEntity",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<TimeSpan>(
                name: "WeekAtivity",
                schema: "GameWorld",
                table: "FractionMemberEntity",
                nullable: false,
                defaultValue: new TimeSpan(0, 0, 0, 0, 0));

            migrationBuilder.AddColumn<byte>(
                name: "Category",
                schema: "GameWorld",
                table: "FractionEntity",
                nullable: false,
                defaultValue: (byte)0);

            migrationBuilder.CreateTable(
                name: "CarShowroomVehicleEntity",
                schema: "GameWorld",
                columns: table => new
                {
                    EntityId = table.Column<Guid>(nullable: false),
                    CarShowroomId = table.Column<Guid>(nullable: false),
                    ModelId = table.Column<long>(nullable: false),
                    Cost_Amount = table.Column<long>(nullable: true),
                    Cost_Currency = table.Column<byte>(nullable: true),
                    Amount = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CarShowroomVehicleEntity", x => x.EntityId);
                    table.ForeignKey(
                        name: "FK_CarShowroomVehicleEntity_CarShowroomPropertyEntity_CarShowr~",
                        column: x => x.CarShowroomId,
                        principalSchema: "GameWorld",
                        principalTable: "CarShowroomPropertyEntity",
                        principalColumn: "EntityId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_CarShowroomVehicleEntity_VehicleModelEntity_ModelId",
                        column: x => x.ModelId,
                        principalSchema: "GameWorld",
                        principalTable: "VehicleModelEntity",
                        principalColumn: "EntityId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "CarShowroomVehicleSpawnPointEntity",
                schema: "GameWorld",
                columns: table => new
                {
                    EntityId = table.Column<Guid>(nullable: false),
                    CarShowroomId = table.Column<Guid>(nullable: false),
                    Position_X = table.Column<float>(nullable: true),
                    Position_Y = table.Column<float>(nullable: true),
                    Position_Z = table.Column<float>(nullable: true),
                    Rotation = table.Column<float>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CarShowroomVehicleSpawnPointEntity", x => x.EntityId);
                    table.ForeignKey(
                        name: "FK_CarShowroomVehicleSpawnPointEntity_CarShowroomPropertyEntit~",
                        column: x => x.CarShowroomId,
                        principalSchema: "GameWorld",
                        principalTable: "CarShowroomPropertyEntity",
                        principalColumn: "EntityId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "ItemEntity",
                schema: "GameWorld",
                columns: table => new
                {
                    EntityId = table.Column<Guid>(nullable: false),
                    Name = table.Column<string>(maxLength: 32, nullable: false),
                    ModelHash = table.Column<long>(nullable: true),
                    ItemCategory = table.Column<byte>(nullable: false),
                    Weight = table.Column<int>(nullable: false),
                    Capacity = table.Column<int>(nullable: false),
                    Stack = table.Column<short>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ItemEntity", x => x.EntityId);
                });

            migrationBuilder.CreateTable(
                name: "ClothingItemEntity",
                schema: "GameWorld",
                columns: table => new
                {
                    EntityId = table.Column<Guid>(nullable: false),
                    Slot = table.Column<byte>(nullable: false),
                    Temperature = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ClothingItemEntity", x => x.EntityId);
                    table.ForeignKey(
                        name: "FK_ClothingItemEntity_ItemEntity_EntityId",
                        column: x => x.EntityId,
                        principalSchema: "GameWorld",
                        principalTable: "ItemEntity",
                        principalColumn: "EntityId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "DrinkItemEntity",
                schema: "GameWorld",
                columns: table => new
                {
                    EntityId = table.Column<Guid>(nullable: false),
                    Alcohol = table.Column<int>(nullable: false),
                    Addiction = table.Column<int>(nullable: false),
                    Thirst = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DrinkItemEntity", x => x.EntityId);
                    table.ForeignKey(
                        name: "FK_DrinkItemEntity_ItemEntity_EntityId",
                        column: x => x.EntityId,
                        principalSchema: "GameWorld",
                        principalTable: "ItemEntity",
                        principalColumn: "EntityId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "DrugItemEntity",
                schema: "GameWorld",
                columns: table => new
                {
                    EntityId = table.Column<Guid>(nullable: false),
                    Alcohol = table.Column<int>(nullable: false),
                    Satiety = table.Column<int>(nullable: false),
                    Addiction = table.Column<int>(nullable: false),
                    HealthRegenerationAmount = table.Column<int>(nullable: false),
                    HealthRegenerationTime = table.Column<TimeSpan>(nullable: false),
                    HealthRegenerationInterval = table.Column<TimeSpan>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DrugItemEntity", x => x.EntityId);
                    table.ForeignKey(
                        name: "FK_DrugItemEntity_ItemEntity_EntityId",
                        column: x => x.EntityId,
                        principalSchema: "GameWorld",
                        principalTable: "ItemEntity",
                        principalColumn: "EntityId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "FoodItemEntity",
                schema: "GameWorld",
                columns: table => new
                {
                    EntityId = table.Column<Guid>(nullable: false),
                    Satiety = table.Column<int>(nullable: false),
                    ExpirationDate = table.Column<TimeSpan>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_FoodItemEntity", x => x.EntityId);
                    table.ForeignKey(
                        name: "FK_FoodItemEntity_ItemEntity_EntityId",
                        column: x => x.EntityId,
                        principalSchema: "GameWorld",
                        principalTable: "ItemEntity",
                        principalColumn: "EntityId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "PlayerCharacterInventoryItemEntity",
                schema: "GameWorld",
                columns: table => new
                {
                    EntityId = table.Column<Guid>(nullable: false),
                    InventorySlot = table.Column<byte>(nullable: false),
                    CharacterId = table.Column<Guid>(nullable: false),
                    ItemId = table.Column<Guid>(nullable: false),
                    Amount = table.Column<short>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PlayerCharacterInventoryItemEntity", x => x.EntityId);
                    table.ForeignKey(
                        name: "FK_PlayerCharacterInventoryItemEntity_PlayerCharacterEntity_Ch~",
                        column: x => x.CharacterId,
                        principalSchema: "GameWorld",
                        principalTable: "PlayerCharacterEntity",
                        principalColumn: "EntityId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_PlayerCharacterInventoryItemEntity_ItemEntity_ItemId",
                        column: x => x.ItemId,
                        principalSchema: "GameWorld",
                        principalTable: "ItemEntity",
                        principalColumn: "EntityId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "ResourceItemEntity",
                schema: "GameWorld",
                columns: table => new
                {
                    EntityId = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ResourceItemEntity", x => x.EntityId);
                    table.ForeignKey(
                        name: "FK_ResourceItemEntity_ItemEntity_EntityId",
                        column: x => x.EntityId,
                        principalSchema: "GameWorld",
                        principalTable: "ItemEntity",
                        principalColumn: "EntityId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_PlayerCharacterEntity_EnteredPropertyId",
                schema: "GameWorld",
                table: "PlayerCharacterEntity",
                column: "EnteredPropertyId");

            migrationBuilder.CreateIndex(
                name: "IX_CarShowroomVehicleEntity_CarShowroomId",
                schema: "GameWorld",
                table: "CarShowroomVehicleEntity",
                column: "CarShowroomId");

            migrationBuilder.CreateIndex(
                name: "IX_CarShowroomVehicleEntity_ModelId",
                schema: "GameWorld",
                table: "CarShowroomVehicleEntity",
                column: "ModelId");

            migrationBuilder.CreateIndex(
                name: "IX_CarShowroomVehicleSpawnPointEntity_CarShowroomId",
                schema: "GameWorld",
                table: "CarShowroomVehicleSpawnPointEntity",
                column: "CarShowroomId");

            migrationBuilder.CreateIndex(
                name: "IX_PlayerCharacterInventoryItemEntity_CharacterId",
                schema: "GameWorld",
                table: "PlayerCharacterInventoryItemEntity",
                column: "CharacterId");

            migrationBuilder.CreateIndex(
                name: "IX_PlayerCharacterInventoryItemEntity_ItemId",
                schema: "GameWorld",
                table: "PlayerCharacterInventoryItemEntity",
                column: "ItemId");

            migrationBuilder.AddForeignKey(
                name: "FK_PlayerCharacterEntity_PropertyEntity_EnteredPropertyId",
                schema: "GameWorld",
                table: "PlayerCharacterEntity",
                column: "EnteredPropertyId",
                principalSchema: "GameWorld",
                principalTable: "PropertyEntity",
                principalColumn: "EntityId",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_PlayerCharacterEntity_PropertyEntity_EnteredPropertyId",
                schema: "GameWorld",
                table: "PlayerCharacterEntity");

            migrationBuilder.DropTable(
                name: "CarShowroomVehicleEntity",
                schema: "GameWorld");

            migrationBuilder.DropTable(
                name: "CarShowroomVehicleSpawnPointEntity",
                schema: "GameWorld");

            migrationBuilder.DropTable(
                name: "ClothingItemEntity",
                schema: "GameWorld");

            migrationBuilder.DropTable(
                name: "DrinkItemEntity",
                schema: "GameWorld");

            migrationBuilder.DropTable(
                name: "DrugItemEntity",
                schema: "GameWorld");

            migrationBuilder.DropTable(
                name: "FoodItemEntity",
                schema: "GameWorld");

            migrationBuilder.DropTable(
                name: "PlayerCharacterInventoryItemEntity",
                schema: "GameWorld");

            migrationBuilder.DropTable(
                name: "ResourceItemEntity",
                schema: "GameWorld");

            migrationBuilder.DropTable(
                name: "ItemEntity",
                schema: "GameWorld");

            migrationBuilder.DropIndex(
                name: "IX_PlayerCharacterEntity_EnteredPropertyId",
                schema: "GameWorld",
                table: "PlayerCharacterEntity");

            migrationBuilder.DropColumn(
                name: "MaxCost_Amount",
                schema: "GameWorld",
                table: "VehicleModelEntity");

            migrationBuilder.DropColumn(
                name: "MaxCost_Currency",
                schema: "GameWorld",
                table: "VehicleModelEntity");

            migrationBuilder.DropColumn(
                name: "MinCost_Amount",
                schema: "GameWorld",
                table: "VehicleModelEntity");

            migrationBuilder.DropColumn(
                name: "MinCost_Currency",
                schema: "GameWorld",
                table: "VehicleModelEntity");

            migrationBuilder.DropColumn(
                name: "Category",
                schema: "GameWorld",
                table: "HeadQuarterPropertyEntity");

            migrationBuilder.DropColumn(
                name: "SalaryPerHour",
                schema: "GameWorld",
                table: "FractionRankEntity");

            migrationBuilder.DropColumn(
                name: "SalaryPerWeek",
                schema: "GameWorld",
                table: "FractionRankEntity");

            migrationBuilder.DropColumn(
                name: "WeekAtivity",
                schema: "GameWorld",
                table: "FractionMemberEntity");

            migrationBuilder.DropColumn(
                name: "Category",
                schema: "GameWorld",
                table: "FractionEntity");
        }
    }
}
