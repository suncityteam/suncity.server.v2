﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace SunCity.Game.World.DataBase.Migrations
{
    public partial class AddedPlayerLicenseEntity : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "PlayerLicenseEntity",
                schema: "GameWorld",
                columns: table => new
                {
                    EntityId = table.Column<Guid>(nullable: false),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    ExpiredDate = table.Column<DateTime>(nullable: false),
                    CharacterId = table.Column<Guid>(nullable: false),
                    LicenseId = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PlayerLicenseEntity", x => x.EntityId);
                    table.ForeignKey(
                        name: "FK_PlayerLicenseEntity_PlayerCharacterEntity_CharacterId",
                        column: x => x.CharacterId,
                        principalSchema: "GameWorld",
                        principalTable: "PlayerCharacterEntity",
                        principalColumn: "EntityId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_PlayerLicenseEntity_GameLicenseEntity_LicenseId",
                        column: x => x.LicenseId,
                        principalSchema: "GameWorld",
                        principalTable: "GameLicenseEntity",
                        principalColumn: "EntityId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_PlayerLicenseEntity_CharacterId",
                schema: "GameWorld",
                table: "PlayerLicenseEntity",
                column: "CharacterId");

            migrationBuilder.CreateIndex(
                name: "IX_PlayerLicenseEntity_LicenseId",
                schema: "GameWorld",
                table: "PlayerLicenseEntity",
                column: "LicenseId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "PlayerLicenseEntity",
                schema: "GameWorld");
        }
    }
}
