﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace SunCity.Game.World.DataBase.Migrations
{
    public partial class BusStationRouteImprovements : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<TimeSpan>(
                name: "MaxDelay",
                schema: "GameWorld",
                table: "BusStationRoutePointEntity",
                nullable: false,
                defaultValue: new TimeSpan(0, 0, 0, 0, 0));

            migrationBuilder.AddColumn<TimeSpan>(
                name: "WaitingTime",
                schema: "GameWorld",
                table: "BusStationRoutePointEntity",
                nullable: false,
                defaultValue: new TimeSpan(0, 0, 0, 0, 0));

            migrationBuilder.AddColumn<long>(
                name: "Bonus_Amount",
                schema: "GameWorld",
                table: "BusStationRouteEntity",
                nullable: true);

            migrationBuilder.AddColumn<byte>(
                name: "Bonus_Currency",
                schema: "GameWorld",
                table: "BusStationRouteEntity",
                nullable: true);

            migrationBuilder.AddColumn<long>(
                name: "Reward_Amount",
                schema: "GameWorld",
                table: "BusStationRouteEntity",
                nullable: true);

            migrationBuilder.AddColumn<byte>(
                name: "Reward_Currency",
                schema: "GameWorld",
                table: "BusStationRouteEntity",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "MaxDelay",
                schema: "GameWorld",
                table: "BusStationRoutePointEntity");

            migrationBuilder.DropColumn(
                name: "WaitingTime",
                schema: "GameWorld",
                table: "BusStationRoutePointEntity");

            migrationBuilder.DropColumn(
                name: "Bonus_Amount",
                schema: "GameWorld",
                table: "BusStationRouteEntity");

            migrationBuilder.DropColumn(
                name: "Bonus_Currency",
                schema: "GameWorld",
                table: "BusStationRouteEntity");

            migrationBuilder.DropColumn(
                name: "Reward_Amount",
                schema: "GameWorld",
                table: "BusStationRouteEntity");

            migrationBuilder.DropColumn(
                name: "Reward_Currency",
                schema: "GameWorld",
                table: "BusStationRouteEntity");
        }
    }
}
