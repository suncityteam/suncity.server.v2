﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace SunCity.Game.World.DataBase.Migrations
{
    public partial class AddedFractionsAndOwners : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "HeadQuarterPropertyEntity",
                schema: "GameWorld",
                columns: table => new
                {
                    EntityId = table.Column<Guid>(nullable: false),
                    Level = table.Column<int>(nullable: false),
                    Experience = table.Column<long>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_HeadQuarterPropertyEntity", x => x.EntityId);
                    table.ForeignKey(
                        name: "FK_HeadQuarterPropertyEntity_PropertyEntity_EntityId",
                        column: x => x.EntityId,
                        principalSchema: "GameWorld",
                        principalTable: "PropertyEntity",
                        principalColumn: "EntityId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "PropertyPlayerOwnerEntity",
                schema: "GameWorld",
                columns: table => new
                {
                    EntityId = table.Column<Guid>(nullable: false),
                    Type = table.Column<int>(nullable: false),
                    CharacterId = table.Column<Guid>(nullable: false),
                    PropertyId = table.Column<Guid>(nullable: false),
                    Begin = table.Column<DateTime>(nullable: false),
                    End = table.Column<DateTime>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PropertyPlayerOwnerEntity", x => x.EntityId);
                    table.ForeignKey(
                        name: "FK_PropertyPlayerOwnerEntity_PlayerCharacterEntity_CharacterId",
                        column: x => x.CharacterId,
                        principalSchema: "GameWorld",
                        principalTable: "PlayerCharacterEntity",
                        principalColumn: "EntityId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_PropertyPlayerOwnerEntity_PropertyEntity_PropertyId",
                        column: x => x.PropertyId,
                        principalSchema: "GameWorld",
                        principalTable: "PropertyEntity",
                        principalColumn: "EntityId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "FractionEntity",
                schema: "GameWorld",
                columns: table => new
                {
                    EntityId = table.Column<Guid>(nullable: false),
                    Type = table.Column<int>(nullable: false),
                    HeadQuarterPropertyId = table.Column<Guid>(nullable: false),
                    Name = table.Column<string>(maxLength: 64, nullable: false),
                    ShortName = table.Column<string>(maxLength: 16, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_FractionEntity", x => x.EntityId);
                    table.ForeignKey(
                        name: "FK_FractionEntity_HeadQuarterPropertyEntity_HeadQuarterPropert~",
                        column: x => x.HeadQuarterPropertyId,
                        principalSchema: "GameWorld",
                        principalTable: "HeadQuarterPropertyEntity",
                        principalColumn: "EntityId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "FractionRankEntity",
                schema: "GameWorld",
                columns: table => new
                {
                    EntityId = table.Column<Guid>(nullable: false),
                    Access = table.Column<int>(nullable: false),
                    Deleted = table.Column<bool>(nullable: false),
                    Name = table.Column<string>(maxLength: 64, nullable: false),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    FractionId = table.Column<Guid>(nullable: false),
                    FractionEntityEntityId = table.Column<Guid>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_FractionRankEntity", x => x.EntityId);
                    table.ForeignKey(
                        name: "FK_FractionRankEntity_FractionEntity_FractionEntityEntityId",
                        column: x => x.FractionEntityEntityId,
                        principalSchema: "GameWorld",
                        principalTable: "FractionEntity",
                        principalColumn: "EntityId",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "PropertyFractionOwnerEntity",
                schema: "GameWorld",
                columns: table => new
                {
                    EntityId = table.Column<Guid>(nullable: false),
                    Type = table.Column<int>(nullable: false),
                    FractionId = table.Column<Guid>(nullable: false),
                    PropertyId = table.Column<Guid>(nullable: false),
                    Begin = table.Column<DateTime>(nullable: false),
                    End = table.Column<DateTime>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PropertyFractionOwnerEntity", x => x.EntityId);
                    table.ForeignKey(
                        name: "FK_PropertyFractionOwnerEntity_FractionEntity_FractionId",
                        column: x => x.FractionId,
                        principalSchema: "GameWorld",
                        principalTable: "FractionEntity",
                        principalColumn: "EntityId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_PropertyFractionOwnerEntity_PropertyEntity_PropertyId",
                        column: x => x.PropertyId,
                        principalSchema: "GameWorld",
                        principalTable: "PropertyEntity",
                        principalColumn: "EntityId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "FractionMemberEntity",
                schema: "GameWorld",
                columns: table => new
                {
                    EntityId = table.Column<Guid>(nullable: false),
                    FractionId = table.Column<Guid>(nullable: false),
                    FractionRankId = table.Column<Guid>(nullable: false),
                    CharacterId = table.Column<Guid>(nullable: false),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    LeaveDate = table.Column<DateTime>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_FractionMemberEntity", x => x.EntityId);
                    table.ForeignKey(
                        name: "FK_FractionMemberEntity_PlayerCharacterEntity_CharacterId",
                        column: x => x.CharacterId,
                        principalSchema: "GameWorld",
                        principalTable: "PlayerCharacterEntity",
                        principalColumn: "EntityId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_FractionMemberEntity_FractionEntity_FractionId",
                        column: x => x.FractionId,
                        principalSchema: "GameWorld",
                        principalTable: "FractionEntity",
                        principalColumn: "EntityId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_FractionMemberEntity_FractionRankEntity_FractionRankId",
                        column: x => x.FractionRankId,
                        principalSchema: "GameWorld",
                        principalTable: "FractionRankEntity",
                        principalColumn: "EntityId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_FractionEntity_HeadQuarterPropertyId",
                schema: "GameWorld",
                table: "FractionEntity",
                column: "HeadQuarterPropertyId");

            migrationBuilder.CreateIndex(
                name: "IX_FractionMemberEntity_CharacterId",
                schema: "GameWorld",
                table: "FractionMemberEntity",
                column: "CharacterId");

            migrationBuilder.CreateIndex(
                name: "IX_FractionMemberEntity_FractionId",
                schema: "GameWorld",
                table: "FractionMemberEntity",
                column: "FractionId");

            migrationBuilder.CreateIndex(
                name: "IX_FractionMemberEntity_FractionRankId",
                schema: "GameWorld",
                table: "FractionMemberEntity",
                column: "FractionRankId");

            migrationBuilder.CreateIndex(
                name: "IX_FractionRankEntity_FractionEntityEntityId",
                schema: "GameWorld",
                table: "FractionRankEntity",
                column: "FractionEntityEntityId");

            migrationBuilder.CreateIndex(
                name: "IX_PropertyFractionOwnerEntity_FractionId",
                schema: "GameWorld",
                table: "PropertyFractionOwnerEntity",
                column: "FractionId");

            migrationBuilder.CreateIndex(
                name: "IX_PropertyFractionOwnerEntity_PropertyId",
                schema: "GameWorld",
                table: "PropertyFractionOwnerEntity",
                column: "PropertyId");

            migrationBuilder.CreateIndex(
                name: "IX_PropertyPlayerOwnerEntity_CharacterId",
                schema: "GameWorld",
                table: "PropertyPlayerOwnerEntity",
                column: "CharacterId");

            migrationBuilder.CreateIndex(
                name: "IX_PropertyPlayerOwnerEntity_PropertyId",
                schema: "GameWorld",
                table: "PropertyPlayerOwnerEntity",
                column: "PropertyId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "FractionMemberEntity",
                schema: "GameWorld");

            migrationBuilder.DropTable(
                name: "PropertyFractionOwnerEntity",
                schema: "GameWorld");

            migrationBuilder.DropTable(
                name: "PropertyPlayerOwnerEntity",
                schema: "GameWorld");

            migrationBuilder.DropTable(
                name: "FractionRankEntity",
                schema: "GameWorld");

            migrationBuilder.DropTable(
                name: "FractionEntity",
                schema: "GameWorld");

            migrationBuilder.DropTable(
                name: "HeadQuarterPropertyEntity",
                schema: "GameWorld");
        }
    }
}
