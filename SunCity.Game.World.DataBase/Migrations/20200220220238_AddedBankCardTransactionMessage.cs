﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace SunCity.Game.World.DataBase.Migrations
{
    public partial class AddedBankCardTransactionMessage : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "Comment",
                schema: "GameWorld",
                table: "BankCardTransferToTransactionEntity",
                maxLength: 256,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Comment",
                schema: "GameWorld",
                table: "BankCardTransferFromTransactionEntity",
                maxLength: 256,
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Comment",
                schema: "GameWorld",
                table: "BankCardTransferToTransactionEntity");

            migrationBuilder.DropColumn(
                name: "Comment",
                schema: "GameWorld",
                table: "BankCardTransferFromTransactionEntity");
        }
    }
}
