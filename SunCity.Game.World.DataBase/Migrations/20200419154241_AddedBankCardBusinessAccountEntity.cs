﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace SunCity.Game.World.DataBase.Migrations
{
    public partial class AddedBankCardBusinessAccountEntity : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "BankCardBusinessAccountEntity",
                schema: "GameWorld",
                columns: table => new
                {
                    EntityId = table.Column<Guid>(nullable: false),
                    CharacterId = table.Column<Guid>(nullable: false),
                    BusinessName = table.Column<string>(maxLength: 128, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BankCardBusinessAccountEntity", x => x.EntityId);
                    table.ForeignKey(
                        name: "FK_BankCardBusinessAccountEntity_PlayerCharacterEntity_Charact~",
                        column: x => x.CharacterId,
                        principalSchema: "GameWorld",
                        principalTable: "PlayerCharacterEntity",
                        principalColumn: "EntityId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_BankCardBusinessAccountEntity_BankCardAccountEntity_EntityId",
                        column: x => x.EntityId,
                        principalSchema: "GameWorld",
                        principalTable: "BankCardAccountEntity",
                        principalColumn: "EntityId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_BankCardBusinessAccountEntity_CharacterId",
                schema: "GameWorld",
                table: "BankCardBusinessAccountEntity",
                column: "CharacterId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "BankCardBusinessAccountEntity",
                schema: "GameWorld");
        }
    }
}
