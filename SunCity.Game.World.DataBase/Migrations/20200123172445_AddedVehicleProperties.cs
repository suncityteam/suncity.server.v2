﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace SunCity.Game.World.DataBase.Migrations
{
    public partial class AddedVehicleProperties : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "VehicleModelEntity",
                schema: "GameWorld",
                columns: table => new
                {
                    EntityId = table.Column<long>(nullable: false),
                    Name = table.Column<string>(maxLength: 32, nullable: false),
                    ModelClass = table.Column<int>(nullable: false),
                    FuelType = table.Column<byte>(nullable: false),
                    FuelTankSize = table.Column<float>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_VehicleModelEntity", x => x.EntityId);
                });

            migrationBuilder.CreateTable(
                name: "VehicleEntity",
                schema: "GameWorld",
                columns: table => new
                {
                    EntityId = table.Column<Guid>(nullable: false),
                    ModelId = table.Column<long>(nullable: false),
                    Position_X = table.Column<float>(nullable: true),
                    Position_Y = table.Column<float>(nullable: true),
                    Position_Z = table.Column<float>(nullable: true),
                    Rotation = table.Column<float>(nullable: false),
                    Fuel = table.Column<float>(nullable: false),
                    Color_FirstColor = table.Column<int>(nullable: true),
                    Color_SecondColor = table.Column<int>(nullable: true),
                    Health_Body = table.Column<float>(nullable: true),
                    Health_Engine = table.Column<float>(nullable: true),
                    Health_Gears = table.Column<float>(nullable: true),
                    DirtyLevel = table.Column<byte>(nullable: false),
                    PartsState = table.Column<int>(nullable: false),
                    WindowsState_WindowFrontLeft = table.Column<byte>(nullable: true),
                    WindowsState_WindowFrontRight = table.Column<byte>(nullable: true),
                    WindowsState_WindowRearRight = table.Column<byte>(nullable: true),
                    WindowsState_WindowRearLeft = table.Column<byte>(nullable: true),
                    WheelsState_Wheel0 = table.Column<byte>(nullable: true),
                    WheelsState_Wheel1 = table.Column<byte>(nullable: true),
                    WheelsState_Wheel2 = table.Column<byte>(nullable: true),
                    WheelsState_Wheel3 = table.Column<byte>(nullable: true),
                    WheelsState_Wheel4 = table.Column<byte>(nullable: true),
                    WheelsState_Wheel5 = table.Column<byte>(nullable: true),
                    WheelsState_Wheel6 = table.Column<byte>(nullable: true),
                    WheelsState_Wheel7 = table.Column<byte>(nullable: true),
                    WheelsState_Wheel8 = table.Column<byte>(nullable: true),
                    WheelsState_Wheel9 = table.Column<byte>(nullable: true),
                    DoorsState_DoorFrontLeft = table.Column<byte>(nullable: true),
                    DoorsState_DoorFrontRight = table.Column<byte>(nullable: true),
                    DoorsState_DoorRearLeft = table.Column<byte>(nullable: true),
                    DoorsState_DoorRearRight = table.Column<byte>(nullable: true),
                    DoorsState_DoorHood = table.Column<byte>(nullable: true),
                    DoorsState_DoorTrunk = table.Column<byte>(nullable: true),
                    PlateNumber_Number = table.Column<string>(maxLength: 16, nullable: true),
                    PlateNumber_Style = table.Column<byte>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_VehicleEntity", x => x.EntityId);
                    table.ForeignKey(
                        name: "FK_VehicleEntity_VehicleModelEntity_ModelId",
                        column: x => x.ModelId,
                        principalSchema: "GameWorld",
                        principalTable: "VehicleModelEntity",
                        principalColumn: "EntityId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_VehicleEntity_ModelId",
                schema: "GameWorld",
                table: "VehicleEntity",
                column: "ModelId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "VehicleEntity",
                schema: "GameWorld");

            migrationBuilder.DropTable(
                name: "VehicleModelEntity",
                schema: "GameWorld");
        }
    }
}
