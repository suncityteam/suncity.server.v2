﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace SunCity.Game.World.DataBase.Migrations
{
    public partial class SplitBankAccountToPlayersAndFractions : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_BankCardAccountEntity_PlayerCharacterEntity_CharacterId",
                schema: "GameWorld",
                table: "BankCardAccountEntity");

            migrationBuilder.DropIndex(
                name: "IX_BankCardAccountEntity_CharacterId",
                schema: "GameWorld",
                table: "BankCardAccountEntity");

            migrationBuilder.DropColumn(
                name: "CharacterId",
                schema: "GameWorld",
                table: "BankCardAccountEntity");

            migrationBuilder.AddColumn<int>(
                name: "Category",
                schema: "GameWorld",
                table: "BankCardAccountEntity",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateTable(
                name: "BankCardFractionAccountEntity",
                schema: "GameWorld",
                columns: table => new
                {
                    EntityId = table.Column<Guid>(nullable: false),
                    FractionId = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BankCardFractionAccountEntity", x => x.EntityId);
                    table.ForeignKey(
                        name: "FK_BankCardFractionAccountEntity_BankCardAccountEntity_EntityId",
                        column: x => x.EntityId,
                        principalSchema: "GameWorld",
                        principalTable: "BankCardAccountEntity",
                        principalColumn: "EntityId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_BankCardFractionAccountEntity_FractionEntity_FractionId",
                        column: x => x.FractionId,
                        principalSchema: "GameWorld",
                        principalTable: "FractionEntity",
                        principalColumn: "EntityId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "BankCardPlayerAccountEntity",
                schema: "GameWorld",
                columns: table => new
                {
                    EntityId = table.Column<Guid>(nullable: false),
                    CharacterId = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BankCardPlayerAccountEntity", x => x.EntityId);
                    table.ForeignKey(
                        name: "FK_BankCardPlayerAccountEntity_PlayerCharacterEntity_Character~",
                        column: x => x.CharacterId,
                        principalSchema: "GameWorld",
                        principalTable: "PlayerCharacterEntity",
                        principalColumn: "EntityId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_BankCardPlayerAccountEntity_BankCardAccountEntity_EntityId",
                        column: x => x.EntityId,
                        principalSchema: "GameWorld",
                        principalTable: "BankCardAccountEntity",
                        principalColumn: "EntityId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_BankCardFractionAccountEntity_FractionId",
                schema: "GameWorld",
                table: "BankCardFractionAccountEntity",
                column: "FractionId");

            migrationBuilder.CreateIndex(
                name: "IX_BankCardPlayerAccountEntity_CharacterId",
                schema: "GameWorld",
                table: "BankCardPlayerAccountEntity",
                column: "CharacterId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "BankCardFractionAccountEntity",
                schema: "GameWorld");

            migrationBuilder.DropTable(
                name: "BankCardPlayerAccountEntity",
                schema: "GameWorld");

            migrationBuilder.DropColumn(
                name: "Category",
                schema: "GameWorld",
                table: "BankCardAccountEntity");

            migrationBuilder.AddColumn<Guid>(
                name: "CharacterId",
                schema: "GameWorld",
                table: "BankCardAccountEntity",
                type: "uuid",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));

            migrationBuilder.CreateIndex(
                name: "IX_BankCardAccountEntity_CharacterId",
                schema: "GameWorld",
                table: "BankCardAccountEntity",
                column: "CharacterId");

            migrationBuilder.AddForeignKey(
                name: "FK_BankCardAccountEntity_PlayerCharacterEntity_CharacterId",
                schema: "GameWorld",
                table: "BankCardAccountEntity",
                column: "CharacterId",
                principalSchema: "GameWorld",
                principalTable: "PlayerCharacterEntity",
                principalColumn: "EntityId",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
