﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace SunCity.Game.World.DataBase.Migrations
{
    public partial class AddedBankATMs : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "BankATMEntity",
                schema: "GameWorld",
                columns: table => new
                {
                    EntityId = table.Column<Guid>(nullable: false),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    Deleted = table.Column<bool>(nullable: false),
                    DeletedDate = table.Column<DateTime>(nullable: true),
                    BankPropertyId = table.Column<Guid>(nullable: false),
                    Position_X = table.Column<float>(nullable: true),
                    Position_Y = table.Column<float>(nullable: true),
                    Position_Z = table.Column<float>(nullable: true),
                    Rotation_X = table.Column<float>(nullable: true),
                    Rotation_Y = table.Column<float>(nullable: true),
                    Rotation_Z = table.Column<float>(nullable: true),
                    Balance = table.Column<long>(nullable: false),
                    AdminComment = table.Column<string>(maxLength: 256, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BankATMEntity", x => x.EntityId);
                    table.ForeignKey(
                        name: "FK_BankATMEntity_BankPropertyEntity_BankPropertyId",
                        column: x => x.BankPropertyId,
                        principalSchema: "GameWorld",
                        principalTable: "BankPropertyEntity",
                        principalColumn: "EntityId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_BankATMEntity_BankPropertyId",
                schema: "GameWorld",
                table: "BankATMEntity",
                column: "BankPropertyId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "BankATMEntity",
                schema: "GameWorld");
        }
    }
}
