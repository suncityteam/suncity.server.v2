﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace SunCity.Game.World.DataBase.Migrations
{
    public partial class RefactoringPropertyOwners : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_PropertyFractionOwnerEntity_PropertyEntity_PropertyId",
                schema: "GameWorld",
                table: "PropertyFractionOwnerEntity");

            migrationBuilder.DropForeignKey(
                name: "FK_PropertyPlayerOwnerEntity_PropertyEntity_PropertyId",
                schema: "GameWorld",
                table: "PropertyPlayerOwnerEntity");

            migrationBuilder.DropIndex(
                name: "IX_PropertyPlayerOwnerEntity_PropertyId",
                schema: "GameWorld",
                table: "PropertyPlayerOwnerEntity");

            migrationBuilder.DropIndex(
                name: "IX_PropertyFractionOwnerEntity_PropertyId",
                schema: "GameWorld",
                table: "PropertyFractionOwnerEntity");

            migrationBuilder.DropColumn(
                name: "Begin",
                schema: "GameWorld",
                table: "PropertyPlayerOwnerEntity");

            migrationBuilder.DropColumn(
                name: "End",
                schema: "GameWorld",
                table: "PropertyPlayerOwnerEntity");

            migrationBuilder.DropColumn(
                name: "PropertyId",
                schema: "GameWorld",
                table: "PropertyPlayerOwnerEntity");

            migrationBuilder.DropColumn(
                name: "Type",
                schema: "GameWorld",
                table: "PropertyPlayerOwnerEntity");

            migrationBuilder.DropColumn(
                name: "Begin",
                schema: "GameWorld",
                table: "PropertyFractionOwnerEntity");

            migrationBuilder.DropColumn(
                name: "End",
                schema: "GameWorld",
                table: "PropertyFractionOwnerEntity");

            migrationBuilder.DropColumn(
                name: "PropertyId",
                schema: "GameWorld",
                table: "PropertyFractionOwnerEntity");

            migrationBuilder.DropColumn(
                name: "Type",
                schema: "GameWorld",
                table: "PropertyFractionOwnerEntity");

            migrationBuilder.CreateTable(
                name: "PropertyOwnerEntity",
                schema: "GameWorld",
                columns: table => new
                {
                    EntityId = table.Column<Guid>(nullable: false),
                    Type = table.Column<int>(nullable: false),
                    Group = table.Column<int>(nullable: false),
                    PropertyId = table.Column<Guid>(nullable: false),
                    Begin = table.Column<DateTime>(nullable: false),
                    End = table.Column<DateTime>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PropertyOwnerEntity", x => x.EntityId);
                    table.ForeignKey(
                        name: "FK_PropertyOwnerEntity_PropertyEntity_PropertyId",
                        column: x => x.PropertyId,
                        principalSchema: "GameWorld",
                        principalTable: "PropertyEntity",
                        principalColumn: "EntityId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_PropertyOwnerEntity_PropertyId",
                schema: "GameWorld",
                table: "PropertyOwnerEntity",
                column: "PropertyId");

            migrationBuilder.AddForeignKey(
                name: "FK_PropertyFractionOwnerEntity_PropertyOwnerEntity_EntityId",
                schema: "GameWorld",
                table: "PropertyFractionOwnerEntity",
                column: "EntityId",
                principalSchema: "GameWorld",
                principalTable: "PropertyOwnerEntity",
                principalColumn: "EntityId",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_PropertyPlayerOwnerEntity_PropertyOwnerEntity_EntityId",
                schema: "GameWorld",
                table: "PropertyPlayerOwnerEntity",
                column: "EntityId",
                principalSchema: "GameWorld",
                principalTable: "PropertyOwnerEntity",
                principalColumn: "EntityId",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_PropertyFractionOwnerEntity_PropertyOwnerEntity_EntityId",
                schema: "GameWorld",
                table: "PropertyFractionOwnerEntity");

            migrationBuilder.DropForeignKey(
                name: "FK_PropertyPlayerOwnerEntity_PropertyOwnerEntity_EntityId",
                schema: "GameWorld",
                table: "PropertyPlayerOwnerEntity");

            migrationBuilder.DropTable(
                name: "PropertyOwnerEntity",
                schema: "GameWorld");

            migrationBuilder.AddColumn<DateTime>(
                name: "Begin",
                schema: "GameWorld",
                table: "PropertyPlayerOwnerEntity",
                type: "timestamp without time zone",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<DateTime>(
                name: "End",
                schema: "GameWorld",
                table: "PropertyPlayerOwnerEntity",
                type: "timestamp without time zone",
                nullable: true);

            migrationBuilder.AddColumn<Guid>(
                name: "PropertyId",
                schema: "GameWorld",
                table: "PropertyPlayerOwnerEntity",
                type: "uuid",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));

            migrationBuilder.AddColumn<int>(
                name: "Type",
                schema: "GameWorld",
                table: "PropertyPlayerOwnerEntity",
                type: "integer",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<DateTime>(
                name: "Begin",
                schema: "GameWorld",
                table: "PropertyFractionOwnerEntity",
                type: "timestamp without time zone",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<DateTime>(
                name: "End",
                schema: "GameWorld",
                table: "PropertyFractionOwnerEntity",
                type: "timestamp without time zone",
                nullable: true);

            migrationBuilder.AddColumn<Guid>(
                name: "PropertyId",
                schema: "GameWorld",
                table: "PropertyFractionOwnerEntity",
                type: "uuid",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));

            migrationBuilder.AddColumn<int>(
                name: "Type",
                schema: "GameWorld",
                table: "PropertyFractionOwnerEntity",
                type: "integer",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateIndex(
                name: "IX_PropertyPlayerOwnerEntity_PropertyId",
                schema: "GameWorld",
                table: "PropertyPlayerOwnerEntity",
                column: "PropertyId");

            migrationBuilder.CreateIndex(
                name: "IX_PropertyFractionOwnerEntity_PropertyId",
                schema: "GameWorld",
                table: "PropertyFractionOwnerEntity",
                column: "PropertyId");

            migrationBuilder.AddForeignKey(
                name: "FK_PropertyFractionOwnerEntity_PropertyEntity_PropertyId",
                schema: "GameWorld",
                table: "PropertyFractionOwnerEntity",
                column: "PropertyId",
                principalSchema: "GameWorld",
                principalTable: "PropertyEntity",
                principalColumn: "EntityId",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_PropertyPlayerOwnerEntity_PropertyEntity_PropertyId",
                schema: "GameWorld",
                table: "PropertyPlayerOwnerEntity",
                column: "PropertyId",
                principalSchema: "GameWorld",
                principalTable: "PropertyEntity",
                principalColumn: "EntityId",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
