﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace SunCity.Game.World.DataBase.Migrations
{
    public partial class AddedBankAccountReferenceForFraction : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<Guid>(
                name: "MainBankCardId",
                schema: "GameWorld",
                table: "FractionEntity",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_FractionEntity_MainBankCardId",
                schema: "GameWorld",
                table: "FractionEntity",
                column: "MainBankCardId");

            migrationBuilder.AddForeignKey(
                name: "FK_FractionEntity_BankCardFractionAccountEntity_MainBankCardId",
                schema: "GameWorld",
                table: "FractionEntity",
                column: "MainBankCardId",
                principalSchema: "GameWorld",
                principalTable: "BankCardFractionAccountEntity",
                principalColumn: "EntityId",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_FractionEntity_BankCardFractionAccountEntity_MainBankCardId",
                schema: "GameWorld",
                table: "FractionEntity");

            migrationBuilder.DropIndex(
                name: "IX_FractionEntity_MainBankCardId",
                schema: "GameWorld",
                table: "FractionEntity");

            migrationBuilder.DropColumn(
                name: "MainBankCardId",
                schema: "GameWorld",
                table: "FractionEntity");
        }
    }
}
