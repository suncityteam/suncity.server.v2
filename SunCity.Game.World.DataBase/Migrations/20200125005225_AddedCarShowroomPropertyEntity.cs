﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace SunCity.Game.World.DataBase.Migrations
{
    public partial class AddedCarShowroomPropertyEntity : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "CarShowroomPropertyEntity",
                schema: "GameWorld",
                columns: table => new
                {
                    EntityId = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CarShowroomPropertyEntity", x => x.EntityId);
                    table.ForeignKey(
                        name: "FK_CarShowroomPropertyEntity_BusinessPropertyEntity_EntityId",
                        column: x => x.EntityId,
                        principalSchema: "GameWorld",
                        principalTable: "BusinessPropertyEntity",
                        principalColumn: "EntityId",
                        onDelete: ReferentialAction.Cascade);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "CarShowroomPropertyEntity",
                schema: "GameWorld");
        }
    }
}
