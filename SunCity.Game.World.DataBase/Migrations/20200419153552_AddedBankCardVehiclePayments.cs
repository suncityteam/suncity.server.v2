﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace SunCity.Game.World.DataBase.Migrations
{
    public partial class AddedBankCardVehiclePayments : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Rotation",
                schema: "GameWorld",
                table: "CarShowroomVehicleSpawnPointEntity");

            migrationBuilder.AddColumn<float>(
                name: "Rotation_X",
                schema: "GameWorld",
                table: "CarShowroomVehicleSpawnPointEntity",
                nullable: true);

            migrationBuilder.AddColumn<float>(
                name: "Rotation_Y",
                schema: "GameWorld",
                table: "CarShowroomVehicleSpawnPointEntity",
                nullable: true);

            migrationBuilder.AddColumn<float>(
                name: "Rotation_Z",
                schema: "GameWorld",
                table: "CarShowroomVehicleSpawnPointEntity",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "BankCardVehiclePaymentFromTransactionEntity",
                schema: "GameWorld",
                columns: table => new
                {
                    EntityId = table.Column<Guid>(nullable: false),
                    PropertyId = table.Column<Guid>(nullable: false),
                    VehicleId = table.Column<Guid>(nullable: false),
                    PayerId = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BankCardVehiclePaymentFromTransactionEntity", x => x.EntityId);
                    table.ForeignKey(
                        name: "FK_BankCardVehiclePaymentFromTransactionEntity_BankCardTransac~",
                        column: x => x.EntityId,
                        principalSchema: "GameWorld",
                        principalTable: "BankCardTransactionEntity",
                        principalColumn: "EntityId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_BankCardVehiclePaymentFromTransactionEntity_BankCardAccount~",
                        column: x => x.PayerId,
                        principalSchema: "GameWorld",
                        principalTable: "BankCardAccountEntity",
                        principalColumn: "EntityId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_BankCardVehiclePaymentFromTransactionEntity_PropertyEntity_~",
                        column: x => x.PropertyId,
                        principalSchema: "GameWorld",
                        principalTable: "PropertyEntity",
                        principalColumn: "EntityId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_BankCardVehiclePaymentFromTransactionEntity_VehicleEntity_V~",
                        column: x => x.VehicleId,
                        principalSchema: "GameWorld",
                        principalTable: "VehicleEntity",
                        principalColumn: "EntityId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "BankCardVehiclePaymentToTransactionEntity",
                schema: "GameWorld",
                columns: table => new
                {
                    EntityId = table.Column<Guid>(nullable: false),
                    PropertyId = table.Column<Guid>(nullable: false),
                    VehicleId = table.Column<Guid>(nullable: false),
                    PayeeId = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BankCardVehiclePaymentToTransactionEntity", x => x.EntityId);
                    table.ForeignKey(
                        name: "FK_BankCardVehiclePaymentToTransactionEntity_BankCardTransacti~",
                        column: x => x.EntityId,
                        principalSchema: "GameWorld",
                        principalTable: "BankCardTransactionEntity",
                        principalColumn: "EntityId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_BankCardVehiclePaymentToTransactionEntity_BankCardAccountEn~",
                        column: x => x.PayeeId,
                        principalSchema: "GameWorld",
                        principalTable: "BankCardAccountEntity",
                        principalColumn: "EntityId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_BankCardVehiclePaymentToTransactionEntity_PropertyEntity_Pr~",
                        column: x => x.PropertyId,
                        principalSchema: "GameWorld",
                        principalTable: "PropertyEntity",
                        principalColumn: "EntityId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_BankCardVehiclePaymentToTransactionEntity_VehicleEntity_Veh~",
                        column: x => x.VehicleId,
                        principalSchema: "GameWorld",
                        principalTable: "VehicleEntity",
                        principalColumn: "EntityId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_BankCardVehiclePaymentFromTransactionEntity_PayerId",
                schema: "GameWorld",
                table: "BankCardVehiclePaymentFromTransactionEntity",
                column: "PayerId");

            migrationBuilder.CreateIndex(
                name: "IX_BankCardVehiclePaymentFromTransactionEntity_PropertyId",
                schema: "GameWorld",
                table: "BankCardVehiclePaymentFromTransactionEntity",
                column: "PropertyId");

            migrationBuilder.CreateIndex(
                name: "IX_BankCardVehiclePaymentFromTransactionEntity_VehicleId",
                schema: "GameWorld",
                table: "BankCardVehiclePaymentFromTransactionEntity",
                column: "VehicleId");

            migrationBuilder.CreateIndex(
                name: "IX_BankCardVehiclePaymentToTransactionEntity_PayeeId",
                schema: "GameWorld",
                table: "BankCardVehiclePaymentToTransactionEntity",
                column: "PayeeId");

            migrationBuilder.CreateIndex(
                name: "IX_BankCardVehiclePaymentToTransactionEntity_PropertyId",
                schema: "GameWorld",
                table: "BankCardVehiclePaymentToTransactionEntity",
                column: "PropertyId");

            migrationBuilder.CreateIndex(
                name: "IX_BankCardVehiclePaymentToTransactionEntity_VehicleId",
                schema: "GameWorld",
                table: "BankCardVehiclePaymentToTransactionEntity",
                column: "VehicleId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "BankCardVehiclePaymentFromTransactionEntity",
                schema: "GameWorld");

            migrationBuilder.DropTable(
                name: "BankCardVehiclePaymentToTransactionEntity",
                schema: "GameWorld");

            migrationBuilder.DropColumn(
                name: "Rotation_X",
                schema: "GameWorld",
                table: "CarShowroomVehicleSpawnPointEntity");

            migrationBuilder.DropColumn(
                name: "Rotation_Y",
                schema: "GameWorld",
                table: "CarShowroomVehicleSpawnPointEntity");

            migrationBuilder.DropColumn(
                name: "Rotation_Z",
                schema: "GameWorld",
                table: "CarShowroomVehicleSpawnPointEntity");

            migrationBuilder.AddColumn<float>(
                name: "Rotation",
                schema: "GameWorld",
                table: "CarShowroomVehicleSpawnPointEntity",
                type: "real",
                nullable: false,
                defaultValue: 0f);
        }
    }
}
