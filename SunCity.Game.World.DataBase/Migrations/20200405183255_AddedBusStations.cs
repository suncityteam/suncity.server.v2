﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace SunCity.Game.World.DataBase.Migrations
{
    public partial class AddedBusStations : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "BusStationPropertyEntity",
                schema: "GameWorld",
                columns: table => new
                {
                    EntityId = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BusStationPropertyEntity", x => x.EntityId);
                    table.ForeignKey(
                        name: "FK_BusStationPropertyEntity_BusinessPropertyEntity_EntityId",
                        column: x => x.EntityId,
                        principalSchema: "GameWorld",
                        principalTable: "BusinessPropertyEntity",
                        principalColumn: "EntityId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "StationPointEntity",
                schema: "GameWorld",
                columns: table => new
                {
                    EntityId = table.Column<Guid>(nullable: false),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    Name = table.Column<string>(nullable: true),
                    Deleted = table.Column<bool>(nullable: false),
                    DeletedDate = table.Column<DateTime>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_StationPointEntity", x => x.EntityId);
                });

            migrationBuilder.CreateTable(
                name: "BusStationRouteEntity",
                schema: "GameWorld",
                columns: table => new
                {
                    EntityId = table.Column<Guid>(nullable: false),
                    BusStationId = table.Column<Guid>(nullable: false),
                    Name = table.Column<string>(maxLength: 64, nullable: false),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    Deleted = table.Column<bool>(nullable: false),
                    DeletedDate = table.Column<DateTime>(nullable: true),
                    Cost_Amount = table.Column<long>(nullable: true),
                    Cost_Currency = table.Column<byte>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BusStationRouteEntity", x => x.EntityId);
                    table.ForeignKey(
                        name: "FK_BusStationRouteEntity_BusStationPropertyEntity_BusStationId",
                        column: x => x.BusStationId,
                        principalSchema: "GameWorld",
                        principalTable: "BusStationPropertyEntity",
                        principalColumn: "EntityId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "BusStationRoutePointEntity",
                schema: "GameWorld",
                columns: table => new
                {
                    EntityId = table.Column<Guid>(nullable: false),
                    RouteId = table.Column<Guid>(nullable: false),
                    StationPointEntityEntityId = table.Column<Guid>(nullable: true),
                    StationPointId = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BusStationRoutePointEntity", x => x.EntityId);
                    table.ForeignKey(
                        name: "FK_BusStationRoutePointEntity_BusStationRouteEntity_RouteId",
                        column: x => x.RouteId,
                        principalSchema: "GameWorld",
                        principalTable: "BusStationRouteEntity",
                        principalColumn: "EntityId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_BusStationRoutePointEntity_StationPointEntity_StationPointE~",
                        column: x => x.StationPointEntityEntityId,
                        principalSchema: "GameWorld",
                        principalTable: "StationPointEntity",
                        principalColumn: "EntityId",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_BusStationRouteEntity_BusStationId",
                schema: "GameWorld",
                table: "BusStationRouteEntity",
                column: "BusStationId");

            migrationBuilder.CreateIndex(
                name: "IX_BusStationRoutePointEntity_RouteId",
                schema: "GameWorld",
                table: "BusStationRoutePointEntity",
                column: "RouteId");

            migrationBuilder.CreateIndex(
                name: "IX_BusStationRoutePointEntity_StationPointEntityEntityId",
                schema: "GameWorld",
                table: "BusStationRoutePointEntity",
                column: "StationPointEntityEntityId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "BusStationRoutePointEntity",
                schema: "GameWorld");

            migrationBuilder.DropTable(
                name: "BusStationRouteEntity",
                schema: "GameWorld");

            migrationBuilder.DropTable(
                name: "StationPointEntity",
                schema: "GameWorld");

            migrationBuilder.DropTable(
                name: "BusStationPropertyEntity",
                schema: "GameWorld");
        }
    }
}
