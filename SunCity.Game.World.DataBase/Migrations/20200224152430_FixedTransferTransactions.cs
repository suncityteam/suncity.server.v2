﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace SunCity.Game.World.DataBase.Migrations
{
    public partial class FixedTransferTransactions : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_BankCardTransferFromTransactionEntity_BankCardAccountEntity~",
                schema: "GameWorld",
                table: "BankCardTransferFromTransactionEntity");

            migrationBuilder.DropForeignKey(
                name: "FK_BankCardTransferToTransactionEntity_BankCardAccountEntity_P~",
                schema: "GameWorld",
                table: "BankCardTransferToTransactionEntity");

            migrationBuilder.DropIndex(
                name: "IX_BankCardTransferToTransactionEntity_PayerId",
                schema: "GameWorld",
                table: "BankCardTransferToTransactionEntity");

            migrationBuilder.DropIndex(
                name: "IX_BankCardTransferFromTransactionEntity_RecipientId",
                schema: "GameWorld",
                table: "BankCardTransferFromTransactionEntity");

            migrationBuilder.DropColumn(
                name: "PayerId",
                schema: "GameWorld",
                table: "BankCardTransferToTransactionEntity");

            migrationBuilder.DropColumn(
                name: "RecipientId",
                schema: "GameWorld",
                table: "BankCardTransferFromTransactionEntity");

            migrationBuilder.AddColumn<Guid>(
                name: "PayeeId",
                schema: "GameWorld",
                table: "BankCardTransferToTransactionEntity",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));

            migrationBuilder.AddColumn<Guid>(
                name: "PayerId",
                schema: "GameWorld",
                table: "BankCardTransferFromTransactionEntity",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));

            migrationBuilder.CreateIndex(
                name: "IX_BankCardTransferToTransactionEntity_PayeeId",
                schema: "GameWorld",
                table: "BankCardTransferToTransactionEntity",
                column: "PayeeId");

            migrationBuilder.CreateIndex(
                name: "IX_BankCardTransferFromTransactionEntity_PayerId",
                schema: "GameWorld",
                table: "BankCardTransferFromTransactionEntity",
                column: "PayerId");

            migrationBuilder.AddForeignKey(
                name: "FK_BankCardTransferFromTransactionEntity_BankCardAccountEntity~",
                schema: "GameWorld",
                table: "BankCardTransferFromTransactionEntity",
                column: "PayerId",
                principalSchema: "GameWorld",
                principalTable: "BankCardAccountEntity",
                principalColumn: "EntityId",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_BankCardTransferToTransactionEntity_BankCardAccountEntity_P~",
                schema: "GameWorld",
                table: "BankCardTransferToTransactionEntity",
                column: "PayeeId",
                principalSchema: "GameWorld",
                principalTable: "BankCardAccountEntity",
                principalColumn: "EntityId",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_BankCardTransferFromTransactionEntity_BankCardAccountEntity~",
                schema: "GameWorld",
                table: "BankCardTransferFromTransactionEntity");

            migrationBuilder.DropForeignKey(
                name: "FK_BankCardTransferToTransactionEntity_BankCardAccountEntity_P~",
                schema: "GameWorld",
                table: "BankCardTransferToTransactionEntity");

            migrationBuilder.DropIndex(
                name: "IX_BankCardTransferToTransactionEntity_PayeeId",
                schema: "GameWorld",
                table: "BankCardTransferToTransactionEntity");

            migrationBuilder.DropIndex(
                name: "IX_BankCardTransferFromTransactionEntity_PayerId",
                schema: "GameWorld",
                table: "BankCardTransferFromTransactionEntity");

            migrationBuilder.DropColumn(
                name: "PayeeId",
                schema: "GameWorld",
                table: "BankCardTransferToTransactionEntity");

            migrationBuilder.DropColumn(
                name: "PayerId",
                schema: "GameWorld",
                table: "BankCardTransferFromTransactionEntity");

            migrationBuilder.AddColumn<Guid>(
                name: "PayerId",
                schema: "GameWorld",
                table: "BankCardTransferToTransactionEntity",
                type: "uuid",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));

            migrationBuilder.AddColumn<Guid>(
                name: "RecipientId",
                schema: "GameWorld",
                table: "BankCardTransferFromTransactionEntity",
                type: "uuid",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));

            migrationBuilder.CreateIndex(
                name: "IX_BankCardTransferToTransactionEntity_PayerId",
                schema: "GameWorld",
                table: "BankCardTransferToTransactionEntity",
                column: "PayerId");

            migrationBuilder.CreateIndex(
                name: "IX_BankCardTransferFromTransactionEntity_RecipientId",
                schema: "GameWorld",
                table: "BankCardTransferFromTransactionEntity",
                column: "RecipientId");

            migrationBuilder.AddForeignKey(
                name: "FK_BankCardTransferFromTransactionEntity_BankCardAccountEntity~",
                schema: "GameWorld",
                table: "BankCardTransferFromTransactionEntity",
                column: "RecipientId",
                principalSchema: "GameWorld",
                principalTable: "BankCardAccountEntity",
                principalColumn: "EntityId",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_BankCardTransferToTransactionEntity_BankCardAccountEntity_P~",
                schema: "GameWorld",
                table: "BankCardTransferToTransactionEntity",
                column: "PayerId",
                principalSchema: "GameWorld",
                principalTable: "BankCardAccountEntity",
                principalColumn: "EntityId",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
