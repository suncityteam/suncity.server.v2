﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace SunCity.Game.World.DataBase.Migrations
{
    public partial class FixedPropertyTransactions : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_BankCardPropertyPaymentFromTransactionEntity_BankCardAccoun~",
                schema: "GameWorld",
                table: "BankCardPropertyPaymentFromTransactionEntity");

            migrationBuilder.DropForeignKey(
                name: "FK_BankCardPropertyPaymentToTransactionEntity_BankCardAccountE~",
                schema: "GameWorld",
                table: "BankCardPropertyPaymentToTransactionEntity");

            migrationBuilder.DropIndex(
                name: "IX_BankCardPropertyPaymentToTransactionEntity_PayerId",
                schema: "GameWorld",
                table: "BankCardPropertyPaymentToTransactionEntity");

            migrationBuilder.DropIndex(
                name: "IX_BankCardPropertyPaymentFromTransactionEntity_RecipientId",
                schema: "GameWorld",
                table: "BankCardPropertyPaymentFromTransactionEntity");

            migrationBuilder.DropColumn(
                name: "PayerId",
                schema: "GameWorld",
                table: "BankCardPropertyPaymentToTransactionEntity");

            migrationBuilder.DropColumn(
                name: "RecipientId",
                schema: "GameWorld",
                table: "BankCardPropertyPaymentFromTransactionEntity");

            migrationBuilder.AddColumn<Guid>(
                name: "PayeeId",
                schema: "GameWorld",
                table: "BankCardPropertyPaymentToTransactionEntity",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));

            migrationBuilder.AddColumn<Guid>(
                name: "PayerId",
                schema: "GameWorld",
                table: "BankCardPropertyPaymentFromTransactionEntity",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));

            migrationBuilder.CreateIndex(
                name: "IX_BankCardPropertyPaymentToTransactionEntity_PayeeId",
                schema: "GameWorld",
                table: "BankCardPropertyPaymentToTransactionEntity",
                column: "PayeeId");

            migrationBuilder.CreateIndex(
                name: "IX_BankCardPropertyPaymentFromTransactionEntity_PayerId",
                schema: "GameWorld",
                table: "BankCardPropertyPaymentFromTransactionEntity",
                column: "PayerId");

            migrationBuilder.AddForeignKey(
                name: "FK_BankCardPropertyPaymentFromTransactionEntity_BankCardAccoun~",
                schema: "GameWorld",
                table: "BankCardPropertyPaymentFromTransactionEntity",
                column: "PayerId",
                principalSchema: "GameWorld",
                principalTable: "BankCardAccountEntity",
                principalColumn: "EntityId",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_BankCardPropertyPaymentToTransactionEntity_BankCardAccountE~",
                schema: "GameWorld",
                table: "BankCardPropertyPaymentToTransactionEntity",
                column: "PayeeId",
                principalSchema: "GameWorld",
                principalTable: "BankCardAccountEntity",
                principalColumn: "EntityId",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_BankCardPropertyPaymentFromTransactionEntity_BankCardAccoun~",
                schema: "GameWorld",
                table: "BankCardPropertyPaymentFromTransactionEntity");

            migrationBuilder.DropForeignKey(
                name: "FK_BankCardPropertyPaymentToTransactionEntity_BankCardAccountE~",
                schema: "GameWorld",
                table: "BankCardPropertyPaymentToTransactionEntity");

            migrationBuilder.DropIndex(
                name: "IX_BankCardPropertyPaymentToTransactionEntity_PayeeId",
                schema: "GameWorld",
                table: "BankCardPropertyPaymentToTransactionEntity");

            migrationBuilder.DropIndex(
                name: "IX_BankCardPropertyPaymentFromTransactionEntity_PayerId",
                schema: "GameWorld",
                table: "BankCardPropertyPaymentFromTransactionEntity");

            migrationBuilder.DropColumn(
                name: "PayeeId",
                schema: "GameWorld",
                table: "BankCardPropertyPaymentToTransactionEntity");

            migrationBuilder.DropColumn(
                name: "PayerId",
                schema: "GameWorld",
                table: "BankCardPropertyPaymentFromTransactionEntity");

            migrationBuilder.AddColumn<Guid>(
                name: "PayerId",
                schema: "GameWorld",
                table: "BankCardPropertyPaymentToTransactionEntity",
                type: "uuid",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));

            migrationBuilder.AddColumn<Guid>(
                name: "RecipientId",
                schema: "GameWorld",
                table: "BankCardPropertyPaymentFromTransactionEntity",
                type: "uuid",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));

            migrationBuilder.CreateIndex(
                name: "IX_BankCardPropertyPaymentToTransactionEntity_PayerId",
                schema: "GameWorld",
                table: "BankCardPropertyPaymentToTransactionEntity",
                column: "PayerId");

            migrationBuilder.CreateIndex(
                name: "IX_BankCardPropertyPaymentFromTransactionEntity_RecipientId",
                schema: "GameWorld",
                table: "BankCardPropertyPaymentFromTransactionEntity",
                column: "RecipientId");

            migrationBuilder.AddForeignKey(
                name: "FK_BankCardPropertyPaymentFromTransactionEntity_BankCardAccoun~",
                schema: "GameWorld",
                table: "BankCardPropertyPaymentFromTransactionEntity",
                column: "RecipientId",
                principalSchema: "GameWorld",
                principalTable: "BankCardAccountEntity",
                principalColumn: "EntityId",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_BankCardPropertyPaymentToTransactionEntity_BankCardAccountE~",
                schema: "GameWorld",
                table: "BankCardPropertyPaymentToTransactionEntity",
                column: "PayerId",
                principalSchema: "GameWorld",
                principalTable: "BankCardAccountEntity",
                principalColumn: "EntityId",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
