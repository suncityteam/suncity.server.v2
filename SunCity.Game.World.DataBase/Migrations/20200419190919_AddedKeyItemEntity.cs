﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace SunCity.Game.World.DataBase.Migrations
{
    public partial class AddedKeyItemEntity : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "PropertyKeyItemEntity",
                schema: "GameWorld",
                columns: table => new
                {
                    EntityId = table.Column<Guid>(nullable: false),
                    Category = table.Column<byte>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PropertyKeyItemEntity", x => x.EntityId);
                    table.ForeignKey(
                        name: "FK_PropertyKeyItemEntity_ItemEntity_EntityId",
                        column: x => x.EntityId,
                        principalSchema: "GameWorld",
                        principalTable: "ItemEntity",
                        principalColumn: "EntityId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "VehicleKeyItemEntity",
                schema: "GameWorld",
                columns: table => new
                {
                    EntityId = table.Column<Guid>(nullable: false),
                    Category = table.Column<byte>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_VehicleKeyItemEntity", x => x.EntityId);
                    table.ForeignKey(
                        name: "FK_VehicleKeyItemEntity_ItemEntity_EntityId",
                        column: x => x.EntityId,
                        principalSchema: "GameWorld",
                        principalTable: "ItemEntity",
                        principalColumn: "EntityId",
                        onDelete: ReferentialAction.Cascade);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "PropertyKeyItemEntity",
                schema: "GameWorld");

            migrationBuilder.DropTable(
                name: "VehicleKeyItemEntity",
                schema: "GameWorld");
        }
    }
}
