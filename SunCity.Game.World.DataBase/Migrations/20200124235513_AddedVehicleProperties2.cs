﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace SunCity.Game.World.DataBase.Migrations
{
    public partial class AddedVehicleProperties2 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "Color_Pearlescent",
                schema: "GameWorld",
                table: "VehicleEntity",
                nullable: true);

            migrationBuilder.AddColumn<double>(
                name: "Mileage",
                schema: "GameWorld",
                table: "VehicleEntity",
                nullable: false,
                defaultValue: 0.0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Color_Pearlescent",
                schema: "GameWorld",
                table: "VehicleEntity");

            migrationBuilder.DropColumn(
                name: "Mileage",
                schema: "GameWorld",
                table: "VehicleEntity");
        }
    }
}
