﻿using Microsoft.EntityFrameworkCore;
using SunCity.Common.Types.Math;
using SunCity.Game.Types.Vehicle;
using SunCity.Game.Types.Vehicle.States;
using SunCity.Game.World.DataBase.Entitites.Fraction;
using SunCity.Game.World.DataBase.Entitites.Interior;
using SunCity.Game.World.DataBase.Entitites.Item;
using SunCity.Game.World.DataBase.Entitites.Item.Category;
using SunCity.Game.World.DataBase.Entitites.Item.World;
using SunCity.Game.World.DataBase.Entitites.Licenses;
using SunCity.Game.World.DataBase.Entitites.Player;
using SunCity.Game.World.DataBase.Entitites.Player.Character;
using SunCity.Game.World.DataBase.Entitites.Player.Inventory;
using SunCity.Game.World.DataBase.Entitites.Player.Licenses;
using SunCity.Game.World.DataBase.Entitites.Property;
using SunCity.Game.World.DataBase.Entitites.Property.Business;
using SunCity.Game.World.DataBase.Entitites.Property.Business.Bank;
using SunCity.Game.World.DataBase.Entitites.Property.Business.Bank.Account;
using SunCity.Game.World.DataBase.Entitites.Property.Business.Bank.Tariff;
using SunCity.Game.World.DataBase.Entitites.Property.Business.Bank.Transaction;
using SunCity.Game.World.DataBase.Entitites.Property.Business.CarShowroom;
using SunCity.Game.World.DataBase.Entitites.Property.Business.DrivingSchool;
using SunCity.Game.World.DataBase.Entitites.Property.Business.DrivingSchool.Routes;
using SunCity.Game.World.DataBase.Entitites.Property.Business.GasStation;
using SunCity.Game.World.DataBase.Entitites.Property.Business.Hospital;
using SunCity.Game.World.DataBase.Entitites.Property.Business.PassengerTransportation.BusStation;
using SunCity.Game.World.DataBase.Entitites.Property.Business.PassengerTransportation.BusStation.Routes;
using SunCity.Game.World.DataBase.Entitites.Property.Business.StreetRacing;
using SunCity.Game.World.DataBase.Entitites.Property.Business.StreetRacing.Heats;
using SunCity.Game.World.DataBase.Entitites.Property.Business.StreetRacing.Routes;
using SunCity.Game.World.DataBase.Entitites.Property.HeadQuarter;
using SunCity.Game.World.DataBase.Entitites.Property.Owners;
using SunCity.Game.World.DataBase.Entitites.Property.Residence;
using SunCity.Game.World.DataBase.Entitites.StationPoint;
using SunCity.Game.World.DataBase.Entitites.Vehicle;

namespace SunCity.Game.World.DataBase
{
    public class GameWorldDbContext : DbContext
    {
        public const string SchemaName = "GameWorld";

        public GameWorldDbContext(DbContextOptions<GameWorldDbContext> InOptions) : base(InOptions)
        {

        }

        protected override void OnConfiguring(DbContextOptionsBuilder InOptionsBuilder)
        {
            //InOptionsBuilder.EnableSensitiveDataLogging();
            InOptionsBuilder.EnableDetailedErrors();

        }

        protected override void OnModelCreating(ModelBuilder InModelBuilder)
        {
            base.OnModelCreating(InModelBuilder);
            InModelBuilder.HasDefaultSchema(SchemaName);
            
            InModelBuilder.Entity<StreetRacingHeatEntity>().HasOne(q => q.WinnerEntity).WithMany().HasForeignKey(q => q.WinnerId);
            InModelBuilder.Entity<StreetRacingHeatEntity>().HasMany(q => q.Members).WithOne(q => q.HeatEntity).HasForeignKey(q => q.HeatId);
            
            InModelBuilder.Entity<StreetRacingRouteEntity>().HasOne(q => q.StartPointEntity).WithMany().HasForeignKey(q => q.StartPointId);
            InModelBuilder.Entity<StreetRacingRouteEntity>().HasMany(q => q.Points).WithOne(q => q.RouteEntity).HasForeignKey(q => q.RouteId);

            
            InModelBuilder.Entity<BusStationRouteEntity>().HasOne(q => q.StartPointEntity).WithMany().HasForeignKey(q => q.StartPointId);
            InModelBuilder.Entity<BusStationRouteEntity>().HasMany(q => q.Points).WithOne(q => q.RouteEntity).HasForeignKey(q => q.RouteId);


            InModelBuilder.Entity<BankCardAccountEntity>().HasIndex(q => q.Number).IsUnique();
            InModelBuilder.Entity<BankCardAccountEntity>().HasIndex(q => new {q.EntityId, q.PinCodeHash});
            InModelBuilder.Entity<BankCardAccountEntity>().HasIndex(q => q.Balance);

            //InModelBuilder.Entity<VehicleEntity>().OwnsOne(u => u.Color).WithOwner();
            //InModelBuilder.Entity<VehicleEntity>().OwnsOne(u => u.WheelsState).WithOwner();
            //InModelBuilder.Entity<VehicleEntity>().OwnsOne(u => u.WindowsState).WithOwner();
            //InModelBuilder.Entity<VehicleEntity>().OwnsOne(u => u.DoorsState).WithOwner();
            //InModelBuilder.Entity<VehicleEntity>().OwnsOne(u => u.PlateNumber).WithOwner();
        }
        
        //=================================
        public virtual DbSet<StationPointEntity> StationPointEntity { get; set; }

        //=================================
        public virtual DbSet<GameLicenseEntity> GameLicenseEntity { get; set; }
        public virtual DbSet<PlayerLicenseEntity> PlayerLicenseEntity { get; set; }
        
        //=================================
        public virtual DbSet<DrivingSchoolPropertyEntity> DrivingSchoolPropertyEntity { get; set; }
        public virtual DbSet<DrivingSchoolLicenseEntity> DrivingSchoolLicenseEntity { get; set; }
        public virtual DbSet<DrivingSchoolRouteEntity> DrivingSchoolRouteEntity { get; set; }
        public virtual DbSet<DrivingSchoolRoutePointEntity> DrivingSchoolRoutePointEntity { get; set; }

        //=================================
        public virtual DbSet<BusStationPropertyEntity> BusStationPropertyEntity { get; set; }
        public virtual DbSet<BusStationRouteEntity> BusStationRouteEntity { get; set; }
        public virtual DbSet<BusStationRoutePointEntity> BusStationRoutePointEntity { get; set; }
        
        //=================================
        public virtual DbSet<StreetRacingPropertyEntity> StreetRacingPropertyEntity { get; set; }
        public virtual DbSet<StreetRacingRouteEntity> StreetRacingRouteEntity { get; set; }
        public virtual DbSet<StreetRacingRoutePointEntity> StreetRacingRoutePointEntity { get; set; }

        //=================================
        public virtual DbSet<GameItemEntity> ItemEntity { get; set; }
        public virtual DbSet<ClothingItemEntity> ClothingItemEntity { get; set; }

        public virtual DbSet<WorldItemEntity> WorldItemEntity { get; set; }

        public virtual DbSet<VehicleEntity> VehicleEntity { get; set; }
        public virtual DbSet<VehicleModelEntity> VehicleModelEntity { get; set; }

        //=================================
        public virtual DbSet<PlayerAccountEntity> PlayerAccountEntity { get; set; }
        public virtual DbSet<PlayerCharacterEntity> PlayerCharacterEntity { get; set; }
        public virtual DbSet<PlayerCharacterInventoryItemEntity> PlayerCharacterInventoryItemEntity { get; set; }

        //=================================
        public virtual DbSet<PropertyEntity> PropertyEntity { get; set; }
        public virtual DbSet<PropertyOwnerEntity> PropertyOwnerEntity { get; set; }

        public virtual DbSet<ResidencePropertyEntity> ResidencePropertyEntity { get; set; }
        public virtual DbSet<HospitalPropertyEntity> HospitalPropertyEntity { get; set; }
        public virtual DbSet<HeadQuarterPropertyEntity> HeadQuarterPropertyEntity { get; set; }


        //=================================
        public virtual DbSet<FractionEntity> FractionEntity { get; set; }
        public virtual DbSet<FractionMemberEntity> FractionMemberEntity { get; set; }
        public virtual DbSet<FractionRankEntity> FractionRankEntity { get; set; }


        //=================================
        public virtual DbSet<InteriorEntity> InteriorEntity { get; set; }

        #region Gas station
        public virtual DbSet<GasStationPropertyEntity> GasStationPropertyEntity { get; set; }
        public virtual DbSet<GasStationPointEntity> GasStationPointEntity { get; set; }

        #endregion


        #region Car / Moto / Boat Showroom
        public virtual DbSet<CarShowroomPropertyEntity> CarShowroomPropertyEntity { get; set; }
        public virtual DbSet<CarShowroomVehicleEntity> CarShowroomVehicleEntity { get; set; }
        public virtual DbSet<CarShowroomVehicleSpawnPointEntity> CarShowroomVehicleSpawnPointEntity { get; set; }
        #endregion

        #region Bank
        public virtual DbSet<BankCardAccountEntity> BankCardAccountEntity { get; set; }
        public virtual DbSet<BankCardBusinessAccountEntity> BankCardBusinessAccountEntity { get; set; }

        public virtual DbSet<BankCardPlayerAccountEntity> BankCardPlayerAccountEntity { get; set; }
        public virtual DbSet<BankCardFractionAccountEntity> BankCardFractionAccountEntity { get; set; }

        public virtual DbSet<BankCardTariffEntity> BankCardTariffEntity { get; set; }
        public virtual DbSet<BankCardTransactionEntity> BankCardTransactionEntity { get; set; }
        #endregion



    }
}
