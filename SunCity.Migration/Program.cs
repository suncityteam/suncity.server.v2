﻿using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using SunCity.Common;
using SunCity.Game.World.DataBase;

namespace SunCity.Migration
{
    public static class Program
    {
        public static void Main(string[] InArgs)
        {
            WebServerStartup.Run(Array.Empty<string>());
        }

        // EF Core uses this method at design time to access the DbContext
        public static IHostBuilder CreateHostBuilder(string[] InArgs)
        {
            Console.WriteLine("CreateHostBuilder");
            return Host.CreateDefaultBuilder(InArgs)
                .ConfigureWebHostDefaults(Configure);
        }

        private static void Configure(IWebHostBuilder webHostBuilder)
        {
            webHostBuilder.UseStartup<WebServerStartup>();
            webHostBuilder.UseEnvironment(EnvironmentNames.ENTITY_FRAMEWORK_MIGRATION);
        }
    }
}