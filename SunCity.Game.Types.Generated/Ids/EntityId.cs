﻿using System;
using System.Collections.Generic;
using System.Text;
using SunCity.Domain.Interfaces.Types;



public interface IEntityWithPlayerCharacterId
{
     PlayerCharacterId  PlayerCharacterId { get; }
}

public readonly struct PlayerCharacterId : IGuidId<PlayerCharacterId>
{
    public Guid Id { get; }

    public PlayerCharacterId(Guid InId)
    {
        Id = InId;
    }

	public static PlayerCharacterId GenerateId() => new PlayerCharacterId(Guid.NewGuid());
    public static PlayerCharacterId Parse(string InStringGuid)
    {
        return new PlayerCharacterId(Guid.Parse(InStringGuid));
    }

    public static bool operator ==(PlayerCharacterId InLhs, PlayerCharacterId InRhs) => InLhs.Equals(InRhs);
    public static bool operator !=(PlayerCharacterId InLhs, PlayerCharacterId InRhs) => !InLhs.Equals(InRhs);
    public bool Equals(PlayerCharacterId InOther) => Id.Equals(InOther.Id);
    public override bool Equals(object? InObj) => InObj is PlayerCharacterId other && Equals(other);
    public int CompareTo(PlayerCharacterId InOther) => Id.CompareTo(InOther.Id);
    public string ToString(string? InFormat, IFormatProvider? InFormatProvider) => Id.ToString(InFormat, InFormatProvider);

    public override int GetHashCode() => Id.GetHashCode();
    public override string ToString() => Id.ToString();

    public bool IsValid => Id != Guid.Empty;
    public bool IsNotValid => !IsValid;
}

public interface IEntityWithPlayerLicenseId
{
     PlayerLicenseId  PlayerLicenseId { get; }
}

public readonly struct PlayerLicenseId : IGuidId<PlayerLicenseId>
{
    public Guid Id { get; }

    public PlayerLicenseId(Guid InId)
    {
        Id = InId;
    }

	public static PlayerLicenseId GenerateId() => new PlayerLicenseId(Guid.NewGuid());
    public static PlayerLicenseId Parse(string InStringGuid)
    {
        return new PlayerLicenseId(Guid.Parse(InStringGuid));
    }

    public static bool operator ==(PlayerLicenseId InLhs, PlayerLicenseId InRhs) => InLhs.Equals(InRhs);
    public static bool operator !=(PlayerLicenseId InLhs, PlayerLicenseId InRhs) => !InLhs.Equals(InRhs);
    public bool Equals(PlayerLicenseId InOther) => Id.Equals(InOther.Id);
    public override bool Equals(object? InObj) => InObj is PlayerLicenseId other && Equals(other);
    public int CompareTo(PlayerLicenseId InOther) => Id.CompareTo(InOther.Id);
    public string ToString(string? InFormat, IFormatProvider? InFormatProvider) => Id.ToString(InFormat, InFormatProvider);

    public override int GetHashCode() => Id.GetHashCode();
    public override string ToString() => Id.ToString();

    public bool IsValid => Id != Guid.Empty;
    public bool IsNotValid => !IsValid;
}

public interface IEntityWithVehicleId
{
     VehicleId  VehicleId { get; }
}

public readonly struct VehicleId : IGuidId<VehicleId>
{
    public Guid Id { get; }

    public VehicleId(Guid InId)
    {
        Id = InId;
    }

	public static VehicleId GenerateId() => new VehicleId(Guid.NewGuid());
    public static VehicleId Parse(string InStringGuid)
    {
        return new VehicleId(Guid.Parse(InStringGuid));
    }

    public static bool operator ==(VehicleId InLhs, VehicleId InRhs) => InLhs.Equals(InRhs);
    public static bool operator !=(VehicleId InLhs, VehicleId InRhs) => !InLhs.Equals(InRhs);
    public bool Equals(VehicleId InOther) => Id.Equals(InOther.Id);
    public override bool Equals(object? InObj) => InObj is VehicleId other && Equals(other);
    public int CompareTo(VehicleId InOther) => Id.CompareTo(InOther.Id);
    public string ToString(string? InFormat, IFormatProvider? InFormatProvider) => Id.ToString(InFormat, InFormatProvider);

    public override int GetHashCode() => Id.GetHashCode();
    public override string ToString() => Id.ToString();

    public bool IsValid => Id != Guid.Empty;
    public bool IsNotValid => !IsValid;
}

public interface IEntityWithVehicleModId
{
     VehicleModId  VehicleModId { get; }
}

public readonly struct VehicleModId : IGuidId<VehicleModId>
{
    public Guid Id { get; }

    public VehicleModId(Guid InId)
    {
        Id = InId;
    }

	public static VehicleModId GenerateId() => new VehicleModId(Guid.NewGuid());
    public static VehicleModId Parse(string InStringGuid)
    {
        return new VehicleModId(Guid.Parse(InStringGuid));
    }

    public static bool operator ==(VehicleModId InLhs, VehicleModId InRhs) => InLhs.Equals(InRhs);
    public static bool operator !=(VehicleModId InLhs, VehicleModId InRhs) => !InLhs.Equals(InRhs);
    public bool Equals(VehicleModId InOther) => Id.Equals(InOther.Id);
    public override bool Equals(object? InObj) => InObj is VehicleModId other && Equals(other);
    public int CompareTo(VehicleModId InOther) => Id.CompareTo(InOther.Id);
    public string ToString(string? InFormat, IFormatProvider? InFormatProvider) => Id.ToString(InFormat, InFormatProvider);

    public override int GetHashCode() => Id.GetHashCode();
    public override string ToString() => Id.ToString();

    public bool IsValid => Id != Guid.Empty;
    public bool IsNotValid => !IsValid;
}

public interface IEntityWithGameItemId
{
     GameItemId  GameItemId { get; }
}

public readonly struct GameItemId : IGuidId<GameItemId>
{
    public Guid Id { get; }

    public GameItemId(Guid InId)
    {
        Id = InId;
    }

	public static GameItemId GenerateId() => new GameItemId(Guid.NewGuid());
    public static GameItemId Parse(string InStringGuid)
    {
        return new GameItemId(Guid.Parse(InStringGuid));
    }

    public static bool operator ==(GameItemId InLhs, GameItemId InRhs) => InLhs.Equals(InRhs);
    public static bool operator !=(GameItemId InLhs, GameItemId InRhs) => !InLhs.Equals(InRhs);
    public bool Equals(GameItemId InOther) => Id.Equals(InOther.Id);
    public override bool Equals(object? InObj) => InObj is GameItemId other && Equals(other);
    public int CompareTo(GameItemId InOther) => Id.CompareTo(InOther.Id);
    public string ToString(string? InFormat, IFormatProvider? InFormatProvider) => Id.ToString(InFormat, InFormatProvider);

    public override int GetHashCode() => Id.GetHashCode();
    public override string ToString() => Id.ToString();

    public bool IsValid => Id != Guid.Empty;
    public bool IsNotValid => !IsValid;
}

public interface IEntityWithWorldItemId
{
     WorldItemId  WorldItemId { get; }
}

public readonly struct WorldItemId : IGuidId<WorldItemId>
{
    public Guid Id { get; }

    public WorldItemId(Guid InId)
    {
        Id = InId;
    }

	public static WorldItemId GenerateId() => new WorldItemId(Guid.NewGuid());
    public static WorldItemId Parse(string InStringGuid)
    {
        return new WorldItemId(Guid.Parse(InStringGuid));
    }

    public static bool operator ==(WorldItemId InLhs, WorldItemId InRhs) => InLhs.Equals(InRhs);
    public static bool operator !=(WorldItemId InLhs, WorldItemId InRhs) => !InLhs.Equals(InRhs);
    public bool Equals(WorldItemId InOther) => Id.Equals(InOther.Id);
    public override bool Equals(object? InObj) => InObj is WorldItemId other && Equals(other);
    public int CompareTo(WorldItemId InOther) => Id.CompareTo(InOther.Id);
    public string ToString(string? InFormat, IFormatProvider? InFormatProvider) => Id.ToString(InFormat, InFormatProvider);

    public override int GetHashCode() => Id.GetHashCode();
    public override string ToString() => Id.ToString();

    public bool IsValid => Id != Guid.Empty;
    public bool IsNotValid => !IsValid;
}

public interface IEntityWithPropertyId
{
     PropertyId  PropertyId { get; }
}

public readonly struct PropertyId : IGuidId<PropertyId>
{
    public Guid Id { get; }

    public PropertyId(Guid InId)
    {
        Id = InId;
    }

	public static PropertyId GenerateId() => new PropertyId(Guid.NewGuid());
    public static PropertyId Parse(string InStringGuid)
    {
        return new PropertyId(Guid.Parse(InStringGuid));
    }

    public static bool operator ==(PropertyId InLhs, PropertyId InRhs) => InLhs.Equals(InRhs);
    public static bool operator !=(PropertyId InLhs, PropertyId InRhs) => !InLhs.Equals(InRhs);
    public bool Equals(PropertyId InOther) => Id.Equals(InOther.Id);
    public override bool Equals(object? InObj) => InObj is PropertyId other && Equals(other);
    public int CompareTo(PropertyId InOther) => Id.CompareTo(InOther.Id);
    public string ToString(string? InFormat, IFormatProvider? InFormatProvider) => Id.ToString(InFormat, InFormatProvider);

    public override int GetHashCode() => Id.GetHashCode();
    public override string ToString() => Id.ToString();

    public bool IsValid => Id != Guid.Empty;
    public bool IsNotValid => !IsValid;
}

public interface IEntityWithPropertyOwnerId
{
     PropertyOwnerId  PropertyOwnerId { get; }
}

public readonly struct PropertyOwnerId : IGuidId<PropertyOwnerId>
{
    public Guid Id { get; }

    public PropertyOwnerId(Guid InId)
    {
        Id = InId;
    }

	public static PropertyOwnerId GenerateId() => new PropertyOwnerId(Guid.NewGuid());
    public static PropertyOwnerId Parse(string InStringGuid)
    {
        return new PropertyOwnerId(Guid.Parse(InStringGuid));
    }

    public static bool operator ==(PropertyOwnerId InLhs, PropertyOwnerId InRhs) => InLhs.Equals(InRhs);
    public static bool operator !=(PropertyOwnerId InLhs, PropertyOwnerId InRhs) => !InLhs.Equals(InRhs);
    public bool Equals(PropertyOwnerId InOther) => Id.Equals(InOther.Id);
    public override bool Equals(object? InObj) => InObj is PropertyOwnerId other && Equals(other);
    public int CompareTo(PropertyOwnerId InOther) => Id.CompareTo(InOther.Id);
    public string ToString(string? InFormat, IFormatProvider? InFormatProvider) => Id.ToString(InFormat, InFormatProvider);

    public override int GetHashCode() => Id.GetHashCode();
    public override string ToString() => Id.ToString();

    public bool IsValid => Id != Guid.Empty;
    public bool IsNotValid => !IsValid;
}

public interface IEntityWithInteriorId
{
     InteriorId  InteriorId { get; }
}

public readonly struct InteriorId : IGuidId<InteriorId>
{
    public Guid Id { get; }

    public InteriorId(Guid InId)
    {
        Id = InId;
    }

	public static InteriorId GenerateId() => new InteriorId(Guid.NewGuid());
    public static InteriorId Parse(string InStringGuid)
    {
        return new InteriorId(Guid.Parse(InStringGuid));
    }

    public static bool operator ==(InteriorId InLhs, InteriorId InRhs) => InLhs.Equals(InRhs);
    public static bool operator !=(InteriorId InLhs, InteriorId InRhs) => !InLhs.Equals(InRhs);
    public bool Equals(InteriorId InOther) => Id.Equals(InOther.Id);
    public override bool Equals(object? InObj) => InObj is InteriorId other && Equals(other);
    public int CompareTo(InteriorId InOther) => Id.CompareTo(InOther.Id);
    public string ToString(string? InFormat, IFormatProvider? InFormatProvider) => Id.ToString(InFormat, InFormatProvider);

    public override int GetHashCode() => Id.GetHashCode();
    public override string ToString() => Id.ToString();

    public bool IsValid => Id != Guid.Empty;
    public bool IsNotValid => !IsValid;
}

public interface IEntityWithInteriorActionId
{
     InteriorActionId  InteriorActionId { get; }
}

public readonly struct InteriorActionId : IGuidId<InteriorActionId>
{
    public Guid Id { get; }

    public InteriorActionId(Guid InId)
    {
        Id = InId;
    }

	public static InteriorActionId GenerateId() => new InteriorActionId(Guid.NewGuid());
    public static InteriorActionId Parse(string InStringGuid)
    {
        return new InteriorActionId(Guid.Parse(InStringGuid));
    }

    public static bool operator ==(InteriorActionId InLhs, InteriorActionId InRhs) => InLhs.Equals(InRhs);
    public static bool operator !=(InteriorActionId InLhs, InteriorActionId InRhs) => !InLhs.Equals(InRhs);
    public bool Equals(InteriorActionId InOther) => Id.Equals(InOther.Id);
    public override bool Equals(object? InObj) => InObj is InteriorActionId other && Equals(other);
    public int CompareTo(InteriorActionId InOther) => Id.CompareTo(InOther.Id);
    public string ToString(string? InFormat, IFormatProvider? InFormatProvider) => Id.ToString(InFormat, InFormatProvider);

    public override int GetHashCode() => Id.GetHashCode();
    public override string ToString() => Id.ToString();

    public bool IsValid => Id != Guid.Empty;
    public bool IsNotValid => !IsValid;
}

public interface IEntityWithPropertyRequiredLicenseId
{
     PropertyRequiredLicenseId  PropertyRequiredLicenseId { get; }
}

public readonly struct PropertyRequiredLicenseId : IGuidId<PropertyRequiredLicenseId>
{
    public Guid Id { get; }

    public PropertyRequiredLicenseId(Guid InId)
    {
        Id = InId;
    }

	public static PropertyRequiredLicenseId GenerateId() => new PropertyRequiredLicenseId(Guid.NewGuid());
    public static PropertyRequiredLicenseId Parse(string InStringGuid)
    {
        return new PropertyRequiredLicenseId(Guid.Parse(InStringGuid));
    }

    public static bool operator ==(PropertyRequiredLicenseId InLhs, PropertyRequiredLicenseId InRhs) => InLhs.Equals(InRhs);
    public static bool operator !=(PropertyRequiredLicenseId InLhs, PropertyRequiredLicenseId InRhs) => !InLhs.Equals(InRhs);
    public bool Equals(PropertyRequiredLicenseId InOther) => Id.Equals(InOther.Id);
    public override bool Equals(object? InObj) => InObj is PropertyRequiredLicenseId other && Equals(other);
    public int CompareTo(PropertyRequiredLicenseId InOther) => Id.CompareTo(InOther.Id);
    public string ToString(string? InFormat, IFormatProvider? InFormatProvider) => Id.ToString(InFormat, InFormatProvider);

    public override int GetHashCode() => Id.GetHashCode();
    public override string ToString() => Id.ToString();

    public bool IsValid => Id != Guid.Empty;
    public bool IsNotValid => !IsValid;
}

public interface IEntityWithBankATMId
{
     BankATMId  BankATMId { get; }
}

public readonly struct BankATMId : IGuidId<BankATMId>
{
    public Guid Id { get; }

    public BankATMId(Guid InId)
    {
        Id = InId;
    }

	public static BankATMId GenerateId() => new BankATMId(Guid.NewGuid());
    public static BankATMId Parse(string InStringGuid)
    {
        return new BankATMId(Guid.Parse(InStringGuid));
    }

    public static bool operator ==(BankATMId InLhs, BankATMId InRhs) => InLhs.Equals(InRhs);
    public static bool operator !=(BankATMId InLhs, BankATMId InRhs) => !InLhs.Equals(InRhs);
    public bool Equals(BankATMId InOther) => Id.Equals(InOther.Id);
    public override bool Equals(object? InObj) => InObj is BankATMId other && Equals(other);
    public int CompareTo(BankATMId InOther) => Id.CompareTo(InOther.Id);
    public string ToString(string? InFormat, IFormatProvider? InFormatProvider) => Id.ToString(InFormat, InFormatProvider);

    public override int GetHashCode() => Id.GetHashCode();
    public override string ToString() => Id.ToString();

    public bool IsValid => Id != Guid.Empty;
    public bool IsNotValid => !IsValid;
}

public interface IEntityWithBankCardId
{
     BankCardId  BankCardId { get; }
}

public readonly struct BankCardId : IGuidId<BankCardId>
{
    public Guid Id { get; }

    public BankCardId(Guid InId)
    {
        Id = InId;
    }

	public static BankCardId GenerateId() => new BankCardId(Guid.NewGuid());
    public static BankCardId Parse(string InStringGuid)
    {
        return new BankCardId(Guid.Parse(InStringGuid));
    }

    public static bool operator ==(BankCardId InLhs, BankCardId InRhs) => InLhs.Equals(InRhs);
    public static bool operator !=(BankCardId InLhs, BankCardId InRhs) => !InLhs.Equals(InRhs);
    public bool Equals(BankCardId InOther) => Id.Equals(InOther.Id);
    public override bool Equals(object? InObj) => InObj is BankCardId other && Equals(other);
    public int CompareTo(BankCardId InOther) => Id.CompareTo(InOther.Id);
    public string ToString(string? InFormat, IFormatProvider? InFormatProvider) => Id.ToString(InFormat, InFormatProvider);

    public override int GetHashCode() => Id.GetHashCode();
    public override string ToString() => Id.ToString();

    public bool IsValid => Id != Guid.Empty;
    public bool IsNotValid => !IsValid;
}

public interface IEntityWithBankCardTariffId
{
     BankCardTariffId  BankCardTariffId { get; }
}

public readonly struct BankCardTariffId : IGuidId<BankCardTariffId>
{
    public Guid Id { get; }

    public BankCardTariffId(Guid InId)
    {
        Id = InId;
    }

	public static BankCardTariffId GenerateId() => new BankCardTariffId(Guid.NewGuid());
    public static BankCardTariffId Parse(string InStringGuid)
    {
        return new BankCardTariffId(Guid.Parse(InStringGuid));
    }

    public static bool operator ==(BankCardTariffId InLhs, BankCardTariffId InRhs) => InLhs.Equals(InRhs);
    public static bool operator !=(BankCardTariffId InLhs, BankCardTariffId InRhs) => !InLhs.Equals(InRhs);
    public bool Equals(BankCardTariffId InOther) => Id.Equals(InOther.Id);
    public override bool Equals(object? InObj) => InObj is BankCardTariffId other && Equals(other);
    public int CompareTo(BankCardTariffId InOther) => Id.CompareTo(InOther.Id);
    public string ToString(string? InFormat, IFormatProvider? InFormatProvider) => Id.ToString(InFormat, InFormatProvider);

    public override int GetHashCode() => Id.GetHashCode();
    public override string ToString() => Id.ToString();

    public bool IsValid => Id != Guid.Empty;
    public bool IsNotValid => !IsValid;
}

public interface IEntityWithBankCardTransactionId
{
     BankCardTransactionId  BankCardTransactionId { get; }
}

public readonly struct BankCardTransactionId : IGuidId<BankCardTransactionId>
{
    public Guid Id { get; }

    public BankCardTransactionId(Guid InId)
    {
        Id = InId;
    }

	public static BankCardTransactionId GenerateId() => new BankCardTransactionId(Guid.NewGuid());
    public static BankCardTransactionId Parse(string InStringGuid)
    {
        return new BankCardTransactionId(Guid.Parse(InStringGuid));
    }

    public static bool operator ==(BankCardTransactionId InLhs, BankCardTransactionId InRhs) => InLhs.Equals(InRhs);
    public static bool operator !=(BankCardTransactionId InLhs, BankCardTransactionId InRhs) => !InLhs.Equals(InRhs);
    public bool Equals(BankCardTransactionId InOther) => Id.Equals(InOther.Id);
    public override bool Equals(object? InObj) => InObj is BankCardTransactionId other && Equals(other);
    public int CompareTo(BankCardTransactionId InOther) => Id.CompareTo(InOther.Id);
    public string ToString(string? InFormat, IFormatProvider? InFormatProvider) => Id.ToString(InFormat, InFormatProvider);

    public override int GetHashCode() => Id.GetHashCode();
    public override string ToString() => Id.ToString();

    public bool IsValid => Id != Guid.Empty;
    public bool IsNotValid => !IsValid;
}

public interface IEntityWithDrivingSchoolLicenseId
{
     DrivingSchoolLicenseId  DrivingSchoolLicenseId { get; }
}

public readonly struct DrivingSchoolLicenseId : IGuidId<DrivingSchoolLicenseId>
{
    public Guid Id { get; }

    public DrivingSchoolLicenseId(Guid InId)
    {
        Id = InId;
    }

	public static DrivingSchoolLicenseId GenerateId() => new DrivingSchoolLicenseId(Guid.NewGuid());
    public static DrivingSchoolLicenseId Parse(string InStringGuid)
    {
        return new DrivingSchoolLicenseId(Guid.Parse(InStringGuid));
    }

    public static bool operator ==(DrivingSchoolLicenseId InLhs, DrivingSchoolLicenseId InRhs) => InLhs.Equals(InRhs);
    public static bool operator !=(DrivingSchoolLicenseId InLhs, DrivingSchoolLicenseId InRhs) => !InLhs.Equals(InRhs);
    public bool Equals(DrivingSchoolLicenseId InOther) => Id.Equals(InOther.Id);
    public override bool Equals(object? InObj) => InObj is DrivingSchoolLicenseId other && Equals(other);
    public int CompareTo(DrivingSchoolLicenseId InOther) => Id.CompareTo(InOther.Id);
    public string ToString(string? InFormat, IFormatProvider? InFormatProvider) => Id.ToString(InFormat, InFormatProvider);

    public override int GetHashCode() => Id.GetHashCode();
    public override string ToString() => Id.ToString();

    public bool IsValid => Id != Guid.Empty;
    public bool IsNotValid => !IsValid;
}

public interface IEntityWithDrivingSchoolRouteId
{
     DrivingSchoolRouteId  DrivingSchoolRouteId { get; }
}

public readonly struct DrivingSchoolRouteId : IGuidId<DrivingSchoolRouteId>
{
    public Guid Id { get; }

    public DrivingSchoolRouteId(Guid InId)
    {
        Id = InId;
    }

	public static DrivingSchoolRouteId GenerateId() => new DrivingSchoolRouteId(Guid.NewGuid());
    public static DrivingSchoolRouteId Parse(string InStringGuid)
    {
        return new DrivingSchoolRouteId(Guid.Parse(InStringGuid));
    }

    public static bool operator ==(DrivingSchoolRouteId InLhs, DrivingSchoolRouteId InRhs) => InLhs.Equals(InRhs);
    public static bool operator !=(DrivingSchoolRouteId InLhs, DrivingSchoolRouteId InRhs) => !InLhs.Equals(InRhs);
    public bool Equals(DrivingSchoolRouteId InOther) => Id.Equals(InOther.Id);
    public override bool Equals(object? InObj) => InObj is DrivingSchoolRouteId other && Equals(other);
    public int CompareTo(DrivingSchoolRouteId InOther) => Id.CompareTo(InOther.Id);
    public string ToString(string? InFormat, IFormatProvider? InFormatProvider) => Id.ToString(InFormat, InFormatProvider);

    public override int GetHashCode() => Id.GetHashCode();
    public override string ToString() => Id.ToString();

    public bool IsValid => Id != Guid.Empty;
    public bool IsNotValid => !IsValid;
}

public interface IEntityWithDrivingSchoolRoutePointId
{
     DrivingSchoolRoutePointId  DrivingSchoolRoutePointId { get; }
}

public readonly struct DrivingSchoolRoutePointId : IGuidId<DrivingSchoolRoutePointId>
{
    public Guid Id { get; }

    public DrivingSchoolRoutePointId(Guid InId)
    {
        Id = InId;
    }

	public static DrivingSchoolRoutePointId GenerateId() => new DrivingSchoolRoutePointId(Guid.NewGuid());
    public static DrivingSchoolRoutePointId Parse(string InStringGuid)
    {
        return new DrivingSchoolRoutePointId(Guid.Parse(InStringGuid));
    }

    public static bool operator ==(DrivingSchoolRoutePointId InLhs, DrivingSchoolRoutePointId InRhs) => InLhs.Equals(InRhs);
    public static bool operator !=(DrivingSchoolRoutePointId InLhs, DrivingSchoolRoutePointId InRhs) => !InLhs.Equals(InRhs);
    public bool Equals(DrivingSchoolRoutePointId InOther) => Id.Equals(InOther.Id);
    public override bool Equals(object? InObj) => InObj is DrivingSchoolRoutePointId other && Equals(other);
    public int CompareTo(DrivingSchoolRoutePointId InOther) => Id.CompareTo(InOther.Id);
    public string ToString(string? InFormat, IFormatProvider? InFormatProvider) => Id.ToString(InFormat, InFormatProvider);

    public override int GetHashCode() => Id.GetHashCode();
    public override string ToString() => Id.ToString();

    public bool IsValid => Id != Guid.Empty;
    public bool IsNotValid => !IsValid;
}

public interface IEntityWithBusStationRouteId
{
     BusStationRouteId  BusStationRouteId { get; }
}

public readonly struct BusStationRouteId : IGuidId<BusStationRouteId>
{
    public Guid Id { get; }

    public BusStationRouteId(Guid InId)
    {
        Id = InId;
    }

	public static BusStationRouteId GenerateId() => new BusStationRouteId(Guid.NewGuid());
    public static BusStationRouteId Parse(string InStringGuid)
    {
        return new BusStationRouteId(Guid.Parse(InStringGuid));
    }

    public static bool operator ==(BusStationRouteId InLhs, BusStationRouteId InRhs) => InLhs.Equals(InRhs);
    public static bool operator !=(BusStationRouteId InLhs, BusStationRouteId InRhs) => !InLhs.Equals(InRhs);
    public bool Equals(BusStationRouteId InOther) => Id.Equals(InOther.Id);
    public override bool Equals(object? InObj) => InObj is BusStationRouteId other && Equals(other);
    public int CompareTo(BusStationRouteId InOther) => Id.CompareTo(InOther.Id);
    public string ToString(string? InFormat, IFormatProvider? InFormatProvider) => Id.ToString(InFormat, InFormatProvider);

    public override int GetHashCode() => Id.GetHashCode();
    public override string ToString() => Id.ToString();

    public bool IsValid => Id != Guid.Empty;
    public bool IsNotValid => !IsValid;
}

public interface IEntityWithBusStationRoutePointId
{
     BusStationRoutePointId  BusStationRoutePointId { get; }
}

public readonly struct BusStationRoutePointId : IGuidId<BusStationRoutePointId>
{
    public Guid Id { get; }

    public BusStationRoutePointId(Guid InId)
    {
        Id = InId;
    }

	public static BusStationRoutePointId GenerateId() => new BusStationRoutePointId(Guid.NewGuid());
    public static BusStationRoutePointId Parse(string InStringGuid)
    {
        return new BusStationRoutePointId(Guid.Parse(InStringGuid));
    }

    public static bool operator ==(BusStationRoutePointId InLhs, BusStationRoutePointId InRhs) => InLhs.Equals(InRhs);
    public static bool operator !=(BusStationRoutePointId InLhs, BusStationRoutePointId InRhs) => !InLhs.Equals(InRhs);
    public bool Equals(BusStationRoutePointId InOther) => Id.Equals(InOther.Id);
    public override bool Equals(object? InObj) => InObj is BusStationRoutePointId other && Equals(other);
    public int CompareTo(BusStationRoutePointId InOther) => Id.CompareTo(InOther.Id);
    public string ToString(string? InFormat, IFormatProvider? InFormatProvider) => Id.ToString(InFormat, InFormatProvider);

    public override int GetHashCode() => Id.GetHashCode();
    public override string ToString() => Id.ToString();

    public bool IsValid => Id != Guid.Empty;
    public bool IsNotValid => !IsValid;
}

public interface IEntityWithBusStationFlightId
{
     BusStationFlightId  BusStationFlightId { get; }
}

public readonly struct BusStationFlightId : IGuidId<BusStationFlightId>
{
    public Guid Id { get; }

    public BusStationFlightId(Guid InId)
    {
        Id = InId;
    }

	public static BusStationFlightId GenerateId() => new BusStationFlightId(Guid.NewGuid());
    public static BusStationFlightId Parse(string InStringGuid)
    {
        return new BusStationFlightId(Guid.Parse(InStringGuid));
    }

    public static bool operator ==(BusStationFlightId InLhs, BusStationFlightId InRhs) => InLhs.Equals(InRhs);
    public static bool operator !=(BusStationFlightId InLhs, BusStationFlightId InRhs) => !InLhs.Equals(InRhs);
    public bool Equals(BusStationFlightId InOther) => Id.Equals(InOther.Id);
    public override bool Equals(object? InObj) => InObj is BusStationFlightId other && Equals(other);
    public int CompareTo(BusStationFlightId InOther) => Id.CompareTo(InOther.Id);
    public string ToString(string? InFormat, IFormatProvider? InFormatProvider) => Id.ToString(InFormat, InFormatProvider);

    public override int GetHashCode() => Id.GetHashCode();
    public override string ToString() => Id.ToString();

    public bool IsValid => Id != Guid.Empty;
    public bool IsNotValid => !IsValid;
}

public interface IEntityWithBusStationFlightMemberId
{
     BusStationFlightMemberId  BusStationFlightMemberId { get; }
}

public readonly struct BusStationFlightMemberId : IGuidId<BusStationFlightMemberId>
{
    public Guid Id { get; }

    public BusStationFlightMemberId(Guid InId)
    {
        Id = InId;
    }

	public static BusStationFlightMemberId GenerateId() => new BusStationFlightMemberId(Guid.NewGuid());
    public static BusStationFlightMemberId Parse(string InStringGuid)
    {
        return new BusStationFlightMemberId(Guid.Parse(InStringGuid));
    }

    public static bool operator ==(BusStationFlightMemberId InLhs, BusStationFlightMemberId InRhs) => InLhs.Equals(InRhs);
    public static bool operator !=(BusStationFlightMemberId InLhs, BusStationFlightMemberId InRhs) => !InLhs.Equals(InRhs);
    public bool Equals(BusStationFlightMemberId InOther) => Id.Equals(InOther.Id);
    public override bool Equals(object? InObj) => InObj is BusStationFlightMemberId other && Equals(other);
    public int CompareTo(BusStationFlightMemberId InOther) => Id.CompareTo(InOther.Id);
    public string ToString(string? InFormat, IFormatProvider? InFormatProvider) => Id.ToString(InFormat, InFormatProvider);

    public override int GetHashCode() => Id.GetHashCode();
    public override string ToString() => Id.ToString();

    public bool IsValid => Id != Guid.Empty;
    public bool IsNotValid => !IsValid;
}

public interface IEntityWithStreetRacingRouteId
{
     StreetRacingRouteId  StreetRacingRouteId { get; }
}

public readonly struct StreetRacingRouteId : IGuidId<StreetRacingRouteId>
{
    public Guid Id { get; }

    public StreetRacingRouteId(Guid InId)
    {
        Id = InId;
    }

	public static StreetRacingRouteId GenerateId() => new StreetRacingRouteId(Guid.NewGuid());
    public static StreetRacingRouteId Parse(string InStringGuid)
    {
        return new StreetRacingRouteId(Guid.Parse(InStringGuid));
    }

    public static bool operator ==(StreetRacingRouteId InLhs, StreetRacingRouteId InRhs) => InLhs.Equals(InRhs);
    public static bool operator !=(StreetRacingRouteId InLhs, StreetRacingRouteId InRhs) => !InLhs.Equals(InRhs);
    public bool Equals(StreetRacingRouteId InOther) => Id.Equals(InOther.Id);
    public override bool Equals(object? InObj) => InObj is StreetRacingRouteId other && Equals(other);
    public int CompareTo(StreetRacingRouteId InOther) => Id.CompareTo(InOther.Id);
    public string ToString(string? InFormat, IFormatProvider? InFormatProvider) => Id.ToString(InFormat, InFormatProvider);

    public override int GetHashCode() => Id.GetHashCode();
    public override string ToString() => Id.ToString();

    public bool IsValid => Id != Guid.Empty;
    public bool IsNotValid => !IsValid;
}

public interface IEntityWithStreetRacingRoutePointId
{
     StreetRacingRoutePointId  StreetRacingRoutePointId { get; }
}

public readonly struct StreetRacingRoutePointId : IGuidId<StreetRacingRoutePointId>
{
    public Guid Id { get; }

    public StreetRacingRoutePointId(Guid InId)
    {
        Id = InId;
    }

	public static StreetRacingRoutePointId GenerateId() => new StreetRacingRoutePointId(Guid.NewGuid());
    public static StreetRacingRoutePointId Parse(string InStringGuid)
    {
        return new StreetRacingRoutePointId(Guid.Parse(InStringGuid));
    }

    public static bool operator ==(StreetRacingRoutePointId InLhs, StreetRacingRoutePointId InRhs) => InLhs.Equals(InRhs);
    public static bool operator !=(StreetRacingRoutePointId InLhs, StreetRacingRoutePointId InRhs) => !InLhs.Equals(InRhs);
    public bool Equals(StreetRacingRoutePointId InOther) => Id.Equals(InOther.Id);
    public override bool Equals(object? InObj) => InObj is StreetRacingRoutePointId other && Equals(other);
    public int CompareTo(StreetRacingRoutePointId InOther) => Id.CompareTo(InOther.Id);
    public string ToString(string? InFormat, IFormatProvider? InFormatProvider) => Id.ToString(InFormat, InFormatProvider);

    public override int GetHashCode() => Id.GetHashCode();
    public override string ToString() => Id.ToString();

    public bool IsValid => Id != Guid.Empty;
    public bool IsNotValid => !IsValid;
}

public interface IEntityWithStreetRacingHeatId
{
     StreetRacingHeatId  StreetRacingHeatId { get; }
}

public readonly struct StreetRacingHeatId : IGuidId<StreetRacingHeatId>
{
    public Guid Id { get; }

    public StreetRacingHeatId(Guid InId)
    {
        Id = InId;
    }

	public static StreetRacingHeatId GenerateId() => new StreetRacingHeatId(Guid.NewGuid());
    public static StreetRacingHeatId Parse(string InStringGuid)
    {
        return new StreetRacingHeatId(Guid.Parse(InStringGuid));
    }

    public static bool operator ==(StreetRacingHeatId InLhs, StreetRacingHeatId InRhs) => InLhs.Equals(InRhs);
    public static bool operator !=(StreetRacingHeatId InLhs, StreetRacingHeatId InRhs) => !InLhs.Equals(InRhs);
    public bool Equals(StreetRacingHeatId InOther) => Id.Equals(InOther.Id);
    public override bool Equals(object? InObj) => InObj is StreetRacingHeatId other && Equals(other);
    public int CompareTo(StreetRacingHeatId InOther) => Id.CompareTo(InOther.Id);
    public string ToString(string? InFormat, IFormatProvider? InFormatProvider) => Id.ToString(InFormat, InFormatProvider);

    public override int GetHashCode() => Id.GetHashCode();
    public override string ToString() => Id.ToString();

    public bool IsValid => Id != Guid.Empty;
    public bool IsNotValid => !IsValid;
}

public interface IEntityWithStreetRacingHeatMemberId
{
     StreetRacingHeatMemberId  StreetRacingHeatMemberId { get; }
}

public readonly struct StreetRacingHeatMemberId : IGuidId<StreetRacingHeatMemberId>
{
    public Guid Id { get; }

    public StreetRacingHeatMemberId(Guid InId)
    {
        Id = InId;
    }

	public static StreetRacingHeatMemberId GenerateId() => new StreetRacingHeatMemberId(Guid.NewGuid());
    public static StreetRacingHeatMemberId Parse(string InStringGuid)
    {
        return new StreetRacingHeatMemberId(Guid.Parse(InStringGuid));
    }

    public static bool operator ==(StreetRacingHeatMemberId InLhs, StreetRacingHeatMemberId InRhs) => InLhs.Equals(InRhs);
    public static bool operator !=(StreetRacingHeatMemberId InLhs, StreetRacingHeatMemberId InRhs) => !InLhs.Equals(InRhs);
    public bool Equals(StreetRacingHeatMemberId InOther) => Id.Equals(InOther.Id);
    public override bool Equals(object? InObj) => InObj is StreetRacingHeatMemberId other && Equals(other);
    public int CompareTo(StreetRacingHeatMemberId InOther) => Id.CompareTo(InOther.Id);
    public string ToString(string? InFormat, IFormatProvider? InFormatProvider) => Id.ToString(InFormat, InFormatProvider);

    public override int GetHashCode() => Id.GetHashCode();
    public override string ToString() => Id.ToString();

    public bool IsValid => Id != Guid.Empty;
    public bool IsNotValid => !IsValid;
}

public interface IEntityWithGasStationPointId
{
     GasStationPointId  GasStationPointId { get; }
}

public readonly struct GasStationPointId : IGuidId<GasStationPointId>
{
    public Guid Id { get; }

    public GasStationPointId(Guid InId)
    {
        Id = InId;
    }

	public static GasStationPointId GenerateId() => new GasStationPointId(Guid.NewGuid());
    public static GasStationPointId Parse(string InStringGuid)
    {
        return new GasStationPointId(Guid.Parse(InStringGuid));
    }

    public static bool operator ==(GasStationPointId InLhs, GasStationPointId InRhs) => InLhs.Equals(InRhs);
    public static bool operator !=(GasStationPointId InLhs, GasStationPointId InRhs) => !InLhs.Equals(InRhs);
    public bool Equals(GasStationPointId InOther) => Id.Equals(InOther.Id);
    public override bool Equals(object? InObj) => InObj is GasStationPointId other && Equals(other);
    public int CompareTo(GasStationPointId InOther) => Id.CompareTo(InOther.Id);
    public string ToString(string? InFormat, IFormatProvider? InFormatProvider) => Id.ToString(InFormat, InFormatProvider);

    public override int GetHashCode() => Id.GetHashCode();
    public override string ToString() => Id.ToString();

    public bool IsValid => Id != Guid.Empty;
    public bool IsNotValid => !IsValid;
}

public interface IEntityWithGasStationTariffId
{
     GasStationTariffId  GasStationTariffId { get; }
}

public readonly struct GasStationTariffId : IGuidId<GasStationTariffId>
{
    public Guid Id { get; }

    public GasStationTariffId(Guid InId)
    {
        Id = InId;
    }

	public static GasStationTariffId GenerateId() => new GasStationTariffId(Guid.NewGuid());
    public static GasStationTariffId Parse(string InStringGuid)
    {
        return new GasStationTariffId(Guid.Parse(InStringGuid));
    }

    public static bool operator ==(GasStationTariffId InLhs, GasStationTariffId InRhs) => InLhs.Equals(InRhs);
    public static bool operator !=(GasStationTariffId InLhs, GasStationTariffId InRhs) => !InLhs.Equals(InRhs);
    public bool Equals(GasStationTariffId InOther) => Id.Equals(InOther.Id);
    public override bool Equals(object? InObj) => InObj is GasStationTariffId other && Equals(other);
    public int CompareTo(GasStationTariffId InOther) => Id.CompareTo(InOther.Id);
    public string ToString(string? InFormat, IFormatProvider? InFormatProvider) => Id.ToString(InFormat, InFormatProvider);

    public override int GetHashCode() => Id.GetHashCode();
    public override string ToString() => Id.ToString();

    public bool IsValid => Id != Guid.Empty;
    public bool IsNotValid => !IsValid;
}

public interface IEntityWithCarShowroomVehicleSpawnId
{
     CarShowroomVehicleSpawnId  CarShowroomVehicleSpawnId { get; }
}

public readonly struct CarShowroomVehicleSpawnId : IGuidId<CarShowroomVehicleSpawnId>
{
    public Guid Id { get; }

    public CarShowroomVehicleSpawnId(Guid InId)
    {
        Id = InId;
    }

	public static CarShowroomVehicleSpawnId GenerateId() => new CarShowroomVehicleSpawnId(Guid.NewGuid());
    public static CarShowroomVehicleSpawnId Parse(string InStringGuid)
    {
        return new CarShowroomVehicleSpawnId(Guid.Parse(InStringGuid));
    }

    public static bool operator ==(CarShowroomVehicleSpawnId InLhs, CarShowroomVehicleSpawnId InRhs) => InLhs.Equals(InRhs);
    public static bool operator !=(CarShowroomVehicleSpawnId InLhs, CarShowroomVehicleSpawnId InRhs) => !InLhs.Equals(InRhs);
    public bool Equals(CarShowroomVehicleSpawnId InOther) => Id.Equals(InOther.Id);
    public override bool Equals(object? InObj) => InObj is CarShowroomVehicleSpawnId other && Equals(other);
    public int CompareTo(CarShowroomVehicleSpawnId InOther) => Id.CompareTo(InOther.Id);
    public string ToString(string? InFormat, IFormatProvider? InFormatProvider) => Id.ToString(InFormat, InFormatProvider);

    public override int GetHashCode() => Id.GetHashCode();
    public override string ToString() => Id.ToString();

    public bool IsValid => Id != Guid.Empty;
    public bool IsNotValid => !IsValid;
}

public interface IEntityWithCarShowroomVehicleId
{
     CarShowroomVehicleId  CarShowroomVehicleId { get; }
}

public readonly struct CarShowroomVehicleId : IGuidId<CarShowroomVehicleId>
{
    public Guid Id { get; }

    public CarShowroomVehicleId(Guid InId)
    {
        Id = InId;
    }

	public static CarShowroomVehicleId GenerateId() => new CarShowroomVehicleId(Guid.NewGuid());
    public static CarShowroomVehicleId Parse(string InStringGuid)
    {
        return new CarShowroomVehicleId(Guid.Parse(InStringGuid));
    }

    public static bool operator ==(CarShowroomVehicleId InLhs, CarShowroomVehicleId InRhs) => InLhs.Equals(InRhs);
    public static bool operator !=(CarShowroomVehicleId InLhs, CarShowroomVehicleId InRhs) => !InLhs.Equals(InRhs);
    public bool Equals(CarShowroomVehicleId InOther) => Id.Equals(InOther.Id);
    public override bool Equals(object? InObj) => InObj is CarShowroomVehicleId other && Equals(other);
    public int CompareTo(CarShowroomVehicleId InOther) => Id.CompareTo(InOther.Id);
    public string ToString(string? InFormat, IFormatProvider? InFormatProvider) => Id.ToString(InFormat, InFormatProvider);

    public override int GetHashCode() => Id.GetHashCode();
    public override string ToString() => Id.ToString();

    public bool IsValid => Id != Guid.Empty;
    public bool IsNotValid => !IsValid;
}

public interface IEntityWithFractionId
{
     FractionId  FractionId { get; }
}

public readonly struct FractionId : IGuidId<FractionId>
{
    public Guid Id { get; }

    public FractionId(Guid InId)
    {
        Id = InId;
    }

	public static FractionId GenerateId() => new FractionId(Guid.NewGuid());
    public static FractionId Parse(string InStringGuid)
    {
        return new FractionId(Guid.Parse(InStringGuid));
    }

    public static bool operator ==(FractionId InLhs, FractionId InRhs) => InLhs.Equals(InRhs);
    public static bool operator !=(FractionId InLhs, FractionId InRhs) => !InLhs.Equals(InRhs);
    public bool Equals(FractionId InOther) => Id.Equals(InOther.Id);
    public override bool Equals(object? InObj) => InObj is FractionId other && Equals(other);
    public int CompareTo(FractionId InOther) => Id.CompareTo(InOther.Id);
    public string ToString(string? InFormat, IFormatProvider? InFormatProvider) => Id.ToString(InFormat, InFormatProvider);

    public override int GetHashCode() => Id.GetHashCode();
    public override string ToString() => Id.ToString();

    public bool IsValid => Id != Guid.Empty;
    public bool IsNotValid => !IsValid;
}

public interface IEntityWithFractionRankId
{
     FractionRankId  FractionRankId { get; }
}

public readonly struct FractionRankId : IGuidId<FractionRankId>
{
    public Guid Id { get; }

    public FractionRankId(Guid InId)
    {
        Id = InId;
    }

	public static FractionRankId GenerateId() => new FractionRankId(Guid.NewGuid());
    public static FractionRankId Parse(string InStringGuid)
    {
        return new FractionRankId(Guid.Parse(InStringGuid));
    }

    public static bool operator ==(FractionRankId InLhs, FractionRankId InRhs) => InLhs.Equals(InRhs);
    public static bool operator !=(FractionRankId InLhs, FractionRankId InRhs) => !InLhs.Equals(InRhs);
    public bool Equals(FractionRankId InOther) => Id.Equals(InOther.Id);
    public override bool Equals(object? InObj) => InObj is FractionRankId other && Equals(other);
    public int CompareTo(FractionRankId InOther) => Id.CompareTo(InOther.Id);
    public string ToString(string? InFormat, IFormatProvider? InFormatProvider) => Id.ToString(InFormat, InFormatProvider);

    public override int GetHashCode() => Id.GetHashCode();
    public override string ToString() => Id.ToString();

    public bool IsValid => Id != Guid.Empty;
    public bool IsNotValid => !IsValid;
}

public interface IEntityWithFractionMemberId
{
     FractionMemberId  FractionMemberId { get; }
}

public readonly struct FractionMemberId : IGuidId<FractionMemberId>
{
    public Guid Id { get; }

    public FractionMemberId(Guid InId)
    {
        Id = InId;
    }

	public static FractionMemberId GenerateId() => new FractionMemberId(Guid.NewGuid());
    public static FractionMemberId Parse(string InStringGuid)
    {
        return new FractionMemberId(Guid.Parse(InStringGuid));
    }

    public static bool operator ==(FractionMemberId InLhs, FractionMemberId InRhs) => InLhs.Equals(InRhs);
    public static bool operator !=(FractionMemberId InLhs, FractionMemberId InRhs) => !InLhs.Equals(InRhs);
    public bool Equals(FractionMemberId InOther) => Id.Equals(InOther.Id);
    public override bool Equals(object? InObj) => InObj is FractionMemberId other && Equals(other);
    public int CompareTo(FractionMemberId InOther) => Id.CompareTo(InOther.Id);
    public string ToString(string? InFormat, IFormatProvider? InFormatProvider) => Id.ToString(InFormat, InFormatProvider);

    public override int GetHashCode() => Id.GetHashCode();
    public override string ToString() => Id.ToString();

    public bool IsValid => Id != Guid.Empty;
    public bool IsNotValid => !IsValid;
}

public interface IEntityWithCharacterInventoryItemId
{
     CharacterInventoryItemId  CharacterInventoryItemId { get; }
}

public readonly struct CharacterInventoryItemId : IGuidId<CharacterInventoryItemId>
{
    public Guid Id { get; }

    public CharacterInventoryItemId(Guid InId)
    {
        Id = InId;
    }

	public static CharacterInventoryItemId GenerateId() => new CharacterInventoryItemId(Guid.NewGuid());
    public static CharacterInventoryItemId Parse(string InStringGuid)
    {
        return new CharacterInventoryItemId(Guid.Parse(InStringGuid));
    }

    public static bool operator ==(CharacterInventoryItemId InLhs, CharacterInventoryItemId InRhs) => InLhs.Equals(InRhs);
    public static bool operator !=(CharacterInventoryItemId InLhs, CharacterInventoryItemId InRhs) => !InLhs.Equals(InRhs);
    public bool Equals(CharacterInventoryItemId InOther) => Id.Equals(InOther.Id);
    public override bool Equals(object? InObj) => InObj is CharacterInventoryItemId other && Equals(other);
    public int CompareTo(CharacterInventoryItemId InOther) => Id.CompareTo(InOther.Id);
    public string ToString(string? InFormat, IFormatProvider? InFormatProvider) => Id.ToString(InFormat, InFormatProvider);

    public override int GetHashCode() => Id.GetHashCode();
    public override string ToString() => Id.ToString();

    public bool IsValid => Id != Guid.Empty;
    public bool IsNotValid => !IsValid;
}

public interface IEntityWithLicenseId
{
     LicenseId  LicenseId { get; }
}

public readonly struct LicenseId : IGuidId<LicenseId>
{
    public Guid Id { get; }

    public LicenseId(Guid InId)
    {
        Id = InId;
    }

	public static LicenseId GenerateId() => new LicenseId(Guid.NewGuid());
    public static LicenseId Parse(string InStringGuid)
    {
        return new LicenseId(Guid.Parse(InStringGuid));
    }

    public static bool operator ==(LicenseId InLhs, LicenseId InRhs) => InLhs.Equals(InRhs);
    public static bool operator !=(LicenseId InLhs, LicenseId InRhs) => !InLhs.Equals(InRhs);
    public bool Equals(LicenseId InOther) => Id.Equals(InOther.Id);
    public override bool Equals(object? InObj) => InObj is LicenseId other && Equals(other);
    public int CompareTo(LicenseId InOther) => Id.CompareTo(InOther.Id);
    public string ToString(string? InFormat, IFormatProvider? InFormatProvider) => Id.ToString(InFormat, InFormatProvider);

    public override int GetHashCode() => Id.GetHashCode();
    public override string ToString() => Id.ToString();

    public bool IsValid => Id != Guid.Empty;
    public bool IsNotValid => !IsValid;
}

public interface IEntityWithStationPointId
{
     StationPointId  StationPointId { get; }
}

public readonly struct StationPointId : IGuidId<StationPointId>
{
    public Guid Id { get; }

    public StationPointId(Guid InId)
    {
        Id = InId;
    }

	public static StationPointId GenerateId() => new StationPointId(Guid.NewGuid());
    public static StationPointId Parse(string InStringGuid)
    {
        return new StationPointId(Guid.Parse(InStringGuid));
    }

    public static bool operator ==(StationPointId InLhs, StationPointId InRhs) => InLhs.Equals(InRhs);
    public static bool operator !=(StationPointId InLhs, StationPointId InRhs) => !InLhs.Equals(InRhs);
    public bool Equals(StationPointId InOther) => Id.Equals(InOther.Id);
    public override bool Equals(object? InObj) => InObj is StationPointId other && Equals(other);
    public int CompareTo(StationPointId InOther) => Id.CompareTo(InOther.Id);
    public string ToString(string? InFormat, IFormatProvider? InFormatProvider) => Id.ToString(InFormat, InFormatProvider);

    public override int GetHashCode() => Id.GetHashCode();
    public override string ToString() => Id.ToString();

    public bool IsValid => Id != Guid.Empty;
    public bool IsNotValid => !IsValid;
}

