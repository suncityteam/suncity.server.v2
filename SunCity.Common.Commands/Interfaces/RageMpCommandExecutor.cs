﻿using System;
using System.Threading.Tasks;
using Microsoft.Extensions.DependencyInjection;
using SunCity.Common.MediatR;
using SunCity.Common.RageMp.Pools.Players;

namespace SunCity.Common.Commands.Interfaces
{
    public sealed class RageMpCommandExecutor
    {
        public static RageMpCommandExecutor CommandExecutor { get; private set; }
        private IServiceProvider ServiceProvider { get; }
        private IPlayerPool PlayerPool { get; }

        public RageMpCommandExecutor(IServiceProvider serviceProvider, IPlayerPool playerPool)
        {
            ServiceProvider = serviceProvider;
            PlayerPool = playerPool;
            CommandExecutor = this;
        }

        public void Execute<TRequest>(GTANetworkAPI.Player player, TRequest request) where TRequest : IRageMpCommandRequest
        {
            Task.Factory.StartNew(async () =>
            {
                using var scope = ServiceProvider.CreateScope();
                var middlewares = scope.ServiceProvider.GetServices<IRageMpCommandMiddleware>();
                var executor = scope.ServiceProvider.GetRequiredService<IOperationExecutor>();

                foreach (var middleware in middlewares)
                {
                    var ragePlayer = PlayerPool.Convert(player);
                    var middlewareResponse = middleware.Prepare(ragePlayer);
                    if (middlewareResponse.IsNotCorrect)
                    {
                        return;
                    }
                }

                await executor.Execute(request);
            }, TaskCreationOptions.LongRunning | TaskCreationOptions.DenyChildAttach | TaskCreationOptions.RunContinuationsAsynchronously);
        }
    }
}