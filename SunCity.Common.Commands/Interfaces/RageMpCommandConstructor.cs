﻿using Microsoft.Extensions.Logging;
using SunCity.Common.RageMp.Pools;
using SunCity.Common.RageMp.Pools.Players;

namespace SunCity.Common.Commands.Interfaces
{
    public sealed class RageMpCommandConstructor<TRequest>
    {
        public ILogger<TRequest> Logger { get; }
        public IRageMpPool RageMpPool { get; }
        public IRagePlayer RagePlayer { get; }

        public RageMpCommandConstructor(IRagePlayer ragePlayer, IRageMpPool rageMpPool, ILogger<TRequest> logger)
        {
            RagePlayer = ragePlayer;
            RageMpPool = rageMpPool;
            Logger = logger;
        }
    }
}