﻿using System.Threading;
using System.Threading.Tasks;
using MediatR;
using Microsoft.Extensions.Logging;
using SunCity.Common.Operation;
using SunCity.Common.RageMp.Pools;
using SunCity.Common.RageMp.Pools.Players;

namespace SunCity.Common.Commands.Interfaces
{
    public abstract class RageMpCommandPipelineBehavior<TRequest> : IRageMpCommandPipelineBehavior<TRequest> where TRequest : IRageMpCommandRequest
    {
        protected ILogger<TRequest> Logger { get; }
        protected IRageMpPool RageMpPool { get; }
        protected IRagePlayer RagePlayer { get; }

        protected RageMpCommandPipelineBehavior(RageMpCommandConstructor<TRequest> constructor)
        {
            RagePlayer = constructor.RagePlayer;
            RageMpPool = constructor.RageMpPool;
            Logger = constructor.Logger;
        }

        protected IRagePlayer GetPlayerById(ushort targetPlayerId)
        {
            return RageMpPool.Players.GetAt(targetPlayerId);
        }

        public abstract Task<OperationResponse> Handle(TRequest request, CancellationToken cancellationToken, RequestHandlerDelegate<OperationResponse> next);
    }
}