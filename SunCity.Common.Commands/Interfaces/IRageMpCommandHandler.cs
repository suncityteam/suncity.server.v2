﻿using SunCity.Common.MediatR;

namespace SunCity.Common.Commands.Interfaces
{
    public interface IRageMpCommandHandler<TRequest> : IOperationHandler<TRequest> where TRequest : IRageMpCommandRequest
    {
    }   
    
    public interface IRageMpCommandPipelineBehavior<TRequest> : IOperationPipelineBehavior<TRequest> where TRequest : IRageMpCommandRequest
    {
    }
}