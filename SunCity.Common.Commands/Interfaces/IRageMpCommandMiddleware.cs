﻿using GTANetworkAPI;
using SunCity.Common.Operation;
using SunCity.Common.RageMp.Pools.Players;

namespace SunCity.Common.Commands.Interfaces
{
    public interface IRageMpCommandMiddleware
    {
        OperationResponse Prepare(IRagePlayer player);
    }
}