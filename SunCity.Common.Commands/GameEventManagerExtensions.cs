﻿using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.DependencyInjection;
using SunCity.Common.Commands.Interfaces;
using SunCity.Common.MediatR.Extensions;

namespace SunCity.Common.Commands
{
    public static class GameCommandsManagerExtensions
    {
        public static void AddGameCommandsManager(this IServiceCollection InServiceCollection)
        {
            InServiceCollection.AddScoped(typeof(RageMpCommandConstructor<>));
            InServiceCollection.AddSingleton<RageMpCommandExecutor>();
        }
        
        public static void UseGameCommandsManager(this IApplicationBuilder applicationBuilder)
        {
            applicationBuilder.ApplicationServices.GetRequiredService<RageMpCommandExecutor>();
        }
        

        public static void AddRageMpCommandHandler<TRequest, THandler>(this IServiceCollection InServiceCollection)
            where TRequest : IRageMpCommandRequest
            where THandler : class, IRageMpCommandHandler<TRequest>
        {
            InServiceCollection.AddRequestHandler<TRequest, THandler>();
        }
    }
}