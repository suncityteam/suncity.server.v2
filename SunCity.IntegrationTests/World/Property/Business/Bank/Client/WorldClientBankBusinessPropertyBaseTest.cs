﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SunCity.IntegrationTests.HttpClients.World.Property.Business.Bank.Client;
using SunCity.IntegrationTests.Services.World.Property.Business.Bank.Client.Services;

namespace SunCity.IntegrationTests.World.Property.Business.Bank.Client
{
    internal abstract class WorldClientBankBusinessPropertyBaseTest : WorldBankBusinessPropertyBaseTest
    {
        protected ClientBankCardAccountService BankCardAccountService => GetService<ClientBankCardAccountService>();
        protected BankCardAccountHttpClient BankCardAccountHttpClient => GetService<BankCardAccountHttpClient>();
    }
}