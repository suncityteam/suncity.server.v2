﻿using System.Threading.Tasks;
using NUnit.Framework;
using SunCity.Game.World.Enums.Character;

namespace SunCity.IntegrationTests.Identity.Characters.Create
{
    //[TestFixture]
    internal class CharacterCreateTests : IdentityBaseTest
    {
        [Test]
        public async Task CreateFirstCharacter()
        {
            var playerConnection = ConnectionService.CreatePlayer();
            await AuthorizationService.Register(playerConnection);

            //===========================================
            //  Required fetch WhatToDo, then create character
            var whatToDo = await CharacterService.WhatToDo(playerConnection);
            Assert.AreEqual(AuthorizationWhatToDo.CreateCharacter, whatToDo, "Failed create character, invalid WhatToDo");

            await CharacterService.Create(playerConnection);

            var characters = await CharacterService.GetList(playerConnection);
            Assert.AreEqual(1, characters.Characters.Length, $"Failed create character, invalid characters count");
        }

        [Test]
        public async Task CreateSecondCharacter()
        {
            var playerConnection = ConnectionService.CreatePlayer();
            await AuthorizationService.Register(playerConnection);

            //===========================================
            //  Required fetch WhatToDo, then create character
            var whatToDo = await CharacterService.WhatToDo(playerConnection);
            Assert.AreEqual(AuthorizationWhatToDo.CreateCharacter, whatToDo, "Failed create character, invalid WhatToDo");

            await CharacterService.Create(playerConnection);
            await CharacterService.Create(playerConnection);

            var characters = await CharacterService.GetList(playerConnection);
            Assert.AreEqual(2, characters.Characters.Length, $"Failed create character, invalid characters count");
        }
    }
}