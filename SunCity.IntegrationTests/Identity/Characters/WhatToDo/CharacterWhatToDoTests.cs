﻿using System.Threading.Tasks;
using NUnit.Framework;
using SunCity.Game.World.Enums.Character;

namespace SunCity.IntegrationTests.Identity.Characters.WhatToDo
{
    //[TestFixture]
    internal class CharacterWhatToDoTests : IdentityBaseTest
    {
        [Test]
        public async Task WhatToDoWithoutCharacters()
        {
            var playerConnection = ConnectionService.CreatePlayer();
            await AuthorizationService.Register(playerConnection);
            var whatToDo = await CharacterService.WhatToDo(playerConnection);
            Assert.AreEqual(AuthorizationWhatToDo.CreateCharacter, whatToDo, $"Invalid WhatToDo");
        }     
        
        [Test]
        public async Task WhatToDoWithDefaultCharacter()
        {
            var playerConnection = ConnectionService.CreatePlayer();
            await AuthorizationService.Register(playerConnection);
            
            await CharacterService.WhatToDo(playerConnection);
            await CharacterService.Create(playerConnection);

            var whatToDo = await CharacterService.WhatToDo(playerConnection);
            Assert.AreEqual(AuthorizationWhatToDo.SpawnCharacter, whatToDo, $"Invalid WhatToDo");
        }   
        
        [Test, Ignore("Always exist default character after create first character")]
        public async Task WhatToDoWithoutDefaultCharacter()
        {

        }
    }
}