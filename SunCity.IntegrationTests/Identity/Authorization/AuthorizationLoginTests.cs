﻿using System.Threading.Tasks;
using NUnit.Framework;
using SunCity.Game.Identity.Services.Sessions;
using SunCity.Game.World.Identity.Operations.Authorization.Register;

namespace SunCity.IntegrationTests.Identity.Authorization
{
    //[TestFixture]
    internal class AuthorizationTests : IdentityBaseTest
    {
        [Test]
        public async Task RegisterWithValidData()
        {
            var playerConnection = ConnectionService.CreatePlayer();
            var registerResponse = await AuthorizationService.Register(playerConnection);

            Assert.NotNull(registerResponse.AccountLogin.Name, $"Invalid {nameof(AuthorizationRegisterResponse.AccountLogin)}");
            Assert.IsNotEmpty(registerResponse.AccountLogin.Name, $"Invalid {nameof(AuthorizationRegisterResponse.AccountLogin)}");
        }

        [Test]
        public async Task LoginWithValidData()
        {
            var playerConnection = ConnectionService.CreatePlayer();

            var registerRequest = RegisterRequestGenerator.Generate();
            await AuthorizationService.Register(playerConnection, registerRequest);

            //  Wait close session
            await Task.Delay(1.5f * GameSessionSettings.Timeout);

            await AuthorizationService.Login(playerConnection, registerRequest);
        }
    }
}