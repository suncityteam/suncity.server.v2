using System;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using NUnit.Framework;
using SunCity.IntegrationTests.Services.Connection;
using SunCity.IntegrationTests.Services.Identity.Authorization;
using SunCity.IntegrationTests.Services.Identity.Authorization.Generators;
using SunCity.IntegrationTests.Services.Identity.Character;

namespace SunCity.IntegrationTests
{
    internal abstract class BaseTest
    {
        private IConfigurationRoot Configuration { get; set; }
        protected IServiceProvider ServiceProvider { get; private set; }

        protected AuthorizationService AuthorizationService => GetService<AuthorizationService>();
        protected CharacterIdentityService CharacterService => GetService<CharacterIdentityService>();
        protected RageConnectionService ConnectionService => GetService<RageConnectionService>();

        protected AuthorizationLoginRequestGenerator LoginRequestGenerator => GetService<AuthorizationLoginRequestGenerator>();
        protected AuthorizationRegisterRequestGenerator RegisterRequestGenerator => GetService<AuthorizationRegisterRequestGenerator>();

        protected T GetService<T>()
        {
            return ServiceProvider.GetRequiredService<T>();
        }
        
        [OneTimeSetUp]
        public void Setup()
        {
            Configuration = BaseTestContext.Configuration;
            ServiceProvider = BaseTestContext.ServiceProvider;
        }
    }
}