using System;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using NUnit.Framework;
using SunCity.Common;
using SunCity.Game.Identity.Services.Sessions;
using SunCity.IntegrationTests.HttpClients;
using SunCity.IntegrationTests.Services;


[SetUpFixture]
public sealed class BaseTestContext
{
    internal static IConfigurationRoot Configuration { get; private set; }
    internal static IServiceProvider ServiceProvider { get; private set; }

    private IConfigurationRoot SetupConfiguration()
    {
        var builder = new ConfigurationBuilder()
            .AddEnvironmentVariables(EnvironmentNames.INTEGRATION_TESTS)

            //.AddJsonFile("Settings/Application.DataBase.json", false)
            //.AddJsonFile("Settings/Application.Identity.json", false)
            //.AddJsonFile("Settings/World.ZoneNames.json", false)
            .AddEnvironmentVariables();

        return builder.Build();
    }

    private IServiceCollection ConfigureServices(IConfigurationRoot configuration)
    {
        var services = new ServiceCollection();
        services.AddHttpClients(configuration);
        services.AddTestServices();

        return services;
    }

    [OneTimeSetUp]
    public void Setup()
    {
        //  For tests
        GameSessionSettings.Timeout = TimeSpan.FromSeconds(3);

        Configuration = SetupConfiguration();
        ServiceProvider = ConfigureServices(Configuration).BuildServiceProvider();
    }
}