﻿using System;
using System.Collections.Generic;
using System.Text;
using SunCity.Domain.Interfaces.Entity;
using SunCity.Game.Interfaces.Property.Bank;
using SunCity.Game.Types.Cost;
using SunCity.Game.Types.Property.Category.Bank;

namespace SunCity.Game.World.Forms.Property.Business.Bank.Tariff
{
    public class CreateBankCardTariffForm 
        : IEntityWithId<PropertyId>
        , IBankTariffProperties
    {
        public PropertyId EntityId { get; set; }
        public string Name { get; set; }
        public BankTariffFee Deposit { get; set; }
        public BankTariffFee WithDraw { get; set; }
        public BankTariffFee Transfer { get; set; }
        public GameCost MaintenanceCost { get; set; }
        public GameCost TariffCost { get; set; }
    }
}
