﻿using System.Threading.Tasks;
using AutoMapper;
using SunCity.Domain.Interfaces.Entity;
using SunCity.Game.Interfaces.Property.Bank;
using SunCity.Game.Types.Cost;
using SunCity.Game.Types.Property.Category.Bank;

namespace SunCity.Game.World.Forms.Property.Business.Bank.Tariff
{
    public class UpdateBankCardTariffForm
        : IEntityWithId<BankCardTariffId>
        , IBankTariffProperties
    {
        public BankCardTariffId EntityId { get; set; }

        public string Name { get; set; }
        public BankTariffFee Deposit { get; set; }
        public BankTariffFee WithDraw { get; set; }
        public BankTariffFee Transfer { get; set; }
        public GameCost MaintenanceCost { get; set; }
        public GameCost TariffCost { get; set; }
    }
}