﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SunCity.Game.World.Forms.Property.Business.Bank
{
    public class BankAuthorizationForm
    {
        public BankCardId PayerBankCardId { get; set; }
        public int PinCode { get; set; }

        public override string ToString()
        {
            return $"PayerBankCardId: {PayerBankCardId} | PinCode: {PinCode}";
        }
    }
}
