﻿using System;
using System.Collections.Generic;
using System.Text;
using GTANetworkAPI;

namespace SunCity.Game.World.Forms.Property.Interior
{
    public class PropertyInteriorInitializeForm
    {
        public MarkerType Marker { get; set; }
        public string TextLabel { get; set; }

    }
}
