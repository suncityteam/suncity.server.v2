﻿using System;
using AutoMapper;
using SunCity.Game.Interfaces.Player.Character;
using SunCity.Game.World.Forms.Character;

namespace SunCity.Game.World.Forms
{
    public sealed class MappingProfile : Profile
    {
        public MappingProfile() : base("SunCity.Game.World.Forms")
        {
            CreateMap<IPlayerCharacterSkin, WorldJoinCharacterSkin>();
        }
    }
}
