﻿using System;
using System.Collections.Generic;
using System.Text;
using SunCity.Domain.Interfaces.Entity;
using SunCity.Game.World.Forms.Property.Business.Bank;

namespace SunCity.Game.World.Forms.Payment
{
    public class PaymentForm
    {
        public BankCardId PayerCard { get; set; }
        public BankCardId PayeeCard { get; set; }
        public int PinCode { get; set; }
        public long Amount { get; set; }
    }
}
