﻿using System;
using System.Collections.Generic;
using System.Text;
using SunCity.Domain.Interfaces.Entity;
using SunCity.Game.Enums.World.Player.Inventory;
using SunCity.Game.World.Models.Item.World;

namespace SunCity.Game.World.Forms.Character.Inventory
{
    public class LoadCharacterInventory
    {
        public LoadCharacterInventoryItemForm[] Slots { get; set; }
    }

    public class LoadCharacterInventoryItemForm : IEntityWithId<CharacterInventoryItemId>
    {
        public CharacterInventoryItemId EntityId { get; set; }
        public PlayerInventorySlot InventorySlot { get; set; }
        public WorldItemModel Item { get; set; }
    }
}
