﻿using System;
using SunCity.Common.RageMp.Pools.Players;
using SunCity.Game.Types.Player.Components;
using SunCity.Game.Types.Player.Entity.Player.Character;
using SunCity.Game.World.Forms.Character.Inventory;
using SunCity.Game.World.Models.Account;
using SunCity.Game.World.Models.Character;

namespace SunCity.Game.World.Forms.Character
{
    public class WorldJoinCharacterForm
    {
        public IRagePlayer RagePlayer { get; set; }
        public PlayerAccountProperties Account { get; set; }

        public PlayerCharacterId PlayerCharacterId { get; set; }
        public PlayerCharacterName PlayerCharacterName { get; set; }
   

        public CharacterExperience Experience { get; set; }
        public PlayerCharacterPosition Position { get; set; }
        public WorldJoinCharacterSkin Skin { get; set; }
        public long Balance { get; set; }
        public TimeSpan PlayedForPayDay { get; set; }
        public Health Health { get; set; }
        public Hunger Hunger { get; set; }
        public Hunger Thirst { get; set; }

        public LoadCharacterInventory Inventory { get; set; }
    }
}
