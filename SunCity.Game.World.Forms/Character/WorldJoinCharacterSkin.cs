﻿using System.Threading.Tasks;
using AutoMapper;
using SunCity.Game.Enums.Character;
using SunCity.Game.Interfaces.Player.Character;

namespace SunCity.Game.World.Forms.Character
{
    public class WorldJoinCharacterSkin : IPlayerCharacterSkin
    {
        public GameCharacterSex Sex { get; set; }
        public byte FirstHeadShape { get; set; }
        public byte SecondHeadShape { get; set; }
        public byte FirstSkinTone { get; set; }
        public byte SecondSkinTone { get; set; }
        public float HeadMix { get; set; }
        public float SkinMix { get; set; }
        public byte HairModel { get; set; }
        public byte FirstHairColor { get; set; }
        public byte SecondHairColor { get; set; }
        public short BeardModel { get; set; }
        public short BeardColor { get; set; }
        public short ChestModel { get; set; }
        public short ChestColor { get; set; }
        public short BlemishesModel { get; set; }
        public short AgeingModel { get; set; }
        public short ComplexionModel { get; set; }
        public short SundamageModel { get; set; }
        public short FrecklesModel { get; set; }
        public byte EyesColor { get; set; }
        public short EyebrowsModel { get; set; }
        public short EyebrowsColor { get; set; }
        public short MakeupModel { get; set; }
        public short BlushModel { get; set; }
        public short BlushColor { get; set; }
        public short LipstickModel { get; set; }
        public short LipstickColor { get; set; }
        public float NoseWidth { get; set; }
        public float NoseHeight { get; set; }
        public float NoseLength { get; set; }
        public float NoseBridge { get; set; }
        public float NoseTip { get; set; }
        public float NoseShift { get; set; }
        public float BrowHeight { get; set; }
        public float BrowWidth { get; set; }
        public float CheekboneHeight { get; set; }
        public float CheekboneWidth { get; set; }
        public float CheeksWidth { get; set; }
        public float Eyes { get; set; }
        public float Lips { get; set; }
        public float JawWidth { get; set; }
        public float JawHeight { get; set; }
        public float ChinLength { get; set; }
        public float ChinPosition { get; set; }
        public float ChinWidth { get; set; }
        public float ChinShape { get; set; }
        public float NeckWidth { get; set; }
    }
}