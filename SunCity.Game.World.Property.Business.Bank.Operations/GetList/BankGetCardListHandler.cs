﻿using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using SunCity.Common.MediatR;
using SunCity.Common.Operation;
using SunCity.Game.World.Models.Property.Business.Bank.Card;
using SunCity.Game.World.Player;
using SunCity.Game.World.Services.Property.Category.Business.Bank.DbQueries.Client.BankCard.Player;

namespace SunCity.Game.World.Property.Business.Bank.Operations.GetList
{
    public class BankGetCardListHandler : IOperationHandler<BankGetCardListRequest, BankCardPreviewModel[]>
    {
        private WorldPlayer WorldPlayer { get; }
        private IMapper Mapper { get; }
        private IBankCardPlayerDbQueries BankCardDbQueries { get; }
        
        public BankGetCardListHandler(WorldPlayer worldPlayer, IMapper mapper, IBankCardPlayerDbQueries bankCardDbQueries)
        {
            WorldPlayer = worldPlayer;
            Mapper = mapper;
            BankCardDbQueries = bankCardDbQueries;
        }

        public async Task<OperationResponse<BankCardPreviewModel[]>> Handle(BankGetCardListRequest request, CancellationToken cancellationToken)
        {
            var bankCards = await BankCardDbQueries.GetList(WorldPlayer.EntityId);
            var models = Mapper.Map<BankCardPreviewModel[]>(bankCards);
            return models;
            
        }
    }
}