﻿using SunCity.Common.MediatR;
using SunCity.Game.World.Models.Property.Business.Bank.Card;

namespace SunCity.Game.World.Property.Business.Bank.Operations.GetList
{
    public class BankGetCardListRequest : IOperationRequest<BankCardPreviewModel[]>
    {
        public PropertyId PropertyId { get; set; }
    }
}