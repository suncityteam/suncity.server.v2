﻿using System.ComponentModel.DataAnnotations;
using SunCity.Common.MediatR;
using SunCity.Game.World.Forms.Property.Business.Bank;
using SunCity.Game.World.Models.Property.Business.Bank.Card;

namespace SunCity.Game.World.Property.Business.Bank.Operations.GetBalance
{
    public class BankGetCardBalanceRequest : IOperationRequest<BankCardBalanceModel>
    {
        [Required]
        public BankAuthorizationForm Credentials { get; set; }
    }
}