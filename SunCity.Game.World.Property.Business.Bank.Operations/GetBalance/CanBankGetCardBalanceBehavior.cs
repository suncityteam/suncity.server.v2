﻿using System.Threading;
using System.Threading.Tasks;
using MediatR;
using SunCity.Common.MediatR;
using SunCity.Common.Operation;
using SunCity.Game.World.Models.Property.Business.Bank.Card;
using SunCity.Game.World.Services.Property.Category.Business.Bank.Service.Client.BankCard;

namespace SunCity.Game.World.Property.Business.Bank.Operations.GetBalance
{
    public class CanBankGetCardBalanceBehavior : IOperationPipelineBehavior<BankGetCardBalanceRequest, BankCardBalanceModel>
    {      
        private IBankCardAuthorizationService BankCardAuthorizationService { get; }

        public CanBankGetCardBalanceBehavior(IBankCardAuthorizationService bankCardAuthorizationService)
        {
            BankCardAuthorizationService = bankCardAuthorizationService;
        }
        
        public async Task<OperationResponse<BankCardBalanceModel>> Handle(BankGetCardBalanceRequest request, CancellationToken cancellationToken,
            RequestHandlerDelegate<OperationResponse<BankCardBalanceModel>> next)
        {
            var auth = await BankCardAuthorizationService.Authorization(request.Credentials);
            if (auth.IsNotCorrect)
                return auth.Error;

            return await next();
        }
    }
}