﻿using System.Threading;
using System.Threading.Tasks;
using SunCity.Common.MediatR;
using SunCity.Common.Operation;
using SunCity.Game.World.Models.Property.Business.Bank.Card;
using SunCity.Game.World.Services.Property.Category.Business.Bank.DbQueries.Client.BankCard.Player;

namespace SunCity.Game.World.Property.Business.Bank.Operations.GetBalance
{
    public class BankGetCardBalanceHandler: IOperationHandler<BankGetCardBalanceRequest, BankCardBalanceModel>
    {
        private IBankCardPlayerDbQueries DbQueries { get; }

        public BankGetCardBalanceHandler(IBankCardPlayerDbQueries dbQueries)
        {
            DbQueries = dbQueries;
        }
        
        public async Task<OperationResponse<BankCardBalanceModel>> Handle(BankGetCardBalanceRequest request, CancellationToken cancellationToken)
        {
            var bankCard = await DbQueries.GetById(request.Credentials.PayerBankCardId);
            
            return new BankCardBalanceModel
            {
                EntityId = bankCard.EntityId,
                Balance = bankCard.Balance,
                Number = bankCard.Number
            };
        }
    }
}