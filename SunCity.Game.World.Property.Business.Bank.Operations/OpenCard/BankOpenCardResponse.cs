﻿namespace SunCity.Game.World.Property.Business.Bank.Operations.OpenCard
{
    public class BankOpenCardResponse
    {
        public BankCardId EntityId { get; set; }
        public long Number { get; set; }
        public int PinCode { get; set; }
        public string SecureWord { get; set; }
    }
}