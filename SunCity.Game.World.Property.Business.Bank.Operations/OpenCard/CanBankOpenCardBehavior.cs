﻿using System.Threading;
using System.Threading.Tasks;
using MediatR;
using SunCity.Common.MediatR;
using SunCity.Common.Operation;
using SunCity.Domain.Interfaces.Extensions;
using SunCity.Game.World.Property.Category.Business.Bank;

namespace SunCity.Game.World.Property.Business.Bank.Operations.OpenCard
{
    public class CanBankOpenCardBehavior : IOperationPipelineBehavior<BankOpenCardRequest, BankOpenCardResponse>
    {     
        private IWorldPropertyStorage PropertyStorage { get; }

        public async Task<OperationResponse<BankOpenCardResponse>> Handle(BankOpenCardRequest request, CancellationToken cancellationToken, RequestHandlerDelegate<OperationResponse<BankOpenCardResponse>> next)
        {
            var property = PropertyStorage.GetById<BankWorldProperty>(request.PropertyId);
            if (property == null)
                return (OperationResponseCode.PropertyIdNotFound, request.PropertyId);
            
            var tariff = property.BankCardTariff.GetById(request.TariffEntityId);
            if (tariff == null)
                return (OperationResponseCode.BankCardTariffNotFound, request.TariffEntityId);
            
            return await next();
        }
    }
}