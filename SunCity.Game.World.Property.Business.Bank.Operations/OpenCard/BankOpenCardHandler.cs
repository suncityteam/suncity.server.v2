﻿using System.Threading;
using System.Threading.Tasks;
using SunCity.Common.Crypto.Crc64;
using SunCity.Common.MediatR;
using SunCity.Common.Operation;
using SunCity.Domain.Interfaces.Extensions;
using SunCity.Game.World.Models.Property.Business.Bank;
using SunCity.Game.World.Player;
using SunCity.Game.World.Property.Category.Business.Bank;
using SunCity.Game.World.Services.Property.Category.Business.Bank.DbQueries.Client.BankCard.Player;
using SunCity.Game.World.Services.Property.Category.Business.Bank.Forms;
using SunCity.Game.World.Services.Property.Category.Business.Bank.Generators;

namespace SunCity.Game.World.Property.Business.Bank.Operations.OpenCard
{
    public class BankOpenCardHandler : IOperationHandler<BankOpenCardRequest, BankOpenCardResponse>
    {
        public BankOpenCardHandler(WorldPlayer worldPlayer, IBankCardPlayerDbQueries dbQueries, IBankCardNumberGenerator numberGenerator, IWorldPropertyStorage propertyStorage)
        {
            WorldPlayer = worldPlayer;
            DbQueries = dbQueries;
            NumberGenerator = numberGenerator;
            PropertyStorage = propertyStorage;
        }

        private IWorldPropertyStorage PropertyStorage { get; }
        private IBankCardPlayerDbQueries DbQueries { get; }
        private IBankCardNumberGenerator NumberGenerator { get; }
        private WorldPlayer WorldPlayer { get; }
        
        public async Task<OperationResponse<BankOpenCardResponse>> Handle(BankOpenCardRequest request, CancellationToken cancellationToken)
        {
            var property = PropertyStorage.GetById<BankWorldProperty>(request.PropertyId);
            var tariff = property.BankCardTariff.GetById(request.TariffEntityId);
                        
            var cardNumber = await NumberGenerator.GenerateCardNumber();
            if (cardNumber.IsNotCorrect)
                return cardNumber.Error;

            var bankCard = await DbQueries.OpenCard(new BankOpenCardDbForm
            {
                Number = cardNumber.Content,
                CharacterId = WorldPlayer.Character.EntityId,
                
                TariffEntityId = request.TariffEntityId,
                SecureWordHash = request.SecureWord.Trim().ToLowerInvariant().Crc64Hash(),
                PinCodeHash = request.PinCode.Crc64Hash(),
            });

            WorldPlayer.SendClientMessage(new[]
            {
                $"Вы открыли счет {bankCard.Number} в банке \"{property.Name}\".",
                $"Ваш тарифеый план: {tariff.Name}, стоимость обслуживания {tariff.MaintenanceCost}.",
                $"Ваш пин-код: {request.PinCode} и секретное слово: {request.SecureWord}",
                "Обязательно запомните их и никому не сообщайте.",
                "Спасибо что воспользовались услугами нашего банка!"
            });

            return new BankOpenCardResponse
            {
                EntityId = bankCard.EntityId,
                Number = bankCard.Number,
                SecureWord = request.SecureWord,
                PinCode = request.PinCode
            };
        }
    }
}