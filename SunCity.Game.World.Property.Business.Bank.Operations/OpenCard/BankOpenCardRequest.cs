﻿using SunCity.Common.MediatR;

namespace SunCity.Game.World.Property.Business.Bank.Operations.OpenCard
{
    public class BankOpenCardRequest : IOperationRequest<BankOpenCardResponse>
    {
        public PropertyId PropertyId { get; set; }
        public BankCardTariffId TariffEntityId { get; set; }
        public int PinCode { get; set; }
        public string SecureWord { get; set; }
    }
}