﻿using SunCity.Common.MediatR;
using SunCity.Game.World.Forms.Property.Business.Bank;
using SunCity.Game.World.Models.Property.Business.Bank.Card.Transaction;

namespace SunCity.Game.World.Property.Business.Bank.Operations.GetTransactions
{
    public class BankGetCardTransactionsRequest : IOperationRequest<BankCardTransactionPreviewModel[]>
    {
        public BankAuthorizationForm Credentials { get; set; }
    }
}