﻿using System.Threading;
using System.Threading.Tasks;
using MediatR;
using SunCity.Common.MediatR;
using SunCity.Common.Operation;
using SunCity.Game.World.Models.Property.Business.Bank.Card.Transaction;
using SunCity.Game.World.Services.Property.Category.Business.Bank.Service.Client.BankCard;

namespace SunCity.Game.World.Property.Business.Bank.Operations.GetTransactions
{
    public class CanBankGetCardTransactionsBehavior : IOperationPipelineBehavior<BankGetCardTransactionsRequest, BankCardTransactionPreviewModel[]>
    {
        private IBankCardAuthorizationService BankCardAuthorizationService { get; }

        public CanBankGetCardTransactionsBehavior(IBankCardAuthorizationService bankCardAuthorizationService)
        {
            BankCardAuthorizationService = bankCardAuthorizationService;
        }

        public async Task<OperationResponse<BankCardTransactionPreviewModel[]>> Handle(BankGetCardTransactionsRequest request, CancellationToken cancellationToken,
            RequestHandlerDelegate<OperationResponse<BankCardTransactionPreviewModel[]>> next)
        {
            var authorization = await BankCardAuthorizationService.Authorization(request.Credentials);
            if (authorization.IsNotCorrect)
                return authorization.Error;
            
            return await next();
        }
    }
}