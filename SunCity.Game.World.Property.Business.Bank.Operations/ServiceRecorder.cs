﻿using System;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using SunCity.Common.MediatR.Extensions;
using SunCity.Game.World.Models.Property.Business.Bank.Card;
using SunCity.Game.World.Models.Property.Business.Bank.Card.Transaction;
using SunCity.Game.World.Property.Business.Bank.Operations.Authorization;
using SunCity.Game.World.Property.Business.Bank.Operations.Deposit;
using SunCity.Game.World.Property.Business.Bank.Operations.GetBalance;
using SunCity.Game.World.Property.Business.Bank.Operations.GetInformation;
using SunCity.Game.World.Property.Business.Bank.Operations.GetList;
using SunCity.Game.World.Property.Business.Bank.Operations.GetOwner;
using SunCity.Game.World.Property.Business.Bank.Operations.GetTransactions;
using SunCity.Game.World.Property.Business.Bank.Operations.OpenCard;
using SunCity.Game.World.Property.Business.Bank.Operations.Transfer;
using SunCity.Game.World.Property.Business.Bank.Operations.Withdraw;

namespace SunCity.Game.World.Property.Business.Bank.Operations
{
    public static class ServiceRecorder
    {
        public static IServiceCollection AddBankPlayerClientOperations(this IServiceCollection InServiceCollection, IConfiguration InConfiguration)
        {
            InServiceCollection.AddBankAuthorizationRequest(InConfiguration);
            InServiceCollection.AddBankDepositCardRequest(InConfiguration);
            
            InServiceCollection.AddBankGetCardBalanceRequest(InConfiguration);
            InServiceCollection.AddBankGetCardInformationRequest(InConfiguration);
            
            InServiceCollection.AddBankGetCardTransactionsRequest(InConfiguration);
            InServiceCollection.AddBankOpenCardRequest(InConfiguration);
            
            InServiceCollection.AddBankTransferToCardRequest(InConfiguration);
            InServiceCollection.AddBankWithdrawCardRequest(InConfiguration);
            
            InServiceCollection.AddBankGetCardOwnerRequest(InConfiguration);
            InServiceCollection.AddBankGetCardListRequest(InConfiguration);
            return InServiceCollection;
        }

        private static void AddBankAuthorizationRequest(this IServiceCollection InServiceCollection, IConfiguration InConfiguration)
        {
            InServiceCollection.AddRequestHandler<BankAuthorizationRequest, BankAuthorizationHandler>();
        }

        private static void AddBankDepositCardRequest(this IServiceCollection InServiceCollection, IConfiguration InConfiguration)
        {
            InServiceCollection.AddPipelineBehavior<BankDepositCardRequest, BankDepositCardResponse, CanBankDepositCardBehavior>();
            InServiceCollection.AddRequestHandler<BankDepositCardRequest, BankDepositCardResponse, BankDepositCardHandler>();
        }

        private static void AddBankGetCardBalanceRequest(this IServiceCollection InServiceCollection, IConfiguration InConfiguration)
        {
            InServiceCollection.AddPipelineBehavior<BankGetCardBalanceRequest, BankCardBalanceModel, CanBankGetCardBalanceBehavior>();
            InServiceCollection.AddRequestHandler<BankGetCardBalanceRequest, BankCardBalanceModel, BankGetCardBalanceHandler>();
        }

        private static void AddBankGetCardInformationRequest(this IServiceCollection InServiceCollection, IConfiguration InConfiguration)
        {
            InServiceCollection.AddPipelineBehavior<BankGetCardInformationRequest, BankGetCardInformationResponse, CanBankGetCardInformationBehavior>();
            InServiceCollection.AddRequestHandler<BankGetCardInformationRequest, BankGetCardInformationResponse, BankGetCardInformationHandler>();
        }

        private static void AddBankGetCardTransactionsRequest(this IServiceCollection InServiceCollection, IConfiguration InConfiguration)
        {
            InServiceCollection.AddPipelineBehavior<BankGetCardTransactionsRequest, BankCardTransactionPreviewModel[], CanBankGetCardTransactionsBehavior>();
            InServiceCollection.AddRequestHandler<BankGetCardTransactionsRequest, BankCardTransactionPreviewModel[], BankGetCardTransactionsHandler>();
        }

        private static void AddBankOpenCardRequest(this IServiceCollection InServiceCollection, IConfiguration InConfiguration)
        {
            InServiceCollection.AddPipelineBehavior<BankOpenCardRequest, BankOpenCardResponse, CanBankOpenCardBehavior>();
            InServiceCollection.AddRequestHandler<BankOpenCardRequest, BankOpenCardResponse, BankOpenCardHandler>();
        }

        private static void AddBankTransferToCardRequest(this IServiceCollection InServiceCollection, IConfiguration InConfiguration)
        {
            InServiceCollection.AddPipelineBehavior<BankTransferToCardRequest, BankTransferToCardResponse, CanBankTransferToCardBehavior>();
            InServiceCollection.AddRequestHandler<BankTransferToCardRequest, BankTransferToCardResponse, BankTransferToCardHandler>();
        }

        private static void AddBankWithdrawCardRequest(this IServiceCollection InServiceCollection, IConfiguration InConfiguration)
        {
            InServiceCollection.AddPipelineBehavior<BankWithdrawCardRequest, BankWithdrawCardResponse, CanBankWithdrawCardBehavior>();
            InServiceCollection.AddRequestHandler<BankWithdrawCardRequest, BankWithdrawCardResponse, BankWithdrawCardHandler>();
        }

        private static void AddBankGetCardOwnerRequest(this IServiceCollection InServiceCollection, IConfiguration InConfiguration)
        {
            InServiceCollection.AddRequestHandler<BankGetCardOwnerRequest, BankCardOwnerModel, BankGetCardOwnerHandler>();
        }

        private static void AddBankGetCardListRequest(this IServiceCollection InServiceCollection, IConfiguration InConfiguration)
        {
            InServiceCollection.AddRequestHandler<BankGetCardListRequest, BankCardPreviewModel[], BankGetCardListHandler>();
        }
    }
}