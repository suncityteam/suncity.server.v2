﻿using System;
using AutoMapper;
using SunCity.Game.World.DataBase.Entitites.Property.Business.Bank.Account;
using SunCity.Game.World.DataBase.Entitites.Property.Business.Bank.Tariff;
using SunCity.Game.World.Enums.Property.Business.Bank;
using SunCity.Game.World.Models.Property.Business.Bank;
using SunCity.Game.World.Models.Property.Business.Bank.Card;
using SunCity.Game.World.Property.Business.Bank.Operations.GetInformation;

namespace SunCity.Game.World.Property.Business.Bank.Operations
{
    public sealed class MappingProfile : Profile
    {
        public MappingProfile() : base("SunCity.Game.World.Models")
        {
            CreateMap<BankCardAccountEntity, BankCardPreviewModel>()
                .ForMember(q => q.Validity, q => q.MapFrom(w => w.ExpiredDate));

            CreateMap<BankCardAccountEntity, BankGetCardInformationResponse>()
                .ForMember(q => q.Validity, q => q.MapFrom(w => w.ExpiredDate))
                .ForMember(q => q.CreatedAt, q => q.MapFrom(w => w.CreatedDate))
                .ForMember(q => q.Status, q => q.MapFrom<BankCardStatus>(MapBankCardStatus));

            CreateMap<BankCardTariffEntity, BankCardTariffModel>();
        }

        private static BankCardStatus MapBankCardStatus(BankCardAccountEntity InEntity, BankGetCardInformationResponse InModel)
        {
            if (InEntity.IsExpired)
            {
                return BankCardStatus.Expired;
            }

            return BankCardStatus.Active;
        }
    }
}
