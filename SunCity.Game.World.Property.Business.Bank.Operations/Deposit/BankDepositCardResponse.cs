﻿namespace SunCity.Game.World.Property.Business.Bank.Operations.Deposit
{
    public class BankDepositCardResponse
    {
        public long AmountWithoutCommission { get; set; }
        public long AmountWithCommission { get; set; }
        public BankCardTransactionId TransactionId { get; set; }
    }
}