﻿using System.ComponentModel.DataAnnotations;
using SunCity.Common.MediatR;
using SunCity.Game.World.Forms.Property.Business.Bank;

namespace SunCity.Game.World.Property.Business.Bank.Operations.Deposit
{
    public class BankDepositCardRequest : IOperationRequest<BankDepositCardResponse>
    {
        [Required]
        public BankAuthorizationForm Credentials { get; set; }
        
        [Required]
        public long Amount { get; set; }
    }
}