﻿using System.Threading;
using System.Threading.Tasks;
using MediatR;
using SunCity.Common.MediatR;
using SunCity.Common.Operation;
using SunCity.Game.World.Services.Property.Category.Business.Bank.Service.Client.BankCard;

namespace SunCity.Game.World.Property.Business.Bank.Operations.Deposit
{
    public class CanBankDepositCardBehavior : IOperationPipelineBehavior<BankDepositCardRequest, BankDepositCardResponse>
    {
        private IBankCardAuthorizationService BankCardAuthorizationService { get; }

        public CanBankDepositCardBehavior(IBankCardAuthorizationService bankCardAuthorizationService)
        {
            BankCardAuthorizationService = bankCardAuthorizationService;
        }

        public async Task<OperationResponse<BankDepositCardResponse>> Handle(BankDepositCardRequest request, CancellationToken cancellationToken,
            RequestHandlerDelegate<OperationResponse<BankDepositCardResponse>> next)
        {
            if (request.Amount <= 0)
                return OperationResponseCode.BankInvalidDepositAmount;
            
            var authorization = await BankCardAuthorizationService.Authorization(request.Credentials);
            if (authorization.IsNotCorrect)
                return authorization.Error;
            
            return await next();
        }
    }
}