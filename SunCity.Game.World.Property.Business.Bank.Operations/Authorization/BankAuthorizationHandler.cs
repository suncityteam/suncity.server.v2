﻿using System.Threading;
using System.Threading.Tasks;
using SunCity.Common.MediatR;
using SunCity.Common.Operation;
using SunCity.Game.World.Services.Property.Category.Business.Bank.Service.Client.BankCard;

namespace SunCity.Game.World.Property.Business.Bank.Operations.Authorization
{
    public class BankAuthorizationHandler : IOperationHandler<BankAuthorizationRequest>
    {      
        private IBankCardAuthorizationService BankCardAuthorizationService { get; }

        public BankAuthorizationHandler(IBankCardAuthorizationService bankCardAuthorizationService)
        {
            BankCardAuthorizationService = bankCardAuthorizationService;
        }
        
        public async Task<OperationResponse> Handle(BankAuthorizationRequest request, CancellationToken cancellationToken)
        {
            var authorization = await BankCardAuthorizationService.Authorization(request.Credentials);
            return authorization;
        }
    }
}