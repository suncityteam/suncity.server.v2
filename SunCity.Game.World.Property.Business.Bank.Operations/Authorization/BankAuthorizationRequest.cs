﻿using System.ComponentModel.DataAnnotations;
using SunCity.Common.MediatR;
using SunCity.Game.World.Forms.Property.Business.Bank;

namespace SunCity.Game.World.Property.Business.Bank.Operations.Authorization
{
    public class BankAuthorizationRequest : IOperationRequest
    {
        //[Required]
        public BankAuthorizationForm Credentials { get; set; }

        public override string ToString()
        {
            return Credentials.ToString();
        }
    }
}