﻿using System.Threading;
using System.Threading.Tasks;
using SunCity.Common.MediatR;
using SunCity.Common.Operation;
using SunCity.Game.World.Player;
using SunCity.Game.World.Services.Property.Category.Business.Bank.DbQueries.Client.BankCard.Player;
using SunCity.Game.World.Services.Property.Category.Business.Bank.Forms;

namespace SunCity.Game.World.Property.Business.Bank.Operations.Withdraw
{
    public class BankWithdrawCardHandler : IOperationHandler<BankWithdrawCardRequest, BankWithdrawCardResponse>
    {
        private IBankCardPlayerDbQueries BankCardDbQueries { get; }
        private WorldPlayer WorldPlayer { get; }

        public BankWithdrawCardHandler(IBankCardPlayerDbQueries bankCardDbQueries, WorldPlayer worldPlayer)
        {
            BankCardDbQueries = bankCardDbQueries;
            WorldPlayer = worldPlayer;
        }

        public async Task<OperationResponse<BankWithdrawCardResponse>> Handle(BankWithdrawCardRequest request, CancellationToken cancellationToken)
        {
            var bankCard = await BankCardDbQueries.GetById(request.Credentials.PayerBankCardId);

            var transaction = await BankCardDbQueries.Deposit(new BankDepositDbForm
            {
                EntityId = request.Credentials.PayerBankCardId,
                Amount = request.Amount,
                CharacterId = WorldPlayer.EntityId
            });
            
            WorldPlayer.GiveMoney(request.Amount);

            WorldPlayer.SendClientMessage(new[]
            {
                $"Вы сняли {request.Amount}$ со счета {bankCard.Name}, остаток на счете {bankCard.Balance}",
                "Спасибо что воспользовались услугами нашего банка!"
            });

            return new BankWithdrawCardResponse
            {
                TransactionId = transaction.EntityId,
                AmountWithCommission = request.Amount,
                AmountWithoutCommission = request.Amount
            };
        }
    }
}