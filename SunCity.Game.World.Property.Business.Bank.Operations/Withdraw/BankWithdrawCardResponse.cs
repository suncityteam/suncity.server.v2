﻿namespace SunCity.Game.World.Property.Business.Bank.Operations.Withdraw
{
    public class BankWithdrawCardResponse
    {
        public long AmountWithoutCommission { get; set; }
        public long AmountWithCommission { get; set; }
        public BankCardTransactionId TransactionId { get; set; }
    }
}