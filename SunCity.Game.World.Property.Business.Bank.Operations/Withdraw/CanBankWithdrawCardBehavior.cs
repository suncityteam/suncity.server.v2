﻿using System.Threading;
using System.Threading.Tasks;
using MediatR;
using SunCity.Common.MediatR;
using SunCity.Common.Operation;
using SunCity.Game.World.Services.Property.Category.Business.Bank.DbQueries.Client.BankCard.Player;
using SunCity.Game.World.Services.Property.Category.Business.Bank.Service.Client.BankCard;

namespace SunCity.Game.World.Property.Business.Bank.Operations.Withdraw
{
    public class CanBankWithdrawCardBehavior : IOperationPipelineBehavior<BankWithdrawCardRequest, BankWithdrawCardResponse>
    {
        private IBankCardPlayerDbQueries BankCardDbQueries { get; }
        private IBankCardAuthorizationService BankCardAuthorizationService { get; }
        
        public CanBankWithdrawCardBehavior(IBankCardAuthorizationService bankCardAuthorizationService, IBankCardPlayerDbQueries bankCardDbQueries)
        {
            BankCardAuthorizationService = bankCardAuthorizationService;
            BankCardDbQueries = bankCardDbQueries;
        }

        public async Task<OperationResponse<BankWithdrawCardResponse>> Handle(BankWithdrawCardRequest request, CancellationToken cancellationToken,
            RequestHandlerDelegate<OperationResponse<BankWithdrawCardResponse>> next)
        {
            if (request.Amount <= 0)
                return OperationResponseCode.BankInvalidDepositAmount;
            
            var authorization = await BankCardAuthorizationService.Authorization(request.Credentials);
            if (authorization.IsNotCorrect)
                return authorization.Error;
            
            var bankCard = await BankCardDbQueries.GetById(request.Credentials.PayerBankCardId);
            if (request.Amount > bankCard.Balance)
                return OperationResponseCode.NotEnoughMoney;
            
            return await next();
        }
    }
}