﻿using System.ComponentModel.DataAnnotations;
using SunCity.Common.MediatR;
using SunCity.Game.World.Forms.Property.Business.Bank;

namespace SunCity.Game.World.Property.Business.Bank.Operations.Transfer
{
    public class BankTransferToCardRequest : IOperationRequest<BankTransferToCardResponse>
    {
        [Required]
        public BankAuthorizationForm Credentials { get; set; }
        public long PayeeBankCardNumber { get; set; }
        public long Amount { get; set; }
    }
}