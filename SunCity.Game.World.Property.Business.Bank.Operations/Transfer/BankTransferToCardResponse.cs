﻿namespace SunCity.Game.World.Property.Business.Bank.Operations.Transfer
{
    public class BankTransferToCardResponse
    {
        public long PayerBankCardNumber { get; set; }
        public long PayeeBankCardNumber { get; set; }

        public long AmountWithoutCommission { get; set; }
        public long AmountWithCommission { get; set; }
        public BankCardTransactionId TransactionId { get; set; }
    }
}