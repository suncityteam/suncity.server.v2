﻿using System.Threading;
using System.Threading.Tasks;
using MediatR;
using SunCity.Common.MediatR;
using SunCity.Common.Operation;
using SunCity.Game.World.Services.Property.Category.Business.Bank.DbQueries.Client.BankCard.Player;
using SunCity.Game.World.Services.Property.Category.Business.Bank.Service.Client.BankCard;

namespace SunCity.Game.World.Property.Business.Bank.Operations.Transfer
{
    public class CanBankTransferToCardBehavior : IOperationPipelineBehavior<BankTransferToCardRequest, BankTransferToCardResponse>
    {
        private IBankCardAuthorizationService BankCardAuthorizationService { get; }
        private IBankCardPlayerDbQueries DbQueries { get; }

        public CanBankTransferToCardBehavior(IBankCardAuthorizationService bankCardAuthorizationService, IBankCardPlayerDbQueries dbQueries)
        {
            BankCardAuthorizationService = bankCardAuthorizationService;
            DbQueries = dbQueries;
        }

        public async Task<OperationResponse<BankTransferToCardResponse>> Handle(BankTransferToCardRequest request, CancellationToken cancellationToken,
            RequestHandlerDelegate<OperationResponse<BankTransferToCardResponse>> next)
        {
            if (request.Amount <= 0)
                return OperationResponseCode.BankInvalidDepositAmount;

            var payerBankCard = await BankCardAuthorizationService.Authorization(request.Credentials);
            if (payerBankCard.IsNotCorrect)
                return payerBankCard.Error;
            
            var payeeBankCard = await DbQueries.GetByNumber(request.PayeeBankCardNumber);
            if (payeeBankCard == null)
                return OperationResponseCode.BankCardNotFound;

            if (payeeBankCard.IsExpired)
                return (OperationResponseCode.BankCardNotValid, payeeBankCard.Number);

            if (payerBankCard.Content.Balance < request.Amount)
                return OperationResponseCode.NotEnoughMoney;
            
            return await next();
        }
    }
}