﻿using System.Threading;
using System.Threading.Tasks;
using SunCity.Common.MediatR;
using SunCity.Common.Operation;
using SunCity.Game.World.Services.Property.Category.Business.Bank.DbQueries.Client.BankCard.Player;
using SunCity.Game.World.Services.Property.Category.Business.Bank.Forms;

namespace SunCity.Game.World.Property.Business.Bank.Operations.Transfer
{
    public class BankTransferToCardHandler : IOperationHandler<BankTransferToCardRequest, BankTransferToCardResponse>
    {
        private IBankCardPlayerDbQueries DbQueries { get; }

        public BankTransferToCardHandler(IBankCardPlayerDbQueries dbQueries)
        {
            DbQueries = dbQueries;
        }
        
        public async Task<OperationResponse<BankTransferToCardResponse>> Handle(BankTransferToCardRequest request, CancellationToken cancellationToken)
        {
            var payerBankCard = await DbQueries.GetById(request.Credentials.PayerBankCardId);
            var payeeBankCard = await DbQueries.GetByNumber(request.PayeeBankCardNumber);
            
            var transaction = await DbQueries.Transfer(new BankTransferDbForm
            {
                PayerId = request.Credentials.PayerBankCardId,
                PayeeId = payeeBankCard.EntityId,
                Amount = request.Amount,
                Commission = 0
            });

            return new BankTransferToCardResponse
            {
                TransactionId = transaction.EntityId,

                AmountWithCommission = transaction.Amount,
                AmountWithoutCommission = transaction.Amount,

                PayeeBankCardNumber = payerBankCard.Number,
                PayerBankCardNumber = payerBankCard.Number
            };
        }
    }
}