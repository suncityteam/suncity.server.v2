﻿using System.ComponentModel.DataAnnotations;
using SunCity.Common.MediatR;
using SunCity.Game.World.Forms.Property.Business.Bank;

namespace SunCity.Game.World.Property.Business.Bank.Operations.GetInformation
{
    public class BankGetCardInformationRequest : IOperationRequest<BankGetCardInformationResponse>
    {
        [Required]
        public BankAuthorizationForm Credentials { get; set; }
    }
}