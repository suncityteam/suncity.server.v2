﻿using System;
using SunCity.Common.Enum;
using SunCity.Game.World.Enums.Property.Business.Bank;
using SunCity.Game.World.Models.Property.Business.Bank.Card;
using SunCity.Game.World.Models.Property.Business.Bank.Card.Transaction;

namespace SunCity.Game.World.Property.Business.Bank.Operations.GetInformation
{
    public class BankGetCardInformationResponse: BankCardPreviewModel
    {
        public long Balance { get; set; }
        public DateTime CreatedAt { get; set; }
        
        public string Tariff { get; set; }
        
        public EnumDescription<BankCardStatus> Status { get; set; }
        
        public long? LastPaymentAmount { get; set; }
        public BankCardTransactionPreviewModel[] Transactions { get; set; }

        public override string ToString()
        {
            return $"{Status.Description} / {base.ToString()}, Balance: {Balance}, Tariff: {Tariff}, CreatedAt: {CreatedAt:d}";
        }
    }
}