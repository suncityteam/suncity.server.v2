﻿using System.Threading;
using System.Threading.Tasks;
using MediatR;
using SunCity.Common.MediatR;
using SunCity.Common.Operation;
using SunCity.Game.World.Services.Property.Category.Business.Bank.Service.Client.BankCard;

namespace SunCity.Game.World.Property.Business.Bank.Operations.GetInformation
{
    public class CanBankGetCardInformationBehavior : IOperationPipelineBehavior<BankGetCardInformationRequest, BankGetCardInformationResponse>
    {
        private IBankCardAuthorizationService BankCardAuthorizationService { get; }

        public CanBankGetCardInformationBehavior(IBankCardAuthorizationService bankCardAuthorizationService)
        {
            BankCardAuthorizationService = bankCardAuthorizationService;
        }

        public async Task<OperationResponse<BankGetCardInformationResponse>> Handle(BankGetCardInformationRequest request, CancellationToken cancellationToken,
            RequestHandlerDelegate<OperationResponse<BankGetCardInformationResponse>> next)
        {
            var auth = await BankCardAuthorizationService.Authorization(request.Credentials);
            if (auth.IsNotCorrect)
                return auth.Error;

            return await next();
        }
    }
}