﻿using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using SunCity.Common.MediatR;
using SunCity.Common.Operation;
using SunCity.Game.World.Enums.Property.Business.Bank;
using SunCity.Game.World.Services.Property.Category.Business.Bank.DbQueries.Client.BankCard.Player;
using SunCity.Game.World.Services.Property.Category.Business.Bank.Service.Client.Tariff;
using SunCity.Game.World.Services.Property.Category.Business.Bank.Service.Client.Transaction;

namespace SunCity.Game.World.Property.Business.Bank.Operations.GetInformation
{
    public class BankGetCardInformationHandler : IOperationHandler<BankGetCardInformationRequest, BankGetCardInformationResponse>
    {
        private IBankCardPlayerDbQueries DbQueries { get; }
        private IBankCardTariffService BankCardTariffService { get; }
        private IBankCardTransactionService BankCardTransactionService { get; }
        private IMapper Mapper { get; }

        public BankGetCardInformationHandler(IBankCardPlayerDbQueries dbQueries, IBankCardTariffService bankCardTariffService, IMapper mapper, IBankCardTransactionService bankCardTransactionService)
        {
            DbQueries = dbQueries;
            BankCardTariffService = bankCardTariffService;
            Mapper = mapper;
            BankCardTransactionService = bankCardTransactionService;
        }

        public async Task<OperationResponse<BankGetCardInformationResponse>> Handle(BankGetCardInformationRequest request, CancellationToken cancellationToken)
        {
            var bankCard = await DbQueries.GetById(request.Credentials.PayerBankCardId);

            var tariff = await BankCardTariffService.GetById(bankCard.TariffId);
            if (tariff.IsNotCorrect)
                return tariff.Error;

            var transactions = await BankCardTransactionService.GetTransactions(bankCard.EntityId);
            if (transactions.IsNotCorrect)
                return transactions.Error;

            var model = Mapper.Map<BankGetCardInformationResponse>(bankCard);
            model.Transactions = transactions.Content;
            if (transactions.Content.Any())
            {
                var transaction = transactions.Content[0];
                if (transaction.Category == BankCardTransactionCategory.Deposit || transaction.Category == BankCardTransactionCategory.TransferFor)
                {
                    model.LastPaymentAmount = transaction.Amount;
                }
                else
                {
                    model.LastPaymentAmount = -transaction.Amount;
                }
            }

            model.Tariff = tariff.Content.Name;

            return model;
        }
    }
}