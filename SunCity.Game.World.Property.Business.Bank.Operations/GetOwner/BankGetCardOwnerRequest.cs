﻿using SunCity.Common.MediatR;
using SunCity.Game.World.Forms.Property.Business.Bank;
using SunCity.Game.World.Models.Property.Business.Bank.Card;

namespace SunCity.Game.World.Property.Business.Bank.Operations.GetOwner
{
    public class BankGetCardOwnerRequest 
        : BankCardOwnerForm
        , IOperationRequest<BankCardOwnerModel>
    {

    }
}