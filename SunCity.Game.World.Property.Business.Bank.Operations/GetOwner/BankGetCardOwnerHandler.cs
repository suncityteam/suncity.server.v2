﻿using System.Threading;
using System.Threading.Tasks;
using SunCity.Common.MediatR;
using SunCity.Common.Operation;
using SunCity.Game.World.Models.Property.Business.Bank.Card;
using SunCity.Game.World.Services.Property.Category.Business.Bank.Service.Client.BankCard.Owners;

namespace SunCity.Game.World.Property.Business.Bank.Operations.GetOwner
{
    public class BankGetCardOwnerHandler : IOperationHandler<BankGetCardOwnerRequest, BankCardOwnerModel>
    {
        private IBankCardOwnersService BankCardOwnersService { get; }

        public BankGetCardOwnerHandler(IBankCardOwnersService bankCardOwnersService)
        {
            BankCardOwnersService = bankCardOwnersService;
        }

        public async Task<OperationResponse<BankCardOwnerModel>> Handle(BankGetCardOwnerRequest request, CancellationToken cancellationToken)
        {
            var owner = await BankCardOwnersService.GetOwner(request);
            return owner;
        }
    }
}