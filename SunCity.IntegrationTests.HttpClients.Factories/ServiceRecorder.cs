﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace SunCity.IntegrationTests.HttpClients.Factories
{
    public static class ServiceRecorder
    {
        public static IServiceCollection AddTestServerHttpClient(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddSingleton<ITestServerHttpClient, TestServerHttpClient>();
            return services;
        }
    }
}