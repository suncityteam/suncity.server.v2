﻿using System;
using System.IO;
using System.Net.Http;
using System.Net.Http.Headers;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc.Testing;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using SunCity.Common;
using SunCity.Common.Extensions;
using SunCity.Common.HttpClient;
using SunCity.Common.Json;
using SunCity.Common.RageMp.Pools.Players;
using SunCity.Common.RageMp.Server;
using SunCity.Server.Test;

namespace SunCity.IntegrationTests.HttpClients.Factories
{
    internal sealed class TestServerHttpClient : BaseHttpClient, ITestServerHttpClient
    {
        private IRageMpServer RageMpServer { get; }
        private WebApplicationFactory<TestWebServerStartup> Factory { get; }

        public TestServerHttpClient(ICommonJsonSerializer commonJsonSerializer, IRageMpServer rageMpServer) : base(commonJsonSerializer)
        {
            RageMpServer = rageMpServer;
            Factory =
                new WebApplicationFactory<TestWebServerStartup>()
                    .WithWebHostBuilder(Configuration);
        }

        private void Configuration(IWebHostBuilder hostBuilder)
        {
            hostBuilder.ConfigureServices(ConfigureServices);
            hostBuilder.ConfigureAppConfiguration(ConfigureAppConfiguration);
        }

        private void ConfigureServices(WebHostBuilderContext context, IServiceCollection services)
        {
            services.ReplaceService(RageMpServer);
            services.ReplaceService(RageMpServer.TaskManager);
            services.ReplaceService(RageMpServer.World);

            services.ReplaceService(RageMpServer.Pool);
            services.ReplaceService(RageMpServer.Pool.Players);
            services.ReplaceService(RageMpServer.Pool.Vehicles);

            services.ReplaceService(RageMpServer.Pool.Blips);
            services.ReplaceService(RageMpServer.Pool.Markers);
            services.ReplaceService(RageMpServer.Pool.TextLabels);
        }

        private void ConfigureAppConfiguration(WebHostBuilderContext context, IConfigurationBuilder configurationBuilder)
        {
            var root = Directory.GetCurrentDirectory();
            configurationBuilder.AddEnvironmentVariables(EnvironmentNames.INTEGRATION_TESTS);
            context.HostingEnvironment.EnvironmentName = EnvironmentNames.INTEGRATION_TESTS;
        }

        protected override HttpClient CreateClient(Action<HttpRequestHeaders> customizeHeaders)
        {
            var httpClient = Factory.CreateClient();
            customizeHeaders?.Invoke(httpClient.DefaultRequestHeaders);
            return httpClient;
        }
    }
}