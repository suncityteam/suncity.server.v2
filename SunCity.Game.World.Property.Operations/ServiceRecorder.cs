﻿using System;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using SunCity.Common.MediatR.Extensions;
using SunCity.Game.World.Property.Operations.Information;
using SunCity.Game.World.Property.Operations.Purchase;
using SunCity.Game.World.Property.Operations.SellingInformation;

namespace SunCity.Game.World.Property.Operations
{
    public static class ServiceRecorder
    {
        public static IServiceCollection AddWorldPropertyOperationHandlers(this IServiceCollection InServiceCollection, IConfiguration InConfiguration)
        {            
            InServiceCollection.AddGetWorldPropertySellingInformationRequest(InConfiguration);
            InServiceCollection.AddPurchaseWorldPropertyRequest(InConfiguration);
            InServiceCollection.AddGetWorldPropertyInformationRequest(InConfiguration);
            return InServiceCollection;
        }

        private static void AddGetWorldPropertyInformationRequest(this IServiceCollection InServiceCollection, IConfiguration InConfiguration)
        {
            //InServiceCollection.AddPipelineBehavior<GetWorldPropertyInformationRequest, GetWorldPropertyInformationResponse, CanGetWorldPropertySellingInformationBehavior>();
            InServiceCollection.AddRequestHandler<GetWorldPropertyInformationRequest, GetWorldPropertyInformationResponse, GetWorldPropertyInformationHandler>();
        }
        
        private static void AddGetWorldPropertySellingInformationRequest(this IServiceCollection InServiceCollection, IConfiguration InConfiguration)
        {
            InServiceCollection.AddPipelineBehavior<GetWorldPropertySellingInformationRequest, GetWorldPropertySellingInformationResponse, CanGetWorldPropertySellingInformationBehavior>();
            InServiceCollection.AddRequestHandler<GetWorldPropertySellingInformationRequest, GetWorldPropertySellingInformationResponse, GetWorldPropertySellingInformationHandler>();
        }
        
        private static void AddPurchaseWorldPropertyRequest(this IServiceCollection InServiceCollection, IConfiguration InConfiguration)
        {
            InServiceCollection.AddPipelineBehavior<PurchaseWorldPropertyRequest, PurchaseWorldPropertyResponse, CanPurchaseWorldPropertyBehavior>();
            InServiceCollection.AddRequestHandler<PurchaseWorldPropertyRequest, PurchaseWorldPropertyResponse, PurchaseWorldPropertyHandler>();
        }
    }
}