﻿using System.Collections.Generic;
using SunCity.Common.Enum;
using SunCity.Game.Enums.World.Property;
using SunCity.Game.Types.Cost;
using SunCity.Game.World.Models.Licenses;

namespace SunCity.Game.World.Property.Operations.SellingInformation
{
    public class GetWorldPropertySellingInformationResponse
    {
        public PropertyId EntityId { get; set; }

        public IReadOnlyList<LicenseModel> RequiredLicenses { get; set; }
        public EnumDescription<WorldPropertyCategory> Category { get; set; }
        public EnumDescription<WorldPropertyClass> Class { get; set; }

        public float Square { get; set; }
        public int VatRate { get; set; }

        public GameCost SquareCost { get; set; }

        public GameCost StateCost { get; set; }
        public GameCost SellerCost { get; set; }
        public float AgencyCommission { get; set; }
    }
}