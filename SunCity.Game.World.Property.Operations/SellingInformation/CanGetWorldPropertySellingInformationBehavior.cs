﻿using System.Threading;
using System.Threading.Tasks;
using MediatR;
using SunCity.Common.MediatR;
using SunCity.Common.Operation;

namespace SunCity.Game.World.Property.Operations.SellingInformation
{
    public class CanGetWorldPropertySellingInformationBehavior : IOperationPipelineBehavior<GetWorldPropertySellingInformationRequest, GetWorldPropertySellingInformationResponse>
    {
        private IWorldPropertyStorage PropertyStorage { get; }

        public Task<OperationResponse<GetWorldPropertySellingInformationResponse>> Handle(GetWorldPropertySellingInformationRequest request, CancellationToken cancellationToken,
            RequestHandlerDelegate<OperationResponse<GetWorldPropertySellingInformationResponse>> next)
        {
            var property = PropertyStorage[request.PropertyId];
            if (property == null)
                return Task.FromResult<OperationResponse<GetWorldPropertySellingInformationResponse>>((OperationResponseCode.PropertyIdNotFound, request.PropertyId));

            if (property.IsNotSelling)
                return Task.FromResult<OperationResponse<GetWorldPropertySellingInformationResponse>>((OperationResponseCode.PropertyIsNotSelling, request.PropertyId));

            return next();
        }
    }
}