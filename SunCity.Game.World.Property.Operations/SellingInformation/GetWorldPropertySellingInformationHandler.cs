﻿using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using SunCity.Common.MediatR;
using SunCity.Common.Operation;
using SunCity.Game.World.Services.License.Services;

namespace SunCity.Game.World.Property.Operations.SellingInformation
{
    public class GetWorldPropertySellingInformationHandler : IOperationHandler<GetWorldPropertySellingInformationRequest, GetWorldPropertySellingInformationResponse>
    {        
        private IWorldPropertyStorage PropertyStorage { get; }
        private IWorldLicensesService LicensesService { get; }

        public async Task<OperationResponse<GetWorldPropertySellingInformationResponse>> Handle(GetWorldPropertySellingInformationRequest request, CancellationToken cancellationToken)
        {
            var property = PropertyStorage[request.PropertyId];

            var model = MapToSellingModel(property);
            return model;
        }
        
        private GetWorldPropertySellingInformationResponse MapToSellingModel(WorldProperty InWorldProperty)
        {
            var licenseIds = InWorldProperty.RequiredLicenses.Select(q => q.LicenseId).ToArray();
            var licenses = LicensesService.GetById(licenseIds);
            
            return new GetWorldPropertySellingInformationResponse
            {
                EntityId = InWorldProperty.EntityId,

                StateCost = InWorldProperty.StateCost,
                SellerCost = InWorldProperty.SellingCost ?? InWorldProperty.StateCost,

                Category = InWorldProperty.PropertyCategory,
                Class = InWorldProperty.PropertyClass,
                
                RequiredLicenses = licenses,

                Square = 100
            };
        }
    }
}