﻿using SunCity.Common.MediatR;

namespace SunCity.Game.World.Property.Operations.SellingInformation
{
    public class GetWorldPropertySellingInformationRequest 
        : IOperationRequest<GetWorldPropertySellingInformationResponse>
        , IEntityWithPropertyId
    {
        public PropertyId PropertyId { get; set; }
    }
}