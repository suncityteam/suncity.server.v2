﻿using SunCity.Common.MediatR;
using SunCity.Game.World.Forms.Payment;
using SunCity.Game.World.Forms.Property.Business.Bank;
using SunCity.Game.World.Player;

namespace SunCity.Game.World.Property.Operations.Purchase
{
    public class PurchaseWorldPropertyRequest : IOperationRequest<PurchaseWorldPropertyResponse>
    {
        public PropertyId PropertyId { get; set; }
        public BankAuthorizationForm Payment { get; set; }
    }
}