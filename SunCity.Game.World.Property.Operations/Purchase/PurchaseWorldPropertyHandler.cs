﻿using System.Threading;
using System.Threading.Tasks;
using SunCity.Common.MediatR;
using SunCity.Common.Operation;
using SunCity.Game.Enums.World.Property.Owner;
using SunCity.Game.World.Fraction;
using SunCity.Game.World.Item.Factory;
using SunCity.Game.World.Models.Property.Owner;
using SunCity.Game.World.Player;
using SunCity.Game.World.Services.Property.Category.Business.Bank.Forms.Payments;
using SunCity.Game.World.Services.Property.Category.Business.Bank.Service.Client.BankCard.Player;
using SunCity.Game.World.Services.Property.Category.Business.Bank.Service.Client.Payments;
using SunCity.Game.World.Services.Property.DbQueries;
using SunCity.Game.World.Services.Property.Forms;

namespace SunCity.Game.World.Property.Operations.Purchase
{
    internal class PurchaseWorldPropertyHandler : IOperationHandler<PurchaseWorldPropertyRequest, PurchaseWorldPropertyResponse>
    {
        private IWorldPropertyStorage PropertyStorage { get; }
        private IBankCardPropertyPaymentsService PropertyPaymentsService { get; }
        private IBankCardPlayerService BankCardService { get; }
        private IPropertyOwnerDbQueries PropertyOwnerDbQueries { get; }
        private IWorldItemFactory WorldItemFactory { get; }
        private IWorldFractionStorage FractionStorage { get; }
        private WorldPlayer WorldPlayer { get; }
        public PurchaseWorldPropertyHandler
        (
            IWorldPropertyStorage InPropertyStorage,
            IBankCardPropertyPaymentsService InPropertyPaymentsService,
            IBankCardPlayerService InBankCardService,
            IWorldFractionStorage InFractionStorage,
            IPropertyOwnerDbQueries InPropertyOwnerDbQueries,
            IWorldItemFactory InWorldItemFactory
        )
        {
            PropertyStorage = InPropertyStorage;
            PropertyPaymentsService = InPropertyPaymentsService;
            BankCardService = InBankCardService;
            FractionStorage = InFractionStorage;
            PropertyOwnerDbQueries = InPropertyOwnerDbQueries;
            WorldItemFactory = InWorldItemFactory;
        }

        public async Task<OperationResponse<PurchaseWorldPropertyResponse>> Handle(PurchaseWorldPropertyRequest request, CancellationToken cancellationToken)
        {
            var property = PropertyStorage[request.PropertyId];

            var purchaseResponse = await Purchase(property, request.Payment.PayerBankCardId);
            if (purchaseResponse.IsNotCorrect)
                return purchaseResponse.Error;

            property.UpdateOwners(new WorldPropertyPlayerOwner
            {
                CharacterId = WorldPlayer.EntityId,
                Name = WorldPlayer.Name.Name,
                Type = PropertyOwnerType.Owner
            }, request.Payment.PayerBankCardId);

            await PropertyOwnerDbQueries.RemoveOwners(property.EntityId);
            await PropertyOwnerDbQueries.AddOwner(new AddPropertyOwnerDbForm
            {
                EntityId = property.EntityId,
                OwnerId = WorldPlayer.EntityId,
                Type = PropertyOwnerType.Owner
            });

            await PropertyOwnerDbQueries.RemoveFromSelling(new RemovePropertyFromSellingDbForm
            {
                EntityId = property.EntityId,
                BankCardId = request.Payment.PayerBankCardId,
            });

            for (var i = 0; i < 3; i++)
            {
                var propertyKey = WorldItemFactory.CreatePropertyKey(property.EntityId);
                WorldPlayer.Inventory.PutItem(propertyKey);
            }

            return new PurchaseWorldPropertyResponse();
        }

        private async Task<OperationResponse> Purchase(WorldProperty InWorldProperty, BankCardId InPayerBankCardId)
        {
            if (InWorldProperty.HasOwner)
            {
                return await Purchase(InWorldProperty, InPayerBankCardId, InWorldProperty.OwnerBankCardId);
            }

            return await Purchase(InWorldProperty, InPayerBankCardId, FractionStorage.Government.MainBankCardId);
        }

        private async Task<OperationResponse> Purchase(WorldProperty InWorldProperty, BankCardId InPayerBankCardId, BankCardId? InPayeeBankCardId)
        {
            if (!InPayeeBankCardId.HasValue)
                return (OperationResponseCode.PropertyRequisitesNotFilled, InWorldProperty.EntityId);

            var payeeIsValid = await BankCardService.Validate(InPayeeBankCardId.Value);
            if (payeeIsValid.IsNotCorrect)
                return payeeIsValid.Error;

            var paymentResult = await PropertyPaymentsService.PropertyBuy(new BankPropertyBuyDbForm
            {
                PropertyId = InWorldProperty.EntityId,
                PayerId = InPayerBankCardId,
                PayeeId = InPayeeBankCardId.Value,
                Cost = InWorldProperty.SellingCost.Amount
            });

            return paymentResult;
        }
    }
}