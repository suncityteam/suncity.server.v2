﻿using System.Threading;
using System.Threading.Tasks;
using MediatR;
using SunCity.Common.MediatR;
using SunCity.Common.Operation;
using SunCity.Game.Enums;
using SunCity.Game.World.Player;
using SunCity.Game.World.Services.Character.Licenses.Services;
using SunCity.Game.World.Services.Property.Category.Business.Bank.Service.Client.BankCard.Player;

namespace SunCity.Game.World.Property.Operations.Purchase
{
    public class CanPurchaseWorldPropertyBehavior: IOperationPipelineBehavior<PurchaseWorldPropertyRequest, PurchaseWorldPropertyResponse>
    {
        public CanPurchaseWorldPropertyBehavior(IWorldPropertyStorage propertyStorage, IBankCardPlayerService bankCardService, IPlayerLicensesService playerLicensesService)
        {
            PropertyStorage = propertyStorage;
            BankCardService = bankCardService;
            PlayerLicensesService = playerLicensesService;
        }

        private IWorldPropertyStorage PropertyStorage { get; }
        private IBankCardPlayerService BankCardService { get; }
        private IPlayerLicensesService PlayerLicensesService { get; }
        private WorldPlayer WorldPlayer { get; }
        
        public async Task<OperationResponse<PurchaseWorldPropertyResponse>> Handle(PurchaseWorldPropertyRequest request, CancellationToken cancellationToken, RequestHandlerDelegate<OperationResponse<PurchaseWorldPropertyResponse>> next)
        {
            var property = PropertyStorage[request.PropertyId];
            if (property == null)
                return (OperationResponseCode.PropertyIdNotFound, request.PropertyId);

            if(property.IsNotSelling)
                return (OperationResponseCode.PropertyIsNotSelling, request.PropertyId);

            if(property.SellingCost.Currency != GameCurrency.Money)
                return (OperationResponseCode.PropertySellingOnlyWithMoney, request.PropertyId);

            foreach (var requiredLicense in property.RequiredLicenses)
            {
                var existRequiredLicense = await PlayerLicensesService.IsExist(WorldPlayer.EntityId, requiredLicense.LicenseId);
                if(!existRequiredLicense)
                    return (OperationResponseCode.NotExistRequiredLicense);
            }
            
            var payerAuthorizationResult = await BankCardService.GetBalance(request.Payment);
            if (payerAuthorizationResult.IsNotCorrect)
                return payerAuthorizationResult.Error;

            if(payerAuthorizationResult.Content.Balance < property.SellingCost.Amount)
                return (OperationResponseCode.NotEnoughMoney);

            return await next();
        }
    }
}