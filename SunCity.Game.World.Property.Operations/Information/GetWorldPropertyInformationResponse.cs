﻿using SunCity.Common.Enum;
using SunCity.Game.Enums.World.Property;

namespace SunCity.Game.World.Property.Operations.Information
{
    public class GetWorldPropertyInformationResponse
    {
        public PropertyId PropertyId { get; set; }
        public string Name { get; set; }
        
        public EnumDescription<WorldPropertyClass> Class { get; set; }
        public EnumDescription<WorldPropertyCategory> Category { get; set; }
        
    }
}