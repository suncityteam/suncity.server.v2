﻿using SunCity.Common.MediatR;

namespace SunCity.Game.World.Property.Operations.Information
{
    public class GetWorldPropertyInformationRequest
        : IOperationRequest<GetWorldPropertyInformationResponse>
            , IEntityWithPropertyId
    {
        public PropertyId PropertyId { get; set; }
    }
}