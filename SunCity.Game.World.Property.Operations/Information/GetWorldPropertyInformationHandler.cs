﻿using System.Threading;
using System.Threading.Tasks;
using SunCity.Common.MediatR;
using SunCity.Common.Operation;
using SunCity.Game.World.Services.License.Services;

namespace SunCity.Game.World.Property.Operations.Information
{
    internal sealed class GetWorldPropertyInformationHandler : IOperationHandler<GetWorldPropertyInformationRequest, GetWorldPropertyInformationResponse>
    {
        private IWorldPropertyStorage PropertyStorage { get; }
        private IWorldLicensesService LicensesService { get; }

        public GetWorldPropertyInformationHandler(IWorldPropertyStorage propertyStorage, IWorldLicensesService licensesService)
        {
            PropertyStorage = propertyStorage;
            LicensesService = licensesService;
        }

        public async Task<OperationResponse<GetWorldPropertyInformationResponse>> Handle(GetWorldPropertyInformationRequest request, CancellationToken cancellationToken)
        {
            var property = PropertyStorage[request.PropertyId];
            return new GetWorldPropertyInformationResponse
            {
                PropertyId = property.EntityId,
                Category = property.PropertyCategory,
                Name = property.Name.Name,
                Class = property.PropertyClass,
            };
        }
    }
}