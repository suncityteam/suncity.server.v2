﻿using Microsoft.AspNetCore.Mvc;
using SunCity.Common.Operation;

namespace SunCity.Controllers
{
    public abstract class RageController : Controller
    {
        protected IActionResult Response()
        {
            return Json(OperationResponseCache.Successfully);
        }
        protected IActionResult Response(OperationResponseCode InOperationError)
        {
            return Json(new OperationResponse(InOperationError));
        }

        protected IActionResult Response(OperationError InOperationError)
        {
            return Json(new OperationResponse(InOperationError));
        }

        protected IActionResult Response<T>(T InResponse)
        {
            return Json(new OperationResponse<T>(InResponse));
        }

        protected IActionResult Response<T>(OperationResponse<T> InResponse)
        {
            return Json(InResponse);
        }

        protected IActionResult Response(OperationResponse InResponse)
        {
            return Json(InResponse);
        }

        protected IActionResult Response<T>(OperationError InOperationError)
        {
            return Json(new OperationResponse<T>(InOperationError));
        }
    }
}
