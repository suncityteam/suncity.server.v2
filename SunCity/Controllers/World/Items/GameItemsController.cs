﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using SunCity.Common.HttpClient.Codes;
using SunCity.Game.World.ItemModels;

namespace SunCity.Controllers.World.Items
{
    [Area("World")]
    [Route(HttpRouteConsts.DefaultController)]
    public class GameItemsController : RageController
    {
        public GameItemsController(IGameItemStorage InGameItemStorage)
        {
            GameItemStorage = InGameItemStorage;
        }

        private IGameItemStorage GameItemStorage { get; }


        [HttpGet]
        [AllowAnonymous]
        [ProducesResponseType(typeof(IReadOnlyList<GameItem>), HttpStatusCodes.OK)]
        public IActionResult GetItemModels()
        {
            return Response(GameItemStorage.Values);
        }
    }
}
