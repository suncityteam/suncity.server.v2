﻿using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using SunCity.Common.HttpClient.Codes;
using SunCity.Common.MediatR;
using SunCity.Common.Operation;
using SunCity.Game.World.Property.Business.VehicleShowRoom.Operations.GetCatalog;
using SunCity.Game.World.Property.Business.VehicleShowRoom.Operations.Purchase;

namespace SunCity.Controllers.World.Property.Business.CarShowRoom
{
    [Area("World/Property/Business")]
    [Route(HttpRouteConsts.DefaultController)]
    public class VehicleShowRoomController : RageController
    {
        private IOperationExecutor OperationExecutor { get; }
        
        public VehicleShowRoomController(IOperationExecutor operationExecutor)
        {
            OperationExecutor = operationExecutor;
        }
        
        [AllowAnonymous, HttpGet]
        [ProducesResponseType(typeof(OperationResponse<GetVehiclesShowRoomCatalogResponse>), HttpStatusCodes.OK)]
        public async Task<IActionResult> GetCatalog([FromQuery] Guid id)
        {
            var catalog = await OperationExecutor.Execute(new GetVehiclesShowRoomCatalogRequest
            {
                PropertyId = new PropertyId(id)
            });
            
            return Response(catalog);
        }
        
        [HttpPost]
        [ProducesResponseType(typeof(OperationResponse<PurchaseVehicleInVehicleShowRoomResponse>), HttpStatusCodes.OK)]
        public async Task<IActionResult> Purchase([FromBody] PurchaseVehicleInVehicleShowRoomRequest request)
        {
            var catalog = await OperationExecutor.Execute(request);
            
            return Response(catalog);
        }
    }
}
