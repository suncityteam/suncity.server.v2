﻿using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using SunCity.Common.HttpClient.Codes;
using SunCity.Common.Operation;
using SunCity.Game.World.Models.Property.Business.Bank;
using SunCity.Game.World.Services.Property.Category.Business.Bank.Service.Client.Tariff;

namespace SunCity.Controllers.World.Property.Business.Bank
{
    [Area("World/Property/Business/Bank")]
    [Route(HttpRouteConsts.DefaultController)]
    public class BankCardTariffController : RageController
    {
        private IBankCardTariffService BankCardTariffService { get; }

        public BankCardTariffController(IBankCardTariffService InBankCardTariffService)
        {
            BankCardTariffService = InBankCardTariffService;
        }

        [AllowAnonymous, HttpGet]
        [ProducesResponseType(typeof(OperationResponse<BankCardTariffModel[]>), HttpStatusCodes.OK)]
        public async Task<IActionResult> GetCardTariffList([FromQuery]Guid id)
        {
            var tariffList = BankCardTariffService.GetList(new PropertyId(id));
            return Response(tariffList);
        }
    }
}