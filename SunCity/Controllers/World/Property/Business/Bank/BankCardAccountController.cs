﻿using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using SunCity.Common.HttpClient.Codes;
using SunCity.Common.MediatR;
using SunCity.Common.Operation;
using SunCity.Game.World.Models.Property.Business.Bank.Card;
using SunCity.Game.World.Models.Property.Business.Bank.Card.Transaction;
using SunCity.Game.World.Property.Business.Bank.Operations.Authorization;
using SunCity.Game.World.Property.Business.Bank.Operations.Deposit;
using SunCity.Game.World.Property.Business.Bank.Operations.GetBalance;
using SunCity.Game.World.Property.Business.Bank.Operations.GetInformation;
using SunCity.Game.World.Property.Business.Bank.Operations.GetList;
using SunCity.Game.World.Property.Business.Bank.Operations.GetOwner;
using SunCity.Game.World.Property.Business.Bank.Operations.GetTransactions;
using SunCity.Game.World.Property.Business.Bank.Operations.OpenCard;
using SunCity.Game.World.Property.Business.Bank.Operations.Transfer;
using SunCity.Game.World.Property.Business.Bank.Operations.Withdraw;
using SunCity.Game.World.Services.Property.Category.Business.Bank.Service.Client.BankCard.Player;

namespace SunCity.Controllers.World.Property.Business.Bank
{
    [Area("World/Property/Business/Bank")]
    [Route(HttpRouteConsts.DefaultController)]
    public class BankCardAccountController : RageController
    {
        private IOperationExecutor OperationExecutor { get; }
        private IBankCardPlayerService BankCardService { get; }
        
        public BankCardAccountController(IBankCardPlayerService InBankCardService, IOperationExecutor operationExecutor)
        {
            BankCardService = InBankCardService;
            OperationExecutor = operationExecutor;
        }

        [AllowAnonymous, HttpPost]
        [ProducesResponseType(typeof(OperationResponse<BankGetCardInformationResponse>), HttpStatusCodes.OK)]
        public async Task<IActionResult> GetBankCardInformation([FromBody]BankGetCardInformationRequest request)
        {
            var bankCard = await OperationExecutor.Execute(request);
            return Response(bankCard);
        }

        [HttpGet]
        [ProducesResponseType(typeof(OperationResponse<BankCardPreviewModel[]>), HttpStatusCodes.OK)]
        public async Task<IActionResult> GetPlayerBankCardList([FromQuery] Guid id)
        {
            var bankCards = await OperationExecutor.Execute(new BankGetCardListRequest
            {
                PropertyId = new PropertyId(id)
            });
            
            return Response(bankCards);
        }

        [AllowAnonymous, HttpPost]
        [ProducesResponseType(typeof(OperationResponse<BankCardId>), HttpStatusCodes.OK)]
        public async Task<IActionResult> Authorization([FromBody]BankAuthorizationRequest request)
        {
            var authorizationResponse = await OperationExecutor.Execute(request);
            return Response(authorizationResponse);
        }

        [HttpPost]
        [ProducesResponseType(typeof(OperationResponse<BankOpenCardResponse>), HttpStatusCodes.OK)]
        public async Task<IActionResult> OpenCard([FromBody]BankOpenCardRequest request)
        {
            var openCardResponse = await OperationExecutor.Execute(request);
            return Response(openCardResponse);
        }

        [HttpPost]
        [ProducesResponseType(typeof(OperationResponse<BankCardTransactionId>), HttpStatusCodes.OK)]
        public async Task<IActionResult> DepositToBankCard([FromBody]BankDepositCardRequest request)
        {
            var depositResponse = await OperationExecutor.Execute(request);
            return Response(depositResponse);
        }
        
        [HttpPost]        
        [ProducesResponseType(typeof(OperationResponse<BankCardTransactionId>), HttpStatusCodes.OK)]
        public async Task<IActionResult> WithdrawFromBankCard([FromBody]BankWithdrawCardRequest request)
        {
            var withdrawResponse = await OperationExecutor.Execute(request);
            return Response(withdrawResponse);
        }
        
        [AllowAnonymous, HttpPost]
        [ProducesResponseType(typeof(OperationResponse<BankCardTransactionId>), HttpStatusCodes.OK)]
        public async Task<IActionResult> TransferToBankCardNumber([FromBody]BankTransferToCardRequest request)
        {
            var transferResponse = await OperationExecutor.Execute(request);
            return Response(transferResponse);
        }

        [AllowAnonymous, HttpPost]
        [ProducesResponseType(typeof(OperationResponse<BankCardBalanceModel>), HttpStatusCodes.OK)]
        public async Task<IActionResult> GetBankCardBalance([FromBody]BankGetCardBalanceRequest request)
        {
            var getBalanceResponse = await OperationExecutor.Execute(request);
            return Response(getBalanceResponse);
        }

        [AllowAnonymous, HttpPost]
        [ProducesResponseType(typeof(OperationResponse<BankCardTransactionPreviewModel[]>), HttpStatusCodes.OK)]
        public async Task<IActionResult> GetBankCardTransactions([FromBody]BankGetCardTransactionsRequest request)
        {
            var getTransactions = await OperationExecutor.Execute(request);
            return Response(getTransactions);
        }

        [AllowAnonymous, HttpPost]
        [ProducesResponseType(typeof(OperationResponse<BankCardOwnerModel>), HttpStatusCodes.OK)]
        public async Task<IActionResult> GetBankCardOwner([FromBody]BankGetCardOwnerRequest request)
        {
            var owner = await OperationExecutor.Execute(request);
            return Response(owner);
        }

    }
}
