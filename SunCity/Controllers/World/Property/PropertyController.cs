﻿using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using SunCity.Common.HttpClient.Codes;
using SunCity.Common.MediatR;
using SunCity.Common.Operation;
using SunCity.Game.World.Property.Operations.Information;
using SunCity.Game.World.Property.Operations.Purchase;
using SunCity.Game.World.Property.Operations.SellingInformation;

namespace SunCity.Controllers.World.Property
{
    [Area("World/Property")]
    [Route(HttpRouteConsts.DefaultController)]
    public class PropertyController : RageController
    {
        private IOperationExecutor OperationExecutor { get; }
        
        public PropertyController(IOperationExecutor operationExecutor)
        {
            OperationExecutor = operationExecutor;
        }

        [AllowAnonymous, HttpGet]
        [ProducesResponseType(typeof(OperationResponse<GetWorldPropertyInformationResponse>), HttpStatusCodes.OK)]
        public async Task<IActionResult> GetInformation([FromQuery] Guid id)
        {
            var model = await OperationExecutor.Execute(new GetWorldPropertyInformationRequest()
            {
                PropertyId = new PropertyId(id)
            });
            return Response(model);
        }
        
        [AllowAnonymous, HttpGet]
        [ProducesResponseType(typeof(OperationResponse<GetWorldPropertySellingInformationResponse>), HttpStatusCodes.OK)]
        public async Task<IActionResult> GetSellingInformation([FromQuery] Guid id)
        {
            var model = await OperationExecutor.Execute(new GetWorldPropertySellingInformationRequest()
            {
                PropertyId = new PropertyId(id)
            });
            return Response(model);
        }

        [HttpPost]
        [ProducesResponseType(typeof(OperationResponse<PurchaseWorldPropertyResponse>), HttpStatusCodes.OK)]
        public async Task<IActionResult> Purchase([FromBody] PurchaseWorldPropertyRequest request)
        {
            var model = await OperationExecutor.Execute(request);
            return Response(model);
        }
    }
}