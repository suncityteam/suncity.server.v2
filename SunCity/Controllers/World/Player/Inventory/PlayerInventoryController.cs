﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using SunCity.Common.HttpClient.Codes;
using SunCity.Common.Operation;
using SunCity.Game.World.Models.Character.Inventory;
using SunCity.Game.World.Player;
using SunCity.Game.World.Services.Character.Inventory.Service;

namespace SunCity.Controllers.World.Player.Inventory
{
    [Area("World/Player")]
    [Route(HttpRouteConsts.DefaultController)]
    public class PlayerInventoryController : RageController
    {
        private ICharacterInventoryService CharacterInventoryService { get; }
        private WorldPlayer WorldPlayer { get; }


        public PlayerInventoryController(ICharacterInventoryService InCharacterInventoryService, WorldPlayer worldPlayer)
        {
            CharacterInventoryService = InCharacterInventoryService;
            WorldPlayer = worldPlayer;
        }

        [HttpPost]
        [ProducesResponseType(typeof(OperationResponse), HttpStatusCodes.OK)]
        public async Task<IActionResult> MoveItemTo([FromBody] InventroryItemMoveForm InForm)
        {
            var inventory = await CharacterInventoryService.MoveItemTo(WorldPlayer, InForm);
            return Response(inventory);
        }

        [HttpPost]
        [ProducesResponseType(typeof(OperationResponse), HttpStatusCodes.OK)]
        public async Task<IActionResult> EquipItem([FromBody] InventroryItemEquipForm InForm)
        {
            var inventory = await CharacterInventoryService.EquipItem(WorldPlayer, InForm);
            return Response(inventory);
        }

        [HttpGet]
        [ProducesResponseType(typeof(OperationResponse<PlayerInventoryModel>), HttpStatusCodes.OK)]
        public async Task<IActionResult> GetInventory()
        {
            var inventory = await CharacterInventoryService.GetPlayerInventory(WorldPlayer);
            return Response(inventory);
        }
    }
}