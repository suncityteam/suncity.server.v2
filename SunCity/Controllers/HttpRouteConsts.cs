﻿namespace SunCity.Controllers
{
    public static class HttpRouteConsts
    {
        public const string DefaultController = "[area]/[controller]/[action]";
        public const string DefaultAction = "[action]";
    }
}
