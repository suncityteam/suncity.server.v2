﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using SunCity.Common.HttpClient.Codes;
using SunCity.Common.MediatR;
using SunCity.Common.Operation;
using SunCity.Game.Identity.Services.Account.Service;
using SunCity.Game.Identity.Services.Character.Services;
using SunCity.Game.World.Enums.Character;
using SunCity.Game.World.Identity.Operations.Character.AccountInformation;
using SunCity.Game.World.Identity.Operations.Character.CreateCharacter;
using SunCity.Game.World.Identity.Operations.Character.JoinToWorld;
using SunCity.Game.World.Identity.Operations.Character.WhatToDo;

namespace SunCity.Controllers.Identity
{
    [Area("World/Identity")]
    [Route(HttpRouteConsts.DefaultController)]
    public class CharacterController : RageController
    {
        private IGameAccountService GameAccountService { get; }
        private IGameCharactersService GameCharactersService { get; }

        private IOperationExecutor OperationExecutor { get; }

        public CharacterController(IGameAccountService InGameAccountService, IGameCharactersService InGameCharactersService, IOperationExecutor operationExecutor)
        {
            GameAccountService = InGameAccountService;
            GameCharactersService = InGameCharactersService;
            OperationExecutor = operationExecutor;
        }

        [HttpGet]
        [ProducesResponseType(typeof(OperationResponse<AuthorizationWhatToDo>), HttpStatusCodes.OK)]
        public async Task<IActionResult> WhatToDo()
        {
            var result = await OperationExecutor.Execute(new GetCharacterWhatToDoRequest());
            if (result.IsCorrect && result.Content.Action == AuthorizationWhatToDo.SpawnCharacter)
            {
                if (result.Content.CharacterId.HasValue)
                {
                    var selectCharacterResult = await OperationExecutor.Execute(new CharacterJoinToWorldRequest
                    {
                        PlayerCharacterId = result.Content.CharacterId.Value,
                        UseAsDefault = true
                    });

                    if (selectCharacterResult.IsNotCorrect)
                        return Response(selectCharacterResult.Error);

                    return Response(AuthorizationWhatToDo.SpawnCharacter);
                }

                return Response(AuthorizationWhatToDo.SelectCharacter);
            }

            if (result.IsCorrect)
            {
                return Response(result.Content.Action);
            }

            return Response(result.Error);
        }

        [HttpGet]
        [ProducesResponseType(typeof(OperationResponse<GetAccountInformationResponse>), HttpStatusCodes.OK)]
        public async Task<IActionResult> GetList()
        {
            var createAccountResult = await OperationExecutor.Execute(new GetAccountInformationRequest());
            return Response(createAccountResult);
        }

        [HttpPost]
        [ProducesResponseType(typeof(OperationResponse), HttpStatusCodes.OK)]
        public async Task<IActionResult> Select([FromBody] CharacterJoinToWorldRequest request)
        {
            var result = await OperationExecutor.Execute(request);
            return Response(result);
        }

        [HttpPost]
        [ProducesResponseType(typeof(OperationResponse), HttpStatusCodes.OK)]
        public async Task<IActionResult> Create([FromBody] CreateCharacterRequest request)
        {
            var createCharacterResult = await OperationExecutor.Execute(request);
            if (createCharacterResult.IsNotCorrect)
                return Response(createCharacterResult.Error);

            var selectCharacterResult = await OperationExecutor.Execute(new CharacterJoinToWorldRequest
            {
                PlayerCharacterId = createCharacterResult.Content.CreatedCharacterId,
                UseAsDefault = true
            });

            return Response(selectCharacterResult);
        }
    }
}