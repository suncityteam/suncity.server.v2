﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using SunCity.Common.HttpClient.Codes;
using SunCity.Common.MediatR;
using SunCity.Common.Operation;
using SunCity.Game.World.Identity.Operations.Authorization.Login;
using SunCity.Game.World.Identity.Operations.Authorization.Register;

namespace SunCity.Controllers.Identity
{
    [Area("World/Identity")]
    [Route(HttpRouteConsts.DefaultController)]
    public class AuthorizationController : RageController
    {
        private IOperationExecutor OperationExecutor { get; }

        public AuthorizationController(IOperationExecutor operationExecutor)
        {
            OperationExecutor = operationExecutor;
        }

        [HttpPost]
        [AllowAnonymous]
        [ProducesResponseType(typeof(OperationResponse<AuthorizationLoginResponse>), HttpStatusCodes.OK)]
        public async Task<IActionResult> Login([FromBody]AuthorizationLoginRequest InWebForm)
        {
            var loginResult = await OperationExecutor.Execute(InWebForm);
            return Response(loginResult);
        }
        
        [HttpPost]
        [AllowAnonymous]
        [ProducesResponseType(typeof(OperationResponse<AuthorizationRegisterResponse>), HttpStatusCodes.OK)]
        public async Task<IActionResult> Register([FromBody]AuthorizationRegisterRequest InWebForm)
        {
            var registerResult = await OperationExecutor.Execute(InWebForm);
            return Response(registerResult);
        }
    }
}
