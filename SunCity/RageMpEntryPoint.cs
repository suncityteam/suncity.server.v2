﻿using System;
using System.Threading.Tasks;
using GTANetworkAPI;
using SunCity.Common.Events;
using SunCity.Game.World.Events;
using SunCity.Game.World.Events.Player;
using SunCity.Game.World.Events.Player.PlayerConnected;
using SunCity.Game.World.Events.Player.PlayerDamage;
using SunCity.Game.World.Events.Player.PlayerDeath;
using SunCity.Game.World.Events.Player.PlayerEnterVehicle;
using SunCity.Game.World.Events.Player.PlayerExitVehicle;
//
namespace SunCity
{
    public sealed class RageMpEntryPoint : Script
    {
        [ServerEvent(Event.ResourceStart)]
        public void ResourceStart()
        {
            NAPI.Server.SetAutoRespawnAfterDeath(false);
            NAPI.Server.SetGlobalServerChat(false);

            Task.Factory.StartNew(() => WebServerStartup.Run(Array.Empty<string>()), TaskCreationOptions.LongRunning | TaskCreationOptions.DenyChildAttach | TaskCreationOptions.RunContinuationsAsynchronously);
        }

        [ServerEvent(Event.ResourceStop)]
        public void ResourceStop()
        {
            WebServerStartup.Stop().Wait();
        }

        [ServerEvent(Event.PlayerDamage)]
        public void PlayerDamage(Player player, float healthLoss, float armourLoss)
        {
            RageMpEventExecutor.EventExecutor.Execute(player, new PlayerDamageRequest
            {
                HealthLoss = healthLoss,
                ArmourLoss = armourLoss
            });
        }

        [ServerEvent(Event.VehicleDamage)]
        public void VehicleDamage(Vehicle vehicle, float bodyHealthLoss, float engineHealthLoss)
        {
            GameWorldEvents.Instance.OnVehicleDamage(vehicle, bodyHealthLoss, engineHealthLoss);
        }

        /// Event Params: <see cref="T:GTANetworkAPI.Client" /> client, <see cref="T:GTANetworkAPI.Client" /> killer, <see cref="T:System.UInt32" /> reason
        /// <para>Death reason can be of either <see cref="T:GTANetworkAPI.WeaponHash" /> or <see cref="T:GTANetworkAPI.DeathReason" /></para>
        [ServerEvent(Event.PlayerDeath)]
        public void PlayerDeath(Player player, Player killer, uint reason)
        {
            RageMpEventExecutor.EventExecutor.Execute(player, new PlayerDeathRequest()
            {
                KillerPlayerId = killer?.Id,
                DeathReason = reason
            });
        }

        [ServerEvent(Event.PlayerEnterVehicle)]
        public void PlayerEnterVehicle(Player player, Vehicle vehicle, sbyte seatId)
        {
            player.TriggerEvent("playerEnteredVehicle", vehicle.Id, seatId);

            RageMpEventExecutor.EventExecutor.Execute(player, new PlayerEnterVehicleRequest()
            {
                TargetVehicleId = vehicle.Id,
                SeatId = seatId
            });
        }

        [ServerEvent(Event.PlayerExitVehicle)]
        public void PlayerExitVehicle(Player player, Vehicle vehicle)
        {
            player.TriggerEvent("playerLeftVehicle", vehicle.Id);

            RageMpEventExecutor.EventExecutor.Execute(player, new PlayerExitVehicleRequest()
            {
                TargetVehicleId = vehicle.Id,
            });
        }

        [ServerEvent(Event.PlayerConnected)]
        public void PlayerConnected(Player player)
        {
            if (player != null && player.Exists)
            {
                if (GameWorldEvents.Instance == null)
                {
                    NAPI.Task.Run(() => PlayerConnected(player), 1000);
                }
                else
                {
                    RageMpEventExecutor.EventExecutor.Execute(player, PlayerConnectedRequest.Request);
                }
            }
        }
    }
}