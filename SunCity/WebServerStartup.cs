﻿using System;
using System.IO;
using System.Reflection;
using System.Text.Json;
using System.Text.Json.Serialization;
using System.Threading.Tasks;
using AutoMapper;
using MediatR;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using SunCity.Common;
using SunCity.Common.Commands;
using SunCity.Common.Data;
using SunCity.Common.Events;
using SunCity.Common.Json.Converters;
using SunCity.Common.MediatR.Extensions;
using SunCity.Common.RageMp;
using SunCity.Common.Reflection;
using SunCity.Extensions;
using SunCity.Game.Rage;
using SunCity.Game.Rage.Middleware;
using SunCity.Game.World;
using SunCity.Game.World.Commands;
using SunCity.Game.World.Events;
using SunCity.Identity.Services;
using SunCity.IntegrationTests.RageMp;
using SunCity.IntegrationTests.RageMp.Pools;
using SunCity.Swagger;

namespace SunCity
{
    public class WebServerStartup
    {
        private static IWebHost Host { get; set; }
        public IConfigurationRoot Configuration { get; }
        public IHostEnvironment HostEnvironment { get; }

        public WebServerStartup(IHostEnvironment InHostEnvironment)
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(InHostEnvironment.ContentRootPath)
                .AddJsonFile("Settings/Application.DataBase.json", false)
                .AddJsonFile("Settings/Application.Identity.json", false)
                .AddJsonFile("Settings/World.ZoneNames.json", false)
                .AddEnvironmentVariables();

            HostEnvironment = InHostEnvironment;
            Configuration = builder.Build();
        }

        public static IWebHostBuilder CreateHostBuilder(string[] InAgrs)
        {
            return WebHost.CreateDefaultBuilder(InAgrs)
                .CaptureStartupErrors(true)
                .UseStartup<WebServerStartup>()
                .UseKestrel()
                .UseUrls("http://localhost:5000/");
        }

        public static void Run(string[] InAgrs)
        {
            Host = CreateHostBuilder(InAgrs).Build();
            Host.Run();
        }

        public static Task Stop()
        {
            return Host.StopAsync();
        }

        public void ConfigureServices(IServiceCollection InServiceCollection)
        {
            //  TODO: https://stackoverflow.com/questions/43878641/custom-type-in-asp-net-core-mvc-controller-method
            InServiceCollection.AddGameDataManager();
            InServiceCollection.AddJWTAuthentication(Configuration);
            InServiceCollection.AddLogging();
            InServiceCollection.AddMemoryCache();
            InServiceCollection.UseCommonJsonSerializer();
            InServiceCollection.AddMediatR(typeof(WebServerStartup).GetTypeInfo().Assembly);
            InServiceCollection.AddOperationExecutor();

            //InServiceCollection.AddConnections();
            InServiceCollection.AddMvc(InOptions =>
            {
                InOptions.EnableEndpointRouting = false;
                InOptions.Filters.Add(typeof(RageMiddleware));
            }).AddJsonOptions(InOptions =>
            {
                InOptions.JsonSerializerOptions.DefaultBufferSize = 1024;

                InOptions.JsonSerializerOptions.Converters.UseAutoNumberConverters();
                InOptions.JsonSerializerOptions.Converters.UseStronglyConverters();
                InOptions.JsonSerializerOptions.IgnoreNullValues = true;
                InOptions.JsonSerializerOptions.PropertyNamingPolicy = JsonNamingPolicy.CamelCase;

#if DEBUG
                InOptions.JsonSerializerOptions.WriteIndented = true;
#endif
            }).AddJsonStringEnumConverter();


            InServiceCollection.AddCors(InOptions =>
            {
                InOptions.AddPolicy("any",
                    InBuilder =>
                    {
                        InBuilder
                            .AllowAnyMethod()
                            .AllowAnyHeader()
                            .AllowCredentials()
                            .WithOrigins("http://localhost:4200", "http://localhost:57860");
                    });
            });

            InServiceCollection.AddSunCityServices(Configuration);


            InServiceCollection.AddRageConnectionManager(HostEnvironment, Configuration);

            InServiceCollection.AddAutoMapper(GenericReflectionExtensions.GetLoadedTypes());


            InServiceCollection.AddHostedService<RageMpGameServer>();

            Console.WriteLine($"HostEnvironment.EnvironmentName: {HostEnvironment.EnvironmentName}");
            if (HostEnvironment.IsEnvironment(EnvironmentNames.ENTITY_FRAMEWORK_MIGRATION) || HostEnvironment.IsEnvironment(EnvironmentNames.INTEGRATION_TESTS))
            {
                InServiceCollection.UseTestRageMpServer();
            }
            else
            {
                InServiceCollection.AddRageMpPools();
            }

            //if (HostEnvironment.IsDevelopment())
            {
                var xmlFile = $"SunCity.xml";
                var xmlPath = Path.Combine(AppContext.BaseDirectory, xmlFile);
                InServiceCollection.AddSwaggerConfiguration(xmlPath);
            }
        }

        public void Configure(IApplicationBuilder InBuilder, IHostEnvironment InHostEnvironment)
        {
            InBuilder.UseGameCommandsManager();
            InBuilder.UseGameEventManager();

            InBuilder.AddGameWorldEvents();
            InBuilder.UseRageMpServices();


            if (!HostEnvironment.IsEnvironment(EnvironmentNames.ENTITY_FRAMEWORK_MIGRATION) && !HostEnvironment.IsEnvironment(EnvironmentNames.INTEGRATION_TESTS))
            {
                InBuilder.UseGameWorld();
            }

            //if (InHostEnvironment.IsDevelopment())
            {
                InBuilder.UseDeveloperExceptionPage();
                InBuilder.UseCors("any");
            }

            //if (InHostEnvironment.IsDevelopment())
            {
                InBuilder.EnableSwaggerUi();
            }

            InBuilder.UseAuthorization();
            InBuilder.UseRouting();
            InBuilder.UseMvc();
        }
    }
}