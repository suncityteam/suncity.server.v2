﻿using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Hosting;
using SunCity.Game.World;

namespace SunCity
{
    public sealed class RageMpGameServer : IHostedService
    {
        private IGameWorld GameWorld { get; }

        public RageMpGameServer(IGameWorld InGameWorld)
        {
            GameWorld = InGameWorld;
        }


        public async Task StartAsync(CancellationToken InCancellationToken)
        {
            await GameWorld.BeginPlay();
        }

        public async Task StopAsync(CancellationToken InCancellationToken)
        {
            await GameWorld.EndPlay();
        }
    }
}