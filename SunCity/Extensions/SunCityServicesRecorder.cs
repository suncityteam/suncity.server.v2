﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using SunCity.Common.Commands;
using SunCity.Common.Events;
using SunCity.Game.Identity.Services;
using SunCity.Game.World;
using SunCity.Game.World.Commands;
using SunCity.Game.World.DataBase;
using SunCity.Game.World.Events;
using SunCity.Game.World.Identity.Operations;
using SunCity.Game.World.Property.Business.Bank.Operations;
using SunCity.Game.World.Property.Business.VehicleShowRoom.Operations;
using SunCity.Game.World.Property.Operations;
using SunCity.Game.World.Services;
using SunCity.Identity.DataBase;
using SunCity.Identity.Services;

namespace SunCity.Extensions
{
    internal static class SunCityServicesRecorder
    {
        public static IServiceCollection AddSunCityServices(this IServiceCollection InServiceCollection, IConfiguration InConfiguration)
        {
            InServiceCollection.AddSunCityIdentity(InConfiguration);
            InServiceCollection.AddSunCityGameWorld(InConfiguration);
            InServiceCollection.AddSunCityOperations(InConfiguration);
            InServiceCollection.AddBankPlayerClientOperations(InConfiguration);
            
            InServiceCollection.AddGameCommandsManager();
            InServiceCollection.AddGameEventManager();
            InServiceCollection.AddGameWorldCommands();
            InServiceCollection.AddGameWorldEvents();
            
            return InServiceCollection;
        }

        private static IServiceCollection AddSunCityOperations(this IServiceCollection InServiceCollection, IConfiguration InConfiguration)
        {
            InServiceCollection.AddGameIdentityOperations(InConfiguration);
            
            InServiceCollection.AddVehicleShowRoomOperationHandlers(InConfiguration);
            InServiceCollection.AddWorldPropertyOperationHandlers(InConfiguration);

            
            return InServiceCollection;
        }
        
        private static IServiceCollection AddSunCityGameWorld(this IServiceCollection InServiceCollection, IConfiguration InConfiguration)
        {
            InServiceCollection.AddGameWorldDataBase(InConfiguration);
            InServiceCollection.AddGameWorldServices(InConfiguration);
            InServiceCollection.AddGameWorld(InConfiguration);

            InServiceCollection.AddGameWorld(InConfiguration);
            
            return InServiceCollection;
        }
        
        private static IServiceCollection AddSunCityIdentity(this IServiceCollection InServiceCollection, IConfiguration InConfiguration)
        {
            InServiceCollection.AddIdentityDataBase(InConfiguration);
            InServiceCollection.AddIdentityServices(InConfiguration);
            InServiceCollection.AddGameIdentityServices(InConfiguration);
            
            return InServiceCollection;
        }
    }
}