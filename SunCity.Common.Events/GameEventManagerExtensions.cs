﻿using System;
using System.Threading.Tasks;
using AutoMapper;
using MediatR;
using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.DependencyInjection;
using SunCity.Common.Events.Interfaces;
using SunCity.Common.MediatR;
using SunCity.Common.MediatR.Extensions;
using SunCity.Common.Operation;

namespace SunCity.Common.Events
{
    public static class GameEventManagerExtensions
    {
        public static void AddGameEventManager(this IServiceCollection InServiceCollection)
        {
            InServiceCollection.AddScoped(typeof(RageMpEventConstructor<>));
            InServiceCollection.AddSingleton<EventManager>();
            InServiceCollection.AddSingleton<RageMpEventExecutor>();
        }

        public static void UseGameEventManager(this IApplicationBuilder InApplicationBuilder)
        {
            var commandManager = InApplicationBuilder.ApplicationServices.GetService<EventManager>();
            commandManager.Initialize();
            
            InApplicationBuilder.ApplicationServices.GetService<RageMpEventExecutor>();
        }

    }
}