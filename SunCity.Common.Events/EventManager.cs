﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using GTANetworkAPI;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using SunCity.Common.Events.Attributes;
using SunCity.Common.Events.Interfaces;
using SunCity.Common.MediatR;
using SunCity.Common.RageMp.Events;
using SunCity.Common.RageMp.Pools.Players;
using SunCity.Common.Reflection;

namespace SunCity.Common.Events
{
    internal class EventManager
    {
        private Dictionary<string, RageMpEventHandler> Events { get; } = new Dictionary<string, RageMpEventHandler>(4096, StringComparer.OrdinalIgnoreCase);
        private IServiceProvider ServiceProvider { get; }
        private ILogger<EventManager> Logger { get; }
        private IRageMpEventManager RageMpEventManager { get; }
        private RageMpEventExecutor EventExecutor { get; }
        private IPlayerPool PlayerPool { get; }

        public EventManager
        (
            IServiceProvider InServiceProvider,
            ILogger<EventManager> InLogger,
            IRageMpEventManager rageMpEventManager, IPlayerPool playerPool, RageMpEventExecutor eventExecutor)
        {
            ServiceProvider = InServiceProvider;
            Logger = InLogger;
            RageMpEventManager = rageMpEventManager;
            PlayerPool = playerPool;
            EventExecutor = eventExecutor;
        }

        public static RageMpEventWrapper[] GetLoadedEvents()
        {
            var loadedTypes = GenericReflectionExtensions.GetLoadedTypes();
            var eventTypes = loadedTypes
                .Where(IsRageMpPlayerEvent)
                .ToArray();

            var eventHandlers = eventTypes.Select(type => new RageMpEventWrapper(type)).ToArray();
            return eventHandlers;
        }

        private static bool IsRageMpPlayerEvent(Type q)
        {
            if (q.IsAbstract)
                return false;

            if (!q.IsClass)
                return false;

            if (q.GetCustomAttribute<IgnoreGameEventAttribute>() != null)
                return false;

            return q.IsImplementedGenericInterface(typeof(IRageMpEventHandler<>));
        }

        public void Initialize()
        {
            var events = GetLoadedEvents();
            Logger.LogInformation($"Detected {events.Length} events");
            foreach (var @event in events)
            {
                if (AddEvent(@event) == false)
                {
                    Logger.LogError($"Failed add event {@event.EventName}");
                }
            }
        }

        private bool AddEvent(RageMpEventWrapper eventWrapper)
        {
            if (string.IsNullOrWhiteSpace(eventWrapper.EventName))
                return false;

            var handler = new RageMpEventHandler(eventWrapper, EventExecutor);
            RageMpEventManager.Register(eventWrapper.EventName, RageMpEventHandler.HandlerMethod, handler);

            Events.Add(eventWrapper.EventName, handler);

            return true;
        }

        private class RageMpEventHandler
        {
            private RageMpEventExecutor EventExecutor { get; }
            private RageMpEventWrapper EventWrapper { get; }

            public static MethodInfo HandlerMethod { get; }

            static RageMpEventHandler()
            {
                HandlerMethod =
                    typeof(RageMpEventHandler)
                        .GetMethod(nameof(Handle), BindingFlags.NonPublic | BindingFlags.Instance);
            }

            public RageMpEventHandler(RageMpEventWrapper eventWrapper, RageMpEventExecutor eventExecutor)
            {
                EventWrapper = eventWrapper;
                EventExecutor = eventExecutor;
            }

            private void Handle(Player InSender, object[] args)
            {
                Task.Factory.StartNew(async () =>
                {
                    if (args != null && args.Length > 0)
                    {
                        var json = args[0].ToString();
                        var request = JsonConvert.DeserializeObject(json, EventWrapper.Request);
                        var operationRequest = request as IRageMpEventRequest;
                        EventExecutor.Execute(InSender, operationRequest);
                    }
                    else
                    {
                        EventExecutor.Execute(InSender, EventWrapper.DefaultRequest);
                    }
                });
            }
        }
    }
}