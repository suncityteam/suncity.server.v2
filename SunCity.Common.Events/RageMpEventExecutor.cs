﻿using System;
using System.Threading.Tasks;
using Microsoft.Extensions.DependencyInjection;
using SunCity.Common.Events.Interfaces;
using SunCity.Common.MediatR;
using SunCity.Common.RageMp.Pools.Players;

namespace SunCity.Common.Events
{
    public class RageMpEventExecutor
    {
        public static RageMpEventExecutor EventExecutor { get; private set; }
        private IServiceProvider ServiceProvider { get; }
        private IPlayerPool PlayerPool { get; }

        public RageMpEventExecutor(IServiceProvider serviceProvider, IPlayerPool playerPool)
        {
            ServiceProvider = serviceProvider;
            PlayerPool = playerPool;
            EventExecutor = this;
        }

        public void Execute<TRequest>(GTANetworkAPI.Player player, TRequest request) where TRequest : IRageMpEventRequest
        {
            Task.Factory.StartNew(async () =>
            {
                using var scope = ServiceProvider.CreateScope();
                var middlewares = scope.ServiceProvider.GetServices<IRageMpEventMiddleware>();
                var executor = scope.ServiceProvider.GetRequiredService<IOperationExecutor>();

                foreach (var middleware in middlewares)
                {
                    var sender = PlayerPool.Convert(player);
                    middleware.Prepare(sender);
                }
                
                await executor.Execute(request);
            }, TaskCreationOptions.LongRunning | TaskCreationOptions.DenyChildAttach | TaskCreationOptions.RunContinuationsAsynchronously);
        }
    }
}