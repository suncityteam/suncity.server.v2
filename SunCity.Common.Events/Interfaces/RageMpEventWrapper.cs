﻿using System;
using System.Reflection;
using SunCity.Common.Events.Attributes;
using SunCity.Common.MediatR;
using SunCity.Common.Reflection;

namespace SunCity.Common.Events.Interfaces
{
    internal class RageMpEventWrapper
    {
        public IRageMpEventRequest DefaultRequest { get; }

        public Type Request { get; }
        public Type HandlerImpl { get; }

        public string EventName { get; }

        public RageMpEventWrapper(Type handler)
        {
            HandlerImpl = handler;

            Request = GetRequestType(handler);
            EventName = GetEventName(handler);

            DefaultRequest = Activator.CreateInstance(Request) as IRageMpEventRequest;
        }

        private Type GetRequestType(Type request)
        {
            var eventArgs = request.GetGenericArguments(typeof(IRageMpEventHandler<>));
            return eventArgs[0];
        }

        private string GetEventName(Type request)
        {
            var attribute = request.GetCustomAttribute<GameEventAttribute>();
            if (attribute != null)
            {
                return attribute.Name;
            }

            return string.Empty;
        }
    }
}