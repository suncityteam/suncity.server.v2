﻿using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using SunCity.Common.Operation;
using SunCity.Common.RageMp.Pools;
using SunCity.Common.RageMp.Pools.Players;

namespace SunCity.Common.Events.Interfaces
{
    public abstract class RageMpEventHandler<TRequest> : IRageMpEventHandler<TRequest> where TRequest : IRageMpEventRequest
    {
        protected ILogger Logger { get; }
        protected IRageMpPool RageMpPool { get; }
        protected IRagePlayer RagePlayer { get; }

        protected RageMpEventHandler(RageMpEventConstructor<TRequest> constructor)
        {
            RagePlayer = constructor.RagePlayer;
            RageMpPool = constructor.RageMpPool;
            Logger = constructor.Logger;
        }

        public abstract Task<OperationResponse> Handle(TRequest request, CancellationToken cancellationToken);
    }
}