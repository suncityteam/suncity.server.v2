﻿using SunCity.Common.MediatR;

namespace SunCity.Common.Events.Interfaces
{
    public interface IRageMpEventRequest : IOperationRequest
    {
    }
}