﻿using Microsoft.Extensions.Logging;
using SunCity.Common.RageMp.Pools;
using SunCity.Common.RageMp.Pools.Players;

namespace SunCity.Common.Events.Interfaces
{
    public sealed class RageMpEventConstructor<THandler>
    {
        public ILogger<THandler> Logger { get; }
        public IRageMpPool RageMpPool { get; }
        public IRagePlayer RagePlayer { get; }

        public RageMpEventConstructor(IRagePlayer ragePlayer, IRageMpPool rageMpPool, ILogger<THandler> logger)
        {
            RagePlayer = ragePlayer;
            RageMpPool = rageMpPool;
            Logger = logger;
        }
    }
}