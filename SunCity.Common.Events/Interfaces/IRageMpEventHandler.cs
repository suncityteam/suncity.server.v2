﻿using SunCity.Common.MediatR;

namespace SunCity.Common.Events.Interfaces
{
    public interface IRageMpEventHandler<in TRequest> : IOperationHandler<TRequest> where TRequest : IRageMpEventRequest
    {
    }
}