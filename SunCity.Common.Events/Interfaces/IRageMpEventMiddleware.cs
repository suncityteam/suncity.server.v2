﻿using SunCity.Common.Operation;
using SunCity.Common.RageMp.Pools.Players;

namespace SunCity.Common.Events.Interfaces
{
    public interface IRageMpEventMiddleware
    {
        OperationResponse Prepare(IRagePlayer player);
    }
}