﻿using System;

namespace SunCity.Common.Events.Attributes
{
    [AttributeUsage(AttributeTargets.Class, AllowMultiple = false)]
    public class IgnoreGameEventAttribute : Attribute
    {

    }
}