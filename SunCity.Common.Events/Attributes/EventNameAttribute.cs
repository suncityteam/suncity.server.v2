﻿using System;

namespace SunCity.Common.Events.Attributes
{
    [AttributeUsage(AttributeTargets.Class, AllowMultiple = false)]
    public class GameEventAttribute : Attribute
    {
        public string Name { get; }

        public GameEventAttribute(string InName)
        {
            Name = InName;
        }
    }
}