﻿using System.Collections.Generic;
using System.Threading.Tasks;
using SunCity.Common.HttpClient;
using SunCity.Game.World.ItemModels;
using SunCity.IntegrationTests.HttpClients.Base;
using SunCity.IntegrationTests.HttpClients.Factories;

namespace SunCity.IntegrationTests.HttpClients.World.GameItems
{
    public sealed class GameItemsHttpClient : BaseTestHttpClient
    {
        private static string Url { get; } = "World/GameItems";

        public GameItemsHttpClient(ITestServerHttpClient InHttpClient) : base(InHttpClient)
        {

        }

        public Task<HttpClientResponse<IReadOnlyList<GameItem>>> GetItemModels()
        {
            return HttpClient.Get<IReadOnlyList<GameItem>>($"{Url}/{nameof(GetItemModels)}");
        }
    }
}
