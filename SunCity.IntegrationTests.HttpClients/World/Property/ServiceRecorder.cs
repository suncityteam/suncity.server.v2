﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using SunCity.IntegrationTests.HttpClients.World.Property.Business;

namespace SunCity.IntegrationTests.HttpClients.World.Property
{
    internal static class ServiceRecorder
    {
        public static IServiceCollection AddWorldPropertyHttpClients(this IServiceCollection InServiceCollection, IConfiguration InConfiguration)
        {
            InServiceCollection.AddSingleton<WorldPropertyHttpClient>();
            InServiceCollection.AddWorldBusinessPropertyHttpClients(InConfiguration);
            return InServiceCollection;
        }
    }
}
