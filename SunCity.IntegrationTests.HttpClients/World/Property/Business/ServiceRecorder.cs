﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using SunCity.IntegrationTests.HttpClients.World.Property.Business.Bank;

namespace SunCity.IntegrationTests.HttpClients.World.Property.Business
{
    internal static class ServiceRecorder
    {
        public static IServiceCollection AddWorldBusinessPropertyHttpClients(this IServiceCollection InServiceCollection, IConfiguration InConfiguration)
        {
            InServiceCollection.AddWorldBankBusinessPropertyHttpClients(InConfiguration);
            InServiceCollection.AddSingleton<WorldPropertyHttpClient>();
            return InServiceCollection;
        }
    }
}
