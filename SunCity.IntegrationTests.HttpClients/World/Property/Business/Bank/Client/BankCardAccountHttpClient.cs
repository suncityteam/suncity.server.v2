﻿using System.Threading.Tasks;
using SunCity.Common.HttpClient;
using SunCity.Common.Operation;
using SunCity.Game.World.Models.Property.Business.Bank.Card;
using SunCity.Game.World.Models.Property.Business.Bank.Card.Transaction;
using SunCity.Game.World.Property.Business.Bank.Operations.Authorization;
using SunCity.Game.World.Property.Business.Bank.Operations.Deposit;
using SunCity.Game.World.Property.Business.Bank.Operations.GetBalance;
using SunCity.Game.World.Property.Business.Bank.Operations.GetInformation;
using SunCity.Game.World.Property.Business.Bank.Operations.GetOwner;
using SunCity.Game.World.Property.Business.Bank.Operations.GetTransactions;
using SunCity.Game.World.Property.Business.Bank.Operations.OpenCard;
using SunCity.Game.World.Property.Business.Bank.Operations.Transfer;
using SunCity.Game.World.Property.Business.Bank.Operations.Withdraw;
using SunCity.IntegrationTests.HttpClients.Base;
using SunCity.IntegrationTests.HttpClients.Base.Connections;
using SunCity.IntegrationTests.HttpClients.Factories;

namespace SunCity.IntegrationTests.HttpClients.World.Property.Business.Bank.Client
{
    public sealed class BankCardAccountHttpClient : BaseTestHttpClient
    {
        private static string Url { get; } = "World/Property/Business/Bank/BankCardAccount";

        public BankCardAccountHttpClient(ITestServerHttpClient httpClient) : base(httpClient)
        {
        }

        public Task<HttpClientResponse<OperationResponse<BankCardId>>> Authorization(RageMpClientConnection connection, BankAuthorizationRequest request)
        {
            return HttpClient.Post<BankAuthorizationRequest, OperationResponse<BankCardId>>($"{Url}/{nameof(Authorization)}", request, FillRequestHeaders(connection));
        }

        public Task<HttpClientResponse<OperationResponse<BankOpenCardResponse>>> OpenCard(RageMpClientConnection connection, BankOpenCardRequest request)
        {
            return HttpClient.Post<BankOpenCardRequest, OperationResponse<BankOpenCardResponse>>($"{Url}/{nameof(OpenCard)}", request, FillRequestHeaders(connection));
        }

        public Task<HttpClientResponse<OperationResponse<BankCardTransactionId>>> DepositToBankCard(RageMpClientConnection connection, BankDepositCardRequest request)
        {
            return HttpClient.Post<BankDepositCardRequest, OperationResponse<BankCardTransactionId>>($"{Url}/{nameof(DepositToBankCard)}", request, FillRequestHeaders(connection));
        }

        public Task<HttpClientResponse<OperationResponse<BankCardTransactionId>>> WithdrawFromBankCard(RageMpClientConnection connection, BankWithdrawCardRequest request)
        {
            return HttpClient.Post<BankWithdrawCardRequest, OperationResponse<BankCardTransactionId>>($"{Url}/{nameof(WithdrawFromBankCard)}", request, FillRequestHeaders(connection));
        }

        public Task<HttpClientResponse<OperationResponse<BankCardTransactionId>>> TransferToBankCardNumber(RageMpClientConnection connection, BankTransferToCardRequest request)
        {
            return HttpClient.Post<BankTransferToCardRequest, OperationResponse<BankCardTransactionId>>($"{Url}/{nameof(TransferToBankCardNumber)}", request, FillRequestHeaders(connection));
        }

        public Task<HttpClientResponse<OperationResponse<BankCardBalanceModel>>> GetBankCardBalance(RageMpClientConnection connection, BankGetCardBalanceRequest request)
        {
            return HttpClient.Post<BankGetCardBalanceRequest, OperationResponse<BankCardBalanceModel>>($"{Url}/{nameof(GetBankCardBalance)}", request, FillRequestHeaders(connection));
        }

        public Task<HttpClientResponse<OperationResponse<BankCardOwnerModel>>> GetBankCardOwner(RageMpClientConnection connection, BankGetCardOwnerRequest request)
        {
            return HttpClient.Post<BankGetCardOwnerRequest, OperationResponse<BankCardOwnerModel>>($"{Url}/{nameof(GetBankCardOwner)}", request, FillRequestHeaders(connection));
        }

        public Task<HttpClientResponse<OperationResponse<BankCardTransactionPreviewModel[]>>> GetBankCardTransactions(RageMpClientConnection connection, BankGetCardTransactionsRequest request)
        {
            return HttpClient.Post<BankGetCardTransactionsRequest, OperationResponse<BankCardTransactionPreviewModel[]>>($"{Url}/{nameof(GetBankCardTransactions)}", request, FillRequestHeaders(connection));
        }

        public Task<HttpClientResponse<OperationResponse<BankGetCardInformationResponse>>> GetBankCardInformation(RageMpClientConnection connection, BankGetCardInformationRequest request)
        {
            return HttpClient.Post<BankGetCardInformationRequest, OperationResponse<BankGetCardInformationResponse>>($"{Url}/{nameof(GetBankCardInformation)}", request, FillRequestHeaders(connection));
        }

        public Task<HttpClientResponse<OperationResponse<BankCardPreviewModel[]>>> GetPlayerBankCardList(RageMpClientConnection connection, PropertyId propertyId)
        {
            return HttpClient.Get< OperationResponse<BankCardPreviewModel[]>>($"{Url}/{nameof(GetPlayerBankCardList)}?id={propertyId.Id}", FillRequestHeaders(connection));
        }
    }
}
