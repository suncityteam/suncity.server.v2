﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using SunCity.IntegrationTests.HttpClients.World.Property.Business.Bank.Client;

namespace SunCity.IntegrationTests.HttpClients.World.Property.Business.Bank
{
    internal static class ServiceRecorder
    {
        public static IServiceCollection AddWorldBankBusinessPropertyHttpClients(this IServiceCollection InServiceCollection, IConfiguration InConfiguration)
        {
            InServiceCollection.AddSingleton<BankCardAccountHttpClient>();
            return InServiceCollection;
        }
    }
}
