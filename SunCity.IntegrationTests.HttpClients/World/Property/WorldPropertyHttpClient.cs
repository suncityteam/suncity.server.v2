﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SunCity.Common.HttpClient;
using SunCity.Common.Operation;
using SunCity.Game.World.Property.Operations.Information;
using SunCity.Game.World.Property.Operations.Purchase;
using SunCity.Game.World.Property.Operations.SellingInformation;
using SunCity.IntegrationTests.HttpClients.Base;
using SunCity.IntegrationTests.HttpClients.Base.Connections;
using SunCity.IntegrationTests.HttpClients.Factories;

namespace SunCity.IntegrationTests.HttpClients.World.Property
{
    public sealed class WorldPropertyHttpClient : BaseTestHttpClient
    {
        private static string Url { get; } = "World/Property/Property";

        public WorldPropertyHttpClient(ITestServerHttpClient httpClient) : base(httpClient)
        {
        }

        public Task<HttpClientResponse<OperationResponse<GetWorldPropertyInformationResponse>>> GetInformation(RageMpClientConnection connection, PropertyId propertyId)
        {
            return HttpClient.Get<OperationResponse<GetWorldPropertyInformationResponse>>($"{Url}/{nameof(GetInformation)}?id={propertyId.Id}", FillRequestHeaders(connection));
        }

        public Task<HttpClientResponse<OperationResponse<GetWorldPropertySellingInformationResponse>>> GetSellingInformation(RageMpClientConnection connection, PropertyId propertyId)
        {
            return HttpClient.Get<OperationResponse<GetWorldPropertySellingInformationResponse>>($"{Url}/{nameof(GetSellingInformation)}?id={propertyId.Id}", FillRequestHeaders(connection));
        }

        public Task<HttpClientResponse<OperationResponse<PurchaseWorldPropertyResponse>>> Purchase(RageMpClientConnection connection, PurchaseWorldPropertyRequest request)
        {
            return HttpClient.Post<PurchaseWorldPropertyRequest, OperationResponse<PurchaseWorldPropertyResponse>>($"{Url}/{nameof(Purchase)}", request, FillRequestHeaders(connection));
        }
    }
}