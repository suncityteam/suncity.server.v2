﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using SunCity.IntegrationTests.HttpClients.World.GameItems;
using SunCity.IntegrationTests.HttpClients.World.Property;

namespace SunCity.IntegrationTests.HttpClients.World
{
    internal static class ServiceRecorder
    {
        public static IServiceCollection AddWorldHttpClients(this IServiceCollection InServiceCollection, IConfiguration InConfiguration)
        {
            InServiceCollection.AddSingleton<GameItemsHttpClient>();
            InServiceCollection.AddWorldPropertyHttpClients(InConfiguration);

            return InServiceCollection;
        }
    }
}
