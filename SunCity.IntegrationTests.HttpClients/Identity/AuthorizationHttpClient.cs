﻿using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using SunCity.Common.HttpClient;
using SunCity.Common.Operation;
using SunCity.Game.World.Identity.Operations.Authorization.Login;
using SunCity.Game.World.Identity.Operations.Authorization.Register;
using SunCity.IntegrationTests.HttpClients.Base;
using SunCity.IntegrationTests.HttpClients.Base.Connections;
using SunCity.IntegrationTests.HttpClients.Factories;

namespace SunCity.IntegrationTests.HttpClients.Identity
{
    public sealed class AuthorizationHttpClient : BaseTestHttpClient
    {
        private static string Url { get; } = "World/Identity/Authorization";

        public AuthorizationHttpClient(ITestServerHttpClient InHttpClient) : base(InHttpClient)
        {

        }

        public Task<HttpClientResponse<OperationResponse<AuthorizationLoginResponse>>> Login(RageMpClientConnection connection, AuthorizationLoginRequest form)
        {
            return HttpClient.Post<AuthorizationLoginRequest, OperationResponse<AuthorizationLoginResponse>>($"{Url}/{nameof(Login)}", form, FillRequestHeaders(connection));
        }

        public Task<HttpClientResponse<OperationResponse<AuthorizationRegisterResponse>>> Register(RageMpClientConnection connection, AuthorizationRegisterRequest form)
        {
            return HttpClient.Post<AuthorizationRegisterRequest, OperationResponse<AuthorizationRegisterResponse>>($"{Url}/{nameof(Register)}", form, FillRequestHeaders(connection));
        }


    }
}