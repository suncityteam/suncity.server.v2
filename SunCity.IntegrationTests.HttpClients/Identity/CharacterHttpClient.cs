﻿using System.Collections.Generic;
using System.Threading.Tasks;
using SunCity.Common.HttpClient;
using SunCity.Common.Operation;
using SunCity.Game.Identity.Forms.Character;
using SunCity.Game.World.Enums.Character;
using SunCity.Game.World.Identity.Operations.Character.AccountInformation;
using SunCity.Game.World.Identity.Operations.Character.CreateCharacter;
using SunCity.Game.World.Identity.Operations.Character.JoinToWorld;
using SunCity.IntegrationTests.HttpClients.Base;
using SunCity.IntegrationTests.HttpClients.Base.Connections;
using SunCity.IntegrationTests.HttpClients.Factories;

namespace SunCity.IntegrationTests.HttpClients.Identity
{
    public sealed class CharacterHttpClient : BaseTestHttpClient
    {
        private static string Url { get; } = "World/Identity/Character";

        public CharacterHttpClient(ITestServerHttpClient httpClient) : base(httpClient)
        {

        }

        public Task<HttpClientResponse<OperationResponse<AuthorizationWhatToDo>>> WhatToDo(RageMpClientConnection connection)
        {
            return HttpClient.Get<OperationResponse<AuthorizationWhatToDo>>($"{Url}/{nameof(WhatToDo)}", FillRequestHeaders(connection));
        }

        public Task<HttpClientResponse<OperationResponse<GetAccountInformationResponse>>> GetList(RageMpClientConnection connection)
        {
            return HttpClient.Get<OperationResponse<GetAccountInformationResponse>>($"{Url}/{nameof(GetList)}", FillRequestHeaders(connection));
        }

        public Task<HttpClientResponse<OperationResponse>> Select(RageMpClientConnection connection, CharacterJoinToWorldRequest form)
        {
            return HttpClient.Post<CharacterJoinToWorldRequest, OperationResponse>($"{Url}/{nameof(Select)}", form, FillRequestHeaders(connection));
        }

        public Task<HttpClientResponse<OperationResponse>> Create(RageMpClientConnection connection, CreateCharacterRequest request)
        {
            return HttpClient.Post<CreateCharacterRequest, OperationResponse>($"{Url}/{nameof(Create)}", request, FillRequestHeaders(connection));
        }

    }
}