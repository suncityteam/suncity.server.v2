﻿using System;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using SunCity.Common.Json.Converters;
using SunCity.IntegrationTests.HttpClients.Factories;
using SunCity.IntegrationTests.HttpClients.Identity;
using SunCity.IntegrationTests.HttpClients.World;

namespace SunCity.IntegrationTests.HttpClients
{
    public static class ServiceRecorder
    {
        public static IServiceCollection AddHttpClients(this IServiceCollection InServiceCollection, IConfiguration InConfiguration)
        {
            InServiceCollection.AddCommonHttpClients(InConfiguration);
            InServiceCollection.AddIdentityHttpClients(InConfiguration);
            InServiceCollection.AddWorldHttpClients(InConfiguration);

            return InServiceCollection;
        }

        private static IServiceCollection AddCommonHttpClients(this IServiceCollection InServiceCollection, IConfiguration InConfiguration)
        {
            InServiceCollection.AddTestServerHttpClient(InConfiguration);
            InServiceCollection.UseCommonJsonSerializer();
            return InServiceCollection;
        }

        private static IServiceCollection AddIdentityHttpClients(this IServiceCollection InServiceCollection, IConfiguration InConfiguration)
        {
            InServiceCollection.AddSingleton<AuthorizationHttpClient>();
            InServiceCollection.AddSingleton<CharacterHttpClient>();

            return InServiceCollection;
        }
    }
}
