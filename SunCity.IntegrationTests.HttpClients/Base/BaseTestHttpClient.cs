﻿using System;
using System.Net.Http.Headers;
using SunCity.IntegrationTests.HttpClients.Base.Connections;
using SunCity.IntegrationTests.HttpClients.Factories;

namespace SunCity.IntegrationTests.HttpClients.Base
{
    public abstract class BaseTestHttpClient
    {
        protected Func<RageMpClientConnection, Action<HttpRequestHeaders>> FillRequestHeaders { get; }
        protected ITestServerHttpClient HttpClient { get; }

        protected BaseTestHttpClient(ITestServerHttpClient httpClient)
        {
            HttpClient = httpClient;
            FillRequestHeaders = connection => headers => ProcessFillRequestHeaders(connection, headers);
        }

        protected virtual void ProcessFillRequestHeaders(RageMpClientConnection connection, HttpRequestHeaders headers)
        {
            headers.Add("RageMp", connection.Player.Id.ToString());
        }
    }
}