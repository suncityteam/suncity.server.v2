﻿using SunCity.Common.RageMp.Pools.Players;

namespace SunCity.IntegrationTests.HttpClients.Base.Connections
{
    public sealed class RageMpClientConnection
    {
        public IRagePlayer Player { get; }

        public RageMpClientConnection(IRagePlayer player)
        {
            Player = player;
        }

        public override string ToString()
        {
            return $"[Player: {Player.Name} / Ид: {Player.Id}]";
        }
    }
}