﻿using Microsoft.Extensions.Hosting;
using SunCity.Common;

namespace SunCity.Server.Test
{
    public class TestWebServerStartup : WebServerStartup
    {
        public TestWebServerStartup(IHostEnvironment InHostEnvironment) : base(InHostEnvironment)
        {
            InHostEnvironment.EnvironmentName = EnvironmentNames.INTEGRATION_TESTS;
        }
    }
}