﻿    using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Hosting;
using SunCity.Common;

namespace SunCity.Server.Test
{
    public class Program
    {
        public static Task Main(string[] InArgs)
        {
            return CreateHostBuilder(InArgs).StartAsync();
        }

        // EF Core uses this method at design time to access the DbContext
        public static IHostBuilder CreateHostBuilder(string[] InArgs)
        {
            return Host.CreateDefaultBuilder(InArgs)
                .ConfigureWebHostDefaults(Configure);
        }

        private static void Configure(IWebHostBuilder webHostBuilder)
        {
            webHostBuilder.UseStartup<WebServerStartup>();
            webHostBuilder.UseEnvironment(EnvironmentNames.INTEGRATION_TESTS);
        }
    }
}
