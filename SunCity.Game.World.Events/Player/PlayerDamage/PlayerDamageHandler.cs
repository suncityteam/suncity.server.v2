﻿using System.Threading;
using System.Threading.Tasks;
using SunCity.Common.Events.Attributes;
using SunCity.Common.Events.Interfaces;
using SunCity.Common.Operation;

namespace SunCity.Game.World.Events.Player.PlayerDamage
{
    [IgnoreGameEvent]
    public class PlayerDamageHandler : RageMpEventHandler<PlayerDamageRequest>
    {
        public PlayerDamageHandler(RageMpEventConstructor<PlayerDamageRequest> constructor) : base(constructor)
        {
        }

        public override async Task<OperationResponse> Handle(PlayerDamageRequest request, CancellationToken cancellationToken)
        {
            throw new System.NotImplementedException();
        }
    }
}