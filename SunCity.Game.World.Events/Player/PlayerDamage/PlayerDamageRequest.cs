﻿using SunCity.Common.Events.Interfaces;

namespace SunCity.Game.World.Events.Player.PlayerDamage
{
    public class PlayerDamageRequest : IRageMpEventRequest
    {
        public float HealthLoss { get; set; }
        public float ArmourLoss { get; set; }
    }
}