﻿using SunCity.Common.Events.Interfaces;

namespace SunCity.Game.World.Events.Player.PlayerConnected
{
    public class PlayerConnectedRequest : IRageMpEventRequest
    {
        public static PlayerConnectedRequest Request { get; } = new PlayerConnectedRequest();
    }
}