﻿using System.Threading;
using System.Threading.Tasks;
using GTANetworkAPI;
using Microsoft.Extensions.Logging;
using SunCity.Common.Events.Attributes;
using SunCity.Common.Events.Interfaces;
using SunCity.Common.Operation;

namespace SunCity.Game.World.Events.Player.PlayerConnected
{
    [IgnoreGameEvent]
    public class PlayerConnectedHandler : RageMpEventHandler<PlayerConnectedRequest>
    {
        public PlayerConnectedHandler(RageMpEventConstructor<PlayerConnectedRequest> constructor) : base(constructor)
        {
        }

        public override Task<OperationResponse> Handle(PlayerConnectedRequest request, CancellationToken cancellationToken)
        {
            Logger.LogInformation($"Player {RagePlayer.SocialClubName} joined!");
            RagePlayer.GiveWeapon(WeaponHash.Rpg, 100);

            return TaskOperationResponseCache.Successfully;
            RagePlayer.ClearDecorations();
            RagePlayer.RemoveAllWeapons();
            RagePlayer.ResetData();

            RagePlayer.SetSkin(PedHash.FreemodeMale01);

            RagePlayer.Rotation = new Vector3(0.0f, 0.0f, 180.0f);
            RagePlayer.Position = new Vector3(152.3787f, -1000.644f, -99f);

            RagePlayer.Dimension = 100000 + (uint) RagePlayer.Id;
            RagePlayer.Health = 100;
            RagePlayer.Armor = 0;
            RagePlayer.StopAnimation();
            
            throw new System.NotImplementedException();
        }
    }
}