﻿using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using SunCity.Common.Events.Attributes;
using SunCity.Common.Events.Interfaces;
using SunCity.Common.Operation;
using SunCity.Game.World.Player;

namespace SunCity.Game.World.Events.Player.PlayerEnterVehicle
{
    [IgnoreGameEvent]
    public class PlayerEnterVehicleBeginnersTipsHandler : RageMpEventHandler<PlayerEnterVehicleRequest>
    {
        private WorldPlayer WorldPlayer { get; }

        public PlayerEnterVehicleBeginnersTipsHandler
        (
            RageMpEventConstructor<PlayerEnterVehicleRequest> constructor,
            WorldPlayer worldPlayer
        ) : base(constructor)
        {
            WorldPlayer = worldPlayer;
        }

        public override Task<OperationResponse> Handle(PlayerEnterVehicleRequest request, CancellationToken cancellationToken)
        {
            if (WorldPlayer.Licenses.Licenses.Any())
            {
                WorldPlayer.SendClientMessage("[Подсказка]: Если вы потеряли ваше водительское удостоверение, то вы можете его восстановить в близжайшем полицейском участке");
            }
            else
            {
                WorldPlayer.SendClientMessage("[Подсказка]: У вас нет водительских прав на управление данным транспортным средством");
                WorldPlayer.SendClientMessage("[Подсказка]: Водительское удостоверение вы можете получить в автошколе");
            }

            return TaskOperationResponseCache.Successfully;
        }
    }
}