﻿using SunCity.Common.Events.Interfaces;

namespace SunCity.Game.World.Events.Player.PlayerEnterVehicle
{
    public class PlayerEnterVehicleRequest: IRageMpEventRequest
    {
        public ushort TargetVehicleId { get; set; }
        public sbyte SeatId { get; set; }
    }
}