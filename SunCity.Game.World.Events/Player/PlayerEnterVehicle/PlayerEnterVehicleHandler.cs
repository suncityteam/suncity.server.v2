﻿using System.Threading;
using System.Threading.Tasks;
using SunCity.Common.Events.Attributes;
using SunCity.Common.Events.Interfaces;
using SunCity.Common.Operation;
using SunCity.Game.World.Player;

namespace SunCity.Game.World.Events.Player.PlayerEnterVehicle
{
    [IgnoreGameEvent]
    public class PlayerEnterVehicleHandler : RageMpEventHandler<PlayerEnterVehicleRequest>
    {
        private WorldPlayer WorldPlayer { get; }

        public PlayerEnterVehicleHandler
        (
            RageMpEventConstructor<PlayerEnterVehicleRequest> constructor,
            WorldPlayer worldPlayer
        ) : base(constructor)
        {
            WorldPlayer = worldPlayer;
        }

        public override Task<OperationResponse> Handle(PlayerEnterVehicleRequest request, CancellationToken cancellationToken)
        {
            return TaskOperationResponseCache.Successfully;
        }
    }
}