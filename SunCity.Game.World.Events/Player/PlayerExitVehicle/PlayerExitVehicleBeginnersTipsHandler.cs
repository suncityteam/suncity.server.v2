﻿using System.Threading;
using System.Threading.Tasks;
using SunCity.Common.Events.Attributes;
using SunCity.Common.Events.Interfaces;
using SunCity.Common.Operation;
using SunCity.Game.World.Player;

namespace SunCity.Game.World.Events.Player.PlayerExitVehicle
{
    [IgnoreGameEvent]
    public class PlayerExitVehicleBeginnersTipsHandler : RageMpEventHandler<PlayerExitVehicleRequest>
    {
        private WorldPlayer WorldPlayer { get; }

        public PlayerExitVehicleBeginnersTipsHandler
        (
            RageMpEventConstructor<PlayerExitVehicleRequest> constructor,
            WorldPlayer worldPlayer
        ) : base(constructor)
        {
            WorldPlayer = worldPlayer;
        }

        public override Task<OperationResponse> Handle(PlayerExitVehicleRequest request, CancellationToken cancellationToken)
        {
            WorldPlayer.SendClientMessage("[Подсказка]: Не забывайте закрывать машину после выхода из нее. Используте для этого контекстное меню");
            return TaskOperationResponseCache.Successfully;
        }
    }
}