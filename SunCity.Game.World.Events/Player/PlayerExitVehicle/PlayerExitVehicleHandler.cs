﻿using System.Threading;
using System.Threading.Tasks;
using SunCity.Common.Events.Attributes;
using SunCity.Common.Events.Interfaces;
using SunCity.Common.Operation;
using SunCity.Game.World.Player;

namespace SunCity.Game.World.Events.Player.PlayerExitVehicle
{
    [IgnoreGameEvent]
    public class PlayerExitVehicleHandler : RageMpEventHandler<PlayerExitVehicleRequest>
    {
        private WorldPlayer WorldPlayer { get; }

        public PlayerExitVehicleHandler
        (
            RageMpEventConstructor<PlayerExitVehicleRequest> constructor,
            WorldPlayer worldPlayer
        ) : base(constructor)
        {
            WorldPlayer = worldPlayer;
        }

        public override Task<OperationResponse> Handle(PlayerExitVehicleRequest request, CancellationToken cancellationToken)
        {
            WorldPlayer.Vehicle.ExitVehicle(null);
            return TaskOperationResponseCache.Successfully;
        }
    }
}