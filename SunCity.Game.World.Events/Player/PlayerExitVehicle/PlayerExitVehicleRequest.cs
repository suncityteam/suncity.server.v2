﻿using SunCity.Common.Events.Interfaces;

namespace SunCity.Game.World.Events.Player.PlayerExitVehicle
{
    public class PlayerExitVehicleRequest: IRageMpEventRequest
    {
        public ushort TargetVehicleId { get; set; }

    }
}