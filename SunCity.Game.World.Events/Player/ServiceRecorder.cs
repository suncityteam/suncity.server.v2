﻿using Microsoft.Extensions.DependencyInjection;
using SunCity.Game.World.Events.Player.PlayerConnected;
using SunCity.Game.World.Events.Player.PlayerDamage;
using SunCity.Game.World.Events.Player.PlayerDeath;
using SunCity.Game.World.Events.Player.PlayerEnterVehicle;
using SunCity.Game.World.Events.Player.PlayerExitVehicle;

namespace SunCity.Game.World.Events.Player
{
    public static class ServiceRecorder
    {
        public static void AddGameWorldPlayerEvents(this IServiceCollection InServiceCollection)
        {
            InServiceCollection.AddRageMpEventHandler<PlayerConnectedRequest, PlayerConnectedHandler>();
            InServiceCollection.AddRageMpEventHandler<PlayerDamageRequest, PlayerDamageHandler>();

            InServiceCollection.AddRageMpEventHandler<PlayerDeathRequest, PlayerDeathHandler>();
            InServiceCollection.AddRageMpEventHandler<PlayerDeathRequest, PlayerDeathBeginnersTipsHandler>();
            
            InServiceCollection.AddRageMpEventHandler<PlayerEnterVehicleRequest, PlayerEnterVehicleHandler>();
            InServiceCollection.AddRageMpEventHandler<PlayerEnterVehicleRequest, PlayerEnterVehicleBeginnersTipsHandler>();
            
            InServiceCollection.AddRageMpEventHandler<PlayerExitVehicleRequest, PlayerExitVehicleHandler>();
            InServiceCollection.AddRageMpEventHandler<PlayerExitVehicleRequest, PlayerExitVehicleBeginnersTipsHandler>();
        }
    }
}