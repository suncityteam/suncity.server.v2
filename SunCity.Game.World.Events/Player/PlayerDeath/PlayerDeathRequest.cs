﻿using SunCity.Common.Events.Interfaces;

namespace SunCity.Game.World.Events.Player.PlayerDeath
{
    public class PlayerDeathRequest : IRageMpEventRequest
    {
        public ushort? KillerPlayerId { get; set; }
        public uint DeathReason { get; set; }
    }
}