﻿using System.Threading;
using System.Threading.Tasks;
using SunCity.Common.Events.Attributes;
using SunCity.Common.Events.Interfaces;
using SunCity.Common.Operation;
using SunCity.Game.World.Player;
using SunCity.Game.World.Property;

namespace SunCity.Game.World.Events.Player.PlayerDeath
{
    [IgnoreGameEvent]
    public class PlayerDeathBeginnersTipsHandler : RageMpEventHandler<PlayerDeathRequest>
    {
        private WorldPlayer WorldPlayer { get; }
        private IWorldPlayerStorage PlayerStorage { get; }
        private IWorldPropertyStorage PropertyStorage { get; }

        public PlayerDeathBeginnersTipsHandler
        (
            RageMpEventConstructor<PlayerDeathRequest> constructor,
            WorldPlayer worldPlayer,
            IWorldPlayerStorage playerStorage,
            IWorldPropertyStorage propertyStorage
        ) : base(constructor)
        {
            WorldPlayer = worldPlayer;
            PlayerStorage = playerStorage;
            PropertyStorage = propertyStorage;
        }

        public override Task<OperationResponse> Handle(PlayerDeathRequest request, CancellationToken cancellationToken)
        {
            WorldPlayer.SendClientMessage($"[Подсказка] После смерти вы окажетесь в близжайшем госпитале");
            WorldPlayer.SendClientMessage($"[Подсказка] Вы можете приобрести страховку что снизить затраты на лечение");
            return TaskOperationResponseCache.Successfully;
        }
    }
}