﻿using System.Threading;
using System.Threading.Tasks;
using SunCity.Common.Events.Attributes;
using SunCity.Common.Events.Interfaces;
using SunCity.Common.Operation;
using SunCity.Game.World.Player;
using SunCity.Game.World.Property;

namespace SunCity.Game.World.Events.Player.PlayerDeath
{
    [IgnoreGameEvent]
    public class PlayerDeathHandler : RageMpEventHandler<PlayerDeathRequest>
    {
        private WorldPlayer WorldPlayer { get; }
        private IWorldPlayerStorage PlayerStorage { get; }
        private IWorldPropertyStorage PropertyStorage { get; }

        public PlayerDeathHandler
        (
            RageMpEventConstructor<PlayerDeathRequest> constructor,
            WorldPlayer worldPlayer,
            IWorldPlayerStorage playerStorage,
            IWorldPropertyStorage propertyStorage
        ) : base(constructor)
        {
            WorldPlayer = worldPlayer;
            PlayerStorage = playerStorage;
            PropertyStorage = propertyStorage;
        }

        public override async Task<OperationResponse> Handle(PlayerDeathRequest request, CancellationToken cancellationToken)
        {
            var killer = request.KillerPlayerId.HasValue ? RageMpPool.Players.GetAt(request.KillerPlayerId.Value) : RagePlayer;

            var nearHospital = await PropertyStorage.FindNearHospital(RagePlayer.Position);
            PlayerStorage.TryGetPlayer(killer, out var killerWorldPlayer);
            await nearHospital.OnPlayerDeath(WorldPlayer, request.DeathReason, killerWorldPlayer);

            return OperationResponseCache.Successfully;
        }
    }
}