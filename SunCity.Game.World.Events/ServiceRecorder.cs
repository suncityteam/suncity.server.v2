﻿using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using MediatR;
using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.DependencyInjection;
using SunCity.Common.Events.Interfaces;
using SunCity.Common.MediatR;
using SunCity.Common.MediatR.Extensions;
using SunCity.Game.World.Events.Player;
using SunCity.Game.World.Events.Property.GetPropertyList;
using SunCity.Game.World.Events.Property.PropertyEnterOrExit;
using SunCity.Game.World.Events.Vehicle.Control;
using SunCity.Game.World.Events.Vehicle.Control.ToggleVehicleEngine;
using SunCity.Game.World.Events.Vehicle.Control.ToggleVehicleSeatBelt;

namespace SunCity.Game.World.Events
{
    public static class ServiceRecorder
    {
        public static void AddGameWorldEvents(this IServiceCollection InServiceCollection)
        {          
            InServiceCollection.AddGameWorldPlayerEvents();

            InServiceCollection.AddSingleton<GameWorldEvents>();
            InServiceCollection.AddScoped<IRageMpEventMiddleware, RageMpPlayerResolver>();

            InServiceCollection.AddRageMpEventHandler<GetPropertyListRequest, GetPropertyList>();
            InServiceCollection.AddRageMpEventHandler<PropertyEnterOrExitRequest, PropertyEnterOrExitHandler>();

            InServiceCollection.AddRageMpEventHandler<ToggleVehicleEngineRequest, ToggleVehicleEngineHandler>();
            InServiceCollection.AddRageMpEventHandler<ToggleVehicleSeatBeltRequest, ToggleVehicleSeatBeltHandler>();

        }

        public static IServiceCollection AddRageMpEventHandler<TRequest, THandler>(this IServiceCollection InServiceCollection) 
            where TRequest : IRageMpEventRequest 
            where THandler : class, IOperationHandler<TRequest>
        {
            InServiceCollection.AddRequestHandler<TRequest, THandler>();

            return InServiceCollection;
        }

        public static void AddGameWorldEvents(this IApplicationBuilder InApplicationBuilder)
        {
            InApplicationBuilder.ApplicationServices.GetService<GameWorldEvents>();
        }
    }
}