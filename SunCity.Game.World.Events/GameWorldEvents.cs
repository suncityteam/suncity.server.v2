﻿using System.Threading.Tasks;
using GTANetworkAPI;
using Microsoft.Extensions.Logging;
using SunCity.Common.RageMp.Pools;
using SunCity.Common.RageMp.Pools.Players;
using SunCity.Common.RageMp.Pools.Vehicles;
using SunCity.Game.World.Player;
using SunCity.Game.World.Property;
using SunCity.Game.World.Vehicle;

namespace SunCity.Game.World.Events
{
    public class GameWorldEvents
    {
        public static GameWorldEvents Instance { get; private set; }

        private IWorldPlayerStorage PlayerStorage { get; }
        private IWorldPropertyStorage PropertyStorage { get; }
        private IWorldVehicleStorage VehicleStorage { get; }
        private ILogger<GameWorldEvents> Logger { get; }
        private IRageMpPool RageMpPool { get; }

        public GameWorldEvents
        (
            IWorldPlayerStorage InPlayerStorage,
            IWorldPropertyStorage InPropertyStorage,
            IWorldVehicleStorage vehicleStorage,
            ILogger<GameWorldEvents> logger, IRageMpPool rageMpPool)
        {
            Instance = this;
            PlayerStorage = InPlayerStorage;
            PropertyStorage = InPropertyStorage;
            VehicleStorage = vehicleStorage;
            Logger = logger;
            RageMpPool = rageMpPool;
        }
        
        public void OnVehicleDamage(GTANetworkAPI.Vehicle vehicle, float bodyHealthLoss, float engineHealthLoss)
        {
            var worldVehicle = VehicleStorage[(IRageVehicle) vehicle];
            worldVehicle?.OnVehicleDamage(bodyHealthLoss, engineHealthLoss);
        }
    }
}