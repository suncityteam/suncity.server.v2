﻿using System;
using System.Threading;
using System.Threading.Tasks;
using SunCity.Common.Events.Attributes;
using SunCity.Common.Events.Interfaces;
using SunCity.Common.Operation;
using SunCity.Common.Types.Extensions;
using SunCity.Game.World.Player;
using SunCity.Game.World.Property;

namespace SunCity.Game.World.Events.Property.PropertyEnterOrExit
{
    [GameEvent("Property:EnterOrExit")]
    public sealed class PropertyEnterOrExitHandler : RageMpEventHandler<PropertyEnterOrExitRequest>
    {
        private WorldPlayer WorldPlayer { get; }
        private IWorldPropertyStorage PropertyStorage { get; }
        
        public PropertyEnterOrExitHandler(RageMpEventConstructor<PropertyEnterOrExitRequest> constructor, WorldPlayer worldPlayer, IWorldPropertyStorage propertyStorage) : base(constructor)
        {
            WorldPlayer = worldPlayer;
            PropertyStorage = propertyStorage;
        }

        public override async Task<OperationResponse> Handle(PropertyEnterOrExitRequest request, CancellationToken cancellationToken)
        {
            if (WorldPlayer.EnteredProperty == null)
            {
                var nearProperty = await PropertyStorage.FindNearProperty(WorldPlayer.Position, WorldPlayer.Dimension.GetRealDimension());
                if (nearProperty != null)
                {
                    await nearProperty.OnPlayerEnter(WorldPlayer);
                }
            }
            else if(WorldPlayer.EnteredProperty.HasInterior)
            {
                if (WorldPlayer.EnteredProperty.Interior.Position.InDistanceRange(WorldPlayer.Position, 2.5))
                {
                    await WorldPlayer.EnteredProperty.OnPlayerExit(WorldPlayer);
                }
                else if (WorldPlayer.EnteredProperty.Interior.TryGetNearAction(WorldPlayer.Position, out var nearAction))
                {
                    await WorldPlayer.EnteredProperty.OnAction(WorldPlayer, nearAction);
                }
            }
            else
            {
                WorldPlayer.SendClientMessage("Неудалось выйти из интерьера. Обратитесь за помощью к администратору!");
            }

            return OperationResponseCache.Successfully;
        }
    }
}