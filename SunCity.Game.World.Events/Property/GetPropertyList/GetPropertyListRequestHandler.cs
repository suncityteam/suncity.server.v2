﻿using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using SunCity.Common.Events.Attributes;
using SunCity.Common.Events.Interfaces;
using SunCity.Common.Operation;
using SunCity.Game.World.Extensions.Player;
using SunCity.Game.World.Models.Property.List;
using SunCity.Game.World.Property;

namespace SunCity.Game.World.Events.Property.GetPropertyList
{
    [GameEvent("Property:GetPropertyList")]
    public sealed class GetPropertyList : RageMpEventHandler<GetPropertyListRequest>
    {
        private IWorldPropertyStorage PropertyStorage { get; }
        
        public GetPropertyList(RageMpEventConstructor<GetPropertyListRequest> constructor, IWorldPropertyStorage propertyStorage) : base(constructor)
        {
            PropertyStorage = propertyStorage;
        }

        public override async Task<OperationResponse> Handle(GetPropertyListRequest request, CancellationToken cancellationToken)
        {
            var models = PropertyStorage.ValuesCopy.Select(q => new GetPropertyListResponse
            {
                EntityId = q.EntityId,
                Position = q.Position,
                Category = q.PropertyCategory,
                HasInterior = q.HasInterior,
                HasOwner = q.HasOwner
            }).ToArray();

            //await RagePlayer.CallClientEventHandler<GetPropertyListResponse[]>("Property:GetPropertyList", models);
            return OperationResponseCache.Successfully;
        }
    }
}
