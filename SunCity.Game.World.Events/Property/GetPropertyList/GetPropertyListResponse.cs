﻿using SunCity.Common.Types.Math;
using SunCity.Domain.Interfaces.Entity;
using SunCity.Game.Enums.World.Property;

namespace SunCity.Game.World.Models.Property.List
{
    public class GetPropertyListResponse : IEntityWithId<PropertyId>
    {
        public PropertyId EntityId { get; set; }
        public Vector3D Position { get; set; }
        public WorldPropertyCategory Category { get; set; }
        public bool HasOwner { get; set; }
        public bool HasInterior { get; set; }
    }
}
