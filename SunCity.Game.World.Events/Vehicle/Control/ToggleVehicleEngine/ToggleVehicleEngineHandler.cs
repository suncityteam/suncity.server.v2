﻿using System;
using System.Threading;
using System.Threading.Tasks;
using SunCity.Common.Events.Attributes;
using SunCity.Common.Events.Interfaces;
using SunCity.Common.Operation;
using SunCity.Game.World.Events.Vehicle.Control.ToggleVehicleEngine;
using SunCity.Game.World.Extensions.Player;
using SunCity.Game.World.Vehicle;

namespace SunCity.Game.World.Events.Vehicle.Control
{
    [GameEvent("Vehicle:ToggleEngine")]
    public sealed class ToggleVehicleEngineHandler : RageMpEventHandler<ToggleVehicleEngineRequest>
    {
        //  https://wiki.rage.mp/index.php?title=Vehicle::getEngineHealth
        private Random Random { get; } = new Random();
        private IWorldVehicleStorage VehicleStorage { get; }

        public ToggleVehicleEngineHandler(RageMpEventConstructor<ToggleVehicleEngineRequest> constructor, IWorldVehicleStorage vehicleStorage) : base(constructor)
        {
            VehicleStorage = vehicleStorage;
        }

        public override async Task<OperationResponse> Handle(ToggleVehicleEngineRequest request, CancellationToken cancellationToken)
        {
            var vehicle = RagePlayer.Vehicle;
            if (vehicle == null || vehicle.Exists == false)
            {
                return OperationResponseCache.Successfully;
            }

            var playerSeat = RagePlayer.VehicleSeat;
            if (playerSeat > 0)
            {
                //  Двигатель может завести только водитель
                return OperationResponseCache.Successfully;
            }

            var worldVehicle = VehicleStorage[vehicle];
            if (worldVehicle.PartsState.EngineState.Value)
            {
                await OnEngineDisabled(worldVehicle, "Вы заглушили двигатель");
                return OperationResponseCache.Successfully;
            }

            if (worldVehicle.Fuel.Fuel.Value < worldVehicle.Model.FuelConsumptionPerSec)
            {
                await OnEngineDisabled(worldVehicle, "В транспорте закончилось топливо");
                return OperationResponseCache.Successfully;
            }

            var vehicleEngineHealth = vehicle.Health;
            if (vehicleEngineHealth <= 5)
            {
                await OnEngineDisabled(worldVehicle, "Неудалось завести двигатель, двигатель сильно поврежден!");
            }
            else if (vehicleEngineHealth <= 400)
            {
                if (Random.Next(0, 10) >= 5)
                {
                    await OnEngineEnabled(worldVehicle);
                }
                else
                {
                    await OnEngineDisabled(worldVehicle, "Неудалось завести двигатель, двигатель поврежден!");
                }
            }
            else // vehicleEngineHealth > 400
            {
                await OnEngineEnabled(worldVehicle);
            }

            return OperationResponseCache.Successfully;
        }
        
        private async Task OnEngineDisabled(WorldVehicle InWorldVehicle, string InMessage)
        {
            InWorldVehicle.PartsState.EngineState.Value = false;
            RagePlayer.SendChatMessage(InMessage);
            RagePlayer.TriggerEvent("Vehicle:ToggleEngine", OperationResponseCache.False);
        }

        private async Task OnEngineEnabled(WorldVehicle InWorldVehicle)
        {
            InWorldVehicle.PartsState.EngineState.Value = true;
            RagePlayer.SendChatMessage("Вы завели двигатель");
            RagePlayer.TriggerEvent("Vehicle:ToggleEngine", OperationResponseCache.True);
        }
    }
}
