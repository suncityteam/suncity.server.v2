﻿using System;
using System.Threading;
using System.Threading.Tasks;
using SunCity.Common.Events.Attributes;
using SunCity.Common.Events.Interfaces;
using SunCity.Common.Operation;
using SunCity.Game.World.Player;

namespace SunCity.Game.World.Events.Vehicle.Control.ToggleVehicleSeatBelt
{
    [GameEvent("Vehicle:ToggleSeatBelt")]
    public sealed class ToggleVehicleSeatBeltHandler : RageMpEventHandler<ToggleVehicleSeatBeltRequest>
    {
        private WorldPlayer WorldPlayer { get; }
        
        public ToggleVehicleSeatBeltHandler(RageMpEventConstructor<ToggleVehicleSeatBeltRequest> constructor, WorldPlayer worldPlayer) : base(constructor)
        {
            WorldPlayer = worldPlayer;
        }

        public override Task<OperationResponse> Handle(ToggleVehicleSeatBeltRequest request, CancellationToken cancellationToken)
        {
            var vehicle = RagePlayer.Vehicle;
            if (vehicle == null || vehicle.Exists == false)
            {
                return TaskOperationResponseCache.Successfully;
            }

            if (WorldPlayer.Vehicle.SeatBeltState.Enabled)
            {
                RagePlayer.SendChatMessage("Вы отстегнули ремень безопасности");
                WorldPlayer.Vehicle.SeatBeltState.RemoveBelt();
                RagePlayer.TriggerEvent("Vehicle:ToggleEngine", OperationResponseCache.False);

            }
            else
            {
                WorldPlayer.Vehicle.SeatBeltState.UseBelt(vehicle);
                RagePlayer.SendChatMessage("Вы пристегнули ремень безопасности");
                RagePlayer.TriggerEvent("Vehicle:ToggleEngine", OperationResponseCache.True);
            }

            return TaskOperationResponseCache.Successfully;
        }
    }
}