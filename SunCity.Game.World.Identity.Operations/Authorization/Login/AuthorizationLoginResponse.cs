﻿using System.Diagnostics;
using SunCity.Game.Types.Player.Entity.Player;
using SunCity.Identity.DataBase.Keys;

namespace SunCity.Game.World.Identity.Operations.Authorization.Login
{
    [DebuggerDisplay("AccountId: {AccountId}, AccountLogin: {AccountLogin}")]
    public class AuthorizationLoginResponse
    {
        public AccountId AccountId { get; set; }
        public PlayerAccountLogin AccountLogin { get; set; }
    }
}