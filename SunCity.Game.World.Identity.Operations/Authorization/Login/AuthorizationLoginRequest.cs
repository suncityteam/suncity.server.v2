﻿using System.Diagnostics;
using SunCity.Common.MediatR;
using SunCity.Game.Types.Player.Entity.Player;

namespace SunCity.Game.World.Identity.Operations.Authorization.Login
{
    public struct AuthorizationLoginRequest : IOperationRequest<AuthorizationLoginResponse>
    {
        public PlayerAccountLogin Login { get; set; }
        public string Password { get; set; }

        public override string ToString()
        {
            return $"Login: {Login}, Password: {Password}";
        }
    }
}
