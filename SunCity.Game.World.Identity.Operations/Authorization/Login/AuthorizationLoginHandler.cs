﻿using System.Threading;
using System.Threading.Tasks;
using SunCity.Common.MediatR;
using SunCity.Common.Operation;
using SunCity.Common.RageMp.Pools.Players;
using SunCity.Game.Identity.Services.Sessions;
using SunCity.Identity.DataBase.Keys;
using SunCity.Identity.Services.Authorization.Forms;
using SunCity.Identity.Services.Authorization.Service;

namespace SunCity.Game.World.Identity.Operations.Authorization.Login
{
    internal class AuthorizationLoginHandler : IOperationHandler<AuthorizationLoginRequest, AuthorizationLoginResponse>
    {
        private IAuthorizationService AuthorizationService { get; }
        private IGameSessionManager SessionManager { get; }
        private IRagePlayer RagePlayer { get; }

        public AuthorizationLoginHandler
        (
            IAuthorizationService authorizationService,
            IGameSessionManager sessionManager, IRagePlayer ragePlayer)
        {
            AuthorizationService = authorizationService;
            SessionManager = sessionManager;
            RagePlayer = ragePlayer;
        }

        public async Task<OperationResponse<AuthorizationLoginResponse>> Handle(AuthorizationLoginRequest request, CancellationToken cancellationToken)
        {
            var loginResult = await AuthorizationService.Login(new AuthorizationLoginForm
            {
                Login = request.Login,
                Password = request.Password
            });

            if (loginResult.IsNotCorrect)
                return loginResult.Error;

            if (SessionManager.HasActiveSession(loginResult.Content.EntityId))
                return OperationResponseCode.PlayerHasActiveSession;

            SessionManager.StartGameSession(loginResult.Content.EntityId, loginResult.Content.Login);
            RagePlayer.SetData(nameof(AccountId), loginResult.Content.EntityId);

            return new AuthorizationLoginResponse
            {
                AccountId = loginResult.Content.EntityId,
                AccountLogin = loginResult.Content.Login
            };
        }
    }
}