﻿using System.Threading;
using System.Threading.Tasks;
using SunCity.Common.MediatR;
using SunCity.Common.Operation;
using SunCity.Common.RageMp.Pools.Players;
using SunCity.Game.Identity.Services.Sessions;
using SunCity.Identity.DataBase.Keys;
using SunCity.Identity.Services.Authorization.Forms;
using SunCity.Identity.Services.Authorization.Service;

namespace SunCity.Game.World.Identity.Operations.Authorization.Register
{
    public class AuthorizationRegisterHandler : IOperationHandler<AuthorizationRegisterRequest, AuthorizationRegisterResponse>
    {
        private IAuthorizationService AuthorizationService { get; }
        private IGameSessionManager SessionManager { get; }
        private IRagePlayer RagePlayer { get; }

        public AuthorizationRegisterHandler(IAuthorizationService authorizationService, IGameSessionManager sessionManager, IRagePlayer ragePlayer)
        {
            AuthorizationService = authorizationService;
            SessionManager = sessionManager;
            RagePlayer = ragePlayer;
        }

        public async Task<OperationResponse<AuthorizationRegisterResponse>> Handle(AuthorizationRegisterRequest request, CancellationToken cancellationToken)
        {
            var registerResult = await AuthorizationService.Register(new AuthorizationRegisterForm
            {
                Login = request.Login,
                Password = request.Password,
                Email = request.Email,
            });

            if (registerResult.IsNotCorrect)
                return registerResult.Error;

            if (SessionManager.HasActiveSession(registerResult.Content.EntityId))
                return OperationResponseCode.PlayerHasActiveSession;

            SessionManager.StartGameSession(registerResult.Content.EntityId, registerResult.Content.Login);
            RagePlayer.SetData(nameof(AccountId), registerResult.Content.EntityId);

            return new AuthorizationRegisterResponse
            {
                AccountId = registerResult.Content.EntityId,
                AccountLogin = registerResult.Content.Login
            };
        }
    }
}