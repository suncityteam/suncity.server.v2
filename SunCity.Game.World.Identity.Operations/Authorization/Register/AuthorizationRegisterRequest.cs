﻿using System.Diagnostics;
using SunCity.Common.MediatR;
using SunCity.Game.Types.Player.Entity.Player;

namespace SunCity.Game.World.Identity.Operations.Authorization.Register
{
    public struct AuthorizationRegisterRequest : IOperationRequest<AuthorizationRegisterResponse>
    {
        public PlayerAccountLogin Login { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public string RefererOrPromoCode { get; set; }


        public override string ToString()
        {
            return $"Login: {Login}, Password: {Password}, Email: {Email}, RefererOrPromoCode: {RefererOrPromoCode}";
        }
    }
}