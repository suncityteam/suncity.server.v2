﻿using System.Diagnostics;
using SunCity.Game.Types.Player.Entity.Player;
using SunCity.Identity.DataBase.Keys;
using SunCity.Identity.Models;

namespace SunCity.Game.World.Identity.Operations.Authorization.Register
{
    [DebuggerDisplay("AccountId: {AccountId}, AccountLogin: {AccountLogin}")]
    public class AuthorizationRegisterResponse
    {
        public AccountId AccountId { get; set; }
        public PlayerAccountLogin AccountLogin { get; set; }
    }
}