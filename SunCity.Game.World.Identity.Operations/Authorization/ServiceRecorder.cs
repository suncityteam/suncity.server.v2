﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using SunCity.Common.MediatR.Extensions;
using SunCity.Game.World.Identity.Operations.Authorization.Login;
using SunCity.Game.World.Identity.Operations.Authorization.Register;

namespace SunCity.Game.World.Identity.Operations.Authorization
{
    internal static class ServiceRecorder
    {
        public static IServiceCollection AddGameAuthorizationOperations(this IServiceCollection InServiceCollection, IConfiguration InConfiguration)
        {
            InServiceCollection.AddAuthorizationLoginRequest(InConfiguration);
            InServiceCollection.AddAuthorizationRegisterRequest(InConfiguration);

            return InServiceCollection;
        }

        private static void AddAuthorizationLoginRequest(this IServiceCollection InServiceCollection, IConfiguration InConfiguration)
        {
            //InServiceCollection.AddPipelineBehavior<AuthorizationLoginRequest, AuthorizationLoginResponse, AuthorizationLogin>();
            InServiceCollection.AddRequestHandler<AuthorizationLoginRequest, AuthorizationLoginResponse, AuthorizationLoginHandler>();
        }
        
        private static void AddAuthorizationRegisterRequest(this IServiceCollection InServiceCollection, IConfiguration InConfiguration)
        {
            //InServiceCollection.AddPipelineBehavior<AuthorizationLoginRequest, AuthorizationLoginResponse, AuthorizationLogin>();
            InServiceCollection.AddRequestHandler<AuthorizationRegisterRequest, AuthorizationRegisterResponse, AuthorizationRegisterHandler>();
        }
    }
}