﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using SunCity.Game.World.Identity.Operations.Authorization;
using SunCity.Game.World.Identity.Operations.Character;

namespace SunCity.Game.World.Identity.Operations
{
    public static class ServiceRecorder
    {
        public static IServiceCollection AddGameIdentityOperations(this IServiceCollection InServiceCollection, IConfiguration InConfiguration)
        {
            InServiceCollection.AddGameAuthorizationOperations(InConfiguration);
            InServiceCollection.AddGameCharactersOperations(InConfiguration);
            return InServiceCollection;
        }
    }
}