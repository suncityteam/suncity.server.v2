﻿using System;
using System.Threading;
using System.Threading.Tasks;
using SunCity.Common.MediatR;
using SunCity.Common.Operation;
using SunCity.Game.Identity.Models.Character;
using SunCity.Game.Identity.Services.Account.DbQueries;
using SunCity.Game.Identity.Services.Character.Services;
using SunCity.Game.Identity.Services.Sessions;
using SunCity.Game.World.DataBase.Entitites.Player;

namespace SunCity.Game.World.Identity.Operations.Character.AccountInformation
{
    public class GetAccountInformationHandler : IOperationHandler<GetAccountInformationRequest, GetAccountInformationResponse>
    {
        public GetAccountInformationHandler
        (
            IGameAccountDbQueries accountDbQueries,
            IGameCharactersService charactersService, IGameSession session)
        {
            AccountDbQueries = accountDbQueries;
            CharactersService = charactersService;
            Session = session;
        }

        private IGameAccountDbQueries AccountDbQueries { get; }
        private IGameCharactersService CharactersService { get; }
        private IGameSession Session { get; }

        public async Task<OperationResponse<GetAccountInformationResponse>> Handle(GetAccountInformationRequest request, CancellationToken cancellationToken)
        {
            var gameAccount = await AccountDbQueries.GetById(Session.AccountId);
            if (gameAccount == null)
                return OperationResponseCode.GameAccountNotFound;

            var characterList = await CharactersService.GetList(Session.AccountId);
            var model = MapEntityToModel(gameAccount, characterList);
            return model;
        }

        private static GetAccountInformationResponse MapEntityToModel(PlayerAccountEntity InPlayerAccountEntity, GameCharacterModel[] InPlayerCharacters)
        {
            var model = new GetAccountInformationResponse
            {
                PlayerAccountId = InPlayerAccountEntity.EntityId,
                Login = InPlayerAccountEntity.Login,

                Balance = InPlayerAccountEntity.Balance,

                Characters = InPlayerCharacters,
                DefaultCharacterId = InPlayerAccountEntity.DefaultCharacterId,
                HasPremiumAccount = InPlayerAccountEntity.PremiumAccountEndDate > DateTime.UtcNow
            };
            return model;
        }
    }
}