﻿using SunCity.Common.MediatR;

namespace SunCity.Game.World.Identity.Operations.Character.AccountInformation
{
    public class GetAccountInformationRequest : IOperationRequest<GetAccountInformationResponse>
    {
        
    }
}