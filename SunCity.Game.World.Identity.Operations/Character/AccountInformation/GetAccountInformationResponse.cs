﻿using SunCity.Game.Identity.Models.Character;
using SunCity.Game.Types.Player.Entity.Player;
using SunCity.Identity.DataBase.Keys;

namespace SunCity.Game.World.Identity.Operations.Character.AccountInformation
{
    public class GetAccountInformationResponse
    {
        public AccountId PlayerAccountId { get; set; }
        public PlayerAccountLogin Login { get; set; }
        public PlayerCharacterId? DefaultCharacterId { get; set; }

        public GameCharacterModel[] Characters { get; set; }
        public bool HasPremiumAccount { get; set; }
        public long Balance { get; set; }
    }
}