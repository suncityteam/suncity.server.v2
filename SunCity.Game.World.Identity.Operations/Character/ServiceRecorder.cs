﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using SunCity.Common.MediatR.Extensions;
using SunCity.Game.World.Identity.Operations.Character.AccountInformation;
using SunCity.Game.World.Identity.Operations.Character.CreateCharacter;
using SunCity.Game.World.Identity.Operations.Character.JoinToWorld;
using SunCity.Game.World.Identity.Operations.Character.WhatToDo;

namespace SunCity.Game.World.Identity.Operations.Character
{
    internal static class ServiceRecorder
    {
        public static IServiceCollection AddGameCharactersOperations(this IServiceCollection InServiceCollection, IConfiguration InConfiguration)
        {
            InServiceCollection.AddGetAccountInformationRequest(InConfiguration);
            InServiceCollection.AddCreateCharacterRequest(InConfiguration);
            InServiceCollection.AddCharacterJoinToWorldRequest(InConfiguration);
            InServiceCollection.AddGetCharacterWhatToDoRequest(InConfiguration);

            return InServiceCollection;
        }
        
        private static void AddGetAccountInformationRequest(this IServiceCollection InServiceCollection, IConfiguration InConfiguration)
        {
            InServiceCollection.AddRequestHandler<GetAccountInformationRequest, GetAccountInformationResponse, GetAccountInformationHandler>();
        }
        
        private static void AddCreateCharacterRequest(this IServiceCollection InServiceCollection, IConfiguration InConfiguration)
        {
            InServiceCollection.AddRequestHandler<CreateCharacterRequest, CreateCharacterResponse, CreateCharacterHandler>();
        }
        
        private static void AddGetCharacterWhatToDoRequest(this IServiceCollection InServiceCollection, IConfiguration InConfiguration)
        {
            InServiceCollection.AddRequestHandler<GetCharacterWhatToDoRequest, GetCharacterWhatToDoResponse, GetCharacterWhatToDoHandler>();
        }
        
        private static void AddCharacterJoinToWorldRequest(this IServiceCollection InServiceCollection, IConfiguration InConfiguration)
        {
            InServiceCollection.AddPipelineBehavior<CharacterJoinToWorldRequest, CharacterJoinToWorldResponse, CanCharacterJoinToWorldBehavior>();
            InServiceCollection.AddRequestHandler<CharacterJoinToWorldRequest, CharacterJoinToWorldResponse, CharacterJoinToWorldHandler>();
        }
        
    }
}