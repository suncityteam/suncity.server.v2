﻿using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using SunCity.Common.MediatR;
using SunCity.Common.Operation;
using SunCity.Common.RageMp.Pools.Players;
using SunCity.Game.Identity.Services.Character.DbQueries;
using SunCity.Game.Identity.Services.Character.Enums;
using SunCity.Game.Identity.Services.Sessions;
using SunCity.Game.World.Forms.Character;
using SunCity.Game.World.Models.Account;
using SunCity.Game.World.Models.Character;
using SunCity.Game.World.Player;
using SunCity.Game.World.Property;
using SunCity.Game.World.Services.Character.Inventory.Service;

namespace SunCity.Game.World.Identity.Operations.Character.JoinToWorld
{
    public class CharacterJoinToWorldHandler : IOperationHandler<CharacterJoinToWorldRequest, CharacterJoinToWorldResponse>
    {
        private IGameCharactersDbQueries GameCharactersDbQueries { get; }
        private IGameSession Session { get; }
        private IRagePlayer RagePlayer { get; }

        private ICharacterInventoryService InventoryService { get; }
        private IWorldPlayerStorage WorldPlayerStorage { get; }
        private IWorldPropertyStorage WorldPropertyStorage { get; }
        private IMapper Mapper { get; }

        public CharacterJoinToWorldHandler
        (
            IGameCharactersDbQueries gameCharactersDbQueries,
            ICharacterInventoryService inventoryService,
            IWorldPlayerStorage worldPlayerStorage,
            IWorldPropertyStorage worldPropertyStorage,
            IMapper mapper,
            IRagePlayer ragePlayer, IGameSession session)
        {
            GameCharactersDbQueries = gameCharactersDbQueries;
            InventoryService = inventoryService;
            WorldPlayerStorage = worldPlayerStorage;
            WorldPropertyStorage = worldPropertyStorage;
            Mapper = mapper;
            RagePlayer = ragePlayer;
            Session = session;
        }


        public async Task<OperationResponse<CharacterJoinToWorldResponse>> Handle(CharacterJoinToWorldRequest request, CancellationToken cancellationToken)
        {
            var characterEntity = await GameCharactersDbQueries.GetById(request.PlayerCharacterId, IncludeCharacterProps.Experience | IncludeCharacterProps.Skin | IncludeCharacterProps.Account);

            if (characterEntity.PlayerAccountEntity.DefaultCharacterId != request.PlayerCharacterId)
            {
                await GameCharactersDbQueries.UpdateDefaultCharacter(characterEntity.PlayerAccountId, characterEntity.EntityId);
            }

            var inventory = await InventoryService.LoadInventory(characterEntity.EntityId);
            if (inventory.IsNotCorrect)
                return inventory.Error;

            var joinForm = new WorldJoinCharacterForm
            {
                //=================================
                RagePlayer = RagePlayer,

                //=================================
                Balance = characterEntity.PlayerAccountEntity.Balance,

                //=================================
                Account = new PlayerAccountProperties
                {
                    Name = Session.AccountLogin,
                    EntityId = Session.AccountId
                },

                //=================================
                PlayerCharacterId = characterEntity.EntityId,
                PlayerCharacterName =  characterEntity.Name,

                //=================================
                PlayedForPayDay = characterEntity.Experience.PlayedForPayDay,

                //=================================
                Experience = new CharacterExperience
                {
                    Level = characterEntity.Experience.Level,
                    Experience = characterEntity.Experience.Experience,
                },

                //=================================
                Position = new PlayerCharacterPosition
                {
                    Position = characterEntity.Position,
                    Rotation = characterEntity.Rotation,
                    PropertyId = characterEntity.EnteredPropertyId
                },

                //=================================
                Health = characterEntity.Health,
                Hunger = characterEntity.Hunger,
                Thirst = characterEntity.Thirst,

                //=================================
                Skin = Mapper.Map<WorldJoinCharacterSkin>(characterEntity.Skin),

                //=================================
                Inventory = inventory.Content
            };

            var enteredProperty = WorldPropertyStorage[joinForm.Position.PropertyId];
            WorldPlayerStorage.JoinToWorld(joinForm, enteredProperty);

            return new CharacterJoinToWorldResponse();
        }
    }
}