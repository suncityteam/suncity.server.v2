﻿using SunCity.Common.MediatR;

namespace SunCity.Game.World.Identity.Operations.Character.JoinToWorld
{
    public class CharacterJoinToWorldRequest : IOperationRequest<CharacterJoinToWorldResponse>
    {
        public bool UseAsDefault { get; set; }
        public PlayerCharacterId PlayerCharacterId { get; set; }
    }
}