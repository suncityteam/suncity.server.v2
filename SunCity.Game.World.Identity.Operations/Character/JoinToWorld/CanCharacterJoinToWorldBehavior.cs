﻿using System.Threading;
using System.Threading.Tasks;
using MediatR;
using SunCity.Common.MediatR;
using SunCity.Common.Operation;
using SunCity.Common.RageMp.Pools.Players;
using SunCity.Game.Identity.Services.Character.DbQueries;
using SunCity.Game.Identity.Services.Character.Enums;
using SunCity.Game.Identity.Services.Sessions;

namespace SunCity.Game.World.Identity.Operations.Character.JoinToWorld
{
    public class CanCharacterJoinToWorldBehavior : IOperationPipelineBehavior<CharacterJoinToWorldRequest, CharacterJoinToWorldResponse>
    {
        private IGameCharactersDbQueries GameCharactersDbQueries { get; }
        private IGameSession Session { get; }
        private IRagePlayer RagePlayer { get; }

        public CanCharacterJoinToWorldBehavior
        (
            IGameCharactersDbQueries gameCharactersDbQueries,
            IRagePlayer ragePlayer, IGameSession session)
        {
            GameCharactersDbQueries = gameCharactersDbQueries;
            RagePlayer = ragePlayer;
            Session = session;
        }


        public async Task<OperationResponse<CharacterJoinToWorldResponse>> Handle(CharacterJoinToWorldRequest request, CancellationToken cancellationToken,
            RequestHandlerDelegate<OperationResponse<CharacterJoinToWorldResponse>> next)
        {
            var characterEntity = await GameCharactersDbQueries.GetById(request.PlayerCharacterId, IncludeCharacterProps.Experience | IncludeCharacterProps.Skin | IncludeCharacterProps.Account);

            if (characterEntity == null)
                return (OperationResponseCode.PlayerCharacterNotFound, request.PlayerCharacterId);

            if (characterEntity.PlayerAccountId != Session.AccountId)
                return (OperationResponseCode.PlayerCharacterNotOwned, request.PlayerCharacterId);

            if (characterEntity.IsBlocked)
                return (OperationResponseCode.PlayerCharacterIsBlocked, request.PlayerCharacterId);

            return await next();
        }
    }
}