﻿using System.Threading;
using System.Threading.Tasks;
using SunCity.Common.MediatR;
using SunCity.Common.Operation;
using SunCity.Game.Identity.Consts.Character;
using SunCity.Game.Identity.Services.Character.DbQueries;
using SunCity.Game.Identity.Services.Character.Forms;
using SunCity.Game.Identity.Services.Sessions;

namespace SunCity.Game.World.Identity.Operations.Character.CreateCharacter
{
    public class CreateCharacterHandler : IOperationHandler<CreateCharacterRequest, CreateCharacterResponse>
    {
        private IGameCharactersDbQueries GameCharactersDbQueries { get; }
        private IGameSession Session { get; }

        public CreateCharacterHandler
        (
            IGameCharactersDbQueries gameCharactersDbQueries,
            IGameSession session
        )
        {
            GameCharactersDbQueries = gameCharactersDbQueries;
            Session = session;
        }


        public async Task<OperationResponse<CreateCharacterResponse>> Handle(CreateCharacterRequest request, CancellationToken cancellationToken)
        {
            var createdCharacter = await GameCharactersDbQueries.Create(new CreateCharacterDbForm()
            {
                AccountId = Session.AccountId,
                SpawnPosition = CreateCharacterConsts.SpawnPosition,
                VirtualWorld = CreateCharacterConsts.SpawnVirtualWorld,

                Character = request.Character,
                Hairs = request.Hairs,

                Name = request.Name,
                FacialFeatures = request.FacialFeatures
            });

            return new CreateCharacterResponse
            {
                CreatedCharacterId = createdCharacter.EntityId
            };
        }
    }
}