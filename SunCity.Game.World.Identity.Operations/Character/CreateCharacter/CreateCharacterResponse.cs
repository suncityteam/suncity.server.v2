﻿namespace SunCity.Game.World.Identity.Operations.Character.CreateCharacter
{
    public class CreateCharacterResponse
    {
        public PlayerCharacterId CreatedCharacterId { get; set; }
    }
}