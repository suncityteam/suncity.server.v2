﻿using SunCity.Common.MediatR;
using SunCity.Game.Identity.Forms.Character.Create;

namespace SunCity.Game.World.Identity.Operations.Character.CreateCharacter
{
    public class CreateCharacterRequest : IOperationRequest<CreateCharacterResponse>
    {
        public CreateCharacterFacialFeatures FacialFeatures { get; set; }
        public CreateCharacterHairAndColor Hairs { get; set; }
        public CreateCharacterGeneralInformation Character { get; set; }
        public CreateCharacterPlayerName Name { get; set; }

        public override string ToString()
        {
            return $"Name: {Name} | Character: {Character.Sex}";
        }
    }
}