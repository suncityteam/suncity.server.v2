﻿using SunCity.Common.MediatR;

namespace SunCity.Game.World.Identity.Operations.Character.WhatToDo
{
    public class GetCharacterWhatToDoRequest : IOperationRequest<GetCharacterWhatToDoResponse>
    {
    }
}