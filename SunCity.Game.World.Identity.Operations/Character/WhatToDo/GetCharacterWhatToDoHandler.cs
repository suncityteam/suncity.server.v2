﻿using System.Threading;
using System.Threading.Tasks;
using SunCity.Common.Extensions;
using SunCity.Common.MediatR;
using SunCity.Common.Operation;
using SunCity.Common.RageMp.Pools.Players;
using SunCity.Game.Identity.Consts.Account;
using SunCity.Game.Identity.Services.Account.DbQueries;
using SunCity.Game.Identity.Services.Account.Forms;
using SunCity.Game.Identity.Services.Account.Service;
using SunCity.Game.Identity.Services.Character.Services;
using SunCity.Game.Identity.Services.Sessions;
using SunCity.Game.World.Enums.Character;

namespace SunCity.Game.World.Identity.Operations.Character.WhatToDo
{
    public class GetCharacterWhatToDoHandler : IOperationHandler<GetCharacterWhatToDoRequest, GetCharacterWhatToDoResponse>
    {
        private IGameAccountDbQueries AccountDbQueries { get; }
        private IGameAccountService GameAccountService { get; }
        private IGameCharactersService GameCharactersService { get; }
        private IGameSession Session { get; }
        private IRagePlayer RagePlayer { get; }

        public GetCharacterWhatToDoHandler
        (
            IGameAccountService gameAccountService,
            IGameCharactersService gameCharactersService,
            IGameAccountDbQueries accountDbQueries,
            IRagePlayer ragePlayer, IGameSession session)
        {
            GameAccountService = gameAccountService;
            GameCharactersService = gameCharactersService;
            AccountDbQueries = accountDbQueries;
            RagePlayer = ragePlayer;
            Session = session;
        }

        public async Task<OperationResponse<GetCharacterWhatToDoResponse>> Handle(GetCharacterWhatToDoRequest request, CancellationToken cancellationToken)
        {
            var existGameAccount = await GameAccountService.IsExist(Session.AccountId);
            if (existGameAccount == false)
            {
                var createAccountResult = await GameAccountService.Create(new CreateGameAccountForm
                {
                    AccountId = Session.AccountId,
                    AccountLogin = Session.AccountLogin,

                    Balance = CreateAccountConsts.StartBalance,
                    PremiunAccount = CreateAccountConsts.PremiunAccount,

                    DeviceId = RagePlayer.Serial
                });

                if (createAccountResult.IsNotCorrect)
                    return createAccountResult.Error;

                return new GetCharacterWhatToDoResponse(AuthorizationWhatToDo.CreateCharacter);
            }
            else
            {
                var gameAccount = await AccountDbQueries.GetById(Session.AccountId);
                if (gameAccount == null)
                    return OperationResponseCode.GameAccountNotFound;

                if (gameAccount.DefaultCharacterId.HasValue)
                {
                    return new GetCharacterWhatToDoResponse(AuthorizationWhatToDo.SpawnCharacter, gameAccount.DefaultCharacterId.Value);
                }

                if (gameAccount.CharacterEntities.IsEmpty())
                {
                    return new GetCharacterWhatToDoResponse(AuthorizationWhatToDo.CreateCharacter);
                }

                return new GetCharacterWhatToDoResponse(AuthorizationWhatToDo.SelectCharacter);
            }
        }
    }
}