﻿using SunCity.Game.World.Enums.Character;

namespace SunCity.Game.World.Identity.Operations.Character.WhatToDo
{
    public class GetCharacterWhatToDoResponse
    {
        public GetCharacterWhatToDoResponse(AuthorizationWhatToDo action)
        {
            Action = action;
        }
        
        public GetCharacterWhatToDoResponse(AuthorizationWhatToDo action, PlayerCharacterId? characterId)
        {
            Action = action;
            CharacterId = characterId;
        }

        public AuthorizationWhatToDo Action { get; }
        public PlayerCharacterId? CharacterId { get; }
    }
}